package sa.com.mobily.eportal.acct.mgmt.cards.portlet;

import java.io.*;
import java.util.logging.Level;

import javax.portlet.*;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.common.beanlocator.BeanLocator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.acct.mgmt.cards.constants.ConstantsIfc;
import sa.com.mobily.eportal.acct.mgmt.cards.service.view.RegisteredCardsServiceLocal;
import sa.com.mobily.eportal.acct.mgmt.cards.valueobjects.RegisteredCardsVO;
import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;

/**
 * A sample portlet based on GenericPortlet
 */
public class RegisteredCardsPortlet extends GenericPortlet {

	public static final String JSP_FOLDER    = "/_RegisteredCardsPortlet/jsp/";    // JSP folder name

	public static final String VIEW_JSP      = "RegisteredCardsPortletView";         // JSP file name to be rendered on the view mode
	public static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.ACCT_MGMT_CARDS_UI);
	private RegisteredCardsServiceLocal registerdCardsService;
	public static final String ERROR_PARAM = "errorMessage";
	public static final String MSISDNCOUNT_PARAM = "msisdnCount";

	private static final String CLASS_NAME = RegisteredCardsPortlet.class.getName(); 

	 
	/**
	 * @MethodName: init
	 * <p>
	 * Initialize portlet class with EJB lookup and logging object
	 * 
	 * 
	 * @throws PortletException
	 * 
	 * @author Abu Sharaf
	 */
	public void init() throws PortletException{
		final String methodName = " init()";
		
		executionContext.startMethod(CLASS_NAME,methodName,null);
		try {
			executionContext.log(Level.INFO," Init Method Called ",CLASS_NAME,methodName,null);
			registerdCardsService = (RegisteredCardsServiceLocal) BeanLocator.lookup("ejblocal:"+ RegisteredCardsServiceLocal.class.getName());
			executionContext.log(Level.INFO," Init Method Ended ",CLASS_NAME,methodName,null);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE,e.getMessage(),CLASS_NAME,methodName,e);
		} 
	}

	/**
	 * @MethodName: showRegisteredCards
	 * <p>
	 * Render JSP page with the list of registered credit card as per the User MSISDN
	 * 
	 * @param RenderRequest request
	 * @param RenderResponse response
	 * 
	 * @throws PortletException, IOException
	 * 
	 * @author Abu Sharaf
	 */
	@RenderMode(name=ConstantsIfc.RENDER_MODE_VIEW)
	public void showRegisteredCards(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		
		final String methodName = "showRegisteredCards";
		request.setAttribute("portlet-class", CLASS_NAME);
		request.setAttribute("portlet-method", methodName);
		
		executionContext.startMethod(CLASS_NAME,methodName,null);
		executionContext.log(Level.INFO," @RenderMode :: showRegisteredCards Method Called ",CLASS_NAME,methodName,null);
		
		
		// Set the MIME type for the render response
		response.setContentType(request.getResponseContentType());

		// testing .. request.setAttribute("msisdnCount", "10");
		
		// This is always active number

		
    	try {
    		PumaUtil pumaUtil = new PumaUtil();
    		String userId = pumaUtil.getUid(); 
    		executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > userId >"+userId,CLASS_NAME,methodName,null);
    		String aSessionId = request.getPortletSession().getId();
    		executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > aSessionId >"+aSessionId,CLASS_NAME,methodName,null);
    		SessionVO sessionvo = CacheManagerServiceUtil.getCachedData(aSessionId, userId);
    		executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > sessionvo >"+sessionvo,CLASS_NAME,methodName,null);
    		

    		String activeMSISDN = sessionvo.getMsisdn();
    	    	
    		// String activeMSISDN = "0541283555";
    			
    		executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > activeMSISDN >"+activeMSISDN,CLASS_NAME,methodName,null);
			
    		RegisteredCardsVO registeredCardVO=registerdCardsService.getCardNumber(activeMSISDN);
    		
    		executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > registeredCardVO >"+registeredCardVO,CLASS_NAME,methodName,null);
			
			if(registeredCardVO != null)
			{
				if(registeredCardVO.getErrorCode().equals("0"))
				{
					request.setAttribute(MSISDNCOUNT_PARAM, registeredCardVO.getNoOfLines());
				}
				else if(registeredCardVO.getErrorCode().equals("9001"))
				{
					request.setAttribute(ERROR_PARAM, getResourceBundle(request.getLocale()).
							getString("error.account.not.found"));
				}
				else
				{
					request.setAttribute(ERROR_PARAM, getResourceBundle(request.getLocale()).
							getString("error.unknown"));
				}
			}
			else
			{
				request.setAttribute(ERROR_PARAM, getResourceBundle(request.getLocale()).
						getString("error.unknown"));
			}
			
		} catch (Exception e) { 
			executionContext.log(Level.SEVERE,e.getMessage(),CLASS_NAME,methodName,e);
			request.setAttribute(ERROR_PARAM, getResourceBundle(request.getLocale()).
					getString("error.unknown"));
		}
    	
    	executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > debug count > "+ request.getAttribute(MSISDNCOUNT_PARAM),CLASS_NAME,methodName,null);
    	executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > debug error > "+ request.getAttribute(ERROR_PARAM),CLASS_NAME,methodName,null);
    	executionContext.log(Level.INFO,CLASS_NAME+" > showRegisteredCards > dispatching request to jsp ",CLASS_NAME,methodName,null);
		

		// Invoke the JSP to render
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(getJspFilePath(request, VIEW_JSP));
		rd.include(request,response);
	}

	/**
	 * Process an action request.
	 * 
	 * @see javax.portlet.Portlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	public void processAction(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {
	}

	/**
	 * Returns JSP file path.
	 * 
	 * @param request Render request
	 * @param jspFile JSP file name
	 * @return JSP file path
	 */
	private static String getJspFilePath(RenderRequest request, String jspFile) {
		String markup = request.getProperty("wps.markup");
		if( markup == null )
			markup = getMarkup(request.getResponseContentType());
		return JSP_FOLDER + markup + "/" + jspFile + "." + getJspExtension(markup);
	}

	/**
	 * Convert MIME type to markup name.
	 * 
	 * @param contentType MIME type
	 * @return Markup name
	 */
	private static String getMarkup(String contentType) {
		if( "text/vnd.wap.wml".equals(contentType) )
			return "wml";
        else
            return "html";
	}

	/**
	 * Returns the file extension for the JSP file
	 * 
	 * @param markupName Markup name
	 * @return JSP extension
	 */
	private static String getJspExtension(String markupName) {
		return "jsp";
	}

}
