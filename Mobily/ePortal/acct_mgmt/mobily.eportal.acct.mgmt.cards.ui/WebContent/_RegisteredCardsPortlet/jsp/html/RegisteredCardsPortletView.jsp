<%@page session="false" contentType="text/html" pageEncoding="ISO-8859-1" import="java.util.*,javax.portlet.*,sa.com.mobily.eportal.acct.mgmt.cards.portlet.*" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>        
<%@taglib uri="http://www.ibm.com/xmlns/prod/websphere/portal/v6.1/portlet-client-model" prefix="portlet-client-model" %>
<%@taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<fmt:setBundle basename="sa.com.mobily.eportal.acct.mgmt.cards.portlet.nl.RegisteredCardsPortletResource"></fmt:setBundle>        
<portlet:defineObjects/>
<portlet-client-model:init>
      <portlet-client-model:require module="ibm.portal.xml.*"/>
      <portlet-client-model:require module="ibm.portal.portlet.*"/>   
</portlet-client-model:init>         


<c:choose>
    <c:when test="${empty requestScope.msisdnCount}">
        <div style="padding-right:20px;padding-left:20px;"><c:out value="${requestScope.errorMessage}" /></div>
    </c:when>
    <c:otherwise>
        <div style="padding-right:20px;padding-left:20px;"><fmt:message key='number.of.cards.registered'/> : <c:out value="${requestScope.msisdnCount}" /></div>
    </c:otherwise>
</c:choose>