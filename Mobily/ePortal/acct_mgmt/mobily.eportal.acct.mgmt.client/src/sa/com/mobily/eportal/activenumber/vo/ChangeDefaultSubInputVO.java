package sa.com.mobily.eportal.activenumber.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ChangeDefaultSubInputVO extends BaseVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	long userAcctId;

	String selectedNumber;

	String userName;
	
	boolean isSpringLDAPRequired = false;

	public long getUserAcctId()
	{
		return userAcctId;
	}

	public void setUserAcctId(long userAcctId)
	{
		this.userAcctId = userAcctId;
	}

	public String getSelectedNumber()
	{
		return selectedNumber;
	}

	public void setSelectedNumber(String selectedNumber)
	{
		this.selectedNumber = selectedNumber;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public boolean isSpringLDAPRequired()
	{
		return isSpringLDAPRequired;
	}

	public void setSpringLDAPRequired(boolean isSpringLDAPRequired)
	{
		this.isSpringLDAPRequired = isSpringLDAPRequired;
	}

}
