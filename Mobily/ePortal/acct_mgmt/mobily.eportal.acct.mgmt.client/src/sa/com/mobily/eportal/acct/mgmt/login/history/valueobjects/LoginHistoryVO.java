/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects;

import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class LoginHistoryVO extends BaseVO {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String source = "";
	private String userName = "";
	private Date loginDateTime;
	private int loginStatus;
	private String loginType = "";
	
	
	/**
	 * @return the source
	 */
	public String getSource()
	{
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source)
	{
		this.source = source;
	}
	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	/**
	 * @return the loginDateTime
	 */
	public Date getLoginDateTime()
	{
		return loginDateTime;
	}
	/**
	 * @param loginDateTime the loginDateTime to set
	 */
	public void setLoginDateTime(Date loginDateTime)
	{
		this.loginDateTime = loginDateTime;
	}
	/**
	 * @return the loginStatus
	 */
	public int getLoginStatus()
	{
		return loginStatus;
	}
	/**
	 * @param loginStatus the loginStatus to set
	 */
	public void setLoginStatus(int loginStatus)
	{
		this.loginStatus = loginStatus;
	}
	/**
	 * @return the loginType
	 */
	public String getLoginType()
	{
		return loginType;
	}
	/**
	 * @param loginType the loginType to set
	 */
	public void setLoginType(String loginType)
	{
		this.loginType = loginType;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "LoginHistoryVO [source=" + source + ", userName=" + userName + ", loginDateTime=" + loginDateTime + ", loginStatus=" + loginStatus + ", loginType=" + loginType
				+ "]";
	}
	
}
