package sa.com.mobily.eportal.blockline.valueobjects;

public class SR_HEADER_REPLY {
	
	private String FuncId;	
	private String MsgVersion;
	private String SecurityKey;
	private String RequestorChannelId;
	private String replierChannelId;
	private long SrDate;
	private long SrRcvDate;
	private int SrStatus;
	public String getFuncId() {
		return FuncId;
	}
	public void setFuncId(String funcId) {
		FuncId = funcId;
	}
	public String getMsgVersion() {
		return MsgVersion;
	}
	public void setMsgVersion(String msgVersion) {
		MsgVersion = msgVersion;
	}
	public String getSecurityKey() {
		return SecurityKey;
	}
	public void setSecurityKey(String securityKey) {
		SecurityKey = securityKey;
	}
	public String getRequestorChannelId() {
		return RequestorChannelId;
	}
	public void setRequestorChannelId(String requestorChannelId) {
		RequestorChannelId = requestorChannelId;
	}
	public String getReplierChannelId() {
		return replierChannelId;
	}
	public void setReplierChannelId(String replierChannelId) {
		this.replierChannelId = replierChannelId;
	}
	public long getSrDate() {
		return SrDate;
	}
	public void setSrDate(long srDate) {
		SrDate = srDate;
	}
	public long getSrRcvDate() {
		return SrRcvDate;
	}
	public void setSrRcvDate(long srRcvDate) {
		SrRcvDate = srRcvDate;
	}
	public int getSrStatus() {
		return SrStatus;
	}
	public void setSrStatus(int srStatus) {
		SrStatus = srStatus;
	}
	public SR_HEADER_REPLY(String funcId, String msgVersion, String securityKey,
			String requestorChannelId, String replierChannelId, long srDate,
			long srRcvDate, int srStatus) {
		super();
		FuncId = funcId;
		MsgVersion = msgVersion;
		SecurityKey = securityKey;
		RequestorChannelId = requestorChannelId;
		this.replierChannelId = replierChannelId;
		SrDate = srDate;
		SrRcvDate = srRcvDate;
		SrStatus = srStatus;
	}
	
	/*
	<FuncId>FULL_BARRING</FuncId>
	
		<SecurityKey>kjasdhfjk48938jkdhf</SecurityKey>
       	<MsgVersion>0000</MsgVersion>
      	<RequestorChannelId>Siebel</RequestorChannelId>
       	<ReplierChannelId>BSL</ReplierChannelId>
       	<SrDate>20051010142255</SrDate>
       	<SrRcvDate>20051010142255</SrRcvDate>
       	<SrStatus>0</SrStatus>
	*/
	
}
