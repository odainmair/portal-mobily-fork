/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class PackageHistoryVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String msisdn = "";
	private String sourcePkgName = "";
	private String destinationPkgName = "";
	private String packageConvCount = "";
	private String errorCode = "";
	private String errorMsg = "";
	
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getSourcePkgName()
	{
		return sourcePkgName;
	}
	public void setSourcePkgName(String sourcePkgName)
	{
		this.sourcePkgName = sourcePkgName;
	}
	public String getDestinationPkgName()
	{
		return destinationPkgName;
	}
	public void setDestinationPkgName(String destinationPkgName)
	{
		this.destinationPkgName = destinationPkgName;
	}
	public String getPackageConvCount()
	{
		return packageConvCount;
	}
	public void setPackageConvCount(String packageConvCount)
	{
		this.packageConvCount = packageConvCount;
	}
	public String getErrorCode()
	{
		return errorCode;
	}
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}
	public String getErrorMsg()
	{
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg)
	{
		this.errorMsg = errorMsg;
	}
	
	
}
