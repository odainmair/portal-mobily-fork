/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.login.history.service.view;

import java.util.List;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryRequestVO;
import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryVO;

/**
 * @author n.gundluru.mit
 *
 */
public interface LoginHistoryServiceLocal
{
	public List<LoginHistoryVO> findLoginHistoryByUserName(@NotNull LoginHistoryRequestVO requestVoObj);
}
