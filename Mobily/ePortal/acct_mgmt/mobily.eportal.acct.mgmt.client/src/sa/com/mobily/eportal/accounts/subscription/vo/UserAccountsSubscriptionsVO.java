package sa.com.mobily.eportal.accounts.subscription.vo;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserAccountsSubscriptionsVO extends BaseVO
{
	private static final long serialVersionUID = 1L;
	private long subscriptionId;
	private long userAccountId;
	private String isDefault;
	private int subscriptionType;
	private String msidsn;
	private String serviceAccountNumber;
	private String customerType;
	private String packageId;
	private Timestamp subscriptionCreatedDate;
	private String isActive;
	private long ccStatus;
	private Timestamp lastUpdatedTime;
	private String subscriptionName;
	private int lineType;
	private String subscriptionIqama;
	
	
	private String roleId;
	private String userName;
	private String email;
	public long getSubscriptionId()
	{
		return subscriptionId;
	}
	public void setSubscriptionId(long subscriptionId)
	{
		this.subscriptionId = subscriptionId;
	}
	public long getUserAccountId()
	{
		return userAccountId;
	}
	public void setUserAccountId(long userAccountId)
	{
		this.userAccountId = userAccountId;
	}
	public String getIsDefault()
	{
		return isDefault;
	}
	public void setIsDefault(String isDefault)
	{
		this.isDefault = isDefault;
	}
	public int getSubscriptionType()
	{
		return subscriptionType;
	}
	public void setSubscriptionType(int subscriptionType)
	{
		this.subscriptionType = subscriptionType;
	}
	public String getMsidsn()
	{
		return msidsn;
	}
	public void setMsidsn(String msidsn)
	{
		this.msidsn = msidsn;
	}
	public String getServiceAccountNumber()
	{
		return serviceAccountNumber;
	}
	public void setServiceAccountNumber(String serviceAccountNumber)
	{
		this.serviceAccountNumber = serviceAccountNumber;
	}
	public String getCustomerType()
	{
		return customerType;
	}
	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}
	public String getPackageId()
	{
		return packageId;
	}
	public void setPackageId(String packageId)
	{
		this.packageId = packageId;
	}
	public Timestamp getSubscriptionCreatedDate()
	{
		return subscriptionCreatedDate;
	}
	public void setSubscriptionCreatedDate(Timestamp subscriptionCreatedDate)
	{
		this.subscriptionCreatedDate = subscriptionCreatedDate;
	}
	public String getIsActive()
	{
		return isActive;
	}
	public void setIsActive(String isActive)
	{
		this.isActive = isActive;
	}
	public long getCcStatus()
	{
		return ccStatus;
	}
	public void setCcStatus(long ccStatus)
	{
		this.ccStatus = ccStatus;
	}
	public Timestamp getLastUpdatedTime()
	{
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(Timestamp lastUpdatedTime)
	{
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getSubscriptionName()
	{
		return subscriptionName;
	}
	public void setSubscriptionName(String subscriptionName)
	{
		this.subscriptionName = subscriptionName;
	}
	public int getLineType()
	{
		return lineType;
	}
	public void setLineType(int lineType)
	{
		this.lineType = lineType;
	}
	public String getSubscriptionIqama()
	{
		return subscriptionIqama;
	}
	public void setSubscriptionIqama(String subscriptionIqama)
	{
		this.subscriptionIqama = subscriptionIqama;
	}
	public String getRoleId()
	{
		return roleId;
	}
	public void setRoleId(String roleId)
	{
		this.roleId = roleId;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	
	

}
