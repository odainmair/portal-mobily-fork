package sa.com.mobily.eportal.mycontact.xls.utility;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import static sa.com.mobily.eportal.mycontact.xls.utility.MobilyResourceBundle.XLS;
import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.SHEET_NAME;

/**
 * This class is responsible for creating the sheet that contains the user
 * contacts
 * 
 * @author Muhammad Bhutto
 */

public class CreateWorkBook
{
	private HSSFWorkbook workbook;

	private HSSFSheet worksheet;

	private XLSLayouter layouter;

	private static final String path = "resources/";

	private static ResourceBundle resourceBundle = null;

	public CreateWorkBook(Locale locale)
	{
		super();
		resourceBundle = ResourceBundle.getBundle(path + XLS.getBundleName(), locale);
	}

	/**
	 * This method creates the sheet of my contacts excel file
	 * 
	 * @return <code>HSSFWorkbook</code>
	 * @author Yousef Alkhalaileh
	 */
	public HSSFWorkbook createWorkBook()
	{
		// 1. Create new workbook
		workbook = new HSSFWorkbook();
		// 2. Create new worksheet
		worksheet = workbook.createSheet(resourceBundle.getString(SHEET_NAME.toString()));
		// 3. Build layout
		// Build title, date, and column headers
		layouter = new XLSLayouter();
		layouter.buildReport(worksheet, resourceBundle);
		return workbook;
	}

	public HSSFWorkbook getWorkbook()
	{
		return workbook;
	}

	public HSSFSheet getWorksheet()
	{
		return worksheet;
	}
}