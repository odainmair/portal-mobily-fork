package sa.com.mobily.eportal.acct.mgmt.cards.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class CardsAppConfig {
    
   
    
    /**
     *  Property file name
     */
    protected static final String propertyFileName="config/cards_config.properties";
    
    /**
     * Instance of AppConfig
     */
    protected static  CardsAppConfig config = null;
    
    /**
     * Contains all properties in a Properties instance
     */
    protected Properties properties;
    
    
    /**
	 * @MethodName: CardsAppConfig
	 * <p>
	 * Creates an AppConfig instance.
	 * 
	 * 
	 * @author Abu Sharaf
	 */
    protected CardsAppConfig() {
        properties = new Properties();
        ClassLoader sc = Thread.currentThread().getContextClassLoader();
        InputStream stream = sc.getResourceAsStream(propertyFileName);
        
        if (stream == null) {
            throw new IllegalArgumentException(
                    "Could not load "+propertyFileName+".Please make sure that it is in CLASSPATH.");
        }
        
        try {
            properties.load(stream);
        } catch (IOException e) {
            IllegalStateException ex = new IllegalStateException(
            "An error occurred when reading from the input stream");
            ex.initCause(e);
            throw ex;
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                IllegalStateException ex = new IllegalStateException(
                "An I/O error occured while closing the stream");
                ex.initCause(e);
                throw ex;
            }
        }
    }
    
    /**
	 * @MethodName: getInstance
	 * <p>
	 *  Gets the instance of AppConfig.
	 * 
	 * @return instance of AppConfig
	 * 
	 * @author Abu Sharaf
	 */
    public static CardsAppConfig getInstance() {
        if (config==null)
            config=new CardsAppConfig();
        return config;
    }
    
    /**
     * Gets the value of a property based on key provided.
     * 
     * @param key
     *            The key for which value needs to be retrieved from
     * @return The value for the key in <code>app.properties</code>. Returns
     *         null if the value does not exist in the property file.
     *  
     */
    public String get(String key) {
        if (properties == null || key == null)
            return null;
        return (String) properties.get(key);
    }
}