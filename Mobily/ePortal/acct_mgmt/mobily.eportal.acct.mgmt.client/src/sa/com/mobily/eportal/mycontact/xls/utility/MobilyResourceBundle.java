package sa.com.mobily.eportal.mycontact.xls.utility;


/**
 * This class holds the bundle for my contacts service
 * @author Muhammad Bhutto
 */
public enum MobilyResourceBundle {

	// add another resource bundle name here
	XLS("xls");
	
	
	private String bundleName;  

    MobilyResourceBundle(String bundleName) {
        this.bundleName = bundleName;
    }

    public String getBundleName() {
        return bundleName;
    }
    
  
    @Override
    public String toString() {
        return bundleName;
    }
}