package sa.com.mobily.eportal.mycontact.service.view;

import java.util.List;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.common.service.model.UserAccount;
import sa.com.mobily.eportal.common.service.vo.LongVO;
import sa.com.mobily.eportal.common.service.vo.StringVO;
import sa.com.mobily.eportal.mycontact.vo.MyContactsRequestVO;
import sa.com.mobily.eportal.mycontact.vo.MyContactsVO;

/**
 * This interface is responsible about managing the contacts of the user
 * <p> 
 * it contains methods for retrieve, add, delete and update.
 *  
 * @author Yousef Alkhalaileh
 */
public interface MyContactsServiceBeanLocal {
	
	public List<MyContactsVO> findByAccountId(@NotNull LongVO accountId);

	public void deleteContactByIds(@NotNull MyContactsRequestVO request);
	
	public void updateContact(@NotNull MyContactsRequestVO request);
	
	public void addContact(@NotNull MyContactsRequestVO request);
	
	public UserAccount findAccountByUserName(@NotNull StringVO userName);
	
	public List<MyContactsVO> findByPhoneNumber(@NotNull String phoneNumber, @NotNull LongVO accountId);
	
	public void addContactForAddSubscription(@NotNull MyContactsRequestVO request);
}
