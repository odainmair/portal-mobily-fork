/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class PackageConversionRepVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String isAcknowledgement = "";
	private String statusCode = "";
	
	
	public String getIsAcknowledgement()
	{
		return isAcknowledgement;
	}
	public void setIsAcknowledgement(String isAcknowledgement)
	{
		this.isAcknowledgement = isAcknowledgement;
	}
	public String getStatusCode()
	{
		return statusCode;
	}
	public void setStatusCode(String statusCode)
	{
		this.statusCode = statusCode;
	}
	
	

	
}
