package sa.com.mobily.eportal.authority.mgmt.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class SubAdminVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String billingAccountNumber;
	
	private String name;
	
	private String msisdn;
	
	private String userName;
	
	private String userAcctId;
	
	private String password;

	private String preferredLanugage;
	
	
	private String companyName;
	
	
	
	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public String getPreferredLanugage()
	{
		return preferredLanugage;
	}

	public void setPreferredLanugage(String preferredLanugage)
	{
		this.preferredLanugage = preferredLanugage;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getBillingAccountNumber()
	{
		return billingAccountNumber;
	}

	public void setBillingAccountNumber(String billingAccountNumber)
	{
		this.billingAccountNumber = billingAccountNumber;
	}



	public String getUserAcctId()
	{
		return userAcctId;
	}

	public void setUserAcctId(String userAcctId)
	{
		this.userAcctId = userAcctId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	

}
