package sa.com.mobily.eportal.mycontact.xls.utility;

import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.CELL_HEADING_TITILE;
import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.EMAIL_COL;
import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.FIRST_NAME_COL;
import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.ID_COL;
import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.LAST_NAME_COL;
import static sa.com.mobily.eportal.mycontact.xls.utility.XLSPropertyData.PHONE_COL;

import java.util.Date;
import java.util.ResourceBundle;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

/**
 * This class builds the report layout, the template, the design, the pattern
 * 
 * @author Muhammad Bhutto
 */
@SuppressWarnings("deprecation")
final class XLSLayouter
{
	private ResourceBundle resourceBundle;

	/**
	 * Builds the report layout, This doesn't have any data yet.
	 * 
	 * @param <code>HSSFSheet</code>
	 * @param <code>ResourceBundle</code>
	 * @author Muhammad Bhutto
	 */
	public void buildReport(final HSSFSheet worksheet, ResourceBundle resourceBundle)
	{
		this.resourceBundle = resourceBundle;
		// Define starting indices for rows and columns
		final int startRowIndex = 0;
		final int startColIndex = 0;
		// Set column widths
		setColumnWidth(worksheet);
		// Build the title and date headers
		buildTitle(worksheet, startRowIndex, startColIndex);
		// Build the column headers
		buildHeaders(worksheet, startRowIndex, startColIndex);
	}

	private void setColumnWidth(final HSSFSheet worksheet)
	{
		worksheet.setColumnWidth(0, 5000);
		worksheet.setColumnWidth(1, 5000);
		worksheet.setColumnWidth(2, 5000);
		worksheet.setColumnWidth(3, 5000);
		worksheet.setColumnWidth(4, 5000);
		// worksheet.setColumnWidth(5, 5000);
	}

	/**
	 * Builds the report title and the date header
	 * 
	 * @param <code>HSSFSheet</code> worksheet
	 * @param <code>int</code> startRowIndex, starting row offset
	 * @param <code>int</code> startColIndex, starting column offset
	 * @author Muhammad Bhutto
	 */
	private void buildTitle(final HSSFSheet worksheet, final int startRowIndex, final int startColIndex)
	{
		// Create font style for the report title
		final Font fontTitle = worksheet.getWorkbook().createFont();
		fontTitle.setBoldweight(Font.BOLDWEIGHT_BOLD);
		fontTitle.setFontHeight((short) 280);

		// Create cell style for the report title
		final HSSFCellStyle cellStyleTitle = worksheet.getWorkbook().createCellStyle();
		cellStyleTitle.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyleTitle.setWrapText(true);
		cellStyleTitle.setFont(fontTitle);

		// Create report title
		final HSSFRow rowTitle = worksheet.createRow((short) startRowIndex);
		rowTitle.setHeight((short) 500);
		HSSFCell cellTitle = rowTitle.createCell(startColIndex);
		cellTitle.setCellValue(resourceBundle.getString(CELL_HEADING_TITILE.toString()));
		cellTitle.setCellStyle(cellStyleTitle);

		// Create merged region for the report title
		worksheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 4));

		// Create date header
		final HSSFRow dateTitle = worksheet.createRow((short) startRowIndex + 1);
		HSSFCell cellDate = dateTitle.createCell(startColIndex);
		cellDate.setCellValue(resourceBundle.getString("excell.time") + new Date());
	}

	/**
	 * Builds the column headers
	 * 
	 * @param <code>HSSFSheet</code> worksheet
	 * @param <code>int</code> startRowIndex, starting row offset
	 * @param <code>int</code> startColIndex, starting column offset
	 * @author Muhammad Bhutto
	 */
	private void buildHeaders(final HSSFSheet worksheet, final int startRowIndex, final int startColIndex)
	{
		// Create font style for the headers
		final Font font = worksheet.getWorkbook().createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		// Create cell style for the headers
		final HSSFCellStyle headerCellStyle = worksheet.getWorkbook().createCellStyle();
		headerCellStyle.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
		headerCellStyle.setFillPattern(CellStyle.FINE_DOTS);
		headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		headerCellStyle.setWrapText(true);
		headerCellStyle.setFont(font);
		headerCellStyle.setBorderBottom(CellStyle.BORDER_THIN);

		// Create the column headers
		final HSSFRow rowHeader = worksheet.createRow((short) startRowIndex + 2);
		rowHeader.setHeight((short) 500);

		if (StringUtils.containsIgnoreCase(resourceBundle.getLocale().getLanguage(), "e"))
		{
			final HSSFCell cell0 = rowHeader.createCell(startColIndex);
			cell0.setCellValue(resourceBundle.getString(ID_COL.toString()));
			cell0.setCellStyle(headerCellStyle);

			final HSSFCell cell1 = rowHeader.createCell(startColIndex + 1);
			cell1.setCellValue(resourceBundle.getString(FIRST_NAME_COL.toString()));
			cell1.setCellStyle(headerCellStyle);

			final HSSFCell cell2 = rowHeader.createCell(startColIndex + 2);
			cell2.setCellValue(resourceBundle.getString(LAST_NAME_COL.toString()));
			cell2.setCellStyle(headerCellStyle);

			final HSSFCell cell3 = rowHeader.createCell(startColIndex + 3);
			cell3.setCellValue(resourceBundle.getString(PHONE_COL.toString()));
			cell3.setCellStyle(headerCellStyle);

			final HSSFCell cell4 = rowHeader.createCell(startColIndex + 4);
			cell4.setCellValue(resourceBundle.getString(EMAIL_COL.toString()));
			cell4.setCellStyle(headerCellStyle);
		}
		else
		{
			final HSSFCell cell4 = rowHeader.createCell(startColIndex);
			cell4.setCellValue(resourceBundle.getString(EMAIL_COL.toString()));
			cell4.setCellStyle(headerCellStyle);
			
			final HSSFCell cell3 = rowHeader.createCell(startColIndex + 1);
			cell3.setCellValue(resourceBundle.getString(PHONE_COL.toString()));
			cell3.setCellStyle(headerCellStyle);
			
			final HSSFCell cell2 = rowHeader.createCell(startColIndex + 2);
			cell2.setCellValue(resourceBundle.getString(LAST_NAME_COL.toString()));
			cell2.setCellStyle(headerCellStyle);
			
			final HSSFCell cell1 = rowHeader.createCell(startColIndex + 3);
			cell1.setCellValue(resourceBundle.getString(FIRST_NAME_COL.toString()));
			cell1.setCellStyle(headerCellStyle);
			
			final HSSFCell cell0 = rowHeader.createCell(startColIndex + 4);
			cell0.setCellValue(resourceBundle.getString(ID_COL.toString()));
			cell0.setCellStyle(headerCellStyle);
		}
	}
}