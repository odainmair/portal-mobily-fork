package sa.com.mobily.eportal.mycontact.xls.utility;


/**
 * This class contains the contacts service constants
 * @author ABDUL MAJID
 */
public final class MyContactConstants {

	private MyContactConstants() throws AssertionError {
		super();
	}

	public static final String VIEW_CONTACT_JSP =  "/_MyContactPortlet/jsp/html/contactList.jsp";
//	public static final String VIEW_CONTACT_JSP =  "/_MyContactPortlet/jsp/html/myContactList.jsp";
	public static final String ADD_CONTACT_JSP =  "/_MyContactPortlet/jsp/html/addContact.jsp";
	public static final String DETAIL_CONTACT_JSP = "/_MyContactPortlet/jsp/html/contactDetail.jsp";
	public static final String ERROR_JSP = "/_MyContactPortlet/jsp/html/errorPage.jsp";
	
	// MY CONTACT MESSAGES
	public static final String SUCCESS_UPDATE_MESSAGE = "updateMessage";
	public static final String SUCCESS_DELETE_MESSAGE = "deleteSuccessMsg";
	public static final String SUCCESS_SAVE_MESSAGE = "saveSuccessMsg";
	
	public static final String FOR_MESSAGE_SAVE =  "save";
	public static final String FOR_MESSAGE_UPDATE = "update";
	public static final String FOR_MESSAGE_DELETE_FIRST_PART = "deleteFirstPart";
	public static final String FOR_MESSAGE_DELETE_SECOND_PART = "deleteSecondPart";
	// MY CONTACT MESSAGES
	
	// ACTION ANNOTATIONS START
	public static final String ACTION_SAVE_OR_UPDATE = "saveAction";
	public static final String RENDER_MODE_VIEW = "view";
	public static final String ACTION_DELETE = "deleteAction";
	public static final String ACTION_DETAIL = "detailAction";
	
	public final static String  JAVAX_PORTLET_ACTION = "javax.portlet.action";
	// ACTION ANNOTATIONS END
	
	
	
	public final static String CONTACTS_ALPHABATES = "alphabates";
	public final static String CONTACTS_CONTACTS_LIST = "contacts";
	public final static String SESSION_USER_ACCT_ID = "userAcctID";
	public final static String SESSION_CONTACT_DETAIL = "sessionContactDetail";
	public final static String CONTACT_DETAIL = "contactDetail";
	public final static String CONTACTS_LSIT_SIZE =   "contactListSize";
	public final static String CONTACTS_FIRSTNAME = "contactFirstName";
	public final static String CONTACTS_LASTNAME = "contactLastName";
	public final static String CONTACTS_MOBILE = "contactMobileNumber";
	
	public final static String CONTACTS_EMAIL = "contactEmail";
	public final static String CONTACTS_DETAIL_ID = "contactDetailId";
	public final static String RESOURCE_BUNDLE_NAME = "sa.com.mobily.eportal.mycontact.portlets.nl.MyContactResource"; 
    
	public final static String ERROR_TASK = "errorPage"; 
	
}
