package sa.com.mobily.eportal.blockline.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CheckStatusRequestVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8603053214945097349L;
	private String userId;
	private String msisdn;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
}
