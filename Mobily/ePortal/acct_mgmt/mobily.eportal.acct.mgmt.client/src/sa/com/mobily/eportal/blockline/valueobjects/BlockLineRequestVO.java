package sa.com.mobily.eportal.blockline.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class BlockLineRequestVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4588718873882835490L;
	
	private String msisdn;
	private String username;
	private String customerType;
	private String packageId;
	private String chargeDeviceInstallment; // values: Y / N
	private String packageDataType;
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getCustomerType()
	{
		return customerType;
	}
	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}
	public String getPackageId()
	{
		return packageId;
	}
	public void setPackageId(String packageId)
	{
		this.packageId = packageId;
	}
	public String getChargeDeviceInstallment()
	{
		return chargeDeviceInstallment;
	}
	public void setChargeDeviceInstallment(String chargeDeviceInstallment)
	{
		this.chargeDeviceInstallment = chargeDeviceInstallment;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public String getPackageDataType()
	{
		return packageDataType;
	}
	public void setPackageDataType(String packageDataType)
	{
		this.packageDataType = packageDataType;
	}
	
	
	/*
	<MOBILY_BSL_SR>
	     <SR_HEADER>
	           <FuncId>FULL_BARRING</FuncId>
	           <SecurityKey>kjasdhfjk48938jkdhf</SecurityKey>
	           <MsgVersion>0000</MsgVersion>
	           <RequestorChannelId>Siebel</RequestorChannelId>
	           <SrDate>20051010142255</SrDate>
	           <RequestorUserId>s.ali</RequestorUserId>
	           <RequestorLanguage>E</RequestorLanguage>
	           <Chargeable>N</Chargeable>
	           <ChargingMode>Payment</ChargingMode>
	           <ChargeAmount>0.0</ChargeAmount>
	     </SR_HEADER>
	     <ChannelTransID>SRID</ChannelTransID>unique number
	     <UserComment>the comment</UserComment>
	     <LineNumber>966565603975</LineNumber>
	     <NotifyBE>0|1</NotifyBE>   1
	     <ReasonId></ReasonId>  value = [3]
	     <FINAL_REPLY_QUEUE></FINAL_REPLY_QUEUE>
	     <FINAL_REPLY_QUEUE_MANAGER></FINAL_REPLY_QUEUE_MANAGER>
	     <PARAMETER>
	           <NAME>MSISDN</NAME>
	           <VALUE>966565603975</VALUE>
	     </PARAMETER>
	     <PARAMETER>
	           <NAME>SIM_ID</NAME>
	           <VALUE>2</VALUE>
	     </PARAMETER>
	</MOBILY_BSL_SR>

	
    private SR_HEADER_VO header;
    private String ChannelTransID;
    private String UserComment;
    private long LineNumber;
    private String NotifyBE;
    private String ReasonId;
    private String FINAL_REPLY_QUEUE;
    private String FINAL_REPLY_QUEUE_MANAGER;
    private List<PARAMETER_VO> Parameter ;
	public BlockLineRequestVO(SR_HEADER_VO header, String channelTransID,
			String userComment, long lineNumber, String notifyBE,
			String reasonId, String fINAL_REPLY_QUEUE,
			String fINAL_REPLY_QUEUE_MANAGER, List<PARAMETER_VO> parameter) {
		super();
		this.header = header;
		ChannelTransID = channelTransID;
		UserComment = userComment;
		LineNumber = lineNumber;
		NotifyBE = notifyBE;
		ReasonId = reasonId;
		FINAL_REPLY_QUEUE = fINAL_REPLY_QUEUE;
		FINAL_REPLY_QUEUE_MANAGER = fINAL_REPLY_QUEUE_MANAGER;
		Parameter = parameter;
	}
	public SR_HEADER_VO getHeader() {
		return header;
	}
	public void setHeader(SR_HEADER_VO header) {
		this.header = header;
	}
	public String getChannelTransID() {
		return ChannelTransID;
	}
	public void setChannelTransID(String channelTransID) {
		ChannelTransID = channelTransID;
	}
	public String getUserComment() {
		return UserComment;
	}
	public void setUserComment(String userComment) {
		UserComment = userComment;
	}
	public long getLineNumber() {
		return LineNumber;
	}
	public void setLineNumber(long lineNumber) {
		LineNumber = lineNumber;
	}
	public String getNotifyBE() {
		return NotifyBE;
	}
	public void setNotifyBE(String notifyBE) {
		NotifyBE = notifyBE;
	}
	public String getReasonId() {
		return ReasonId;
	}
	public void setReasonId(String reasonId) {
		ReasonId = reasonId;
	}
	public String getFINAL_REPLY_QUEUE() {
		return FINAL_REPLY_QUEUE;
	}
	public void setFINAL_REPLY_QUEUE(String fINAL_REPLY_QUEUE) {
		FINAL_REPLY_QUEUE = fINAL_REPLY_QUEUE;
	}
	public String getFINAL_REPLY_QUEUE_MANAGER() {
		return FINAL_REPLY_QUEUE_MANAGER;
	}
	public void setFINAL_REPLY_QUEUE_MANAGER(String fINAL_REPLY_QUEUE_MANAGER) {
		FINAL_REPLY_QUEUE_MANAGER = fINAL_REPLY_QUEUE_MANAGER;
	}
	public List<PARAMETER_VO> getParameter() {
		return Parameter;
	}
	public void setParameter(List<PARAMETER_VO> parameter) {
		Parameter = parameter;
	}
   

	public BlockLineRequestVO() {
	}
	public BlockLineRequestVO getRequestObject(String msisdn,String language, String userId ) {
		
		BlockLineUtility blockLineUtility = new BlockLineUtility();
		return  blockLineUtility.createBlockRequest(userId, msisdn, language);
		//SR_HEADER_VO header = new SR_HEADER_VO(funcId, securityKey, msgVersion, requestorChannelId, srDate, requestorUserId, requestorLanguage, chargeable, chargingMode, chargeAmount)
		
	}
	
	*/	
}
