package sa.com.mobily.eportal.mycontact.xls.utility;

/**
 * This Enum contains the column names of the contacts excel sheet
 * @author Muhammad Bhutto
 */
enum XLSPropertyData {

	ID_COL("id"), 
	FIRST_NAME_COL("first.name"), 
	LAST_NAME_COL("last.name"), 
	PHONE_COL("phone"), 
	EMAIL_COL("email"), 
	SHEET_NAME("sheet.name"), 
	CELL_HEADING_TITILE("heading.title"),
	FILE_NAME("file.name");

	private String name;

	private XLSPropertyData(final String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
