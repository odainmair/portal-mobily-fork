package sa.com.mobily.eportal.mycontact.xls.utility;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.mycontact.vo.MyContactsVO;

/**
 * This class is responsible for creating the excel sheet for downloading the
 * user contacts
 * 
 * @author Muhammad Bhutto
 */

public class XLSDownloadService
{

	private XLSFileManager fileManager;

	private static String clazz = XLSDownloadService.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.MY_CONTACTS_SERVICE_LOGGER_NAME);

	public XLSDownloadService()
	{
		super();
		this.fileManager = new XLSFileManager();
	}
	
	/**
	 * @MethodName: downloadXLS
	 * <p>
	 * Flush of stream of bytes to the user screen containing the Excel sheet for their contacts list
	 * 
	 * @param OutputStream outputStream
	 * @param List<MyContactsVO> contacts
	 * @param Locale locae
	 * 
	 * 
	 * @author Abu Sharaf
	 */
	public void downloadXLS(OutputStream outputStream, List<MyContactsVO> contacts , Locale locale)
	{
		try
		{
			CreateWorkBook workBook = new CreateWorkBook(locale);
			workBook.createWorkBook();
			fileManager.fillReport(workBook.getWorksheet(), contacts, locale);

			workBook.getWorksheet().getWorkbook().write(outputStream);
			outputStream.flush();
			outputStream.close();
		}
		catch (IOException e)
		{
			executionContext.log(Level.SEVERE, "XLSDownloadService > downloadXLS : error message: " + e.getMessage(), clazz, "downloadXLS", e);
			throw new RuntimeException(e);
		}
	}
}