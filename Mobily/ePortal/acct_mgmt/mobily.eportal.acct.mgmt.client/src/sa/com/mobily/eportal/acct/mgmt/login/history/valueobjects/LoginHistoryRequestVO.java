/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class LoginHistoryRequestVO extends BaseVO 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String userName = "";

	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	
	
}
