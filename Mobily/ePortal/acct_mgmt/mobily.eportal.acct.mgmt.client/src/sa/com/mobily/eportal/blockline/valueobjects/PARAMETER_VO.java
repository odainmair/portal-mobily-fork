package sa.com.mobily.eportal.blockline.valueobjects;

public class PARAMETER_VO {
	 private String NAME;
     private String VALUE;
	public PARAMETER_VO(String nAME, String vALUE) {
		super();
		NAME = nAME;
		VALUE = vALUE;
	}
	public String getNAME() {
		return NAME;
	}
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getVALUE() {
		return VALUE;
	}
	public void setVALUE(String vALUE) {
		VALUE = vALUE;
	}
     
}
