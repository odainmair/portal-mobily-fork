package sa.com.mobily.eportal.mycontact.xls.utility;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellStyle;

import sa.com.mobily.eportal.mycontact.vo.MyContactsVO;

/**
 * This class is responsible for filling the excel sheet with the contacts
 * information
 * 
 * @author Muhammad Bhutto
 */
public class XLSFileManager
{

	public XLSFileManager()
	{
		super();
	}

	/**
	 * Fills the report with content
	 * 
	 * @param <code>HSSFSheet</code> worksheet
	 * @param <code>List<MyContactsVO></code> contacts information
	 * @author Muhammad Bhutto
	 */
	public void fillReport(final HSSFSheet worksheet, final List<MyContactsVO> contacts , Locale locale)
	{
		//
		int startRowIndex = 0;
		int startColIndex = 0;
		// Row offset
		startRowIndex += 2;

		// Create cell style for the body
		final HSSFCellStyle bodyCellStyle = worksheet.getWorkbook().createCellStyle();
		bodyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		bodyCellStyle.setWrapText(true);

		// Create body
		int counter = 1;
		for (int i = startRowIndex; i + startRowIndex - 2 < contacts.size() + 2; i++)
		{
			// Create a new row
			final HSSFRow row = worksheet.createRow((short) i + 1);

			if (StringUtils.containsIgnoreCase(locale.getLanguage(), "e"))
			{
				HSSFCell cell0 = row.createCell(startColIndex);
				cell0.setCellValue(counter++);

				// Retrieve the first name value
				HSSFCell cell1 = row.createCell(startColIndex + 1);
				cell1.setCellValue(contacts.get(i - 2).getFirstName());

				cell1.setCellStyle(bodyCellStyle);

				// Retrieve the last name value
				HSSFCell cell2 = row.createCell(startColIndex + 2);
				cell2.setCellValue(contacts.get(i - 2).getLastName());
				cell2.setCellStyle(bodyCellStyle);

				// Retrieve the phone power value
				HSSFCell cell3 = row.createCell(startColIndex + 3);
				cell3.setCellValue(contacts.get(i - 2).getPhone());
				cell3.setCellStyle(bodyCellStyle);

				// Retrieve the email value
				HSSFCell cell4 = row.createCell(startColIndex + 4);
				cell4.setCellValue(contacts.get(i - 2).getEmail());
				cell4.setCellStyle(bodyCellStyle);
			}
			else
			{
				HSSFCell cell0 = row.createCell(startColIndex + 4);
				cell0.setCellValue(counter++);

				// Retrieve the first name value
				HSSFCell cell1 = row.createCell(startColIndex + 3);
				cell1.setCellValue(contacts.get(i - 2).getFirstName());

				cell1.setCellStyle(bodyCellStyle);

				// Retrieve the last name value
				HSSFCell cell2 = row.createCell(startColIndex + 2);
				cell2.setCellValue(contacts.get(i - 2).getLastName());
				cell2.setCellStyle(bodyCellStyle);

				// Retrieve the phone power value
				HSSFCell cell3 = row.createCell(startColIndex + 1);
				cell3.setCellValue(contacts.get(i - 2).getPhone());
				cell3.setCellStyle(bodyCellStyle);

				// Retrieve the email value
				HSSFCell cell4 = row.createCell(startColIndex);
				cell4.setCellValue(contacts.get(i - 2).getEmail());
				cell4.setCellStyle(bodyCellStyle);
			}
		}
	}
}