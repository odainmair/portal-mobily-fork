package sa.com.mobily.eportal.blockline.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CheckStatusResponseVO extends BaseVO {
	private static final long serialVersionUID = -8443745303450026578L;
	
	private boolean blocked;

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
}