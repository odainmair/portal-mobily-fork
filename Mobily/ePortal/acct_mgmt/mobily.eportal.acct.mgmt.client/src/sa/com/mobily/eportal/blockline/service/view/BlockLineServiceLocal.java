package sa.com.mobily.eportal.blockline.service.view;

import sa.com.mobily.eportal.blockline.valueobjects.BlockLineRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineResponseVO;
import sa.com.mobily.eportal.blockline.valueobjects.CheckStatusRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.CheckStatusResponseVO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;

public interface BlockLineServiceLocal {
	public BlockLineResponseVO blockLine(BlockLineRequestVO blockLineRequestBO) throws MobilyCommonException, Exception;
	public CheckStatusResponseVO checkStatus(CheckStatusRequestVO checkStatusRequestBO) throws MobilyCommonException, Exception;
}
