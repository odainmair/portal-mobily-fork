package sa.com.mobily.eportal.blockline.valueobjects;


public class SR_HEADER_VO {
	
	/*
	 * 
	 *  <SR_HEADER>
	           <FuncId>FULL_BARRING</FuncId>
	           <SecurityKey>kjasdhfjk48938jkdhf</SecurityKey>
	           <MsgVersion>0000</MsgVersion>
	           <RequestorChannelId>Siebel</RequestorChannelId>
	           <SrDate>20051010142255</SrDate>
	           <RequestorUserId>s.ali</RequestorUserId>
	           <RequestorLanguage>E</RequestorLanguage>
	           <Chargeable>N</Chargeable>
	           <ChargingMode>Payment</ChargingMode>
	           <ChargeAmount>0.0</ChargeAmount>
	     </SR_HEADER>
	 * 
	 */
	
	private String FuncId;
    private String SecurityKey;
    private String MsgVersion;
    private String RequestorChannelId;
    private long SrDate;
    private String RequestorUserId;
    private String RequestorLanguage;
    private String Chargeable;
    private String ChargingMode;
    private String ChargeAmount;
    
	public SR_HEADER_VO(String funcId, String securityKey, String msgVersion,
			String requestorChannelId, long srDate, String requestorUserId,
			String requestorLanguage, String chargeable, String chargingMode,
			String chargeAmount) {
		super();
		FuncId = funcId;
		SecurityKey = securityKey;
		MsgVersion = msgVersion;
		RequestorChannelId = requestorChannelId;
		SrDate = srDate;
		RequestorUserId = requestorUserId;
		RequestorLanguage = requestorLanguage;
		Chargeable = chargeable;
		ChargingMode = chargingMode;
		ChargeAmount = chargeAmount;
	}

	public String getFuncId() {
		return FuncId;
	}

	public void setFuncId(String funcId) {
		FuncId = funcId;
	}

	public String getSecurityKey() {
		return SecurityKey;
	}

	public void setSecurityKey(String securityKey) {
		SecurityKey = securityKey;
	}

	public String getMsgVersion() {
		return MsgVersion;
	}

	public void setMsgVersion(String msgVersion) {
		MsgVersion = msgVersion;
	}

	public String getRequestorChannelId() {
		return RequestorChannelId;
	}

	public void setRequestorChannelId(String requestorChannelId) {
		RequestorChannelId = requestorChannelId;
	}

	public long getSrDate() {
		return SrDate;
	}

	public void setSrDate(long srDate) {
		SrDate = srDate;
	}

	public String getRequestorUserId() {
		return RequestorUserId;
	}

	public void setRequestorUserId(String requestorUserId) {
		RequestorUserId = requestorUserId;
	}

	public String getRequestorLanguage() {
		return RequestorLanguage;
	}

	public void setRequestorLanguage(String requestorLanguage) {
		RequestorLanguage = requestorLanguage;
	}

	public String getChargeable() {
		return Chargeable;
	}

	public void setChargeable(String chargeable) {
		Chargeable = chargeable;
	}

	public String getChargingMode() {
		return ChargingMode;
	}

	public void setChargingMode(String chargingMode) {
		ChargingMode = chargingMode;
	}

	public String getChargeAmount() {
		return ChargeAmount;
	}

	public void setChargeAmount(String chargeAmount) {
		ChargeAmount = chargeAmount;
	}
    
}
