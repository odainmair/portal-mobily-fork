package sa.com.mobily.eportal.mycontact.vo;

import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * The persistent class for the CONTACTS database table.
 * 
 * @author Yousef Alkhalaileh
 */
public class MyContactsVO extends BaseVO {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	private Long userAccountId;

	private String email;
	
	private String firstName;

	private String lastName;

	private String phone;
	
	private Date creationDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserAccountId() {
		return userAccountId;
	}

	public void setUserAccountId(Long userAccountId) {
		this.userAccountId = userAccountId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public String toString()
	{
		return "MyContactsVO [id=" + id + ", userAccountId=" + userAccountId + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone
				+ ", creationDate=" + creationDate + "]";
	} 
	
	
}