/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.cards.valueobjects;

/**
 * @author Umer Rasheed
 *
 */
public class RegisteredCardsVO {

	private String errorCode = "";
	private String errorMessage = "";
	private String noOfLines = "";
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getNoOfLines() {
		return noOfLines;
	}
	public void setNoOfLines(String noOfLines) {
		this.noOfLines = noOfLines;
	}
	
	public String toString()	{
		return "RegisteredCardsVO: ErrorCode=" + this.errorCode + ", ErrorMessage=" + this.errorMessage + ", NoOfLines=" + this.noOfLines;
	}
	
}
