/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class PackageHistoryRequestVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId = "";
	private String msisdn = "";
	private String sourcePkgName = "";
	private String destinationPkgName = "";
	private String transactionId = "";
	
	private boolean isTesting = false;
	
	
	
	public String getUserId()
	{
		return userId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getSourcePkgName()
	{
		return sourcePkgName;
	}
	public void setSourcePkgName(String sourcePkgName)
	{
		this.sourcePkgName = sourcePkgName;
	}
	public String getDestinationPkgName()
	{
		return destinationPkgName;
	}
	public void setDestinationPkgName(String destinationPkgName)
	{
		this.destinationPkgName = destinationPkgName;
	}
	public String getTransactionId()
	{
		return transactionId;
	}
	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}
	public boolean isTesting()
	{
		return isTesting;
	}
	public void setTesting(boolean isTesting)
	{
		this.isTesting = isTesting;
	}
	
	
}
