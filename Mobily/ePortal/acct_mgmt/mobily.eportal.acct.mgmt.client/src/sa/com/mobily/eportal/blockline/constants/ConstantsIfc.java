package sa.com.mobily.eportal.blockline.constants;

public class ConstantsIfc {
	public static final int STATUS_ACTIVE = 1;
	public static final String REPLY_QUEUE_BLOCK_SERVICE_INQUIRY = "mq.blockLine.reply.queue";
	public static final String REQUEST_QUEUE_BLOCK_SERVICE_INQUIRY = "mq.blockLine.request.queue";
	public static final String ERROR_CODE_EMPTY_REPLY_XML = "";
	public static final String BLOCK_ACCOUNT_ERRORMSG_GENERAL = "block.error.message.general";
	public static final String REMAINIG_INSTALLMENTS_MESSAGE = "remaining.installments";
	public static final String FUNCID = "SUSPENSE";
	public static final String SECURITYKEY = "0000";
	public static final String MSGVERSION = "0000";
	public static final String REQUESTCHANNELID = "ePortal";
	public static final String REQUESTOR_LANGUAGE = "E";
	public static final String CHARGABLE = "N";
	public static final String CHARGINGMODE = "";
	public static final String CHARGINGAMOUNT = "0.0";
	public static final String CHANNELTRANSID = "SRID";
	public static final String USERCOMMENT = "";
	public static final String NOTIFYBE = "Y";
	public static final String REASONID = "3";
	public static final String FINAL_REPLAY_QUEUE = "";
	public static final String FINAL_REPLAY_QUEUE_MANAGER = "";
	public static final String PARAMETER_NAME1 = "MSISDN";
	public static final String PARAMETER_NAME2 = "SIM_ID";
	public static final String PARAMETER_VALUE2 = "2";
	public static final String ALLOW_DUE_AMT_VALIDATION = "N";


	public static final String SUCCESS_MSG = "SUCCESS_MSG";
	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR_MSG = "ERROR_MSG";
	public static final String ERROR_BLOCK_LINE_NUMBER_BLOCKED = "BL-001";
	//public static final String ERROR_CODE_REMAINING_INSTALLMENTS = "7371";
	public static final String ERROR_CODE_REMAINING_INSTALLMENTS_7371 = "7371";
	public static final String ERROR_CODE_REMAINING_INSTALLMENTS = "1852";
}