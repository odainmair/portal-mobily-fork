package sa.com.mobily.eportal.acct.mgmt.login.history.util;

import java.text.SimpleDateFormat;
import java.util.Date;


public class LoginHistoryViewHelper {

	transient SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss aa"); 
    
	private Date loginHistoryDateTime;
	private String loginChannel;
	private int loginStatus;
	
	
	/**
	 * @return the loginHistoryDateTime
	 */
	public Date getLoginHistoryDateTime()
	{
		return loginHistoryDateTime;
	}
	/**
	 * @param loginHistoryDateTime the loginHistoryDateTime to set
	 */
	public void setLoginHistoryDateTime(Date loginHistoryDateTime)
	{
		this.loginHistoryDateTime = loginHistoryDateTime;
		//this.loginHistoryDateTime = formatter.format(loginHistoryDateTime);
	}
	/**
	 * @return the loginChannel
	 */
	public String getLoginChannel()
	{
		return loginChannel;
	}
	/**
	 * @param loginChannel the loginChannel to set
	 */
	public void setLoginChannel(String loginChannel)
	{
		this.loginChannel = loginChannel;
	}
	/**
	 * @return the loginStatus
	 */
	public int getLoginStatus()
	{
		return loginStatus;
	}
	/**
	 * @param loginStatus the loginStatus to set
	 */
	public void setLoginStatus(int loginStatus)
	{
		this.loginStatus = loginStatus;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "LoginHistoryViewHelper [loginHistoryDateTime=" + loginHistoryDateTime + ", loginChannel=" + loginChannel + ", loginStatus=" + loginStatus + "]";
	}

	

}
