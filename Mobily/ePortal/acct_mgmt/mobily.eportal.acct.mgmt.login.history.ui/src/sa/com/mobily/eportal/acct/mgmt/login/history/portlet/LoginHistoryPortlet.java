package sa.com.mobily.eportal.acct.mgmt.login.history.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderMode;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.acct.mgmt.login.history.service.view.LoginHistoryServiceLocal;
import sa.com.mobily.eportal.acct.mgmt.login.history.util.LoginHistoryContstants;
import sa.com.mobily.eportal.acct.mgmt.login.history.util.LoginHistoryViewHelper;
import sa.com.mobily.eportal.acct.mgmt.login.history.util.MyDateComparator;
import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryRequestVO;
import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryVO;
import sa.com.mobily.eportal.common.beanlocator.BeanLocator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;

import com.google.gson.Gson;
/**
 * A sample portlet based on GenericPortlet
 */
public class LoginHistoryPortlet extends GenericPortlet {

	private static String className = LoginHistoryPortlet.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.ACCT_MGMT_LOGIN_HISTORY);

	private LoginHistoryServiceLocal loginHistoryServiceLocalObj; 

	/**
	 * @see javax.portlet.Portlet#init()
	 */
	public void init() throws PortletException{
		loginHistoryServiceLocalObj = ((LoginHistoryServiceLocal) BeanLocator.lookup("ejblocal:"+ LoginHistoryServiceLocal.class.getName()));
	}

	/**
	 * Serve up the <code>view</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@RenderMode(name = LoginHistoryContstants.DEFAULT_VIEW)
	public void showLoginHistory(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		String methodName = "showLoginHistory";
		executionContext.audit(Level.INFO, "default view Called....", className, methodName);
		// Set the MIME type for the render response
		response.setContentType(request.getResponseContentType());

		// Invoke the JSP to render
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(getJspFilePath(request, LoginHistoryContstants.VIEW_JSP));
		rd.include(request,response);
	}


	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
		String methodName = "serveResource";
		
		String actionType = request.getParameter(LoginHistoryContstants.ACTION_TYPE);
		executionContext.audit(Level.INFO, "actionType : " + actionType, className, methodName);
		
		Gson gson = new Gson();
		
		try {
			
			if(LoginHistoryContstants.ACTION_TYPE_INITIALIZE_DATA_GRID.equalsIgnoreCase(actionType)){
				
				PumaUtil pumaUtil = new PumaUtil();
				String userName = pumaUtil.getUid();
				executionContext.audit(Level.INFO, "get Login History for userName =["+userName+"]", className, methodName);
				
				//Get the login history information from DB
					LoginHistoryRequestVO requestVoObj = new LoginHistoryRequestVO();
						requestVoObj.setUserName(userName);
						
					List<LoginHistoryVO> loadedLoginHistory = loginHistoryServiceLocalObj.findLoginHistoryByUserName(requestVoObj);

						
				Map<String,LoginHistoryViewHelper> loginHistoryViewHelperMap = new TreeMap<String,LoginHistoryViewHelper>();
				LoginHistoryViewHelper loginHistoryViewHelperObj = null;
				LoginHistoryVO loginHistoryV0Obj = null;
				
				for(int i=0; loadedLoginHistory != null && i < loadedLoginHistory.size(); i++){
					loginHistoryViewHelperObj = new LoginHistoryViewHelper();
					loginHistoryV0Obj = loadedLoginHistory.get(i);
					
					loginHistoryViewHelperObj.setLoginHistoryDateTime(loginHistoryV0Obj.getLoginDateTime());
					loginHistoryViewHelperObj.setLoginChannel(loginHistoryV0Obj.getSource());
					loginHistoryViewHelperObj.setLoginStatus(loginHistoryV0Obj.getLoginStatus());
	
					loginHistoryViewHelperMap.put(i+"", loginHistoryViewHelperObj);
				}
			
				List<LoginHistoryViewHelper> loginHistoryViewHelpersList = new ArrayList<LoginHistoryViewHelper>(loginHistoryViewHelperMap.values());

				//Sort the data based on the Date & time in descending order
					Collections.sort(loginHistoryViewHelpersList, new MyDateComparator());
				
				response.getWriter().write(gson.toJson(loginHistoryViewHelpersList));
				
			}

		}catch(Exception e){
			executionContext.log(Level.SEVERE, "Exception [" + e.getMessage() + "] ", className, methodName, e);
			response.getWriter().write(LoginHistoryContstants.FAILD_OPERATION);
		}
	}
	
	/**
	 * Process an action request.
	 * 
	 * @see javax.portlet.Portlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	public void processAction(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {

	}

	/**
	 * Returns JSP file path.
	 * 
	 * @param request Render request
	 * @param jspFile JSP file name
	 * @return JSP file path
	 */
	private static String getJspFilePath(RenderRequest request, String jspFile) {
		String markup = request.getProperty("wps.markup");
		if( markup == null )
			markup = getMarkup(request.getResponseContentType());
		return LoginHistoryContstants.JSP_FOLDER + markup + "/" + jspFile + "." + getJspExtension(markup);
	}

	/**
	 * Convert MIME type to markup name.
	 * 
	 * @param contentType MIME type
	 * @return Markup name
	 */
	private static String getMarkup(String contentType) {
		if( "text/vnd.wap.wml".equals(contentType) )
			return "wml";
        else
            return "html";
	}

	/**
	 * Returns the file extension for the JSP file
	 * 
	 * @param markupName Markup name
	 * @return JSP extension
	 */
	private static String getJspExtension(String markupName) {
		return "jsp";
	}

}