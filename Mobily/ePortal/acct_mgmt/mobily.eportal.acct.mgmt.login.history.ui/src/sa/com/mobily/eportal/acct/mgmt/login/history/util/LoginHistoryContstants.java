/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.login.history.util;

/**
 * @author n.gundluru.mit
 *
 */
public class LoginHistoryContstants {

	public static final String JSP_FOLDER    = "/_LoginHistoryPortlet/jsp/";    // JSP folder name
	public static final String VIEW_JSP      = "LoginHistoryPortletView";         // JSP file name to be rendered on the view mode

	public static final String DEFAULT_VIEW = "view";
	public static final String ACTION_TYPE = "action_type";
	public static final String ACTION_TYPE_INITIALIZE_DATA_GRID = "initializeDataGrid";
	public static final String FAILD_OPERATION = "faildOperation";
	
	public static final int LOGIN_STATUS_SUCCESS = 0;
	public static final int LOGIN_STATUS_FAILURE = 1;
	
	public static final String LOGIN_CHANNEL_WEBSITE = "EPRTL";
	public static final String LOGIN_CHANNEL_APP = "APPS";

}
