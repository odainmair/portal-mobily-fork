package sa.com.mobily.eportal.blockline.xml;

import java.io.StringReader;
import java.util.Date;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.blockline.constants.ConstantsIfc;
import sa.com.mobily.eportal.blockline.jaxp.request.MOBILYBSLSR;
import sa.com.mobily.eportal.blockline.jaxp.request.SRHEADER;
import sa.com.mobily.eportal.blockline.jaxp.response.MOBILYBSLSRREPLY;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineResponseVO;
import sa.com.mobily.eportal.blockline.valueobjects.SR_HEADER_REPLY;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

public class BlockLineHandler
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.BLOCK_LINE_SERVICE_LOGGER_NAME);

	private static String clazz = BlockLineHandler.class.getName();

	/**
	 * @MethodName: generateBlockAccountRequestXML
	 * <p>
	 * Handle the XML generation for MQ service that do the block request
	 * 
	 * @param BlockLineRequestVO blockLineRequestBO
	 * 
	 * @return String generatedXML 
	 * 
	 * @author Abu Sharaf
	 */
	public String generateBlockAccountRequestXML(BlockLineRequestVO blockLineRequestVO)
	{
		String xml = "";
		final ByteOutputStream outputStream = new ByteOutputStream();
		XMLStreamWriter xmlStreamWriter = null;

		String srDate = FormatterUtility.FormateDate(new Date());
		String srId = "ePortal_" + srDate;

		/* start Set XML Request BO */
		MOBILYBSLSR mOBILYBSLSR = new MOBILYBSLSR();
		SRHEADER header = new SRHEADER();
		header.setFuncId(ConstantsIfc.FUNCID);
		header.setSecurityKey(ConstantsIfc.SECURITYKEY);
		header.setMsgVersion(ConstantsIfc.MSGVERSION);
		header.setRequestorChannelId(ConstantsIfc.REQUESTCHANNELID);
		header.setSrDate(srDate);
		header.setRequestorUserId(blockLineRequestVO.getUsername());
		header.setRequestorLanguage(ConstantsIfc.REQUESTOR_LANGUAGE);
		header.setChargeable(ConstantsIfc.CHARGABLE);
		header.setChargingMode(ConstantsIfc.CHARGINGMODE);
		header.setChargeAmount(ConstantsIfc.CHARGINGAMOUNT);
		mOBILYBSLSR.setSRHEADER(header);
		mOBILYBSLSR.setSuspenseReason("5");
		mOBILYBSLSR.setMSISDN(blockLineRequestVO.getMsisdn());
		mOBILYBSLSR.setLOGINID("");
		mOBILYBSLSR.setDOMAIN("");
		mOBILYBSLSR.setREPLYQUEUENAME("");
		mOBILYBSLSR.setREPLYMQUEUENAME("");
		mOBILYBSLSR.setNOTIFYBE(ConstantsIfc.NOTIFYBE);
		mOBILYBSLSR.setSuspenseDate("");
		mOBILYBSLSR.setChannelTransId(srId);
		mOBILYBSLSR.setCustomerType(blockLineRequestVO.getCustomerType());
		mOBILYBSLSR.setReasonType(ConstantsIfc.REASONID);
		mOBILYBSLSR.setUserComment("");
		mOBILYBSLSR.setPackageName("");
		mOBILYBSLSR.setPackageId(blockLineRequestVO.getPackageId());
		mOBILYBSLSR.setPackageDataType(blockLineRequestVO.getPackageDataType());
		mOBILYBSLSR.setSuspenseDateFrom("");
		mOBILYBSLSR.setSuspenseDateTo("");
		mOBILYBSLSR.setAllowDueAmtValidation(ConstantsIfc.ALLOW_DUE_AMT_VALIDATION);
		mOBILYBSLSR.setChargeDeviceInstallment(blockLineRequestVO.getChargeDeviceInstallment());
		
		/* start Set XML Request BO */
		try
		{
			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
			JAXBContext jaxbContext = JAXBContext.newInstance(MOBILYBSLSR.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(mOBILYBSLSR, xmlStreamWriter);
			outputStream.flush();

			xml = new String(outputStream.getBytes(), "UTF-8").trim();

		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "generateBlockAccountRequestXML", e);
			executionContext.log(logEntryVO);
			throw new XMLParserException(e.getMessage(), e);
		}
		return xml;
	}

	/**
	 * @MethodName: parseBlockAccountRequestXML
	 * <p>
	 * Parse the generated XML from BlockLineHandler to fill the request object correctly
	 * 
	 * @param String xml
	 * 
	 * @return BlockLineResponseVO 
	 * 
	 * @author Abu Sharaf
	 */
	public BlockLineResponseVO parseBlockAccountRequestXML(String xml)
	{
		BlockLineResponseVO blockLineResponseVO = null;
		try
		{
			JAXBContext jc = JAXBContext.newInstance(MOBILYBSLSRREPLY.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			XMLInputFactory xmlif = XMLInputFactory.newInstance();
			XMLStreamReader xmlstreamer = xmlif.createXMLStreamReader(new StringReader(xml));
			MOBILYBSLSRREPLY obj = (MOBILYBSLSRREPLY) unmarshaller.unmarshal(xmlstreamer);

			blockLineResponseVO = new BlockLineResponseVO();

			blockLineResponseVO.setErrorCode(FormatterUtility.checkNull(obj.getErrorCode()));
			blockLineResponseVO.setErrorMessage(obj.getErrorMsg());
			blockLineResponseVO.setRemainingInstallments(FormatterUtility.checkNull(obj.getRemainingInstallments()));

		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "parseBlockAccountRequestXML", e);
			executionContext.log(logEntryVO);
			throw new XMLParserException(e.getMessage(), e);
		}
		return blockLineResponseVO;
	}
}