package sa.com.mobily.eportal.blockline.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.blockline.config.AppConfig;
import sa.com.mobily.eportal.blockline.constants.ConstantsIfc;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.BlockLineResponseVO;
import sa.com.mobily.eportal.blockline.valueobjects.CheckStatusRequestVO;
import sa.com.mobily.eportal.blockline.valueobjects.CheckStatusResponseVO;
import sa.com.mobily.eportal.blockline.xml.BlockLineHandler;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.facade.MobilyCommonFacade;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.common.service.vo.CustomerStatusVO;

public class BlockLineJMSRepository
{

	private static String clazz = BlockLineJMSRepository.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.BLOCK_LINE_SERVICE_LOGGER_NAME);

	/**
	 * @MethodName: checkStatus
	 * <p>
	 * Check user phone status if it's blocked or not
	 * 
	 * @param CheckStatusRequestVO checkStatusRequestBO
	 * 
	 * @return CheckStatusResponseVO 
	 * 
	 * @author Abu Sharaf
	 */
	public CheckStatusResponseVO checkStatus(CheckStatusRequestVO checkStatusRequestBO)
	{
		CheckStatusResponseVO checkStatusResponseBO = null;
		try
		{
			checkStatusResponseBO = new CheckStatusResponseVO();
			CustomerStatusVO custStatusVo = new CustomerStatusVO();
			custStatusVo.setLineNumber(checkStatusRequestBO.getMsisdn());
			custStatusVo.setRequesterUserId(checkStatusRequestBO.getUserId());

			/*
			 * 1 - Active; 2 - Warned; 3 - OG Barred; 4 - FULL barred; 5 -
			 * Suspended; 6 - De-activated
			 */
			CustomerStatusVO replyVo = new MobilyCommonFacade().getCustomerStatus(custStatusVo);
			if (null != replyVo)
			{
				LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > checkBlockStatus > msisdn [" + checkStatusRequestBO.getMsisdn()
						+ "] status from customer status service is [" + replyVo.getStatus() + "]", clazz, "checkStatus", null);
				executionContext.log(logEntryVO);
				if (ConstantsIfc.STATUS_ACTIVE == replyVo.getStatus())
				{
					checkStatusResponseBO.setBlocked(false);
				}
				else
				{
					checkStatusResponseBO.setBlocked(true);
				}
			}
			return checkStatusResponseBO;

		}
		catch (MobilyCommonException e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "MobilyCommonException " + e.getMessage(), clazz, "checkStatus", e);
			executionContext.log(logEntryVO);
			throw new ServiceException(e.getMessage(), e);
		}
	}

	/**
	 * @MethodName: blockLine
	 * <p>
	 * Perform the block procedure throw MQ to stop and block the number
	 * 
	 * @param BlockLineRequestVO blockLineRequestBO
	 * 
	 * @return BlockLineResponseVO 
	 * 
	 * @author Abu Sharaf
	 */
	public BlockLineResponseVO blockLine(BlockLineRequestVO blockLineRequestBO)
	{
		MQAuditVO mqAuditVO = null;
		String strXMLRequest = "";
		String strXMLReply = "";
		String requestQueueName = "";
		String replyQueueName = "";
		boolean isTesting = false;
		
		requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_BLOCK_SERVICE_INQUIRY);
		replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_BLOCK_SERVICE_INQUIRY);

		executionContext.log(Level.INFO,"BlockLineJMSRepository > BlockLine > started... " + requestQueueName,clazz, "blockLine");
		
		BlockLineResponseVO blockLineResponseBO = new BlockLineResponseVO();
		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > BlockLine : requestQueueName[" + requestQueueName + "]", clazz, "blockLine", null);
		executionContext.log(logEntryVO);
		logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > BlockLine : replyQueueName[" + replyQueueName + "]", clazz, "blockLine", null);
		executionContext.log(logEntryVO);
		
		executionContext.log(Level.INFO,"BlockLineJMSRepository > BlockLine > before generate request " + requestQueueName,clazz, "blockLine");

		strXMLRequest = new BlockLineHandler().generateBlockAccountRequestXML(blockLineRequestBO);
		logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > BlockLine : strXMLRequest: " + strXMLRequest, clazz, "blockLine", null);
		executionContext.log(logEntryVO);
		mqAuditVO = new MQAuditVO();
		mqAuditVO.setFunctionName(ConstantsIfc.FUNCID);
		mqAuditVO.setUserName(FormatterUtility.checkNull(blockLineRequestBO.getUsername()));
		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(FormatterUtility.checkNull(blockLineRequestBO.getMsisdn()));
		mqAuditVO.setMessage(FormatterUtility.checkNull(strXMLRequest));
		
		if(isTesting){
			 strXMLReply = "<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>SUSPENSE</FuncId><SecurityKey>0000</SecurityKey><MsgVersion>0000</MsgVersion>" +
					"<RequestorChannelId>BSL</RequestorChannelId><SrDate>20170208154805</SrDate><ReplierChanneld>PORTAL</ReplierChanneld>" +
					"<SrRcvDate>20170208154805</SrRcvDate><SrStatus>6</SrStatus></SR_HEADER_REPLY>" +
					"<ChannelTransId>ePortal_20170208154803</ChannelTransId><ServiceRequestId>SR_8501515170</ServiceRequestId>" +
					"<ErrorCode>0</ErrorCode><ErrorMsg>Successfully fetched the Installment Deal Details</ErrorMsg>" +
					"<RemainingInstallments>1430</RemainingInstallments></MOBILY_BSL_SR_REPLY>";
		}else{
			strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		}
		
		mqAuditVO.setReply(FormatterUtility.checkNull(strXMLReply));
		logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > BlockLine : strXMLReply: " + strXMLReply, clazz, "blockLine", null);
		executionContext.log(logEntryVO);

		if (FormatterUtility.isNotEmpty(strXMLReply)) {
			blockLineResponseBO = new BlockLineHandler().parseBlockAccountRequestXML(strXMLReply);
			if(blockLineResponseBO != null){
				mqAuditVO.setServiceError(FormatterUtility.checkNull(blockLineResponseBO.getErrorCode()));
				mqAuditVO.setErrorMessage(FormatterUtility.checkNull(blockLineResponseBO.getErrorMessage()));
			}
		} else {
			blockLineResponseBO.setErrorMessage(ConstantsIfc.BLOCK_ACCOUNT_ERRORMSG_GENERAL);
			mqAuditVO.setServiceError("TimeOut");
			mqAuditVO.setErrorMessage("TimeOut");
			// throw new MobilyCommonException(
			// ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML,
			// ConstantsIfc.BLOCK_ACCOUNT_ERRORMSG_GENERAL);
		}
		MQUtility.saveMQAudit(mqAuditVO);
		// if the error code returned from back-end is greater than 0 then
		// it will throw MobilyCommonexception object
		
		if(blockLineResponseBO != null && FormatterUtility.isNotEmpty(blockLineResponseBO.getErrorCode())
				&& (ConstantsIfc.ERROR_CODE_REMAINING_INSTALLMENTS_7371.equals(blockLineResponseBO.getErrorCode())
						|| Integer.parseInt(blockLineResponseBO.getErrorCode()) == 0)){
			
			return blockLineResponseBO; 

		}else{
			
			String errorCode = FormatterUtility.checkNull("" + blockLineResponseBO.getErrorCode());
			logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > BlockLine : errorCode: " + errorCode, clazz, "blockLine", null);
			executionContext.log(logEntryVO);
			throw new BackEndException(ExceptionConstantIfc.BLOCK_NUMBER, errorCode);
			
		}
		/*
		if (null == blockLineResponseBO || 
				(Integer.parseInt("" + blockLineResponseBO.getErrorCode()) > 0 && !blockLineResponseBO.getErrorCode().equals(ConstantsIfc.ERROR_CODE_REMAINING_INSTALLMENTS))
		)
		{
			String errorCode = FormatterUtility.checkNull("" + blockLineResponseBO.getErrorCode());
			logEntryVO = new LogEntryVO(Level.INFO, "BlockLineJMSRepository > BlockLine : errorCode: " + errorCode, clazz, "blockLine", null);
			executionContext.log(logEntryVO);
			throw new BackEndException(ExceptionConstantIfc.BLOCK_NUMBER, errorCode);
		}
		return blockLineResponseBO;
		*/
	}
}