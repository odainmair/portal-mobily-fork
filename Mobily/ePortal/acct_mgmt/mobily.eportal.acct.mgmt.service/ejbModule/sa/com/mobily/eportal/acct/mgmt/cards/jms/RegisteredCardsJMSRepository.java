package sa.com.mobily.eportal.acct.mgmt.cards.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.cards.constants.ConstantsIfc;
import sa.com.mobily.eportal.acct.mgmt.cards.constants.TagIfc;
import sa.com.mobily.eportal.acct.mgmt.cards.util.CardsAppConfig;
import sa.com.mobily.eportal.acct.mgmt.cards.valueobjects.RegisteredCardsVO;
import sa.com.mobily.eportal.acct.mgmt.cards.xml.RegisteredCardsXMLHandler;
import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;



public final class RegisteredCardsJMSRepository {

	private static String clazz = RegisteredCardsJMSRepository.class
			.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory
			.getExecutionContext(AcctMgmtLoggerConstantsIfc.CARDS_SERVICE_LOGGER_NAME);


	/**
	 * @MethodName: getCardNumber
	 * <p>
	 * Either return the card number by msisdn for user or return error code if not
	 * 
	 * @param String msisdn
	 * 
	 * @return RegisteredCardsVO
	 * 
	 * @author Abu Sharaf
	 */
	public RegisteredCardsVO getCardNumber(
			String  msisdn) {

		String methodName = "getCardNumber"; 
		RegisteredCardsVO registeredCardsVO = null;
		executionContext.audit(Level.INFO, "Started for msisdn[" + msisdn + "]", clazz, methodName);
		
		String requestQueueName = CardsAppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_NAME);
		String replyQueueName = CardsAppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_NAME); 
		String strXMLReply = "";
		boolean isTesting = false;
		MQAuditVO mqAuditVO = null;
		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", clazz, methodName);

		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", clazz, methodName);

		String strXMLRequest = new RegisteredCardsXMLHandler().generateNumberOfLinesRequestXML(msisdn);
		executionContext.audit(Level.INFO, "strXMLRequest[" + strXMLRequest + "]", clazz, methodName);
		
		mqAuditVO = new MQAuditVO();
		mqAuditVO.setFunctionName(TagIfc.FUNC_ID_ADV_GENERAL_INQ);
		mqAuditVO.setUserName("Anonymous");
		mqAuditVO.setMsisdn(msisdn);
		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMessage(FormatterUtility.checkNull(strXMLRequest));
		
		if(isTesting){
			 strXMLReply ="<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>ADV_GENERAL_INQ</FuncId><SecurityKey>123</SecurityKey><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><ServiceRequestId>SR_20180102121352</ServiceRequestId><SrDate>20180102121352</SrDate><RequestorUserId>ePortal</RequestorUserId><RequestorLanguage>E</RequestorLanguage></SR_HEADER_REPLY><ReturnCode><ErrorCode>0</ErrorCode></ReturnCode><ErrorMsg>Success</ErrorMsg><NoOfLines>5</NoOfLines></MOBILY_BSL_SR_REPLY>";
		}else{
			 //strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
			strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		}
		executionContext.audit(Level.INFO, "strXMLReply[" + strXMLReply + "]", clazz, methodName);
		mqAuditVO.setReply(FormatterUtility.checkNull(strXMLReply));
		
		if (FormatterUtility.isNotEmpty(strXMLReply))
		{
			registeredCardsVO = new RegisteredCardsXMLHandler().parseNumberOfLinesReplyXML(strXMLReply);
			mqAuditVO.setErrorMessage(FormatterUtility.checkNull(registeredCardsVO.getErrorCode()));
			executionContext.audit(Level.INFO, " registeredCardsVO [" + registeredCardsVO + "]", clazz, methodName);
		}
		else
		{
			registeredCardsVO = new RegisteredCardsVO();
			mqAuditVO.setErrorMessage("Time Out");
			mqAuditVO.setServiceError("Time Out");
			registeredCardsVO.setErrorCode(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
		}
		MQUtility.saveMQAudit(mqAuditVO);
		if (FormatterUtility.isNotEmpty(registeredCardsVO.getErrorCode())
				&& Integer.parseInt(registeredCardsVO.getErrorCode()) > 0) {
			String errorCode = FormatterUtility.checkNull(registeredCardsVO.getErrorCode());

			executionContext.audit(Level.INFO, "errorCode[" + errorCode + "]", clazz, methodName);

			throw new BackEndException(13, errorCode);
		}
		return registeredCardsVO;
	}

	
}
