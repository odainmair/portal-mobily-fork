package sa.com.mobily.eportal.authority.config;

import java.io.IOException;

import java.io.InputStream;
import java.util.Properties;

public class AppConfig
{

	protected static final String propertyFileName = "config/config.properties";

	protected static AppConfig config = null;

	protected Properties properties;

	protected AppConfig()
	{
		properties = new Properties();
		ClassLoader sc = Thread.currentThread().getContextClassLoader();
		InputStream stream = sc.getResourceAsStream(propertyFileName);

		if (stream == null)
		{
			throw new IllegalArgumentException("Could not load " + propertyFileName + ".Please make sure that it is in CLASSPATH.");
		}

		try
		{
			properties.load(stream);
		}
		catch (IOException e)
		{
			IllegalStateException ex = new IllegalStateException("An error occurred when reading from the input stream");
			ex.initCause(e);
			throw ex;
		}
		finally
		{
			try
			{
				stream.close();
			}
			catch (IOException e)
			{
				IllegalStateException ex = new IllegalStateException("An I/O error occured while closing the stream");
				ex.initCause(e);
				throw ex;
			}
		}
	}

	public static AppConfig getInstance()
	{
		if (config == null)
			config = new AppConfig();
		return config;
	}

	public String get(String key)
	{
		if (properties == null || key == null)
			return null;
		return (String) properties.get(key);
	}
}