/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author n.gundluru.mit
 * 
 */
public class PkgMgmtUtil
{
	public static String FormateDateT(Date date)
	{
		String formatedDate = null;
		SimpleDateFormat sformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

		formatedDate = sformat.format(date);
		return validate(formatedDate);
	}

	public static String FormateDate(Date date)
	{
		String formatedDate = null;
		SimpleDateFormat sformat = new SimpleDateFormat("yyyyMMddHHmmss");

		formatedDate = sformat.format(date);
		return validate(formatedDate);
	}

	public static String validate(String str)
	{
		return (str == null) ? "" : str;
	}

}
