package sa.com.mobily.eportal.mycontact.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;
import sa.com.mobily.eportal.common.service.dao.UserAccountDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.model.UserAccount;
import sa.com.mobily.eportal.common.service.util.caching.CachingService;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.LongVO;
import sa.com.mobily.eportal.common.service.vo.StringVO;
import sa.com.mobily.eportal.mycontact.dao.MyContactsDAO;
import sa.com.mobily.eportal.mycontact.service.view.MyContactsServiceBeanLocal;
import sa.com.mobily.eportal.mycontact.service.view.MyContactsServiceBeanRemote;
import sa.com.mobily.eportal.mycontact.vo.MyContactsRequestVO;
import sa.com.mobily.eportal.mycontact.vo.MyContactsVO;

/**
 * This class is responsible about managing the contacts of the user
 * <p> 
 * it contains methods for retrieve, add, delete and update.
 *  
 * @author Yousef Alkhalaileh
 */
@Stateless
@Local(MyContactsServiceBeanLocal.class)
@Remote(MyContactsServiceBeanRemote.class)
@LocalBean
@TransactionManagement(TransactionManagementType.CONTAINER)
public class MyContactsServiceBean implements MyContactsServiceBeanLocal, MyContactsServiceBeanRemote {
	
	private static final String CONTACTS_KEY = "CONTACTS";
	private static final String USER_ACCOUNT_KEY = "USER_ACCOUNT";
	
	private static String clazz = MyContactsServiceBean.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.MY_CONTACTS_SERVICE_LOGGER_NAME);
	
	/**
	 * This method retrieves all the contacts for a specific account ID
	 * 
	 * @param <code>accountId</code>
	 * @return <code>List<Contact></code>
	 * @author Yousef Alkhalaileh
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public List<MyContactsVO> findByAccountId(@NotNull LongVO accountId){
		ArrayList<MyContactsVO> contacts = MyContactsDAO.getInstance().findContactsByAcountId(accountId.getValue());
		executionContext.log(Level.INFO, "Getting contacts from DB for account ID "+accountId+", Contacts list size = "+contacts.size(), clazz, "findByAccountId", null);
		CachingService.getInstance().putCachedResponse(accountId.getValue().longValue() + CONTACTS_KEY, contacts);
		return contacts;
	}
	
	/**
	 * This method deletes a list of contacts for a specific account id
	 * 
	 * @param <code>List<Long> myContactIds, Long accountId</code> myContactIds to be deleted
	 * @author Yousef Alkhalaileh
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Interceptors(ServiceInterceptor.class)
	public void deleteContactByIds(@NotNull MyContactsRequestVO request) {
		executionContext.log(Level.INFO, "Deleting contacts id =["+request+"] for useraccountid =["+request.getAccountId()+"]", clazz, "deleteContactByIds", null);
		if(null != request.getAccountId() && null != request.getContactIds() && request.getContactIds().size() > 0){
			
			int delCount = MyContactsDAO.getInstance().deleteContactById(request.getContactIds(), request.getAccountId());
			executionContext.log(Level.INFO, "["+delCount+"] contacts deleted for useraccountid =["+request.getAccountId()+"]", clazz, "deleteContactByIds", null);
			if(delCount == 0){
				executionContext.log(Level.INFO, "Since count is zero, thorw the exception...!", clazz, "deleteContactByIds", null);
				throw new SystemException();
			}
			
			CachingService.getInstance().putCachedResponse(request.getAccountId().longValue() + CONTACTS_KEY, null);
		}
	}
	
	/**
	 * This method updates a contact for a specific account id
	 * 
	 * @param <code>Contact contact</code> Contact to be updated
	 * @author Yousef Alkhalaileh
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Interceptors(ServiceInterceptor.class)
	public void updateContact(@NotNull MyContactsRequestVO request){
		executionContext.log(Level.INFO, "Updating contact "+request, clazz, "updateContact", null);
		if(null != request.getContact() && null != request.getContact().getUserAccountId() ){
			MyContactsDAO.getInstance().updateContact(request.getContact());
			CachingService.getInstance().putCachedResponse(request.getContact().getUserAccountId().longValue() + CONTACTS_KEY, null);
		}
	}
	
	/**
	 * This method adds a contact for a specific account id
	 * 
	 * @param <code>Contact contact</code> Contact to be inserted
	 * @param <code>int</code> number of records affected
	 * @author Yousef Alkhalaileh
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Interceptors(ServiceInterceptor.class)
	public void addContact(@NotNull MyContactsRequestVO request){
		executionContext.log(Level.INFO, "Adding a contact "+request, clazz, "addContact", null);
		if(null != request.getContact() && null != request.getContact().getUserAccountId()){
			MyContactsDAO.getInstance().saveContact(request.getContact());
			CachingService.getInstance().putCachedResponse(request.getContact().getUserAccountId().longValue() + CONTACTS_KEY, null);
		}
	}
	
	/**
	 * This method retrieves user account information based on the user name
	 * 
	 * @param <code>String userName</code>
	 * @return <code>UserAccount</code>
	 * @author Yousef Alkhalaileh
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public UserAccount findAccountByUserName(@NotNull StringVO userName) throws DataNotFoundException{
		executionContext.log(Level.INFO, "Getting account info for user name "+userName, clazz, "findAccountByUserName", null);
		UserAccount userAccount = (UserAccount)CachingService.getInstance().getCachedResponse(userName + USER_ACCOUNT_KEY);
		if(null != userAccount){
			return userAccount;
		}else{
			List<UserAccount> accounts = UserAccountDAO.getInstance().findByUserName(userName.getValue().toUpperCase());
			if(null == accounts || 0 == accounts.size()){
				executionContext.log(Level.INFO, "Could not find account info for user name "+userName, clazz, "findAccountByUserName", null);
				throw new DataNotFoundException("Could not find data for user name " + userName);
			}
			CachingService.getInstance().putCachedResponse(userName + USER_ACCOUNT_KEY, accounts.get(0));
			return accounts.get(0);
		}
	}
	
	
	/**
	 * This method returns a list contacts which are Starting with the given Phone number
	 * 
	 * @param <code>phoneNumber</code>
	 * @return <code>List<Contact></code>
	 * @author Nagendra B. Gundluru
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Interceptors(ServiceInterceptor.class)
	public List<MyContactsVO> findByPhoneNumber(@NotNull String phoneNumber, @NotNull LongVO accountId){
		ArrayList<MyContactsVO> contacts = MyContactsDAO.getInstance().findContactsByPhone(phoneNumber, accountId.getValue());
		executionContext.log(Level.INFO, "Getting contacts from DB for phoneNumber =["+phoneNumber+"] and under accountId =["+accountId.getValue()+"], Contacts list size = "+contacts.size(), clazz, "findByAccountId", null);

		return contacts;
	}
	
	/**
	 * This method updates contact on the basis of account Id and phone number
	 * 
	 * @param <code>Contact contact</code> Contact to be updated
	 * @author Umair Saleem
	 * @dated 25-4-2019
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Interceptors(ServiceInterceptor.class)
	public int updateContactsByAccountId(@NotNull MyContactsRequestVO request){
		executionContext.log(Level.INFO, "Updating contacts "+request, clazz, "updateContactsByAccountIdAndPhone", null);
		int updatedContacts = 0;
		if(request.getContact() != null && request.getContact().getUserAccountId() != null &&  request.getContact().getPhone() != null){
			updatedContacts = MyContactsDAO.getInstance().updateContactsByAccountId(request.getContact());
			CachingService.getInstance().putCachedResponse(request.getContact().getUserAccountId().longValue() + CONTACTS_KEY, null);
		}
		
		return updatedContacts;
	}
	
	/**
	 * This method adds a contact for a specific account id and will be in Pending State until AddConfirm Call
	 * 
	 * @param <code>Contact contact</code> Contact to be inserted
	 * @param <code>int</code> number of records affected
	 * @author Amr Osman
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Interceptors(ServiceInterceptor.class)
	public void addContactForAddSubscription(@NotNull MyContactsRequestVO request){
		executionContext.log(Level.INFO, "Adding a contact "+request, clazz, "addContact", null);
		if(null != request.getContact() && null != request.getContact().getUserAccountId()){
			MyContactsDAO.getInstance().saveContactAddSubscription(request.getContact());
			CachingService.getInstance().putCachedResponse(request.getContact().getUserAccountId().longValue() + CONTACTS_KEY, null);
		}
	}
	
}
	