package sa.com.mobily.eportal.activenumber.service.constants;

public interface ErrorCodeConstantsIfc
{

	String MOBILE_IQAMA_MISMATCH = "200001";

	String USER_NOT_LOADED = "200002";

	String GENERAL_ERROR = "900999";

	String MOBILE_VERIFICATION_SMS_FAILED = "200003";

	String USER_RELATED_SUBS_NOT_LOADED = "200004";

	String DEFAULT_SUBS_EXISTS = "200005";

	String SUBS_EXISTS = "200006";
	
	String SUB_VO_NOT_VALID = "InvalidSubScription";
	
	public static final int REGISTERATION_ACTIVATION_PROJECT_ID=4;
	public static final String ACTIVATION_CODE_SMS_ID_AR = "652";
	public static final String ACTIVATION_CODE_SMS_ID_EN = "651";
	public static final String PIN_CODE_INVALID = "-1";
}