package sa.com.mobily.eportal.activenumber.service.mock;

import java.util.ArrayList;
import java.util.List;

import sa.com.mobily.eportal.common.service.valueobject.common.ManagedMSISDNDataVO;

public class CRMMock
{

	/**
	 * @MethodName: getCustomerRelatedAccountByNumber
	 * <p>
	 * Get Customer related accounts by MSISDN
	 * 
	 * @param String msisdn
	 * 
	 * @return List<ManagedMSISDNDataVO> 
	 * 
	 * @author Abu Sharaf
	 */
	public static List<ManagedMSISDNDataVO> getCustomerRelatedAccountByNumber(String msisdn)
	{
		List<ManagedMSISDNDataVO> relatedList = null;
		try
		{
			relatedList = new ArrayList<ManagedMSISDNDataVO>();

			ManagedMSISDNDataVO vo = new ManagedMSISDNDataVO();
			vo.setMSISDN("966569998501");
			vo.setServiceAccountNumber("966569998501");
			vo.setCustomertype("Prepaid Plan");
			vo.setType("2");
			vo.setLineType(15);
			relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN("966593568201");
			vo.setServiceAccountNumber(null);
			vo.setCustomertype("Postpaid Plan");
			vo.setType("1");
			vo.setLineType(11);
			relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN("966507138301");
			vo.setServiceAccountNumber("966507138301");
			vo.setCustomertype("Prepaid Plan");
			vo.setType("2");
			vo.setLineType(11);
			relatedList.add(vo);

			// vo = new ManagedMSISDNDataVO();
			// vo.setMSISDN(null);
			// vo.setServiceAccountNumber("966569998401");
			// vo.setCustomertype("Broadband");
			// vo.setType("2");
			// vo.setLineType(2);
			// relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN("966569998502");
			vo.setServiceAccountNumber("966569998502");
			vo.setCustomertype("Postpaid Plan");
			vo.setType("2");
			vo.setLineType(15);

			relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN(null);
			vo.setServiceAccountNumber("500980076543");
			vo.setCustomertype("Prepaid Plan");
			vo.setType("3");
			vo.setLineType(17);

			relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN(null);
			vo.setServiceAccountNumber("500980076544");
			vo.setCustomertype("Postpaid Plan");
			vo.setType("3");
			vo.setLineType(17);

			relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN("966569998601");
			vo.setServiceAccountNumber("100980076123");
			vo.setCustomertype("Prepaid Plan");
			vo.setType("4");
			vo.setLineType(16);

			relatedList.add(vo);

			vo = new ManagedMSISDNDataVO();
			vo.setMSISDN("966569998602");
			vo.setServiceAccountNumber("100980076124");
			vo.setCustomertype("Postpaid Plan");
			vo.setType("4");
			vo.setLineType(16);

			relatedList.add(vo);

		}
		catch (Exception e)
		{
		}
		return relatedList;
	}
}