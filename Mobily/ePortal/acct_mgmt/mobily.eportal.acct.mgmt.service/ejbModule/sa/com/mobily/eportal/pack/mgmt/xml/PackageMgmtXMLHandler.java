package sa.com.mobily.eportal.pack.mgmt.xml;

import java.util.Date;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.pack.mgmt.constants.ConstantsIfc;
import sa.com.mobily.eportal.pack.mgmt.conv.request.Account;
import sa.com.mobily.eportal.pack.mgmt.conv.request.AccountOrderItem;
import sa.com.mobily.eportal.pack.mgmt.conv.request.AsyncResponse;
import sa.com.mobily.eportal.pack.mgmt.conv.request.CustomerOrder;
import sa.com.mobily.eportal.pack.mgmt.conv.request.CustomerOrderProperties;
import sa.com.mobily.eportal.pack.mgmt.conv.request.CustomerOrderProperty;
import sa.com.mobily.eportal.pack.mgmt.conv.request.MsgRqHeader;
import sa.com.mobily.eportal.pack.mgmt.conv.request.NumberResource;
import sa.com.mobily.eportal.pack.mgmt.conv.request.NumberResourceOrderItem;
import sa.com.mobily.eportal.pack.mgmt.conv.request.Order;
import sa.com.mobily.eportal.pack.mgmt.conv.request.Product;
import sa.com.mobily.eportal.pack.mgmt.conv.request.ProductInstanceOrderItem;
import sa.com.mobily.eportal.pack.mgmt.conv.request.ProductOrderItem;
import sa.com.mobily.eportal.pack.mgmt.conv.request.ProductProperties;
import sa.com.mobily.eportal.pack.mgmt.conv.request.ProductProperty;
import sa.com.mobily.eportal.pack.mgmt.conv.request.SubscriberOrderItem;
import sa.com.mobily.eportal.pack.mgmt.hist.request.MOBILYBSLSR;
import sa.com.mobily.eportal.pack.mgmt.hist.request.SRHEADER;
import sa.com.mobily.eportal.pack.mgmt.hist.response.MOBILYBSLSRREPLY;
import sa.com.mobily.eportal.pack.mgmt.util.PkgMgmtUtil;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageConversionRepVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageConversionReqVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryRequestVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class PackageMgmtXMLHandler implements ConstantsIfc {
	private static final String className = PackageMgmtXMLHandler.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.ACCT_MGMT_PACKAGE_MGMT);
	
	/**
	 * 
	 * @param getSIMDetailsRequestVO
	 * @return
	 */
	public String generateGetPkgHistoryRequestXML(PackageHistoryRequestVO requestVO) {
		String method = "generateGetPkgHistoryRequestXML";
		String xmlRequest = null;

		try {
			String srDate = MobilyUtility.FormateDate(new Date());

			//ResourceEnvironmentProviderService resourceEnvironmentProviderService = ResourceEnvironmentProviderService.getInstance();
			String requestorChannelId = REQUESTER_CHANNEL_ID;
			/*if (resourceEnvironmentProviderService != null) {
				Object value = resourceEnvironmentProviderService.getPropertyValue("REQUESTOR_CHANNEL_ID");
				if (value != null) {
					requestorChannelId = (String) value;
				} 
			}*/
			executionContext.log(Level.INFO, "requestorChannelId ["+requestorChannelId+"]", className, method);

			
			MOBILYBSLSR requestObj = new MOBILYBSLSR();
			
				SRHEADER header = new SRHEADER();
					header.setFuncId(FUNC_ID_PKGCONV_HISTORY_INQUIRY);
					header.setSecurityKey(SECURITYKEY);
					header.setMsgVersion(MSGVERSION_00001);
					header.setRequestorChannelId(requestorChannelId);
					header.setSrDate(srDate);
					if(FormatterUtility.isNotEmpty(requestVO.getUserId()))
						header.setRequestorUserId(requestVO.getUserId());
					else
						header.setRequestorUserId(REQUESTER_USER_ID_ANONYMOUS);
					header.setRequestorLanguage(REQUESTOR_LANGUAGE);
					header.setOverwriteOpenOrder(Y);
					header.setChargeable(N);
					header.setChargingMode(CHARGING_MODE);
					header.setChargeAmount(CHARGING_AMOUNT);
					header.setOrderCreationDate(srDate);
					header.setOrderSubmitDate(srDate);
				requestObj.setSRHEADER(header);
				
				requestObj.setChannelTransId(requestVO.getTransactionId());
				requestObj.setMSISDN(requestVO.getMsisdn());
				requestObj.setSourcePackageName(FormatterUtility.checkNull(requestVO.getSourcePkgName()));
				requestObj.setDestinationPackageName(FormatterUtility.checkNull(requestVO.getDestinationPkgName()));
				
			xmlRequest = JAXBUtilities.getInstance().marshal(requestObj);
			
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception ::>"+e.getMessage(), className, method, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		
		return xmlRequest;
	}

	/**
	 * 
	 * @param replyXML
	 * @return
	 */
	public PackageHistoryVO parseGetPkgHistoryResponseXML(String replyXML) {
		
		String method = "parseGetPkgHistoryResponseXML";
		PackageHistoryVO replyVO = new PackageHistoryVO();
		
		try {
			if(FormatterUtility.isNotEmpty(replyXML)) {
				MOBILYBSLSRREPLY replyObj = (MOBILYBSLSRREPLY) JAXBUtilities.getInstance().unmarshal(MOBILYBSLSRREPLY.class, replyXML);
				if (replyObj != null) {
					String errorCode = replyObj.getErrorCode();
					if (StringUtils.isNotEmpty(errorCode) && StringUtils.isNumeric(errorCode) && Integer.parseInt(errorCode) != 0) {
						replyVO.setErrorCode(replyObj.getErrorCode());
						replyVO.setErrorMsg(replyObj.getErrorMsg());
					} else {
						replyVO.setMsisdn(replyObj.getMSISDN());
						replyVO.setSourcePkgName(replyObj.getSourcePackageName());
						replyVO.setDestinationPkgName(replyObj.getDestinationPackageName());
						replyVO.setPackageConvCount(replyObj.getPackageConversionCount());
					}
				}
			} else {
				executionContext.log(Level.SEVERE, "XML Reply is null.", className, method, null);
				throw new XMLParserException();
			}
		} catch (BackEndException e) {
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw e;
		}
		catch (Throwable e) {
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		
		return replyVO;
	}
	
	
	
	/**
	 * 
	 * @param PackageConversionReqVO
	 * @return
	 */
	public String generatePkgConversionRequestXML(PackageConversionReqVO requestVO) {
		String method = "generatePkgConversionRequestXML";
		String xmlRequest = null;

		try {
			//ResourceEnvironmentProviderService resourceEnvironmentProviderService = ResourceEnvironmentProviderService.getInstance();
			String requestorChannelId = REQUESTER_CHANNEL_ID;
			/*if (resourceEnvironmentProviderService != null) {
				Object value = resourceEnvironmentProviderService.getPropertyValue("REQUESTOR_CHANNEL_ID");
				if (value != null) {
					requestorChannelId = (String) value;
				} 
			}*/
			executionContext.log(Level.INFO, "requestorChannelId ["+requestorChannelId+"]", className, method);

			
			Order orderObj = new Order();
			
				MsgRqHeader header = new MsgRqHeader();
					header.setOrderType(FUNC_ID_PKGCONV_SMOOTH_MIGIRATION);
					header.setSourceID(requestorChannelId);
					if(FormatterUtility.isNotEmpty(requestVO.getUserId()))
						header.setSourceUsrId(requestVO.getUserId());
					else
						header.setSourceUsrId(REQUESTER_USER_ID_ANONYMOUS);
					header.setOrderID(requestVO.getOrderId());
					header.setOrderDate(PkgMgmtUtil.FormateDateT(new Date()));
					header.setDestinationID(DEST_ID);
					header.setDestinationFuncId(DEST_FUNC_ID);
					header.setCallerServiceID(REQUESTER_CHANNEL_ID);
						AsyncResponse asyncResponseObj = new AsyncResponse();
							asyncResponseObj.setCorrelationId("");
							asyncResponseObj.setReplyURI("");
							asyncResponseObj.setReplyToQMgr("");
					header.setAsyncResponse(asyncResponseObj);		
					header.setTimestamp(PkgMgmtUtil.FormateDateT(new Date()));
					header.setMsgType(MSG_TYPE_REQUEST);
					header.setMsgVersion(MSGVERSION_1_0);
					header.setEchoData("");

				orderObj.setMsgRqHeader(header);
				
					CustomerOrder customerOrderObj = new CustomerOrder();
					
						CustomerOrderProperties custOrderProps = new CustomerOrderProperties();
							CustomerOrderProperty custOrderProp = new CustomerOrderProperty();
								custOrderProp.setPropertyName(CHANGE_TYPE);
								custOrderProp.setPropertyValue("");
							custOrderProps.setCustomerOrderProperty(custOrderProp);		
					customerOrderObj.setCustomerOrderProperties(custOrderProps);
	
					
					SubscriberOrderItem subscriberOrderItemObj = new SubscriberOrderItem();
						subscriberOrderItemObj.setAction(ACTION_MODIFY);
						
							NumberResourceOrderItem numResourceOrderItemObj = new NumberResourceOrderItem(); //NumberResourceOrderItem
								numResourceOrderItemObj.setAction(ACTION_NO);
									NumberResource numberResourceObj = new NumberResource();
										numberResourceObj.setServiceNumber(requestVO.getMsisdn());
								numResourceOrderItemObj.setNumberResource(numberResourceObj);

						subscriberOrderItemObj.setNumberResourceOrderItem(numResourceOrderItemObj);
						
							ProductInstanceOrderItem prodInstanceOrderItemObj = new ProductInstanceOrderItem(); //ProductInstanceOrderItem Source
								prodInstanceOrderItemObj.setAction(ACTION_DELETE);
								
								ProductOrderItem productOrderItemObj = new ProductOrderItem();
									productOrderItemObj.setAction(ACTION_DELETE);
									
									Product productObj = new Product();
										productObj.setProductID(requestVO.getSrcPkgId());
										productObj.setProdyctName(requestVO.getSrcPkgTechName());
										productObj.setPrimaryFlag(Y);
										
											ProductProperties productPropertiesObj = new ProductProperties();
												ProductProperty productProperty = new ProductProperty();
													productProperty.setPropertyName(PRODUCT_CATEGORY);
													productProperty.setPropertyValue(PRODUCT_CATEGORY_VALUE);
												
												productPropertiesObj.setProductProperty(productProperty);
											productObj.setProductProperties(productPropertiesObj);
										
										productOrderItemObj.setProduct(productObj);	
										
								prodInstanceOrderItemObj.setProductOrderItem(productOrderItemObj);
								
						subscriberOrderItemObj.getProductInstanceOrderItem().add(prodInstanceOrderItemObj);
							
								prodInstanceOrderItemObj = new ProductInstanceOrderItem(); //ProductInstanceOrderItem Destination
									prodInstanceOrderItemObj.setAction(ACTION_ADD);
								
									productOrderItemObj = new ProductOrderItem();
										productOrderItemObj.setAction(ACTION_ADD);
									
											productObj = new Product();
												productObj.setProductID(requestVO.getDestPkgId());
												productObj.setProdyctName(requestVO.getDestPkgTechName());
												productObj.setPrimaryFlag(Y);
										
												productPropertiesObj = new ProductProperties();
													productProperty = new ProductProperty();
														productProperty.setPropertyName(PRODUCT_CATEGORY);
														productProperty.setPropertyValue(PRODUCT_CATEGORY_VALUE);
												
												productPropertiesObj.setProductProperty(productProperty);
											productObj.setProductProperties(productPropertiesObj);
										
										productOrderItemObj.setProduct(productObj);	
										
								prodInstanceOrderItemObj.setProductOrderItem(productOrderItemObj);
							
						subscriberOrderItemObj.getProductInstanceOrderItem().add(prodInstanceOrderItemObj);
					
					customerOrderObj.setSubscriberOrderItem(subscriberOrderItemObj);	
						
					AccountOrderItem accountOrderItemObj = new AccountOrderItem(); //AccountOrderItem SOURCE
						accountOrderItemObj.setAction(ACTION_DELETE);
						Account accountObj = new Account();
							accountObj.setPlanType(requestVO.getSrcPkgPlanType());
					accountOrderItemObj.setAccount(accountObj);

					customerOrderObj.getAccountOrderItem().add(accountOrderItemObj);
					
					accountOrderItemObj = new AccountOrderItem(); //AccountOrderItem DESTINATION
						accountOrderItemObj.setAction(ACTION_ADD);
						accountObj = new Account();
							accountObj.setPlanType(requestVO.getDestPkgPlanType());
					accountOrderItemObj.setAccount(accountObj);
	
					customerOrderObj.getAccountOrderItem().add(accountOrderItemObj);	
					
					
				orderObj.setCustomerOrder(customerOrderObj);
				
			xmlRequest = JAXBUtilities.getInstance().marshal(orderObj);
			
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception ::>"+e.getMessage(), className, method, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		
		return xmlRequest;
	}

	/**
	 * 
	 * @param replyXML
	 * @return
	 */
	public PackageConversionRepVO parsePkgConvResponseXML(String replyXML) {
		
		String method = "parsePkgConvResponseXML";
		PackageConversionRepVO replyVO = new PackageConversionRepVO();
		
		try {
			if(FormatterUtility.isNotEmpty(replyXML)) {
				sa.com.mobily.eportal.pack.mgmt.conv.response.Order orderObj = (sa.com.mobily.eportal.pack.mgmt.conv.response.Order) JAXBUtilities.getInstance().unmarshal(sa.com.mobily.eportal.pack.mgmt.conv.response.Order.class, replyXML);
				if (orderObj != null && orderObj.getMsgRsHeader() != null) {
					replyVO.setStatusCode(orderObj.getMsgRsHeader().getStatusCode());
					replyVO.setIsAcknowledgement(orderObj.getMsgRsHeader().getIsAcknowledgement());
				}
			} else {
				executionContext.log(Level.SEVERE, "XML Reply is null.", className, method, null);
				throw new XMLParserException();
			}
		} catch (BackEndException e) {
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw e;
		}
		catch (Throwable e) {
			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		
		return replyVO;
	}
	
}