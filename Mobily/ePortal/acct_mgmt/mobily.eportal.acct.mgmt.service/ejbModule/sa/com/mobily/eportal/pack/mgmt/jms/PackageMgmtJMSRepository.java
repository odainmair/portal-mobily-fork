/**
 * 
 */
package sa.com.mobily.eportal.pack.mgmt.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.pack.mgmt.constants.ConstantsIfc;
import sa.com.mobily.eportal.pack.mgmt.util.PkgConfig;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageConversionRepVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageConversionReqVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryRequestVO;
import sa.com.mobily.eportal.pack.mgmt.vo.PackageHistoryVO;
import sa.com.mobily.eportal.pack.mgmt.xml.PackageMgmtXMLHandler;
import sa.com.mobily.eportal.util.TransactionIdGenerator;

/**
 * @author n.gundluru.mit
 *
 */
public class PackageMgmtJMSRepository {

	private static String className = PackageMgmtJMSRepository.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.ACCT_MGMT_PACKAGE_MGMT);


	/**
	 * 
	 * @param requestVO
	 * @return
	 */
	public PackageHistoryVO getPackageChangeHistory(PackageHistoryRequestVO requestVO) {
		PackageHistoryVO replyVO = null;
		String method = "getPackageChangeHistory";
		
		boolean isTesting = requestVO.isTesting();
		
		String requestQueueName = PkgConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_PACKAGE_HISTORY);
		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", className, method);
		String replyQueueName = PkgConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_PACKAGE_HISTORY);
		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", className, method);

		requestVO.setTransactionId(TransactionIdGenerator.generateTransactionID(ConstantsIfc.CHANNELTRANSID));
		
		//Audit
			MQAuditVO mqAuditVO=new MQAuditVO();
				mqAuditVO.setRequestQueue(requestQueueName);
				mqAuditVO.setReplyQueue(replyQueueName);
				mqAuditVO.setFunctionName(ConstantsIfc.FUNC_ID_PKGCONV_HISTORY_INQUIRY);
				mqAuditVO.setUserName(requestVO.getUserId());
				
			String strXMLRequest=null;
			String strXMLReply = null;
		
			try {
				strXMLRequest = new PackageMgmtXMLHandler().generateGetPkgHistoryRequestXML(requestVO);
				executionContext.audit(Level.INFO, "XML Request sent to MQ : " + strXMLRequest, className, method);
				mqAuditVO.setMessage(strXMLRequest);
				
				if (isTesting) {
					strXMLReply = "<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>PKGCONV_HISTORY_INQUIRY</FuncId><SecurityKey>0000</SecurityKey><MsgVersion>0001</MsgVersion>"+
									"<ChannelTransId>SR_15070062231</ChannelTransId><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20190501091747</SrDate><RequestorUserId>QAUSER</RequestorUserId>"+
									"<RequestorLanguage>E</RequestorLanguage><OverwriteOpenOrder>Y</OverwriteOpenOrder><Chargeable>N</Chargeable><ChargingMode>Payment</ChargingMode>"+
									"<ChargeAmount>0</ChargeAmount><OrderCreationDate>20190501091747</OrderCreationDate><OrderSubmitDate>20190501091747</OrderSubmitDate></SR_HEADER_REPLY>"+
									"<MSISDN>966563027592</MSISDN><SourcePackageName>Postpaid 100</SourcePackageName><DestinationPackageName>Postpaid 300</DestinationPackageName>"+
									"<PackageConversionCount>6</PackageConversionCount><ErrorCode>0</ErrorCode><ErrorMsg>Success/Failure</ErrorMsg></MOBILY_BSL_SR_REPLY>";
				} else {
					strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
				}
	
				executionContext.log(Level.INFO, "XML Reply received from MQ : " + strXMLReply, className, method, null);
				
				mqAuditVO.setReply(strXMLReply);

				replyVO = new PackageMgmtXMLHandler().parseGetPkgHistoryResponseXML(strXMLReply);
				
				if(replyVO != null){
					mqAuditVO.setServiceError(replyVO.getErrorCode());
					mqAuditVO.setErrorMessage(replyVO.getErrorMsg());
				}
			} catch (Exception e) {
				executionContext.audit(Level.SEVERE, "Exception >>>>>"+e.getMessage(), className, method);
				throw new MobilyApplicationException("Exception while doing package conversion history Inquiry >"+e.getMessage(), e);
				
			} finally{
	     	   try{
	              	MQUtility.saveMQAudit(mqAuditVO);
		   	   }catch(Exception e){
		   		   executionContext.audit(Level.SEVERE, "auditing > Exception > "+e.getMessage(), className, method);   
		   	   }
			}

			if(replyVO != null){
				if(FormatterUtility.isNotEmpty(replyVO.getErrorCode()) && Integer.parseInt(replyVO.getErrorCode()) != 0) {
					String errorCode = FormatterUtility.checkNull("" + replyVO.getErrorCode());
	
					executionContext.audit(Level.INFO, "Exception :: Error code >>>>>"+errorCode+"<<", className, method);
					throw new BackEndException(0, errorCode, replyVO.getErrorMsg());
				}
			}else{
				executionContext.audit(Level.INFO, "REPLY VO is NULL.....!", className, method);
				throw new MobilyApplicationException("Reply Vo Is Null.....!");
			}
			
		return replyVO;
	}

	
	/**
	 * 
	 * @param requestVO
	 */
	public void doPackageConversion(PackageConversionReqVO requestVO) {
		PackageConversionRepVO replyVO = null;
		String method = "doPackageConversion";
		
		boolean isTesting = requestVO.isTesting();
		
		String requestQueueName = PkgConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_PACKAGE_CONVERSION);
		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", className, method);
		String replyQueueName = PkgConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_PACKAGE_CONVERSION);
		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", className, method);

		requestVO.setOrderId(TransactionIdGenerator.generateTransactionID(ConstantsIfc.ORDER_ID));
		
		//Audit
			MQAuditVO mqAuditVO=new MQAuditVO();
				mqAuditVO.setRequestQueue(requestQueueName);
				mqAuditVO.setReplyQueue(replyQueueName);
				mqAuditVO.setFunctionName(ConstantsIfc.FUNC_ID_PKGCONV_SMOOTH_MIGIRATION);
				mqAuditVO.setUserName(requestVO.getUserId());
				
			String strXMLRequest=null;
			String strXMLReply = null;
		
			try {
				strXMLRequest = new PackageMgmtXMLHandler().generatePkgConversionRequestXML(requestVO);
				executionContext.audit(Level.INFO, "XML Request sent to MQ : " + strXMLRequest, className, method);
				mqAuditVO.setMessage(strXMLRequest);
				
				if (isTesting) {
					strXMLReply = "<Order><MsgRsHeader><OrderType>SMOOTH_MIGIRATION</OrderType><SourceUsrId/><OrderID>1-BK3VAMQ</OrderID><OrderDate>2018-07-12T14:37:32</OrderDate>"+
							"<DestinationID>OM</DestinationID><DestinationFuncId>OrchestratorSyncAsync</DestinationFuncId><CallerServiceID>SIEBEL</CallerServiceID>"+
							"<AsyncResponse><CorrelationId>1-BK3VAMQ</CorrelationId><ReplyURI>TEST.OUT</ReplyURI><ReplyToQMgr></ReplyToQMgr>"+
							"</AsyncResponse><Timestamp>2018-07-12T14:37:32</Timestamp><MsgType>REQUEST</MsgType><MsgVersion>1.0</MsgVersion><EchoData>"+
							"<ActivityID>1-BK3VAMP</ActivityID><ServiceAccountNumber>100125156825538</ServiceAccountNumber><MessageHeader><OrderType>SMOOTH_MIGIRATION</OrderType>"+
							"<SourceUsrId/><OrderID>1-BK3VAMQ</OrderID><OrderDate>2018-07-12T14:37:32</OrderDate><DestinationID>CHANNELS</DestinationID>"+
							"<DestinationFuncId>CHANNELS_SMOOTH_MIGIRATION</DestinationFuncId><CallerServiceID>SIEBEL</CallerServiceID><AsyncResponse>"+
							"<CorrelationId>1-BK3VAMQ</CorrelationId><ReplyURI/><ReplyToQMgr/></AsyncResponse><Timestamp>2018-07-12T14:37:32</Timestamp>"+
							"<MsgType>REQUEST</MsgType><MsgVersion>1.0</MsgVersion><EchoData><ActivityID>1-BK3VAMP</ActivityID>"+
							"<ServiceAccountNumber>100125156825538</ServiceAccountNumber></EchoData></MessageHeader><MQMD>"+
							"<SourceQueue>MOBILY.FUNC.TS.REQ</SourceQueue><Transactional>true</Transactional><Encoding>546</Encoding><CodedCharSetId>1208</CodedCharSetId>"+
							"<Format>MQSTR</Format><Version>2</Version><Report>0</Report><MsgType>8</MsgType><Expiry>-1</Expiry><Feedback>0</Feedback><Priority>0</Priority>"+
							"<Persistence>1</Persistence><MsgId>414d51204545415832303649202020205b1f264123a5390b</MsgId>"+
							"<CorrelId>414d51204545415832303649202020205b1f264123a5390b</CorrelId><BackoutCount>0</BackoutCount><ReplyToQ>MOBILY.FUNC.LOAD.DATA.UPDATE.RES</ReplyToQ>"+
							"<ReplyToQMgr>EEAX206I</ReplyToQMgr><UserIdentifier>iibadm</UserIdentifier>"+
							"<AccountingToken>0130000000000000000000000000000000000000000000000000000000000006</AccountingToken><ApplIdentityData></ApplIdentityData>"+
							"<PutApplType>6</PutApplType><PutApplName>WebSphere Datapower MQClient</PutApplName><PutDate>2018-07-12</PutDate>"+
							"<PutTime>11:53:02.230</PutTime><ApplOriginData></ApplOriginData><GroupId>000000000000000000000000000000000000000000000000</GroupId>"+
							"<MsgSeqNumber>1</MsgSeqNumber><Offset>0</Offset><MsgFlags>0</MsgFlags><OriginalLength>-1</OriginalLength></MQMD>"+
							"</EchoData><IsAcknowledgement>Y</IsAcknowledgement><StatusCode>9981</StatusCode></MsgRsHeader></Order>";
				} else {
					strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
				}
	
				executionContext.log(Level.INFO, "XML Reply received from MQ : " + strXMLReply, className, method, null);
				
				mqAuditVO.setReply(strXMLReply);

				replyVO = new PackageMgmtXMLHandler().parsePkgConvResponseXML(strXMLReply);
				
				if(replyVO != null){
					mqAuditVO.setServiceError(replyVO.getStatusCode());
					mqAuditVO.setErrorMessage("");
				}
			} catch (Exception e) {
				executionContext.audit(Level.SEVERE, "Exception >>>>>"+e.getMessage(), className, method);
				throw new MobilyApplicationException("Exception while doing package conversion >"+e.getMessage(), e);
				
			} finally{
	     	   try{
	              	MQUtility.saveMQAudit(mqAuditVO);
		   	   }catch(Exception e){
		   		   executionContext.audit(Level.SEVERE, "auditing > Exception > "+e.getMessage(), className, method);   
		   	   }
			}

			if(replyVO != null){
				String errorCode = FormatterUtility.checkNull("" + replyVO.getStatusCode());
				executionContext.audit(Level.INFO, "Error code >>>>>"+errorCode+"<<", className, method);
				
				if(!ConstantsIfc.PKG_CONV_SUCCESS.equalsIgnoreCase(errorCode)) {
					throw new BackEndException(0, errorCode, "");
				}
			}else{
				executionContext.audit(Level.INFO, "REPLY VO is NULL.....!", className, method);
				throw new MobilyApplicationException("Reply Vo Is Null.....!");
			}
	}
}
