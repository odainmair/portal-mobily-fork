//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.05.29 at 03:01:58 PM IST 
//


package sa.com.mobily.eportal.pack.mgmt.conv.request;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}Action"/>
 *         &lt;element ref="{}NumberResourceOrderItem"/>
 *         &lt;element ref="{}ProductInstanceOrderItem" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "action",
    "numberResourceOrderItem",
    "productInstanceOrderItem"
})
@XmlRootElement(name = "SubscriberOrderItem")
public class SubscriberOrderItem {

    @XmlElement(name = "Action", required = true)
    protected String action;
    @XmlElement(name = "NumberResourceOrderItem", required = true)
    protected NumberResourceOrderItem numberResourceOrderItem;
    @XmlElement(name = "ProductInstanceOrderItem", required = true)
    protected List<ProductInstanceOrderItem> productInstanceOrderItem;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the numberResourceOrderItem property.
     * 
     * @return
     *     possible object is
     *     {@link NumberResourceOrderItem }
     *     
     */
    public NumberResourceOrderItem getNumberResourceOrderItem() {
        return numberResourceOrderItem;
    }

    /**
     * Sets the value of the numberResourceOrderItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberResourceOrderItem }
     *     
     */
    public void setNumberResourceOrderItem(NumberResourceOrderItem value) {
        this.numberResourceOrderItem = value;
    }

    /**
     * Gets the value of the productInstanceOrderItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productInstanceOrderItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductInstanceOrderItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductInstanceOrderItem }
     * 
     * 
     */
    public List<ProductInstanceOrderItem> getProductInstanceOrderItem() {
        if (productInstanceOrderItem == null) {
            productInstanceOrderItem = new ArrayList<ProductInstanceOrderItem>();
        }
        return this.productInstanceOrderItem;
    }

}
