package sa.com.mobily.eportal.mycontact.dao;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.common.dao.constants.DAOConstants;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.mycontact.service.MyContactsServiceBean;
import sa.com.mobily.eportal.mycontact.vo.MyContactsVO;

/**
 * This DAO class is responsible for DB operations on CONTACTS table
 * <p> 
 * it contains methods for retrieve, add, delete and update.
 *  
 * @author Yousef Alkhalaileh
 */
public class MyContactsDAO extends AbstractDBDAO<MyContactsVO>{
	
	private static String clazz = MyContactsDAO.class.getName();
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(AcctMgmtLoggerConstantsIfc.MY_CONTACTS_SERVICE_LOGGER_NAME);

	public static final String INSERT_CONTACT = "INSERT INTO CONTACTS(ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID) "+ 
    "VALUES(USER_CONTACTS_SEQ.nextval, ?, ?, ?, ?, SYSDATE, ?)";
	
	public static final String INSERT_CONTACT_ADD_SUBSCRIPTON = "INSERT INTO CONTACTS(ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID, STATUS) "+ 
		    "VALUES(USER_CONTACTS_SEQ.nextval, ?, ?, ?, ?, SYSDATE, ?, 0)";
	
	public static final String UPDATE_CONTACT_ADD_CONFIRM = "UPDATE CONTACTS SET STATUS=1 WHERE USER_ACCT_ID=? AND PHONE=?";
	
	public static final String UPDATE_CONTACT = "UPDATE CONTACTS SET FIRST_NAME=?, LAST_NAME=?, PHONE=?, EMAIL=? where ID = ?";
	public static final String DELETE_CONTACT = "DELETE FROM CONTACTS WHERE ID in ";
	public static final String FIND_ALL_BY_ACCOUNT_ID = "SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE STATUS = 1 AND USER_ACCT_ID = ? order by FIRST_NAME";
	public static final String FIND_ALL_BY_PHONE = "SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE (PHONE LIKE '?%' OR PHONE LIKE '+?%') AND USER_ACCT_ID = ? order by FIRST_NAME";
	public static final String UPDATE_CONTACT_ACCOUNT_ID = "UPDATE CONTACTS SET FIRST_NAME=nvl(?,FIRST_NAME), LAST_NAME=nvl(?,LAST_NAME), PHONE=nvl(?,PHONE), EMAIL=nvl(?,EMAIL) where USER_ACCT_ID = ? and PHONE=?";
	
    
	private static MyContactsDAO instance = new MyContactsDAO();
	
	protected MyContactsDAO() {
		super(DataSources.DEFAULT);
	}
	
	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>MyContactsDAO</code>
	 * @author Yousef Alkhalaileh
	 */
	public static MyContactsDAO getInstance() {
		return instance;
	}
	
	/**
	 * This method saves new contact for a specific account in the Contacts table
	 * 
	 * @param request <code>MyContactsVO</code> contact to be saved
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Yousef Alkhalaileh
	 */
	public int saveContact(MyContactsVO contact){
		return update(INSERT_CONTACT, contact.getFirstName(), contact.getLastName(), contact.getPhone(), contact.getEmail(), contact.getUserAccountId());
	}
	
	public int saveContactAddSubscription(MyContactsVO contact){
		return update(INSERT_CONTACT_ADD_SUBSCRIPTON, contact.getFirstName(), contact.getLastName(), contact.getPhone(), contact.getEmail(), contact.getUserAccountId());
	}
	
	/**
	 * This method updates a contact for a specific account in the Contacts table
	 * 
	 * @param request <code>MyContactsVO</code> contact to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Yousef Alkhalaileh
	 */
	public int updateContact(MyContactsVO contact){
		return update(UPDATE_CONTACT, contact.getFirstName(), contact.getLastName(), contact.getPhone(), contact.getEmail(), contact.getId());
	}
	
	public int updateContactAddConfirm(MyContactsVO contact){
		return update(UPDATE_CONTACT_ADD_CONFIRM, contact.getUserAccountId(), contact.getPhone());
	}
	
	/**
	 * This method deletes a list contacts for a specific account id
	 * 
	 * @param request <code>List<Long> ids</code> contacts ids to be deleted
	 * @param  rootAccountId account number under which the id's needs to be deleted 
	 * @return <code>int</code> number of records affected
	 * @author Yousef Alkhalaileh
	 */
	public int deleteContactById(List<Long> ids, Long rootAccountId){
		String idsString = null;
		for (Iterator<Long> iterator = ids.iterator(); iterator.hasNext();) {
			if(null == idsString)
				idsString = "(" + iterator.next().longValue();
			else
				idsString += ", " + iterator.next().longValue();
		}
		idsString += ") AND USER_ACCT_ID ="+rootAccountId;
		
		executionContext.log(Level.INFO, "Deleting query ["+DELETE_CONTACT + idsString+"]", clazz, "deleteContactById", null);
		
		return update(DELETE_CONTACT + idsString);
	}
	
	/**
	 * This method returns a list contacts for a specific account id
	 * 
	 * @param request <code>Long id</code> account id
	 * @return <code>ArrayList<MyContactsVO></code> a list of contacts
	 * @author Yousef Alkhalaileh
	 */
	public ArrayList<MyContactsVO> findContactsByAcountId(Long id) {
		 return (ArrayList<MyContactsVO>) query(FIND_ALL_BY_ACCOUNT_ID, id);
	}
	
	/**
	 * This method returns a list contacts which are Starting with the given Phone number and user acct id
	 * 
	 * @param request <code>Phone number</code> account id
	 * @return <code>ArrayList<MyContactsVO></code> a list of contacts
	 * @author Nagendra B Gundluru - MIT
	 */
	public ArrayList<MyContactsVO> findContactsByPhone(String phoneNumber, long accountId) {
		 return (ArrayList<MyContactsVO>) query("SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE (PHONE LIKE '"+phoneNumber+"%' OR PHONE LIKE '+"+phoneNumber+"%') AND USER_ACCT_ID = "+accountId+" order by FIRST_NAME");
	}
	
	/**
	 * This method returns a list contacts for a specific account id
	 * 
	 * @param request <code>Long id</code> account id
	 * @return <code>MyContactsVO</code> a list of contacts
	 * @author Yousef Alkhalaileh
	 */
	@Override
	protected MyContactsVO mapDTO(ResultSet rs) throws SQLException {
		MyContactsVO contact = new MyContactsVO();
		contact.setId(rs.getLong(DAOConstants.Contacts.ID));
		contact.setFirstName(rs.getString(DAOConstants.Contacts.FIRST_NAME));
		contact.setLastName(rs.getString(DAOConstants.Contacts.LAST_NAME));
		contact.setPhone(rs.getString(DAOConstants.Contacts.PHONE));
		contact.setEmail(rs.getString(DAOConstants.Contacts.EMAIL));
		contact.setCreationDate(rs.getTimestamp(DAOConstants.Contacts.CREATION_DATE));
		contact.setUserAccountId(rs.getLong(DAOConstants.Contacts.USER_ACCT_ID));
		return contact;
	}
	
	/**
	 * This method updates contact on the basis of account Id and phone number
	 * 
	 * @param request <code>MyContactsVO</code> contact to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Umair Saleem
	 * @Dated 28-4-2019
	 */
	public int updateContactsByAccountId(MyContactsVO contact){
		return update(UPDATE_CONTACT_ACCOUNT_ID, contact.getFirstName(), contact.getLastName(), contact.getPhone(), contact.getEmail(), contact.getUserAccountId(), contact.getPhone());
	}
}
