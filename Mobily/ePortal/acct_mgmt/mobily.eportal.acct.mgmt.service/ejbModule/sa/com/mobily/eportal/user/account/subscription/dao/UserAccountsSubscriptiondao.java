package sa.com.mobily.eportal.user.account.subscription.dao;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.accounts.subscription.vo.UserAccountsSubscriptionsVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.customerinfo.util.SubscriptionTypeUtil;

public class UserAccountsSubscriptiondao extends AbstractDBDAO<UserAccountsSubscriptionsVO>
{
	
	private static UserAccountsSubscriptiondao INSTANCE = new UserAccountsSubscriptiondao();
	
	
	private static final String COL_USER_NAME = "USER_NAME";
	private static final String COL_ROLE_ID = "ROLE_ID";
	private static final String COL_SUBSCRIPTION_ID = "SUBSCRIPTION_ID";
	private static final String COL_USER_ACCT_ID = "USER_ACCT_ID";
	private static final String COL_IS_DEFAULT = "IS_DEFAULT";
	private static final String COL_SUBSCRIPTION_TYPE = "SUBSCRIPTION_TYPE";
	private static final String COL_MSISDN = "MSISDN";
	private static final String COL_SERVICE_ACCT_NUMBER = "SERVICE_ACCT_NUMBER";
	private static final String COL_CUSTOMER_TYPE = "CUSTOMER_TYPE";
	private static final String COL_PACKAGE_ID = "PACKAGE_ID";
	private static final String COL_CREATED_DATE = "CREATED_DATE";
	private static final String COL_IS_ACTIVE = "IS_ACTIVE";
	private static final String COL_CC_STATUS = "CC_STATUS";
	private static final String COL_LAST_UPDATED_TIME = "LAST_UPDATED_TIME";
	private static final String COL_IQAMA = "IQAMA";
	private static final String COL_SUB_SUBSCRIPTION_TYPE = "SUB_SUBSCRIPTION_TYPE";
	private static final String US_FIND_ACTIVE_USER_ACCOUNTS_SUBSCRIPTION_BY_ACCOUNT_ID = " SELECT USER_NAME,ROLE_ID,SUBSCRIPTION_ID,A.USER_ACCT_ID,IS_DEFAULT,SUBSCRIPTION_TYPE,MSISDN,SERVICE_ACCT_NUMBER,CUSTOMER_TYPE, "
	+ " PACKAGE_ID,A.CREATED_DATE,IS_ACTIVE,CC_STATUS,LAST_UPDATED_TIME,SUBSCRIPTION_NAME,SUB_SUBSCRIPTION_TYPE,A.IQAMA "
	+ " FROM USER_SUBSCRIPTION A INNER JOIN USER_ACCOUNTS B ON A.USER_ACCT_ID = B.USER_ACCT_ID "
	+ " WHERE IS_ACTIVE = 'Y' AND A.USER_ACCT_ID = ?";

	
	
	protected UserAccountsSubscriptiondao(){
		super(DataSources.DEFAULT);
	}
	
	public static UserAccountsSubscriptiondao getInstance(){
		return INSTANCE;
	}
	
	public List<UserAccountsSubscriptionsVO> getUserAccountSubscriptionsByAccountId(Long userAcctId){
		return query(US_FIND_ACTIVE_USER_ACCOUNTS_SUBSCRIPTION_BY_ACCOUNT_ID,userAcctId);
	}
	
	@Override
	protected UserAccountsSubscriptionsVO mapDTO(ResultSet rs) throws SQLException{
		UserAccountsSubscriptionsVO refactoredUserAccountSubscriptionsVO = new UserAccountsSubscriptionsVO();
		refactoredUserAccountSubscriptionsVO.setUserName(rs.getString(COL_USER_NAME));
		refactoredUserAccountSubscriptionsVO.setRoleId(rs.getString(COL_ROLE_ID));
		refactoredUserAccountSubscriptionsVO.setCcStatus(rs.getLong(COL_CC_STATUS));
		refactoredUserAccountSubscriptionsVO.setSubscriptionCreatedDate(rs.getTimestamp(COL_CREATED_DATE));
		refactoredUserAccountSubscriptionsVO.setCustomerType(rs.getString(COL_CUSTOMER_TYPE));
		refactoredUserAccountSubscriptionsVO.setIsActive(rs.getString(COL_IS_ACTIVE));
		refactoredUserAccountSubscriptionsVO.setIsDefault(rs.getString(COL_IS_DEFAULT));
		refactoredUserAccountSubscriptionsVO.setLastUpdatedTime(rs.getTimestamp(COL_LAST_UPDATED_TIME));
		refactoredUserAccountSubscriptionsVO.setLineType(rs.getInt(COL_SUB_SUBSCRIPTION_TYPE));
		refactoredUserAccountSubscriptionsVO.setMsidsn(rs.getString(COL_MSISDN));
		refactoredUserAccountSubscriptionsVO.setPackageId(rs.getString(COL_PACKAGE_ID));
		refactoredUserAccountSubscriptionsVO.setServiceAccountNumber(rs.getString(COL_SERVICE_ACCT_NUMBER));
		refactoredUserAccountSubscriptionsVO.setSubscriptionId(rs.getLong(COL_SUBSCRIPTION_ID));
		refactoredUserAccountSubscriptionsVO.setSubscriptionType(rs.getInt(COL_SUBSCRIPTION_TYPE));
		refactoredUserAccountSubscriptionsVO.setSubscriptionName(SubscriptionTypeUtil.getSubSubScriptionName(refactoredUserAccountSubscriptionsVO.getSubscriptionType(), refactoredUserAccountSubscriptionsVO.getLineType()));
		refactoredUserAccountSubscriptionsVO.setSubscriptionIqama(rs.getString(COL_IQAMA));
		refactoredUserAccountSubscriptionsVO.setUserAccountId(rs.getLong(COL_USER_ACCT_ID));
		return refactoredUserAccountSubscriptionsVO;
	}

}
