package sa.com.mobily.eportal.authority.management.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.authority.mgmt.constants.AuthorityMgmtConstants;
import sa.com.mobily.eportal.authority.mgmt.vo.SubAdminVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class SubAdminAccountsDAO extends AbstractDBDAO<SubAdminVO>  implements AcctMgmtLoggerConstantsIfc
{

	private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(ACCT_MGMT_AUTHORITY_MGMT);
	
	private String className = SubAdminAccountsDAO.class.getName();
			
	private static SubAdminAccountsDAO instance = new SubAdminAccountsDAO();
	
	
	private static final String USER_ACCOUNT_ADD = 
            "INSERT INTO USER_ACCOUNTS " +
            //"(USER_ACCT_ID,USER_NAME,CREATED_DATE,EMAIL,STATUS,FIRST_NAME,LAST_NAME,CUST_SEGMENT,BIRTHDATE,NATIONALITY,GENDER,REGISTRATION_DATE) VALUES " +
            // added by MHASSAN
            "(USER_ACCT_ID,USER_NAME,CREATED_DATE,STATUS,FIRST_NAME,CUST_SEGMENT,ACTIVATION_DATE,REGISTRATION_SOURCE_ID,ROLE_ID,REGISTRATION_DATE) VALUES " +
            
            "(?,?, CURRENT_TIMESTAMP, ?, ?,?,CURRENT_TIMESTAMP,?,?,CURRENT_TIMESTAMP)";
    
	 private static final String GET_SEQ_USER_ACCT_ID =
	    		"SELECT USER_ACCOUNT_SEQ.NEXTVAL userAcctId FROM DUAL";
	 
	protected SubAdminAccountsDAO() {
	     super(DataSources.DEFAULT);
	}
	  
	public static synchronized SubAdminAccountsDAO getInstance() {
	    if (instance == null) {
	        instance = new SubAdminAccountsDAO();
	    }
	    return instance;
	}
	
	
	public SubAdminVO getUserAcctIdSeq() {
			
	 	SubAdminVO dto = null;
    	List<SubAdminVO> userInformationVOList = query(GET_SEQ_USER_ACCT_ID);
		
    	if(userInformationVOList != null && userInformationVOList.size() > 0) {
    		dto = userInformationVOList.get(0);
    	} 
    	executionContext.audit(Level.INFO, "SubAdminAccountsDAO >> getUserAcctIdSeq>> dto::"+dto, className, "getUserAcctIdSeq");
        return dto;
	 }
	
	public int addUserAccounts(SubAdminVO dto) {
		executionContext.audit(Level.INFO, "SubAdminAccountsDAO >> userName::"+dto.getUserName(), className, "addUserAccounts");
		return update(USER_ACCOUNT_ADD, dto.getUserAcctId(), dto.getUserName(), 
				AuthorityMgmtConstants.STATUS_ACTIVE, dto.getName(),
				AuthorityMgmtConstants.CUSTOMER_SEGMENT_BUSINESS,AuthorityMgmtConstants.REGISTRATION_SOURCE_ID,AuthorityMgmtConstants.ROLE_ID);
    }
	
	 
	@Override
	protected SubAdminVO mapDTO(ResultSet rs) throws SQLException
	{
		SubAdminVO adminVO = new SubAdminVO();
		if (rs.getMetaData().getColumnCount() == 1) {
			adminVO.setUserAcctId(rs.getString("userAcctId"));
    	}
		return adminVO;
	}

}
