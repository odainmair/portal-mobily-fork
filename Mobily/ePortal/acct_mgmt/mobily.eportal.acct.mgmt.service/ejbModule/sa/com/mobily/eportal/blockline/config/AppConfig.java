package sa.com.mobily.eportal.blockline.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfig
{

	protected static final String propertyFileName = "config/blockline.properties";

	protected static AppConfig config = null;

	protected Properties properties;

	/**
	 * @MethodName: AppConfig
	 * <p>
	 * constructor to create an instance of properties object to reference
	 * 
	 * @param String msisdn
	 * 
	 * @return boolean 
	 * 
	 * @author Abu Sharaf
	 */
	protected AppConfig()
	{
		properties = new Properties();
		ClassLoader sc = Thread.currentThread().getContextClassLoader();
		InputStream stream = sc.getResourceAsStream(propertyFileName);

		if (null == stream)
		{
			throw new IllegalArgumentException("Could not load " + propertyFileName + ".Please make sure that it is in CLASSPATH.");
		}

		try
		{
			properties.load(stream);
		}
		catch (IOException e)
		{
			IllegalStateException ex = new IllegalStateException("An error occurred when reading from the input stream");
			ex.initCause(e);
			throw ex;
		}
		finally
		{
			try
			{
				stream.close();
			}
			catch (IOException e)
			{
				IllegalStateException ex = new IllegalStateException("An I/O error occured while closing the stream");
				ex.initCause(e);
				throw ex;
			}
		}
	}

	public static AppConfig getInstance()
	{
		if (null == config)
			config = new AppConfig();
		return config;
	}

	public String get(String key)
	{
		if (null == properties || null == key)
			return null;
		return (String) properties.get(key);
	}
}