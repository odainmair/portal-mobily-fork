package sa.com.mobily.eportal.activenumber.service.mock;

public class SubscriptionTypes
{

	private int subscriptionTypeId;

	private String subscriptionType;

	private String subscriptionKey;

	public int getSubscriptionTypeId()
	{
		return subscriptionTypeId;
	}

	public void setSubscriptionTypeId(int subscriptionTypeId)
	{
		this.subscriptionTypeId = subscriptionTypeId;
	}

	public String getSubscriptionType()
	{
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType)
	{
		this.subscriptionType = subscriptionType;
	}

	public String getSubscriptionKey()
	{
		return subscriptionKey;
	}

	public void setSubscriptionKey(String subscriptionKey)
	{
		this.subscriptionKey = subscriptionKey;
	}

}
