/**
 * 
 */
package sa.com.mobily.eportal.acct.mgmt.login.history.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import sa.com.mobily.eportal.acct.mgmt.login.history.valueobjects.LoginHistoryVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

/**
 * @author n.gundluru.mit
 *
 */
public class LoginHistoryDAO extends AbstractDBDAO<LoginHistoryVO> {

	private static LoginHistoryDAO instance = new LoginHistoryDAO();
	
	protected LoginHistoryDAO() {
		super(DataSources.DEFAULT);
	}

	public static final String FIND_ALL_BY_USER_NAME = "SELECT SOURCE, USER_NAME, CREATED_DATE, STATUS, ERROR_MESSAGE, TYPE, ELAPSED_TIME, DB_ELAPSED_TIME, LDAP_ELAPSED_TIME, MQ_ELAPSED_TIME"
															+" FROM EEDBUSR.MOBILY_LOGIN_SERVICE_LOG"
															+" WHERE LOWER(USER_NAME)=LOWER(?) AND CREATED_DATE >= TRUNC(SYSDATE) - ?"
															+" ORDER BY CREATED_DATE DESC";
	
	/**
	 * 
	 * @return
	 */
	public static LoginHistoryDAO getInstance() {
		return instance;
	}
	
	
	/**
	 * This method returns a list login history for a specific user name and duration
	 * 
	 * @param request <codeuserName</code> account id
	 * @param request <code>duration</code> account id
	 * @return <code>ArrayList<LoginHistoryVO></code> a list of contacts
	 * @author Nagendra Gundluru
	 */
	public ArrayList<LoginHistoryVO> findLoginHistoryByUserName(String userName, int duration) {
		 return (ArrayList<LoginHistoryVO>) query(FIND_ALL_BY_USER_NAME, userName, duration);
	}
	
	
	
	/**
	 * This method returns a list login history for a specific user name and duration
	 * 
	 * @param request <codeuserName</code> account id
	 * @param request <code>duration</code> account id
	 * 
	 * @return <code>LoginHistoryVO</code> a list of contacts
	 * @author Nagendra Gundluru
	 */
	@Override
	protected LoginHistoryVO mapDTO(ResultSet rs) throws SQLException
	{
		LoginHistoryVO loginHistoryVoObj = new LoginHistoryVO();
			loginHistoryVoObj.setLoginDateTime(rs.getTimestamp("CREATED_DATE"));
			loginHistoryVoObj.setSource(rs.getString("SOURCE"));
			loginHistoryVoObj.setLoginType(rs.getString("TYPE"));
			loginHistoryVoObj.setLoginStatus(rs.getInt("STATUS"));
			loginHistoryVoObj.setUserName(rs.getString("USER_NAME"));
		
		return loginHistoryVoObj;
	}

}
