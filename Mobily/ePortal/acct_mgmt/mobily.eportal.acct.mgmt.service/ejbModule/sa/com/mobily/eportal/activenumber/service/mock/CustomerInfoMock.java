package sa.com.mobily.eportal.activenumber.service.mock;

import sa.com.mobily.eportal.customerinfo.vo.CustomerInfoVO;

public class CustomerInfoMock
{

	/**
	 * @MethodName: getCustomerProfile
	 * <p>
	 * Get Customer profile by package info
	 * 
	 * @param CustomerINfoVO requestVO
	 * 
	 * @return CustomerInfoVO
	 * 
	 * @author Abu Sharaf
	 */
	public static CustomerInfoVO getCustomerProfile(CustomerInfoVO requestVO)
	{
		CustomerInfoVO customerInfoVO = null;
		customerInfoVO = new CustomerInfoVO();
		customerInfoVO.setCreditScoring("25");
		customerInfoVO.setCustomerCategory("P");
		customerInfoVO.setPayType("1");
		customerInfoVO.setPackageId("PKG-123");
		customerInfoVO.setPackageName("My Package");
		customerInfoVO.setPreferredLanguage("En");
		customerInfoVO.setCustomerIdNumber("AX0877292");
		return customerInfoVO;
	}
}