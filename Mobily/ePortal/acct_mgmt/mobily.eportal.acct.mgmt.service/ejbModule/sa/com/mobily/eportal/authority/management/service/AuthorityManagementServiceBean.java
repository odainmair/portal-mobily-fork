package sa.com.mobily.eportal.authority.management.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.authority.config.AppConfig;
import sa.com.mobily.eportal.authority.management.dao.SubAdminAccountSubscriptionDAO;
import sa.com.mobily.eportal.authority.management.dao.SubAdminAccountsDAO;
import sa.com.mobily.eportal.authority.mgmt.constants.AuthorityMgmtConstants;
import sa.com.mobily.eportal.authority.mgmt.service.view.AuthorityManagementServiceLocal;
import sa.com.mobily.eportal.authority.mgmt.service.view.AuthorityManagementServiceRemote;
import sa.com.mobily.eportal.authority.mgmt.vo.CAPUserAccountVO;
import sa.com.mobily.eportal.authority.mgmt.vo.SubAdminVO;
import sa.com.mobily.eportal.capregistration.service.view.CAPRegistrationAsyncServiceRemote;
import sa.com.mobily.eportal.capregistration.vo.AccountInformationVO;
import sa.com.mobily.eportal.capregistration.vo.UserInformationVO;
import sa.com.mobily.eportal.common.constants.PumaProfileConstantsIfc;
import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;
import sa.com.mobily.eportal.common.service.ldapservice.LDAPManagerFactory;
import sa.com.mobily.eportal.common.service.ldapservice.MobilyUser;
import sa.com.mobily.eportal.common.service.sms.SMSSender;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.ldap.LDAPManager;
import sa.com.mobily.eportal.registration.constant.RegisterationConstantsIfc;

@Stateless
@Local(AuthorityManagementServiceLocal.class)
@Remote(AuthorityManagementServiceRemote.class)
@LocalBean
public class AuthorityManagementServiceBean implements AuthorityManagementServiceLocal,AuthorityManagementServiceRemote,AcctMgmtLoggerConstantsIfc
{
	private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(ACCT_MGMT_AUTHORITY_MGMT);
	
	private String className = AuthorityManagementServiceBean.class.getName();
	
	/*public static final String STATUS_ACTIVE = "2";
	
	public static final String CUSTOMER_SEGMENT_BUSINESS = "2";
	
	public static final String REGISTRATION_SOURCE_ID = "1";
	
	public static final String ROLE_ID = "2";*/
	public AuthorityManagementServiceBean(){
		
	}
	
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean createSubAdmin(SubAdminVO subAdminVO,CAPRegistrationAsyncServiceRemote capServiceRegistrationService){
		String methodName="createSubAdmin";
		boolean regStatus=false; 
		String userAcctId = "";
		MobilyUser mobilyUser=null;
		Date startDate = null;
		short step=1;
		try {
			
			executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+ " >>Create user in DB Started", className, methodName);
			
			SubAdminVO subAdminVO1 = SubAdminAccountsDAO.getInstance().getUserAcctIdSeq();
			
			if (subAdminVO1 != null)
				userAcctId = subAdminVO1.getUserAcctId();
			
			subAdminVO.setUserAcctId(userAcctId);
			
			String userPassword = RandomGenerator
					.generateAlphabetNumericRandom(3).toLowerCase()
					+ RandomGenerator.generateAlphabetNumericRandom(3)
							.toUpperCase()
					+ RandomGenerator.generateNumericRandom().substring(0, 3);
			
			executionContext.audit(Level.INFO, "AuthorityManagementServiceBean>> "+methodName+ " userPassword[" + userPassword + "]", className, methodName);
			
			subAdminVO.setPassword(userPassword);
			startDate = new Date();
			int status = SubAdminAccountsDAO.getInstance().addUserAccounts(subAdminVO);
			executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+ " >>user accounts DB status::"+status +" >> executed with ["+getExecutionTime(startDate)+"] Milliseconds", className, methodName);
			
			
			String subscriptionId ="";
			if (status > 0)
			{
				subscriptionId =SubAdminAccountSubscriptionDAO.getInstance().getUserSubscriptionId();
				CAPUserAccountVO registrationVO = new CAPUserAccountVO();
				registrationVO.setSubscriptionId(subscriptionId);
				registrationVO.setUserAcctId(userAcctId);
				registrationVO.setBillingAccountNumber(subAdminVO.getBillingAccountNumber());
				registrationVO.setCapMobile(subAdminVO.getMsisdn());
				startDate = new Date();
				status = SubAdminAccountSubscriptionDAO.getInstance().addSubscription(registrationVO);
				executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+ "status of insert in corporate subscription table:: status--->" + status+" >> executed with ["+getExecutionTime(startDate)+"] Milliseconds", className, methodName);
				
			}
			
			if (status > 0){
				
				executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+" >>Create user in LDAP Started", className, methodName);
				startDate = new Date();
				mobilyUser = getLDAPUserEntity(subAdminVO,subscriptionId);
				
				LDAPManager.getInstance().createNewUserOnLDAP(mobilyUser);
				
				executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+" >> Create user in LDAP Ended >> executed with ["+getExecutionTime(startDate)+"] Milliseconds", className, methodName);
			    step=2;
				startDate = new Date();
				//TAMService.getInstance().createNewUserOnTAM(mobilyUser);
				
				//executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" > User Created into TAM Successfully, executed with ["+getExecutionTime(startDate)+"] Milliseconds", className, methodName);
				
				//step=3;
				AccountInformationVO accountInformationVO = new AccountInformationVO();
				accountInformationVO.setBillingAccountNumber(subAdminVO.getBillingAccountNumber());
				UserInformationVO userInformationVO = new UserInformationVO();
				userInformationVO.setUserAcctId(userAcctId);
				userInformationVO.setUsername(subAdminVO.getUserName());
				userInformationVO.setPreferredLang(AuthorityMgmtConstants.PREFERRED_LANGUAGE_EN);
				accountInformationVO.setUserInformationVO(userInformationVO);
				capServiceRegistrationService.processRegisteration(accountInformationVO);
				executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" Async Bean has been called: " + mobilyUser.getUid(), className, methodName);
				
				sendSubAdminUserCreationSMS(subAdminVO.getMsisdn(),subAdminVO.getPreferredLanugage(),subAdminVO.getPassword(),subAdminVO.getCompanyName());
				executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" SMS sent to the sub admin details :: " + mobilyUser.getUid(), className, methodName);

			}
				
			regStatus = true;
		}catch(Exception e){
			
			executionContext.log(Level.SEVERE,"AuthorityManagementServiceBean >> "+methodName+ " >> Exception while creating the sub admin user ::"+e.getMessage(),className,methodName,e);
			executionContext.log(Level.SEVERE,"AuthorityManagementServiceBean >> "+methodName+ " >> Error occured during registration step = ["+step+"]",className,methodName,e);
			if(step >1){
				
				executionContext.log(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+ " >>Deleted from DB", className, methodName,null);	
				SubAdminAccountSubscriptionDAO.getInstance().deleteCorporateSubscriptionAndUserAccount(FormatterUtility.isNotEmpty(userAcctId)?Long.valueOf(userAcctId):0);
				
				executionContext.log(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+">> Delete from LDAP", className, methodName,null);
				LDAPManager.getInstance().deleteUser(mobilyUser);
				
				//executionContext.log(Level.INFO, "AuthorityManagementServiceBean >> "+methodName+">> Delete from TAM", className, methodName,null);
        		//TAMService.getInstance().deleteTamUser(mobilyUser.getUid());
			}
			
			throw new RuntimeException(e);
		}
		
		
		return  regStatus;
	}
	
	
	
	
	/**
	 * This method is used to populate the MobilyUser LDAP VO
	 * @param subAdminVO
	 * @param subscriptionId
	 * @return
	 */
	public MobilyUser getLDAPUserEntity(SubAdminVO subAdminVO,String subscriptionId)
	{
		MobilyUser mobilyUser = new MobilyUser();
		mobilyUser.setSn(subAdminVO.getUserName());
		mobilyUser.setUid(subAdminVO.getUserName());
		mobilyUser.setCn(subAdminVO.getUserName());
		mobilyUser.setGivenName(subAdminVO.getName());
		mobilyUser.setPreferredLanguage(subAdminVO.getPreferredLanugage());
		//mobilyUser.setFirstName(subAdminVO.getName());
		//mobilyUser.setLastName(accountInformationVO.getUserInformationVO().getLastName());
		//mobilyUser.setPreferredLanguage(AuthorityMgmtConstants.PREFERRED_LANGUAGE_EN);
		//mobilyUser.setEmail(accountInformationVO.getUserInformationVO().getEmail());
		mobilyUser.setUserPassword(subAdminVO.getPassword());
		//mobilyUser.setSecurityQuestion(accountInformationVO.getUserInformationVO().getSecurityQuesId());
		//mobilyUser.setSecurityAnswer(accountInformationVO.getUserInformationVO().getSecurityAnswer());
		mobilyUser.setCorpMasterAcctNo(subAdminVO.getBillingAccountNumber());
		mobilyUser.setCorpAPNumber(subAdminVO.getMsisdn());
		//mobilyUser.setCorpAPIDNumber(accountInformationVO.getCommercialId());

		List<String> groupList = new ArrayList<String>();
		groupList.add(AuthorityMgmtConstants.CORP_LDAP_GROUP_NAME);

		mobilyUser.setGroupList(groupList);

		mobilyUser.setDefaultMsisdn(subAdminVO.getMsisdn());
		mobilyUser.setDefaultSubId(subscriptionId);
		
		/*
		 * mobilyUser
		 * .setLineType(ConstantIfc.LINE_TYPE_CORPORATE_AUTHORIZED_PERSON);
		 */
		mobilyUser.setStatus(AuthorityMgmtConstants.STATUS_ACTIVE);
		mobilyUser.setCustSegment(AuthorityMgmtConstants.CUSTOMER_SEGMENT_BUSINESS);
		mobilyUser.setRoleId(Integer.parseInt(AuthorityMgmtConstants.ROLE_ID));

		//mobilyUser.setBirthDate(accountInformationVO.getUserInformationVO().getBirthDate());
		mobilyUser.setNationality("");// accountInformationVO.getUserInformationVO().getNationality()
		//mobilyUser.setGender(accountInformationVO.getUserInformationVO().getGender());
		mobilyUser.setRegistrationSourceId(Integer.parseInt(AuthorityMgmtConstants.REGISTRATION_SOURCE_ID));
		mobilyUser.setRegisterationDate(new Date(new Timestamp(new Date().getTime()).getTime()));
		mobilyUser.setUserAcctReference(subAdminVO.getUserAcctId());

		mobilyUser.setRegisterationDate(new Date());
		return mobilyUser;
	}

	private long getExecutionTime(Date StartDate)
	{
		long endTime = 0;
		long startTime = 0;
		try {
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			endTime = c.getTimeInMillis();
	
			c.setTime(StartDate);
			startTime = c.getTimeInMillis();
		} catch(Exception e) {}

		return endTime - startTime;

	}
	
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<CAPUserAccountVO> getSubAdminsList(String billingAccountNumber){

		String methodName="getSubAdminsList";
		
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> Billing Account No:: " + billingAccountNumber, className, methodName);
		List<CAPUserAccountVO> subAdminsList = SubAdminAccountSubscriptionDAO.getInstance().getSubAdminsList(billingAccountNumber);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >> subAdminsList:: " + subAdminsList, className, methodName);
		return subAdminsList;
		
	}

	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean disableSubAdminUser(String userAcctId,String billingAccountNumber){
		String methodName="disableSubAdminUser";
		boolean status =false;
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> userAcctId:: " + userAcctId, className, methodName);
		int count = SubAdminAccountSubscriptionDAO.getInstance().disableSubAdminUser(userAcctId, billingAccountNumber);
		if(count>0){ 
			status =true;
		}
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> status:: " + status, className, methodName);		
		return status;
	}
	
	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean resetPasswordForSubAdminUser(String userAcctId,String billingAccountNumber,String language){
		
		String methodName="resetPasswordForSubAdminUser";
		boolean status =false;
		
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> userAcctId:: " + userAcctId, className, methodName);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> language:: " + language, className, methodName);
		String userPassword = RandomGenerator
				.generateAlphabetNumericRandom(3).toLowerCase()
				+ RandomGenerator.generateAlphabetNumericRandom(3)
						.toUpperCase()
				+ RandomGenerator.generateNumericRandom().substring(0, 3);
		
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean>> "+methodName+ " userPassword[" + userPassword + "]", className, methodName);
		
		CAPUserAccountVO accountVO = SubAdminAccountSubscriptionDAO.getInstance().getCapMobileUserName(userAcctId, billingAccountNumber);
		if(updatePassword(accountVO.getUserName(),userPassword)){
			executionContext.audit(Level.INFO, "AuthorityManagementServiceBean>> "+methodName+ ">> Update password in LDAP is success" , className, methodName);
			if(FormatterUtility.isNotEmpty(accountVO.getCapMobile())){
				sendResetPasswordSMS(accountVO.getCapMobile(),language,userPassword);
				executionContext.audit(Level.INFO, "AuthorityManagementServiceBean>> "+methodName+ ">> SMS sent with password to user successfully " , className, methodName);
				status=true;
			}
		}
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> status:: " + status, className, methodName);		
		return status;
	}

	@Override
	@Interceptors(ServiceInterceptor.class)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean checkMobileForDuplicate(String mobileNumber, String billingAccountNumber){
		
		String methodName="checkMobileForDuplicate";
		boolean status =false;
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> mobileNumber:: " + mobileNumber, className, methodName);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> mobileNumber:: " + mobileNumber, className, methodName);
		try{
			status = SubAdminAccountSubscriptionDAO.getInstance().getCapMobileForDuplicateCheck(mobileNumber, billingAccountNumber);
		}catch(Exception e){
			executionContext.log(Level.SEVERE,"AuthorityManagementServiceBean >> "+methodName+ " >> Exception while getting the cap mobile and checking for duplicate ::"+e.getMessage(),className,methodName,e);
		}
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+">> status:: " + status, className, methodName);
		return status;
	}
	
	/**
	 * Sending reset password SMS to Sub Admin User
	 * @param mobileNumber
	 * @param locale
	 * @param userPassword
	 */
	public void sendResetPasswordSMS(String mobileNumber, String locale, String userPassword){
		
		String methodName="sendSMSToSubAdmin";
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>mobileNumber ::"+mobileNumber, className, methodName);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>locale ::"+locale, className, methodName);
		String[] parametersArray = new String[1];

		StringBuffer smsText = new StringBuffer();
		String messageId = AuthorityMgmtConstants.GENERIC_SMS_ID_EN;
		if(RegisterationConstantsIfc.LOCALE_AR.equalsIgnoreCase(locale)){
			messageId =AuthorityMgmtConstants.GENERIC_SMS_ID_AR;
			smsText.append(AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_RESET_PWD_AR_SMS_TEXT_1));
			String arSMS= AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_RESET_PWD_AR_SMS_TEXT_2);
			arSMS =arSMS.replace("{0}",userPassword);
			smsText.append(arSMS);
		}else{
			smsText.append(AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_RESET_PWD_EN_SMS_TEXT_1));
			String enSMS= AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_RESET_PWD_EN_SMS_TEXT_2);
			enSMS =enSMS.replace("{0}",userPassword);
			smsText.append(enSMS);
		}
		
		parametersArray[0] = smsText.toString();
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>smsText ::"+smsText, className, methodName);

		SMSSender.sendSMS(mobileNumber, messageId, parametersArray);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>Password Message sent", className, methodName);
		 
	}

	/**
	 * Sending Sub Admin user creation SMS 
	 * @param mobileNumber
	 * @param locale
	 * @param userPassword
	 * @param companyName
	 */
	public void sendSubAdminUserCreationSMS(String mobileNumber, String locale, String userPassword,String companyName){
		
		String methodName="sendSubAdminUserCreationSMS";
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>mobileNumber ::"+mobileNumber, className, methodName);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>locale ::"+locale, className, methodName);
		
		String[] parametersArray = new String[1];
		
		StringBuffer smsText = new StringBuffer();
		String messageId = AuthorityMgmtConstants.GENERIC_SMS_ID_EN;
		if(RegisterationConstantsIfc.LOCALE_AR.equalsIgnoreCase(locale)){
			messageId =AuthorityMgmtConstants.GENERIC_SMS_ID_AR;
			String arSMSLine1=  AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_CREATE_AR_SMS_TEXT_1);
			arSMSLine1 = arSMSLine1.replace("{0}", companyName);
			String arSMSLine2=  AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_CREATE_AR_SMS_TEXT_2);
			arSMSLine2 = arSMSLine2.replace("{1}",userPassword);
			
			smsText.append(arSMSLine1);
			smsText.append(arSMSLine2);
		}else{
			String enSMSLine1=  AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_CREATE_EN_SMS_TEXT_1);
			enSMSLine1 = enSMSLine1.replace("{0}", companyName);
			String enSMSLine2=  AppConfig.getInstance().get(AuthorityMgmtConstants.SUB_ADMIN_CREATE_EN_SMS_TEXT_2);
			enSMSLine2 = enSMSLine2.replace("{1}",userPassword);
			
			smsText.append(enSMSLine1);
			smsText.append(enSMSLine2);
		}
		
		parametersArray[0] =smsText.toString();
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>smsText ::"+smsText, className, methodName);
		
		SMSSender.sendSMS(mobileNumber, messageId, parametersArray);
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>Sub Admin details -> Message sent", className, methodName);
		
	}
	
	public boolean updatePassword(String userName, String password){

		String methodName="updatePassword";
		boolean status =false;
		MobilyUser mobilyUser= null;
		try{
			executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>checking the user in LDAP", className, methodName);
			try{
				mobilyUser= LDAPManagerFactory.getInstance().getLDAPManagerImpl().findMobilyUserByAttribute(PumaProfileConstantsIfc.uid,userName);
			}catch(Exception e){
				executionContext.log(Level.SEVERE,"AuthorityManagementServiceBean >> "+methodName+ " >> Couldn't find user with the provided attributes ->"+e.getMessage(),className,methodName,e);
			}
			executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >>user exists and updating password in LDAP ->"+mobilyUser, className, methodName);
			LDAPManagerFactory.getInstance().getLDAPManagerImpl().updatePasswordOnLDAP(userName, password);
			status =true;
			
		}catch(Exception e){
			executionContext.log(Level.SEVERE,"AuthorityManagementServiceBean >> "+methodName+ " >> Exception while updating the password in LDAP:: ->"+e.getMessage(),className,methodName,e);
		}
		executionContext.audit(Level.INFO, "AuthorityManagementServiceBean >"+methodName+" >> update password -> status :: "+status, className, methodName);		
		return status;
	}

}
