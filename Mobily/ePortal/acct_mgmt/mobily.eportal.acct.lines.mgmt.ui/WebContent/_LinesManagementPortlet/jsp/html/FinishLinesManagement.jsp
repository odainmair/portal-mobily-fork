<%@page session="false" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*,javax.portlet.*,sa.com.mobily.eportal.lines.mgmt.portlet.*" %>
<%@ taglib uri="/WEB-INF/tld/portal.tld" prefix="wps" %>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>        
<%@taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt_rt"%> 
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>        
<portlet:defineObjects/>
<fmt:setBundle  basename="sa.com.mobily.eportal.lines.mgmt.portlet.nl.LinesManagementPortletResource" />
<style>
#account_dashboard.broadband .notification_box ul{width:auto}
#account_dashboard.broadband .notification_box li a {width:auto !important; padding:10px; background:#cfbe64 !important;}
#account_dashboard.broadband .notification_box{border:none !important}
#account_dashboard.broadband .data_box .cta_data_consumption{padding:0 !important}
#content_main_cont .alert_msg p, #content_main_cont .service_msg p{box-sizing:border-box}
</style>
<script type="text/javascript" >
    var djConfig  = {
        parseOnLoad: false,
        locale: 'en-us',
        packages: [ {
            name: 'custom',
            location: '/MobilyResources/default/js/custom'
        } ],
        require : [
         'dojo/NodeList-traverse',
            'dojo/ready',
            'dojo/query',
            'dijit/Dialog',
            'dijit/form/Button',
            'dijit/form/CheckBox',
            'dijit/form/DateTextBox',
            'dijit/form/FilteringSelect',
            'dijit/form/Form',
            'dijit/form/HorizontalSlider',
            'dijit/form/Select',
            'dijit/form/RadioButton',
            'dijit/form/TextBox',
            'dijit/form/Textarea',
            'dijit/form/TimeTextBox',
            'dijit/form/ValidationTextBox',
            'dojox/form/BusyButton',
            'dojox/form/CheckedMultiSelect',
            'dojox/grid/EnhancedGrid',
            'dojox/grid/DataGrid',
            'dijit/layout/TabContainer',
            'dijit/layout/ContentPane',
            'dijit/layout/AccordionContainer'
        ],
        async : true
    }

</script>
<script type="text/javascript" src="/MobilyResources/default/js/dojo/dojo/dojo.js" ></script>
<script type="text/javascript">require(['custom/run']);</script>
<section id="content_main" class="block clearfix">
	<div>
		<div class="row alert_msg ${requestScope.resultClass}" id="lineManagementEnd">
			<p class="col-lg-12 col-md-12 col-sm-10 col-xs-10"><fmt_rt:message key="${requestScope.Result}" /></p>
			<ul class="prompt_controls floatr">
				<li>
					<wps:urlGeneration contentNode="sa.com.mobily.home.personal.my.mobily.my.profile" layoutNode="sa.com.mobily.eportal.navigation.my.profile" keepNavigationalState="false">
					<a href="<%wpsURL.write(out);%>" class="cta_prompt transition"><fmt_rt:message key="common.msgs.dialog.button.ok.label" /></a>
					</wps:urlGeneration>
				</li>
			</ul>
			<a href="javascript:hideMessageDiv('lineManagementEnd');" class="close_alert_msg kir"><fmt_rt:message key="common.msgs.dialog.button.close.label" /> </a>
		</div>
	</div>
</section>

<script>
function hideMessageDiv(divID) {
	document.getElementById(divID).style.display = 'none';
}
</script>
