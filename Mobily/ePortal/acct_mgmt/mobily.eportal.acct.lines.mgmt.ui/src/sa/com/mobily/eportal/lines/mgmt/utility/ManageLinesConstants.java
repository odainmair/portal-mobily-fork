package sa.com.mobily.eportal.lines.mgmt.utility;


public final class ManageLinesConstants {

	private ManageLinesConstants() {
		super();
	}

	public static final String ADD_NEW_SUB_JSP = "/_LinesManagementPortlet/jsp/html/LinesManagementPortletView.jsp";
	public static final String SUCCESS_JSP =  "/_LinesManagementPortlet/jsp/html/FinishLinesManagement.jsp";
	public static final String ADD_DEFAULT_LINE_JSP =  "/_LinesManagementPortlet/jsp/html/AddDefaultLine.jsp";
	
	
	// CONTENT TYPE
	public static final String CONTENT_TYPE = "text/html";

	// ACTION ANNOTATIONS START
	public static final String ACTION_SAVE = "save";
	public static final String RENDER_MODE_VIEW = "view";
	
	public final static String  JAVAX_PORTLET_ACTION = "javax.portlet.action";
	// ACTION ANNOTATIONS END
	public static final String ERROR_MSGS_MAP_ID = "errorMessagesMap";
	public static final String FIELD_SUBSCRIPTION_TYPE = "SubscriptionType";
	public static final String SUBSCRIPTION_PORTLET_FORM = "addNewSubForm";
	public static final String FIELD_MOBILE_NUMBER = "MobileNumber";
	public static final String MOBILE_NUMBER_MAX_SIZE = "15";
	public static final String FIELD_NATIONALID_OR_IQAMA_NUMBER = "NationalIdORIqamaNumber";
	public static final String FIELD_ACTIVATION_CODE = "ActivationCode";
	public static final int    ACTIVATION_CODE_MAX_SIZE = 8;
	public static final String FIELD_SERVICE_ACCOUNT_NUMBER = "ServiceAccountNumber";
	public final static int    SERVICE_ACCOUNT_NUMBER_MAX_SIZE = 20;
	public final static int NATIONAL_ID_OR_IQAMA_MAX_SIZE = 50;
	public static final String SEND_ACTIVATION_PIN = "SendActivationPin";
	public static final String REQ_SUBS_TYPES = "subscriptionTypes";
	public static final String ACTIVATION_CODE_BACKEND = "addLineActivationCodeFromBackend";
	public static final String ADD_LINE_USER_SUB_VO = "userSubscriptionVO";
	public static final String ERROR_MSGS_MAP = "errorMsgsMap";
	public static final String EXCEPTION_OCCURRED = "exceptionOccurred";
	public static final String GENERIC_ERROR = "system.error";
	public static final String GENERIC_FIELDS_VALIDATION = "validation.msg";
	public static final String ADD_LINE_SUCCESS_MSG = "addLineSuccessMsg";
	public static final String HANDLE_TYPE = "handleType";
	public static final String MAIN_CONTENT_FLAG = "mainContainer";
	
	public static final String SEARCH_MOBILE_NUMBER = "SearchMobileNumber";
	
	public final static String MSISDN_REG_EXP = "\\d{12,15}"; //"\\d{12}";
	public final static String SERVICE_ACCOUNT_REG_EXP = "[0-9]+";
	
}
