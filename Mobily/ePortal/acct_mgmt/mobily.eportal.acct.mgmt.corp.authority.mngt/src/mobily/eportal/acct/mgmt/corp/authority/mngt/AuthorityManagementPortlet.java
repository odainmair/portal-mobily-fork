package mobily.eportal.acct.mgmt.corp.authority.mngt;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import mobily.eportal.acct.mgmt.corp.authority.Util.AuthorityManagementUtil;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.authority.mgmt.service.view.AuthorityManagementServiceLocal;
import sa.com.mobily.eportal.authority.mgmt.vo.CAPUserAccountVO;
import sa.com.mobily.eportal.authority.mgmt.vo.SubAdminVO;
import sa.com.mobily.eportal.bp.corporate.pagination.headerparser.util.PaginationHeaderParser;
import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.capregistration.service.view.CAPRegistrationAsyncServiceRemote;
import sa.com.mobily.eportal.common.beanlocator.BeanLocator;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.PagingVO;
import sa.com.mobily.eportal.companyprofile.service.view.CompanyProfileServiceRemote;
import sa.com.mobily.eportal.registration.service.view.PersonalRegistrationServiceRemote;

/**
 * A sample portlet
 */
public class AuthorityManagementPortlet extends javax.portlet.GenericPortlet implements AcctMgmtLoggerConstantsIfc{
	
	private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(ACCT_MGMT_AUTHORITY_MGMT);
	
	private String className = AuthorityManagementPortlet.class.getName();
	
	public static final String AUTH_MGMT_JSP_PATH="/_AuthMgmt/jsp/html/authMgmtView.jsp";

	private PersonalRegistrationServiceRemote personalRegistrationService;
	
	private CAPRegistrationAsyncServiceRemote capServiceRegistrationService;
	
	private AuthorityManagementServiceLocal authorityManagementService;
	
	private CompanyProfileServiceRemote companyProfileService;
	/**
	 * @see javax.portlet.Portlet#init()
	 */
	public void init() throws PortletException{
		super.init();
		
		try
		{
		
			authorityManagementService =(AuthorityManagementServiceLocal)BeanLocator.lookup("ejblocal:"+AuthorityManagementServiceLocal.class.getName());
			personalRegistrationService = (PersonalRegistrationServiceRemote) BeanLocator.remoteLookup(PersonalRegistrationServiceRemote.class.getName());
			capServiceRegistrationService=(CAPRegistrationAsyncServiceRemote)BeanLocator.remoteLookup(CAPRegistrationAsyncServiceRemote.class.getName());
			companyProfileService=(CompanyProfileServiceRemote)BeanLocator.remoteLookup(CompanyProfileServiceRemote.class.getName());
			
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "init() -> Error initializing Filter: ", className, "init", e);
		
		}
	}

	/**
	 * Serve up the <code>view</code> mode.
	 * 
	 * @see javax.portlet.GenericPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
		
		String methodName = "doView";
		// Set the MIME type for the render response
		response.setContentType(request.getResponseContentType());
		executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ " >> start", className, methodName, null);
		String jspPage = AUTH_MGMT_JSP_PATH;
		try{
			SessionVO session = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), request.getRemoteUser());
			String billingAccountNumber = session.getRootBillingAccountNumber();
			//String billingAccountNumber ="1000110355809822";
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ " >> Logged in Billing Account Number::"+ billingAccountNumber, className, methodName, null);

			List<CAPUserAccountVO> subAdminsList = authorityManagementService.getSubAdminsList(billingAccountNumber);
			if(CollectionUtils.isNotEmpty(subAdminsList))
				request.setAttribute("subAdminsList", subAdminsList);
			
			if(request.getPortletSession().getAttribute("error")!=null){
			
				request.setAttribute("error",request.getPortletSession().getAttribute("error"));
				request.setAttribute("name",request.getPortletSession().getAttribute("name"));
				request.setAttribute("mobileNumber",request.getPortletSession().getAttribute("mobileNumber"));
				request.setAttribute("userName",request.getPortletSession().getAttribute("userName"));
				
				request.getPortletSession().removeAttribute("error");
				request.getPortletSession().removeAttribute("name");
				request.getPortletSession().removeAttribute("mobileNumber");
				request.getPortletSession().removeAttribute("userName");
				
				if(request.getPortletSession().getAttribute("addUserFailed")!=null){
					request.setAttribute("addUserFailed",request.getPortletSession().getAttribute("addUserFailed"));
					request.getPortletSession().removeAttribute("addUserFailed");
				}
			}

			if(request.getPortletSession().getAttribute("success")!=null){
				request.setAttribute("success",request.getPortletSession().getAttribute("success"));
				request.getPortletSession().removeAttribute("success");
			}
			
			jspPage = AUTH_MGMT_JSP_PATH;
			
			
		}catch(Exception e){
			executionContext.log(Level.SEVERE,"AuthorityManagementPortlet >> "+methodName+ " >> Exception while fetching the sub admins from DB::"+e.getMessage(),className,methodName,e);
			jspPage = AUTH_MGMT_JSP_PATH;
		
		}
		PortletRequestDispatcher rd = getPortletContext().getRequestDispatcher(jspPage);
		rd.include(request,response);
	}

	

	
	@ProcessAction(name = "addSubAdminAction")
	public void addSubAdminUser(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {
		// TODO: auto-generated method stub

		String methodName="addSubAdminUser";
			
		String name=request.getParameter("name");
		
		String mobileNumber=request.getParameter("mobileNo");
		
		String userName=request.getParameter("userName");
		
		try{
			SessionVO session = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), request.getRemoteUser());
			String billingAccountNumber = session.getRootBillingAccountNumber();
			//String billingAccountNumber ="1000110355809822";
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ " >> Logged in Billing Account Number::"+ billingAccountNumber, className, methodName, null);

			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :userName:"+userName , className, methodName, null);
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :mobileNumber:"+mobileNumber , className, methodName, null);
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :name:"+name , className, methodName, null);
			mobileNumber = FormatterUtility.formatMobileNumber(mobileNumber);
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> after formatting :mobileNumber ->"+mobileNumber , className, methodName, null);
			boolean userNameExists = AuthorityManagementUtil.getInstance().verifyUsername(userName, personalRegistrationService);
			//boolean userNameExists = true;
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :userNameExists::"+userNameExists , className, methodName, null);
			boolean mobileNumberExists =AuthorityManagementUtil.getInstance().checkMobileForDuplicate(mobileNumber, billingAccountNumber, authorityManagementService);
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :mobileNumberExists::"+mobileNumberExists , className, methodName, null);
	
			if(userNameExists){
				request.getPortletSession().setAttribute("error","auth.mgmt.exists.userName");
				request.getPortletSession().setAttribute("addUserFailed","YES");
				request.getPortletSession().setAttribute("name",name);
				request.getPortletSession().setAttribute("mobileNumber",mobileNumber);
				request.getPortletSession().setAttribute("userName",userName);
				executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> User name exists:error message::"+request.getAttribute("error") , className, methodName, null);

			}
			else if(mobileNumberExists){
				request.getPortletSession().setAttribute("error","auth.mgmt.exists.mobile.number");
				request.getPortletSession().setAttribute("addUserFailed","YES");
				request.getPortletSession().setAttribute("name",name);
				request.getPortletSession().setAttribute("mobileNumber",mobileNumber);
				request.getPortletSession().setAttribute("userName",userName);
				executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> Mobile Number exists :error message::"+request.getAttribute("error") , className, methodName, null);
			}
			else{
				try{
					String companyName = AuthorityManagementUtil.getInstance().getCompanyName(billingAccountNumber, companyProfileService);
					executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :companyName::"+companyName , className, methodName, null);	
					SubAdminVO subAdminVO = new SubAdminVO();
					subAdminVO.setBillingAccountNumber(billingAccountNumber);
					subAdminVO.setMsisdn(mobileNumber);
					subAdminVO.setUserName(userName);
					subAdminVO.setName(name);
					subAdminVO.setPreferredLanugage(request.getLocale().getLanguage());
					subAdminVO.setCompanyName(companyName);
					boolean status = authorityManagementService.createSubAdmin(subAdminVO, capServiceRegistrationService);
					executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :sub admin user created successfully::"+status , className, methodName, null);
					
					request.getPortletSession().setAttribute("success","auth.mgmt.user.creation.success.message");


				}catch(Exception e){
					request.getPortletSession().setAttribute("error","auth.mgmt.reset.password.failure.message");
					request.getPortletSession().setAttribute("addUserFailed","YES");
					request.getPortletSession().setAttribute("name",name);
					request.getPortletSession().setAttribute("mobileNumber",mobileNumber);
					request.getPortletSession().setAttribute("userName",userName);

					executionContext.log(Level.SEVERE,"AuthorityManagementPortlet >> "+methodName+ " >> Exception while creating the sub admin user::"+e.getMessage(),className,methodName,e);
				}
			}
		}
		catch(Exception e){
			request.getPortletSession().setAttribute("error","auth.mgmt.reset.password.failure.message");
			request.getPortletSession().setAttribute("addUserFailed","YES");
			request.getPortletSession().setAttribute("name",name);
			request.getPortletSession().setAttribute("mobileNumber",mobileNumber);
			request.getPortletSession().setAttribute("userName",userName);
			executionContext.log(Level.SEVERE,"AuthorityManagementPortlet >> "+methodName+ " >> Exception while verifying the username::"+e.getMessage(),className,methodName,e);
		}
		
	}
	
	@ProcessAction(name = "disableSubAdminAction")
	public void disableSubAdminUser(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {
		
		String methodName="disableSubAdminUser";
		try{
			SessionVO session = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), request.getRemoteUser());
			String billingAccountNumber = session.getRootBillingAccountNumber();
			//String billingAccountNumber ="1000110355809822";
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ " >> Logged in Billing Account Number::"+ billingAccountNumber, className, methodName, null);
	
			String userAcctId=request.getParameter("userAccId");
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> userAcctId::"+userAcctId , className, methodName, null);

			boolean status = AuthorityManagementUtil.getInstance().disableSubAdminUser(userAcctId, billingAccountNumber,authorityManagementService);
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :sub admin user disabled successfully::"+status , className, methodName, null);
			
			if (status) {
				request.getPortletSession().setAttribute("success","auth.mgmt.delete.success.message");
			} else {
				request.getPortletSession().setAttribute("error","auth.mgmt.delete.failure.message");
			}
		

		}catch(Exception e){
			executionContext.log(Level.SEVERE,"AuthorityManagementPortlet >> "+methodName+ " >> Exception while disabling  the sub admin user::"+e.getMessage(),className,methodName,e);
			request.getPortletSession().setAttribute("error","auth.mgmt.delete.failure.message");
		}
	}

	@ProcessAction(name = "resetPasswordAction")
	public void resetPasswordForSubAdmin(ActionRequest request, ActionResponse response) throws PortletException, java.io.IOException {
		
		String methodName="resetPasswordForSubAdmin";
		
		try{
			SessionVO session = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), request.getRemoteUser());
			String billingAccountNumber = session.getRootBillingAccountNumber();
			//String billingAccountNumber ="1000110355809822";
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ " >> Logged in Billing Account Number::"+ billingAccountNumber, className, methodName, null);
			
			String userAcctId=request.getParameter("userAccId");
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> userAcctId::"+userAcctId , className, methodName, null);
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> language::"+request.getLocale().getLanguage() , className, methodName, null);
			
			boolean status = AuthorityManagementUtil.getInstance().resetPasswordForSubAdmin(userAcctId, billingAccountNumber,request.getLocale().getLanguage(),authorityManagementService);
			
			executionContext.log(Level.INFO, "AuthorityManagementPortlet >> "+methodName+ ">> :password resetted for sub admin user successfully::"+status , className, methodName, null);
			if (status) {
				request.getPortletSession().setAttribute("success","auth.mgmt.reset.password.success.message");
			} else {
				request.getPortletSession().setAttribute("error","auth.mgmt.reset.password.failure.message");
			}
		}catch(Exception e){
			executionContext.log(Level.SEVERE,"AuthorityManagementPortlet >> "+methodName+ " >> Exception while resetting the password  the sub admin user::"+e.getMessage(),className,methodName,e);
			request.getPortletSession().setAttribute("error","auth.mgmt.reset.password.failure.message");
		}

	}

}
