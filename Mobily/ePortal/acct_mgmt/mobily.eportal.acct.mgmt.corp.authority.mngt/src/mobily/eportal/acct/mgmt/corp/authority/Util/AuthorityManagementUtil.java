package mobily.eportal.acct.mgmt.corp.authority.Util;

import java.util.logging.Level;

import sa.com.mobily.eportal.acct.mgmt.constants.AcctMgmtLoggerConstantsIfc;
import sa.com.mobily.eportal.authority.mgmt.service.view.AuthorityManagementServiceLocal;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.companyprofile.service.view.CompanyProfileServiceRemote;
import sa.com.mobily.eportal.registration.service.view.PersonalRegistrationServiceRemote;

public class AuthorityManagementUtil implements AcctMgmtLoggerConstantsIfc{

	private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(ACCT_MGMT_AUTHORITY_MGMT);
	
	private String className = AuthorityManagementUtil.class.getName();
	
	
	private AuthorityManagementUtil() {
	}
	
	public static AuthorityManagementUtil getInstance() {
		return AuthorityManagementUtilHolder.utilInstance;
	}
	
	private static class AuthorityManagementUtilHolder {
		private static final AuthorityManagementUtil utilInstance = new AuthorityManagementUtil();
	}
	
	public  boolean verifyUsername(String userName, PersonalRegistrationServiceRemote registrationService)
	{
		executionContext.log(Level.INFO, "AuthorityManagementUtil >> verifyUsername >> userName::"+userName, className, "verifyUsername", null);
		return registrationService.isUsernameExisted(userName);
	}

	public boolean disableSubAdminUser(String userAcctId, String billingAccountNumber,AuthorityManagementServiceLocal authorityManagementService){
		executionContext.log(Level.INFO, "AuthorityManagementUtil >> disableSubAdminUser >> userAcctId::"+userAcctId, className, "disableSubAdminUser", null);
		return authorityManagementService.disableSubAdminUser(userAcctId, billingAccountNumber);
	}
	
	
	
	public boolean resetPasswordForSubAdmin(String userAcctId, String billingAccountNumber,String language,AuthorityManagementServiceLocal authorityManagementService){
		executionContext.log(Level.INFO, "AuthorityManagementUtil >> resetPasswordForSubAdmin >> userAcctId::"+userAcctId, className, "resetPasswordForSubAdmin", null);
		return authorityManagementService.resetPasswordForSubAdminUser(userAcctId, billingAccountNumber,language);
	}
	
	public String getCompanyName(String billingAccountNumber,CompanyProfileServiceRemote companyProfileService){
		
		executionContext.log(Level.INFO, "AuthorityManagementUtil >> getCompanyName >> billingAccountNumber::"+billingAccountNumber, className, "getCompanyName", null);
		
		return companyProfileService.getCorporateProfileV01(billingAccountNumber).getCompanyName();
	}
	
	public boolean checkMobileForDuplicate(String mobileNumber, String billingAccountNumber,AuthorityManagementServiceLocal authorityManagementService){
		executionContext.log(Level.INFO, "AuthorityManagementUtil >> checkMobileForDuplicate >> billingAccountNumber::"+billingAccountNumber, className, "checkMobileForDuplicate", null);
		return authorityManagementService.checkMobileForDuplicate(mobileNumber, billingAccountNumber);
		
	}
	
}
