/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerTypeDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.CustomerTypeVO;
import sa.com.mobily.eportal.common.service.xml.CustomerTypeXMLHandler;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerTypeBO {

    private static final Logger log = LoggerInterface.log;
    /**
     * This method is reposnsible for GetCustoemr Type info from backend by determin is the customer
     * prepaid or postpaid
     * @return
     * @throws MobilyCommonException
     * Feb 18, 2008
     * CustomerTypeBO.java
     * Mandatory Fields:
     * 		1- LineNumber
     * 		2- requestorUserId
     * msayed
     */
    public CustomerTypeVO getCustomerType(CustomerTypeVO requestVO)throws MobilyCommonException{
        
        CustomerTypeVO  replyObj = null;
        //generate XML request
        try {
            String xmlReply = "";
            CustomerTypeXMLHandler  xmlHandler = new CustomerTypeXMLHandler();
            String xmlMessage  = xmlHandler.generateXMLRequest(requestVO);
           	log.info("CustomerTypeBO >getCustomerType > generate xml message for Customer  > "+xmlMessage);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            CustomerTypeDAO customerTypeDAO = daoFactory.getCustomerTypeDAO();
            xmlReply = customerTypeDAO.getCustomerType(xmlMessage);
            log.info("CustomerTypeBO >getCustomerType > parsing the reply message for Customer  > "+xmlReply);
            
            //parsing the xml reply
            replyObj = xmlHandler.parsingXMLRreply(xmlReply);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
      return replyObj;    
    
    }
    
    /**
     * check is the customer postpaid
     * @return
     * @throws MobilyCommonException
     * Feb 18, 2008
     * CustomerTypeBO.java
     * msayed
     * Mandatory Fields:
     * 		1- LineNumber
     * 		2- requestorUserId
     */
    public boolean isCustomerPostPaid(CustomerTypeVO requestVO)throws MobilyCommonException{
        boolean isCustomer = false;
        try{
          CustomerTypeVO  obj =   getCustomerType(requestVO);
          if(obj.getCustomerType() == ConstantIfc.CUSTOMER_POSTPAID_TYPE){
              isCustomer = true;
          }
        }catch(MobilyCommonException e){
            throw e;
        }
        log.info("CustomerTypeBO >isCustomerPostPaid > for Customer ["+requestVO.getLineNumber()+"] >  "+isCustomer);
        return isCustomer;    

      }

    
    public static void main(String[] args) {
    	
    	CustomerTypeVO vo = new CustomerTypeVO();
//    	vo.setAccountNumber("1000");
    	vo.setLineNumber("1dd000");
    	CustomerTypeBO bo = new CustomerTypeBO();
try {
	bo.getCustomerType(vo);
} catch (MobilyCommonException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}       
        
        
    }
}
