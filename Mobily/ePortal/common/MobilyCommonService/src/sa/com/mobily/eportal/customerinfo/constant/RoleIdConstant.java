package sa.com.mobily.eportal.customerinfo.constant;

public interface RoleIdConstant {
	public static final int ROLE_ID_CONSUMER_CUSTOMER = 1;
	public static final int ROLE_ID_CORPORATE_CUSTOMER = 2;
	public static final int ROLE_ID_REGISTERED_USER = 3;//NON subscribers
	public static final int ROLE_ID_REGISTERED_MOBILY_USER = 4; //Mobily user without default subscription  
	//public static final int ROLE_ID_DOCTOR_ON_DEMAND = 4;//This constant is depricated.
	public static final int ROLE_ID_SALES_ORDER = 5;
	public static final int ROLE_ID_CALL_CENTER = 6;
	public static final int ROLE_ID_WPS_ADMIN = 7;
	public static final int ROLE_ID_ANONYMOUS = 100;
	
	// Added by r.agarwal.mit
	public static final int ROLE_ID_B2C = 8;
	public static final int ROLE_ID_B2B = 9;
	public static final int ROLE_ID_ALSOMOU = 10; // Virtual Role Id
	public static final int ROLE_ID_NEQATY_PARTNER = 11;
	
}
