package sa.com.mobily.eportal.customerAccDetails.vo;

import java.util.List;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ListOfContactVO extends BaseVO
{
	private static final long serialVersionUID = 1L;
	private List<ContactVO> contactVOs;

	public List<ContactVO> getContactVOs()
	{
		return contactVOs;
	}

	public void setContactVOs(List<ContactVO> contactVOs)
	{
		this.contactVOs = contactVOs;
	}
}
