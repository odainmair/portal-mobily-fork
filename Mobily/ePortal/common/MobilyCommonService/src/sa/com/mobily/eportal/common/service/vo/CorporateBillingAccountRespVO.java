/*
 * Created on Nov 18, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CorporateBillingAccountRespVO extends BaseVO {

	private String firstName;

	private String lastName;

	private String midName;

	private String cellNumber;

	private String email;

	private String faxNum;

	private String gender;

	private String workNumber;

	private String birthdate;

	private String address;

	private String city;

	private String country;

	private String zipCode;

	private String billCreatedLogin;

	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Returns the billCreatedLogin.
	 */
	public String getBillCreatedLogin() {
		return billCreatedLogin;
	}

	/**
	 * @param billCreatedLogin
	 *            The billCreatedLogin to set.
	 */
	public void setBillCreatedLogin(String billCreatedLogin) {
		this.billCreatedLogin = billCreatedLogin;
	}

	/**
	 * @return Returns the birthdate.
	 */
	public String getBirthdate() {
		return birthdate;
	}

	/**
	 * @param birthdate
	 *            The birthdate to set.
	 */
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	

	/**
	 * @return Returns the cellNumber.
	 */
	public String getCellNumber() {
		return cellNumber;
	}
	/**
	 * @param cellNumber The cellNumber to set.
	 */
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	/**
	 * @return Returns the city.
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return Returns the country.
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            The country to set.
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return Returns the faxNum.
	 */
	public String getFaxNum() {
		return faxNum;
	}

	/**
	 * @param faxNum
	 *            The faxNum to set.
	 */
	public void setFaxNum(String faxNum) {
		this.faxNum = faxNum;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the gender.
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            The gender to set.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the midName.
	 */
	public String getMidName() {
		return midName;
	}

	/**
	 * @param midName
	 *            The midName to set.
	 */
	public void setMidName(String midName) {
		this.midName = midName;
	}

	/**
	 * @return Returns the workNumber.
	 */
	public String getWorkNumber() {
		return workNumber;
	}

	/**
	 * @param workNumber
	 *            The workNumber to set.
	 */
	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}

	/**
	 * @return Returns the zipCode.
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @param zipCode
	 *            The zipCode to set.
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
}
