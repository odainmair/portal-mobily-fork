/*
 * Created on Feb 20, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MobilyPromotionVO {

    private ArrayList listOfSubscribedPromotion = null;
	private ArrayList listOfNotSubscribedpromotion = null;
	
    public static void main(String[] args) {
    }
    /**
     * @return Returns the listOfSubscribedPromotion.
     */
    public ArrayList getListOfSubscribedPromotion() {
        return listOfSubscribedPromotion;
    }
    /**
     * @param listOfSubscribedPromotion The listOfSubscribedPromotion to set.
     */
    public void setListOfSubscribedPromotion(ArrayList listOfSubscribedPromotion) {
        this.listOfSubscribedPromotion = listOfSubscribedPromotion;
    }
    /**
     * @return Returns the listOfNotSubscribedpromotion.
     */
    public ArrayList getListOfNotSubscribedpromotion() {
        return listOfNotSubscribedpromotion;
    }
    /**
     * @param listOfNotSubscribedpromotion The listOfNotSubscribedpromotion to set.
     */
    public void setListOfNotSubscribedpromotion(
            ArrayList listofUnSubscribedPromotion) {
        this.listOfNotSubscribedpromotion = listofUnSubscribedPromotion;
    }
}