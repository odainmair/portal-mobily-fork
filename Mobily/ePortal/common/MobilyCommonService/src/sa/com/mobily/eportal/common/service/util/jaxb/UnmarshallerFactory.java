package sa.com.mobily.eportal.common.service.util.jaxb;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import org.apache.commons.pool.KeyedPoolableObjectFactory;
import org.springframework.stereotype.Component;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/**
 * Pool of JAXB Unmarshallers.
 * 
 */
@Component
public class UnmarshallerFactory implements KeyedPoolableObjectFactory {
    private final static Map<Object, JAXBContext> JAXB_CONTEXT_MAP = new HashMap<Object, JAXBContext>();
    
    private static String class_name = UnmarshallerFactory.class.getName();

    private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
    @Override
	public void activateObject(Object arg0, Object arg1) throws Exception {}
    
	@Override
	public void destroyObject(Object arg0, Object arg1) throws Exception {}
	
	@Override
	public void passivateObject(Object arg0, Object arg1) throws Exception {}

	@Override
	public boolean validateObject(Object arg0, Object arg1) {
		return true;
	}
	
    /**
     * Create a new instance of Unmarshaller if none exists for the specified
     * key.
     * 
     * @param unmarshallerKey
     *            : Class used to create an instance of Unmarshaller
     */
    @SuppressWarnings("rawtypes")
    @Override
    public final Object makeObject(final Object unmarshallerKey) throws JAXBException{
        if (unmarshallerKey instanceof Class) {
            Class clazz = (Class) unmarshallerKey;
            // Retrieve or create a JACBContext for this key
            JAXBContext jc = JAXB_CONTEXT_MAP.get(unmarshallerKey);
            if (jc == null) {
                try {
                    jc = JAXBContext.newInstance(clazz);
                    // JAXB Context is thread safe, it can be reused, so let's store it for later
                    JAXB_CONTEXT_MAP.put(unmarshallerKey, jc);
                } catch (JAXBException e) {
                	executionContext.log(Level.SEVERE, "Exception while making unmarshaller object", class_name, "makeObject", e);
                    throw e;
                }
            }
            try {
                return jc.createUnmarshaller();
            } catch (JAXBException e) {
            	executionContext.log(Level.SEVERE, "General exception while making unmarshaller object", class_name, "makeObject", e);
            	throw e;
            }
        }
        return null;
    }
}
