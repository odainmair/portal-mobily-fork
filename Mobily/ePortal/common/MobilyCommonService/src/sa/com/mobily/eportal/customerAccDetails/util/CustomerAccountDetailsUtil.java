package sa.com.mobily.eportal.customerAccDetails.util;

import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.SerializationUtils;

import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerAccDetails.constants.CustomerAccDetailsConstants;
import sa.com.mobily.eportal.customerAccDetails.dao.CustomerAccDetailsDAO;
import sa.com.mobily.eportal.customerAccDetails.jms.CustomerAccDetailsJMSRepository;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerAccDetailsRequestVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerAccDetailsVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerDetailsFinalVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerDetailsVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerHeaderVO;

public class CustomerAccountDetailsUtil implements CustomerAccDetailsConstants
{
	
public static final String CUSTOMER_ACC_DETAILS_LOGGER_NAME ="MobilyCommonService/customerAccDetails";
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CUSTOMER_ACC_DETAILS_LOGGER_NAME);
	private static String className = CustomerAccountDetailsUtil.class.getName();
	
	private static class CustomerAccountDetailsUtilHolder {
		private static final CustomerAccountDetailsUtil customerAccountDetailsUtilInstance = new CustomerAccountDetailsUtil();
	}
	
	public static CustomerAccountDetailsUtil getInstance() {
		return CustomerAccountDetailsUtilHolder.customerAccountDetailsUtilInstance;
	}
	public    CustomerAccDetailsVO getConsumerAccountDetails(String idType,String id){
		String method = "getConsumerAccountDetails";
    //	executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccount details > id:: "+id, className, method);
		CustomerAccDetailsVO replyVO = null;
    	CustomerDetailsFinalVO detailsFinalReply = null;
    	try{
    		boolean status = false;
    		System.out.println("readched");
			if(FormatterUtility.isNotEmpty(id)) {
				
				CustomerDetailsVO customerDetails  = CustomerAccDetailsDAO.getInstance().getCustomerDetails(idType,id);
				
				if(customerDetails !=null ){
					// Data available in DB
					executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > data exists in db", className, "getConsumerProfile");
					executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > ::"+customerDetails.getIdNumber(), className, method);
					if(isCustomerrDetailsIsExpired(customerDetails.getCreatedTimestamp())){ // Data available but expired
					
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > data is expired", className, "getConsumerProfile");
						replyVO = getCustomerAccDetails(idType,id); //get data from back end
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Fetching data from MOBILY.FUNC.WSGATEWAY.REQ inquiry "+replyVO, className, method);
						
						if (replyVO != null && replyVO.getHeaderVO().getErrorCode().equalsIgnoreCase("0000") && FormatterUtility.isEmpty(replyVO.getErrorSpecCode())) {	// check errorCode from backend			
							
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >" , className, method);
							 // On Success only update DB
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > success reply from backend: ", className, method);
								updateCustomerDetails(replyVO);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Updated the customer profile in db successfully", className, method);								
														
						} else {
							//Data is available in DB but expired and from back-end timeout/failure scenario, need to send the same data from DB
							detailsFinalReply = (CustomerDetailsFinalVO) SerializationUtils.deserialize(customerDetails.getCustomerDetailsData());		

							if(detailsFinalReply != null) {															
								replyVO = getfinalDesObj(detailsFinalReply,replyVO); 
								
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing ServiceAccountNo:"+replyVO.getAccountNumber(), className, method);	
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing ID TYPE :"+replyVO.getEECCIDDocType(), className, method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing ID Number: "+replyVO.getEECCIDNumber(), className, method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Customer Category: "+replyVO.getEECCCustomerCategory(), className,method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Customer Type: "+replyVO.getEECCCustomerType(), className, method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Name: "+replyVO.getName(), className, method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Blacklist Number: "+replyVO.getEECCBlackList(), className, method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Language Preference: "+replyVO.getEECCEALanguagePreference(), className,method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Fingerprint Status: "+replyVO.getFingerprintStatus(), className, method);
							
							}						
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > No Reply from EAI, return same data from DB ", className, method);
						}

					}else{
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > ConsumerDetails is not expired, returning the customerinfo from DB", className, method);
						if(customerDetails.getCustomerDetailsData()!=null){
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > ConsumerDetails is not null in db", className, method);							
							
							detailsFinalReply = (CustomerDetailsFinalVO) SerializationUtils.deserialize(customerDetails.getCustomerDetailsData());
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > after detailsFinalReply: "+detailsFinalReply, className, method);
							if(detailsFinalReply != null) {																
									replyVO = getfinalDesObj(detailsFinalReply,replyVO);
									if(replyVO != null){
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing ServiceAccountNo:"+replyVO.getAccountNumber(), className, method);	
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing ID TYPE :"+replyVO.getEECCIDDocType(), className, method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing ID Number: "+replyVO.getEECCIDNumber(), className, method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Customer Category: "+replyVO.getEECCCustomerCategory(), className,method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Customer Type: "+replyVO.getEECCCustomerType(), className, method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Name: "+replyVO.getName(), className, method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Blacklist Number: "+replyVO.getEECCBlackList(), className, method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Language Preference: "+replyVO.getEECCEALanguagePreference(), className,method);
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails >>> after deserializing Fingerprint Status: "+replyVO.getFingerprintStatus(), className, method);
									
									}
								}
							
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > replyVO "+replyVO, className, method);
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Returning the customer details from DB", className, method);
						}else{
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > ConsumerProfile is null in db", className, method);
							replyVO = getCustomerAccDetails(idType, id);
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Fetched the consumer profie from MOBILY.INQ.CUST.LINE.INFO.REQ inquiry "+replyVO, className, method);
							
							if (replyVO != null && replyVO.getHeaderVO().getErrorCode().equalsIgnoreCase("0000") && FormatterUtility.isEmpty(replyVO.getErrorSpecCode())) {				
								
								
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > replyVO:: "+replyVO, className, method);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > success reply from backend:: ", className, method);
									detailsFinalReply = getFinalSerObj(replyVO);
									if(detailsFinalReply != null){
										customerDetails.setCustomerDetailsData(SerializationUtils.serialize(detailsFinalReply));
										status = CustomerAccDetailsDAO.getInstance().updateCustomerDetails(customerDetails);
									}else{
										executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Iuuse occured while getting serialized obj"+status, className, method);
									}
									executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Update the ConsumerProfile profile for the SAN in db > status::"+status, className, method);									
							}else {
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Reply not success from EAI, do not update db, return same reply from backend ", className, method);
							}							
							
							
						}
					}
				}else {          /// if Consumer profile doesn't exists in DB
					executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > CustomerDetails is not exists in db (NULL)", className, method);
					replyVO = getCustomerAccDetails(idType, id);
    		
    		// for testing
				/*	replyVO = new CustomerAccDetailsVO();
				CustomerHeaderVO customerHeaderVO = new CustomerHeaderVO();
				customerHeaderVO.setErrorCode("0000");
					replyVO.setHeaderVO(customerHeaderVO);
					
					replyVO.setAccountNumber("12345");
					replyVO.setExceptionFlag("0");
					replyVO.setFingerprintStatus("N");
					replyVO.setEECCBlackList("N");
					replyVO.setEECCCustomerCategory("Individual");
					replyVO.setEECCCustomerType("Consumer");
					replyVO.setEECCEALanguagePreference("E");
					replyVO.setEECCIDDocExpDate("08/22/2019 00:00:00");
					replyVO.setEECCIDDocExpHDate("20/12/1440");
					replyVO.setEECCIDDocIssueDate("08/09/2017 00:00:00");
					replyVO.setEECCIDDocIssueHDate("16/11/1438");
					replyVO.setEECCIDDocIssuePlace("dfada");
					replyVO.setEECCIDDocType("GCC ID");
					replyVO.setEECCIDNumber("380571");
					replyVO.setEEMBLAlelmLastSyncStatus("");
					replyVO.setAlelmOnlineOverallStatus("Not Applicable");
					replyVO.setUpdatedByAlElm("N");
					replyVO.setHouseholdHeadOccupation("Professional");
					replyVO.setIntegrationId("5000541597");
					replyVO.setLocation("GCC ID380571");
					replyVO.setName("NIRAJ QA");
					replyVO.setPrimaryAddressId("1-L7KE");
					replyVO.setPrimaryContactId("1-BK9QSSD");
					replyVO.setPrimaryOrganization("IT Test Organization, IT Test Organization");
					replyVO.setListOfCutServiceSubAccounts("");
					ListOfAddressVO listOfAddressVO = new ListOfAddressVO();
					List<AddressVO> addressVOs = new ArrayList<AddressVO>();
					AddressVO addressVO1 = new AddressVO();
						addressVO1.setId("1-BK9QSSI");
						addressVO1.setIsPrimaryMVG("N");
						addressVO1.setStreetAddress("N/A");
						addressVO1.setCountry("Saudi Arabia");
						addressVO1.setCity("Riyadh");
						addressVO1.setEECCPOBox("0");
						addressVO1.setEECCPostalCode("0");
					addressVOs.add(addressVO1);
					AddressVO addressVO2 = new AddressVO();
					addressVO2.setId("1-L3NX");
					addressVO2.setIsPrimaryMVG("N");
					addressVO2.setStreetAddress("PL18091815170651200269 FT180926144332132 FT180927102925776");
					addressVO2.setCountry("Saudi Arabia");
					addressVO2.setCity("Jeddahh");
					addressVO2.setEECCPOBox("3245");
					addressVO2.setEECCPostalCode("00602");
				addressVOs.add(addressVO2);
				listOfAddressVO.setAddressVOs(addressVOs);
				replyVO.setListOfAddressVO(listOfAddressVO);
				ListOfContactVO listOfContactVO = new  ListOfContactVO();
				List<ContactVO> contactVOs = new ArrayList<ContactVO>();
				ContactVO contact = new ContactVO();	
					
				contact.setId("1-BK9QSSD");
				contact.setIsPrimaryMVG("Y");
				contact.setMM("MR.");
				contact.setMF("M");
				contact.setFirstName("NIRAJ");
				contact.setMiddleName("");
				contact.setLastName("MODI");
				contact.setEECCNationality("Oman");
				contact.setEECCDOBFormat("MELADI");
				contact.setEECCBirthDateHijri("03/10/1401");
				contact.setBirthDate("08/04/1981");
				contact.setContactPreference("Email");
				contact.setCellularPhone("+966560329764");
				contact.setWorkPhone2("+966560329764");
				contactVOs.add(contact);
				listOfContactVO.setContactVOs(contactVOs);
				replyVO.setListOfContactVO(listOfContactVO);
				
					System.out.println("go tpo method");*/
					if(replyVO!=null){
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil >error code::>"+ replyVO.getHeaderVO().getErrorCode(), className, method);
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > ErrorSpecCode ::>"+ replyVO.getErrorSpecCode(), className, method);
						
						//System.out.println("1111111");
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Got the profile EAI ", className, method);
						if (replyVO != null && replyVO.getHeaderVO().getErrorCode().equalsIgnoreCase("0000") && FormatterUtility.isEmpty(replyVO.getErrorSpecCode())) {	
							//System.out.println("2222222");
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > no error code from backend", className, method);
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > success reply from backend--- ", className, method);
								status = insertCustomerDetails(replyVO);
								executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Got the profile from CustomerInfo Inquiry & Insert the record in db successfully -> status:: "+status, className, method);
													
						}else{
							executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > Reply not success from EAI, not insert into db, return same reply from backend errorCode:: "+replyVO.getErrorSpecCode(), className, method);
						}
						
					}else{
						executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerAccountDetails > customerDetails is  not available from MOBILY.FUNC.WSGATEWAY.REQ CustomerInfo Inquiry", className, method);
					}
				}
			}else{ // If ID Number is empty
				executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getConsumerProfile > ID number is empty", className, method);
			}
		}catch(Exception e){
			executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > getConsumerProfile > Exception while fetching the customer information ::"+e.getMessage(), className, method, e);
		}
		return replyVO;
	}
	 public  boolean  insertCustomerDetails(CustomerAccDetailsVO accDetailsVO){
		 boolean status =false;
		 CustomerDetailsFinalVO cfinal = null;
		 String method = "insertCustomerDetails";
		 
		 try{
			 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > accDetailsVO ::"+accDetailsVO, className, method);
			 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > ID Type ::"+accDetailsVO.getEECCIDDocType(), className, method);
			 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > ID Number ::"+accDetailsVO.getEECCIDNumber(), className, method);
			 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > Account number ::"+accDetailsVO.getAccountNumber(), className, method);
			CustomerDetailsVO customerDetails  = new CustomerDetailsVO();
			customerDetails.setIdType(accDetailsVO.getEECCIDDocType());
			customerDetails.setIdNumber(accDetailsVO.getEECCIDNumber());			
			customerDetails.setAccountNumber(accDetailsVO.getAccountNumber());
			//System.out.println("insert customerDetails:: "+customerDetails);
			try{
				executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > Before serializing the customer info", className, method);
				//System.out.println("befre serializing accDetailsReplyVO:: "+accDetailsReplyVO);
				cfinal = getFinalSerObj(accDetailsVO);
				//System.out.println("after serializing accDetailsReplyVO:: "+cfinal);
				executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > after serializing --cfinal "+cfinal, className, method);
				if(cfinal != null){
					customerDetails.setCustomerDetailsData(SerializationUtils.serialize(cfinal));
					
					//System.out.println("customerDetails Date>> "+ customerDetails.getCustomerDetailsData());
					status = CustomerAccDetailsDAO.getInstance().saveCustomerDetailsData(customerDetails);
					executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > after serializing --status "+status, className, method);
				}else {
					executionContext.log(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > issue occured while serializing the consumer :", className, method);	
				}
			}catch(Exception e){
				executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > insertCustomerDetails > Exception while serializing the consumer info VO ::"+e.getMessage(), className, method, e);
			}
			
			executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > Insert the record successfully ", className, method);
		 }catch(Exception e){
			 executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > insertCustomerDetails > Exception while inserting the customer details Profile in DB "+e.getMessage(), className, method, e); 
		 }
		executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > insertCustomerDetails > status::"+status, className, method);
		 return status;
	 }
	 private  CustomerDetailsFinalVO getFinalSerObj(CustomerAccDetailsVO accDetailsVO){
		 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getFinalSerObj > Start ", className, "getFinalSerObj");
		 CustomerDetailsFinalVO cfinal = null;
		 	try
			{
		 		CustomerHeaderVO ch = new CustomerHeaderVO();				
				BeanUtils.copyProperties(ch, accDetailsVO.getHeaderVO());
				cfinal = new CustomerDetailsFinalVO();
				BeanUtils.copyProperties(cfinal, accDetailsVO);
				cfinal.setHeaderVO(ch);
				//System.out.println("cfinal >>" +cfinal);
				executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getFinalSerObj > cfinal "+cfinal, className, "getFinalSerObj");
			}
			catch (IllegalAccessException e)
			{
				executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > getFinalSerObj > Exception while getting final serilized Obj::"+e.getMessage(), className, "getFinalSerObj", e);
			}
			catch (InvocationTargetException e)
			{
				executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > getFinalSerObj > Exception while getting final serilized Obj::"+e.getMessage(), className, "getFinalSerObj", e);			
			}
		 	return cfinal;
		 
	 }
	 private CustomerAccDetailsVO getfinalDesObj(CustomerDetailsFinalVO detailsFinalVO, CustomerAccDetailsVO accDetailsVO){
		 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getfinalDeserializedObj > Start ", className, "getfinalDesObj");
		 	try
			{
		 		//System.out.println("detailsFinalVO>>>"+ detailsFinalVO);
		 		CustomerHeaderVO customerHeaderVO = new  CustomerHeaderVO();		
		 		BeanUtils.copyProperties(customerHeaderVO, detailsFinalVO.getHeaderVO());
				accDetailsVO = new CustomerAccDetailsVO();
				BeanUtils.copyProperties(accDetailsVO, detailsFinalVO);
				accDetailsVO.setHeaderVO(customerHeaderVO);
				//System.out.println("accDetailsReplyVO::>"+ accDetailsVO);
				executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getfinalDesObj > End "+accDetailsVO, className, "getfinalDesObj");
			}
			catch (IllegalAccessException e)
			{
				 executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > getfinalDesObj > Exception while getting final Deserilized Obj::"+e.getMessage(), className, "getfinalDesObj", e);
			}
			catch (InvocationTargetException e)
			{
				 executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > getfinalDesObj > Exception while getting final Deserilized Obj::"+e.getMessage(), className, "getfinalDesObj", e);
			}
			return accDetailsVO;
	 }
	
	public void updateCustomerDetails(CustomerAccDetailsVO accDetailsVO){
		String method = "updateCustomerDetails";
		CustomerDetailsFinalVO cfinal = null;
		 
		try{
			
			 CustomerDetailsVO customerDetails  = CustomerAccDetailsDAO.getInstance().getCustomerDetails(accDetailsVO.getEECCIDDocType(), accDetailsVO.getEECCIDNumber());
			 
			 String idNumb = accDetailsVO.getEECCIDNumber();
			 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > updateCustomerDetails > id type::"+accDetailsVO.getEECCIDDocType() +" id :: "+idNumb, className, method);
			 boolean status =false;
			 if(customerDetails!=null){
				 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > updateCustomerDetails > ID Number from Customer Details from DB :: "+customerDetails.getIdNumber(), className, method);
				 if(customerDetails.getIdNumber().equals(idNumb)){
					 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > updateCustomerDetails >   both ID's are same", className, method);
					 try{
						
						 cfinal = getFinalSerObj(accDetailsVO);
							if(cfinal != null){						
								executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > updateCustomerDetails >- after serializing the customer info VO:"+ customerDetails, className, method);
								customerDetails.setCustomerDetailsData(SerializationUtils.serialize(cfinal));
								 status = CustomerAccDetailsDAO.getInstance().updateCustomerDetails(customerDetails);
							} else {
								 executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > updateCustomerDetails > Issue occurred while serializing the customer info VO ::", className, method);
							}
					 }catch(Exception e){
						 executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > updateCustomerDetails > Exception while serializing the customer info VO ::"+e.getMessage(), className, method, e);
					 }
					 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > updateCustomerDetails > updated the record successfully -> status:: "+status, className, method);
				 }else{
					 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > updateCustomerDetails >   both ID's are not same", className, method);
					 status = insertCustomerDetails(accDetailsVO);
					 executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > updateCustomerDetails > Insert the record successfully -> status::"+status, className, method);
				 }
			 }
		 }
		 catch(Exception e){
			 executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil > updateCustomerDetails > Exception while fetching the customer information from DB ::"+e.getMessage(), className, method, e);
		 }
	 }
	
	 private  CustomerAccDetailsVO getCustomerAccDetails(String idType,String id)
	 {
		 String method = "getCustomerAccDetails";
		 	executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > getCustomerAccDetails > id:: "+id, className, method);
		 	CustomerAccDetailsRequestVO requestVO = new CustomerAccDetailsRequestVO();
			requestVO.setIdType(idType);
			requestVO.setIdNumber(id);
			CustomerAccDetailsJMSRepository customerInfo = new CustomerAccDetailsJMSRepository();			
			return customerInfo.getCustomerDetails(requestVO);								
   }
	public  boolean isCustomerrDetailsIsExpired(Timestamp passExpDate){
		String methodName = "isConsumerDetailsIsExpired";
    	Timestamp currTimeStamp = new Timestamp(new  java.util.Date().getTime());
    	Calendar cal = GregorianCalendar.getInstance();
    	executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > isCustomerrDetailsIsExpired > after passExpDate:: "+passExpDate, className, methodName);
    	executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > isCustomerrDetailsIsExpired > currTimeStamp:: "+currTimeStamp, className, methodName);
    	try {
    		if(passExpDate!=null){
        		cal.setTimeInMillis(passExpDate.getTime());
        		//String expDays = (String)ResourceEnvironmentProviderService.getInstance().getPropertyValue("USER_ACC_DETAILS_EXP_DAYS");
        		String expDays = "3";
        		if(FormatterUtility.isNumber(expDays)){
        			//cal.add(Calendar.DATE, Integer.parseInt(expDays));
        			cal.add(Calendar.MINUTE, Integer.parseInt(expDays));
        		}
        		else{
        			cal.add(Calendar.DATE, 365);
        		}
        		passExpDate =  new Timestamp(cal.getTimeInMillis());
        		executionContext.audit(Level.INFO, "CustomerAccountDetailsUtil > isCustomerrDetailsIsExpired > passExpDate:: "+passExpDate, className, methodName);
        		if(passExpDate.before(currTimeStamp)){
            		return true;
            	}
            	
        	}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerAccountDetailsUtil>> isCustomerrDetailsIsExpired >>Exception while checking the date is expired: " + e.getMessage(), className, methodName, e);
		}
		return false;    	
	 }
	
	/*public static void main(String[] args){
		String idType="";
		String id="123";
		System.out.println("started");
		CustomerAccDetailsVO replyVO= getConsumerAccountDetails( idType, id);
	}*/
}
