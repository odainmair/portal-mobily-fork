package sa.com.mobily.eportal.billing.util;

import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;

public class SubscriptionMappingUtil
{

	public static boolean isMSISDNSubscription(int subscriptionType){
		if(subscriptionType==SubscriptionTypeConstant.SUBS_TYPE_GSM||
				subscriptionType==SubscriptionTypeConstant.SUBS_TYPE_CONNECT)
			return true;
		return false;
	}
}
