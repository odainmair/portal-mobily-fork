package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import sa.com.mobily.eportal.common.service.vo.UserRedirectionVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

/**
 *  This Class is a DAO implementation for storing and fetching eSales redirection url during the process of the registration 
 *  @author r.agarwal.mit 
 **/

public class UserRedirectionDAO extends AbstractDBDAO<UserRedirectionVO>{
	
	public static final String FIND_REDIRECTION_URL = "SELECT * FROM eSales_User_redirection where USER_ACCT_ID = ?";
	
	public static final String INSERT_REDIRECTION_URL = "INSERT INTO eSales_User_redirection(ID, USER_ACCT_ID, REDIRECT_URL, CREATED_DATE)"+ 
    "VALUES(SEQ_ESALES_USER_REDIRECTION.nextval, ?, ?, CURRENT_TIMESTAMP)";
	
	public static final String DELETE_REDIRECTION_URL = "DELETE FROM eSales_User_redirection where USER_ACCT_ID = ?";
	
    
	private static UserRedirectionDAO instance = new UserRedirectionDAO();
	
	protected UserRedirectionDAO() {
		super(DataSources.DEFAULT);
	}
	
	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>UserRedirectionVO</code>
	 * @author r.agarwal.mit
	 */
	public static UserRedirectionDAO getInstance() {
		return instance;
	}
	
	/**
	 * This method returns the Redirection URL for the selected userAcctId
	 * 
	 * @param request <code>Long userAcctId</code>
	 * @return <code>UserRedirectionVO</code>
	 * @author r.agarwal.mit
	 */
	public UserRedirectionVO getRedirectionURL(Long userAcctId) {
		UserRedirectionVO dto = null;
		List<UserRedirectionVO> userRedirectionVOList = query(FIND_REDIRECTION_URL, userAcctId);
		if(userRedirectionVOList != null && userRedirectionVOList.size() > 0) {
			dto = userRedirectionVOList.get(0);
		}
		return dto;
	}
	
	/**
	 * This method saves eSales URL
	 * 
	 * @param request <code>UserRedirectionVO</code> to be saved
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author r.agarwal.mit
	 */
	public int saveRedirectionURL(UserRedirectionVO userRedirectionVO){
		return update(INSERT_REDIRECTION_URL, userRedirectionVO.getUserAcctId(), userRedirectionVO.getRedirectURL());
	}
	
	/**
	 * This method deletes eSales URL
	 * 
	 * @param request <code>userAcctId</code> to be deleted
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author r.agarwal.mit
	 */
	public int deleteRedirectionURLByUserAcctId(Long userAcctId){
		return update(DELETE_REDIRECTION_URL, userAcctId);
	}
	
	
	/**
	 * 
	 * @param request <code>ResultSet rs</code>
	 * @return <code>UserRedirectionVO</code>
	 * @author r.agarwal.mit
	 */
	@Override
	protected UserRedirectionVO mapDTO(ResultSet rs) throws SQLException {
		UserRedirectionVO userRedirectionVO = new UserRedirectionVO();
		userRedirectionVO.setId(rs.getLong("ID"));
		userRedirectionVO.setUserAcctId(rs.getLong("USER_ACCT_ID"));
		userRedirectionVO.setRedirectURL(rs.getString("REDIRECT_URL"));
		userRedirectionVO.setCreationDate(rs.getTimestamp("CREATED_DATE"));
		
		return userRedirectionVO;
	}
}
