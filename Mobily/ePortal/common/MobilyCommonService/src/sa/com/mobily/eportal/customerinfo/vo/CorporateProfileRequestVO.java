package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER"/>
 *         &lt;element ref="{}KeyType"/>
 *         &lt;element ref="{}KeyValue"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "keyType",
    "keyValue"
})
@XmlRootElement(name = "MOBILY_BSL_SR")
public class CorporateProfileRequestVO {

    @XmlElement(name = "SR_HEADER", required = true)
    protected HeaderVO headerVO;
    @XmlElement(name = "KeyType", required = true)
    protected String keyType;
    @XmlElement(name = "KeyValue", required = true)
    protected String keyValue;

    public HeaderVO getHeaderVO()
	{
		return headerVO;
	}

	public void setHeaderVO(HeaderVO headerVO)
	{
		this.headerVO = headerVO;
	}

	/**
     * Gets the value of the keyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyType() {
        return keyType;
    }

    /**
     * Sets the value of the keyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyType(String value) {
        this.keyType = value;
    }

    /**
     * Gets the value of the keyValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyValue() {
        return keyValue;
    }

    /**
     * Sets the value of the keyValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyValue(String value) {
        this.keyValue = value;
    }

}
