package sa.com.mobily.eportal.core.api;

import java.io.Serializable;

public class BackEndService implements Serializable {

	private static final long serialVersionUID = 3750982654894327461L;

	private long id;

	private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}