package sa.com.mobily.eportal.common.exception.portletfilter;

import javax.portlet.RenderRequest;
import javax.portlet.filter.RenderRequestWrapper;

public class XSSRenderRequestWrapper extends RenderRequestWrapper
{
	public XSSRenderRequestWrapper(RenderRequest request)
	{
		super(request);
		System.out.println("XSSRenderRequestWrapper2 ***");
	}

	public String[] getParameterValues(String parameter)
	{
		String[] values = super.getParameterValues(parameter);
		System.out.println("XSSRenderRequestWrapper2 getParameterValues ***");
		if (values == null)
		{
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for (int i = 0; i < count; i++)
		{
			encodedValues[i] = cleanXSS(values[i]);
		}
		return encodedValues;
	}

	public String getParameter(String parameter)
	{
		String value = super.getParameter(parameter);
		System.out.println("**** Param Filter: getParameter " + parameter);
		if (value == null)
		{
			return null;
		}
		return cleanXSS(value);
	}

	private String cleanXSS(String value)
	{
		return SecurityUtils.cleanXSS(value);
	}
}
