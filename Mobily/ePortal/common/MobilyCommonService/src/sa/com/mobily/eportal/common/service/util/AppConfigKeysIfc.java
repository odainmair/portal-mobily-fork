/*
 * Created on May 16, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

/**
 * @author aghareeb
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface AppConfigKeysIfc {    
	public static final String DATE_FORMAT_AR            = "DATE_FORMAT_AR";
	public static final String DATE_FORMAT               = "DATE_FORMAT";
	public static final String CALLS_DATE_FORMAT         = "CALLS_DATE_FORMAT";
	public static final String CALLS_DATE_FORMAT_AR      = "CALLS_DATE_FORMAT_AR";
	public static final String LAST_LOGIN_DATE_FORMAT    = "LAST_LOGIN_DATE_FORMAT";
	public static final String LAST_LOGIN_DATE_FORMAT_AR = "LAST_LOGIN_DATE_FORMAT_AR";
	public static final String BILLING_PORTAL_DATA_SOURCE_NAME = "BILLING_PORTAL_DATA_SOURCE_NAME";
	public static final String EEDB_DATA_SOURCE_NAME = "EEDB_DATA_SOURCE_NAME";
	public static final String CREDIT_DATA_SOURCE_NAME = "CREDIT_DATA_SOURCE_NAME";
	public static final String BSL_DATA_SOURCE_NAME = "BSL_DATA_SOURCE_NAME";
	public static final String BILLS_DATA_DATA_SOURCE_NAME="BILLS_DATA_DATA_SOURCE_NAME";
	public static final String REGISTRATION_EMAIL_CONFIRMATION_URL = "REGISTRATION_EMAIL_CONFIRMATION_URL";           
	public static final String REGISTRATION_EMAIL_SUBJECT="REGISTRATION_EMAIL_SUBJECT";
	public static final String REGISTRATION_EMAIL_SENDER="REGISTRATION_EMAIL_SENDER";
	public static final String REGISTRATION_EMAIL_BODY="REGISTRATION_EMAIL_BODY";
	public static final String REGISTRATION_EMAIL_BODY_KEY_01="{0}";
	public static final String REGISTRATION_EMAIL_BODY_KEY_02="{1}";
	public static final String REGISTRATION_EMAIL_ALLOWANCE_DAYS="REGISTRATION_EMAIL_ALLOWANCE_DAYS";
	public static final String REGISTRATION_EMAIL_SUBJECT_AR="REGISTRATION_EMAIL_SUBJECT_AR";
	public static final String REGISTRATION_EMAIL_BODY_AR="REGISTRATION_EMAIL_BODY_AR";
	public static final String FOOT_PRINT_ENABLED="FOOTPRINT_SERVICE_ENABLED";
	public static final String FOOT_PRINT_QUEUE_NAME="FOOT_PRINT_QUEUE_NAME";
	public static final String VANITY_EMAIL_CONFIRMATION_URL = "VANITY_EMAIL_CONFIRMATION_URL";           
	public static final String VANITY_EMAIL_SUBJECT="VANITY_EMAIL_SUBJECT";
	public static final String VANITY_EMAIL_SENDER="VANITY_EMAIL_SENDER";
	public static final String VANITY_EMAIL_BODY="VANITY_EMAIL_BODY";
	public static final String VANITY_EMAIL_BODY_KEY_01="{0}";
	public static final String VANITY_EMAIL_BODY_KEY_02="{1}";
	public static final String VANITY_EMAIL_ALLOWANCE_DAYS="VANITY_EMAIL_ALLOWANCE_DAYS";
	public static final String VANITY_EMAIL_SUBJECT_AR="VANITY_EMAIL_SUBJECT_AR";
	public static final String VANITY_EMAIL_BODY_AR="VANITY_EMAIL_BODY_AR";
	public static final String GLOBYS_EBILL_USER_NAME = null;
	public static final String GLOBYS_EBILL_PASSWORD = null;
	public static final String GLOBYS_EBILL_URL = null;
	
	
	
	
	
	
	
	public static final String PSW_SEND_EMAIL_SUBJECT_AR ="PSW_SEND_EMAIL_SUBJECT_AR";
	public static final String  PSW_SEND_EMAIL_BODY_AR ="PSW_SEND_EMAIL_BODY_AR";
	public static final String PSW_SEND_EMAIL_SUBJECT ="PSW_SEND_EMAIL_SUBJECT";
	public static final String PSW_SEND_EMAIL_BODY="PSW_SEND_EMAIL_BODY";
	
	//TAM
	public static final String TAM_ADMIN_USER_NAME="TAM_ADMIN_USER_NAME";
	public static final String TAM_ADMIN_USER_PASSWORD="TAM_ADMIN_USER_PASSWORD";
	public static final String TAM_CONFIG_FILE_URL="TAM_CONFIG_FILE_URL";
	public static final String LDAP_SUFFIX="LDAP_SUFFIX";

	//LDAP 
	public static final String LDAP_SERVER="LDAP_SERVER";
	
}
