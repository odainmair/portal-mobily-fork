package sa.com.mobily.eportal.common.service.lookup;

import java.util.ArrayList;



import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.naming.InitialContext;

import com.ibm.websphere.cache.DistributedMap;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.lookup.dao.LookupsDOA;
import sa.com.mobily.eportal.common.service.util.DynamicComparator;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.lookup.vo.Lookup;

public class LookupDataManager implements LookupIdConstants
{
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String clazz = LookupDataManager.class.getName();
	
	private static DistributedMap lookupsMap;
	
	private LookupDataManager(){}
	
	public static List<Lookup> getGenericLookup(Long lookupId, Long subId, boolean isEnglishLocale){
		List<Lookup> resultList = new ArrayList<Lookup>();
		
		if(lookupId == null)
			return null;
		try{
			if(lookupsMap == null)
				lookupsMap = (DistributedMap) new InitialContext().lookup("cache/lookups/generic");
			
			@SuppressWarnings("unchecked")
			Map<Long, Lookup> lookupMap = (Map<Long, Lookup>) lookupsMap.get(lookupId);
			if(lookupMap == null){
				List<Lookup> lookupsList = LookupsDOA.getInstance().getLookupsByID(lookupId);
				if(lookupsList != null && lookupsList.size() > 0){
					lookupMap = new HashMap<Long, Lookup>();
					Lookup lookup = null;
					for (Iterator<Lookup> iterator = lookupsList.iterator(); iterator.hasNext();){
						lookup = (Lookup) iterator.next();
						lookupMap.put(lookup.getSubId(), lookup);	
						if(subId == null || lookup.getSubId() == subId.longValue())
							resultList.add(lookup);
					}
				}
				lookupsMap.put(lookupId, lookupMap);
			} else if(subId != null){
				resultList.add(lookupMap.get(subId));
			} else {
				for (Iterator<Lookup> iterator = lookupMap.values().iterator(); iterator.hasNext();){
					resultList.add(iterator.next());
				}
			}
			if(isEnglishLocale && resultList != null && resultList.size() > 0){
				DynamicComparator.sort(resultList, "itemDescriptionInEnglish", true);
			}
			executionContext.log(Level.INFO, "Lookup ID for inquirey  " + lookupId, clazz, "getGenericLookup", null);
			if(lookupMap != null)
				executionContext.log(Level.INFO, "Lookup size  " + lookupMap.size(), clazz, "getGenericLookup", null);
		}  catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "getGenericLookup", e);
			throw new RuntimeException(e);
		}
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Lookup> getCustomizedLookup(String lookupId, boolean isEnglishLocale){
		
		if(FormatterUtility.isEmpty(lookupId))
			return null;
		
		List<Lookup> lookupList = null;
		
		try{
			if(lookupsMap == null)
				lookupsMap = (DistributedMap) new InitialContext().lookup("cache/lookups/generic");
			
			lookupList = (List<Lookup>) lookupsMap.get(lookupId);
			
			if(lookupList == null){
				lookupList = LookupsDOA.getInstance().getCustomizedLookup(lookupId);
				lookupsMap.put(lookupId, lookupList);
			}
			
			// commented by MHASSAN
			/*
			if(isEnglishLocale && lookupList != null && lookupList.size() > 0){
				DynamicComparator.sort(lookupList, "itemDescriptionInEnglish", true);
			}
			*/
			
			
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "getGenericLookup", e);
			throw new RuntimeException(e);
		}
		return lookupList;
	}
}