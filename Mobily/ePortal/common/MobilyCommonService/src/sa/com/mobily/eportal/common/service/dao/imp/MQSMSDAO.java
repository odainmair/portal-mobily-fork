/*
 * Created on Feb 20, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.SMSDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mail.ApacheMailSender;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.sms.vo.SMSVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.EmailVO;



import com.mobily.exception.mq.MQSendException;

/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQSMSDAO implements SMSDAO,ConstantIfc
{
	private static final Logger log = LoggerInterface.log;
	
	public void sendSMS( String aRequest ) 
	{
		String tempQueueName             = null;
		String tempReplyQueueName        = null;
		log.debug("MQSMSDAO > MQSMSDAO >Called [ aRequest ]");
		try 
		{
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String s[] = aRequest.split(";");
	    	String msgId=  s[1];
	    	log.debug("msgId::"+msgId);
           if(msgId.equals(SUPPLEMENTARY_DATA_PIN_CODE_SMS_ID_EN) || msgId.equals(SUPPLEMENTARY_DATA_PIN_CODE_SMS_ID_AR)){
				tempQueueName      = MobilyUtility.getPropertyValue( SMS_SUPPLEMENTARY_DATA_REQUEST_QUEUENAME  );
			}else{
				tempQueueName      = MobilyUtility.getPropertyValue( SMS_REQUEST_QUEUENAME  );	
				}
		    
		    tempReplyQueueName = MobilyUtility.getPropertyValue( SMS_RESPONSE_QUEUENAME );
		    
			log.debug("MQSMSDAO, Request Queue Name: " + tempQueueName );
						
			//Get reply Queu manager Name
			//tempReplyQueueManagerName = MobilyUtility.getPropertyValue( QUEUE_MANAGER_NAME_IN_CLUSTER );
			
			
			MQConnectionHandler.getInstance().sendToMQ( aRequest, 
														 tempQueueName);
			
			/*try
			{
				if(MobilyUtility.getPropertyValue("smsHandler.sendEmail") != null && MobilyUtility.getPropertyValue("smsHandler.sendEmail").equals("true")){
					sendEmail(aRequest);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			log.debug("MQSMSDAO, The request was sent successfully" );*/
		} 
		catch (Exception e) {
			log.fatal("MQSMSDAO > sendSMS >Exception " + e);
			throw new SystemException(e);
		}
	}

	public void sendSMS( SMSVO smsVO ) {
		String tempQueueName             = null;
		log.debug("MQSMSDAO > MQSMSDAO >Called [ aRequest ]");
		try {
			tempQueueName      = MobilyUtility.getPropertyValue( SMS_REQUEST_QUEUENAME  );
			if(FormatterUtility.isNotEmpty(smsVO.getQueueName())){
				tempQueueName  = smsVO.getQueueName();
			}
		
			log.debug("MQSMSDAO, Request Queue Name: " + tempQueueName );
			MQConnectionHandler.getInstance().sendToMQ( smsVO.getSmsRequest(), tempQueueName);
		} catch (Exception e) {
			log.fatal("MQSMSDAO > sendSMS >Exception " + e);
			throw new SystemException(e);
		}
	}

	
	/**
	 * Method for send email
	 * 
	 * @param receiverEmail
	 * @param occasionName
	 * @param occasionDate
	 * @param lang
	 * @throws EmailException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private static void sendEmail(String request)  {

		EmailVO emailVO = new EmailVO();
		
		
		emailVO.setSubject("SMS");
		emailVO.setMailBody(request);
		
		emailVO.setSender("donotreply@mobily.com.sa");
		

		ArrayList<String> recipientList = new ArrayList<String>();
		recipientList.add("josephmangaly@gmail.com");
		emailVO.setRecipients(recipientList);

		new ApacheMailSender().sendEmailAsHtml(emailVO);
	}
}




