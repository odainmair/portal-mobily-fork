package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.vo.IPhoneRequestVO;

/**
 * 
 * @author n.gundluru.mit
 *
 */

public interface IPhoneServiceDAO {

	public void getUserDetails(IPhoneRequestVO requestVo);
}
