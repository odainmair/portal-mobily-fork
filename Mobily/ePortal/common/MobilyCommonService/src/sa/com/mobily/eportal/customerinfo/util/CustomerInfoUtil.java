package sa.com.mobily.eportal.customerinfo.util;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author r.agarwal.mit
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerInfoUtil
{
	private static final Logger logger = Logger.getLogger("lib-customerInfoService");

	private static ResourceBundle bundle = null;

	private static final String configPropertyFileName = "/config";

	/**
	 * Get the property values from config properties
	 * 
	 * @param key
	 * @return value
	 */
	public static String getPropertyValue(String key)
	{
		String value = "";
		try
		{
			if (bundle == null)
			{
				bundle = ResourceBundle.getBundle(configPropertyFileName);
			}
			value = bundle.getString(key);
		}
		catch (Exception e)
		{
			logger.logp(Level.SEVERE, "CustomerInfoUtil", "getPropertyValue", "Exception while getting property from config file", e);
		}
		return value;
	}
}