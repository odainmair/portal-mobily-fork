package sa.com.mobily.eportal.customerAccDetails.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ContactVO extends BaseVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String Id;
	private String IsPrimaryMVG;
	private String MM;
	private String MF;
	private String FirstName;
	private String MiddleName;
	private String LastName;
	private String EECCNationality;
	private String EECCDOBFormat;
	private String EECCBirthDateHijri;
	private String BirthDate;
	private String ContactPreference;
	private String CellularPhone;
	private String WorkPhone2;
	public String getId()
	{
		return Id;
	}
	public void setId(String id)
	{
		Id = id;
	}
	public String getIsPrimaryMVG()
	{
		return IsPrimaryMVG;
	}
	public void setIsPrimaryMVG(String isPrimaryMVG)
	{
		IsPrimaryMVG = isPrimaryMVG;
	}
	public String getMM()
	{
		return MM;
	}
	public void setMM(String mM)
	{
		MM = mM;
	}
	public String getMF()
	{
		return MF;
	}
	public void setMF(String mF)
	{
		MF = mF;
	}
	public String getFirstName()
	{
		return FirstName;
	}
	public void setFirstName(String firstName)
	{
		FirstName = firstName;
	}
	public String getMiddleName()
	{
		return MiddleName;
	}
	public void setMiddleName(String middleName)
	{
		MiddleName = middleName;
	}
	public String getLastName()
	{
		return LastName;
	}
	public void setLastName(String lastName)
	{
		LastName = lastName;
	}
	public String getEECCNationality()
	{
		return EECCNationality;
	}
	public void setEECCNationality(String eECCNationality)
	{
		EECCNationality = eECCNationality;
	}
	public String getEECCDOBFormat()
	{
		return EECCDOBFormat;
	}
	public void setEECCDOBFormat(String eECCDOBFormat)
	{
		EECCDOBFormat = eECCDOBFormat;
	}
	public String getEECCBirthDateHijri()
	{
		return EECCBirthDateHijri;
	}
	public void setEECCBirthDateHijri(String eECCBirthDateHijri)
	{
		EECCBirthDateHijri = eECCBirthDateHijri;
	}
	public String getBirthDate()
	{
		return BirthDate;
	}
	public void setBirthDate(String birthDate)
	{
		BirthDate = birthDate;
	}
	public String getContactPreference()
	{
		return ContactPreference;
	}
	public void setContactPreference(String contactPreference)
	{
		ContactPreference = contactPreference;
	}
	public String getCellularPhone()
	{
		return CellularPhone;
	}
	public void setCellularPhone(String cellularPhone)
	{
		CellularPhone = cellularPhone;
	}
	public String getWorkPhone2()
	{
		return WorkPhone2;
	}
	public void setWorkPhone2(String workPhone2)
	{
		WorkPhone2 = workPhone2;
	}
	
}
