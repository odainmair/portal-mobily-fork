/*
 * Created on Apr 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.RoyalGuardReplyVO;
import sa.com.mobily.eportal.common.service.vo.RoyalGuardRequestVO;
import sa.com.mobily.eportal.common.service.vo.SubscriberVO;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RoyalGuardXMLHandler {
	private static final Logger log = LoggerInterface.log;

	
	/**
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	/*
	<MOBILY_BSL_SR>
		<SR_HEADER>
			<FuncId>FUNC_RG_SUBSCRIBER</FuncId>
			<SecurityKey>
			</SecurityKey>
			<MsgVersion>0000</MsgVersion>
			<RequestorChannelId>ePortal</RequestorChannelId>
			<SrDate>20100104220634</SrDate>
			<RequestorUserId>0566669993</RequestorUserId>
			<ServiceRequestId>ePortal20100104220634</ServiceRequestId>
		</SR_HEADER>
		<BillingAccount>107856351719</BillingAccount>
		<COUNT>Y/N</COUNT >
		<MSISDN></MSISDN >
		<FETCHTYPE>ALL/Single/RG/Temp</FETCHTYPE> - Output information will depend on fetchtype. To retrieve all information ALL has to be used.
		<INFO>BASIC</INFO>
	</MOBILY_BSL_SR>

	 */
	public String generateXMLRequest(RoyalGuardRequestVO requestVO) throws MobilyCommonException {
		String xmlRequest = "";
		try {
			log.debug("RoyalGuardXMLHandler > generateXMLRequest > called....");
			Document doc = new DocumentImpl();

			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
	        String srDate =MobilyUtility.FormateDate(new Date());
	        
			//gernerate Header element
			Element header = XMLUtility.openParentTag(doc, TagIfc.TAG_SR_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID, requestVO.getOperation());				
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY, "");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION, ConstantIfc.MESSAGE_VERSION_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE, srDate);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID, requestVO.getRequestorUserId());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SERVICE_REQUEST_ID, ConstantIfc.CHANNEL_ID_VALUE+srDate);
			XMLUtility.closeParentTag(root, header);
			
			//body
			if(ConstantIfc.ROYAL_GUARD_ALL_MSISDN_FUNC_VALUE.equalsIgnoreCase(requestVO.getOperation())) {
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_BILLING_ACCOUNT, requestVO.getBillingAccount());
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_COUNT, requestVO.getCount());
				
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_FETCHTYPE, requestVO.getFetchType());
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_INFO, requestVO.getInfo());
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, requestVO.getMsisdn());
			} else if(ConstantIfc.ROYAL_GUARD_BALANCE_MSISDN.equalsIgnoreCase(requestVO.getOperation())) {
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, requestVO.getMsisdn());
			} 
			
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
			log.debug("RoyalGuardXMLHandler > generateXMLRequest > XML generated....");
		} catch (Exception e) {
			log.fatal("RoyalGuardXMLHandler > generateXMLRequest > Exception ," + e.getMessage());
			throw new MobilyCommonException(e);
		}		
		return xmlRequest;		
	}
	
	/**
	 * 
	 * @param xmlReply
	 * @return
	 */
	/*
	 <MOBILY_BSL_SR>
		<SR_HEADER>
			<FuncId>FUNC_RG_SUBSCRIBER</FuncId>
			<SecurityKey>
			</SecurityKey>
			<MsgVersion>0000</MsgVersion>
			<RequestorChannelId>ePortal</RequestorChannelId>
			<SrDate>20100104220634</SrDate>
			<RequestorUserId>0566669993</RequestorUserId>
			<ServiceRequestId>ePortal20100104220634</ServiceRequestId>
		</SR_HEADER>
		<SubscriberList>
		<Subscriber>
		            <MSISDN>966540111111</MSISDN>
		            <UserName>966540111111</UserName>
		            <AccountNumber>966540111111</AccountNumber>
		            <Type>RG</Type>
		            <email>test@mobily.com.sa</email>
		</Subscriber>
		<Subscriber>
		            <MSISDN>966540111112</MSISDN>
		            <UserName>966540111111</UserName>
		            <AccountNumber>966540111111</AccountNumber>
		            <Type>Temp</Type>
		            <email>test@mobily.com.sa</email>
		</Subscriber>
		</SubscriberList>
	</MOBILY_BSL_SR>

	 */
	public RoyalGuardReplyVO parseXMLReply(String xmlReply) {
		log.debug("RoyalGuardXMLHandler > parseXMLReply > Called");
		RoyalGuardReplyVO replyVO = null;
		try {
			
			if (xmlReply == null || xmlReply == "")
				throw new MobilyCommonException();

			replyVO = new RoyalGuardReplyVO();
			Document doc = XMLUtility.parseXMLString(xmlReply);

			Node rootNode = doc.getElementsByTagName(
					TagIfc.TAG_MOBILY_BSL_SR_REPLY).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null){
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				}					
				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER)) {
						//
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUBSCRIBER_LIST)) {
					//log.debug("here l_Node="+l_Node);
					List subscriberVOList = getListOfSubscriberVOList(l_Node);
					replyVO.setSubscriberVOList(subscriberVOList);					
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)) {
					replyVO.setErrorCode(l_szNodeValue);
				} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)) {
					replyVO.setErrorMessage(l_szNodeValue);
				} 
			}
		}  catch (Exception e) {
			log.fatal("RoyalGuardXMLHandler > parseXMLReply > Exception , "	+ e);
			throw new SystemException(e);
		}
		return replyVO;
	}
	
	private List getListOfSubscriberVOList(Element l_Node) throws SystemException {
		List subscriberVOList = new ArrayList();
		log	.debug("RoyalGuardXMLHandler > getListOfSubscriberVOList > called....");
		SubscriberVO subscriberVO = null;
		try {
			NodeList subscribersNodeList = l_Node.getChildNodes();			
			for(int i=0; subscribersNodeList != null && i< subscribersNodeList.getLength(); i++){
				Element s_Node = (Element) subscribersNodeList.item(i);
				String s_szNodeTagName = s_Node.getTagName();
				subscriberVO = new SubscriberVO();
				if (s_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SUBSCRIBER)){
					NodeList minutesNodeList = s_Node.getChildNodes();				
					for (int j = 0; j < minutesNodeList.getLength(); j++) {					
						if ((minutesNodeList.item(j)).getNodeType() != Node.ELEMENT_NODE){
							continue;
						}
						Element c_Node = (Element) minutesNodeList.item(j);
						String p_szTagName = c_Node.getTagName();
						String l_szNodeValue = null;
						if (c_Node.getFirstChild() != null){
							l_szNodeValue = c_Node.getFirstChild().getNodeValue();
						}
						if (TagIfc.TAG_MSISDN.equalsIgnoreCase(p_szTagName)) {						
							subscriberVO.setMsisdn(l_szNodeValue);						
						} else if (TagIfc.TAG_USER_NAME.equalsIgnoreCase(p_szTagName)) {	
							subscriberVO.setUserName(l_szNodeValue);						
						} else if (TagIfc.TAG_ACCOUNT_NUMBER.equalsIgnoreCase(p_szTagName)) {						
							subscriberVO.setAccountNumber(l_szNodeValue);						
						} else if (TagIfc.TAG_TYPE.equalsIgnoreCase(p_szTagName)) {						
							subscriberVO.setType(l_szNodeValue);							
						} else if (TagIfc.TAG_RG_EMAIL.equalsIgnoreCase(p_szTagName)) {						
							subscriberVO.setEmail(l_szNodeValue);							
						} else if (TagIfc.TAG_CURRENT_BALANCE.equalsIgnoreCase(p_szTagName)) {	
							double currentBalance = 0;
							if(l_szNodeValue != null && l_szNodeValue.length()>0){
								currentBalance = Double.parseDouble(l_szNodeValue.trim());
							}
							subscriberVO.setCurrentBalance(currentBalance);							
						}						
					}
					subscriberVOList.add(subscriberVO);			   	
				}
		  }
		} catch (DOMException e) {
			throw new SystemException(e);
		}
		if(subscriberVOList != null)
			log	.debug("RoyalGuardXMLHandler > getListOfSubscriberVOList > Done , List size = "+subscriberVOList.size());
		
		return subscriberVOList;
	}
	


	public static void main(String args[]){
		RoyalGuardXMLHandler handler = new RoyalGuardXMLHandler();
		
		//System.out.println("Testing parseXMLReply for enquiry");
		//System.out.println("-------------------------------------------");
		String enquiryReplyXMLString = "<MOBILY_BSL_SR_REPLY><SR_HEADER><FuncId>FUNC_RG_INQUIRY</FuncId><SecurityKey></SecurityKey><ServiceRequestId></ServiceRequestId><MsgVersion>0000</MsgVersion><RequestorChannelId>EPortal</RequestorChannelId><SrDate>20100329143307</SrDate></SR_HEADER><ErrorCode>0</ErrorCode><SubscriberList/><ErrorMessage/><ConsumptionReport></ConsumptionReport></MOBILY_BSL_SR_REPLY>";
		//System.out.println("Parsed RoyalGuardReplyVO for Enquiry : "+handler.parseXMLReply(enquiryReplyXMLString).toString());
		//System.out.println("-------------------------------------------");
		
		//System.out.println("Testing parseXMLReply for distribution");
		//System.out.println("-------------------------------------------");
		String distriReplyXMLString = "<MOBILY_BSL_SR_REPLY><SR_HEADER><FuncId>FUNC_RG_DISTRUBUTE</FuncId><SecurityKey/><MsgVersion>0000</MsgVersion><RequestorChannelId>EPortal</RequestorChannelId><SrDate>20071023170900</SrDate><RequestorUserId>EPortal</RequestorUserId></SR_HEADER><ErrorCode>0</ErrorCode><ErrorMessage>Successfull</ErrorMessage></MOBILY_BSL_SR_REPLY>";
		//System.out.println("Parsed RoyalGuardReplyVO for Distribution : "+handler.parseXMLReply(distriReplyXMLString).toString());
		//System.out.println("-------------------------------------------");
		
		RoyalGuardReplyVO replyVO = null;	
		replyVO = handler.parseXMLReply(enquiryReplyXMLString);
		//System.out.println("vo list="+replyVO.getSubscriberVOList());
		if(replyVO.getErrorCode() != null && !"".equals(replyVO.getErrorCode()) && Integer.parseInt(replyVO.getErrorCode()) > 0) {
			//System.out.println("here");
		}
		
		

	}
	
}
