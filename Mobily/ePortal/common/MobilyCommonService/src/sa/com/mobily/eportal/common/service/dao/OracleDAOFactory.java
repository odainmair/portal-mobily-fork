/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao;

import sa.com.mobily.eportal.common.service.dao.ifc.AccountFilterServiceDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.AuthenticateAndGoDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.BBRegDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ConnectOTSDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ContactUSCacheDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerPackageDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerStatusDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerTypeDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.EBillDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.FavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.FootPrintTrackingDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPTVInqDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneDeviceInfoDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneServiceDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.InternationalBarringInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.LoyaltyDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRRelatedNumDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PandaDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PaymentotifyDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PhoneBookDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PromotionInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.RoyalGuardCommonDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.ePortalDBDaoIfc;
import sa.com.mobily.eportal.common.service.dao.imp.OracleAuthenticateAndGoDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleContactUSCacheDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCreditTransferDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCustomerPackageDataDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleFootPrintTrackingDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleIPhoneDeviceInfoDAOImpl;
import sa.com.mobily.eportal.common.service.dao.imp.OracleIPhoneServiceDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleInternationalBarringInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleLineTypeByPackageNameDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleLookupTableDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleMCRDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OraclePackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OraclePhoneBookDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleSohoAddOnTableDAO;
import sa.com.mobily.eportal.common.service.dao.imp.SeibelBBRegDAO;
import sa.com.mobily.eportal.common.service.dao.imp.ePortalDBDao;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleDAOFactory extends DAOFactory{  
 
    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getCustomerTypeDAO()
     */
    public CustomerTypeDAO getCustomerTypeDAO() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getCustomerStatusDAO()
     */
    public CustomerStatusDAO getCustomerStatusDAO() {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getCustomerProfileDAO()
     */
    public CustomerProfileDAO getCustomerProfileDAO() {
        // TODO Auto-generated method stub
        return new OracleCustomerProfileDAO();
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getInternationalBarringInquiryDAO()
     */
    public InternationalBarringInquiryDAO getInternationalBarringInquiryDAO() {
        // TODO Auto-generated method stub
        return new OracleInternationalBarringInquiryDAO();
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getPromotionInquiryDAO()
     */
    public PromotionInquiryDAO getPromotionInquiryDAO() {
        // TODO Auto-generated method stub
        return null;
    }

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getFavoriteNumberInquiryDAO()
	 */
	public FavoriteNumberInquiryDAO getFavoriteNumberInquiryDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public  CustomerPackageDAO getCustomerPackageDAO(){
		return new OracleCustomerPackageDataDAO();
	}

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getLookupTableDAO()
     */
    public OracleLookupTableDAO getLookupTableDAO() {
        // TODO Auto-generated method stub
        return new OracleLookupTableDAO();
    }

    public OracleLineTypeByPackageNameDAO getLineTypeByPackageNameDAO() {
        // TODO Auto-generated method stub
        return new OracleLineTypeByPackageNameDAO();
    }
    
	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getContactUSCacheDAO()
	 */
	public ContactUSCacheDAO getContactUSCacheDAO() {
		// TODO Auto-generated method stub
		return new OracleContactUSCacheDAO();
	}

	public PhoneBookDAO getPhoneBookDAO(){
		// TODO Auto-generated method stub
		return new OraclePhoneBookDAO();
	}
	
	public BBRegDAO getBBRegDAO(){
		return new SeibelBBRegDAO();
	}
	public AccountFilterServiceDAO getCustomerServiceDetails() {
		// TODO Auto-generated method stub
		return null;
	}
	public ePortalDBDaoIfc getePortalDBDao(){
		return new ePortalDBDao();
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getMQEBillDAO()
	 */
	public EBillDAO getMQEBillDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getAuthenticateAndGoDAO()
     */
    public AuthenticateAndGoDAO getAuthenticateAndGoDAO() {
        // TODO Auto-generated method stub
        return new OracleAuthenticateAndGoDAO();
    }
    
	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getMQPandaService()
	 */
	public PandaDAO getMQPandaDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getManageCreditCardDAO()
	 */
	public ManageCreditCardDAO getManageCreditCardDAO() {
		// TODO Auto-generated method stub
		return new OracleManageCreditCardDAO();
	}

	@Override
	public PackageConversionDAO getOraclePackageConversionDAO() {
		// TODO Auto-generated method stub
		return new OraclePackageConversionDAO();
	}

	@Override
	public PackageConversionDAO getMQPackageConversionDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPhoneServiceDAO getOracleIPhoneServiceDAO() {
		// TODO Auto-generated method stub
		return new OracleIPhoneServiceDAO();
	}

	@Override
	public RoyalGuardCommonDAO getMQRoyalGuardCommonDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FootPrintTrackingDAO getOracleFootPrintTrackingDAO() {
		// TODO Auto-generated method stub
		return new OracleFootPrintTrackingDAO();
	}
	
	 public CorporateStrategicTrackDAO getCorporateStrategicTrackDAO() {
	        // TODO Auto-generated method stub
	        return new OracleCorporateStrategicTrackDAO();
	  }

	@Override
	public MCRDAO getOracleMCRDAO() {
		// TODO Auto-generated method stub
		return new OracleMCRDAO();
	}

	@Override
	public ConnectOTSDAO getConnectOTSDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getFavoriteNumberInquiryDAO()
	 */
	public MCRRelatedNumDAO getMCRRelatedNumDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getLoyaltyDAO()
	 */
	@Override
	public LoyaltyDAO getLoyaltyDAO() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getSohoAddOnTableDAO()
	 */
	public  OracleSohoAddOnTableDAO getSohoAddOnTableDAO(){
		return new OracleSohoAddOnTableDAO();
	}

	/*
	 * (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.DAOFactory#getIPhoneDeviceInfoDAO()
	 */
	@Override
	public IPhoneDeviceInfoDAO getIPhoneDeviceInfoDAO() {
		// TODO Auto-generated method stub
		return new OracleIPhoneDeviceInfoDAOImpl();
	}
	
	public OracleCreditTransferDAO getCreditTransferDAO(){
		return new OracleCreditTransferDAO();
	}
	
	@Override
	public IPTVInqDAO getIPTVInqDAO() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PaymentotifyDAO getPaymentotifyDAO() {
		// TODO Auto-generated method stub
		return null;
	}
}
