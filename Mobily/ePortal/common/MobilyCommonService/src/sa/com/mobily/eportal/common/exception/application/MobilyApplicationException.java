package sa.com.mobily.eportal.common.exception.application;

import sa.com.mobily.eportal.common.exception.MobilyException;


public class MobilyApplicationException extends MobilyException {

	private static final long serialVersionUID = 6929182193662624282L;

	public MobilyApplicationException() {
	}

	public MobilyApplicationException(String message) {
		super(message);
	}

	public MobilyApplicationException(Throwable cause) {
		super(cause);
	}

	public MobilyApplicationException(String message, Throwable cause) {
		super(message, cause);
	}
}