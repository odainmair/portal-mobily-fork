package sa.com.mobily.eportal.customerinfo.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author r.agarwal.mit 
 * September 10, 2013
 * Description: 
 */

public class FavoriteNumberVO  extends BaseVO{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Request & Response
	private String PackageId = null;
	private String PackageName = null; 
	private String msisdn = null;
	private String billingAcctNumber = null;
	
	
	//Reply
	private String ChargeFees = null;
	private String NationalFavNo = null;
	private String NationalFavNoCode = null;
	private String InternationalFavNo = null;
	private String InternationalFavNoCode = null;
	private String FixedLineFavNo = null;
	private String FixedLineFavNoCode = null;
	private String OnNetFavNo = null;
	private String OnNetFavNoCode = null;
	private String CrossNetFavNo = null;
	private String CrossNetFavNoCode = null;
	private String InternationalSMS = null;
	private String InternationalSMSCode = null;
	private String FavCountry = null;
	private String FavCountryCode = null;
	private String MCC = null;
	private String MCCCode = null;
	private String InternationalBasketFavNo = null;
	private String MobilyBasketFavNo = null;
	private String NonMobilyBasketFavNo = null;
	private String FixedLineBasketFavNo = null;
	private String MaxBasketFavNo = null;
	private String SMSChannelAllowed = null;
	private String MaxFAFCount = null;
	private String MaxFreeFAF = null;
	private String AddChargeAmount = null;
	
	private String statusCode = null;
	
	public static final String ACTION_ADD = "Add";
	public static final String ACTION_DELETE = "Delete";
	public static final String ACTION_REPLACE = "Replace";
	
	
	public String getStatusCode()
	{
		return statusCode;
	}
	public void setStatusCode(String statusCode)
	{
		this.statusCode = statusCode;
	}
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getBillingAcctNumber()
	{
		return billingAcctNumber;
	}
	public void setBillingAcctNumber(String billingAcctNumber)
	{
		this.billingAcctNumber = billingAcctNumber;
	}
	public String getPackageId()
	{
		return PackageId;
	}
	public void setPackageId(String packageId)
	{
		PackageId = packageId;
	}
	public String getPackageName()
	{
		return PackageName;
	}
	public void setPackageName(String packageName)
	{
		PackageName = packageName;
	}
	public String getChargeFees()
	{
		return ChargeFees;
	}
	public void setChargeFees(String chargeFees)
	{
		ChargeFees = chargeFees;
	}
	public String getNationalFavNo()
	{
		return NationalFavNo;
	}
	public void setNationalFavNo(String nationalFavNo)
	{
		NationalFavNo = nationalFavNo;
	}
	public String getNationalFavNoCode()
	{
		return NationalFavNoCode;
	}
	public void setNationalFavNoCode(String nationalFavNoCode)
	{
		NationalFavNoCode = nationalFavNoCode;
	}
	public String getInternationalFavNo()
	{
		return InternationalFavNo;
	}
	public void setInternationalFavNo(String internationalFavNo)
	{
		InternationalFavNo = internationalFavNo;
	}
	public String getInternationalFavNoCode()
	{
		return InternationalFavNoCode;
	}
	public void setInternationalFavNoCode(String internationalFavNoCode)
	{
		InternationalFavNoCode = internationalFavNoCode;
	}
	public String getFixedLineFavNo()
	{
		return FixedLineFavNo;
	}
	public void setFixedLineFavNo(String fixedLineFavNo)
	{
		FixedLineFavNo = fixedLineFavNo;
	}
	public String getFixedLineFavNoCode()
	{
		return FixedLineFavNoCode;
	}
	public void setFixedLineFavNoCode(String fixedLineFavNoCode)
	{
		FixedLineFavNoCode = fixedLineFavNoCode;
	}
	public String getOnNetFavNo()
	{
		return OnNetFavNo;
	}
	public void setOnNetFavNo(String onNetFavNo)
	{
		OnNetFavNo = onNetFavNo;
	}
	public String getOnNetFavNoCode()
	{
		return OnNetFavNoCode;
	}
	public void setOnNetFavNoCode(String onNetFavNoCode)
	{
		OnNetFavNoCode = onNetFavNoCode;
	}
	public String getCrossNetFavNo()
	{
		return CrossNetFavNo;
	}
	public void setCrossNetFavNo(String crossNetFavNo)
	{
		CrossNetFavNo = crossNetFavNo;
	}
	public String getCrossNetFavNoCode()
	{
		return CrossNetFavNoCode;
	}
	public void setCrossNetFavNoCode(String crossNetFavNoCode)
	{
		CrossNetFavNoCode = crossNetFavNoCode;
	}
	public String getInternationalSMS()
	{
		return InternationalSMS;
	}
	public void setInternationalSMS(String internationalSMS)
	{
		InternationalSMS = internationalSMS;
	}
	public String getInternationalSMSCode()
	{
		return InternationalSMSCode;
	}
	public void setInternationalSMSCode(String internationalSMSCode)
	{
		InternationalSMSCode = internationalSMSCode;
	}
	public String getFavCountry()
	{
		return FavCountry;
	}
	public void setFavCountry(String favCountry)
	{
		FavCountry = favCountry;
	}
	public String getFavCountryCode()
	{
		return FavCountryCode;
	}
	public void setFavCountryCode(String favCountryCode)
	{
		FavCountryCode = favCountryCode;
	}
	public String getMCC()
	{
		return MCC;
	}
	public void setMCC(String mCC)
	{
		MCC = mCC;
	}
	public String getMCCCode()
	{
		return MCCCode;
	}
	public void setMCCCode(String mCCCode)
	{
		MCCCode = mCCCode;
	}
	public String getInternationalBasketFavNo()
	{
		return InternationalBasketFavNo;
	}
	public void setInternationalBasketFavNo(String internationalBasketFavNo)
	{
		InternationalBasketFavNo = internationalBasketFavNo;
	}
	public String getMobilyBasketFavNo()
	{
		return MobilyBasketFavNo;
	}
	public void setMobilyBasketFavNo(String mobilyBasketFavNo)
	{
		MobilyBasketFavNo = mobilyBasketFavNo;
	}
	public String getNonMobilyBasketFavNo()
	{
		return NonMobilyBasketFavNo;
	}
	public void setNonMobilyBasketFavNo(String nonMobilyBasketFavNo)
	{
		NonMobilyBasketFavNo = nonMobilyBasketFavNo;
	}
	public String getFixedLineBasketFavNo()
	{
		return FixedLineBasketFavNo;
	}
	public void setFixedLineBasketFavNo(String fixedLineBasketFavNo)
	{
		FixedLineBasketFavNo = fixedLineBasketFavNo;
	}
	public String getMaxBasketFavNo()
	{
		return MaxBasketFavNo;
	}
	public void setMaxBasketFavNo(String maxBasketFavNo)
	{
		MaxBasketFavNo = maxBasketFavNo;
	}
	public String getSMSChannelAllowed()
	{
		return SMSChannelAllowed;
	}
	public void setSMSChannelAllowed(String sMSChannelAllowed)
	{
		SMSChannelAllowed = sMSChannelAllowed;
	}
	public String getMaxFAFCount()
	{
		return MaxFAFCount;
	}
	public void setMaxFAFCount(String maxFAFCount)
	{
		MaxFAFCount = maxFAFCount;
	}
	public String getMaxFreeFAF()
	{
		return MaxFreeFAF;
	}
	public void setMaxFreeFAF(String maxFreeFAF)
	{
		MaxFreeFAF = maxFreeFAF;
	}
	public String getAddChargeAmount()
	{
		return AddChargeAmount;
	}
	public void setAddChargeAmount(String addChargeAmount)
	{
		AddChargeAmount = addChargeAmount;
	}
	

	
	
}
