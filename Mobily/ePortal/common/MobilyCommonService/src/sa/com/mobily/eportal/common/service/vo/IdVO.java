/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class IdVO extends BaseVO {

    private String idDocType;
    private String idNumber;

    public static void main(String[] args) {
    }
    /**
     * @return Returns the idDocType.
     */
    public String getIdDocType() {
        return idDocType;
    }
    /**
     * @param idDocType The idDocType to set.
     */
    public void setIdDocType(String idDocType) {
        this.idDocType = idDocType;
    }
    /**
     * @return Returns the idNumber.
     */
    public String getIdNumber() {
        return idNumber;
    }
    /**
     * @param idNumber The idNumber to set.
     */
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
}
