/*
 * Created on Sep 25, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;



/**
 * @author aghareeb
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PhoneUsageVO   extends BaseVO 
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8718455164842960421L;
	private PhoneUsageItemListVO servicesDetailsItemVO;
	private PhoneUsageItemListVO discountDetailsItemVO;
	private PhoneUsageItemListVO callDetailsItemVO;
	private PhoneUsageItemListVO messageDetailsItemVO;
	
	//	for Corporate use
	private ArrayList local;
	private ArrayList international;
	private ArrayList roaming;
	private double    totalLocal;
	private double    totalInternational;
	private double    totalRoaming;
	
	
	
	/**
	 * @return Returns the callDetailsItemVO.
	 */
	public PhoneUsageItemListVO getCallDetailsItemVO() {
		return callDetailsItemVO;
	}
	/**
	 * @param callDetailsItemVO The callDetailsItemVO to set.
	 */
	public void setCallDetailsItemVO(PhoneUsageItemListVO callDetailsItemVO) {
		this.callDetailsItemVO = callDetailsItemVO;
	}
	/**
	 * @return Returns the discountDetailsItemVO.
	 */
	public PhoneUsageItemListVO getDiscountDetailsItemVO() {
		return discountDetailsItemVO;
	}
	/**
	 * @param discountDetailsItemVO The discountDetailsItemVO to set.
	 */
	public void setDiscountDetailsItemVO(PhoneUsageItemListVO discountDetailsItemVO) {
		this.discountDetailsItemVO = discountDetailsItemVO;
	}
	/**
	 * @return Returns the messageDetailsItemVO.
	 */
	public PhoneUsageItemListVO getMessageDetailsItemVO() {
		return messageDetailsItemVO;
	}
	/**
	 * @param messageDetailsItemVO The messageDetailsItemVO to set.
	 */
	public void setMessageDetailsItemVO(PhoneUsageItemListVO messageDetailsItemVO) {
		this.messageDetailsItemVO = messageDetailsItemVO;
	}
	/**
	 * @return Returns the servicesDetailsItemVO.
	 */
	public PhoneUsageItemListVO getServicesDetailsItemVO() {
		return servicesDetailsItemVO;
	}
	/**
	 * @param servicesDetailsItemVO The servicesDetailsItemVO to set.
	 */
	public void setServicesDetailsItemVO(PhoneUsageItemListVO servicesDetailsItemVO) {
		this.servicesDetailsItemVO = servicesDetailsItemVO;
	}
    public ArrayList getInternational()
    {
        return international;
    }
    public void setInternational(ArrayList international)
    {
        this.international = international;
    }
    public ArrayList getLocal()
    {
        return local;
    }
    public void setLocal(ArrayList local)
    {
        this.local = local;
    }
    public ArrayList getRoaming()
    {
        return roaming;
    }
    public void setRoaming(ArrayList roaming)
    {
        this.roaming = roaming;
    }
    public double getTotalInternational()
    {
        return totalInternational;
    }
    public void setTotalInternational(double totalInternational)
    {
        this.totalInternational = totalInternational;
    }
    public double getTotalLocal()
    {
        return totalLocal;
    }
    public void setTotalLocal(double totalLocal)
    {
        this.totalLocal = totalLocal;
    }
    public double getTotalRoaming()
    {
        return totalRoaming;
    }
    public void setTotalRoaming(double totalRoaming)
    {
        this.totalRoaming = totalRoaming;
    }
}
