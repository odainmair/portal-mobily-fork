package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.dao.constants.DAOConstants;
import sa.com.mobily.eportal.common.service.model.UserAccount;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
/**
 * This DAO class is responsible for DB operations on USER_ACCOUNTS table
 * <p> 
 * it contains methods for retrieve, add and update.
 *  

 */
public class UserMyConnectAcctDAO extends AbstractDBDAO<UserAccount>
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static String clazz = UserMyConnectAcctDAO.class.getName();

	private static UserMyConnectAcctDAO instance = new UserMyConnectAcctDAO();

	private static final String UA_ADD = "INSERT INTO USER_MYCONNECT_ACCOUNTS(USER_ACCT_ID, USER_NAME, CONTACT_NUMBER, REGISTRATION_DATE,PACKAGE_ID,PACKAGE_TYPE) "
			+ "VALUES(?, ?, ?,CURRENT_TIMESTAMP,?,?) ";
	
	private static final String GET_SEQ_USER_MYCONNECT_ACCT_ID = "SELECT USER_MYCONNECT_ACCOUNT_SEQ.NEXTVAL userAcctId FROM DUAL";

	private static final String UA_QUERY_BY_USERNAME = "SELECT USER_ACCT_ID, USER_NAME, CONTACT_NUMBER, REGISTRATION_DATE, " +
			" STATUS, LAST_LOGIN_TIME, PACKAGE_ID, PACKAGE_TYPE " +
			"  FROM USER_MYCONNECT_ACCOUNTS WHERE USER_NAME =?";
	
	
	private static final String UA_UPDATE_ALL = "UPDATE USER_MYCONNECT_ACCOUNTS SET CONTACT_NUMBER=? " +
			" WHERE USER_ACCT_ID = ?";
	
	protected UserMyConnectAcctDAO()
	{
		super(DataSources.DEFAULT);
	}

	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>UserAccountDAO</code>
	 * 
	 */
	public static synchronized UserMyConnectAcctDAO getInstance()
	{
		return instance;
	}
	/**
	 * This method returns a UserAccount information for a specific user name
	 * @param userName
	 * @return
	 */
	public List<UserAccount> findByUserName(String userName)
	{
		return query(UA_QUERY_BY_USERNAME, new Object[] { userName });
	}
	
	
	/**
	 * This method update UserAccount for a specific account in the USER_MYCONNECT_ACCOUNTS table
	 */
	public int updateUserAccountInfo(UserAccount userAccount)
	{
		return update(UA_UPDATE_ALL, new Object[] { userAccount.getContactNumber(),  userAccount.getUserAcctId()});
	}
	
	/**
	 * This method saves UserAccount for a specific account in the USER_MYCONNECT_ACCOUNTS table
	 * 
	 * @param request <code>UserAccount</code> user account to be saved
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * 
	 */
	public int saveMyConnectUserAccount(UserAccount userAccount)
	{

		return update(UA_ADD, userAccount.getUserAcctId(), userAccount.getUserName(), userAccount.getContactNumber(),userAccount.getPackageId(),
				userAccount.getPackageType());
	}

	@Override
	protected UserAccount mapDTO(ResultSet rs) throws SQLException
	{
		UserAccount userAccount = new UserAccount();
		if (rs.getMetaData().getColumnCount() > 1)
		{
			userAccount.setUserAcctId(rs.getLong(DAOConstants.UserAccount.USER_ACCT_ID));
			userAccount.setUserName(rs.getString(DAOConstants.UserAccount.USER_NAME));
			userAccount.setContactNumber(rs.getString(DAOConstants.UserAccount.CONTACT_NUMBER));
			userAccount.setRegistrationDate(rs.getTimestamp(DAOConstants.UserAccount.REGISTRATION_DATE));
			
			userAccount.setStatus(rs.getInt(DAOConstants.UserAccount.STATUS));
			userAccount.setLastLoginTime(rs.getTimestamp(DAOConstants.UserAccount.LAST_LOGIN_TIME));
			
			
			userAccount.setPackageId(rs.getString(DAOConstants.UserAccount.PACKAGE_ID));
			userAccount.setPackageType(rs.getString(DAOConstants.UserAccount.PACKAGE_TYPE));
		}else{
			userAccount.setUserAcctId(rs.getLong(DAOConstants.UserAccount.USERACCTID));
		}
		
		
		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "dto >>>>" + userAccount, clazz, "mapDTO", null);
		executionContext.audit(logEntryVO);
	
		// TODO Auto-generated method stub
		return userAccount;
	}
	
	public Long getUserMyConnectAcctIdSeq()
	{
		executionContext.log(Level.INFO, "getUserMyConnectAcctIdSeq called", clazz, "mapDTO", null);

		UserAccount dto = null;
		List<UserAccount> userAccountList = query(GET_SEQ_USER_MYCONNECT_ACCT_ID);
		executionContext.log(Level.INFO, "getUserMyConnectAcctIdSeq > userAccountList::" + userAccountList, clazz, "getUserMyConnectAcctIdSeq", null);

		if (userAccountList != null && userAccountList.size() > 0)
		{
			dto = userAccountList.get(0);
		}
		return dto.getUserAcctId();
	}
}