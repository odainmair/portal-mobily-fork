//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the implementation of the Context interface
 * when an error occurs while retrieving the context information.
 * @see sa.com.mobily.eportal.core.api.Context
 */
public class ContextException extends Exception {

	private static final long serialVersionUID = 1L;

    public ContextException(String message) {
        super(message);
    }

    public ContextException(Throwable cause) {
        super(cause);
    }

    public ContextException(String message, Throwable cause) {
        super(message, cause);
    }

}
