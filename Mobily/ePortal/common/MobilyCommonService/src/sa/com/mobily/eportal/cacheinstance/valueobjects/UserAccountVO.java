package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.sql.Date;
import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserAccountVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	private long userAccountId;
	private String userName;
	//private String iqama;
	private Timestamp createdDate;
	private Timestamp activationDate;
	private Timestamp lastLoginTime;
	private String email;
	private String status;
	private String firstName;
	private String lastName;
	private int custSegment;
	private Date birthDate;
	private String nationality;
	private String gender;
	private Timestamp registrationDate;
	private int roleId;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
/*	public String getIqama() {
		return iqama;
	}
	public void setIqama(String iqama) {
		this.iqama = iqama;
	}*/
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Timestamp activationDate) {
		this.activationDate = activationDate;
	}
	public Timestamp getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(Timestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getCustSegment() {
		return custSegment;
	}
	public void setCustSegment(int custSegment) {
		this.custSegment = custSegment;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public Timestamp getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getUserAccountId()
	{
		return userAccountId;
	}
	public void setUserAccountId(long userAccountId)
	{
		this.userAccountId = userAccountId;
	}
	/**
	 * @return the roleId
	 */
	public int getRoleId()
	{
		return roleId;
	}
	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}
	
}
