package sa.com.mobily.eportal.resourceenvprovider;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Muzammil Mohsin Shaikh
 * 
 */
public class MobilyConfiguration
{
	private Map<String, Object> attributes = null;

	public MobilyConfiguration()
	{
		attributes = new HashMap<String, Object>(10);
	}

	public void setAttribute(String attributeName, String attributeValue)
	{
		attributes.put(attributeName, attributeValue);
	}

	public Object getAttribute(String attributeName)
	{
		return attributes.get(attributeName);
	}
}
