package sa.com.mobily.eportal.usermanagement.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

//import org.apache.openjpa.persistence.DataCache;


/**
 * The persistent class for the SR_USER_SESSIONVO_TBL database table.
 * 
 */
@Entity
@Table(name="SR_USER_SESSIONVO_TBL")
//@DataCache(enabled=false)
public class UserSession implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserSessionPK id;

    public UserSession() {
    }

	public UserSessionPK getId() {
		return this.id;
	}
	
	public void setId(UserSessionPK id) {
		this.id = id;
	}
	
	@Column(name="SUBSCRIPTION_ID")
	private Long subscriptionID;

	public Long getSubscriptionID() {
		return subscriptionID;
	}

	public void setSubscriptionID(Long subscriptionID) {
		this.subscriptionID = subscriptionID;
	}
	
	@Column(name="CREATED_DATE")
	@NotNull
	private Timestamp createdDate;

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}	
}