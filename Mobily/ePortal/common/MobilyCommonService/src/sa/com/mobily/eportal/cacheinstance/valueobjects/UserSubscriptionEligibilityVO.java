package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserSubscriptionEligibilityVO extends BaseVO{
	
	private static final long serialVersionUID = 1L;
	private long useId;
	private long subscriptionId;
	private int projectId;
	private String status;
	private Timestamp updatedTime;
	
	public long getUseId()
	{
		return useId;
	}
	public void setUseId(long useId)
	{
		this.useId = useId;
	}
	public long getSubscriptionId()
	{
		return subscriptionId;
	}
	public void setSubscriptionId(long subscriptionId)
	{
		this.subscriptionId = subscriptionId;
	}
	public int getProjectId()
	{
		return projectId;
	}
	public void setProjectId(int projectId)
	{
		this.projectId = projectId;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public Timestamp getUpdatedTime()
	{
		return updatedTime;
	}
	public void setUpdatedTime(Timestamp updatedTime)
	{
		this.updatedTime = updatedTime;
	}
	
	
}
