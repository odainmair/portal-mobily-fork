/* 
 * Licensed Material - Property of Mobily.
 * (c) Copyright Mobily 2013. All rights reserved.
 */

package sa.com.mobily.eportal.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;

import javax.jms.BytesMessage;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.mq.dao.MQAuditDAO;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.service.EPortalUtils.EPortalProperty;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

/**
 * JMSHelper class will be responsible on sending and receiving message from & to back-end 
 * using JMS APIs
 * @version v1 - with JDK v1.6
 * @author Emadm
 */

public class JMSConnectionManager {
    public static final String DEFAULT_CONNECTION_FACTORY = "mobily/jms/connectionfactory";
    public static final int NON_PERSISTENT_DELIVERY_MODE = DeliveryMode.NON_PERSISTENT;
    public static final int PERSISTENT_DELIVERY_MODE = DeliveryMode.PERSISTENT;
    
    private static final Logger log = Logger.getLogger(JMSConnectionManager.class);
    
    
    private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("MobilyCommonService/JMSConnectionManager");
    
    private final String CORRELATION_ID = "JMSCorrelationID= '";
    private final int RECEIVE_WITH_NO_WAIT = 0;
    private final int RECEIVE_AND_WAIT = -1;    

    private QueueConnectionFactory mCnxToFactory = null;
    private QueueConnectionFactory mCnxReplyFactory = null;
    private String mCorrelationId = null;
    private boolean mUseMsgID = true;
    private int mDeliveryMode = DeliveryMode.NON_PERSISTENT;
    private int mTimeOut = Integer.valueOf(EPortalUtils.getProperty(EPortalProperty.JMS_TIMEOUT)).intValue();
    
    /***
     * Constructs a JMSConnectionManager using the default connection factory
     * @throws NamingException if the default connection factory could not be located
     */
    private JMSConnectionManager() throws NamingException {
        initConnectionFactory(DEFAULT_CONNECTION_FACTORY);
    }
    
    public JMSConnectionManager(String connectionFactory) throws NamingException {
        initConnectionFactory(connectionFactory);
    }
    
    public void setDeliveryMode(int deliveryMode) {
        this.mDeliveryMode = deliveryMode;
    }
    
    public static JMSConnectionManager getInstance() throws NamingException {
 //       return JMSStubConnectionManager.getInstance(DEFAULT_CONNECTION_FACTORY);	// TODO: comment this to disable stubs completely
    	return new JMSConnectionManager();	// TODO: uncomment this to enable normal/production mode
    }

    protected boolean initConnectionFactory(String queueConnectionFactory) throws NamingException {
        InitialContext ctx = new InitialContext();
        mCnxReplyFactory = mCnxToFactory = (QueueConnectionFactory)ctx.lookup(queueConnectionFactory);

        return (mCnxToFactory != null) ? true : false;
    }
    
    private boolean initReplyConnectionFactory(String queueConnectionFactory) throws Exception {
        InitialContext ctx = new InitialContext();
        mCnxReplyFactory = (QueueConnectionFactory)ctx.lookup(queueConnectionFactory);

        return (mCnxReplyFactory != null) ? true : false;
    }
    
    public void setJMSPrivateCorrelationID(){
        mUseMsgID = false;
        mCorrelationId = "ID-" + Double.doubleToLongBits(Math.random()) + "-" + new Date().getTime();
    }

    public boolean sendMessage(String message, Queues jniToQueue, String replyQueueName, String replyQueueMgrName) throws Exception {
        QueueSession session = null;
        QueueConnection cnx = null;
        boolean status = true;

		try
		{

			cnx = mCnxToFactory.createQueueConnection();
			
			if (cnx == null)
			{
				throw new ServiceException("Unable to obtain Queue Connection");
			}
			
			log.debug("Queue Connection Created " + cnx.getClass().getCanonicalName(), "sendMessage");

			
			executionContext.severe("m.hassan.dar says connection created ");
			
			session = cnx.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

			if (session == null)
			{
				throw new ServiceException("Unable to obtain JMS Session");
			}
			log.debug("Session Created " + session.getClass().getCanonicalName(), "sendMessage");

			TextMessage inMessage = session.createTextMessage(message);

			if (!mUseMsgID)
			{
				inMessage.setJMSCorrelationID(mCorrelationId);
			}

			if (replyQueueName != null)
			{
				replyQueueName = (replyQueueMgrName == null) ? replyQueueName : "queue://" + replyQueueMgrName + "/" + replyQueueName;
				Destination replyDest = session.createQueue(replyQueueName);
				inMessage.setJMSReplyTo(replyDest);
				log.debug("replyDest : " + replyDest, "sendMessage");
			}

			inMessage.setJMSDeliveryMode(mDeliveryMode);

			Queue fmsQueue = (Queue) ServiceLocator.lookupQueue(jniToQueue);
			log.debug("Sending to Queue : " + fmsQueue + " : " + message, "sendMessage");

			QueueSender fmsSender = session.createSender(fmsQueue);
			log.debug("Sender Created", "sendMessage");

			cnx.start();
			log.debug("Connection Started", "sendMessage");

			fmsSender.send(inMessage);
			log.debug("Message Sent, " + inMessage.getJMSMessageID(), "sendMessage");

			if (mUseMsgID)
			{
				mCorrelationId = inMessage.getJMSMessageID();
			}

		}
		catch (Exception e)
		{
			status = false;
			executionContext.severe("m.hassan.dar says error when sendMessage " + e);
			throw e;
		}
		finally
		{
			if (session != null)
			{
				try
				{
					session.close();
				}
				catch (Exception ex)
				{
					executionContext.severe("m.hassan.dar says error when closing the session " + ex);
				}
				
			}

			if (cnx != null)
			{
				try
				{
					cnx.close();
					log.debug("cnx closed", "sendMessage");
					executionContext.severe("m.hassan.dar says connection closed");
				}
				catch (Exception ex)
				{
					log.error("Unable to close JMS connection", "sendMessage", ex);
					executionContext.severe("m.hassan.dar says error when closing the connection " + ex);
				}
			}
		}        
        
       
        
        return status;
    }
    
    public String receiveMessage(Queues jniToQueue, String correlationId, long timeOut) throws Exception {
        QueueSession session = null;
        QueueConnection cnx = null;
        Message outMessage = null;
        String msgText = null;
        
		try
		{
			cnx = mCnxReplyFactory.createQueueConnection();

			if (cnx == null)
			{
				throw new ServiceException("Unable to obtain Queue Connection");
			}

			executionContext.severe("m.hassan.dar says connection created ");
			
			session = cnx.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			if (session == null)
			{
				throw new ServiceException("Unable to obtain JMS Session");
			}

			Queue fmsQueue = (Queue) ServiceLocator.lookupQueue(jniToQueue);
			QueueReceiver fmsReceiver = session.createReceiver(fmsQueue, CORRELATION_ID + correlationId + "'");

			cnx.start();

			if (timeOut == RECEIVE_WITH_NO_WAIT)
			{
				outMessage = fmsReceiver.receiveNoWait();
			}
			else if (timeOut == RECEIVE_AND_WAIT)
			{
				outMessage = fmsReceiver.receive(mTimeOut);
			}
			else
			{
				outMessage = fmsReceiver.receive(timeOut);
			}

			if (outMessage != null)
			{
				if (outMessage instanceof TextMessage)
				{
					TextMessage msg = (TextMessage) outMessage;
					msgText = msg.getText();
				}
				else if (outMessage instanceof BytesMessage)
				{
					int len = new Long(((BytesMessage) outMessage).getBodyLength()).intValue();
					byte[] msgBytes = new byte[len];
					((BytesMessage) outMessage).readBytes(msgBytes, len);
					msgText = new String(msgBytes);
				}
			}
			log.debug("Message Received In Queue: " + fmsQueue.getQueueName() + " : " + msgText, "receiveMessage");

		}
		catch (Exception e)
		{
			executionContext.severe("m.hassan.dar says error when receiveMessage " + e);
			throw e;
		}
		finally
		{
			if (session != null)
			{
				try
				{
					session.close();
				}
				catch (Exception ex)
				{
					executionContext.severe("m.hassan.dar says error when closing the session " + ex);
				}
				
			}

			if (cnx != null)
			{
				try
				{
					cnx.close();
					log.debug("cnx closed", "receiveMessage");
					executionContext.severe("m.hassan.dar says connection closed");
				}
				catch (Exception ex)
				{
					log.error("Unable to close JMS connection", "receiveMessage", ex);
					executionContext.severe("m.hassan.dar says error when closing the connection " + ex);
				}
			}
		}        
        
       
    
        return msgText;
    }

    public boolean sendMessage(String message, Queues jniToQueue) throws Exception {
        return sendMessage(message, jniToQueue, null);
    }
    
    String receiveMessage(Queues jniToQueue) throws Exception {
        return receiveMessage(jniToQueue, mCorrelationId, RECEIVE_AND_WAIT);
    }
    
    private String receiveMsgWithNoWait(Queues jniToQueue) throws Exception {
        return receiveMessage(jniToQueue, mCorrelationId, RECEIVE_WITH_NO_WAIT);
    }
    
    public boolean sendMessage(String message, Queues jniToQueue, Queues jndiReplyQueue) throws Exception {
        String replyQueueName = null;

        if (jndiReplyQueue != null) {
            Queue replyQueue = (Queue)ServiceLocator.lookupQueue(jndiReplyQueue);
            replyQueueName = replyQueue.getQueueName();
        }
    
        return sendMessage(message, jniToQueue, replyQueueName, null);
    }
    
    private String receiveMsgWithNoWait(Queues jniToQueue, String jniConnectionFactory) throws Exception {
        if (initReplyConnectionFactory(jniConnectionFactory)){
            return receiveMessage(jniToQueue, mCorrelationId, RECEIVE_WITH_NO_WAIT);
        }
        
        return null;
    }
    
    private String receiveMessage(Queues jniToQueue, String jniConnectionFactory) throws Exception {
        if (initReplyConnectionFactory(jniConnectionFactory)){
            return receiveMessage(jniToQueue);
        }
        
        return null;
    }
    
    public String sendMessageWithReply(String message, Queues jniToQueue, Queues jniReplyqueue, long timeOut) throws Exception {
    	String reply = "";
    	try {

    		int waitingtime = -1;
    		
    		long timeBeforeSending = new Date().getTime() / 1000;

            executionContext.log(Level.INFO, " XML Request =:["+message+"]", "JMSConnectionManager", "sendMessageWithReply");

    		sendMessage(message, jniToQueue, jniReplyqueue);
	        
	        reply = receiveMessage(jniReplyqueue, mCorrelationId, timeOut);
        
	        waitingtime = (int) (new Date().getTime() / 1000 - timeBeforeSending);
	        
        
            String requestQueue = getValidQueueName(((Queue) ServiceLocator.lookupQueue(jniToQueue)).getQueueName());
            String replyQueue = getValidQueueName(((Queue) ServiceLocator.lookupQueue(jniReplyqueue)).getQueueName());
            
            executionContext.log(Level.INFO, " requestQueue =["+requestQueue+"] and replyQueue =["+replyQueue+"]", "JMSConnectionManager", "sendMessageWithReply");

            executionContext.log(Level.INFO, " XML Reply =:["+reply+"]", "JMSConnectionManager", "sendMessageWithReply");

        	String isAuditRequired = ResourceEnvironmentProviderService.getInstance().getStringProperty(ConstantsIfc.MQ_AUDIT_REQUIRED);
        	executionContext.log(Level.INFO, " isAuditRequired =["+isAuditRequired+"]", "JMSConnectionManager", "sendMessageWithReply");

            if(FormatterUtility.isEmpty(isAuditRequired)
            		|| ConstantsIfc.TRUE.equalsIgnoreCase(FormatterUtility.checkNull(isAuditRequired))){
            	
            	//MQUtility.saveMQAudit(requestQueue, replyQueue, message, reply);
            	MQAuditVO mqAuditVO = new MQAuditVO();
					mqAuditVO.setRequestQueue(requestQueue);
					mqAuditVO.setReplyQueue(replyQueue);
					mqAuditVO.setMessage(message);
					mqAuditVO.setReply(reply);
					mqAuditVO.setWaitingTime(waitingtime);
	
				MQUtility.saveMQAudit(mqAuditVO);
            	
            	executionContext.log(Level.INFO, "MQ request Saved Successfuly.....", "JMSConnectionManager", "sendMessageWithReply");
            } else  {
            	executionContext.log(Level.INFO, "MQ request Audit Not Required....", "JMSConnectionManager", "sendMessageWithReply");
            }
        } catch (Exception exception) {
        	log.info("An error happens while logging MQ message", "sendMessageWithReply");
        }
        
        return reply;
    }
    
    /***
     * Sends a message and returns a {@link FutureResponse} instance that could be used to receive the reply message later.
     * <br/>This method is unblocking, will return once the request message is sent. 
     * @param message	the message to be sent
     * @param requestQ	the request queue
     * @param replyQ	the reply queue
     * @param responseHandler	a {@link ResponseHandler} instance which will be used in parsing the response.
     * @return	a {@link FutureResponse} instance that could be used to receive the reply message later.
     * @throws Exception	in case of any failure while sending the request message or constructing the returned future response.
     */
    public <T> FutureResponse<T> sendMessageWithReply(String message, Queues requestQ, Queues replyQ, ResponseHandler<T> responseHandler) throws Exception {
        sendMessage(message, requestQ, replyQ);
        
//        System.out.println(">>T> Msg sent, correlation id [" + mCorrelationId + "]");
        final FutureResponse<T> futureResponse = new FutureResponse<T>(requestQ, replyQ, message, this.mCorrelationId, responseHandler);
        return futureResponse;
    }
    
    public String sendMessageWithReply(String message, Queues jniToQueue, Queues jniReplyqueue) throws Exception {        
        return sendMessageWithReply(message, jniToQueue, jniReplyqueue, mTimeOut);
    }
    
    public ArrayList<String> getAllMessages(Queues jniToQueue) throws Exception {
        ArrayList<String> msgList = new ArrayList<String>();
        QueueSession session = null;
        QueueConnection cnx = null;
        Message outMessage = null;

        
		try
		{
			cnx = mCnxReplyFactory.createQueueConnection();

			if (cnx == null)
			{
				throw new ServiceException("Unable to obtain Queue Connection");
			}

			executionContext.severe("m.hassan.dar says connection created ");
			
			cnx.start();

			session = cnx.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			if (session == null)
			{
				throw new ServiceException("Unable to obtain JMS Session");
			}

			Queue fmsQueue = (Queue) ServiceLocator.lookupQueue(jniToQueue);
			QueueReceiver fmsReceiver = session.createReceiver(fmsQueue);

			do
			{
				outMessage = fmsReceiver.receiveNoWait();

				if (outMessage != null)
				{
					if (outMessage instanceof TextMessage)
					{
						TextMessage msg = (TextMessage) outMessage;
						// System.out.println(msg.getText());
						msgList.add(msg.getText());
					}
					else if (outMessage instanceof BytesMessage)
					{
						int len = new Long(((BytesMessage) outMessage).getBodyLength()).intValue();
						byte[] msgBytes = new byte[len];
						((BytesMessage) outMessage).readBytes(msgBytes, len);
						String msgText = new String(msgBytes);
						// System.out.println(msgText);
						msgList.add(msgText);
					}
				}
			}
			while (outMessage != null);

		}
		catch (Exception e)
		{
			executionContext.severe("m.hassan.dar says error when getAllMessages " + e);
			throw e;
		}
		finally
		{
			if (session != null)
			{
				try
				{
					session.close();
				}
				catch (Exception ex)
				{
					executionContext.severe("m.hassan.dar says error when closing the session " + ex);
				}
				
			}

			if (cnx != null)
			{
				try
				{
					cnx.close();
					log.debug("cnx closed", "getAllMessages");
					executionContext.severe("m.hassan.dar says connection closed");
				}
				catch (Exception ex)
				{
					log.error("Unable to close JMS connection", "getAllMessages", ex);
					executionContext.severe("m.hassan.dar says error when closing the connection " + ex);
				}
			}
		}        
        
        
        return msgList;
    }
    
	private void saveMQAudit(String requestQueue, String replyQueue, String message, String reply) {
		MQAuditVO mqAuditVO = new MQAuditVO();

		try {
			mqAuditVO.setRequestQueue(requestQueue);
			mqAuditVO.setReplyQueue(replyQueue);
			mqAuditVO.setMessage(message);
			mqAuditVO.setReply(reply);

			log.info("[Start] Audit log MQ request", "saveMQAudit");
			log.info("MQ Audit Request Queue: " + mqAuditVO.getRequestQueue(), "saveMQAudit");
			log.info("MQ Audit Reply Queue: " + mqAuditVO.getReplyQueue(), "saveMQAudit");
			log.info("MQ Audit Request Message: " + mqAuditVO.getMessage(), "saveMQAudit");
			log.info("MQ Audit Reply Message: " + mqAuditVO.getReply(), "saveMQAudit");
			
			MQAuditDAO.getInstance().saveMQRequest(mqAuditVO);
			log.info("[End] Audit log MQ request", "saveMQAudit");
		} catch (Exception e) {
			log.info("Exception while saving MQ request", "saveMQAudit");
			log.info(mqAuditVO.toString(), "saveMQAudit");
			e.printStackTrace();
		}
	}    
	
    static String getValidQueueName(String wasQueueName) {
    	if (wasQueueName != null && wasQueueName.startsWith("queue:///")) {
    		wasQueueName = wasQueueName.substring(9);
    		
    		if (wasQueueName.contains("?")) {
    			wasQueueName = wasQueueName.substring(0, wasQueueName.indexOf("?"));
    		}
    	}
    	
    	
    	return wasQueueName;
    }	
}