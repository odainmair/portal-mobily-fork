package sa.com.mobily.eportal.common.service.lookup;

/**
 * Class containing constants for Ids in Lookup type in lookup table.
 * 
 * @author Muzammil Mohsin Shaikh
 */
public interface LookupIdConstants {

	public static final String FIELD_ID = "id";
	public static final Long LOOKUP_ID_SECURITY_QUESTION = 38L;
	public static final Long LOOKUP_ID_SECURITY_QUESTION_DEV = 34L;
	public static final Long LOOKUP_ID_NATIONALITY = 9L;
	public static final Long LOOKUP_ID_PREFERRED_LANGUAGE= 0L;
	public static final Long LOOKUP_ID_AGE = 7L;
	public static final Long LOOKUP_ID_BACK_END_DATA = 11L;
	
	public static final String LOOKUP_SALES_ORDER = "SALES_ORDER";
	public static final String LOOKUP_CITY = "CITY";
	public static final String LOOKUP_SCENARIO = "20";
	public static final String LOOKUP_EMIRATES = "EEMBL_Emirates";
	public static final String LOOKUP_GOVERNATE = "EEMBL_Governates";
	public static final String LOOKUP_CITY_CMS = "EEMBL_City";
	public static final String LOOKUP_DISTRICT = "EEMBL_District";
	public static final String LOOKUP_REGION = "EEMBL_Region";
}
