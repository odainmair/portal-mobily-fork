package sa.com.mobily.eportal.common.util.ldap;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.Filter;
import org.springframework.ldap.pool.factory.PoolingContextSource;
import org.springframework.ldap.pool.validation.DefaultDirContextValidator;
import org.springframework.ldap.support.LdapNameBuilder;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.constants.PumaProfileConstantsIfc;
import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.service.ldapservice.LDAPManagerFactory;
import sa.com.mobily.eportal.common.service.ldapservice.MobilyUser;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

//Updated By Rasha 
public class SpringLDAPManager 
{

//public class SpringLDAPManager implements LDAPManagerInterface
//{

//	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
//	
//	private static final String className = SpringLDAPManager.class.getName();
//	
	private static SpringLDAPManager instance;
//
//	private   String LDAP_USER_NAME = "LDAP_USER_NAME";
//
//	private   String LDAP_PASSWORD = "LDAP_PASSWORD";
//
//	private  final String LDAP_IP_ADDRESS = "LDAP_IP_ADDRESS";
//
//	private  final String LDAP_PORT = "LDAP_PORT";
//
//	private   String LDAP_USERS_BASE = "LDAP_USERS_BASE";
//	
//	private   String LDAP_GROUPS_BASE = "LDAP_GROUPS_BASE";
//	
//	private String LDAP_URL_PREFIX="ldap://";
//
//	private String LDAP_MAX_CONN ="LDAP_MAX_CONN";
//	private String LDAP_MAX_ACT_CONN ="LDAP_MAX_ACT_CONN";
//	
	public static SpringLDAPManager getInstance()
	{
		if (instance == null ){
			instance = new SpringLDAPManager();
		}
		return instance;
	}
	
//	
//	public  LdapContextSource getLdapContextSource(String base){
//		
//		String methodName="getLdapContextSource";
//		//executionContext.log(Level.INFO, "SpringLDAPManager >> getLdapContextSource > start", className, methodName);
//		long startTime= System.currentTimeMillis();
//		
//		LdapContextSource contextSource = new LdapContextSource();
//		StringBuffer url=new StringBuffer();
//		url.append(LDAP_URL_PREFIX);
//		url.append(ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_IP_ADDRESS));
//		//url.append("10.14.170.165");
//		url.append(":");
//		url.append(ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_PORT));
//		//url.append("389");
//		contextSource.setUrl(url.toString());
//		contextSource.setBase((String)ResourceEnvironmentProviderService.getInstance().getPropertyValue(base));
//		contextSource.setUserDn((String)ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_USER_NAME));
//		contextSource.setPassword((String)ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_PASSWORD));
//		contextSource.setPooled(true);
//		/*contextSource.setBase("cn=users,O=MOBILY,C=SA");
//		contextSource.setUserDn("uid=wpsadmin,cn=users,o=mobily,c=sa");
//		contextSource.setPassword("passw0rd");
//		contextSource.setPooled(true);*/
//		
//		contextSource.afterPropertiesSet();
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > Got the LDAPContextSource........ ", className, methodName);
//		long endTime=System.currentTimeMillis();
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > Time taken to get the LDAPContextSource........ "+(endTime-startTime), className, methodName);
//		//System.out.println("Time taken to get the LDAPContextSource........ "+(endTime-startTime));
//		return contextSource;
//	}
//	
//	public boolean isValidUser(String userName, String pwd ){
//		String methodName="isValidUser";
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > start...", className, methodName);
//		boolean isValid =false;
//		try{
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_USERS_BASE));
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > Got the LDAPTemplate....", className, methodName);
//			ldapTemplate.afterPropertiesSet();
//	
//			Filter filter = new EqualsFilter("uid",userName);
//	
//			isValid= ldapTemplate.authenticate(DistinguishedName.EMPTY_PATH, filter.encode(),pwd);
//			
//		}catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+methodName+" >Exception During isValidUser >"+e.getMessage(), className, methodName, e);
//		}
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > isValid:: "+isValid, className, methodName);
//		return isValid;
//	}
//	
	public MobilyUser authenticateAndGetUserDetails(String username, String password){
		
		return LDAPManagerFactory.getInstance().getLDAPManagerImpl().authenticateAndGetUserDetails(username, password);
		
//		String methodName="authenticateAndGetUserDetails";
//		
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > start", className, methodName);
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > Username::"+username, className, methodName);
//		/*if(isValidUser(username, FormatterUtility.checkNull(password))){
//			executionContext.log(Level.INFO, "SpringLDAPManager > authenticateAndGetUserDetails > userName =["+username+"] credentials are valid.", className, methodName);*/
//			
//			MobilyUser mobilyuserObj = findMobilyUserByAttribute(PumaProfileConstantsIfc.uid, FormatterUtility.checkNull(username));
//
//			if(mobilyuserObj!=null){
//				if(mobilyuserObj.getUserPassword().equalsIgnoreCase(password)){
//					executionContext.log(Level.INFO, "SpringLDAPManager > "+methodName+" > userName =["+username+"] credentials are valid.", className, methodName);
//					return mobilyuserObj;
//				}else{
//					
//					executionContext.log(Level.INFO, "SpringLDAPManager > "+methodName+" > Profile Exists and password entered by user is not correct ---- userName =["+username+"] .", className, methodName);
//					return null;
//				}
//			}else{
//				executionContext.log(Level.INFO, "SpringLDAPManager > "+methodName+" > userName =["+username+"] credentials are not valid.", className, methodName);
//				return null;
//			}
//			
//			
//		/*}else{
//			executionContext.log(Level.INFO, "SpringLDAPManager > authenticateAndGetUserDetails > userName =["+username+"] credentials are not valid.", className, methodName);
//		}*/
		
		
	}

	public MobilyUser findMobilyUserByAttribute(String attributeName, String attributeValue)
	{
		return LDAPManagerFactory.getInstance().getLDAPManagerImpl().findMobilyUserByAttribute(attributeName,attributeValue);
//		Map<String, String> attributes = new HashMap<String, String>();
//		attributes.put(attributeName, attributeValue);
//		return findMobilyUserByAttributes(attributes);
	}
//	
//	public List<MobilyUser> findMobilyUserListByAttribute(String attributeName, String attributeValue)
//	{
//		Map<String, String> attributes = new HashMap<String, String>();
//		attributes.put(attributeName, attributeValue);
//		return findMobilyUserListByAttributes(attributes);
//	}
//	
	public MobilyUser findMobilyUserByAttributes(Map<String, String> attributes)
	{
		return LDAPManagerFactory.getInstance().getLDAPManagerImpl().findMobilyUserByAttributes(attributes);
//		String methodName = "findMobilyUserByAttributes";
//		executionContext.audit(Level.INFO, "SpringLDAPManager > "+methodName+" > called", className, methodName);
//		
//		MobilyUser user = null;
//		try{
//			PoolingContextSource poolContextSource =  getPoolingLDAPContextSource();
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(poolContextSource);
//			ldapTemplate.afterPropertiesSet();
//			//System.out.println("No of  MAx Connections::"+poolContextSource.getMaxTotal());
//			//System.out.println("No of  Idle Connections::"+poolContextSource.getNumIdle());
//			//executionContext.audit(Level.INFO, "SpringLDAPManager > findMobilyUserByAttributes > Max Active Connections "+poolContextSource.getMaxActive(), className, methodName);
//			SearchControls controls = new SearchControls();
//			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
//		
//			String[] attributeIds = { PumaProfileConstantsIfc.cn, PumaProfileConstantsIfc.sn, PumaProfileConstantsIfc.uid, PumaProfileConstantsIfc.defaultMsisdn,
//				PumaProfileConstantsIfc.email, PumaProfileConstantsIfc.preferredLanguage, PumaProfileConstantsIfc.serviceAccountNumber,
//				PumaProfileConstantsIfc.defaultSubscriptionType, PumaProfileConstantsIfc.defaultSubId, PumaProfileConstantsIfc.corpMasterAcctNo,
//				PumaProfileConstantsIfc.corpAPNumber, PumaProfileConstantsIfc.corpAPIDNumber, PumaProfileConstantsIfc.commercialRegisterationID,
//				PumaProfileConstantsIfc.corpSurveyCompleted, "userPassword", PumaProfileConstantsIfc.givenName, PumaProfileConstantsIfc.Status,
//				PumaProfileConstantsIfc.firstName, PumaProfileConstantsIfc.lastName, PumaProfileConstantsIfc.custSegment, PumaProfileConstantsIfc.roleId,
//				PumaProfileConstantsIfc.SecurityQuestion, PumaProfileConstantsIfc.SecurityAnswer, PumaProfileConstantsIfc.birthDate, PumaProfileConstantsIfc.nationality,
//				PumaProfileConstantsIfc.Gender, PumaProfileConstantsIfc.registerationDate, PumaProfileConstantsIfc.iqama, PumaProfileConstantsIfc.facebookId,
//				PumaProfileConstantsIfc.registrationSourceId, PumaProfileConstantsIfc.defaultSubSubscriptionType, PumaProfileConstantsIfc.userAcctReference,
//				PumaProfileConstantsIfc.contactNumber, PumaProfileConstantsIfc.twitterId };
//
//			controls.setReturningAttributes(attributeIds);
//		
//		
//			//String userName=null;
//			AndFilter andFilter = new AndFilter();
//			andFilter.and(new EqualsFilter("objectclass", "person"));
//			
//			for (Iterator<String> iterator = attributes.keySet().iterator(); iterator.hasNext();)
//			{
//				String attributeName = (String) iterator.next();
//				executionContext.audit(Level.INFO, "SpringLDAPManager > "+methodName+" > attributeName::"+attributeName, className, methodName);
//				String attributeValue=attributes.get(attributeName);
//				executionContext.audit(Level.INFO, "SpringLDAPManager > "+methodName+" > attributeValue::"+attributeValue, className, methodName);
//				andFilter.and(new EqualsFilter(attributeName, attributeValue));
//				//userName = attributes.get(attributeName);
//				   
//			}
//			
//			List<MobilyUser>  mobilyUserList=ldapTemplate.search(DistinguishedName.EMPTY_PATH,andFilter.encode(),controls,new UserAttributesMapper());
//			//executionContext.audit(Level.INFO, "SpringLDAPManager > findMobilyUserByAttributes > Got the result for the LDAP search ..............", className, methodName);
//			if(CollectionUtils.isNotEmpty(mobilyUserList)){
//				user=mobilyUserList.get(0);
//			}
//		
//		}
//		catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager > "+methodName+" >Exception During isValidUser >"+e.getMessage(), className, methodName, e);
//		}
//		
//		return user;
	}
//	
//	public List<MobilyUser> findMobilyUserListByAttributes(Map<String, String> attributes)
//	{
//		String methodName = "findMobilyUserByAttributes";
//		executionContext.audit(Level.INFO, "SpringLDAPManager > "+methodName+" > called", className, methodName);
//		
//		List<MobilyUser>  mobilyUserList = null;
//		try{
//			PoolingContextSource poolContextSource =  getPoolingLDAPContextSource();
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(poolContextSource);
//			ldapTemplate.afterPropertiesSet();
//			//System.out.println("No of  MAx Connections::"+poolContextSource.getMaxTotal());
//			//System.out.println("No of  Idle Connections::"+poolContextSource.getNumIdle());
//			//executionContext.audit(Level.INFO, "SpringLDAPManager > findMobilyUserByAttributes > Max Active Connections "+poolContextSource.getMaxActive(), className, methodName);
//			SearchControls controls = new SearchControls();
//			controls.setSearchScope(SearchControls.ONELEVEL_SCOPE);
//		
//			String[] attributeIds = { PumaProfileConstantsIfc.cn, PumaProfileConstantsIfc.sn, PumaProfileConstantsIfc.uid, PumaProfileConstantsIfc.defaultMsisdn,
//				PumaProfileConstantsIfc.email, PumaProfileConstantsIfc.preferredLanguage, PumaProfileConstantsIfc.serviceAccountNumber,
//				PumaProfileConstantsIfc.defaultSubscriptionType, PumaProfileConstantsIfc.defaultSubId, PumaProfileConstantsIfc.corpMasterAcctNo,
//				PumaProfileConstantsIfc.corpAPNumber, PumaProfileConstantsIfc.corpAPIDNumber, PumaProfileConstantsIfc.commercialRegisterationID,
//				PumaProfileConstantsIfc.corpSurveyCompleted, "userPassword", PumaProfileConstantsIfc.givenName, PumaProfileConstantsIfc.Status,
//				PumaProfileConstantsIfc.firstName, PumaProfileConstantsIfc.lastName, PumaProfileConstantsIfc.custSegment, PumaProfileConstantsIfc.roleId,
//				PumaProfileConstantsIfc.SecurityQuestion, PumaProfileConstantsIfc.SecurityAnswer, PumaProfileConstantsIfc.birthDate, PumaProfileConstantsIfc.nationality,
//				PumaProfileConstantsIfc.Gender, PumaProfileConstantsIfc.registerationDate, PumaProfileConstantsIfc.iqama, PumaProfileConstantsIfc.facebookId,
//				PumaProfileConstantsIfc.registrationSourceId, PumaProfileConstantsIfc.defaultSubSubscriptionType, PumaProfileConstantsIfc.userAcctReference,
//				PumaProfileConstantsIfc.contactNumber, PumaProfileConstantsIfc.twitterId };
//
//			controls.setReturningAttributes(attributeIds);
//		
//		
//			//String userName=null;
//			AndFilter andFilter = new AndFilter();
//			andFilter.and(new EqualsFilter("objectclass", "person"));
//			
//			for (Iterator<String> iterator = attributes.keySet().iterator(); iterator.hasNext();)
//			{
//				String attributeName = (String) iterator.next();
//				executionContext.audit(Level.INFO, "SpringLDAPManager > "+methodName+" > attributeName::"+attributeName, className, methodName);
//				String attributeValue=attributes.get(attributeName);
//				executionContext.audit(Level.INFO, "SpringLDAPManager > "+methodName+" > attributeValue::"+attributeValue, className, methodName);
//				andFilter.and(new EqualsFilter(attributeName, attributeValue));
//				//userName = attributes.get(attributeName);
//				   
//			}
//			
//			mobilyUserList = ldapTemplate.search(DistinguishedName.EMPTY_PATH,andFilter.encode(),controls,new UserAttributesMapper());
//			//executionContext.audit(Level.INFO, "SpringLDAPManager > findMobilyUserByAttributes > Got the result for the LDAP search ..............", className, methodName);
//		
//		}
//		catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager > "+methodName+" >Exception During isValidUser >"+e.getMessage(), className, methodName, e);
//		}
//		
//		return mobilyUserList;
//	}
//	
//	public  PoolingContextSource getPoolingLDAPContextSource(){
//
//		String methodName="getPoolingLDAPContextSource";
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > start", className, methodName);
//		long startTime= System.currentTimeMillis();
//		
//		PoolingContextSource poolingContextSource = new PoolingContextSource();
//		poolingContextSource.setDirContextValidator(new DefaultDirContextValidator());
//	    poolingContextSource.setContextSource(getLdapContextSource(LDAP_USERS_BASE));
//	    poolingContextSource.setTestOnBorrow(true);
//	    poolingContextSource.setTestWhileIdle(true);
//	    poolingContextSource.setMaxTotal(Integer.parseInt((String)(ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_MAX_CONN))));
//	    poolingContextSource.setMaxActive(Integer.parseInt((String)(ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_MAX_ACT_CONN))));
//	    //TransactionAwareContextSourceProxy proxy = new TransactionAwareContextSourceProxy(poolingContextSource);
//	    //executionContext.log(Level.INFO, "SpringLDAPManager >> getPoolingLDAPContextSource > got the pool context source...........", className, methodName);
//	    long endTime= System.currentTimeMillis();
//	    executionContext.log(Level.INFO, "SpringLDAPManager >> "+methodName+" > Time taken to get the pool context source..........."+(endTime-startTime), className, methodName);
//	    return poolingContextSource;
//	}
//
//
	public void createNewUserOnLDAP(@NotNull MobilyUser mobilyUser){
		LDAPManagerFactory.getInstance().getLDAPManagerImpl().createNewUserOnLDAP(mobilyUser);
		
//		String methodName = "createNewUserOnLDAP";
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > mobilyUser ["+mobilyUser+"]", className, methodName);
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > username =["+FormatterUtility.checkNull(mobilyUser.getUid()).trim().toLowerCase()+"]", className, methodName);
//
//		try{
//		
//			Name userDn =getUserDn(mobilyUser.getUid());
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > Got the User DN", className, methodName);
//			DirContextAdapter context = new DirContextAdapter(userDn);
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > Got the Context using User DN", className, methodName);
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_USERS_BASE));
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > Got the LDAP Template", className, methodName);
//			context.setAttributeValues( "objectclass", 
//				      new String[] 
//				        { "top", 
//				          "person", 
//				          "organizationalPerson", 
//				          "inetOrgPerson" });
//				    
//			context.setAttributeValue(PumaProfileConstantsIfc.cn, mobilyUser.getCn());
//		    context.setAttributeValue(PumaProfileConstantsIfc.sn, mobilyUser.getSn());
//		    context.setAttributeValue(PumaProfileConstantsIfc.uid, FormatterUtility.checkNull(mobilyUser.getUid()).trim().toLowerCase());
//		    context.setAttributeValue(PumaProfileConstantsIfc.defaultMsisdn, mobilyUser.getDefaultMsisdn());
//		    context.setAttributeValue(PumaProfileConstantsIfc.serviceAccountNumber, (FormatterUtility.toLowerCase(mobilyUser.getServiceAccountNumber())));
//		    context.setAttributeValue(PumaProfileConstantsIfc.email, FormatterUtility.checkNull(mobilyUser.getEmail()).trim().toLowerCase());
//		    context.setAttributeValue(PumaProfileConstantsIfc.preferredLanguage, mobilyUser.getPreferredLanguage());
//		    context.setAttributeValue(PumaProfileConstantsIfc.firstName, mobilyUser.getFirstName());
//		    context.setAttributeValue(PumaProfileConstantsIfc.lastName, mobilyUser.getLastName());
//		    context.setAttributeValue(PumaProfileConstantsIfc.givenName, mobilyUser.getGivenName());
//		    context.setAttributeValue("userPassword", FormatterUtility.checkNull(mobilyUser.getUserPassword()).trim());
//		    context.setAttributeValue(PumaProfileConstantsIfc.iqama, mobilyUser.getIqama());
//		    context.setAttributeValue(PumaProfileConstantsIfc.defaultSubId, mobilyUser.getDefaultSubId());
//		    context.setAttributeValue(PumaProfileConstantsIfc.defaultSubscriptionType, ""+mobilyUser.getDefaultSubscriptionType());
//		    context.setAttributeValue(PumaProfileConstantsIfc.defaultSubSubscriptionType,""+ mobilyUser.getDefaultSubSubscriptionType());
//		    context.setAttributeValue(PumaProfileConstantsIfc.corpMasterAcctNo, mobilyUser.getCorpMasterAcctNo());
//		    context.setAttributeValue(PumaProfileConstantsIfc.corpAPNumber, mobilyUser.getCorpAPNumber());
//		    context.setAttributeValue(PumaProfileConstantsIfc.corpAPIDNumber, mobilyUser.getCorpAPIDNumber());
//		    context.setAttributeValue(PumaProfileConstantsIfc.commercialRegisterationID, mobilyUser.getCommercialRegisterationID());
//		    context.setAttributeValue(PumaProfileConstantsIfc.corpSurveyCompleted, ""+mobilyUser.isCorpSurveyCompleted());
//		    context.setAttributeValue(PumaProfileConstantsIfc.Status, mobilyUser.getStatus());
//		    context.setAttributeValue(PumaProfileConstantsIfc.roleId, ""+mobilyUser.getRoleId());
//		    context.setAttributeValue(PumaProfileConstantsIfc.custSegment, mobilyUser.getCustSegment());
//		    context.setAttributeValue(PumaProfileConstantsIfc.SecurityQuestion, mobilyUser.getSecurityQuestion());
//		    context.setAttributeValue(PumaProfileConstantsIfc.SecurityAnswer, mobilyUser.getSecurityAnswer());
//		    context.setAttributeValue(PumaProfileConstantsIfc.nationality, mobilyUser.getNationality());
//		    context.setAttributeValue(PumaProfileConstantsIfc.Gender, mobilyUser.getGender());
//		    context.setAttributeValue(PumaProfileConstantsIfc.facebookId, FormatterUtility.toLowerCase(mobilyUser.getFacebookId()));
//		    context.setAttributeValue(PumaProfileConstantsIfc.twitterId, FormatterUtility.toLowerCase(mobilyUser.getTwitterId()));
//		    context.setAttributeValue(PumaProfileConstantsIfc.userAcctReference, mobilyUser.getUserAcctReference());
//		    context.setAttributeValue(PumaProfileConstantsIfc.registrationSourceId, ""+mobilyUser.getRegistrationSourceId());
//		    context.setAttributeValue(PumaProfileConstantsIfc.contactNumber, mobilyUser.getContactNumber());
//
//		    ldapTemplate.bind(context);
//		    executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > user created successfully in LDAP", className, methodName);
//		    
//		    
//		    if (mobilyUser.getGroupList() != null && mobilyUser.getGroupList().size() > 0)
//			{
//				for (Iterator<String> iterator = mobilyUser.getGroupList().iterator(); iterator.hasNext();)
//				{
//					assignUser(iterator.next(),userDn);
//				}
//			}
//		    
//		    executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > user assigned to the group successfully in LDAP", className, methodName);
//		    
//		}
//		catch(Exception e){
//			
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" > Exception during User Creation ::"+e.getMessage(), className, methodName, e);
//			
//			throw new MobilyApplicationException(e.getMessage(), e.getCause());
//		}
		
	}
//	
//	private Name getUserDn(@NotNull String userName){
//		 return LdapNameBuilder.newInstance().add(PumaProfileConstantsIfc.uid, FormatterUtility.checkNull(userName).trim().toLowerCase()).build();
//	}
//	
//	private Name getGroupDn(@NotNull String groupName){
//		return LdapNameBuilder.newInstance().add(PumaProfileConstantsIfc.cn, FormatterUtility.checkNull(groupName)).build();
//	}
//	
//	private void assignUser(String groupName,Name userDn) throws MobilyApplicationException{
//		String methodName="assignUser";
//		try{
//			Name groupDn= getGroupDn(groupName);
//			executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> Got the groupDn", className, methodName);
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_GROUPS_BASE));
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > Got the LDAP Template for groupdn", className, methodName);
//			DirContextOperations ctx = ldapTemplate.lookupContext(groupDn);
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > Got the ctx for the groupdn", className, methodName);
//			ctx.addAttributeValue("uniqueMember",userDn);
//			ldapTemplate.modifyAttributes(ctx);
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > Assigned the user to the group successfully", className, methodName);
//		}catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" > Exception while assigning the user to the group ::"+e.getMessage(), className, methodName, e);
//			throw new MobilyApplicationException(e.getMessage(), e.getCause());
//		}
//		
//	}
//
//	public void assignUserToGroup(String username, String groupName){
//		String methodName="assignUserToGroup";
//		try{
//			Name userDn =getUserDn(username);
//			assignUser(groupName,userDn);
//			
//		}catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" > Exception while assigning the user to the group ::"+e.getMessage(), className, methodName, e);
//			throw new MobilyApplicationException(e.getMessage(), e.getCause());
//		}
//	}
//	
//	public void deleteUser(@NotNull MobilyUser mobilyUser){
//		
//		String methodName = "deleteUser";
//		executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > username =["+mobilyUser.getUid()+"]", className, methodName, null);
//		try{
//			Name userDn =getUserDn(mobilyUser.getUid());
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_USERS_BASE));
//			
//			ldapTemplate.unbind(userDn);
//			
//			executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" > user deleted successfully in LDAP", className, methodName);
//		}catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" >Exception during deleteUser:: "+e.getMessage(), className, methodName, e);
//			throw new RuntimeException(e);
//		}
//	}
//	
//	public void updateUserOnLDAPByMSISDN(String MSISDN, String attrName, String attrValue){
//		
//		MobilyUser user = findMobilyUserByAttribute(PumaProfileConstantsIfc.defaultMsisdn, MSISDN);
//		if (user != null)
//		{
//			updateUserOnLDAPByUserId(user.getUid(), attrName, attrValue);
//		}
//	}
//	
//	public void updateUserOnLDAPByUserId(String userId, String attrName, String attrValue){
//		
//		String methodName="updateUserOnLDAPByUserId";
//		executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> userId::"+userId, className, methodName);
//		try
//		{
//			Name userDn =getUserDn(userId);
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_USERS_BASE));
//			
//			DirContextOperations ctx = ldapTemplate.lookupContext(userDn);
//			ctx.setAttributeValue(attrName,attrValue);
//			
//			ldapTemplate.modifyAttributes(ctx);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" >Exception during updateUserOnLDAPByUserId > "+e.getMessage(), className, methodName, e);
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_UPDATE_ATTRIBUTE_LDAP_ID");
//		}
//	}
//	
	public void updatePasswordOnLDAP(String userName, String newPassword){
		LDAPManagerFactory.getInstance().getLDAPManagerImpl().updatePasswordOnLDAP(userName, newPassword);
//		updateUserOnLDAPByUserId(userName, "userPassword", newPassword);
	}
//	
//	
//	public void updateUserAttributesOnLDAP(String userId,  Map<String, Object> attributes){
//		
//		String methodName="updateUserAttributesOnLDAP";
//		executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> userId::"+userId, className, methodName);
//		try
//		{
//			Name userDn =getUserDn(userId);
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_USERS_BASE));
//			
//			DirContextOperations ctx = ldapTemplate.lookupContext(userDn);
//			for (String key : attributes.keySet()){
//				executionContext.log(Level.INFO, "SpringLDAPManager >> "+ methodName +" >modifying attribute: " + key + ", value :" + String.valueOf(attributes.get(key)), className, methodName, null);
//				ctx.setAttributeValue(key,String.valueOf(attributes.get(key)));
//			}
//			ldapTemplate.modifyAttributes(ctx);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" >Exception during updateUserAttributesOnLDAP > "+e.getMessage(), className, methodName, e);
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_UPDATE_ATTRIBUTE_LDAP_ID");
//		}
//	}
//	
//	public void updateUserAttributesOnLDAPByUserId(String userId, Map<String, Object> attributes){
//		
//		String methodName = "updateUserAttributesOnLDAPByUserId";
//		executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> userId::"+userId, className, methodName);
//		updateUserAttributesOnLDAP(userId, attributes);
//	}
//	
//	public void updateUserGroup(String userId, String oldGroup, String newGroup){
//		String methodName = "updateUserGroup";
//		executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> userId::"+userId, className, methodName);
//		try
//		{
//			removeGroup(userId,oldGroup);
//			executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> removed user from old group", className, methodName);
//			assignUserToGroup(userId, newGroup);
//			executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> assign user to new group", className, methodName);
//			
//		}catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" >Exception During updateUserGroup :: "+e.getMessage(), className, methodName, e);
//		}
//	}
//
//	private void removeGroup(String username, String groupName) throws MobilyApplicationException
//	{
//		String methodName = "removeGroup";
//		executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> username::"+username, className, methodName);
//		try
//		{
//			Name userDn =getUserDn(username);
//			Name groupDn= getGroupDn(groupName);
//			
//			LdapTemplate ldapTemplate= new LdapTemplate(getLdapContextSource(LDAP_GROUPS_BASE));
//			
//			DirContextOperations ctx = ldapTemplate.lookupContext(groupDn);
//			ctx.removeAttributeValue("uniqueMember",userDn);
//			ldapTemplate.modifyAttributes(ctx);
//			
//			executionContext.log(Level.INFO, "SpringLDAPManager >>"+ methodName +"> Removed the group for the user successfully...", className, methodName);
//		}catch(Exception e){
//			executionContext.log(Level.SEVERE, "SpringLDAPManager >> "+ methodName +" >Exception During removeGroup :: "+e.getMessage(), className, methodName, e);
//			throw new MobilyApplicationException("BACKEND_ERROR_UNABLE_TO_REMOVE_USER_TO_GROUP_ID");
//		}
//	}
//	
//	public boolean checkMembership(String username, String groupName){
//		
//		return false;
//	}
//	private class UserAttributesMapper implements AttributesMapper<MobilyUser> {
//
//		@Override
//		public MobilyUser mapFromAttributes(Attributes attributes) throws NamingException {
//			
//			String methodName="mapFromAttributes";
//			executionContext.log(Level.INFO, "SpringLDAPManager >> Inner class----UserAttributesMapper >mapFromAttributes >> start", className, methodName);
//			MobilyUser mobilyUser =null;
//			try{
//				if (attributes == null) {
//					return mobilyUser;
//				}
//				
//				mobilyUser = new MobilyUser();
//				if (attributes.get(PumaProfileConstantsIfc.cn) != null)
//					mobilyUser.setCn(attributes.get(PumaProfileConstantsIfc.cn).get().toString());
//	
//				if (attributes.get("userPassword") != null) {
//					String userPassword = null;
//					try {
//						userPassword = new String((byte[]) attributes.get("userPassword").get(), "UTF-8");
//					} catch (UnsupportedEncodingException e) {
//						executionContext.log(Level.SEVERE, "SpringLDAPManager >> Inner class----UserAttributesMapper >mapFromAttributes--Exception while getting the userpassword ::"+e.getMessage(), className, methodName, e);
//					}
//					mobilyUser.setUserPassword(userPassword);
//				}
//				if (attributes.get(PumaProfileConstantsIfc.uid) != null) {
//					mobilyUser.setUid(attributes.get(PumaProfileConstantsIfc.uid).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.sn) != null) {
//					mobilyUser.setSn(attributes.get("sn").get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.firstName) != null) {
//					mobilyUser.setFirstName(attributes.get(PumaProfileConstantsIfc.firstName).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.lastName) != null) {
//					mobilyUser.setLastName(attributes.get(PumaProfileConstantsIfc.lastName).get().toString());
//				}
//				
//				
//				if (attributes.get(PumaProfileConstantsIfc.defaultMsisdn) != null) {
//					mobilyUser.setDefaultMsisdn(attributes.get(PumaProfileConstantsIfc.defaultMsisdn).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.email) != null) {
//					mobilyUser.setEmail(attributes.get(PumaProfileConstantsIfc.email).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.preferredLanguage) != null) {
//					mobilyUser.setPreferredLanguage(attributes.get(PumaProfileConstantsIfc.preferredLanguage).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.serviceAccountNumber) != null) {
//					mobilyUser.setServiceAccountNumber(attributes.get(PumaProfileConstantsIfc.serviceAccountNumber).get().toString());
//				}
//				
//				if (attributes.get(PumaProfileConstantsIfc.defaultSubscriptionType) != null)
//					mobilyUser.setDefaultSubscriptionType(Integer.parseInt(attributes.get(PumaProfileConstantsIfc.defaultSubscriptionType).get().toString()));
//				if (attributes.get(PumaProfileConstantsIfc.defaultSubSubscriptionType) != null)
//					mobilyUser.setDefaultSubSubscriptionType(Integer.parseInt(attributes.get(PumaProfileConstantsIfc.defaultSubSubscriptionType).get().toString()));
//				
//				if (attributes.get(PumaProfileConstantsIfc.defaultSubId) != null)
//					mobilyUser.setDefaultSubId(attributes.get(PumaProfileConstantsIfc.defaultSubId).get().toString());
//				
//				if (attributes.get(PumaProfileConstantsIfc.corpMasterAcctNo) != null)
//					mobilyUser.setCorpMasterAcctNo(attributes.get(PumaProfileConstantsIfc.corpMasterAcctNo).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.corpAPNumber) != null)
//					mobilyUser.setCorpAPNumber(attributes.get(PumaProfileConstantsIfc.corpAPNumber).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.corpAPIDNumber) != null)
//					mobilyUser.setCorpAPIDNumber(attributes.get(PumaProfileConstantsIfc.corpAPIDNumber).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.commercialRegisterationID) != null)
//					mobilyUser.setCommercialRegisterationID(attributes.get(PumaProfileConstantsIfc.commercialRegisterationID).get().toString());
//				
//				if (attributes.get(PumaProfileConstantsIfc.corpSurveyCompleted) != null){
//					
//					mobilyUser.setCorpSurveyCompleted(Boolean.parseBoolean(attributes.get(PumaProfileConstantsIfc.corpSurveyCompleted).get().toString()));
//				}
//				if (attributes.get(PumaProfileConstantsIfc.givenName) != null)
//					mobilyUser.setGivenName(attributes.get(PumaProfileConstantsIfc.givenName).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.Status) != null)
//					mobilyUser.setStatus(attributes.get(PumaProfileConstantsIfc.Status).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.custSegment) != null)
//					mobilyUser.setCustSegment(attributes.get(PumaProfileConstantsIfc.custSegment).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.roleId) != null){
//					mobilyUser.setRoleId(Integer.parseInt(attributes.get(PumaProfileConstantsIfc.roleId).get().toString()));
//				}
//				if (attributes.get(PumaProfileConstantsIfc.SecurityQuestion) != null)
//					mobilyUser.setSecurityQuestion(attributes.get(PumaProfileConstantsIfc.SecurityQuestion).get().toString());
//				if (attributes.get(PumaProfileConstantsIfc.SecurityAnswer) != null)
//					mobilyUser.setSecurityAnswer(attributes.get(PumaProfileConstantsIfc.SecurityAnswer).get().toString());
//	
//				try{
//					if (attributes.get(PumaProfileConstantsIfc.birthDate) != null){
//						mobilyUser.setBirthDate(new SimpleDateFormat("yyyyMMddHHmmss").parse(attributes.get(PumaProfileConstantsIfc.birthDate).get().toString()));
//					}
//					
//					if (attributes.get(PumaProfileConstantsIfc.registerationDate) != null){
//						mobilyUser.setRegisterationDate(new SimpleDateFormat("yyyyMMddHHmmss").parse(attributes.get(PumaProfileConstantsIfc.registerationDate).get().toString()));
//					}
//				}catch(Exception e){
//					executionContext.log(Level.SEVERE, "SpringLDAPManager >> Inner class----UserAttributesMapper >mapFromAttributes--Exception while parsing the birthdate and registration date ::"+e.getMessage(), className, methodName, e);
//				}
//				if (attributes.get(PumaProfileConstantsIfc.nationality) != null){
//					mobilyUser.setNationality(attributes.get(PumaProfileConstantsIfc.nationality).get().toString());
//				}
//			
//				if (attributes.get(PumaProfileConstantsIfc.Gender) != null){
//					mobilyUser.setGender(attributes.get(PumaProfileConstantsIfc.Gender).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.userAcctReference) != null){
//					mobilyUser.setUserAcctReference(attributes.get(PumaProfileConstantsIfc.userAcctReference).get().toString());
//				}
//				
//				if (attributes.get(PumaProfileConstantsIfc.iqama) != null){
//					mobilyUser.setIqama(attributes.get(PumaProfileConstantsIfc.iqama).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.facebookId) != null){
//					mobilyUser.setFacebookId(attributes.get(PumaProfileConstantsIfc.facebookId).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.twitterId) != null){
//					mobilyUser.setTwitterId(attributes.get(PumaProfileConstantsIfc.twitterId).get().toString());
//				}
//				if (attributes.get(PumaProfileConstantsIfc.registrationSourceId) != null){
//					mobilyUser.setRegistrationSourceId(Integer.parseInt(attributes.get(PumaProfileConstantsIfc.registrationSourceId).get().toString()));
//				}
//				if (attributes.get(PumaProfileConstantsIfc.contactNumber) != null)
//					mobilyUser.setContactNumber(attributes.get(PumaProfileConstantsIfc.contactNumber).get().toString());
//				
//			}catch(Exception e){
//				executionContext.log(Level.SEVERE, "SpringLDAPManager >> Inner class----UserAttributesMapper >mapFromAttributes--Exception while fetching the user profile from LDAP ::"+e.getMessage(), className, methodName, e);
//			}
//			executionContext.log(Level.INFO, "SpringLDAPManager >> Inner class----UserAttributesMapper >mapFromAttributes >> end", className, methodName);
//			return mobilyUser;
//		}
//	}
//	
}
