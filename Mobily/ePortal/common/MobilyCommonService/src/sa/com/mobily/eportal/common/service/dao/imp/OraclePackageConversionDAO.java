package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.PackageConversionDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.PackageTypeVO;

/**
 * 
 * @author n.gundluru.mit
 * 
 */
public class OraclePackageConversionDAO implements PackageConversionDAO {

	private static final Logger log = LoggerInterface.log;

	/**
	 * @return
	 * @throws ContentProviderException
	 */
	public ArrayList loadPackageTypesFromDB(int customerType) {

		log.info("OraclePackageConversionDAO > loadPackageTypesFromDB > Called ");
		ArrayList PackageType = new ArrayList();

		Connection connection = null;
		ResultSet resultset = null;
		PreparedStatement pstm = null;

		try {
			String sqlStatament = "SELECT ID , NAME_EN , NAME_AR FROM DEFAULT_Package  where customer_type = ?";
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pstm = connection.prepareStatement(sqlStatament);
			pstm.setInt(1, customerType);
			resultset = pstm.executeQuery();
			PackageTypeVO objectVO = null;
			
			while (resultset.next()) {
				objectVO = new PackageTypeVO();
				objectVO.setId(resultset.getInt("ID"));
				objectVO.setPackageName_en(resultset.getString("NAME_EN"));
				objectVO.setPackageName_ar(resultset.getString("NAME_AR"));
				
				PackageType.add(objectVO);
			}
			
			log.debug("OraclePackageConversionDAO > loadPackageTypesFromDB > Done ["+ PackageType.size() + "]");
		} catch (SQLException e) {
			log.fatal("OraclePackageConversionDAO > getUserAccountWebProfile > SQLException "+e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("OraclePackageConversionDAO > getUserAccountWebProfile > Exception "+e.getMessage());
			throw new SystemException(e);
		} finally{
		    JDBCUtility.closeJDBCResoucrs(connection,pstm,resultset);		
		}

		return PackageType;
	}

	/**
	 * 
	 */
	public PackageTypeVO getDefaultPackageById(String packageId) {

		log.info("OraclePackageConversionDAO > getDefaultPackageById > Called ");

		Connection connection = null;
		ResultSet resultset = null;
		PreparedStatement pstm = null;
		PackageTypeVO objectVO = null;
		try {
			StringBuffer str = new StringBuffer();
				str.append(" select default_package.id  , default_package.name_en , default_package.name_ar");
					str.append(" from default_package , packagesfvnsettings ");
						str.append(" where default_package.id = packagesfvnsettings.default_package_id");
							str.append(" and packagesfvnsettings.packageid = ? ");

			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pstm = connection.prepareStatement(str.toString());
			pstm.setString(1, packageId);
			resultset = pstm.executeQuery();

			if (resultset.next()) {
				objectVO = new PackageTypeVO();
				objectVO.setId(resultset.getInt("ID"));
				objectVO.setPackageName_en(resultset.getString("NAME_EN"));
				objectVO.setPackageName_ar(resultset.getString("NAME_AR"));
			}
			log.debug("OraclePackageConversionDAO > getDefaultPackageById > Done ");
		} catch (SQLException e) {
			log.fatal("OraclePackageConversionDAO > getDefaultPackageById > SQLException "+e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("OraclePackageConversionDAO > getDefaultPackageById > Exception "+e.getMessage());
			throw new SystemException(e);
		} finally{
		    JDBCUtility.closeJDBCResoucrs(connection,pstm,resultset);		
		}
		
		return objectVO;
	}

	/**
	 * 
	 */
	public PackageTypeVO getDefaultPackageByDefaultId(int defaultPackageId) {

		log.info("OraclePackageConversionDAO > getDefaultPackageByDefaultId > Called ");

		Connection connection = null;
		ResultSet resultset = null;
		PreparedStatement pstm = null;
		PackageTypeVO objectVO = null;
		try {
			StringBuffer str = new StringBuffer();
				str.append(" select  id  ,  name_en ,  name_ar from  default_package where id=?");

			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			pstm = connection.prepareStatement(str.toString());
			pstm.setInt(1, defaultPackageId);
			resultset = pstm.executeQuery();

			if (resultset.next()) {
				objectVO = new PackageTypeVO();
				objectVO.setId(resultset.getInt(1));
				objectVO.setPackageName_en(resultset.getString(2));
				objectVO.setPackageName_ar(resultset.getString(3));
			}
			log.debug("OraclePackageConversionDAO > getDefaultPackageByDefaultId > Done ");
		} catch (SQLException e) {
			log.fatal("OraclePackageConversionDAO > getDefaultPackageByDefaultId > SQLException "+e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("OraclePackageConversionDAO > getDefaultPackageByDefaultId > Exception "+e.getMessage());
			throw new SystemException(e);
		} finally{
		    JDBCUtility.closeJDBCResoucrs(connection,pstm,resultset);		
		}

		return objectVO;
	}

	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
    public boolean hasPendingRequest(String msisdn) {
		
		log.info(" OraclePackageConversionDAO > hasPendingRequest > called");
		
		Connection connection = null;
		PreparedStatement pStmt=null;
		ResultSet resultSet=null;

		boolean found = false;
		
		try{
		      
			String sqlQuery = "SELECT COUNT(*) FROM SR_SERVICEREQUEST_TBL WHERE func_id = 160 AND status NOT IN (3, 5, 6, 8) AND line_number = ? ";
	 
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BSL));

			pStmt  = connection.prepareStatement(sqlQuery.toString());
			pStmt.setString(1,msisdn);
			resultSet = pStmt.executeQuery();				
			
			if(resultSet.next())
				if (resultSet.getInt(1)>0){
			    found = true;
			}
			
		} catch (SQLException e) {
			log.fatal("OraclePackageConversionDAO > hasPendingRequest > SQLException "+e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("OraclePackageConversionDAO > hasPendingRequest > Exception "+e.getMessage());
			throw new SystemException(e);
		} finally{
		    JDBCUtility.closeJDBCResoucrs(connection,pStmt,resultSet);		
		}
	
		log.debug(" OraclePackageConversionDAO > hasPendingRequest > found = "+found);
		
		return found;
	}

	public String handlePackageConversion(String xmlRequestMessage) {
		// TODO Auto-generated method stub
		return null;
	}
}