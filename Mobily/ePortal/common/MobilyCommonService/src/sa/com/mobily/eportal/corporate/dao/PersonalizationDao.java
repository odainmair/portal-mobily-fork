/**
 * 
 */
package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.PersonalizationVo;

/**
 * @author Mohamed Shalaby
 * 
 */
public class PersonalizationDao extends AbstractDBDAO<PersonalizationVo>
{
	// private static final String ETHERNET = "Ethernet";
	//
	// private static final String DIA = "DIA";
	//
	// private static final String IPVPN = "IPVPN";

	private static final String ICE_CUBE_ELIGIBILITY = "ICE_CUBE_ELIGIBILITY";

	// private static final String dataConnectivityQuery = "Select * from " +
	// "(SELECT count(*) IPVPN FROM ( " +
	// "SELECT distinct *  FROM ACCOUNT_HIERARCHY_CORP_TBL "
	// + "START WITH account_no = ? CONNECT BY prior ID = PARENT_ID " +
	// ") subQ "
	// +
	// "WHERE subQ.Account_type  = 'Service' AND subQ.PACKAGE_NAME IS NOT NULL AND subQ.account_status = 'Active' "
	// + "START WITH account_type = 'Billing' "
	// +
	// "AND Product_type  = 'IPVPN' CONNECT BY prior ID = PARENT_ID order by subQ.customer_id),"
	// + "(SELECT count(*) Ethernet FROM ( "
	// + "SELECT distinct *  FROM ACCOUNT_HIERARCHY_CORP_TBL " +
	// "START WITH account_no = ? CONNECT BY prior ID = PARENT_ID " + ") subQ2 "
	// +
	// "WHERE subQ2.Account_type  = 'Service' AND subQ2.PACKAGE_NAME IS NOT NULL AND subQ2.account_status = 'Active' "
	// + "START WITH account_type = 'Billing' "
	// +
	// "AND Product_type  = 'Ethernet' CONNECT BY prior ID = PARENT_ID order by subQ2.customer_id), "
	// + "(SELECT count(*) DIA FROM ( "
	// + "SELECT distinct *  FROM ACCOUNT_HIERARCHY_CORP_TBL " +
	// "START WITH account_no = ? CONNECT BY prior ID = PARENT_ID " + ") subQ3 "
	// +
	// "WHERE subQ3.Account_type  = 'Service' AND subQ3.PACKAGE_NAME IS NOT NULL AND subQ3.account_status = 'Active' "
	// + "START WITH account_type = 'Billing' "
	// +
	// "AND Product_type  = 'DIA' CONNECT BY prior ID = PARENT_ID order by subQ3.customer_id)";

	private static final String iceCubeQuery = "SELECT ICE_CUBE_ELIGIBILITY FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_STATUS='Active' AND ACCOUNT_NO = (SELECT DISTINCT ACCOUNT_NO FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE PARENT_ID IS NULL START WITH ACCOUNT_NO = ? CONNECT BY PRIOR  PARENT_ID = ID)";

	private static PersonalizationDao instance = new PersonalizationDao();

	protected PersonalizationDao()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static PersonalizationDao getInstance()
	{
		return instance;
	}

	// public PersonalizationVo getPersonalizedVo(String billingAccountNumber)
	// {
	// System.out.println(" >>>>>>> Executing Corporate Personalization Query for MBA: "
	// + billingAccountNumber + "<<<<<<<<<");
	// System.out.println(dataConnectivityQuery);
	// List<PersonalizationVo> personalizationVos = query(dataConnectivityQuery,
	// billingAccountNumber, billingAccountNumber, billingAccountNumber);
	// if (CollectionUtils.isNotEmpty(personalizationVos))
	// {
	// return personalizationVos.get(0);
	// }
	// else
	// {
	// return new PersonalizationVo();
	// }
	// }

	public boolean isIceCubeEligibilty(String masterBillingId)
	{
		boolean result = false;
		List<PersonalizationVo> personalizationVos = query(iceCubeQuery, masterBillingId);
		if (CollectionUtils.isNotEmpty(personalizationVos))
		{
			PersonalizationVo personalizationVo = personalizationVos.get(0);
			if (StringUtils.equalsIgnoreCase(personalizationVo.getIceCubeEligible(), "Y"))
			{
				result = true;
			}
		}
		return result;
	}

	@Override
	protected PersonalizationVo mapDTO(ResultSet rs) throws SQLException
	{
		PersonalizationVo personalizationVo = new PersonalizationVo();
		// personalizationVo.setIpVpn(rs.getInt(IPVPN));
		// personalizationVo.setEthernet(rs.getInt(ETHERNET));
		// personalizationVo.setDia(rs.getInt(DIA));
		personalizationVo.setIceCubeEligible(rs.getString(ICE_CUBE_ELIGIBILITY));
		return personalizationVo;
	}
}