package sa.com.mobily.eportal.core.service;

import sa.com.mobily.eportal.core.api.ServiceException;

public interface ResponseHandler<T> {
	T handleResponse(String response) throws ServiceException;
}
