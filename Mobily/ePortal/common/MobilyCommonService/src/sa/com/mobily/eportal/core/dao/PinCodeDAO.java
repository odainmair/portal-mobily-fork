//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.core.api.PinCode;
import sa.com.mobily.eportal.core.api.PinCodeCaller;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.core.service.InvalidParametersException;

/**
 * @author Hossam Omar
 * 
 */
public class PinCodeDAO extends AbstractDBDAO<PinCode>
{
	/***
	 * Singleton instance of the class
	 */
	private static PinCodeDAO instance = new PinCodeDAO();
    private static final String INSERT = 
	    "INSERT INTO SR_ACTIVATION_TBL(MSISDN,PROJECT_ID,ACTIVATION_CODE,REQ_DATE) " + 
	    " VALUES(?,?,?,CURRENT_TIMESTAMP)";
    private static final String DELETE = 
	    "DELETE FROM SR_ACTIVATION_TBL" +
	    " WHERE MSISDN = ? AND PROJECT_ID = ?";
    private static final String GET = 
	    "SELECT ACTIVATION_CODE, PROJECT_ID" +
	    " FROM SR_ACTIVATION_TBL " +
	    " WHERE MSISDN = ? AND PROJECT_ID = ? ";
    private static final String UPDATE = 
	    "UPDATE SR_ACTIVATION_TBL" +
	    " SET ACTIVATION_CODE = ?" +
	    ", REQ_DATE=CURRENT_TIMESTAMP" +
	    " WHERE MSISDN = ? AND PROJECT_ID = ? ";
    private static final String MERGE = 
	    "MERGE INTO SR_ACTIVATION_TBL OLD_PIN" + 
		    " USING (SELECT ? MSISDN, ? PROJECT_ID, ? ACTIVATION_CODE FROM DUAL) NEW_PIN" + 
		    	" ON (OLD_PIN.MSISDN = NEW_PIN.MSISDN AND OLD_PIN.PROJECT_ID = NEW_PIN.PROJECT_ID)" + 
		    " WHEN MATCHED THEN" + 
		    	" UPDATE SET OLD_PIN.ACTIVATION_CODE = NEW_PIN.ACTIVATION_CODE" + 
		    " WHEN NOT MATCHED THEN" + 
		    	" INSERT (OLD_PIN.MSISDN, OLD_PIN.PROJECT_ID, OLD_PIN.ACTIVATION_CODE, OLD_PIN.REQ_DATE)" + 
		    	" VALUES (NEW_PIN.MSISDN, NEW_PIN.PROJECT_ID, NEW_PIN.ACTIVATION_CODE, CURRENT_TIMESTAMP)";

	protected PinCodeDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static PinCodeDAO getInstance()
	{
		return instance;
	}

	/***
	 * Inserts the new pin code to the DB
	 * 
	 * @param MSISDN
	 *            user MSISDN
	 * @param pinCode
	 *            the new pinCode object
	 * @return true when the the operation succeeds, false otherwise
	 */
	public boolean insertPinCode(String MSISDN, PinCode pinCode)
	{
		int insertCount = update(INSERT, MSISDN, getProjectId(pinCode.getPinCodeCaller()), pinCode.getCode());
		return insertCount > 0;
	}

	/***
	 * Inserts the new pin code if there's existing record for the passed MSISDN
	 * and pin code caller<br/>
	 * If there's an existing record the method updates the pin code value of it
	 * 
	 * @param MSISDN
	 *            user MSISDN
	 * @param pinCode
	 *            the new/updated pinCode object
	 * @return true when the the operation succeeds, false otherwise
	 */
	public boolean insertOrUpdatePinCode(String MSISDN, PinCode pinCode)
	{
		int insertCount = update(MERGE, MSISDN, getProjectId(pinCode.getPinCodeCaller()), pinCode.getCode());
		return insertCount > 0;
	}

	/***
	 * Updates an existing pin code (matched via MSISDN and projectId)
	 * 
	 * @param MSISDN
	 *            user MSISDN
	 * @param pinCode
	 *            the updated pinCode
	 * @return true when the the operation succeeds, false otherwise
	 */
	public boolean updatePinCode(String MSISDN, PinCode pinCode)
	{
		int insertCount = update(UPDATE, pinCode.getCode(), MSISDN, getProjectId(pinCode.getPinCodeCaller()));
		return insertCount > 0;
	}

	/***
	 * Deletes an existing pin code (matched via MSISDN and projectId)
	 * 
	 * @param MSISDN
	 *            user MSISDN
	 * @param projectId
	 *            the pin code project ID
	 * @return true when records could be matched and deleted, false otherwise
	 */
	public boolean deletePinCode(String MSISDN, ProjectID projectId)
	{
		int insertCount = update(DELETE, MSISDN, projectId.getValue());
		return insertCount > 0;
	}

	public PinCode getPinCode(String MSISDN, ProjectID projectId)
	{
		List<PinCode> lstPinCode = query(GET, MSISDN, projectId.getValue());
		if (lstPinCode != null && !lstPinCode.isEmpty())
		{
			return lstPinCode.get(0);
		}

		return null;
	}

	@Override
	protected PinCode mapDTO(ResultSet rs) throws SQLException
	{
		PinCode pinCode = new PinCode();
		pinCode.setCode(rs.getString("ACTIVATION_CODE"));
		pinCode.setPinCodeCaller(getPinCodeCaller(rs.getInt("PROJECT_ID")));
		return pinCode;
	}

	public static int getProjectId(PinCodeCaller pinCodeCaller)
	{
		if (pinCodeCaller == PinCodeCaller.FlyWithNeqatyEtihad)
		{
			return ProjectID.LOYALTY_ETIHAD.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.FlyWithNeqatyGulfAir)
		{
			return ProjectID.LOYALTY_GULFAIR.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.FlyWithNeqatyRj)
		{
			return ProjectID.LOYALTY_ROYALPLUS.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.Souq)
		{
			return ProjectID.SOUQ.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.Neqatona)
		{
			return ProjectID.NEQATONA.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.PriorityPass)
		{
			return ProjectID.PRIORITY_PASS.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.Cobone)
		{
			return ProjectID.LOYALTY_COBONE.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.NeqatyAuthGo)
		{
			return ProjectID.GIFTS_BUNDLE.getValue();
		}
		else if (pinCodeCaller == PinCodeCaller.FlyWithNeqatyFlynas)
		{
			return ProjectID.LOYALTY_FLYNAS.getValue();
		}else if (pinCodeCaller == PinCodeCaller.NeqatyShukran)
		{
			return ProjectID.NeqatyShukran.getValue();
		}
		
		
		throw new InvalidParametersException("PinCodeCaller [" + pinCodeCaller + "] is not mapped to an existing ProjectID");
	}

	private PinCodeCaller getPinCodeCaller(int projectId)
	{
		if (projectId == ProjectID.LOYALTY_ETIHAD.getValue())
		{
			return PinCodeCaller.FlyWithNeqatyEtihad;
		}
		else if (projectId == ProjectID.LOYALTY_GULFAIR.getValue())
		{
			return PinCodeCaller.FlyWithNeqatyGulfAir;
		}
		else if (projectId == ProjectID.LOYALTY_ROYALPLUS.getValue())
		{
			return PinCodeCaller.FlyWithNeqatyRj;
		}
		else if (projectId == ProjectID.SOUQ.getValue())
		{
			return PinCodeCaller.Souq;
		}
		else if (projectId == ProjectID.PRIORITY_PASS.getValue())
		{
			return PinCodeCaller.PriorityPass;
		}
		else if (projectId == ProjectID.NEQATONA.getValue())
		{
			return PinCodeCaller.Neqatona;
		}
		else if (projectId == ProjectID.LOYALTY_COBONE.getValue())
		{
			return PinCodeCaller.Cobone;
		}
		else if (projectId == ProjectID.GIFTS_BUNDLE.getValue())
		{
			return PinCodeCaller.NeqatyAuthGo;
		}
		else if (projectId == ProjectID.LOYALTY_FLYNAS.getValue())
		{
			return PinCodeCaller.FlyWithNeqatyFlynas;
		}
		else if (projectId == ProjectID.NeqatyShukran.getValue())
		{
			return PinCodeCaller.NeqatyShukran;
		}

		throw new InvalidParametersException("ProjectID [" + projectId + "] is not mapped to an existing ProjectID");
	}

	public static void main(String[] args) throws ServiceException
	{
		PinCodeDAO pinCodeDAO = PinCodeDAO.getInstance();

		PinCode pinCode = new PinCode();
		pinCode.setCode("1111");
		pinCode.setPinCodeCaller(PinCodeCaller.PriorityPass);

		pinCodeDAO.insertOrUpdatePinCode("966565885281", pinCode);
		// pinCode = pinCodeDAO.getPinCode("456", ProjectID.LOYALTY_ETIHAD);
		// System.out.println(pinCode.getCode() + ", " +
		// pinCode.getPinCodeCaller());
		// pinCodeDAO.deletePinCode("456", 17);
	}

}
