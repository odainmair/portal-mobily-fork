/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EBillReplyVO {

	
	private EBillReplyHeaderVO Header = null;
	
	private String ErrorMsg = null;
	private String ErrorCode = null;
	private String LineNumber = null;
	private String InvoiceMechanism = null;
	private String Email = null;
	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return Email;
	}
	/**
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		Email = email;
	}
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return ErrorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	/**
	 * @return Returns the errorMsg.
	 */
	public String getErrorMsg() {
		return ErrorMsg;
	}
	/**
	 * @param errorMsg The errorMsg to set.
	 */
	public void setErrorMsg(String errorMsg) {
		ErrorMsg = errorMsg;
	}
	/**
	 * @return Returns the header.
	 */
	public EBillReplyHeaderVO getHeader() {
		return Header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(EBillReplyHeaderVO header) {
		Header = header;
	}
	/**
	 * @return Returns the invoiceMechanism.
	 */
	public String getInvoiceMechanism() {
		return InvoiceMechanism;
	}
	/**
	 * @param invoiceMechanism The invoiceMechanism to set.
	 */
	public void setInvoiceMechanism(String invoiceMechanism) {
		InvoiceMechanism = invoiceMechanism;
	}
	/**
	 * @return Returns the lineNumber.
	 */
	public String getLineNumber() {
		return LineNumber;
	}
	/**
	 * @param lineNumber The lineNumber to set.
	 */
	public void setLineNumber(String lineNumber) {
		LineNumber = lineNumber;
	}
}
