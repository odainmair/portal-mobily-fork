
package sa.com.mobily.eportal.common.service.pincode.activation.model;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;



public class PinCodeActivation  extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private String msisdn;
	
	private String activationCode;
	
	private String projectId;
	
	private Timestamp requestDate;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public Timestamp getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Timestamp requestDate) {
		this.requestDate = requestDate;
	}
	
    @Override
    public String toString() {
        return "ActivationVO ["
                + "msisdn = " + msisdn
                + ", activationCode = " + activationCode
                + ", projectId = " + projectId
                + ", requestDate = " + requestDate
                + "]";
    }
	
}
