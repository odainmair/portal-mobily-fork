package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.sql.Timestamp;
import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class NeqatyUserAccountVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private Long userAcctId;
	private Long subscriptionId;
	
	private String email;
	private Integer roleId;
	private Timestamp createdDate;
	private Timestamp lastLoginTime;
	private String userName;
	private String status;
	private String partnerNameEn;
	private String partnerNameAr;
	private String msisdn;

	
	public Long getUserAcctId()
	{
		return userAcctId;
	}
	public void setUserAcctId(Long userAcctId)
	{
		this.userAcctId = userAcctId;
	}
	public Long getSubscriptionId()
	{
		return subscriptionId;
	}
	public void setSubscriptionId(Long subscriptionId)
	{
		this.subscriptionId = subscriptionId;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public Integer getRoleId()
	{
		return roleId;
	}
	public void setRoleId(Integer roleId)
	{
		this.roleId = roleId;
	}
	public Date getCreatedDate()
	{
		return createdDate;
	}

	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getPartnerNameEn()
	{
		return partnerNameEn;
	}
	public void setPartnerNameEn(String partnerNameEn)
	{
		this.partnerNameEn = partnerNameEn;
	}
	public String getPartnerNameAr()
	{
		return partnerNameAr;
	}
	public void setPartnerNameAr(String partnerNameAr)
	{
		this.partnerNameAr = partnerNameAr;
	}
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public Timestamp getLastLoginTime()
	{
		return lastLoginTime;
	}
	public void setLastLoginTime(Timestamp lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public void setCreatedDate(Timestamp createdDate)
	{
		this.createdDate = createdDate;
	}

	

}
