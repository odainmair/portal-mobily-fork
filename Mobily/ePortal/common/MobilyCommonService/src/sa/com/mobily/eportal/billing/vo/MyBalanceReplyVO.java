package sa.com.mobily.eportal.billing.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MyBalanceReplyVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4380760444090592614L;
	private BalanceInquiryReplyVO balanceInquiryReply;
	private FreeBalancesReplyVO freeBalancesReply;
	private FavoriteNumberReplyVO favoriteNumberReply;
	public BalanceInquiryReplyVO getBalanceInquiryReply()
	{
		return balanceInquiryReply;
	}
	public void setBalanceInquiryReply(BalanceInquiryReplyVO balanceInquiryReply)
	{
		this.balanceInquiryReply = balanceInquiryReply;
	}
	public FreeBalancesReplyVO getFreeBalancesReply()
	{
		return freeBalancesReply;
	}
	public void setFreeBalancesReply(FreeBalancesReplyVO freeBalancesReply)
	{
		this.freeBalancesReply = freeBalancesReply;
	}
	public FavoriteNumberReplyVO getFavoriteNumberReply()
	{
		return favoriteNumberReply;
	}
	public void setFavoriteNumberReply(FavoriteNumberReplyVO favoriteNumberReply)
	{
		this.favoriteNumberReply = favoriteNumberReply;
	}
	
	
}
