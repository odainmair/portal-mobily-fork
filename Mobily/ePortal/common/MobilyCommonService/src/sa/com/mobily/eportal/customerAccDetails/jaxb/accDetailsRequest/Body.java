//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.11.27 at 05:34:07 PM IST 
//


package sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsRequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}GetRelatedAccounts_Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getRelatedAccountsInput"
})
@XmlRootElement(name = "Body")
public class Body {

    @XmlElement(name = "GetRelatedAccounts_Input", namespace = "http://siebel.com/CustomUI", required = true)
    protected GetRelatedAccountsInput getRelatedAccountsInput;

    /**
     * Gets the value of the getRelatedAccountsInput property.
     * 
     * @return
     *     possible object is
     *     {@link GetRelatedAccountsInput }
     *     
     */
    public GetRelatedAccountsInput getGetRelatedAccountsInput() {
        return getRelatedAccountsInput;
    }

    /**
     * Sets the value of the getRelatedAccountsInput property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetRelatedAccountsInput }
     *     
     */
    public void setGetRelatedAccountsInput(GetRelatedAccountsInput value) {
        this.getRelatedAccountsInput = value;
    }

}
