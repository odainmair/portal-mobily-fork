package sa.com.mobily.eportal.customerAccDetails.vo;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CustomerDetailsVO extends BaseVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idType;
	private String idNumber;
	private String createdDate;
	private String updatedDate;
	private Timestamp createdTimestamp;
	private byte[] customerDetailsData;
	private String accountNumber;
	public String getIdType()
	{
		return idType;
	}
	public void setIdType(String idType)
	{
		this.idType = idType;
	}
	public String getIdNumber()
	{
		return idNumber;
	}
	public void setIdNumber(String idNumber)
	{
		this.idNumber = idNumber;
	}
	public String getCreatedDate()
	{
		return createdDate;
	}
	public void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}
	public String getUpdatedDate()
	{
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate)
	{
		this.updatedDate = updatedDate;
	}
	public Timestamp getCreatedTimestamp()
	{
		return createdTimestamp;
	}
	public void setCreatedTimestamp(Timestamp createdTimestamp)
	{
		this.createdTimestamp = createdTimestamp;
	}
	public byte[] getCustomerDetailsData()
	{
		return customerDetailsData;
	}
	public void setCustomerDetailsData(byte[] customerDetailsData)
	{
		this.customerDetailsData = customerDetailsData;
	}
	public String getAccountNumber()
	{
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
}
