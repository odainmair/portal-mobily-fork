package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.valueobjects.RefactoredSessionVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class SessionVODAO extends AbstractDBDAO<RefactoredSessionVO>
{

	private static SessionVODAO INSTANCE = new SessionVODAO();

	private static final String COL_SESSIONID = "SESSIONID";

	private static final String COL_USERNAME = "USERNAME";

	private static final String COL_SUBSCRIPTIONID = "SUBSCRIPTIONID";

	private static final String COL_CREATED_DATE = "CREATED_DATE";

	private static final String SV_FIND_USER_BY_USERNAME = "SELECT SESSIONID,USERNAME,SUBSCRIPTIONID FROM SR_USER_SESSIONVO_TBL WHERE USERNAME=?";

	private static final String SV_FIND_USER_BY_SESSION_ID = "SELECT SESSIONID,USERNAME,SUBSCRIPTIONID FROM SR_USER_SESSIONVO_TBL WHERE SESSIONID=?";

	private static final String SV_FIND_USER_BY_SESSION_ID_AND_USER_NAME = "SELECT SESSIONID,USERNAME,SUBSCRIPTIONID FROM SR_USER_SESSIONVO_TBL WHERE SESSIONID=? AND USERNAME=?";

	private static final String SV_FIND_USER_BY_SUBSCRIPTION_ID = "SELECT SESSIONID,USERNAME,SUBSCRIPTIONID FROM SR_USER_SESSIONVO_TBL WHERE SUBSCRIPTIONID=?";

	private static final String SV_INSERT_SESSION_VO = "INSERT INTO SR_USER_SESSIONVO_TBL (SESSIONID,USERNAME,SUBSCRIPTIONID,CREATED_DATE) values(?,?,?,sysdate)";
	
	private static final String SV_UPDATE_SESSION_VO = "UPDATE SR_USER_SESSIONVO_TBL SET SUBSCRIPTIONID = ? WHERE SESSIONID = ?";
	
	private static final String SV_DELETE_SESSION_VO = "DELETE from SR_USER_SESSIONVO_TBL WHERE SESSIONID=?";
	
	protected SessionVODAO()
	{
		super(DataSources.DEFAULT);
	}

	public static SessionVODAO getInstance()
	{
		return INSTANCE;
	}

	public List<RefactoredSessionVO> getUserSessionVOByUserName(String userName)
	{
		return query(SV_FIND_USER_BY_USERNAME, userName);
	}

	public List<RefactoredSessionVO> getUserSessionVOBySessionId(String sessionId)
	{
		return query(SV_FIND_USER_BY_SESSION_ID, sessionId);
	}

	public List<RefactoredSessionVO> getUserSessionVOBySubScriptionId(String subscriptionId)
	{
		return query(SV_FIND_USER_BY_SUBSCRIPTION_ID, subscriptionId);
	}

	public List<RefactoredSessionVO> getUserSessionVOBySessionIdAndUserName(RefactoredSessionVO refactoredSessionVO)
	{
		return query(SV_FIND_USER_BY_SESSION_ID_AND_USER_NAME, refactoredSessionVO.getSessionId(), refactoredSessionVO.getUserName());
	}

	public int insertUserSessionVO(RefactoredSessionVO userSessionVO)
	{
		return update(SV_INSERT_SESSION_VO, userSessionVO.getSessionId(), userSessionVO.getUserName(), userSessionVO.getSubscriptionId());
	}
	
	public int updateUserSessionVO(RefactoredSessionVO userSessionVO)
	{
		return update(SV_UPDATE_SESSION_VO, userSessionVO.getSubscriptionId() , userSessionVO.getSessionId());
	}
	
	public int deleteUserSessionVO(String sessionId)
	{
		return update(SV_DELETE_SESSION_VO, sessionId);
	}

	@Override
	protected RefactoredSessionVO mapDTO(ResultSet rs) throws SQLException
	{
		RefactoredSessionVO userSessionVO = new RefactoredSessionVO();
		userSessionVO.setSessionId(rs.getString(COL_SESSIONID));
		userSessionVO.setSubscriptionId(rs.getLong(COL_SUBSCRIPTIONID));
		userSessionVO.setUserName(rs.getString(COL_USERNAME));
		// userSessionVO.setCreatedDate(rs.getTimestamp(COL_CREATED_DATE));
		return userSessionVO;
	}

}
