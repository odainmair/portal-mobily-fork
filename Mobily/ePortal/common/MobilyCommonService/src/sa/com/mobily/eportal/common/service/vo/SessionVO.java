/*
 * Created on Apr 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SessionVO implements Serializable,  Cloneable  
{
    private String rbtTones;
    private byte   customerType;
    private String activeMSISDN;
    private String originalMsisdn ;
    boolean isWebProfileUpdated = true;
    private int customerStatus ;
    private String iqamaId ="";
    private boolean isIUC = false;
    
    private String customerSegment = null;
    
	public String getCustomerSegment() {
		return customerSegment;
	}
	public void setCustomerSegment(String customerSegment) {
		this.customerSegment = customerSegment;
	}
	public int getCustomerStatus() {
		return customerStatus;
	}
	public void setCustomerStatus(int customerStatus) {
		this.customerStatus = customerStatus;
	}
	public boolean isWebProfileUpdated() {
		return isWebProfileUpdated;
	}
	public void setWebProfileUpdated(boolean isWebProfileUpdated) {
		this.isWebProfileUpdated = isWebProfileUpdated;
	}
	private String packageID;
    //	For deactivation of lines
    private Date   creationDate;  
    private boolean isBlockedCustomer;//to indicate if the postpaid customer is blocked from credit control system.
    /*	One out of four types:
     * 1) CONSUMER                     							 normal line
     * 2) CORPORATE_LINE               							 corporate line
     * 3) CORPORATE_AUTHORIZED_PERSON  							 corporate authorized person
     * 4) CORPORATE_AUTHORUIZED_PERSON_CHANGE_IDENTITY corporate authorized person selected line to manage
    */
    private int    lineType = 0;
    /*	One out of five types:
     * 1) NO_INTERNET_BUNDLE
     * 2) INTERNET_BUNDLE_1G
     * 3) INTERNET_BUNDLE_5G
     * 4) INTERNET_BUNDLE_UNLIMITED
     * 5) MOBILY_CONNECT
    */
    private int    skyType;
    //	for corporate authorized person
    private String billingAccountId;
    /* Email status will be used only for grace and scheduled to be created
     * This will be used at forwarding to Google through SSO portlet only
     */
    private int skyEmailStatus;
    private Date schesuledTime; 
    //	Blocking of corporate lines according to authorized person action
    private boolean isBlockedCorporate;
    //	Blocking of customer either corporate line or consumer, according to site administrator
    private boolean isBlockedUser = false;
    //	Customer is not active if he is still in the registration process. Activation is done through a link sent to his email
    private boolean isActiveCustomer = true;
    private String portalUserName;
    private ArrayList listOfServices = null;
    
    private String ebillStatus;
    //Added by Nagendra for Manage Complaints Project
    	//From Siebel
    	String SiebelCustomerType;  //Consumer,Business
    	String SiebelPackageName;   
    	String SiebelPricingPlan;

    //added by Suresh V to redirect connect users to mobily site insteadof Mobily connect : 1 Feb 2009 
    private boolean isMobilyConnectUser = false; // for connect users
    private boolean isDataSIMUser = false; // for Data SIM users
    
    //added by Suresh V for corporate revamp
    private Object billingAccountVO = null; 
    
    
    //added by Suresh V on June 29 2010
    private String rootBillingAccountNumber = null; // Root or department billing account number of corporate
    private String companyRowId = null; // row id of corporate
    
  
    
    public String getOriginalMsisdn() {
		return originalMsisdn;
	}
	public void setOriginalMsisdn(String originalMsisdn) {
		this.originalMsisdn = originalMsisdn;
	}
	
    /**
	 * @return the rootBillingAccountNumber
	 */
	public String getRootBillingAccountNumber() {
		return rootBillingAccountNumber;
	}
	/**
	 * @param rootBillingAccountNumber the rootBillingAccountNumber to set
	 */
	public void setRootBillingAccountNumber(String rootBillingAccountNumber) {
		this.rootBillingAccountNumber = rootBillingAccountNumber;
	}
	/**
	 * @return the companyRowId
	 */
	public String getCompanyRowId() {
		return companyRowId;
	}
	/**
	 * @param companyRowId the companyRowId to set
	 */
	public void setCompanyRowId(String companyRowId) {
		this.companyRowId = companyRowId;
	}
	
	public Object getBillingAccountVO() {
		return billingAccountVO;
	}
	public void setBillingAccountVO(Object billingAccountVO) {
		this.billingAccountVO = billingAccountVO;
	}
	
	public boolean isMobilyConnectUser() {
        return isMobilyConnectUser;
    }
    public void setMobilyConnectUser(boolean isMobilyConnectUser) {
        this.isMobilyConnectUser = isMobilyConnectUser;
    }
    
    public boolean isDataSIMUser() {
        return isDataSIMUser;
    }
    public void setDataSIMUser(boolean isDataSIMUser) {
        this.isDataSIMUser = isDataSIMUser;
    }
    public int getLineType()
    {
        return lineType;
    }
    public void setLineType(int lineType)
    {
        this.lineType = lineType;
    }
    public byte getCustomerType()
    {
        return customerType;
    }
    public void setCustomerType(byte customerType)
    {
        this.customerType = customerType;
    }
	/**
	 * @return Returns the activeMSISDN.
	 */
	public String getActiveMSISDN() 
	{
		return activeMSISDN;
	}
	/**
	 * @param activeMSISDN The activeMSISDN to set.
	 */
	public void setActiveMSISDN(String activeMSISDN) 
	{
		this.activeMSISDN = activeMSISDN;
	}
	/**
	 * @return Returns the packageID.
	 */
	public String getPackageID() 
	{
		return packageID;
	}
	/**
	 * @param packageID The packageID to set.
	 */
	public void setPackageID(String packageID) 
	{
		this.packageID = packageID;
	}
	
	
	/**
	 * @return Returns the isBlockedCustomer.
	 */
	public boolean isBlockedCustomer() {
		return isBlockedCustomer;
	}
	/**
	 * @param isBlockedCustomer The isBlockedCustomer to set.
	 */
	public void setBlockedCustomer(boolean isBlockedCustomer) {
		this.isBlockedCustomer = isBlockedCustomer;
	}
	public String toString(){
		return("SessionVO:"+" customerType="+customerType+" ,activeMSISDN"+activeMSISDN+" ,packageID="+packageID+" ,isBlockedCustomer="+isBlockedCustomer+" ,portalUserName="+portalUserName);
	}
    public Date getCreationDate()
    {
        return creationDate;
    }
    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }
    public String getBillingAccountId()
    {
        return billingAccountId;
    }
    public void setBillingAccountId(String billingAccountId)
    {
        this.billingAccountId = billingAccountId;
    }
    public boolean isBlockedCorporate()
    {
        return isBlockedCorporate;
    }
    public void setBlockedCorporate(boolean isBlockedCorporate)
    {
        this.isBlockedCorporate = isBlockedCorporate;
    }
    
	/**
	 * @return Returns the portalUserName.
	 */
	public String getPortalUserName() {
		return portalUserName;
	}
	/**
	 * @param portalUserName The portalUserName to set.
	 */
	public void setPortalUserName(String portalUserName) {
		this.portalUserName = portalUserName;
	}
    public int getSkyType()
    {
        return skyType;
    }
    public void setSkyType(int skyType)
    {
        this.skyType = skyType;
    }
    public boolean isBlockedUser()
    {
        return isBlockedUser;
    }
    public void setBlockedUser(boolean isBlockedUser)
    {
        this.isBlockedUser = isBlockedUser;
    }
    public boolean isActiveCustomer()
    {
        return isActiveCustomer;
    }
    public void setActiveCustomer(boolean isActiveCustomer)
    {
        this.isActiveCustomer = isActiveCustomer;
    }
    public Date getSchesuledTime()
    {
        return schesuledTime;
    }
    public void setSchesuledTime(Date schesuledTime)
    {
        this.schesuledTime = schesuledTime;
    }
    public int getSkyEmailStatus()
    {
        return skyEmailStatus;
    }
    public void setSkyEmailStatus(int skyEmailStatus)
    {
        this.skyEmailStatus = skyEmailStatus;
    }
    /**
     * @return Returns the rbtTones.
     */
    public String getRbtTones() {
        return rbtTones;
    }
    /**
     * @param rbtTones The rbtTones to set.
     */
    public void setRbtTones(String rbtTones) {
        this.rbtTones = rbtTones;
    }
	/**
	 * @return Returns the listOfServices.
	 */
	public ArrayList getListOfServices() {
		return listOfServices;
	}
	/**
	 * @param listOfServices The listOfServices to set.
	 */
	public void setListOfServices(ArrayList listOfServices) {
		this.listOfServices = listOfServices;
	}
	
	/**
	 * get status of certain supllementary service from the session
	 * @param serviceName
	 * @return
	 */
	public int getServiceStatusByServiceName(String serviceName){
	    int status = 0;
	    	if(listOfServices!= null){
	    	     ServiceVO serviceVO = null;
	    	     for(int i=0 ; i< listOfServices.size(); i++){
	    	         serviceVO = (ServiceVO)listOfServices.get(i);
	    	         if(serviceVO.getServiceName().equalsIgnoreCase(serviceName)){
	    	             status = serviceVO.getStatus();
	    	             break;
	    	         }
	    	     }
	    	}else{
	    	    status = -1;
	    	}
	    return status;
	
	}
	
	/**
	 * saving new supplementary servicve status into session
	 * @param serviceName
	 * @param status
	 */
	public void setServiceStatusIntoSession(String serviceName , int status){
	    ServiceVO serviceVO = new ServiceVO();
	    serviceVO.setServiceName(serviceName);
	    serviceVO.setStatus(status);
	    
	    if(listOfServices == null){
	        listOfServices = new ArrayList();   
	    }
	    listOfServices.add(serviceVO);
	}
	
	
    /**
     * @return Returns the siebelCustomerType.
     */
    public String getSiebelCustomerType() {
        return SiebelCustomerType;
    }
    /**
     * @param siebelCustomerType The siebelCustomerType to set.
     */
    public void setSiebelCustomerType(String siebelCustomerType) {
        SiebelCustomerType = siebelCustomerType;
    }
        /**
         * @return Returns the siebelPackageName.
         */
        public String getSiebelPackageName() {
            return SiebelPackageName;
        }
        /**
         * @param siebelPackageName The siebelPackageName to set.
         */
        public void setSiebelPackageName(String siebelPackageName) {
            SiebelPackageName = siebelPackageName;
        }
        /**
         * @return Returns the siebelPricingPlan.
         */
        public String getSiebelPricingPlan() {
            return SiebelPricingPlan;
        }
        /**
         * @param siebelPricingPlan The siebelPricingPlan to set.
         */
        public void setSiebelPricingPlan(String siebelPricingPlan) {
            SiebelPricingPlan = siebelPricingPlan;
        }
	/**
	 * @return Returns the ebillStatus.
	 */
	public String getEbillStatus() {
		return ebillStatus;
	}
	/**
	 * @param ebillStatus The ebillStatus to set.
	 */
	public void setEbillStatus(String ebillStatus) {
		this.ebillStatus = ebillStatus;
	}
	public String getIqamaId() {
		return iqamaId;
	}
	public void setIqamaId(String iqamaId) {
		this.iqamaId = iqamaId;
	}
	public boolean isIUC() {
		return isIUC;
	}
	public void setIUC(boolean isIUC) {
		this.isIUC = isIUC;
	}
}
