package sa.com.mobily.eportal.common.service.vo;




/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MCRDetailsVO extends BaseVO {

	
	protected String billAcctNumber;
	protected String serviceAcctNumber;
	protected String msisdn;
	protected String sim;
	protected String payType;
	protected String ipAddress;
	protected String ipType;
	protected String packageId;
	protected String apn;
	protected String web1wap1Status;
	protected String eportalAliasName;
	protected String eportalEmail;
	protected String packageNameSiebel;
	
	public String getBillAcctNumber() {
		return billAcctNumber;
	}
	public void setBillAcctNumber(String billAcctNumber) {
		this.billAcctNumber = billAcctNumber;
	}
	public String getServiceAcctNumber() {
		return serviceAcctNumber;
	}
	public void setServiceAcctNumber(String serviceAcctNumber) {
		this.serviceAcctNumber = serviceAcctNumber;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getSim() {
		return sim;
	}
	public void setSim(String sim) {
		this.sim = sim;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getIpType() {
		return ipType;
	}
	public void setIpType(String ipType) {
		this.ipType = ipType;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getApn() {
		return apn;
	}
	public void setApn(String apn) {
		this.apn = apn;
	}
	public String getWeb1wap1Status() {
		return web1wap1Status;
	}
	public void setWeb1wap1Status(String web1wap1Status) {
		this.web1wap1Status = web1wap1Status;
	}
	public String getEportalAliasName() {
		return eportalAliasName;
	}
	public void setEportalAliasName(String eportalAliasName) {
		this.eportalAliasName = eportalAliasName;
	}
	/**
	 * @return the eportalEmail
	 */
	public String getEportalEmail() {
		return eportalEmail;
	}
	/**
	 * @param eportalEmail the eportalEmail to set
	 */
	public void setEportalEmail(String eportalEmail) {
		this.eportalEmail = eportalEmail;
	}
	/**
	 * @return the packageNameSiebel
	 */
	public String getPackageNameSiebel() {
		return packageNameSiebel;
	}
	/**
	 * @param packageNameSiebel the packageNameSiebel to set
	 */
	public void setPackageNameSiebel(String packageNameSiebel) {
		this.packageNameSiebel = packageNameSiebel;
	}
}