/*
 * Created on Sep 14, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.Vector;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

//public class CreditCardPaymentReplyVO extends BaseVO {
public class CreditCardPaymentReplyVO {

    public CreditCardMessageHeaderVO header = new CreditCardMessageHeaderVO();
    
    public Vector creditCardsDetails = null;
    public String transactionID = null; 

	public String merchantID = null;
	public String merchantName = null;
	public String paymentDateTime = null;
	public String amount = null;
	private String msisdn ;
	private String requestorUserID ;
	private String requstorChannel ="";
	
	String errorCode = "";
	String errorMessage = "";
	private String totalBilledAmount = "";

	private String returnCode = null;
	
	
	public String getTotalBilledAmount() {
		return totalBilledAmount;
	}
	public void setTotalBilledAmount(String totalBilledAmount) {
		this.totalBilledAmount = totalBilledAmount;
	}
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the errorMessage.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage The errorMessage to set.
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return Returns the header.
	 */
	public CreditCardMessageHeaderVO getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(CreditCardMessageHeaderVO header) {
		this.header = header;
	}

	/**
	 * @return Returns the creditCardsDetails.
	 */
	public Vector getCreditCardsDetails() {
		return creditCardsDetails;
	}
	/**
	 * @param creditCardsDetails The creditCardsDetails to set.
	 */
	public void setCreditCardsDetails(Vector creditCardsDetails) {
		this.creditCardsDetails = creditCardsDetails;
	}
	/**
	 * @return Returns the transactionID.
	 */
	public String getTransactionID() {
		return transactionID;
	}
	/**
	 * @param transactionID The transactionID to set.
	 */
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	/**
	 * @return Returns the amount.
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount The amount to set.
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return Returns the bankName.
	 */
	public String getMerchantName() {
		return merchantName;
	}
	/**
	 * @param bankName The bankName to set.
	 */
	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}
	/**
	 * @return Returns the merchantID.
	 */
	public String getMerchantID() {
		return merchantID;
	}
	/**
	 * @param merchantID The merchantID to set.
	 */
	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}
	/**
	 * @return Returns the paymentDateTime.
	 */
	public String getPaymentDateTime() {
		return paymentDateTime;
	}
	/**
	 * @param paymentDateTime The paymentDateTime to set.
	 */
	public void setPaymentDateTime(String paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the requestorUserID.
	 */
	public String getRequestorUserID() {
		return requestorUserID;
	}
	/**
	 * @param requestorUserID The requestorUserID to set.
	 */
	public void setRequestorUserID(String requestorUserID) {
		this.requestorUserID = requestorUserID;
	}
	/**
	 * @return Returns the requstorChannel.
	 */
	public String getRequstorChannel() {
		return requstorChannel;
	}
	/**
	 * @param requstorChannel The requstorChannel to set.
	 */
	public void setRequstorChannel(String requstorChannel) {
		this.requstorChannel = requstorChannel;
	}
	public String getReturnCode()
	{
		return returnCode;
	}
	public void setReturnCode(String returnCode)
	{
		this.returnCode = returnCode;
	}
}
