package sa.com.mobily.eportal.common.service.vo;

/**
 * This bean contains information regarding currently processed page.
 * <p>
 * Indexes starts from zero
 * 
 * @author Omar Kalaldeh
 *
 */
public class PagingVO extends BaseVO {
	/* The index for the first item from the whole list on current page*/
	private Integer startIndex;

	/* The index for the last item from the whole list on current page*/
	private Integer endIndex;
	
	/* The size of current page */
	private Integer pageSize;
	
	/* Total number of all items from the whole list */
	private Integer totalItems;
	
	/* Total number of pages for the whole list, this number depends on current page size */
	private Integer totalPages;
	
	public PagingVO() {
	}
	
	public PagingVO(Integer startIndex, Integer endIndex, Integer totalItems) {
		setStartIndex(startIndex);
		setEndIndex(endIndex);
		setTotalItems(totalItems);
	}


	/**
	 * @return the startIndex
	 */
	public Integer getStartIndex() {
		return startIndex;
	}
	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
		compute();
	}
	/**
	 * @return the endIndex
	 */
	public Integer getEndIndex() {
		return endIndex;
	}
	/**
	 * @param endIndex the endIndex to set
	 */
	public void setEndIndex(Integer endIndex) {
		this.endIndex = endIndex;
		compute();
	}
	/**
	 * @return the totalItems
	 */
	public Integer getTotalItems() {
		return totalItems;
	}
	/**
	 * @param totalItems the totalItems to set
	 */
	public void setTotalItems(Integer totalItems) {
		this.totalItems = totalItems;
		compute();
	}
	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}
	/**
	 * @return the totalPages
	 */
	public Integer getTotalPages() {
		return totalPages;
	}
	
	/**
	 * Compute page size and total pages
	 */
	private void compute() {
		if(startIndex != null && endIndex != null) {
			pageSize = endIndex - startIndex + 1;
			if(totalItems != null) {
				if(totalItems==0)
					totalPages = 0;
				else
				totalPages = totalItems % pageSize;
			}
		}
		
	}
}
