package sa.com.mobily.eportal.google.captcha.util;



import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.google.captcha.v2.GoogleReCaptchaV2Impl;
import sa.com.mobily.eportal.google.captcha.v2.vo.CaptchaResponseVO;
import sa.com.mobily.eportal.google.captcha.v2.vo.GoogleReCaptchaV2VO;
import sa.com.mobily.eportal.recaptcha.ReCaptchaImpl;
import sa.com.mobily.eportal.recaptcha.ReCaptchaResponse;

public class GoogleCaptchaValidator {
	 public static boolean validateCaptcha(String challenge, String userResponse, String privateKey)
		{
			ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
			reCaptcha.setPrivateKey(privateKey);
			ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer("127.0.0.1", challenge, userResponse);

			if (reCaptchaResponse.isValid())
				return true;
			else
				return false;
		}
	 
	 
	 public static boolean validateGoogleRecaptchaV2(GoogleReCaptchaV2VO googleReCaptchaV2VO){
		
		 GoogleReCaptchaV2Impl googleReCaptchaV2 = GoogleReCaptchaV2Impl.getInstance();
		 googleReCaptchaV2VO.setSecretKey(GoogleCaptchaHelperUtil.getInstance().getReCaptchaV2SecretKey());
		 if(FormatterUtility.isEmpty(googleReCaptchaV2VO.getRemoteIp() ))
			 googleReCaptchaV2VO.setRemoteIp(GoogleCaptchaHelperUtil.getInstance().getRemoteIP());
		 
		 CaptchaResponseVO captchaResponseVO = googleReCaptchaV2.validateCaptcha(googleReCaptchaV2VO);
		 return captchaResponseVO!=null ? captchaResponseVO.isSuccess(): false;
		}
}
