package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.MobilyPackageCatalogLookUp;

public class MobilyPackageCatalogLookUpDAO extends AbstractDBDAO<MobilyPackageCatalogLookUp>
{
	private static MobilyPackageCatalogLookUpDAO instance = new MobilyPackageCatalogLookUpDAO();

	private static final String GET_ALL_PACKAGES = "SELECT * FROM MOBILY_PACKAGE_CATALOG_TBL WHERE PACKAGE_ID = ?";

	//private static final String GET_PACKAGES_BY_PACKAGE_GLOBAL_ID = "SELECT * FROM MOBILY_PACKAGE_CATALOG_TBL WHERE PACKAGE_GLOBAL_ID = ?";
	
	private static final String GET_PACKAGES_BY_PACKAGE_GLOBAL_ID = "SELECT * FROM (SELECT *  FROM MOBILY_PACKAGE_CATALOG_TBL WHERE PACKAGE_GLOBAL_ID = ?) WHERE rownum < 2";
	
	
	
	
	protected MobilyPackageCatalogLookUpDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static MobilyPackageCatalogLookUpDAO getInstance()
	{
		return instance;
	}

	
	public List<MobilyPackageCatalogLookUp> getPackagesByPackageGlobalId(String packageGlobalId)
	{
		List<MobilyPackageCatalogLookUp> lstLookUp = query(GET_PACKAGES_BY_PACKAGE_GLOBAL_ID,packageGlobalId);
		if (CollectionUtils.isNotEmpty(lstLookUp))
		{
			return lstLookUp;
		}
		return null;
	}
	
	public List<MobilyPackageCatalogLookUp> getUserPackages(String packageID)
	{
		List<MobilyPackageCatalogLookUp> lstLookUp = query(GET_ALL_PACKAGES,packageID);
		if (CollectionUtils.isNotEmpty(lstLookUp))
		{
			return lstLookUp;
		}
		return null;
	}

	
	
	
	@Override
	protected MobilyPackageCatalogLookUp mapDTO(ResultSet rs) throws SQLException
	{
		MobilyPackageCatalogLookUp mobilyPackageCatalogLookUp = new MobilyPackageCatalogLookUp();
		mobilyPackageCatalogLookUp.setCOMMERCIAL_PLAN_TYPE(rs.getString("COMMERCIAL_PLAN_TYPE"));
		mobilyPackageCatalogLookUp.setLINE_OF_BUSINESS(rs.getString("LINE_OF_BUSINESS"));
		mobilyPackageCatalogLookUp.setPACKAGE_ID(rs.getString("PACKAGE_ID"));
		mobilyPackageCatalogLookUp.setPACKAGE_NAME(rs.getString("PACKAGE_NAME"));
		mobilyPackageCatalogLookUp.setPACKAGE_NAME_AR(rs.getString("PACKAGE_NAME_AR"));
		mobilyPackageCatalogLookUp.setPACKAGE_NAME_EN(rs.getString("PACKAGE_NAME_EN"));
		mobilyPackageCatalogLookUp.setPLAN_TYPE(rs.getString("PLAN_TYPE"));
		mobilyPackageCatalogLookUp.setPRODUCT_FAMILY(rs.getString("PRODUCT_FAMILY"));
		mobilyPackageCatalogLookUp.setUPDATE_DATE(rs.getDate("UPDATE_DATE"));
		
		mobilyPackageCatalogLookUp.setPACKAGE_GLOBAL_ID(rs.getString("PACKAGE_GLOBAL_ID"));
		
		
		return mobilyPackageCatalogLookUp;
	}
}