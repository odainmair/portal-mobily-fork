/*
 * Created on Feb 23, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FavoriteNumberInquiryVO {

	private String msisdn = null;
	private String requestorUserId;
	private ArrayList nationalList =null;
	private ArrayList internationalList =null;
	private ArrayList countryFavList =null;
	private ArrayList mccFavList =null;
	
	private ArrayList mobilyList =null;
	private ArrayList landLineList =null;
	private ArrayList otherList =null;
	
	private String packageId;
	
	public void addtoInternational(String number){
		if(internationalList == null)
			internationalList = new ArrayList();
		internationalList.add(number);
	}
	
	public void addtoNational(String number){
		if(nationalList == null)
			nationalList = new ArrayList();
		nationalList.add(number);
		
	}

	public void addtoMobilyList(String number){
		if(mobilyList == null)
			mobilyList = new ArrayList();
		mobilyList.add(number);
		
	}

	public void addtoLandLineList(String number){
		if(landLineList == null)
			landLineList = new ArrayList();
		landLineList.add(number);
		
	}

	public void addtoOtherList(String number){
		if(otherList == null)
			otherList = new ArrayList();
		otherList.add(number);
		
	}

	public void addtoCountryFavList(String number){
		if(countryFavList == null)
			countryFavList = new ArrayList();
		countryFavList.add(number);
	}

	public void addtoMCCFavList(String number){
		if(mccFavList == null)
			mccFavList = new ArrayList();
		mccFavList.add(number);
	}

	
	
	/**
	 * @return Returns the internationalList.
	 */
	public ArrayList getInternationalList() {
		return internationalList;
	}
	/**
	 * @param internationalList The internationalList to set.
	 */
	public void setInternationalList(ArrayList internationalList) {
		this.internationalList = internationalList;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the nationalList.
	 */
	public ArrayList getNationalList() {
		return nationalList;
	}
	/**
	 * @param nationalList The nationalList to set.
	 */
	public void setNationalList(ArrayList nationalList) {
		this.nationalList = nationalList;
	}
	/**
	 * @return Returns the requestorUserId.
	 */
	public String getRequestorUserId() {
		return requestorUserId;
	}
	/**
	 * @param requestorUserId The requestorUserId to set.
	 */
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	/**
	 * @return Returns the landLineList.
	 */
	public ArrayList getLandLineList() {
		return landLineList;
	}
	/**
	 * @param landLineList The landLineList to set.
	 */
	public void setLandLineList(ArrayList landLineList) {
		this.landLineList = landLineList;
	}
	/**
	 * @return Returns the mobilyList.
	 */
	public ArrayList getMobilyList() {
		return mobilyList;
	}
	/**
	 * @param mobilyList The mobilyList to set.
	 */
	public void setMobilyList(ArrayList mobilyList) {
		this.mobilyList = mobilyList;
	}
	/**
	 * @return Returns the otherList.
	 */
	public ArrayList getOtherList() {
		return otherList;
	}
	/**
	 * @param otherList The otherList to set.
	 */
	public void setOtherList(ArrayList otherList) {
		this.otherList = otherList;
	}

	/**
	 * @return Returns the packageId.
	 */
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId The packageId to set.
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList getCountryFavList() {
		return countryFavList;
	}
	/**
	 * 
	 * @param countryFavList
	 */
	public void setCountryFavList(ArrayList countryFavList) {
		this.countryFavList = countryFavList;
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList getMccFavList() {
		return mccFavList;
	}
	/**
	 * 
	 * @param mccFavList
	 */
	public void setMccFavList(ArrayList mccFavList) {
		this.mccFavList = mccFavList;
	}
}