package sa.com.mobily.eportal.common.service.util.context;

import java.util.Date;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class LogEntryVO extends BaseVO
{
	private static final long serialVersionUID = 4519331133828784410L;

	private Level logLevel = null;

	private String message = "";

	private String className = "";

	private String methodName = "";

	private Throwable error;
	
	private Date createDate;

	public LogEntryVO(Level logLevel, String message, String className, String methodName, Throwable error)
	{
		this.logLevel = logLevel;
		this.message = message;
		this.className = className;
		this.methodName = methodName;
		this.error = error;
		this.createDate = new Date();
	}

	public Level getLogLevel()
	{
		return logLevel;
	}

	public void setLogLevel(Level logLevel)
	{
		this.logLevel = logLevel;
	}

	public String getClassName()
	{
		return className;
	}

	public void setClassName(String className)
	{
		this.className = className;
	}

	public String getMethodName()
	{
		return methodName;
	}

	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public Throwable getError()
	{
		return error;
	}

	public void setError(Throwable error)
	{
		this.error = error;
	}
	
	public Date getCreateDate()
	{
		return createDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.createDate = createDate;
	}

	@Override
	public String toString()
	{
		return logLevel + " " + className + " " + methodName + " " + message + " " + error;
	}
	
	
}