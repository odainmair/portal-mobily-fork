/**
 * 
 */
package sa.com.mobily.eportal.customerinfo.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author v.ravipati.mit
 *
 */

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}EmailContact"/>
 *         &lt;element ref="{}TelephoneContact"/>
 *        
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "emailContact",
    "telephoneContact"
    
})
@XmlRootElement(name = "ContactMedium")
public class ContactMedium{
	
		@XmlElement(name = "EmailContact", required = true)
	    protected EmailContact emailContact;
	    @XmlElement(name = "TelephoneContact", required = true)
	    protected TelephoneContact telephoneContact;
		public EmailContact getEmailContact()
		{
			return emailContact;
		}
		public void setEmailContact(EmailContact emailContact)
		{
			this.emailContact = emailContact;
		}
		public TelephoneContact getTelephoneContact()
		{
			return telephoneContact;
		}
		public void setTelephoneContact(TelephoneContact telephoneContact)
		{
			this.telephoneContact = telephoneContact;
		}

}
