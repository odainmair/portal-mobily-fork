package sa.com.mobily.eportal.common.service.util;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneDeviceInfoDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.IPhoneDeviceDetailsVO;

/**
 * @author n.gundluru.mit
 *
 */
public class IphoneDeviceUtility {

	private static final Logger logger = LoggerInterface.log;

	private static ArrayList<IPhoneDeviceDetailsVO> deviceInfoDataList = null;
	
	private static IphoneDeviceUtility iphoneDeviceUtility = null;
	
	
	/**
	 * private constructor
	 */
	private IphoneDeviceUtility(){
	}
	
	
	/**
	 * Creates the object if it is not created before.
	 * 
	 * @return iphoneDeviceUtility
	 */
	public static IphoneDeviceUtility getInstance() {
		if (iphoneDeviceUtility == null) {
			iphoneDeviceUtility = new IphoneDeviceUtility();
		}
		return iphoneDeviceUtility;
	}
	
	
	/**
	 * Return the device information object for the given deviceId
	 * 
	 * @param deviceId
	 * @return iphoneDeviceDataVo
	 */
	public IPhoneDeviceDetailsVO getIphoneDeviceDataById(String deviceId) {
		logger.info("IphoneDeviceUtility > getIphoneDeviceDataById > deviceId >"+deviceId);
		
		IPhoneDeviceDetailsVO iphoneDeviceDataVo = null;
		
		try {
			if (deviceInfoDataList == null) {
				deviceInfoDataList = loadIphoneDeviceInfoCachedItem();
			}

			for (int i = 0; i < deviceInfoDataList.size(); i++) {
				iphoneDeviceDataVo =  deviceInfoDataList.get(i);
				
				if (iphoneDeviceDataVo.getDeviceId().equalsIgnoreCase(deviceId)) {
					
					logger.info("IphoneDeviceUtility > getIphoneDeviceDataById > Matched");
					return iphoneDeviceDataVo;
				}
			}
		} catch (Exception e) {
			logger.fatal("IphoneDeviceUtility > getIphoneDeviceDataById > Exception > "+e.getMessage());
		   	  throw new SystemException(e);
		}
		
		logger.info("IphoneDeviceUtility > getIphoneDeviceDataById > End ");
		return iphoneDeviceDataVo;
	}
	
	
	/**
	 * Loads the iphone device data from the database
	 * 
	 * @return deviceInfoDataList
	 */
	public ArrayList<IPhoneDeviceDetailsVO> getIphoneDeviceInfo() {

		if (deviceInfoDataList == null) {
			deviceInfoDataList = loadIphoneDeviceInfoCachedItem();
			logger.debug("IphoneDeviceUtility > getIphoneDeviceInfo > Loading iphone device data for the first time.");
		}
		return deviceInfoDataList;
	}

	/**
	 * Load iphone device Information from DB Into Memory
	 * 
	 * @return
	 * @throws MobilyServicesException
	 */
	private static ArrayList<IPhoneDeviceDetailsVO> loadIphoneDeviceInfoCachedItem() {
		logger.info("IphoneDeviceUtility > loadIphoneDeviceInfoCachedItem > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		IPhoneDeviceInfoDAO deviceInfoDAO = daoFactory.getIPhoneDeviceInfoDAO();

		return deviceInfoDAO.getAllIPhoneDeviceInfo();
	}

	public void clearCache() {
		logger.info("IphoneDeviceUtility > clearCache > Called");
		deviceInfoDataList = null;
		
	}
}