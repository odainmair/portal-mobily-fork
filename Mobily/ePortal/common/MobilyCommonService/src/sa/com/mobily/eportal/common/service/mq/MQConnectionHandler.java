/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.mq;


/**
 * @author msayed
 * this class contain method for communicate with mq 
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
/**
 * @author Administrator
 *
 */
public class MQConnectionHandler {
		
	private static MobilyJMS mobilyJMS;
			
	private MQConnectionHandler(){
	}	
	
	public static MobilyJMS getInstance(){
		if(mobilyJMS == null){
			try {
				mobilyJMS = NativeJMS.getInstance();
			} catch (Exception e) {}
		}
		return mobilyJMS;
		
	}
}