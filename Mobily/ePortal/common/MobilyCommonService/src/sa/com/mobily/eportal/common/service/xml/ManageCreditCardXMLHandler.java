/*
 * Created on Sep 14, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.util.Date;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CreditCardDetailsVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardMessageHeaderVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentReplyVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentRequestVO;

import com.mobily.exception.xml.XMLException;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ManageCreditCardXMLHandler implements ConstantIfc, TagIfc 
{
	private static final Logger log = LoggerInterface.log;
    
	public static CreditCardPaymentReplyVO parseXMLReplyMessage(String XMLReply)
	{
		log.debug("ManageCreditCardXMLHandler > parseXMLReplyMessage > called");  
    	CreditCardPaymentReplyVO objCreditCardPaymentReplyVO = new CreditCardPaymentReplyVO();
    	CreditCardMessageHeaderVO objCreditCardMessageHeaderVO = null;
    	CreditCardDetailsVO objCreditCardDetailsVO = null;
    	Vector creditCardsDetailsVec = new Vector();
    	
        Document document = null;
        Element node = null;
	    String tagName = null;
	    String nodeValue = null;
	    
	    Node rootNode = null;
	    NodeList parentNodeList = null;
	    
	    NodeList childList = null;
	    Element childNode = null;
        String childNodeTagName = null;
        String childNodeValue = null;
        
        XMLException tempXMLException = null;
	    try{
	    	document = XMLUtility.parseXMLString(XMLReply);
	        
			rootNode = document.getElementsByTagName(MAIN_EAI_TAG_NAME).item(0);
		parentNodeList = rootNode.getChildNodes(); 
			for( int i = 0; parentNodeList != null && i < parentNodeList.getLength(); i++ ){
			    if( (parentNodeList.item(i)).getNodeType() !=  Node.ELEMENT_NODE)
			        continue;
			    node = (Element)parentNodeList.item(i);
			    tagName = node.getTagName();
			    //	Getting the header tag values
			    if(node.getFirstChild() != null){    
			        if( tagName.equalsIgnoreCase(EAI_HEADER_TAG)){
			        	objCreditCardMessageHeaderVO = new CreditCardMessageHeaderVO();
			        	nodeValue = node.getFirstChild().getNodeValue();   
				        childList = node.getChildNodes();
				        for(int j = 0; j < childList.getLength(); j++){
				            if((childList.item(j)).getNodeType() != Node.ELEMENT_NODE)
				                continue;
				            childNode = (Element)childList.item(j);
				            childNodeTagName = childNode.getTagName();
				            childNodeValue = "";
						    if(childNode.getFirstChild() != null )
						        childNodeValue = childNode.getFirstChild().getNodeValue();
						    if(childNodeTagName.equalsIgnoreCase(MSG_FORMAT))
						        objCreditCardMessageHeaderVO.setMsgFormat(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(TAG_REQUESTOR_CHANNEL_ID))
						        objCreditCardMessageHeaderVO.setRequestorChannelId(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(REQUESTOR_CHANNEL_FUNCTION)) 
						        objCreditCardMessageHeaderVO.setRequestorChannelFunction(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(RETURN_CODE))
						        objCreditCardMessageHeaderVO.setReturnCode(childNodeValue);
				        }
				        objCreditCardPaymentReplyVO.setHeader(objCreditCardMessageHeaderVO);
				        
		            }
			        
			        else if(tagName.equalsIgnoreCase(TAG_TransactionID)) 
		            {
		            	nodeValue = node.getFirstChild().getNodeValue();
				        objCreditCardPaymentReplyVO.setTransactionID(nodeValue);
		            }
			        else if(tagName.equalsIgnoreCase(TAG_MerchantID)) 
		            {
		            	nodeValue = node.getFirstChild().getNodeValue();
		            	objCreditCardPaymentReplyVO.setMerchantID(nodeValue);
		            }
			        else if(tagName.equalsIgnoreCase(TAG_MerchantName)) 
		            {
		            	nodeValue = node.getFirstChild().getNodeValue();
				        objCreditCardPaymentReplyVO.setMerchantName(nodeValue);
		            }
			        else if(tagName.equalsIgnoreCase(TAG_PaymentDateTime)) 
			        {
		            	nodeValue = node.getFirstChild().getNodeValue();
				        objCreditCardPaymentReplyVO.setPaymentDateTime(nodeValue);
		            }
			        else if(tagName.equalsIgnoreCase(TAG_Amount)) 
			        {
		            	nodeValue = node.getFirstChild().getNodeValue();
				        objCreditCardPaymentReplyVO.setAmount(nodeValue);
		            }
			        else if(tagName.equalsIgnoreCase(TAG_CreditCardDetails)) 
			        {
		            	objCreditCardDetailsVO = new CreditCardDetailsVO();
		            	nodeValue = node.getFirstChild().getNodeValue();   
				        childList = node.getChildNodes();
				        for(int j = 0; j < childList.getLength(); j++) 
				        {
				            if((childList.item(j)).getNodeType() != Node.ELEMENT_NODE)
				                continue;
				            childNode = (Element)childList.item(j);
				            childNodeTagName = childNode.getTagName();
				            childNodeValue = "";
						    if(childNode.getFirstChild() != null)
						        childNodeValue = childNode.getFirstChild().getNodeValue();
						    if(childNodeTagName.equalsIgnoreCase(TAG_CrediCardID))
						    	objCreditCardDetailsVO.setId(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(TAG_CreditCardNumber))
						    	objCreditCardDetailsVO.setCreditCardNumber(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(TAG_CreditCardType))
						    	objCreditCardDetailsVO.setCreditCardType(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(TAG_PrimeMSISDN))
						    	objCreditCardDetailsVO.setPrimeMSISDN(childNodeValue);
						    else if(childNodeTagName.equalsIgnoreCase(TAG_CreditCardStatus))
						    	objCreditCardDetailsVO.setCreditCardStatus(childNodeValue);
				        }
		            	creditCardsDetailsVec.add(objCreditCardDetailsVO);
		            }
			    }
			}
	    	objCreditCardPaymentReplyVO.setCreditCardsDetails(creditCardsDetailsVec);

	    } catch (Exception e) {
            log.fatal("ManageCreditCardXMLHandler > parseXMLReplyMessage > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        
	    return objCreditCardPaymentReplyVO;
    }

    public static String generateXMLRequestMessage(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO)
	{
    	log.debug("ManageCreditCardXMLHandler > generateXMLRequestMessage > called");  
    	String tempXMLRequest = null;
        Document document = new DocumentImpl();
        
        Element elementEE_EAI_MESSAGE = document.createElement(MAIN_EAI_TAG_NAME);
        Element elementTAG_EE_EAI_HEADER = document.createElement(EAI_HEADER_TAG);
        
        Element elementTAG_MsgFormat = document.createElement(MSG_FORMAT);
        Element elementTAG_RequestorChannelId = document.createElement(TAG_REQUESTOR_CHANNEL_ID);
        Element elementTAG_RequestorChannelFunction = document.createElement(REQUESTOR_CHANNEL_FUNCTION);
		Element elementTAG_RequestorIpAddress = document.createElement(TAG_RequestorIpAddress);
        Element elementTAG_RequestorUserId = document.createElement(REQUESTOR_USER_ID);
        Element elementTAG_ReturnCode = document.createElement(RETURN_CODE);
        
        Element elementTAG_IsCorpAP = document.createElement(TAG_IsCorpAP);
        
        Element elementTAG_TransactionID = document.createElement(TAG_TransactionID);
        Element elementTAG_TransactionDate = document.createElement(TAG_TransactionDate);

        Element elementTAG_CrediCardID = document.createElement(TAG_CrediCardID);

        Element elementTAG_TransactionPIN = document.createElement(TAG_TransactionPIN);
        Element elementTAG_NewTransactionPIN = document.createElement(TAG_NewTransactionPIN);
        
        Element elementTAG_CreditCardMSISDN = document.createElement(TAG_CreditCardMSISDN);
        Element elementTAG_CorporateAccountNumber = document.createElement(TAG_CORPORATE_ACCOUNT_NUMBER);
        Element elementTAG_PrimeMSISDN = document.createElement(TAG_PrimeMSISDN);
        Element elementTAG_NewPrimeMSISDN = document.createElement(TAG_NewPrimeMSISDN);
        
        Element elementTAG_CreditCardNumber = document.createElement(TAG_CreditCardNumber);
        Element elementTAG_CreditCardType = document.createElement(TAG_CreditCardType);
        
        Element elementTAG_ExpiryDate = document.createElement(TAG_ExpiryDate);
        Element elementTAG_CVV = document.createElement(TAG_CVV);
        Element elementTAG_Amount = document.createElement(TAG_Amount);

        Element elementTAG_Name = document.createElement(TAG_Name);
        Element elementTAG_CustomerType = document.createElement(TAG_CustomerType);

        Element elementTAG_AccountNumber = document.createElement(TAG_AccountNumber);
        Element elementTAG_SMS_TO = document.createElement(TAG_SMS_TO);
        

        //	Write Header Elements data
        elementTAG_MsgFormat.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getHeader().getMsgFormat()));
        elementTAG_RequestorChannelId.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getHeader().getRequestorChannelId()));
        elementTAG_RequestorChannelFunction.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getHeader().getRequestorChannelFunction()));
		elementTAG_RequestorIpAddress.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getHeader().getRequestorIpAddress()));
        elementTAG_RequestorUserId.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getHeader().getRequestorUserId()));
        elementTAG_ReturnCode.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getHeader().getReturnCode()));

        //	Add HEADER sub tags
        elementTAG_EE_EAI_HEADER.appendChild(elementTAG_MsgFormat);
        elementTAG_EE_EAI_HEADER.appendChild(elementTAG_RequestorChannelId);
        elementTAG_EE_EAI_HEADER.appendChild(elementTAG_RequestorChannelFunction);
        elementTAG_EE_EAI_HEADER.appendChild(elementTAG_RequestorIpAddress);
        elementTAG_EE_EAI_HEADER.appendChild(elementTAG_RequestorUserId);
        elementTAG_EE_EAI_HEADER.appendChild(elementTAG_ReturnCode);
        
        //	Add EE_EAI_MESSAGE sub tag
        elementEE_EAI_MESSAGE.appendChild(elementTAG_EE_EAI_HEADER);
        
        if(!FormatterUtility.isEmpty(objCreditCardPaymentRequestVO.getIsCorpAP()))
        {
        //	Write Element data
	        elementTAG_IsCorpAP.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getIsCorpAP()));
	        
	        //	Add EE_EAI_MESSAGE sub tag
	        elementEE_EAI_MESSAGE.appendChild(elementTAG_IsCorpAP);
        }

        //	Write Element data
        elementTAG_TransactionID.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getTransactionID()));
        
        //	Add EE_EAI_MESSAGE sub tag
        elementEE_EAI_MESSAGE.appendChild(elementTAG_TransactionID);
        
        if(objCreditCardPaymentRequestVO.getTransactionDate() != null)
        {
            //	Write Element data
        	elementTAG_TransactionDate.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getTransactionDate()));
            //	Add EE_EAI_MESSAGE sub tag
            elementEE_EAI_MESSAGE.appendChild(elementTAG_TransactionDate);
        }
        
        if(objCreditCardPaymentRequestVO.getID() != null)
        {
        	elementTAG_CrediCardID.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getID()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CrediCardID);
        }

        if(objCreditCardPaymentRequestVO.getTransactionPIN() != null)
        {
        	elementTAG_TransactionPIN.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getTransactionPIN()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_TransactionPIN);
        }

        if(objCreditCardPaymentRequestVO.getNewTransactionPIN() != null)
        {
        	elementTAG_NewTransactionPIN.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getNewTransactionPIN()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_NewTransactionPIN);
        }

        if(objCreditCardPaymentRequestVO.getMSISDN() != null)
        {
        	elementTAG_CreditCardMSISDN.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getMSISDN()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CreditCardMSISDN);
        }

        if(objCreditCardPaymentRequestVO.getBillAccountNumber() != null)
        {
        	elementTAG_CorporateAccountNumber.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getBillAccountNumber()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CorporateAccountNumber);
        }
        
        if(objCreditCardPaymentRequestVO.getPrimeMSISDN() != null)
        {
        	elementTAG_PrimeMSISDN.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getPrimeMSISDN()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_PrimeMSISDN);
        }

        if(objCreditCardPaymentRequestVO.getNewPrimeMSISDN() != null)
        {
        	elementTAG_NewPrimeMSISDN.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getNewPrimeMSISDN()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_NewPrimeMSISDN);
        }

        if(objCreditCardPaymentRequestVO.getCreditCardNumber() != null)
        {
        	elementTAG_CreditCardNumber.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getCreditCardNumber()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CreditCardNumber);
        }
		
        if(objCreditCardPaymentRequestVO.getCreditCardType() != null)
        {
        	elementTAG_CreditCardType.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getCreditCardType()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CreditCardType);
        }
		
        if(objCreditCardPaymentRequestVO.getExpiryDate() != null)
        {
        	elementTAG_ExpiryDate.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getExpiryDate()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_ExpiryDate);
        }
		
        if(objCreditCardPaymentRequestVO.getCVV() != null)
        {
        	elementTAG_CVV.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getCVV()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CVV);
        }
		
        if(objCreditCardPaymentRequestVO.getAmount() != null)
        {
        	elementTAG_Amount.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getAmount()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_Amount);
        }
		
        if(objCreditCardPaymentRequestVO.getName() != null)
        {
        	elementTAG_Name.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getName()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_Name);
        }
        
        if(objCreditCardPaymentRequestVO.getCustomerType() != null)
        {
        	elementTAG_CustomerType.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getCustomerType()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_CustomerType);
        }

        if(objCreditCardPaymentRequestVO.getAccountNumber() != null)
        {
        	elementTAG_AccountNumber.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getAccountNumber()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_AccountNumber);
        }

		if(objCreditCardPaymentRequestVO.getSmsTo()!=null)
        {
        	elementTAG_SMS_TO.appendChild(document.createTextNode(objCreditCardPaymentRequestVO.getSmsTo()));
            elementEE_EAI_MESSAGE.appendChild(elementTAG_SMS_TO);
        }
		
       // 	Add to Document
        document.appendChild(elementEE_EAI_MESSAGE);

        try {
            //	Serialize
            tempXMLRequest = XMLUtility.serializeRequest( document );

        } catch (java.io.IOException e) {
            log.fatal("ManageCreditCardXMLHandler > generateXMLRequestMessage > IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            log.fatal("ManageCreditCardXMLHandler > generateXMLRequestMessage > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        return tempXMLRequest;
    }
}
