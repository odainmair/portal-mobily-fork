/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ContactVO extends BaseVO {
    protected String contactPreference;
    protected String streetAddress;
    protected String poBox;
    protected String mobile;
    protected String phone;
    protected String fax;
    protected String email;
    protected String Mdn;
    protected String Name;
    protected String OldMdn;
    protected String OldName;
    protected String companyRowId;
    protected String postalCode;
    protected String billingCountry ;
    
    
    
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	/**
     * @return Returns the email.
     */
    public String getEmail()
    {
        return email;
    }
    public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getOldMdn() {
		return OldMdn;
	}
	public void setOldMdn(String oldMdn) {
		OldMdn = oldMdn;
	}
	public String getOldName() {
		return OldName;
	}
	public void setOldName(String oldName) {
		OldName = oldName;
	}
	public String getMdn() {
		return Mdn;
	}
	public void setMdn(String mdn) {
		Mdn = mdn;
	}
	/**
     * @param email The email to set.
     */
    public void setEmail(String email)
    {
        this.email = email;
    }
    /**
     * @return Returns the fax.
     */
    public String getFax()
    {
        return fax;
    }
    /**
     * @param fax The fax to set.
     */
    public void setFax(String fax)
    {
        this.fax = fax;
    }
    /**
     * @return Returns the mobile.
     */
    public String getMobile()
    {
        return mobile;
    }
    /**
     * @param mobile The mobile to set.
     */
    public void setMobile(String mobile)
    {
        this.mobile = mobile;
    }
    /**
     * @return Returns the contactPreference.
     */
    public String getContactPreference()
    {
        return contactPreference;
    }
    /**
     * @param contactPreference The contactPreference to set.
     */
    public void setContactPreference(String contactPreference)
    {
        this.contactPreference = contactPreference;
    }
    /**
     * @return Returns the streetAddress.
     */
    public String getStreetAddress()
    {
        return streetAddress;
    }
    /**
     * @param streetAddress The streetAddress to set.
     */
    public void setStreetAddress(String streetAddress)
    {
        this.streetAddress = streetAddress;
    }
    /**
     * @return Returns the phone.
     */
    public String getPhone()
    {
        return phone;
    }
    /**
     * @param phone The phone to set.
     */
    public void setPhone(String phone)
    {
        this.phone = phone;
    }
    /**
     * @return Returns the poBox.
     */
    public String getPoBox()
    {
        return poBox;
    }
    /**
     * @param poBox The poBox to set.
     */
    public void setPoBox(String poBox)
    {
        this.poBox = poBox;
    }

    public String getCompanyRowId() {
		return companyRowId;
	}
	
    public void setCompanyRowId(String companyRowId) {
		this.companyRowId = companyRowId;
	}
}