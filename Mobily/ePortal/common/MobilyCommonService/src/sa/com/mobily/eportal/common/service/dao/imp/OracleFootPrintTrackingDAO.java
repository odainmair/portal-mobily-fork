package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.FootPrintTrackingDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.FootPrintVO;


/**
 * 
 * @author n.gundluru.mit
 *
 */
public class OracleFootPrintTrackingDAO implements FootPrintTrackingDAO{

	private static final Logger logger = LoggerInterface.log;

	
	/**
	 * 
	 */
	public void addRow(FootPrintVO  footPrintVO) {
		 
		logger.info("OracleFootPrintTrackingDAO > addRow > start");
		Connection connection = null;
		PreparedStatement pstmt = null;

		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));

		    StringBuffer sqlQuery = new StringBuffer();
		    
				sqlQuery.append("INSERT INTO FT_EVENT_TRACKING (ID, TIME_STAMP, EVENT_ID, USER_ID, KEY, SUB_KEY, COMMENTS,");
					sqlQuery.append(" SYSTEM_ERROR_MESSAGE, USER_ERROR_MESSAGE, ERROR_CODE, REQUESTERCHANNELID,SERVICE_ACT_NUM)");
					sqlQuery.append(" VALUES (FT_EVENT_TRACKING_ID_SEQ.nextval, sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
					
					pstmt = connection.prepareStatement(sqlQuery.toString());
					
						pstmt.setString(1, footPrintVO.getEventName());
						pstmt.setString(2,footPrintVO.getUserId());
						pstmt.setString(3,footPrintVO.getKey());
						pstmt.setString(4,footPrintVO.getSubKey());
						pstmt.setString(5,footPrintVO.getComments());
						pstmt.setString(6,footPrintVO.getSystemErrorMessage());
						pstmt.setString(7,footPrintVO.getUserErrorMessage());
						pstmt.setString(8,footPrintVO.getErrorCode());
						pstmt.setInt(9,footPrintVO.getRequesterChannelId());
						pstmt.setString(10,footPrintVO.getServiceAccount());
						
					pstmt.executeUpdate();
					
		} catch(Exception e){
			logger.fatal("OracleFootPrintTrackingDAO > addRow > Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
		}finally{
			JDBCUtility.closeJDBCResoucrs(connection, pstmt, null);
		}
	}
}