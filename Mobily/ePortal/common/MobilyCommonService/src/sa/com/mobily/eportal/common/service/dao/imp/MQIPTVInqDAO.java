/**
 * @date: Sep 27, 2012
 * @author: s.n.gundluru.mit
 * @file name: MQIPTVDAO.java
 * @description:  
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.util.Date;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.IPTVInqDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * @author s.gundluru.mit
 *
 */
public class MQIPTVInqDAO implements IPTVInqDAO{ 

	private static final Logger logger = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
	
	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.IPTVInqDAO#doIPTVInquiry(java.lang.String)
	 */
	public String doIPTVInquiry(String strXMLRequest){
		 String replyMessage = "";
		 logger.info("MQIPTVDAO > doIPTVInquiry :: strXMLRequest = "+strXMLRequest);
		 String requestQueName = MobilyUtility.getPropertyValue(ConstantIfc.REQUEST_QUEUE_IPTV_INQ_INQUIRY);
		 String replyQueName = MobilyUtility.getPropertyValue( ConstantIfc.REPLY_QUEUE_IPTV_INQ_INQUIRY);
		  	 
		 logger.debug("MQIPTVDAO > doIPTVInquiry : Request Queue Name:=["+requestQueName+"]");
		 logger.debug("MQIPTVDAO > doIPTVInquiry : Reply Queue Name:=["+ replyQueName+"]");
		 
		try {
			 replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueName, replyQueName);
		}catch (RuntimeException e) {
		    logger.debug("MQIPTVDAO > doIPTVInquiry > exception time: "+new Date());
			logger.fatal("MQIPTVDAO > doIPTVInquiry > MQSend Exception > "+ e);
			throw new SystemException(e);
		} catch (Exception e) {
		    logger.fatal("MQIPTVDAO > doIPTVInquiry  > Exception > "+ e.getMessage());
			throw new SystemException(e);
		}
		 logger.info("MQIPTVDAO > doIPTVInquiry  > end :: replyMessage = "+replyMessage);
		 return replyMessage;
	}
}