/*
 * Created on Apr 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ContactUSObjectVO {

	private String inquiryTypeDesc_ar;
	private String inquiryTypeDesc_en;
	private String mailTO;
	private int count;
	private String ID;
	private String reply;
	private int showToUser;
	
	public static void main(String[] args) {
	}
	
	public String getInquiryDesc(String locale){
		String desc = "";
	   if(locale.equalsIgnoreCase("ar"))
	   	desc= inquiryTypeDesc_ar;
	   else if(locale.equalsIgnoreCase("en"))
	   	desc= inquiryTypeDesc_en;
	   
	   return desc;
	}
	/**
	 * @return Returns the count.
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count The count to set.
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return Returns the iD.
	 */
	public String getID() {
		return ID;
	}
	/**
	 * @param id The iD to set.
	 */
	public void setID(String id) {
		ID = id;
	}
	/**
	 * @return Returns the inquiryTypeDesc_ar.
	 */
	public String getInquiryTypeDesc_ar() {
		return inquiryTypeDesc_ar;
	}
	/**
	 * @param inquiryTypeDesc_ar The inquiryTypeDesc_ar to set.
	 */
	public void setInquiryTypeDesc_ar(String inquiryTypeDesc_ar) {
		this.inquiryTypeDesc_ar = inquiryTypeDesc_ar;
	}
	/**
	 * @return Returns the inquiryTypeDesc_en.
	 */
	public String getInquiryTypeDesc_en() {
		return inquiryTypeDesc_en;
	}
	/**
	 * @param inquiryTypeDesc_en The inquiryTypeDesc_en to set.
	 */
	public void setInquiryTypeDesc_en(String inquiryTypeDesc_en) {
		this.inquiryTypeDesc_en = inquiryTypeDesc_en;
	}
	/**
	 * @return Returns the mailTO.
	 */
	public String getMailTO() {
		return mailTO;
	}
	/**
	 * @param mailTO The mailTO to set.
	 */
	public void setMailTO(String mailTO) {
		this.mailTO = mailTO;
	}
	/**
	 * @return Returns the reply.
	 */
	public String getReply() {
		return reply;
	}
	/**
	 * @param reply The reply to set.
	 */
	public void setReply(String reply) {
		this.reply = reply;
	}

	public int getShowToUser() {
		return showToUser;
	}

	public void setShowToUser(int showToUser) {
		this.showToUser = showToUser;
	}
}
