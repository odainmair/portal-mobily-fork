package sa.com.mobily.eportal.common.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import org.apache.commons.collections.CollectionUtils;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.valueobject.common.AppErrorCodeInfoVO;

public class AppErrorCodeInfoDAO extends AbstractDBDAO<AppErrorCodeInfoVO>{
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static final String className =AppErrorCodeInfoDAO.class.getName();

	private static AppErrorCodeInfoDAO instance = new AppErrorCodeInfoDAO();

	private static final String GET_ALL_ERROR_CODES_INFO="SELECT ERROR_CODE,ERROR_DESC_EN,ERROR_DESC_AR,UPDATED_DATE FROM APP_ERROR_CODE_INFO_TBL";
	
	private static final String GET_ERROR_INFO_BY_ERROR_CODE="SELECT ERROR_CODE,ERROR_DESC_EN,ERROR_DESC_AR,UPDATED_DATE FROM APP_ERROR_CODE_INFO_TBL WHERE ERROR_CODE =?";
	
	protected AppErrorCodeInfoDAO(){
		super(DataSources.APPDB);
	}
	
	public static AppErrorCodeInfoDAO getInstance()
	{
		return instance;
	}
	/*
	 * Get the error info by Error Code
	 * @see sa.com.mobily.eportal.app.error.info.dao.ifc.AppErrorCodeInfoIfc#getErrorInfoByErrorCode(java.lang.String)
	 */
	 
	public AppErrorCodeInfoVO getErrorInfoByErrorCode(String errorCode){

		List<AppErrorCodeInfoVO> errorCodeInfoList = null;
		AppErrorCodeInfoVO errorCodeInfo=null;
		executionContext.audit(Level.INFO,"AppErrorCodeInfoDAO >> getErrorInfoByErrorCode >errorCode::"+errorCode,"getErrorInfoByErrorCode",className);
		executionContext.audit(Level.INFO,"AppErrorCodeInfoDAO >> getErrorInfoByErrorCode >sql query::"+GET_ERROR_INFO_BY_ERROR_CODE,"getErrorInfoByErrorCode",className);
		errorCodeInfoList = query(GET_ERROR_INFO_BY_ERROR_CODE,errorCode);
		executionContext.audit(Level.INFO,"AppErrorCodeInfoDAO >> getAllErrorCodeInfo >errorCodeInfoList::"+errorCodeInfoList,"getAllErrorCodeInfo",className);
		if(CollectionUtils.isNotEmpty(errorCodeInfoList)){
			errorCodeInfo= errorCodeInfoList.get(0);
		}
		return  errorCodeInfo;
	}
	/*
	 * Get All the Error Codes
	 * @see sa.com.mobily.eportal.app.error.info.dao.ifc.AppErrorCodeInfoIfc#getAllErrorCodeInfo()
	 */
	public List<AppErrorCodeInfoVO> getAllErrorCodeInfo(){
		
		List<AppErrorCodeInfoVO> errorCodeInfoList = null;
		executionContext.audit(Level.INFO,"AppErrorCodeInfoDAO >> getAllErrorCodeInfo >sql query::"+GET_ALL_ERROR_CODES_INFO,"getAllErrorCodeInfo",className);
		errorCodeInfoList = query(GET_ALL_ERROR_CODES_INFO);
		executionContext.audit(Level.INFO,"AppErrorCodeInfoDAO >> getAllErrorCodeInfo >errorCodeInfoList::"+errorCodeInfoList,"getAllErrorCodeInfo",className);
		return errorCodeInfoList;
	}
	
	@Override
	protected AppErrorCodeInfoVO mapDTO(ResultSet rs) throws SQLException {
		
		AppErrorCodeInfoVO errorCodeInfo = new AppErrorCodeInfoVO();
		if(rs.getMetaData().getColumnCount() == 4){
			errorCodeInfo.setErrorCode(rs.getString("ERROR_CODE"));
			errorCodeInfo.setErrorCodeEn(rs.getString("ERROR_DESC_EN"));
			errorCodeInfo.setErrorCodeAr(rs.getString("ERROR_DESC_AR"));
			errorCodeInfo.setUpdatedDate(rs.getDate("UPDATED_DATE"));
		}

		return errorCodeInfo;
	}

}

