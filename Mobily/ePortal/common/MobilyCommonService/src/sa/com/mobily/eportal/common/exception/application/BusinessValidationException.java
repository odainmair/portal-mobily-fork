package sa.com.mobily.eportal.common.exception.application;


public class BusinessValidationException extends MobilyApplicationException {

	private static final long serialVersionUID = -3667808518632392290L;

	public BusinessValidationException() {
	}
	public BusinessValidationException(String message) {
		super(message);
	}

	public BusinessValidationException(Throwable cause) {
		super(cause);
	}

	public BusinessValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}