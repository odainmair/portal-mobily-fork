/*
 * Created on Dec 3, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PhoneBookGroupVO {
private String MSISDN;
private int distributionListId;
private int groupId;
private int functionId;
private String groupName;
private ArrayList listOfGroupMemeber = null;
	



public int getGroupId() {
	return groupId;
}
public void setGroupId(int groupId) {
	this.groupId = groupId;
}
public String getGroupName() {
	return groupName;
}
public void setGroupName(String groupName) {
	this.groupName = groupName;
}
public int getDistributionListId() {
	return distributionListId;
}
public void setDistributionListId(int groupSequenceId) {
	this.distributionListId = groupSequenceId;
}
public ArrayList getListOfGroupMemeber() {
	return listOfGroupMemeber;
}
public void setListOfGroupMemeber(ArrayList listOfGroupMemeber) {
	this.listOfGroupMemeber = listOfGroupMemeber;
}
public int getFunctionId() {
	return functionId;
}
public void setFunctionId(int functionId) {
	this.functionId = functionId;
}
public String getMSISDN() {
	return MSISDN;
}
public void setMSISDN(String msisdn) {
	MSISDN = msisdn;
}
}
