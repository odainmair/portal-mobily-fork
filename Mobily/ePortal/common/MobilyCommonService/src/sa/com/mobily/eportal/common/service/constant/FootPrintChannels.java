package sa.com.mobily.eportal.common.service.constant;

public enum FootPrintChannels{
	
	WEB(1),
	Mobile(2),
	Toolbar(3),
	OTT(4),
	BMS(5),
	UCM(6),
	NGSDP(7)
	;
	
	
	private final int channel;
	
	FootPrintChannels(int channel){
		this.channel = channel;
	}

	public int getChannel()
	{
		return channel;
	}
}
