/*
 * Created on Feb 26, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.LookupTableDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.LookupTableVO;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleLookupTableDAO implements LookupTableDAO {
	private static final Logger log = LoggerInterface.log;
	
	public  ArrayList getLookupByID(String id) 
	{
		log.info("LookupTableDAO.getLookupByID(): start get items by id="+id);
 	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet results = null;

		ArrayList itemsList = new ArrayList();
		LookupTableVO objLookupTableVO = null;

		try
		{
			String sqlQuery = "SELECT ID , SUB_ID , ITEM , ITEM_DESC_EN , ITEM_DESC_AR  FROM SR_LOOKUP_TBL WHERE ID=? and SUB_ID<>0 ORDER BY SUB_ID";
			connection = DataBaseConnectionHandler.getDBConnection(ConstantIfc.DATA_SOURCE_EEDB);

			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1, id);
			results = statement.executeQuery();				
					
		      while(results.next())
		      {
		      	objLookupTableVO = new LookupTableVO();
		      	objLookupTableVO.setId(results.getString("ID"));
		      	objLookupTableVO.setSub_id(results.getInt("SUB_ID"));
		      	objLookupTableVO.setItem(results.getString("ITEM"));
		      	objLookupTableVO.setItemDesc_en(results.getString("ITEM_DESC_EN"));
		      	objLookupTableVO.setItemDesc_ar(results.getString("ITEM_DESC_AR"));
		      	itemsList.add(objLookupTableVO);
		      	
		      }

		}
		catch(Exception e)
		{
			throw new SystemException(e);
		}
		finally
		{
			JDBCUtility.closeJDBCResoucrs(connection , statement , results);
		}

		return itemsList;
	}
	
	public  LookupTableVO getLookupByIdAndSubId(String id,String SubId) 
	{
		log.info("LookupTableDAO.getLookupByID(): start get items by id="+id);
 	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet results = null;
		LookupTableVO objLookupTableVO = null;

		try
		{
			String sqlQuery = "SELECT ID , SUB_ID , ITEM , ITEM_DESC_EN , ITEM_DESC_AR  FROM SR_LOOKUP_TBL WHERE ID=? and SUB_ID=? ORDER BY SUB_ID";
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));

			statement  = connection.prepareStatement(sqlQuery.toString());
			statement.setString(1, id);
			statement.setString(2, SubId);
			results = statement.executeQuery();				
					
		      while(results.next())
		      {
		      	objLookupTableVO = new LookupTableVO();
		      	objLookupTableVO.setId(results.getString("ID"));
		      	objLookupTableVO.setSub_id(results.getInt("SUB_ID"));
		      	objLookupTableVO.setItem(results.getString("ITEM"));
		      	objLookupTableVO.setItemDesc_en(results.getString("ITEM_DESC_EN"));
		      	objLookupTableVO.setItemDesc_ar(results.getString("ITEM_DESC_AR"));		      	
		      }

		}
		catch(Exception e)
		{
			throw new SystemException(e);
		}
		finally
		{
			JDBCUtility.closeJDBCResoucrs(connection , statement , results);
		}

		return objLookupTableVO;
	}
	/**
	 * @return
	 * @throws ContentProviderException
	 */
	public   Hashtable loadLookupFromDB() {
		
		log.info("LookupTableDAO > loadLookupFromDB > Called ");
		Hashtable lookupHash  = new Hashtable();
 	    Connection connection   = null;
	    ResultSet resultset     = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
     	  String sqlStatament = "SELECT ID , SUB_ID , ITEM , ITEM_DESC_EN , ITEM_DESC_AR  FROM SR_LOOKUP_TBL  where SUB_ID != 0 ORDER BY ID , SUB_ID ";
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament);
	      resultset  = pstm.executeQuery();
	      ArrayList listOfItem  = null;
	      LookupTableVO objectVO  = null;
	      while(resultset.next()){
	      	    objectVO = new LookupTableVO();
		      	String id = resultset.getString("ID");
		      	objectVO.setId(id);
		      	objectVO.setSub_id(resultset.getInt("SUB_ID"));
		      	objectVO.setItem(resultset.getString("ITEM"));
		      	objectVO.setItemDesc_en(resultset.getString("ITEM_DESC_EN"));
		      	objectVO.setItemDesc_ar(resultset.getString("ITEM_DESC_AR"));
		      	if(lookupHash.containsKey(id)) {
		      		listOfItem = (ArrayList)lookupHash.get(id);
		      		listOfItem.add(objectVO);
		      	 }else {
		      	 	listOfItem = new ArrayList();
		      	 	listOfItem.add(objectVO);
		      	 	lookupHash.put(id , listOfItem);
		      	}
	      }
	      log.debug("LookupTableDAO > loadLookupFromDB > Done ["+lookupHash.size()+"]");
	   }catch(SQLException e){
	   		log.fatal("LookupTableDAO > loadLookupFromDB > SQLException > "+e.getMessage());
	   		throw new SystemException(e);
	   	
	   }catch(Exception e){
	   		log.fatal("LookupTableDAO > loadLookupFromDB >  Exception > "+e.getMessage());
	   		throw new SystemException(e);  
	   }
	   finally{
	       JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
	  }
	  
	   return lookupHash;
	}	

	
	/**
	 * 
	 * 
	 * @return Hashtable
	 * @throws ContentProviderException
	 */
	public   Hashtable loadCityLookupFromDB() {
		
		log.info("LookupTableDAO > loadCityLookupFromDB > Called ");
		Hashtable lookupHash  = new Hashtable();
 	    Connection connection   = null;
	    ResultSet resultset     = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
     	  String sqlStatament = "SELECT ID , SUB_ID , ITEM , ITEM_DESC_EN , ITEM_DESC_AR  FROM CITY_LOOKUP_TBL  where SUB_ID != 0 ORDER BY ID , SUB_ID";
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament);
	      resultset  = pstm.executeQuery();
	      ArrayList listOfItem  = null;
	      LookupTableVO objectVO  = null;
	      while(resultset.next()){
	      	    objectVO = new LookupTableVO();
		      	String id = resultset.getString("ID");
		      	objectVO.setId(id);
		      	objectVO.setSub_id(resultset.getInt("SUB_ID"));
		      	objectVO.setItem(resultset.getString("ITEM"));
		      	objectVO.setItemDesc_en(resultset.getString("ITEM_DESC_EN"));
		      	objectVO.setItemDesc_ar(resultset.getString("ITEM_DESC_AR"));
		      	if(lookupHash.containsKey(id)) {
		      		listOfItem = (ArrayList)lookupHash.get(id);
		      		listOfItem.add(objectVO);
		      	 }else {
		      	 	listOfItem = new ArrayList();
		      	 	listOfItem.add(objectVO);
		      	 	lookupHash.put(id , listOfItem);
		      	}
	      }
	      log.debug("LookupTableDAO > loadCityLookupFromDB > Done ["+lookupHash.size()+"]");
	   }catch(SQLException e){
	   		log.fatal("LookupTableDAO > loadCityLookupFromDB > SQLException > "+e.getMessage());
	   		throw new SystemException(e);
	   	
	   }catch(Exception e){
	   		log.fatal("LookupTableDAO > loadCityLookupFromDB >  Exception > "+e.getMessage());
	   		throw new SystemException(e);  
	   }
	   finally{
	       JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
	  }
	  
	   return lookupHash;
	}	


	
	/**
	 * Sorts the columns based on the given column name
	 * 
	 * @param colmnName
	 * @return hashTable
	 */
	public Hashtable loadCityLookupFromDBByColumn(String colmnName) {
		
		log.info("LookupTableDAO > loadCityLookupFromDBByColumn > Called ");
		Hashtable lookupHash  = new Hashtable();
 	    Connection connection   = null;
	    ResultSet resultset     = null;
	    Statement stmt  = null;
     	  
	   try{
   	  
	       StringBuffer sqlStatament = new StringBuffer();
	       	sqlStatament.append("SELECT ID , SUB_ID , ITEM , ITEM_DESC_EN , ITEM_DESC_AR");
	       	sqlStatament.append(" FROM CITY_LOOKUP_TBL");
	       	sqlStatament.append(" WHERE SUB_ID != 0 ORDER BY ");
	       	sqlStatament.append(colmnName);
	       
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));

	      stmt       = connection.createStatement();
	      
	      resultset  = stmt.executeQuery(sqlStatament.toString());
	      
	      ArrayList listOfItem  = null;
	      LookupTableVO objectVO  = null;
	      while(resultset.next()){
	      	    objectVO = new LookupTableVO();
		      	String id = resultset.getString("ID");
		      	objectVO.setId(id);
		      	objectVO.setSub_id(resultset.getInt("SUB_ID"));
		      	objectVO.setItem(resultset.getString("ITEM"));
		      	objectVO.setItemDesc_en(resultset.getString("ITEM_DESC_EN"));
		      	objectVO.setItemDesc_ar(resultset.getString("ITEM_DESC_AR"));
		      	if(lookupHash.containsKey(id)) {
		      		listOfItem = (ArrayList)lookupHash.get(id);
		      		listOfItem.add(objectVO);
		      	 }else {
		      	 	listOfItem = new ArrayList();
		      	 	listOfItem.add(objectVO);
		      	 	lookupHash.put(id , listOfItem);
		      	}
	      }
	      log.debug("LookupTableDAO > loadCityLookupFromDBByColumn > Done ["+lookupHash.size()+"]");
	   }catch(SQLException e){
	   		log.fatal("LookupTableDAO > loadCityLookupFromDBByColumn > SQLException > "+e.getMessage());
	   		throw new SystemException(e);
	   	
	   }catch(Exception e){
	   		log.fatal("LookupTableDAO > loadCityLookupFromDBByColumn >  Exception > "+e.getMessage());
	   		throw new SystemException(e);  
	   }
	   finally{
	       JDBCUtility.closeJDBCResoucrs(connection , stmt , resultset);
	  }
	  
	   return lookupHash;
	}	
	
	/**
	 * Sorts the columns based on the given column name
	 * 
	 * @param colmnName
	 * @return hashTable
	 */
	public Hashtable loadCountriesByColumn(String columnName) {
		
		log.info("LookupTableDAO > loadCountriesByColumn > Called ");
		Hashtable lookupHash  = new Hashtable();
 	    Connection connection   = null;
	    ResultSet resultset     = null;
	    Statement stmt  = null;
     	  
	   try{
   	  
	       StringBuffer sqlStatament = new StringBuffer();
	       sqlStatament.append("SELECT ID , SUB_ID , ITEM , ITEM_DESC_EN , ITEM_DESC_AR");
	       sqlStatament.append(" FROM SR_LOOKUP_TBL");
	       sqlStatament.append(" WHERE SUB_ID != 0 ORDER BY ");
	       sqlStatament.append(columnName);
	       
	       connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	       stmt       = connection.createStatement();
	       resultset  = stmt.executeQuery(sqlStatament.toString());
	      
	       ArrayList listOfItem  = null;
	       LookupTableVO objectVO  = null;
	       while(resultset.next()){
	      	    objectVO = new LookupTableVO();
		      	String id = resultset.getString("ID");
		      	objectVO.setId(id);
		      	objectVO.setSub_id(resultset.getInt("SUB_ID"));
		      	objectVO.setItem(resultset.getString("ITEM"));
		      	objectVO.setItemDesc_en(resultset.getString("ITEM_DESC_EN"));
		      	objectVO.setItemDesc_ar(resultset.getString("ITEM_DESC_AR"));
		      	if(lookupHash.containsKey(id)) {
		      		listOfItem = (ArrayList)lookupHash.get(id);
		      		listOfItem.add(objectVO);
		      	 } else {
		      	 	listOfItem = new ArrayList();
		      	 	listOfItem.add(objectVO);
		      	 	lookupHash.put(id , listOfItem);
		      	}
	       }
	       log.debug("LookupTableDAO > loadCountriesByColumn > Done ["+lookupHash.size()+"]");
	   } catch(SQLException e){
	   		log.fatal("LookupTableDAO > loadCountriesByColumn > SQLException > "+e.getMessage());
	   		throw new SystemException(e);
	   }catch(Exception e){
	   		log.fatal("LookupTableDAO > loadCountriesByColumn >  Exception > "+e.getMessage());
	   		throw new SystemException(e);  
	   }
	   finally{
	       JDBCUtility.closeJDBCResoucrs(connection , stmt , resultset);
	  }
	  
	   return lookupHash;
	}	

}
