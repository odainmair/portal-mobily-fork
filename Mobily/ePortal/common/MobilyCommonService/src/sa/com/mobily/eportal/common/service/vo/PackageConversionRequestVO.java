/*
 * Created on Jun 9, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PackageConversionRequestVO {

	private String msisdn = "";
	private int changeType;
	private String requestorUserId;
	public static void main(String[] args) {
	}
	/**
	 * @return Returns the changeType.
	 */
	public int getChangeType() {
		return changeType;
	}
	/**
	 * @param changeType The changeType to set.
	 */
	public void setChangeType(int changeType) {
		this.changeType = changeType;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the requestorUserId.
	 */
	public String getRequestorUserId() {
		return requestorUserId;
	}
	/**
	 * @param requestorUserId The requestorUserId to set.
	 */
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
}
