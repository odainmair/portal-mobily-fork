//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

import java.util.List;
import java.util.Map;

/**
 * The base interface of EJBs that use the REST design pattern.
 * It provides CRUD operations to manage backend resources.
 * It contains the basic REST-based methods (get, post, put, delete).
 * It also contains the common services that include retrieval of generic
 * items and creation of pin code.
 */
public interface ResourceManager {

	/**
	 * Retrieves a resource object.
	 * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be retrieved
     * @return the resource object
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
	 */
    public Object get(Context ctx, String uri) throws ServiceException;

    /**
     * Submits a resource creation request.
     * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be created
     * @param data the data to be submitted to the backend
     * @return the created resource object
     * @throws ServiceException when an error occurs while processing the
     * submission request
     * 
     * @see Context
     */
    public Object post(Context ctx, String uri, String data) throws ServiceException;

    /**
     * Updates a resource the with new data.
     * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be updated
     * @param data the data to be submitted to the backend
     * @throws ServiceException when an error occurs while processing the
     * submission request
     * 
     * @see Context
     */
    public void put(Context ctx, String uri, String data) throws ServiceException;

    /**
     * Submits a request to delete a resource.
     * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be deleted
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     */
    public void delete(Context ctx, String uri) throws ServiceException;

    /**
     * Retrieves the list of Country resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findCountry(Context ctx, Map<String, String> params) throws ServiceException;

    /**
     * Retrieves the list of City resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findCity(Context ctx, Map<String, String> params) throws ServiceException;

    /**
     * Retrieves the list of Regions resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findRegions(Context ctx, Map<String, String> params) throws ServiceException;

    /**
     * Retrieves the list of Emirates resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findEmirates(Context ctx, Map<String, String> params) throws ServiceException;

    /**
     * Retrieves the list of Governerate resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findGovernerate(Context ctx, Map<String, String> params) throws ServiceException;

    /**
     * Submits a Pin Code creation request.
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param data the PinCode data to be submitted to the backend
     * @return the new PinCode resource
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see PinCode
     * @see PinCode
     */
    public PinCode createPinCode(Context ctx, PinCode data) throws ServiceException;
}
