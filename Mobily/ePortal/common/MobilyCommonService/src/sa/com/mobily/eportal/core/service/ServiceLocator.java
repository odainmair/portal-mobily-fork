package sa.com.mobily.eportal.core.service;


import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
public class ServiceLocator {
	
	/**
	 * 
	 * @param 
	 * @return 
	 * @throws NamingException 
	 */
	public static Object lookup(String jndiName) throws NamingException {
		InitialContext ic = new InitialContext();
		return ic.lookup(jndiName);
	}

	/**
	 * 
	 * @param jndiName
	 * @return
	 * @throws NamingException
	 */
	public static Destination lookupQueue(Queues jndiName) throws NamingException {
		Destination d = null;
		Object o = lookup(jndiName.toString());
		if (o instanceof Queue)
			d = (Queue)o; 
		else if (o instanceof Topic)
			d = (Topic)o; 
		return d;
	}

	/**
	 * 
	 * @param jndiName
	 * @return
	 * @throws NamingException
	 */
	public static QueueConnectionFactory lookupQueueConnectionFactory(String jndiName) throws NamingException {
		QueueConnectionFactory q = null;
		Object o = lookup(jndiName);
		if (o instanceof QueueConnectionFactory)
			q = (QueueConnectionFactory)o; 
		
		return q;
	}
}
