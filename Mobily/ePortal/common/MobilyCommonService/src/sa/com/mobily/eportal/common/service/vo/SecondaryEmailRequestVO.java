/*
 * Created on Sep 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author abadr
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SecondaryEmailRequestVO {

	private SecondaryEmailRequestHeaderVO header = null;
	private String email = null;
	private String givenName = null;
	private String famiylName = null;
	private String password = null;

	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return Returns the famiylName.
	 */
	public String getFamiylName() {
		return famiylName;
	}
	/**
	 * @param famiylName The famiylName to set.
	 */
	public void setFamiylName(String famiylName) {
		this.famiylName = famiylName;
	}
	/**
	 * @return Returns the givenName.
	 */
	public String getGivenName() {
		return givenName;
	}
	/**
	 * @param givenName The givenName to set.
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	/**
	 * @return Returns the header.
	 */
	public SecondaryEmailRequestHeaderVO getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(SecondaryEmailRequestHeaderVO header) {
		this.header = header;
	}
	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}

