package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRRelatedNumDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersReplyVO;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersReqVO;
import sa.com.mobily.eportal.common.service.xml.MCRRelatedNumberXMLHandler;

/**
 * @author n.gundluru.mit
 *
 */
public class MCRRelatedNumBO {

	 private static final Logger log = LoggerInterface.log;
	 
	/**
	 * Request xml for get related numbers from MCR
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public MCRRelatedNumbersReplyVO getRelatedMsisdnFromMCR(MCRRelatedNumbersReqVO requestVO) throws MobilyCommonException {
        
		MCRRelatedNumbersReplyVO replyVO = null;
            String xmlReply = "";
            MCRRelatedNumberXMLHandler  xmlHandler = new MCRRelatedNumberXMLHandler();
            String xmlMessage  = xmlHandler.generateRelatedNumbersXMLRequest(requestVO);
            
            log.info("MCRRelatedNumBO > getRelatedMsisdnFromMCR > generate xml message for Customer ["+requestVO.getMsisdn()+"] > xmlRequest >"+xmlMessage);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
         	MCRRelatedNumDAO mcrRelDAO = (MCRRelatedNumDAO)daoFactory.getMCRRelatedNumDAO();
         	xmlReply= mcrRelDAO.getRelatedMsisdnFromMCR(xmlMessage);
            log.info("MCRRelatedNumBO > getRelatedMsisdnFromMCR > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > xmlReply >"+xmlReply);
            
            //parsing the xml reply
            replyVO = xmlHandler.parseReleatedMsisdnReply(xmlReply);

        return replyVO;    
    }
}