package sa.com.mobily.eportal.customerAccDetails.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AccountVO extends BaseVO
{
	private static final long serialVersionUID = 1L;
	private String borderNumber = null;
	private String exceptionFlag= null;
	private String fingerprintStatus= null;
	private String accountNumber= null;
	private String EECCBlackList= null;
	private String EECCCustomerCategory= null;
	private String EECCCustomerType= null;
	private String EECCEALanguagePreference= null;
	private String EECCIDDocExpDate= null;
	private String EECCIDDocExpHDate= null;
	private String EECCIDDocIssueDate= null;
	private String EECCIDDocIssueHDate= null;
	private String EECCIDDocIssuePlace= null;
	private String EECCIDDocType= null;
	private String EECCIDNumber = null;
	private String EEMBLAlelmLastSyncStatus= null;
	private String alelmOnlineOverallStatus= null;
	private String updatedByAlElm= null;
	private String householdHeadOccupation= null;
	private String integrationId= null;
	private String location= null;
	private String name= null;
	private String primaryAddressId= null;
	private String primaryContactId= null;
	private String primaryOrganization= null;
	private String listOfCutServiceSubAccounts= null;
	private ListOfAddressVO listOfAddressVO;
	private ListOfContactVO listOfContactVO;
	public String getBorderNumber()
	{
		return borderNumber;
	}
	public void setBorderNumber(String borderNumber)
	{
		this.borderNumber = borderNumber;
	}
	public String getExceptionFlag()
	{
		return exceptionFlag;
	}
	public void setExceptionFlag(String exceptionFlag)
	{
		this.exceptionFlag = exceptionFlag;
	}
	public String getFingerprintStatus()
	{
		return fingerprintStatus;
	}
	public void setFingerprintStatus(String fingerprintStatus)
	{
		this.fingerprintStatus = fingerprintStatus;
	}
	public String getAccountNumber()
	{
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
	public String getEECCBlackList()
	{
		return EECCBlackList;
	}
	public void setEECCBlackList(String eECCBlackList)
	{
		EECCBlackList = eECCBlackList;
	}
	public String getEECCCustomerCategory()
	{
		return EECCCustomerCategory;
	}
	public void setEECCCustomerCategory(String eECCCustomerCategory)
	{
		EECCCustomerCategory = eECCCustomerCategory;
	}
	public String getEECCCustomerType()
	{
		return EECCCustomerType;
	}
	public void setEECCCustomerType(String eECCCustomerType)
	{
		EECCCustomerType = eECCCustomerType;
	}
	public String getEECCEALanguagePreference()
	{
		return EECCEALanguagePreference;
	}
	public void setEECCEALanguagePreference(String eECCEALanguagePreference)
	{
		EECCEALanguagePreference = eECCEALanguagePreference;
	}
	public String getEECCIDDocExpDate()
	{
		return EECCIDDocExpDate;
	}
	public void setEECCIDDocExpDate(String eECCIDDocExpDate)
	{
		EECCIDDocExpDate = eECCIDDocExpDate;
	}
	public String getEECCIDDocExpHDate()
	{
		return EECCIDDocExpHDate;
	}
	public void setEECCIDDocExpHDate(String eECCIDDocExpHDate)
	{
		EECCIDDocExpHDate = eECCIDDocExpHDate;
	}
	public String getEECCIDDocIssueDate()
	{
		return EECCIDDocIssueDate;
	}
	public void setEECCIDDocIssueDate(String eECCIDDocIssueDate)
	{
		EECCIDDocIssueDate = eECCIDDocIssueDate;
	}
	public String getEECCIDDocIssueHDate()
	{
		return EECCIDDocIssueHDate;
	}
	public void setEECCIDDocIssueHDate(String eECCIDDocIssueHDate)
	{
		EECCIDDocIssueHDate = eECCIDDocIssueHDate;
	}
	public String getEECCIDDocIssuePlace()
	{
		return EECCIDDocIssuePlace;
	}
	public void setEECCIDDocIssuePlace(String eECCIDDocIssuePlace)
	{
		EECCIDDocIssuePlace = eECCIDDocIssuePlace;
	}
	public String getEECCIDDocType()
	{
		return EECCIDDocType;
	}
	public void setEECCIDDocType(String eECCIDDocType)
	{
		EECCIDDocType = eECCIDDocType;
	}
	public String getEECCIDNumber()
	{
		return EECCIDNumber;
	}
	public void setEECCIDNumber(String eECCIDNumber)
	{
		EECCIDNumber = eECCIDNumber;
	}
	public String getEEMBLAlelmLastSyncStatus()
	{
		return EEMBLAlelmLastSyncStatus;
	}
	public void setEEMBLAlelmLastSyncStatus(String eEMBLAlelmLastSyncStatus)
	{
		EEMBLAlelmLastSyncStatus = eEMBLAlelmLastSyncStatus;
	}
	public String getAlelmOnlineOverallStatus()
	{
		return alelmOnlineOverallStatus;
	}
	public void setAlelmOnlineOverallStatus(String alelmOnlineOverallStatus)
	{
		this.alelmOnlineOverallStatus = alelmOnlineOverallStatus;
	}
	public String getUpdatedByAlElm()
	{
		return updatedByAlElm;
	}
	public void setUpdatedByAlElm(String updatedByAlElm)
	{
		this.updatedByAlElm = updatedByAlElm;
	}
	public String getHouseholdHeadOccupation()
	{
		return householdHeadOccupation;
	}
	public void setHouseholdHeadOccupation(String householdHeadOccupation)
	{
		this.householdHeadOccupation = householdHeadOccupation;
	}
	public String getIntegrationId()
	{
		return integrationId;
	}
	public void setIntegrationId(String integrationId)
	{
		this.integrationId = integrationId;
	}
	public String getLocation()
	{
		return location;
	}
	public void setLocation(String location)
	{
		this.location = location;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getPrimaryAddressId()
	{
		return primaryAddressId;
	}
	public void setPrimaryAddressId(String primaryAddressId)
	{
		this.primaryAddressId = primaryAddressId;
	}
	public String getPrimaryContactId()
	{
		return primaryContactId;
	}
	public void setPrimaryContactId(String primaryContactId)
	{
		this.primaryContactId = primaryContactId;
	}
	public String getPrimaryOrganization()
	{
		return primaryOrganization;
	}
	public void setPrimaryOrganization(String primaryOrganization)
	{
		this.primaryOrganization = primaryOrganization;
	}
	public String getListOfCutServiceSubAccounts()
	{
		return listOfCutServiceSubAccounts;
	}
	public void setListOfCutServiceSubAccounts(String listOfCutServiceSubAccounts)
	{
		this.listOfCutServiceSubAccounts = listOfCutServiceSubAccounts;
	}
	public ListOfAddressVO getListOfAddressVO()
	{
		return listOfAddressVO;
	}
	public void setListOfAddressVO(ListOfAddressVO listOfAddressVO)
	{
		this.listOfAddressVO = listOfAddressVO;
	}
	public ListOfContactVO getListOfContactVO()
	{
		return listOfContactVO;
	}
	public void setListOfContactVO(ListOfContactVO listOfContactVO)
	{
		this.listOfContactVO = listOfContactVO;
	}
	
}
