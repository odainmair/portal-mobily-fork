package sa.com.mobily.eportal.common.service.lookup.common;

import java.util.List;

import sa.com.mobily.eportal.common.service.lookup.vo.Lookup;

public interface LookupCommonService {
	
	public List<Lookup> findById(String id, boolean isEnglishLocale);
	
	
}
