package sa.com.mobily.eportal.customerinfo.vo;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ConsumerProfileVO extends BaseVO
{
	private static final long serialVersionUID = 1L;
	private String san = null;	
	private String accountNumber =null;
	private String createdDate =null;
	private String updatedDate = null;
	private String accountStatus =null;
	private Timestamp createdTimeStamp = null;
	private byte[] customeProfileData;
	
	/**
	 * @return the san
	 */
	public String getSan()
	{
		return san;
	}
	/**
	 * @param san the san to set
	 */
	public void setSan(String san)
	{
		this.san = san;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the createdDate
	 */
	public String getCreatedDate()
	{
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(String createdDate)
	{
		this.createdDate = createdDate;
	}
	/**
	 * @return the updatedDate
	 */
	public String getUpdatedDate()
	{
		return updatedDate;
	}
	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(String updatedDate)
	{
		this.updatedDate = updatedDate;
	}
	/**
	 * @return the createdTimeStamp
	 */
	public Timestamp getCreatedTimeStamp()
	{
		return createdTimeStamp;
	}
	/**
	 * @param createdTimeStamp the createdTimeStamp to set
	 */
	public void setCreatedTimeStamp(Timestamp createdTimeStamp)
	{
		this.createdTimeStamp = createdTimeStamp;
	}
	/**
	 * @return the customeProfileData
	 */
	public byte[] getCustomeProfileData()
	{
		return customeProfileData;
	}
	/**
	 * @param customeProfileData the customeProfileData to set
	 */
	public void setCustomeProfileData(byte[] customeProfileData)
	{
		this.customeProfileData = customeProfileData;
	}
	/**
	 * @return the accountStatus
	 */
	public String getAccountStatus()
	{
		return accountStatus;
	}
	/**
	 * @param accountStatus the accountStatus to set
	 */
	public void setAccountStatus(String accountStatus)
	{
		this.accountStatus = accountStatus;
	}
	
}
