/**
 * 
 */
package sa.com.mobily.eportal.core.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import oracle.sql.BLOB;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.core.service.Logger;

/**
 * @author anas
 *
 * Table definition is "CREATE TABLE TEMP_BLOBS (ID int, 
		 * NAME varchar2(256), 
		 * CONTENET_TYPE varchar2(64), 
		 * CONTENT BLOB)"
 */
public class TempBlobDAO extends AbstractDBDAO<Object> {
//	private static final String TABLE_NAME = "TEMP_BLOBS";
	private static TempBlobDAO instance = null;
	private static final Logger logger = Logger.getLogger(TempBlobDAO.class);
	
	protected TempBlobDAO() {
		super(DataSources.DEFAULT);
	}

	public static synchronized TempBlobDAO getInstance() {
		if (instance == null) {
			instance = new TempBlobDAO();
		}
		return instance;
	}

	@Override
	protected Object mapDTO(ResultSet rs) throws SQLException {
		throw new UnsupportedOperationException ("Method is not supported for this type of DAO.");
	}

	public void delete(int id) {
		update(DELETE_RECORD, new Object[]{id});
	}
	
	public int add(String name, String contentType, byte[] data) throws SQLException, IOException {
		logger.entering("add");
		Connection c = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int key = 0;
		
		try {
			c = getConnection();
			logger.debug("Setting auto commit to false.");
			c.setAutoCommit(false);
			logger.debug("Get the next sequence number.");
			rs = c.createStatement().executeQuery("select SEQ_TEMP_BLOBS_ID.nextval FROM dual");
			if (rs.next() ) {
				key = rs.getInt(1);
			}
			logger.debug("The current sequence number is [" + key + "]");
			ps = c.prepareStatement("insert into TEMP_BLOBS (ID, NAME, CONTENET_TYPE, CONTENT) values (?, ?,?,empty_blob())");
			ps.setInt(1, key);
			ps.setString(2, name);
			ps.setString(3, contentType);
			logger.debug("Inserting a new record with empty_blob()");
			ps.executeUpdate();
			logger.debug("An empty BLOB has been inserted successfully.");
			
			logger.debug("Selecting the inserted records for update");
			ps = c.prepareStatement("SELECT CONTENT FROM TEMP_BLOBS WHERE ID = ? FOR UPDATE");
			ps.setInt(1, key);
			rs = ps.executeQuery();

            if (rs.next()) {
            	InputStream instream = new ByteArrayInputStream(data);;
            	OutputStream outstream = null;
            	try {
	                BLOB blob = (BLOB)rs.getBlob(1);
	                outstream = blob.getBinaryOutputStream();
	                logger.info("Copying data into the BLOB output stream");
	                copy(instream, outstream, blob.getBufferSize());
	                outstream.flush();
            	} catch(Exception ex) {
            		ex.printStackTrace();
            	} finally {
	                if (instream != null) instream.close();
	                if(outstream != null) outstream.close();
            	}
            }

			logger.debug("Commiting the work.");
			c.commit();
			logger.debug("Work Committed successfully.");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			if  (c != null) c.rollback();
			logger.exiting("add");
			throw e;
		} finally {
			if (rs != null) rs.close();
			if (ps != null) ps.close();
			if (c != null) c.close();
		}
		
		logger.exiting("add");
		
		return key;	
	}

	/**
	 * The method is using buffering to avoid OutOfMemoryError for large files.
	 * @param is
	 * @param os
	 * @throws IOException
	 */
	private static void copy(InputStream is, OutputStream os, final int bufferSize) throws IOException {
		byte[] b = new byte[bufferSize];
		int av=is.available();
		while (bufferSize < av) {
			logger.debug("Copying " +bufferSize + " bytes into BLOB" );
			is.read(b);
			os.write(b);
			av=is.available();
		}
		if (av > 0) {
			logger.debug("Copying " + av + " bytes into BLOB" );
			b = new byte[av];
			is.read(b);
			os.write(b);
		}
	}
//	private static final int BUFFER_SIZE = 4096;
	private static final String DELETE_RECORD = "DELETE FROM TEMP_BLOBS WHERE ID = ?";
	
	
	public static void main(String[] args) throws SQLException, IOException {
		getInstance().add("Test.txt", "text/plain", "Sample text content".getBytes());
		
//		for (int i = 0; i < 20; i++) {
//			getInstance().delete(i);
//		}
	}
	
	
}
