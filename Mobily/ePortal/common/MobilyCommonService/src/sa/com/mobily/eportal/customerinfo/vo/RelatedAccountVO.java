package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.model.UserSubscription;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.vo.BaseVO;
import sa.com.mobily.eportal.customerinfo.constant.CustSegmentConstant;
import sa.com.mobily.eportal.customerinfo.constant.CustomerTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.LineTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.RelatedNumberConstant;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}ServiceAccountNumber"/>
 *         &lt;element ref="{}BillingAccountNumber"/>
 *         &lt;element ref="{}MSISDN"/>
 *         &lt;element ref="{}IDDocType"/>
 *         &lt;element ref="{}IDDocNumber"/>
 *         &lt;element ref="{}CPESerialNumber"/>
 *         &lt;element ref="{}Status"/>
 *         &lt;element ref="{}Type"/>
 *         &lt;element ref="{}PackageID"/>
 *         &lt;element ref="{}Promotion"/>
 *         &lt;element ref="{}PackageName"/>
 *         &lt;element ref="{}CustomerCategory"/>
 *         &lt;element ref="{}ProductLine"/>
 *         &lt;element ref="{}ActivationDate"/>
 *         &lt;element ref="{}VIPStatus"/>         
 *         &lt;element ref="{}VIPTier"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "serviceAccountNumber", "billingAccountNumber", "msisdn", "idDocType", "idDocNumber", "cpeSerialNumber", "status", "type", "packageID",
		"promotion", "packageName", "customerCategory", "productLine", "customerSegment","vipStatus", "vipTier" })
@XmlRootElement(name = "RelatedAccountInfo")
public class RelatedAccountVO extends  BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 612402721461350572L;

	@XmlElement(name = "ServiceAccountNumber", required = true)
	protected String serviceAccountNumber;

	@XmlElement(name = "BillingAccountNumber", required = true)
	protected String billingAccountNumber;

	@XmlElement(name = "MSISDN", required = true)
	protected String msisdn;

	@XmlElement(name = "IDDocType", required = true)
	protected String idDocType;

	@XmlElement(name = "IDDocNumber", required = true)
	protected String idDocNumber;

	@XmlElement(name = "CPESerialNumber", required = true)
	protected String cpeSerialNumber;

	@XmlElement(name = "Status", required = true)
	protected String status;

	@XmlElement(name = "Type", required = true)
	protected String type;

	@XmlElement(name = "PackageID", required = true)
	protected String packageID;

	@XmlElement(name = "Promotion", required = true)
	protected String promotion;

	@XmlElement(name = "PackageName", required = true)
	protected String packageName;

	@XmlElement(name = "CustomerCategory", required = true)
	protected String customerCategory;

	@XmlElement(name = "ProductLine", required = true)
	protected String productLine;

	// Application mapped constants
	@XmlTransient
	private int subscriptionType = -1;

	@XmlTransient
	private int lineType = -1;

	@XmlTransient
	private int customerType = -1;
	
	@XmlElement(name = "CustomerSegment")
	private String customerSegment;
	
	@XmlElement(name = "VIPStatus", required = true)
	protected String vipStatus;
	
	@XmlElement(name = "VIPTier", required = true)
	protected String vipTier;
	

	public int getSubscriptionType()
	{
		if (productLine.equalsIgnoreCase(RelatedNumberConstant.PrePaid) || productLine.equalsIgnoreCase(RelatedNumberConstant.PostPaid))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_GSM;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.WiMax))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_WiMAX;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.Connect))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_CONNECT;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.eLifePostPaidBundle))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.FTTH))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.IPTVBundle))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.LTE))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_CONNECT;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.ELIFE_FIXED_VOICE))//SR22516 - ePortal Fixed Voice
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FV;
		}
		return subscriptionType;
	}

	public void setSubscriptionType(int subscriptionType)
	{
		this.subscriptionType = subscriptionType;
	}

	public int getLineType()
	{
		if (productLine.equalsIgnoreCase(RelatedNumberConstant.PrePaid) || productLine.equalsIgnoreCase(RelatedNumberConstant.PostPaid))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_GSM;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.WiMax))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_WiMAX;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.Connect))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_3G;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.eLifePostPaidBundle))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.FTTH))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.IPTVBundle))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_IPTV;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.LTE))
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_LTE;
		}
		else if (productLine.equalsIgnoreCase(RelatedNumberConstant.ELIFE_FIXED_VOICE))//SR22516 - ePortal Fixed Voice
		{
			this.lineType = LineTypeConstant.SUBS_TYPE_FV;
		}
		return lineType;
	}

	public void setLineType(int lineType)
	{
		this.lineType = lineType;
	}

	public int getCustomerType()
	{
		setType(this.type);
		return customerType;
	}

	public void setCustomerType(int customerType)
	{
		this.customerType = customerType;
	}

	/**
	 * Gets the value of the serviceAccountNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getServiceAccountNumber()
	{
		return serviceAccountNumber;
	}

	/**
	 * Sets the value of the serviceAccountNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setServiceAccountNumber(String value)
	{
		this.serviceAccountNumber = value;
	}

	/**
	 * Gets the value of the billingAccountNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getBillingAccountNumber()
	{
		return billingAccountNumber;
	}

	/**
	 * Sets the value of the billingAccountNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setBillingAccountNumber(String value)
	{
		this.billingAccountNumber = value;
	}

	/**
	 * Gets the value of the msisdn property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getMSISDN()
	{
		return msisdn;
	}

	/**
	 * Sets the value of the msisdn property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setMSISDN(String value)
	{
		this.msisdn = value;
	}

	/**
	 * Gets the value of the idDocType property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDDocType()
	{
		return idDocType;
	}

	/**
	 * Sets the value of the idDocType property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDDocType(String value)
	{
		this.idDocType = value;
	}

	/**
	 * Gets the value of the idDocNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getIDDocNumber()
	{
		return idDocNumber;
	}

	/**
	 * Sets the value of the idDocNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setIDDocNumber(String value)
	{
		this.idDocNumber = value;
	}

	/**
	 * Gets the value of the cpeSerialNumber property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCPESerialNumber()
	{
		return cpeSerialNumber;
	}

	/**
	 * Sets the value of the cpeSerialNumber property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCPESerialNumber(String value)
	{
		this.cpeSerialNumber = value;
	}

	/**
	 * Gets the value of the status property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getStatus()
	{
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setStatus(String value)
	{
		this.status = value;
	}

	/**
	 * Gets the value of the type property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getType()
	{
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setType(String value)
	{
		this.type = value;
		this.customerType = CustomerTypeConstant.getCustomerType(value);
	}

	/**
	 * Gets the value of the packageID property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPackageID()
	{
		return packageID;
	}

	/**
	 * Sets the value of the packageID property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPackageID(String value)
	{
		this.packageID = value;
	}

	/**
	 * Gets the value of the promotion property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPromotion()
	{
		return promotion;
	}

	/**
	 * Sets the value of the promotion property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPromotion(String value)
	{
		this.promotion = value;
	}

	/**
	 * Gets the value of the packageName property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getPackageName()
	{
		return packageName;
	}

	/**
	 * Sets the value of the packageName property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setPackageName(String value)
	{
		this.packageName = value;
	}

	/**
	 * Gets the value of the customerCategory property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getCustomerCategory()
	{
		return customerCategory;
	}

	/**
	 * Sets the value of the customerCategory property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setCustomerCategory(String value)
	{
		this.customerCategory = value;
		if(!FormatterUtility.isEmpty(value))
			if(value.equalsIgnoreCase("Individual"))
				this.customerSegment=CustSegmentConstant.CUST_SEGMENT_CONSUMER;
			else 
				this.customerSegment=CustSegmentConstant.CUST_SEGMENT_BUSINESS;
	}

	/**
	 * Gets the value of the productLine property.
	 * 
	 * @return possible object is {@link String }
	 * 
	 */
	public String getProductLine()
	{
		return productLine;
	}

	/**
	 * Sets the value of the productLine property.
	 * 
	 * @param value
	 *            allowed object is {@link String }
	 * 
	 */
	public void setProductLine(String value)
	{
		this.productLine = value;
		if (value.equalsIgnoreCase(RelatedNumberConstant.PrePaid) || value.equalsIgnoreCase(RelatedNumberConstant.PostPaid))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_GSM;
			this.lineType = LineTypeConstant.SUBS_TYPE_GSM;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.WiMax))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_WiMAX;
			this.lineType = LineTypeConstant.SUBS_TYPE_WiMAX;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.Connect))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_CONNECT;
			this.lineType = LineTypeConstant.SUBS_TYPE_3G;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.eLifePostPaidBundle))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
			this.lineType = LineTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.FTTH))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
			this.lineType = LineTypeConstant.SUBS_TYPE_FTTH;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.IPTVBundle))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
			this.lineType = LineTypeConstant.SUBS_TYPE_IPTV;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.LTE))
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_CONNECT;
			this.lineType = LineTypeConstant.SUBS_TYPE_LTE;
		}
		else if (value.equalsIgnoreCase(RelatedNumberConstant.ELIFE_FIXED_VOICE))//SR22516 - ePortal Fixed Voice
		{
			this.subscriptionType = SubscriptionTypeConstant.SUBS_TYPE_FV;
			this.lineType = LineTypeConstant.SUBS_TYPE_FV;
		}
	}
	
	
	
	
	public String getCustomerSegment()
	{
		return customerSegment;
	}

	public void setCustomerSegment(String customerSegment)
	{
		this.customerSegment = customerSegment;
	}
	
	
	public String getVipStatus()
	{
		return vipStatus;
	}

	public void setVipStatus(String vipStatus)
	{
		this.vipStatus = vipStatus;
	}

	public String getVipTier()
	{
		return vipTier;
	}

	public void setVipTier(String vipTier)
	{
		this.vipTier = vipTier;
	}

	public boolean isEqualUserSub(UserSubscription userSubscription){
		
		if (userSubscription.getSubscriptionType().intValue()==this.getSubscriptionType())
			if(userSubscription.getNationalIdOrIqamaNumber().equalsIgnoreCase(this.getIDDocNumber()))
				 if(userSubscription.getSubscriptionType().intValue()==SubscriptionTypeConstant.SUBS_TYPE_GSM){
					 if(userSubscription.getMsisdn().equals(this.getMSISDN()))
						 return true;
				 }
				 else if(userSubscription.getServiceAcctNumber().equals(this.getServiceAccountNumber()))
						   return true;
		return false;
	}
	
}