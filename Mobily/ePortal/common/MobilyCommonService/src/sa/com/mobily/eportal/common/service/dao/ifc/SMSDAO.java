/*
 * Created on May 15, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.sms.vo.SMSVO;


/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface SMSDAO
{
    public void sendSMS( String aRequst ) throws MobilyCommonException;
    public void sendSMS( SMSVO smsVO ) throws MobilyCommonException;
}
