package sa.com.mobily.eportal.common.service.vo;


/**
 * 
 * @author n.gundluru.mit
 *
 */
public class PaymentNotifyRequestVO {

	private String requestorUserId;
	private String referenceNumber;
	private String dealerID;
	private String shopID;
	private String agentID;
	private String paymentType;
	private String paymentMode;
	private String accountNumber;
	private String billNumber;
	private String lineNumber;
	private String amount;
	private String paymentDateTime;
	private String comments;
	private String paymentReason;
	private String docIDNo;
	private String discountAmount;
	private String terminalID;
	private String statusCode;
	private String cardNumber;
	private String authorizationCode;
	private String rrn;
	private String cardSchemes;
	private String serviceId;
	private String channel = null;
	private String language = null;
	private String customerName = null;
	private String docIDType = null;
	
	
	
	/**
	 * @return the customerName
	 */
	public String getCustomerName()
	{
		return customerName;
	}
	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName)
	{
		this.customerName = customerName;
	}
	/**
	 * @return the docIDType
	 */
	public String getDocIDType()
	{
		return docIDType;
	}
	/**
	 * @param docIDType the docIDType to set
	 */
	public void setDocIDType(String docIDType)
	{
		this.docIDType = docIDType;
	}
	/**
	 * @return the channel
	 */
	public String getChannel()
	{
		return channel;
	}
	/**
	 * @param channel the channel to set
	 */
	public void setChannel(String channel)
	{
		this.channel = channel;
	}
	/**
	 * @return the language
	 */
	public String getLanguage()
	{
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language)
	{
		this.language = language;
	}
	/**
	 * @return the requestorUserId
	 */
	public String getRequestorUserId() {
		return requestorUserId;
	}
	/**
	 * @param requestorUserId the requestorUserId to set
	 */
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	/**
	 * @return the dealerID
	 */
	public String getDealerID() {
		return dealerID;
	}
	/**
	 * @param dealerID the dealerID to set
	 */
	public void setDealerID(String dealerID) {
		this.dealerID = dealerID;
	}
	/**
	 * @return the shopID
	 */
	public String getShopID() {
		return shopID;
	}
	/**
	 * @param shopID the shopID to set
	 */
	public void setShopID(String shopID) {
		this.shopID = shopID;
	}
	/**
	 * @return the agentID
	 */
	public String getAgentID() {
		return agentID;
	}
	/**
	 * @param agentID the agentID to set
	 */
	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}
	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}
	/**
	 * @param paymentType the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}
	/**
	 * @param paymentMode the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the billNumber
	 */
	public String getBillNumber() {
		return billNumber;
	}
	/**
	 * @param billNumber the billNumber to set
	 */
	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}
	/**
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the paymentDateTime
	 */
	public String getPaymentDateTime() {
		return paymentDateTime;
	}
	/**
	 * @param paymentDateTime the paymentDateTime to set
	 */
	public void setPaymentDateTime(String paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the paymentReason
	 */
	public String getPaymentReason() {
		return paymentReason;
	}
	/**
	 * @param paymentReason the paymentReason to set
	 */
	public void setPaymentReason(String paymentReason) {
		this.paymentReason = paymentReason;
	}
	/**
	 * @return the docIDNo
	 */
	public String getDocIDNo() {
		return docIDNo;
	}
	/**
	 * @param docIDNo the docIDNo to set
	 */
	public void setDocIDNo(String docIDNo) {
		this.docIDNo = docIDNo;
	}
	/**
	 * @return the discountAmount
	 */
	public String getDiscountAmount() {
		return discountAmount;
	}
	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(String discountAmount) {
		this.discountAmount = discountAmount;
	}
	/**
	 * @return the terminalID
	 */
	public String getTerminalID() {
		return terminalID;
	}
	/**
	 * @param terminalID the terminalID to set
	 */
	public void setTerminalID(String terminalID) {
		this.terminalID = terminalID;
	}
	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}
	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	/**
	 * @return the authorizationCode
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	/**
	 * @param authorizationCode the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	/**
	 * @return the rrn
	 */
	public String getRrn() {
		return rrn;
	}
	/**
	 * @param rrn the rrn to set
	 */
	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
	/**
	 * @return the cardSchemes
	 */
	public String getCardSchemes() {
		return cardSchemes;
	}
	/**
	 * @param cardSchemes the cardSchemes to set
	 */
	public void setCardSchemes(String cardSchemes) {
		this.cardSchemes = cardSchemes;
	}
	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}
	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
}