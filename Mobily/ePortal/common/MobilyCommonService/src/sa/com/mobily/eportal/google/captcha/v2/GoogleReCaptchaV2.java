package sa.com.mobily.eportal.google.captcha.v2;

import sa.com.mobily.eportal.google.captcha.v2.vo.CaptchaResponseVO;
import sa.com.mobily.eportal.google.captcha.v2.vo.GoogleReCaptchaV2VO;

public interface GoogleReCaptchaV2{
	public CaptchaResponseVO validateCaptcha(GoogleReCaptchaV2VO googleReCaptchaV2VO);
}
