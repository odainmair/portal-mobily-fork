package sa.com.mobily.eportal.common.service.accountActivation.model;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AccountActivation extends BaseVO {

	private static final long serialVersionUID = 7470219788607684567L;
	
	private Long id;
    private Long userAcctId;
    private Long activationCode;
    private Integer project_id;
    private Timestamp reqDate;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getUserAcctId() {
        return userAcctId;
    }
    public void setUserAcctId(Long userAcctId) {
        this.userAcctId = userAcctId;
    }
    public Long getActivationCode() {
        return activationCode;
    }
    public void setActivationCode(Long activationCode) {
        this.activationCode = activationCode;
    }
    public Integer getProject_id() {
        return project_id;
    }
    public void setProject_id(Integer project_id) {
        this.project_id = project_id;
    }
    public Timestamp getReqDate() {
        return reqDate;
    }
    public void setReqDate(Timestamp reqDate) {
        this.reqDate = reqDate;
    }
}