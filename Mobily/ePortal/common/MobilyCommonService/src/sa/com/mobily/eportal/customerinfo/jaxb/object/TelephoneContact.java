/**
 * 
 */
package sa.com.mobily.eportal.customerinfo.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * @author v.ravipati.mit
 *
 */


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "number"    
})
@XmlRootElement(name = "TelephoneContact")
public class TelephoneContact{
	@XmlElement(name = "Number", required = true)
	protected String number;

	public String getNumber()
	{
		return number;
	}

	public void setNumber(String number)
	{
		this.number = number;
	}

}
