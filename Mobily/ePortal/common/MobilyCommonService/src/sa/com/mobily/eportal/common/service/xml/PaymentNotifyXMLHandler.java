package sa.com.mobily.eportal.common.service.xml;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CustomerTypeVO;
import sa.com.mobily.eportal.common.service.vo.MessageHeaderVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyReplyVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyRequestVO;

/**
 * @author n.gundluru.mit
 */
public class PaymentNotifyXMLHandler {
    
	private static final Logger logger = LoggerInterface.log;

    /**
     * Generate the XML request for Payment Notification Request
     * 
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     */
    public String generateXMLRequest(PaymentNotifyRequestVO requestVO) throws MobilyCommonException {
        String requestMessage = "";
        try {
            Document doc = new DocumentImpl();

            Element root = doc.createElement(TagIfc.MAIN_EAI_TAG_NAME);
            Element header = XMLUtility.openParentTag(doc,TagIfc.EAI_HEADER_TAG);
            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_FORMAT,ConstantIfc.PAYMENT_NOTIFY_MESSAGE_FORMAT_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.MSG_VER_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, FormatterUtility.isEmpty(requestVO.getChannel()) ? ConstantIfc.CHANNEL_ID_VALUE_EPOR : requestVO.getChannel());
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_FUNCTION,ConstantIfc.CHANNEL_FUNC_VALUE_PMTNOT);
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,FormatterUtility.checkNull(requestVO.getRequestorUserId()));
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_SECURITY_INFO,ConstantIfc.SECURITY_INFO_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.RETURN_CODE_VALUE);
            XMLUtility.closeParentTag(root, header);
            
            //Body
            
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_REFERENCE_NUMBER, FormatterUtility.checkNull(requestVO.getReferenceNumber()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_EI_DealerId, FormatterUtility.checkNull(requestVO.getDealerID()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_EI_ShopId, FormatterUtility.checkNull(requestVO.getShopID()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_EI_AgentId, FormatterUtility.checkNull(requestVO.getAgentID()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_PAYMENT_TYPE, FormatterUtility.checkNull(requestVO.getPaymentType()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_PAYMENT_MODE, FormatterUtility.checkNull(requestVO.getPaymentMode()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ACCOUNT_NUMBER, FormatterUtility.checkNull(requestVO.getAccountNumber()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_BILL_NUMBER, FormatterUtility.checkNull(requestVO.getBillNumber()));
            XMLUtility.generateElelemt(doc, root, TagIfc.LINE_NUMBER, FormatterUtility.checkNull(requestVO.getLineNumber()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_Amount, FormatterUtility.checkNull(requestVO.getAmount()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_PaymentDateTime, FormatterUtility.checkNull(requestVO.getPaymentDateTime()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_COMMENTS, FormatterUtility.checkNull(requestVO.getComments()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_PAYMENT_REASON, FormatterUtility.checkNull(requestVO.getPaymentReason()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_DOC_ID_NUM, FormatterUtility.checkNull(requestVO.getDocIDNo()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_DOC_ID_TYPE, FormatterUtility.checkNull(requestVO.getDocIDType()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_CUSTOMER_NAME, FormatterUtility.checkNull(requestVO.getCustomerName()));
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_DISC_AMOUNT, FormatterUtility.checkNull(requestVO.getDiscountAmount()));
            
            Element posheader = XMLUtility.openParentTag(doc,TagIfc.TAG_POS_INFO);
            	XMLUtility.generateElelemt(doc, posheader, TagIfc.TAG_TERMINAL_ID, FormatterUtility.checkNull(requestVO.getTerminalID()));
            	XMLUtility.generateElelemt(doc, posheader, TagIfc.TAG_StatusCode, FormatterUtility.checkNull(requestVO.getStatusCode()));
            	XMLUtility.generateElelemt(doc, posheader, TagIfc.TAG_CARD_NUMBER, FormatterUtility.checkNull(requestVO.getCardNumber()));
            	XMLUtility.generateElelemt(doc, posheader, TagIfc.TAG_AUTH_CODE, FormatterUtility.checkNull(requestVO.getAuthorizationCode()));
            	XMLUtility.generateElelemt(doc, posheader, TagIfc.TAG_RRN, FormatterUtility.checkNull(requestVO.getRrn()));
            	XMLUtility.generateElelemt(doc, posheader, TagIfc.TAG_CARD_SCHEMES, FormatterUtility.checkNull(requestVO.getCardSchemes()));
            XMLUtility.closeParentTag(root, posheader);

            doc.appendChild(root);
            
            requestMessage = XMLUtility.serializeRequest(doc);
        } catch (java.io.IOException e) {
            logger.fatal("PaymentNotifyXMLHandler > generateXMLRequest >IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
        	logger.fatal("PaymentNotifyXMLHandler > generateXMLRequest > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        
        return requestMessage;
    }

    
    /**
     * Parsing the Payment Notification XML Reply
     * 
     * <EE_EAI_MESSAGE>
	    <EE_EAI_HEADER>
	         <MsgFormat>Payment Notification</MsgFormat> 
	         <MsgVersion>01</MsgVersion> 
	         <RequestorChannelId>MPAY</RequestorChannelId> 
	        <RequestorChannelFunction>PMTNOT</RequestorChannelFunction> 
	        <RequestorUserId>PMTNOT</RequestorUserId> 
	        <RequestorLanguage>E</RequestorLanguage> 
	        <RequestorSecurityInfo>secure</RequestorSecurityInfo> 
	        <ReturnCode>0000</ReturnCode> 
	     </EE_EAI_HEADER>
	     <ReferenceNumber>43981 35029 08871 18405</ReferenceNumber> 
	     <PartnerReferenceNumber>MOBILY6014</PartnerReferenceNumber> 
	  </EE_EAI_MESSAGE>
     * @param xmlMessage
     * 
     */
    public PaymentNotifyReplyVO parsingXMLRreply(String xmlReplyMessage){
    	PaymentNotifyReplyVO replyVo = null;
        
    	if (FormatterUtility.isEmpty(xmlReplyMessage))
            return replyVo;
        
    	try {
        	replyVo = new PaymentNotifyReplyVO();

            Document doc = XMLUtility.parseXMLString(xmlReplyMessage);

            Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME).item(0);

            NodeList nl = rootNode.getChildNodes();
            for (int j = 0; j < nl.getLength(); j++) {
                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                Element l_Node = (Element) nl.item(j);
                String p_szTagName = l_Node.getTagName();
                String l_szNodeValue = null;
                if (l_Node.getFirstChild() != null)
                    l_szNodeValue = FormatterUtility.checkNull(l_Node.getFirstChild().getNodeValue());

                if (p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG)) {
                    MessageHeaderHandler headerParser = new MessageHeaderHandler();
                    MessageHeaderVO headerObj = headerParser.parsingXMLHeaderReplyMessage(l_Node);
                    
                    replyVo.setErrorCode(headerObj.getReturnCode());
                    	/*if (!headerObj.getReturnCode().equals(ConstantIfc.RETURN_CODE_VALUE)) {
                    		throw new MobilyCommonException(headerObj.getReturnCode());
                    	}*/
                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_REFERENCE_NUMBER)) {
               		replyVo.setReferenceNumber(l_szNodeValue);
                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PARTNER_REF_NUMBER)) {
               		replyVo.setPartnerRefNumber(l_szNodeValue);
                } 
            }
        } catch (Exception e) {
            logger.fatal("PaymentNotifyXMLHandler parsingXMLRreply > > Exception Occured When pasing Xml Customer Type Request , " + e);
        }
        logger.info("PaymentNotifyXMLHandler > parsingXMLRreply > done");
        return replyVo;

    }

    public static void main(String[] args) {
    	String xx="<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>HLR.INQUIRY.REQUEST</MsgFormat><MsgVersion>0001</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId>IUCTestingUser</RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0000</ReturnCode></EE_EAI_HEADER><AccountNumber>1000112462088816</AccountNumber><UserGender>M</UserGender><LineNumber>966560986656</LineNumber><PackageId>1212</PackageId><CustomerType>2</CustomerType><PackageDescription>Business 500 Employee Program</PackageDescription><CustomerCategory>Consumer</CustomerCategory><CreationTime>03102012154657</CreationTime><IsMix>N</IsMix><CorporatePackage>No</CorporatePackage><IsNewControl>N</IsNewControl><IsCorporateKA>No</IsCorporateKA><CustomerBSLId>1-5Q3LT9S</CustomerBSLId><IDType>Passport</IDType><IDNumber>552211</IDNumber><IS_IUC>N</IS_IUC><ISMSIM>N</ISMSIM><PackageServiceType>GSM</PackageServiceType></EE_EAI_MESSAGE>";
    	CustomerTypeXMLHandler customer = new  CustomerTypeXMLHandler();
    	try {
			CustomerTypeVO customert= customer.parsingXMLRreply(xx);
//			System.out.println(customert.isIUC());
			
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
    	
    }
}
