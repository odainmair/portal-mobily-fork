package sa.com.mobily.eportal.common.service.emergencyCredit;

import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.service.emergencyCredit.dao.EmergencyCreditDAO;
import sa.com.mobily.eportal.common.service.emergencyCredit.vo.EmergencyCreditVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

public class EmergencyCreditService
{

	EmergencyCreditDAO emergencyCreditDAO = EmergencyCreditDAO.getInstance();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("MobilyCommonService/commonServices");
	private static String clazz = EmergencyCreditService.class.getName();
	
	public List<EmergencyCreditVO> getEmergencyCreditDetails(){
		List<EmergencyCreditVO> emergencyCreditVOs = null;
		executionContext.log(Level.INFO, "EmergencyCreditService started >emergencyCreditVOs", clazz, "getEmergencyCreditDetails");
		emergencyCreditVOs = emergencyCreditDAO.getEmergencyCreditDetails();
		
		executionContext.log(Level.INFO, "emergencyCreditVOs size: "+emergencyCreditVOs.size(), clazz, "getEmergencyCreditDetails");
		return emergencyCreditVOs;
	}
	
}
