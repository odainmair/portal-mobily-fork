package sa.com.mobily.eportal.common.service.mq.dao;

import java.sql.ResultSet;

import java.sql.SQLException;

import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

import java.net.InetAddress;
import java.net.UnknownHostException;
import com.ibm.websphere.runtime.ServerName;

public class MQAuditDAO extends AbstractDBDAO<MQAuditVO>{
	 
	private static String serverHostAndNode = "";
	
	static {
		try {
			InetAddress ip;
			ip = InetAddress.getLocalHost();
			serverHostAndNode=ServerName.getFullName().toString(); 
		  } catch (UnknownHostException e) {
			  e.printStackTrace();
		  }
	}

	/*	public static final String INSERT_MQ_REQUEST = "INSERT INTO SR_MQ_AUDIT(REQUEST_QUEUE_NAME, REPLY_QUEUE_NAME, MESAGE, CREATED_DATE, REPLY_MESSAGE, ERROR_MESSAGE,WAITING_TIME_SEC) "+
    "VALUES(?, ?, ?, CURRENT_TIMESTAMP, ?, ?,?)";
	public static final String INSERT_MQ_REQUEST = "INSERT INTO SR_MQ_AUDIT_TBL(REQUEST_QUEUE_NAME, REPLY_QUEUE_NAME, MESAGE, CREATED_DATE, REPLY_MESSAGE, ERROR_MESSAGE, WAITING_TIME_SEC, ENVIRONMENT_ID) "+
		    "VALUES(?, ?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?)";*/
	//Change for PBI000000003457 - End
	public static final String INSERT_MQ_REQUEST = "INSERT INTO SR_MQ_AUDIT_TBL(REQUEST_QUEUE_NAME, REPLY_QUEUE_NAME, MESAGE, CREATED_DATE, REPLY_MESSAGE, ERROR_MESSAGE, WAITING_TIME_SEC, ENVIRONMENT_ID,ACCOUNT_NUMBER,USER_NAME,FUNCTION_NAME,SERVICE_ERROR) "+
		    "VALUES(?, ?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?,?,?,?,?)";
	private static MQAuditDAO instance = new MQAuditDAO();
	
	
	
	protected MQAuditDAO() {
		super(DataSources.DEFAULT);
	}
	
	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>ContactDAO</code>
	 * @author Yousef Alkhalaileh
	 */
	public static MQAuditDAO getInstance() {
		return instance;
	}
	
	/**
	 * This method saves new mq request for a specific queue
	 * 
	 * @param request <code>MQAudit</code> mq request to be saved
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Yousef Alkhalaileh
	 */
	public int saveMQRequest(MQAuditVO mqRequest){
		
		//Change for PBI000000003457 - Start
				
		/*return update(INSERT_MQ_REQUEST, mqRequest.getRequestQueue(), mqRequest.getReplyQueue(), mqRequest.getMessage(), mqRequest.getReply(), mqRequest.getErrorMessage(),mqRequest.getWaitingTime());*/
		//return update(INSERT_MQ_REQUEST, mqRequest.getRequestQueue(), mqRequest.getReplyQueue(), mqRequest.getMessage(), mqRequest.getReply(), mqRequest.getErrorMessage(),mqRequest.getWaitingTime(), serverHostAndNode); 
		//Change for PBI000000003457 - End
		return update(INSERT_MQ_REQUEST, mqRequest.getRequestQueue(), mqRequest.getReplyQueue(), mqRequest.getMessage(), mqRequest.getReply(), mqRequest.getErrorMessage(),mqRequest.getWaitingTime(), serverHostAndNode,mqRequest.getMsisdn(),mqRequest.getUserName(),mqRequest.getFunctionName(),mqRequest.getServiceError());
	}
	
	@Override
	protected MQAuditVO mapDTO(ResultSet rs) throws SQLException {
		return null;
	}
}
