package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.AccountFlags;

public class AccountFlagsDAO extends AbstractDBDAO<AccountFlags>
{
	private static AccountFlagsDAO instance = new AccountFlagsDAO();

	private static final String INSERT_ACCOUNT_FLAG = "INSERT INTO ACCOUNT_FLAGS_CORP_TBL (ACCOUNT_ID,FLAG,VALUE) values (?,?,?)";

	protected AccountFlagsDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static AccountFlagsDAO getInstance()
	{
		return instance;
	}

	public int saveCorporateUserFlag(AccountFlags flag)
	{
		return update(INSERT_ACCOUNT_FLAG,flag.getACCOUNT_ID(),flag.getFLAG(),flag.getVALUE());
	}

	@Override
	protected AccountFlags mapDTO(ResultSet rs) throws SQLException
	{
		AccountFlags accountFlags = new AccountFlags();
		accountFlags.setACCOUNT_ID(rs.getString("ACCOUNT_ID"));
		accountFlags.setFLAG(rs.getString("FLAG"));
		accountFlags.setVALUE(rs.getString("VALUE"));
		return accountFlags;
	}
}