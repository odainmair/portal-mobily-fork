/**
 * @date: Dec 28, 2011
 * @author: s.vathsavai.mit
 * @file name: LoyaltyDAO.java
 * @description:  
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

/**
 * @author s.vathsavai.mit
 *
 */
public interface LoyaltyDAO {

	/**
	 * @date: Dec 28, 2011
	 * @author: s.vathsavai.mit
	 * @description: Responsible to do the Neqaty Plus inquiry with Loyalty System
	 * @param strXMLRequest
	 * @return
	 */
	public String doNeqatyPlusCorporateLevelInquiry(String strXMLRequest);
	
	
}
