/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.portlet.PortletRequest;

import org.apache.log4j.Logger;

import com.ibm.ejs.models.base.resources.env.ResourceEnvironmentProvider;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.facade.MobilyCommonFacade;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MobilyUtility {
    private static final Logger log = LoggerInterface.log;
    private static ResourceBundle bundle  = null;
    private static ResourceBundle mqBundle  = null;
    private static ResourceBundle mqJMSBundle  = null;
	public static final String UNIQUE_ID = "ePortalUniqueSystemId";
	private static long id;
	 
	 static{
		 Random random = new Random();
		 id = Math.abs(random.nextInt());
	 }
	
	 public static synchronized String getUniqueID() {
	     return System.currentTimeMillis() + "" + id++;
	 }
	 
	 public boolean isContainSpecialCharacter(String str){
		 

//		 String SCRIPT_REGEX = "/<script\b[^<]*(?:(?!<\\/script>)<[^<]*)*<\\/script>/gi";
//		
//		 Pattern pattern = Pattern.compile(SCRIPT_REGEX);
//		 Matcher matcher = pattern.matcher(str);

		 String SPECIAL_REGEX =	"/[`~!@#$%^&*()_|+\\-=?;:'\",.<>\\{\\}\\[\\]\\\\/]/gi";
		 Pattern pattern1 = Pattern.compile(SPECIAL_REGEX);
		 Matcher matcher1 = pattern1.matcher(str);
		 
		 return matcher1.find();
	 }
	 
	public static String getPropertyValue(String key){
	    log.debug("MobilyUtility > getPropertyValue > For key = ["+key+"]");
	    String value = "";
		try{
		    if(bundle  == null)
		    	{
		        	log.debug("MobilyUtility > getPropertyValue > The Properties file not loaded before , start the loading process now");
		        	bundle =  ResourceBundle.getBundle("dbconfig");
		    	}
		    if(ConstantIfc.EMAIL_SMTP_HOST_NAME.equals(key)){
		    	value = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME);
		    	log.fatal("MobilyUtility > getPropertyValue > key::"+ConstantIfc.EMAIL_SMTP_HOST_NAME+" value::"+value);
		    }else{
		    	value = bundle.getString(key);
		    }
		    
		}catch(Exception e){
		    log.fatal("MobilyUtility > getPropertyValue >Exception > "+e.getMessage());
			throw new SystemException(e);	
		}
	  return value;
	}
    
	/**
	 * This method is added by r.agarwal.mit on 12/11/2009 to be used for PANDA project
	 * @param key
	 * @return
	 */
	public static String getMQConfigPropertyValue(String key){
	    log.debug("MobilyUtility > getMQConfigPropertyValue > For key = ["+key+"]");
	    String value = "";
		try{
		    if(mqBundle  == null)
		    	{
		        	log.debug("MobilyUtility > getMQConfigPropertyValue > The Properties file not loaded before , start the loading process now");
		        	mqBundle =  ResourceBundle.getBundle("mq_config");
		    	}
		    value = mqBundle.getString(key);
		}catch(Exception e){
		    log.fatal("MobilyUtility > getMQConfigPropertyValue >Exception > "+e.getMessage());
			throw new SystemException(e);	
		}
	  return value;
	}
	
	public static String getMQJMSConfigPropertyValue(String key){
	    log.debug("MobilyUtility > getMQJMSConfigPropertyValue > For key = ["+key+"]");
	    String value = "";
		try{
		    if(mqJMSBundle  == null)
		    	{
		        	log.debug("MobilyUtility > getMQJMSConfigPropertyValue > The Properties file not loaded before , start the loading process now");
		        	mqJMSBundle =  ResourceBundle.getBundle("MobilyNativeJMS");
		    	}
		    value = mqJMSBundle.getString(key);
		}catch(Exception e){
		    log.fatal("MobilyUtility > getMQJMSConfigPropertyValue >Exception > "+e.getMessage());
			throw new SystemException(e);	
		}
	  return value;
	}
	
	
	public static void main(String ar[]) {
//		MobilyUtility mobilyUtility = new MobilyUtility();
//		boolean isContainSpecialCharacters = mobilyUtility.isContainSpecialCharacter("*"); 
//		System.out.println("isContain ? " + isContainSpecialCharacters);

		ResourceBundle resourceBundle = ResourceBundle.getBundle("test", Locale.ENGLISH );
		
		
		 //String SPECIAL_REGEX =	"/[`~!@#$%^&*()_|+\\-=?;:'\",.<>\\{\\}\\[\\]\\\\\\/]/gi";
		 String SPECIAL_REGEX = resourceBundle.getString("reg");
//		 System.out.println(SPECIAL_REGEX);
	
		 
		 Pattern pattern1 = Pattern.compile(SPECIAL_REGEX);
		 Matcher matcher1 = pattern1.matcher("&");
		 
//		 System.out.println(matcher1.find());
		 
	}
	
	public static String FormateDate(Date date){
		String formatedDate = "";
		SimpleDateFormat sformat=new SimpleDateFormat("yyyyMMddHHmmss");
		formatedDate=sformat.format(date);
		return formatedDate;
	}
	
	public static String getCalendarDate(){
    	Calendar date = Calendar.getInstance();
        SimpleDateFormat objSimpleDateFormat = new SimpleDateFormat(ConstantIfc.DATE_FORMAT);
        String dateStr = objSimpleDateFormat.format(date.getTime());
        return dateStr;
        
	}
	
	public static String FormateDatebyFormate(Date date, String Formate) throws Exception{
		String formatedDate = "";
			if(date != null){
			try{
			SimpleDateFormat sformat=new SimpleDateFormat(Formate);
			formatedDate=sformat.format(date);
			}catch(Exception e){
				log.debug("Formate date to String Failed ........."+e.getMessage());
				throw e;
			}
		}
		return formatedDate;
	}

	
	public static Date FormateStringToDate(String strDate){
		Date formatedDate = null;
		SimpleDateFormat sformat=new SimpleDateFormat("yyyyMMddHHmmss");
		   try
	        {
		   	formatedDate= sformat.parse(strDate);
	        }
	        catch(ParseException tempParseException)
	        {
	        	sformat=new SimpleDateFormat("yyyyMMdd");
	        	try {
					formatedDate= sformat.parse(strDate);
				} catch (ParseException e) {
				}
	        }
		return formatedDate;
	}
	
	 
	/**
	 * Method 		:	checkNull
	 * Description	:	Checks for nullability of String object and returns empty string if null
	 * @param value String data
	 * @return value Returns the value - String data
	*/
	public static String checkNull(String value) {
		if(value == null || "null".equalsIgnoreCase(value)) {
			value = "";						
		}
		return value.trim();
	}
	
    public static Timestamp getTimestampDate(String date) {
        
        int day = Integer.parseInt(date.substring(0,date.indexOf(":")));
    	String schDateAfterDay = date.substring(date.indexOf(":") + 1);
    	
    	int month = Integer.parseInt(schDateAfterDay.substring(0,schDateAfterDay.indexOf(":")));
    	String schDateAfterMonth = schDateAfterDay.substring(schDateAfterDay.indexOf(":") + 1);
    	
    	int year = Integer.parseInt(schDateAfterMonth.substring(0,schDateAfterMonth.indexOf(";")));
    	String schDateAfterYear = schDateAfterMonth.substring(schDateAfterMonth.indexOf(";")+2);
    	
    	int hours = Integer.parseInt(schDateAfterYear.substring(0,schDateAfterYear.indexOf(":")));
    	String schDateAfterHour = schDateAfterYear.substring(schDateAfterYear.indexOf(":") + 1);
    	
    	int mins = Integer.parseInt(schDateAfterHour.substring(0,schDateAfterHour.indexOf(";")));
    	String meridianTime = schDateAfterHour.substring(schDateAfterHour.indexOf(";") + 2);

		/*if(ConstantIfc.PM.equalsIgnoreCase(meridianTime)) {
			hours = hours + 12;
		}*/
		
		month = month - 1;
    	year = year - 1900;
    	Timestamp timeStamp = new Timestamp(year,month,day,hours,mins,0,0);
    	
    	return timeStamp;
    	
    }
    
	public static String getHostName(String hostIP) {
		log.info("MobilyUtility > getHostName : hostIP = "+hostIP);
		if(FormatterUtility.isEmpty(hostIP)) //default set to production
			hostIP = getPropertyValue(ConstantIfc.PRODUCTION_HOST);
		
		if(hostIP.indexOf(":") != -1)
			hostIP = hostIP.substring(0,hostIP.indexOf(":"));
		
		String hostName = getPropertyValue(ConstantIfc.PRODUCTION_HOST); //default production
		String integrationIP = getPropertyValue(ConstantIfc.INTEGRATION_IP_ADDRESS);
		String stagingIP = getPropertyValue(ConstantIfc.STAGING_IP_ADDRESS);
		
		if(integrationIP != null && integrationIP.contains(hostIP))
			hostName = getPropertyValue(ConstantIfc.INTEGRATION_HOST);
		else if(stagingIP != null && stagingIP.contains(hostIP))
			hostName = getPropertyValue(ConstantIfc.STAGING_HOST);
		
		log.info("MobilyUtility > getHostName : hostName = "+hostName);
		return hostName;
	}
	
	/**
	 * @date: Mar 8, 2012
	 * @author: s.vathsavai.mit
	 * @description: 
	 * @param strPageUniqueName
	 * @return boolean
	 */
	public boolean isPageRequiredResetPortlet(String strPageUniqueName) {
		log.info("MobilyUtility : isPageRequiredResetPortlet : start :strPageUniqueName="+strPageUniqueName);
		boolean isRequired = false;
		if(!FormatterUtility.isEmpty(strPageUniqueName)) {
			List pagesList = MobilyCommonFacade.getResetPortletRequiredPages();
			if(pagesList != null && pagesList.contains(strPageUniqueName))
				isRequired = true;
		}
		log.info("MobilyUtility : isPageRequiredResetPortlet : end :isRequired="+isRequired);
		return isRequired;
	}
	
	/**
	 * @date: Oct 30, 2012
	 * @author: r.agarwal.mit
	 * @description: 
	 * @param InputStream
	 * @return String
	 */
	public static String convertIStoString(InputStream is) {
		InputStreamReader isr = null;
		StringBuilder sb = null;
		BufferedReader br = null;
		try {
			isr = new InputStreamReader(is);
			sb = new StringBuilder();
			br = new BufferedReader(isr);
			String read;
			read = br.readLine();
			while (read != null) {
				sb.append(read);
				read = br.readLine();
			}
		} catch (IOException e) {
			log.fatal("MobilyUtility > convertIStoString > IOException::" + e.getMessage());
		} finally {
			try {
				if(br != null)
					br.close();
				if(is != null)
					is.close();
			} catch (IOException e) {
				log.fatal("MobilyUtility > convertIStoString > IOException while closing stream::" + e.getMessage());
			}
			
		}
		
		return (sb != null) ? sb.toString() : null;

	}
	
	/**
     * This method checks if a String contains only numbers
     */
    public static boolean containsOnlyNumbers(String str) {
    	
        //It can't contain only numbers if it's null or empty...
        if (str == null || str.length() == 0)
            return false;
        
        for (int i = 0; i < str.length(); i++) {

            //If we find a non-digit character we return false.
            if (!Character.isDigit(str.charAt(i)))
                return false;
        }
        
        return true;
    }
    
	/**
	 * Checks whether the given string is integer or not 
	 * @param num
	 * @return
	 */
	public static boolean isIntNumber(String num){
	    try{
	        Integer.parseInt(num);
	    } catch(Exception nfe) {
	        return false;
	    }
	    return true;
	}
}