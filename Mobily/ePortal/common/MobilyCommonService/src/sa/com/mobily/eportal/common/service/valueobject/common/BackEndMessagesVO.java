/*
 * Created on May 17, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.valueobject.common;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * @author aghareeb
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BackEndMessagesVO extends BaseVO {
	private String key;
	private String messageEn;
	private String messageAr;
	private String description;
	

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return Returns the key.
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key The key to set.
	 */
	public void setKey(String key) {
		this.key = key;
	}
	/**
	 * @return Returns the messageAr.
	 */
	public String getMessageAr() {
		return messageAr;
	}
	/**
	 * @param messageAr The messageAr to set.
	 */
	public void setMessageAr(String messageAr) {
		this.messageAr = messageAr;
	}
	/**
	 * @return Returns the messageEn.
	 */
	public String getMessageEn() {
		return messageEn;
	}
	/**
	 * @param messageEn The messageEn to set.
	 */
	public void setMessageEn(String messageEn) {
		this.messageEn = messageEn;
	}
}
