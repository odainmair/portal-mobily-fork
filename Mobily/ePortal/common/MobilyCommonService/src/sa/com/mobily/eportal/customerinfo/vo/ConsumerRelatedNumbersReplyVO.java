package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerReplyHeaderVO;
import sa.com.mobily.eportal.common.service.vo.BaseVO;



/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER_REPLY"/>
 *         &lt;element ref="{}RelatedAccountsList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "relatedAccountsListVO"
})
@XmlRootElement(name = "MOBILY_BSL_SR_REPLY")
public class ConsumerRelatedNumbersReplyVO  extends BaseVO{


	private static final long serialVersionUID = 8079977076545156671L;
	@XmlElement(name = "SR_HEADER_REPLY", required = true)
    protected ConsumerReplyHeaderVO headerVO;
    @XmlElement(name = "RelatedAccountsList", required = true)
    protected RelatedAccountsListVO relatedAccountsListVO;
    
	public ConsumerReplyHeaderVO getHeaderVO() {
		return headerVO;
	}
	public void setHeaderVO(ConsumerReplyHeaderVO headerVO) {
		this.headerVO = headerVO;
	}
	public RelatedAccountsListVO getRelatedAccountsListVO() {
		return relatedAccountsListVO;
	}
	public void setRelatedAccountsListVO(RelatedAccountsListVO relatedAccountsListVO) {
		this.relatedAccountsListVO = relatedAccountsListVO;
	}

}
