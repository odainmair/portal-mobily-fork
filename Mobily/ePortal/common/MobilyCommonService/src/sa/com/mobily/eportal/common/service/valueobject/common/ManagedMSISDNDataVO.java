/*
 * Created on Feb 20, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.valueobject.common;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * @author aymanh
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ManagedMSISDNDataVO extends BaseVO 
{
	private String mSISDN = null;
	private String name   = null;
	private int    privelege;
	private String type = null;
	private String serviceAccountNumber;
	private String billingAccountNumber ;
	private String id ;
	private String cpeSerialNumber;
	private String status;
	private String customertype;
	private String packageName;
	private String promotion;
	private int lineType = 0;
	
	
    public int getLineType() {
		return lineType;
	}
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	public String getServiceAccountNumber() {
		return serviceAccountNumber;
	}
	public void setServiceAccountNumber(String serviceAccountNumber) {
		if(serviceAccountNumber == null)
			serviceAccountNumber ="";

		this.serviceAccountNumber = serviceAccountNumber;
	}
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}
	public void setBillingAccountNumber(String billingAccountNumber) {
		if(billingAccountNumber == null)
			billingAccountNumber ="";
		this.billingAccountNumber = billingAccountNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCpeSerialNumber() {
		return cpeSerialNumber;
	}
	public void setCpeSerialNumber(String cpeSerialNumber) {
		if(cpeSerialNumber == null)
			cpeSerialNumber ="";
		this.cpeSerialNumber = cpeSerialNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomertype() {
		return customertype;
	}
	public void setCustomertype(String customertype) {
		this.customertype = customertype;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPromotion() {
		return promotion;
	}
	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}
	/**
     * @return Returns the type.
     */
    public String getType() {
        return type;
    }
    /**
     * @param type The type to set.
     */
    public void setType(String type) {
        this.type = type;
    }
	/**
	 * @return Returns the mSISDN.
	 */
	public String getMSISDN() {
		return mSISDN;
	}
	/**
	 * @param msisdn The mSISDN to set.
	 */
	public void setMSISDN(String msisdn) {
		this.mSISDN = msisdn;
	}
	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return Returns the privelege.
	 */
	public int getPrivelege() {
		return privelege;
	}
	/**
	 * @param privelege The privelege to set.
	 */
	public void setPrivelege(int privelege) {
		this.privelege = privelege;
	}

}
