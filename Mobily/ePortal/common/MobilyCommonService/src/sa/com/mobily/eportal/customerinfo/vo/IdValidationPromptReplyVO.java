/**
 * 
 */
package sa.com.mobily.eportal.customerinfo.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author Suresh Vathsavai - MIT
 * Date: Nov 28, 2013
 */
public class IdValidationPromptReplyVO extends BaseVO{

	private static final long serialVersionUID = 1L;
	
	private String errorCode = null; //0000(Success),9999(Unknown EE Exception)
	private String errorMessage = null;
	private String prompt = null; //Y or N: Y means id validation is required
	
	/**
	 * @return the errorCode
	 */
	public String getErrorCode()
	{
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	/**
	 * @return the prompt
	 */
	public String getPrompt()
	{
		return prompt;
	}
	/**
	 * @param prompt the prompt to set
	 */
	public void setPrompt(String prompt)
	{
		this.prompt = prompt;
	}
	
	
}
