/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface SupplementaryServiceInquieyDAO {
    public String getSupplementaryInquiry(String xmlRequestMessage) throws MobilyCommonException;
    public String getWifiServiceInquiry(String xmlRequestMessage) throws MobilyCommonException;
    public String getRaomingCallServiceInquiry(String xmlRequestMessage) throws MobilyCommonException;
    public String getSafeArrStatAndRecipInquiry(String xmlRequestMessage) throws MobilyCommonException;
}
