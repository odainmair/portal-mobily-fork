package sa.com.mobily.eportal.common.service.mq.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MQAuditVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String msisdn;
	private String requestQueue;
	private String replyQueue;
	private String message;
	private String reply;
	private String errorMessage;
	private byte[] correlationID;
	private boolean sendingReply = false;
	private Integer waitingTime;
	private String userName;
	private String serviceError;
	private String functionName;
	
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getRequestQueue()
	{
		return requestQueue;
	}
	public void setRequestQueue(String requestQueue)
	{
		this.requestQueue = requestQueue;
	}
	public String getReplyQueue()
	{
		return replyQueue;
	}
	public void setReplyQueue(String replyQueue)
	{
		this.replyQueue = replyQueue;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getReply()
	{
		return reply;
	}
	public void setReply(String reply)
	{
		this.reply = reply;
	}
	public String getErrorMessage()
	{
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
	public byte[] getCorrelationID()
	{
		return correlationID;
	}
	public void setCorrelationID(byte[] correlationID)
	{
		this.correlationID = correlationID;
	}
	public boolean isSendingReply()
	{
		return sendingReply;
	}
	public void setSendingReply(boolean sendingReply)
	{
		this.sendingReply = sendingReply;
	}
	public Integer getWaitingTime()
	{
		return waitingTime;
	}
	public void setWaitingTime(Integer waitingTime)
	{
		this.waitingTime = waitingTime;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getServiceError()
	{
		return serviceError;
	}
	public void setServiceError(String serviceError)
	{
		this.serviceError = serviceError;
	}
	public String getFunctionName()
	{
		return functionName;
	}
	public void setFunctionName(String functionName)
	{
		this.functionName = functionName;
	}
	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}
	
}