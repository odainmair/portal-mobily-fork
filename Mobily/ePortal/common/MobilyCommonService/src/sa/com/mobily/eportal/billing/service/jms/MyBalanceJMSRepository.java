package sa.com.mobily.eportal.billing.service.jms;

import java.text.SimpleDateFormat;
import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.BalanceInquiryConstants;
import sa.com.mobily.eportal.billing.constants.BealIfc;
import sa.com.mobily.eportal.billing.constants.BillingConstantsIfc;
import sa.com.mobily.eportal.billing.constants.ConstantsIfc;
import sa.com.mobily.eportal.billing.constants.ExceptionIfc;
import sa.com.mobily.eportal.billing.util.AppConfig;
import sa.com.mobily.eportal.billing.vo.BalanceInquiryReplyVO;
import sa.com.mobily.eportal.billing.vo.BalanceInquiryRequestVO;
import sa.com.mobily.eportal.billing.xml.MyBalanceXMLHandler;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.customerinfo.constant.LineTypeConstant;

public final class MyBalanceJMSRepository implements BalanceInquiryConstants {
	
	private static String clazz = MyBalanceJMSRepository.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	/**
	 * @MethodName: getMyBalance
	 * <p>
	 * Get user balance throw MQ
	 * 
	 * @param BalanceInquiryRequestVO balanceInquiryRequestVO
	 * 
	 * @return BalanceInquiryReplyVO balanceInquiryReplyVO
	 * 
	 * @author Abu Sharaf
	 */
	 public  BalanceInquiryReplyVO getMyBalance(BalanceInquiryRequestVO balanceInquiryRequestVO){
		 String methodName = "getMyBalance";
		 String requestQueueName = "";
		 
		 BalanceInquiryReplyVO balanceInquiryReplyVO =null;
		 MQAuditVO mqAuditVO = new MQAuditVO();
		 
		if(balanceInquiryRequestVO.getLineType() == LineTypeConstant.SUBS_TYPE_WiMAX || balanceInquiryRequestVO.getLineType() == LineTypeConstant.SUBS_TYPE_FTTH) {
			requestQueueName = AppConfig.getInstance().get(BALANCE_INQ_WIMAX_REQUEST_QUEUENAME);
		} else {
			requestQueueName = AppConfig.getInstance().get(BALANCE_INQ_REQUEST_QUEUENAME);
		}
		String replyQueueName = AppConfig.getInstance().get(BALANCE_INQ_REPLY_QUEUENAME);
		executionContext.log(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : requestQueueName[" + requestQueueName + "]", clazz, methodName, null);
		executionContext.log(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : replyQueueName[" + replyQueueName + "]", clazz, methodName, null);
		
		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setUserName(balanceInquiryRequestVO.getUserID());
		mqAuditVO.setMsisdn(balanceInquiryRequestVO.getLineNumber());
		if("100".equals(balanceInquiryRequestVO.getLineNumber().substring(0,3))){
			mqAuditVO.setFunctionName(BillingConstantsIfc.MQ_WIMAX_MESSAGE_FORMAT);
		}else{
			mqAuditVO.setFunctionName(BealIfc.BALANCE_MESSAGE_FORMAT_VALUE);
		}
		
		String strXMLRequest = new MyBalanceXMLHandler().generateRequestXML(balanceInquiryRequestVO);
		executionContext.log(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : strXMLRequest: " + strXMLRequest, clazz, methodName, null);
		mqAuditVO.setMessage(strXMLRequest);
		//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
		String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		//String strXMLReply = "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>BALANCE_INQ</MsgFormat><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId>devuser02</RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>9754</ReturnCode></EE_EAI_HEADER></EE_EAI_MESSAGE>";
//		For Testing
		//String strXMLReply = "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>BALANCE_INQ</MsgFormat><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId>smsuser</RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0000</ReturnCode></EE_EAI_HEADER><LineNumber>966540511462</LineNumber><CustomerType>1</CustomerType><Balance>64.00</Balance><ExpirationDate>20130717</ExpirationDate><BilledAmount>13.0</BilledAmount><UnbilledAmount>44.0</UnbilledAmount><AccountNumber>966540511462</AccountNumber></EE_EAI_MESSAGE>";
		executionContext.log(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : strXMLReply: " + strXMLReply, clazz, methodName, null);
		mqAuditVO.setReply(strXMLReply);
				
		if (!FormatterUtility.isEmpty(strXMLReply))
		{
			balanceInquiryReplyVO = new MyBalanceXMLHandler().parseReplyXML(strXMLReply);
			if (balanceInquiryReplyVO != null && balanceInquiryReplyVO.getHeader() != null){ 
				executionContext.log(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : ReturnCode: " + balanceInquiryReplyVO.getHeader().getReturnCode(), clazz, methodName, null);
				mqAuditVO.setErrorMessage(balanceInquiryReplyVO.getHeader().getErrorMessage());
				mqAuditVO.setServiceError(balanceInquiryReplyVO.getHeader().getReturnCode());
			}			
				MQUtility.saveMQAudit(mqAuditVO);
		}
		
		if (balanceInquiryReplyVO == null || (balanceInquiryReplyVO.getErrorCode() != null && Integer.parseInt("" + balanceInquiryReplyVO.getErrorCode()) > 0))
		{
			String errorCode = ExceptionIfc.EXCEPTION_XML_ERROR;
			if(balanceInquiryReplyVO != null)
				errorCode = FormatterUtility.checkNull("" + balanceInquiryReplyVO.getErrorCode());
			executionContext.log(Level.INFO, "FreeBenefitsJMSRepositoryyy > "+methodName+" : errorCode: " + errorCode, clazz, methodName, null);
			mqAuditVO.setErrorMessage(errorCode);
			mqAuditVO.setServiceError(errorCode);
			MQUtility.saveMQAudit(mqAuditVO);
			throw new BackEndException(ExceptionConstantIfc.BALANCE_DATA, errorCode);
		}
		
		if(balanceInquiryRequestVO.isFtthRenewInfoRequired()){
			BalanceInquiryReplyVO ftthResponse = getFtthRenewalAmtInfo(balanceInquiryRequestVO);
			if(ftthResponse != null){
				balanceInquiryReplyVO.setRenewalAmount(ftthResponse.getRenewalAmount());
				balanceInquiryReplyVO.setFtthRenewexpDate(new SimpleDateFormat("yyyyMMddHHmmss").format(ftthResponse.getTempFtthRenewexpDate()));
			}
		}
			 
		 return balanceInquiryReplyVO;
	 }
	 
 /**
	 * This method is used to get the Renewal Amount information for FTTH 
	 * 
	 * @param requestVO
	 * @return
	 * @throws Exception
	 */
	private BalanceInquiryReplyVO getFtthRenewalAmtInfo( BalanceInquiryRequestVO requestVO) {
        
		BalanceInquiryReplyVO balanceInquiryReplyVO =null;
		String methodName = "getFtthRenewalAmtInfo";
		 MQAuditVO mqAuditVO = new MQAuditVO();
		 
			 String requestQueueName = AppConfig.getInstance().get(FTTH_RENEWAL_PAYMENT_REQUEST_QUEUENAME);
			
			String replyQueueName = AppConfig.getInstance().get(FTTH_RENEWAL_PAYMENT_REPLY_QUEUENAME);
			
			LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : requestQueueName[" + requestQueueName + "]", clazz, methodName, null);
			executionContext.log(logEntryVO);
			logEntryVO = new LogEntryVO(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : replyQueueName[" + replyQueueName + "]", clazz, methodName, null);
			executionContext.log(logEntryVO);
			
			mqAuditVO.setRequestQueue(requestQueueName);
			mqAuditVO.setReplyQueue(replyQueueName);
			mqAuditVO.setUserName(requestVO.getUserID());
			mqAuditVO.setMsisdn(requestVO.getLineNumber());
			mqAuditVO.setFunctionName(BillingConstantsIfc.FUNC_ID_FTTH_RENEW_INQ);

			String strXMLRequest = new MyBalanceXMLHandler().generateFTTHRenewInqXMLRequest(requestVO);
			
			logEntryVO = new LogEntryVO(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : strXMLRequest: " + strXMLRequest, clazz, methodName, null);
			executionContext.log(logEntryVO);
			mqAuditVO.setMessage(strXMLRequest);
			
			//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
			String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			logEntryVO = new LogEntryVO(Level.INFO, "MyBalanceJMSRepository > "+methodName+" : strXMLReply: " + strXMLReply, clazz, methodName, null);
			executionContext.log(logEntryVO);
			mqAuditVO.setReply(strXMLReply);
//				For Testing
//				String strXMLReply = "<MOBILY_BSL_SR_REPLY><SR_HEADER><FuncId>FTTHAR_Renewal_Payment_Amount_Inq</FuncId><SecurityKey></SecurityKey><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20121214100139</SrDate><RequestorUserId>ftth001</RequestorUserId></SR_HEADER><ErrorCode>0</ErrorCode><ErrorMsg></ErrorMsg><ChannelTransId>ePortal20121214100139</ChannelTransId><ServiceRequestId>SR_20924711</ServiceRequestId><RenewalAmount>2798.0</RenewalAmount><ExpiryDate>20130102</ExpiryDate><CustomerBalance>-2299.0</CustomerBalance></MOBILY_BSL_SR_REPLY>";
			if (!FormatterUtility.isEmpty(strXMLReply))
			{
				balanceInquiryReplyVO = new MyBalanceXMLHandler().parsingFTTHRenewInqReplyMessage(strXMLReply);
				mqAuditVO.setErrorMessage(balanceInquiryReplyVO.getErrorMessage());
				mqAuditVO.setServiceError(balanceInquiryReplyVO.getErrorCode());
				MQUtility.saveMQAudit(mqAuditVO);
			}
			
			if (balanceInquiryReplyVO == null || Integer.parseInt("" + balanceInquiryReplyVO.getErrorCode()) > 0)
			{
				String errorCode = ExceptionIfc.EXCEPTION_XML_ERROR;
				if(balanceInquiryReplyVO != null)
					errorCode = FormatterUtility.checkNull("" + balanceInquiryReplyVO.getErrorCode());
				logEntryVO = new LogEntryVO(Level.INFO, "FreeBenefitsJMSRepository > "+methodName+" : errorCode: " + errorCode, clazz, methodName, null);
				executionContext.log(logEntryVO);
				mqAuditVO.setErrorMessage(errorCode);
				mqAuditVO.setServiceError(errorCode);
				MQUtility.saveMQAudit(mqAuditVO);
				throw new BackEndException(ExceptionConstantIfc.BALANCE_DATA, errorCode);
			}	 
		 return balanceInquiryReplyVO;
	}
}