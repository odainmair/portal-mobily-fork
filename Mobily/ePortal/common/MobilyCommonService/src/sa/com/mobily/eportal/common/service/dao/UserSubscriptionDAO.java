package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.common.service.model.UserSubscription;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class UserSubscriptionDAO extends AbstractDBDAO<UserSubscription>
{
	private static UserSubscriptionDAO instance = new UserSubscriptionDAO();

	private static final String COLUMNS = "SUBSCRIPTION_ID, USER_ACCT_ID, IS_DEFAULT, SUBSCRIPTION_TYPE, MSISDN, SERVICE_ACCT_NUMBER, "
			+ "CUSTOMER_TYPE, PACKAGE_ID, CREATED_DATE, IS_ACTIVE, CC_STATUS, LAST_UPDATED_TIME, SUBSCRIPTION_NAME, SUB_SUBSCRIPTION_TYPE,IQAMA ";

	private static final String US_QUERY_BY_SUBSCRIPTION_ID = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE SUBSCRIPTION_ID = ? ";

	private static final String US_QUERY_BY_MSISDN_ID = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE MSISDN = ? AND IS_DEFAULT = ?";

	private static final String US_QUERY_BY_SERVICE_ACCOUNT_NUMBER = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE SERVICE_ACCT_NUMBER = ? AND IS_DEFAULT = ?";
	
	private static final String US_QUERY_BY_MSISDN_ID_IS_ACTIVE_DEFAULT = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE MSISDN = ? AND IS_DEFAULT = ? AND IS_ACTIVE = ?";

	private static final String US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_IS_ACTIVE_DEFAULT = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE SERVICE_ACCT_NUMBER = ? AND IS_DEFAULT = ? AND IS_ACTIVE = ?";

	private static final String US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_AND_MSISDN = "SELECT " + COLUMNS
			+ " FROM USER_SUBSCRIPTION WHERE (SERVICE_ACCT_NUMBER = ? OR MSISDN = ?) AND IS_DEFAULT = ?";

	private static final String US_QUERY_BY_USER_ACCT_ID = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE USER_ACCT_ID = ? ";
	
	private static final String US_QUERY_BY_USER_ACCT_ID_ACTIVE = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE USER_ACCT_ID = ? AND IS_ACTIVE = ?";

	private static final String USER_SUBSCRIPTION_ADD = "INSERT INTO USER_SUBSCRIPTION "
			+ "(SUBSCRIPTION_ID,USER_ACCT_ID,IS_DEFAULT,SUBSCRIPTION_TYPE,MSISDN,SERVICE_ACCT_NUMBER,CUSTOMER_TYPE,PACKAGE_ID,"
			+ "CREATED_DATE,IS_ACTIVE,CC_STATUS,SUBSCRIPTION_NAME,SUB_SUBSCRIPTION_TYPE,IQAMA) VALUES " + "(USER_SUBSCRIPTION_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

	private static final String USER_SUBSCRIPTION_ADD_WITHOUT_SEQ = "INSERT INTO USER_SUBSCRIPTION "
			+ "(SUBSCRIPTION_ID,USER_ACCT_ID,IS_DEFAULT,SUBSCRIPTION_TYPE,MSISDN,SERVICE_ACCT_NUMBER,CUSTOMER_TYPE,PACKAGE_ID,"
			+ "CREATED_DATE,IS_ACTIVE,CC_STATUS,SUBSCRIPTION_NAME,SUB_SUBSCRIPTION_TYPE,IQAMA) VALUES " + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	
	private static final String USER_SUBSCRIPTION_UPDATE_DEFAULT_NUMBER = "UPDATE USER_SUBSCRIPTION SET IS_DEFAULT = ? WHERE SUBSCRIPTION_ID = ?";

	private static final String US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_OR_MSISDN = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE SERVICE_ACCT_NUMBER = ?";
	
	private static final String US_QUERY_BY_SERVICE_MSISDN = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE MSISDN = ?";

	private static final String USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_INACTIVE = "UPDATE USER_SUBSCRIPTION SET IS_ACTIVE = 'N' WHERE SUBSCRIPTION_ID = ?";
	
	// added by MHASSAN 
	
	private static final String USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_MSISDN = "UPDATE USER_SUBSCRIPTION SET MSISDN = ? WHERE SUBSCRIPTION_ID = ?";
	
	private static final String USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_SUBSCRIPTION_TYPE = "UPDATE USER_SUBSCRIPTION SET SUBSCRIPTION_TYPE = ?, SUBSCRIPTION_NAME = ? WHERE SUBSCRIPTION_ID = ?";

	private static final String USER_SUBSCRIPTION_UPDATE_SUB_SUBSCRIPTION_TYPE = "UPDATE USER_SUBSCRIPTION SET SUB_SUBSCRIPTION_TYPE = ? WHERE SUBSCRIPTION_ID = ?";
	
	private static final String USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_CUSTOMER_TYPE = "UPDATE USER_SUBSCRIPTION SET CUSTOMER_TYPE = ?, LAST_UPDATED_TIME = CURRENT_TIMESTAMP WHERE SUBSCRIPTION_ID = ?";

	private static final String USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_PACKAGE_ID = "UPDATE USER_SUBSCRIPTION SET PACKAGE_ID = ? WHERE SUBSCRIPTION_ID = ?";
	
	private static final String USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_INACTIVE_DEFAULT = "UPDATE USER_SUBSCRIPTION SET IS_ACTIVE = 'N', IS_DEFAULT = 'N', LAST_UPDATED_TIME = CURRENT_TIMESTAMP  WHERE SUBSCRIPTION_ID = ?";
	
	private static final String GET_SEQ_USER_SUBSCRIPTION_ID = "SELECT USER_SUBSCRIPTION_SEQ.NEXTVAL SUBSCRIPTION_ID FROM DUAL";
	
	private static final String DELETE_USER_SUBSCRIPTIONS_BY_USERACCID = "DELETE FROM USER_SUBSCRIPTION WHERE USER_ACCT_ID=?";
	
	private static final String DELETE_USER_SUBSCRIPTION_AND_USER_ACCT_BY_USERACCID = "BEGIN DELETE FROM USER_SUBSCRIPTION WHERE USER_ACCT_ID=?; DELETE FROM USER_ACCOUNTS WHERE USER_ACCT_ID=?; END;";
	
	private static final String DELETE_USER_SUBSCRIPTION_BY_SUBID = "DELETE FROM USER_SUBSCRIPTION WHERE SUBSCRIPTION_ID=?";
	
	private static final String US_QUERY_BY_ACCOUNT_NUMBER = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE (SERVICE_ACCT_NUMBER = ? OR MSISDN = ?)";

	private static final String US_QUERY_BY_USER_ACCT_ID_DEFAULT = "SELECT " + COLUMNS + " FROM USER_SUBSCRIPTION WHERE USER_ACCT_ID = ? AND IS_DEFAULT = 'Y'";
	
	protected UserSubscriptionDAO()
	{
		super(DataSources.DEFAULT);
	}

	/**
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static synchronized UserSubscriptionDAO getInstance()
	{
		return instance;
	}

	
	public Long getUserSubscriptionIdSeq()
	{

		UserSubscription dto = null;
		List<UserSubscription> userSubscriptionList = query(GET_SEQ_USER_SUBSCRIPTION_ID);

		if (userSubscriptionList != null && userSubscriptionList.size() > 0)
		{
			dto = userSubscriptionList.get(0);
		}
		return dto.getSubscriptionId();
	}

	
	public List<UserSubscription> findByUserSubscriptionId(Integer userSubscriptionId)
	{
		return query(US_QUERY_BY_SUBSCRIPTION_ID, new Object[] { userSubscriptionId });
	}

	public List<UserSubscription> findDefaultByMSISDN(String msisdn)
	{
		return query(US_QUERY_BY_MSISDN_ID, new Object[] { msisdn, "Y" });
	}
	
	public List<UserSubscription> findNonDefaultByMSISDN(String msisdn)
	{
		return query(US_QUERY_BY_MSISDN_ID, new Object[] { msisdn, "N" });
	}

	public List<UserSubscription> findDefaultByServiceAccountNumber(String serviceAccountNumber)
	{
		return query(US_QUERY_BY_SERVICE_ACCOUNT_NUMBER, new Object[] { serviceAccountNumber, "Y" });
	}
	
	public List<UserSubscription> findActiveDefaultByMSISDN(String msisdn)
	{
		return query(US_QUERY_BY_MSISDN_ID_IS_ACTIVE_DEFAULT, new Object[] { msisdn, "Y", "Y" });
	}

	public List<UserSubscription> findActiveDefaultByServiceAccountNumber(String serviceAccountNumber)
	{
		return query(US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_IS_ACTIVE_DEFAULT, new Object[] { serviceAccountNumber, "Y", "Y" });
	}
	
	public List<UserSubscription> findActiveNonDefaultByMSISDN(String msisdn)
	{
		return query(US_QUERY_BY_MSISDN_ID_IS_ACTIVE_DEFAULT, new Object[] { msisdn, "N","Y" });
	}

	public List<UserSubscription> findActiveNonDefaultByServiceAccountNumber(String serviceAccountNumber)
	{
		return query(US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_IS_ACTIVE_DEFAULT, new Object[] { serviceAccountNumber, "N", "Y" });
	}

	public int saveUserSubscripton(UserSubscription dto)
	{
		return update(USER_SUBSCRIPTION_ADD, dto.getUserAcctId(), dto.getIsDefault(), dto.getSubscriptionType(), dto.getMsisdn(), dto.getServiceAcctNumber(),
				dto.getCustomerType(), dto.getPackageId(), dto.getCreatedDateAsTimestamp(), dto.getIsActive(), dto.getCcStatus(), dto.getSubscriptionName(), dto.getLineType(), dto.getNationalIdOrIqamaNumber());
	}
	/**
	 * Method for inserting new subscription without using the table sequence .this method expect sendin the sequence value in the inout DTO
	 * 
	 * @return
	 */
	
	public int addUserSubscripton(UserSubscription dto)
	{
		return update(USER_SUBSCRIPTION_ADD_WITHOUT_SEQ, dto.getSubscriptionId(),dto.getUserAcctId(), dto.getIsDefault(), dto.getSubscriptionType(), dto.getMsisdn(), dto.getServiceAcctNumber(),
				dto.getCustomerType(), dto.getPackageId(), dto.getCreatedDateAsTimestamp(), dto.getIsActive(), dto.getCcStatus(), dto.getSubscriptionName(), dto.getLineType(), dto.getNationalIdOrIqamaNumber());
	}
	
	public List<UserSubscription> findDefaultByServiceAccountNumberOrMsisdn(String number)
	{
		return query(US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_AND_MSISDN, new Object[] { number, number, "Y" });
	}

	public List<UserSubscription> findByUserAccountID(long number)
	{
		return query(US_QUERY_BY_USER_ACCT_ID, new Object[] { number });
	}
	
	public List<UserSubscription> findActiveByUserAccountID(long number)
	{
		return query(US_QUERY_BY_USER_ACCT_ID_ACTIVE, new Object[] { number, "Y" });
	}

	public int updateUserDefaultSubscription(long subscriptionID, String isDefault)
	{
		return update(USER_SUBSCRIPTION_UPDATE_DEFAULT_NUMBER, isDefault, subscriptionID);
	}

	public List<UserSubscription> findAllSubscriptionsByServiceAccountNumberOrMsisdn(String number)
	{
		return query(US_QUERY_BY_SERVICE_ACCOUNT_NUMBER_OR_MSISDN, new Object[] { number });
	}
	
	public List<UserSubscription> findAllSubscriptionsByMsisdn(String number)
	{
		return query(US_QUERY_BY_SERVICE_MSISDN, new Object[] { number });
	}

	public int deActiveSubscription(List<Long> subscriptionIDList)
	{
		int result = -1;
		for (Long subscriptionID : subscriptionIDList)
		{
			update(USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_INACTIVE, new Object[] { subscriptionID });
		}
		result = 0;
		return result;
	}
	
	// added by MHASSAN
	
	public int updateSubscriptionMsisdn(long subscriptionID , String msisdn )
	{
		return update(USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_MSISDN, msisdn , subscriptionID);
	}

			
	public int updateSubscriptionPaymentType(long subscriptionID , String customerType)
	{
		return update(USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_CUSTOMER_TYPE, customerType , subscriptionID);
	}
	
	
	
	public int updateSubscriptionType(long subscriptionID , int subscriptionType, String subscriptionName)
	{
		return update(USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_SUBSCRIPTION_TYPE, subscriptionType , subscriptionName, subscriptionID);
	}
	
	public int updateSubSubscriptionType(long subscriptionID , int subSubscriptionType)
	{
		return update(USER_SUBSCRIPTION_UPDATE_SUB_SUBSCRIPTION_TYPE, subSubscriptionType , subscriptionID);
	}
	
	public int updateSubscriptionPackageId(long subscriptionID , String packageID)
	{
		return update(USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_PACKAGE_ID, packageID , subscriptionID);
	}
	
	
	public int deActiveSubscriptionBySubscriptionID(Long subscriptionID)
	{
		return 	update(USER_SUBSCRIPTION_UPDATE_SUBSCRIPTION_INACTIVE_DEFAULT, new Object[] { subscriptionID });
	}
	
	/***
	 * Delete User Subscriptions by user account id
	 * @param userAccountId
	 * @return
	 */
	public int deleteUserSubscriptions(Long userAccountId) {
        return update(DELETE_USER_SUBSCRIPTIONS_BY_USERACCID, new Object[] {userAccountId});
    }
	
	/***
	 * Delete User Subscriptions by user account id and user account row
	 * @param userAccountId
	 * @return
	 */
	public int deleteUserSubscriptionsAndUserAccount(Long userAccountId) {
        return update(DELETE_USER_SUBSCRIPTION_AND_USER_ACCT_BY_USERACCID, new Object[] {userAccountId, userAccountId});
    }
	
	
	private static final String DELETE_USER_NON_DEFAULT_SUBSCRIPTION = "BEGIN DELETE FROM USER_SUBSCRIPTION WHERE MSISDN=?; DELETE FROM USER_BILL_POID WHERE MSISDN=?; END;";
	
	public int deleteNonDefaultSubscription(String msisdn)
	{
		return update(DELETE_USER_NON_DEFAULT_SUBSCRIPTION, new Object[] {msisdn, msisdn});
	}
	
	
	/***
	 * Delete User Subscriptions by user account id
	 * @param userAccountId
	 * @return
	 */
	public int deleteUserSubscription(Long subscriptionId) {
        return update(DELETE_USER_SUBSCRIPTION_BY_SUBID, new Object[] {subscriptionId});
    }
	
	public List<UserSubscription> findAllSubscriptionsByAccountNumber(String number)
	{
		return query(US_QUERY_BY_ACCOUNT_NUMBER, new Object[] { number, number });
	}
	
	public UserSubscription findDefaultByUserAccountID(long number)
	{
		UserSubscription dto = null;
		List<UserSubscription> userSubscriptionList = query(US_QUERY_BY_USER_ACCT_ID_DEFAULT, new Object[] { number });

		if (userSubscriptionList != null && userSubscriptionList.size() > 0)
		{
			dto = userSubscriptionList.get(0);
		}
		return dto;
	}

	@Override
	protected UserSubscription mapDTO(ResultSet rs) throws SQLException
	{
		
		UserSubscription userSubscription = new UserSubscription();
		userSubscription.setSubscriptionId(rs.getLong("SUBSCRIPTION_ID"));
		if(rs.getMetaData().getColumnCount() > 10){
			
			userSubscription.setSubscriptionId(rs.getLong("SUBSCRIPTION_ID"));
			userSubscription.setUserAcctId(rs.getLong("USER_ACCT_ID"));
			userSubscription.setIsDefault(rs.getString("IS_DEFAULT"));
			userSubscription.setSubscriptionType(rs.getInt("SUBSCRIPTION_TYPE"));
			userSubscription.setMsisdn(rs.getString("MSISDN"));
			userSubscription.setServiceAcctNumber(rs.getString("SERVICE_ACCT_NUMBER"));
			userSubscription.setCustomerType(rs.getString("CUSTOMER_TYPE"));
			userSubscription.setPackageId(rs.getString("PACKAGE_ID"));
			userSubscription.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
			userSubscription.setIsActive(rs.getString("IS_ACTIVE"));
			userSubscription.setCcStatus(rs.getInt("CC_STATUS"));
			userSubscription.setLastUpdatedTime(rs.getTimestamp("LAST_UPDATED_TIME"));
			userSubscription.setSubscriptionName(rs.getString("SUBSCRIPTION_NAME"));
			userSubscription.setLineType(rs.getInt("SUB_SUBSCRIPTION_TYPE"));
			userSubscription.setNationalIdOrIqamaNumber(rs.getString("IQAMA"));
		
		}
		return userSubscription;
	}
}