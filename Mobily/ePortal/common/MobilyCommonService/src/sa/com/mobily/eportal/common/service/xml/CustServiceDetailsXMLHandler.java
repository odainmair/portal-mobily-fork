/*
 * Created on Jul 3, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CustServiceDetReqVO;
import sa.com.mobily.eportal.common.service.vo.CustomerServiceDetReplyVO;
import sa.com.mobily.eportal.common.service.vo.ServiceVO;



/*<EE_EAI_MESSAGE>
 <EE_EAI_HEADER>
 <MsgFormat>CUSTOMER_DETAILS</MsgFormat>
 <MsgVersion>0001</MsgVersion>
 <SecurityKey>asf2123aqws123123</SecurityKey>
 <RequestorChannelId>ePortal</RequestorChannelId>
 <RequestorUserId>S1234</RequestorUserId>
 <RequestorLanguage>E</RequestorLanguage>
 <ReturnCode>0000</ReturnCode>
 </EE_EAI_HEADER>
 <MSISDN>966540511334</MSISDN>
 <SupplementaryInfo>Y</SupplementaryInfo>
 </EE_EAI_MESSAGE>*/

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustServiceDetailsXMLHandler {
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
	ArrayList  listofStatus = null;
	ArrayList  listofSupplmentaryServices = null;
	ArrayList  listofServices = null;
	
	public String getcustomerServiceDetailsFromBSL(CustServiceDetReqVO custServiceDetReqVO) {
		String requestMessage = "";
		Document doc = new DocumentImpl();
		Element root = doc.createElement(TagIfc.MAIN_EAI_TAG_NAME);
		Element header = XMLUtility.openParentTag(doc,TagIfc.EAI_HEADER_TAG);
		XMLUtility.generateElelemt(doc, header,TagIfc.TAG_MSG_FORMAT, ConstantIfc.CUST_INFO_MSG_FORMAT_VALUE);
		XMLUtility.generateElelemt(doc, header,TagIfc.TAG_MSG_VERSION, ConstantIfc.CUST_INFO_MSG_VERSION_VALUE);
		XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,ConstantIfc.CUST_INFO_SECURITY_VALUE);
		XMLUtility.generateElelemt(doc, header,TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.CUST_INFO_REQUESTOR_CHANNEL_ID_VALUE);
		XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID,custServiceDetReqVO.getUserID());
		XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE,ConstantIfc.CUST_INFO_REQUESTOR_LANG_VALUE);
		XMLUtility.generateElelemt(doc, header,TagIfc.TAG_RETURN_CODE,ConstantIfc.CUST_INFO_RETURN_CODE_VALUE);
		XMLUtility.closeParentTag(root, header);
		XMLUtility.generateElelemt(doc, root,TagIfc.TAG_MSISDN , custServiceDetReqVO.getLineNumber());
		
		if(!"".equals(custServiceDetReqVO.getInternetBundle())){
			XMLUtility.generateElelemt(doc, root,TagIfc.TAG_GET_INTERNET_BUNDLE,custServiceDetReqVO.getInternetBundle());
		}else{
			XMLUtility.generateElelemt(doc, root,TagIfc.TAG_GET_INTERNET_BUNDLE,ConstantIfc.CUST_INFO_GET_INTERNET_BUNDLE );
		}
		
		doc.appendChild(root);
		try {
			requestMessage = XMLUtility.serializeRequest(doc);
			
		} catch (IOException e) {
			//log.fatal("CustServiceDetailsXMLHandler >getcustomerServiceDetailsFromBSL > IOException  > "+e.getMessage());
			throw new SystemException();
		}
		
		//log.debug("CustServiceDetailsXMLHandler >getcustomerServiceDetailsFromBSL > for Customer ended ");
		return requestMessage;
		
	}
	
	
	/*<EE_EAI_MESSAGE>
	 <EE_EAI_HEADER>
	 <MsgFormat>CUSTOMER_DETAILS</MsgFormat>
	 <MsgVersion>0001</MsgVersion>
	 <SecurityKey>asf2123aqws123123</SecurityKey>
	 <RequestorChannelId>ePortal</RequestorChannelId>
	 <RequestorUserId>S1234</RequestorUserId>
	 <RequestorLanguage>E</RequestorLanguage>
	 <ReturnCode>0000</ReturnCode>
	 </EE_EAI_HEADER>
	 <LineNumber>966540511334</LineNumber>
	 <PackageId>1241</PackageId>
	 <CustomerType>1</CustomerType>
	 <PackageDescription>Lucky</PackageDescription>
	 <CreationTime>05052008102141</CreationTime>
	 <CorporatePackage>No</CorporatePackage>
	 <Status>1</Status>
	 
	 <SupplementaryServices>
	 
	 <SupplementaryService>
	 <ServiceName>Video Streaming</ServiceName>
	 <Status>1</Status>
	 </SupplementaryService>
	 
	 <SupplementaryService>
	 <ServiceName>RBT</ServiceName>
	 <Status>1</Status>
	 </SupplementaryService>
	 
	 <SupplementaryService>
	 <ServiceName>ICS</ServiceName>
	 <Status>0</Status>
	 </SupplementaryService>
	 
	 </SupplementaryServices
	 */
	/**
	 * @param xmlReply
	 * @return
	 */
	public CustomerServiceDetReplyVO parsingcustomerServiceDetails(String replyMessage) {
		CustomerServiceDetReplyVO replyVO = null;
		//log.debug("CustServiceDetailsXMLHandler > parsingcustomerServiceDetails > Called");
		Date date = null;
		try {
			if (replyMessage == null || replyMessage == "")
				throw new SystemException();
			
			replyVO = new CustomerServiceDetReplyVO();
			Document doc = XMLUtility.parseXMLString(replyMessage);
			
			
			Node rootNode = doc.getElementsByTagName(
					TagIfc.MAIN_EAI_TAG_NAME).item(0);
			
			NodeList nl = rootNode.getChildNodes();
			
			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				
				
				if(p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG)){

					NodeList headerNode = l_Node.getChildNodes();
					Element nodeElement = null;
					String nodeTagName = "";
					String nodeTageValue = "";
					
					for(int k=0; k< headerNode.getLength(); k++) {
						nodeTageValue = "";
						nodeElement = (Element)headerNode.item(k);
						nodeTagName =nodeElement.getTagName();
						if (nodeElement.getFirstChild() != null)
							nodeTageValue = nodeElement.getFirstChild().getNodeValue();
						
						if (nodeTagName.equalsIgnoreCase(TagIfc.TAG_RETURN_CODE)) 
							replyVO.setReturnCode(nodeTageValue);	
					}
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_LINE_NUMBER)) {
					replyVO.setLineNumber(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PACKAGE_ID)) {
					replyVO.setPackageID(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CUSTOMER_TYPE)) {
					if(FormatterUtility.isEmpty(l_szNodeValue)){
						l_szNodeValue = "1";
					}
					replyVO.setCustomerType(Integer.parseInt(l_szNodeValue));
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PACKAGE_DESCRIPTION)) {
					replyVO.setPackageDescription(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CREATION_TIME)) {
					try {
						SimpleDateFormat df=new SimpleDateFormat("MM/dd/yyyy");
						date=df.parse(l_szNodeValue);
					}
					catch (Exception err) {
					}
					replyVO.setCreationDate(date);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CORPORATE_PACKAGE)) {
					replyVO.setCorporatePackage(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_STATUS)) {
					if(FormatterUtility.isEmpty(l_szNodeValue)){
						l_szNodeValue = "1";
					}
					replyVO.setStatus(Integer.parseInt(l_szNodeValue));
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SUPPLEMENTARYSERVICES)) {
					ArrayList serviceVOs = getListOfServicesVOs(l_Node);
					replyVO.setListOfServices(serviceVOs);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ACCOUNT_NUMBER)) {
					replyVO.setAccountNumber(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_PACKAGE_NAME)) {
					replyVO.setPackageName(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CPE_SERIAL_NO)) {
					replyVO.setCpeSerialNo(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MAX_UPLOAD_RATE)) {
					replyVO.setMaxUploadRate(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_MAX_DOWNLOAD_RATE)) {
					replyVO.setMaxDownloadRate(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_DURATION)) {
					replyVO.setDuration(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CUST_CATEGORY)) {
					replyVO.setCustomerCategory(l_szNodeValue);
				}else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CPE_MAC_ADDRESS)) {
					replyVO.setCpeMACAddress(l_szNodeValue);
				}
				
			}
			
			
			//log.debug("CustServiceDetailsXMLHandler > parsingcustomerServiceDetails > ended  " + replyVO.getReturnCode());
		}  catch (Exception e) {
			log.fatal("CustServiceDetailsXMLHandler > parsingcustomerServiceDetails >> Exception , "	+ e.getMessage());
			throw new SystemException(e);
		}
		// TODO Auto-generated method stub
		return replyVO;
	}
	
	private ArrayList getListOfServicesVOs(Element l_Node) throws SystemException{
		ArrayList serviceVOs = new ArrayList();
		log	.debug("CustServiceDetailsXMLHandler > getListOfServicesVOs > called....");
		ServiceVO serviceVO = null;
		try {
			NodeList serviceNodeList = l_Node.getChildNodes();			
			for(int i=0; i< serviceNodeList.getLength(); i++){
				Element s_Node = (Element) serviceNodeList.item(i);
				String s_szNodeTagName = s_Node.getTagName();
				serviceVO = new ServiceVO();
				if (s_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SUPPLEMENTARYSERVICE)){
					NodeList supplementaryserviceNodeList = s_Node.getChildNodes();				
					for (int j = 0; j < supplementaryserviceNodeList.getLength(); j++) {					
						if ((supplementaryserviceNodeList.item(j)).getNodeType() != Node.ELEMENT_NODE){
							continue;
						}
						Element c_Node = (Element) supplementaryserviceNodeList.item(j);
						String p_szTagName = c_Node.getTagName();
						String l_szNodeValue = null;
						if (c_Node.getFirstChild() != null){
							l_szNodeValue = c_Node.getFirstChild().getNodeValue();
						}
						if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SERVICENAME)) {						
							serviceVO.setServiceName(l_szNodeValue);						
						} else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_STATUS)) {	
							if(FormatterUtility.isEmpty(l_szNodeValue)){
								l_szNodeValue = "0";
							}
							serviceVO.setStatus(Integer.parseInt(l_szNodeValue));						
						}			
					}
					serviceVOs.add(serviceVO);			   	
				}
		  }
		} catch (DOMException e) {
			throw new SystemException(e);
		}
		if(serviceVOs != null)
			log	.debug("CustServiceDetailsXMLHandler > getListOfServicesVOs > Done , List size = "+serviceVOs.size());
		
		return serviceVOs;
	}
	
	public static void main(String args[]){
		CustServiceDetailsXMLHandler custServiceDetailsXMLHandler = new CustServiceDetailsXMLHandler();
		String xmlReply = "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>CUSTOMER_DETAILS</MsgFormat><MsgVersion>0001</MsgVersion><SecurityKey>asf2123aqws123123</SecurityKey><RequestorChannelId>ePortal</RequestorChannelId><RequestorUserId>mobilylogin</RequestorUserId><RequestorLanguage>E</RequestorLanguage><ReturnCode>0000</ReturnCode></EE_EAI_HEADER><LineNumber>966540510553</LineNumber><PackageId>1467</PackageId><CustomerType>1</CustomerType><PackageDescription>7ala</PackageDescription><CreationTime>02012012123233</CreationTime><CorporatePackage>No</CorporatePackage><Status>1</Status></EE_EAI_MESSAGE>";
		try {
			CustomerServiceDetReplyVO  replyVo = custServiceDetailsXMLHandler.parsingcustomerServiceDetails(xmlReply);
			
//			System.out.println(replyVo.getStatus());
//			System.out.println(replyVo.getCustomerType());
		} catch (Exception e) {
			
//			e.printStackTrace();
		}
		
	}
	
}
