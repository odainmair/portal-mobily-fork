package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.PackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQPackageConversionDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OraclePackageConversionDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.PackageConversionReplyVO;
import sa.com.mobily.eportal.common.service.vo.PackageConversionRequestVO;
import sa.com.mobily.eportal.common.service.xml.PackageConversionXMLHandler;


/**
 * 
 * @author n.gundluru.mit
 *
 */

public class PackageConversionBO {

	private static final Logger log = LoggerInterface.log;
	
	/**
	 * @param xmlRequest
	 * @return
	 */
	public PackageConversionReplyVO handlePackageConversion(PackageConversionRequestVO packageConversionReqVO) throws MobilyCommonException{
		// TODO Auto-generated method stub
		String xmlResponse = "";
		PackageConversionReplyVO replyVO = new PackageConversionReplyVO();
		log.debug("PackageConversionBO > handlePackageConversion > Call MQ Backend for Inquiry");
		
			String xmlRequest = PackageConversionXMLHandler.generateXMLRequest(packageConversionReqVO);
			log.debug("PackageConversionBO > handlePackageConversion > xmlRequest >"+xmlRequest);
			
			MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
			PackageConversionDAO serviceDAO = (MQPackageConversionDAO)daoFactory.getMQPackageConversionDAO();
			xmlResponse = serviceDAO.handlePackageConversion(xmlRequest);

			log.debug("PackageConversionBO > handlePackageConversion > xmlResponse >"+xmlResponse);
			
			replyVO = PackageConversionXMLHandler.parsingXMLReply(xmlResponse);

			log.debug("PackageConversionBO > handlePackageConversion > Inquiry Status Retrieved "+xmlResponse);
			
		return replyVO;
	}
	
	
	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SystemException
	 */
	public boolean hasPackageConversionPendingRequest(String msisdn) throws SystemException {

		log.debug("PackageConversionBO > hasPackageConversionPendingRequest > Called");
		
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		PackageConversionDAO serviceDAO = (OraclePackageConversionDAO)daoFactory.getOraclePackageConversionDAO();

		return serviceDAO.hasPendingRequest(msisdn);
	}

}