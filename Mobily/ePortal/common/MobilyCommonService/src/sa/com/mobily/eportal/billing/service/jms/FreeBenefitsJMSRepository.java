package sa.com.mobily.eportal.billing.service.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.ExceptionIfc;
import sa.com.mobily.eportal.billing.constants.FreeBenefitsConstants;
import sa.com.mobily.eportal.billing.util.AppConfig;
import sa.com.mobily.eportal.billing.vo.FreeBalancesReplyVO;
import sa.com.mobily.eportal.billing.vo.FreeBalancesRequestVO;
import sa.com.mobily.eportal.billing.vo.MyFreeBalancesReplyVO;
import sa.com.mobily.eportal.billing.xml.FreeBenefitsXMLHandler;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

public final class FreeBenefitsJMSRepository implements FreeBenefitsConstants
{

	private static String clazz = FreeBenefitsJMSRepository.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	/**
	 * @MethodName: getFreeBalance
	 * <p>
	 * Retrieves User free balance, data, minutes, SMS and other related free information throw MQ
	 * by MSISDN and/or customer type
	 * 
	 * @param FreeBalancesRequestVO freeBalancesRequestVO
	 * 
	 * @return FreeBalancesReplyVO freeBalancesReplyVO
	 * 
	 * @author Abu Sharaf
	 */
	public FreeBalancesReplyVO getFreeBalance(FreeBalancesRequestVO freeBalancesRequestVO)
	{

		FreeBalancesReplyVO freeBalancesReplyVO = null;
		String methodName = "getFreeBalance";
		
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(FREE_BALANCE_INQ_REQUEST_QUEUENAME);
		String replyQueueName = AppConfig.getInstance().get(FREE_BALANCE_INQ_REPLY_QUEUENAME);

		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : requestQueueName[" + requestQueueName + "]", clazz, methodName, null);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : replyQueueName[" + replyQueueName + "]", clazz, methodName, null);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setUserName(freeBalancesRequestVO.getReqUserName());
		mqAuditVO.setMsisdn(freeBalancesRequestVO.getMsisdn());
		mqAuditVO.setFunctionName("Free Balance");
		
		String strXMLRequest = new FreeBenefitsXMLHandler().generateXMLRequestMessage(freeBalancesRequestVO);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : strXMLRequest: " + strXMLRequest, clazz, methodName, null);
		
		mqAuditVO.setMessage(strXMLRequest);
		// For Testing
//		String strXMLReply = "RESP:0:SUBSCRIBERNUMBER,966540510324:CUSTOMERTYPE,2:FreeMinutes,10:FreeSMS,5:FreeGPRS,30720:FreeMobilyMinutes,3:FreeMobilySMS,23:NationalMinutes,34:BlackBerry,1048576:BlackBerryRoaming,5120;";

		//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
		String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : strXMLReply: " + strXMLReply, clazz, methodName, null);
		mqAuditVO.setReply(strXMLReply);
		if (!FormatterUtility.isEmpty(strXMLReply))
		{
			freeBalancesReplyVO = new FreeBenefitsXMLHandler().parsingXMLReplyMessage(strXMLReply);
			mqAuditVO.setErrorMessage(freeBalancesReplyVO.getErrorCode());
			mqAuditVO.setServiceError(freeBalancesReplyVO.getErrorCode());
			MQUtility.saveMQAudit(mqAuditVO);
		}

		if (freeBalancesReplyVO == null)
		{
			String errorCode = ExceptionIfc.EXCEPTION_XML_ERROR;
			executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : errorCode: " + errorCode, clazz, methodName, null);
			mqAuditVO.setErrorMessage(errorCode);
			mqAuditVO.setServiceError(errorCode);
			MQUtility.saveMQAudit(mqAuditVO);
			throw new BackEndException(ExceptionConstantIfc.FREE_BALANCE, errorCode);
		}
		return freeBalancesReplyVO;
	}
	
	
	
	/**
	 * @MethodName: getFreeBalance
	 * <p>
	 * Retrieves User free balance, data, minutes, SMS and other related free information throw MQ
	 * by MSISDN and/or customer type and handles the "Unlimited" case for all
	 * 
	 * @param FreeBalancesRequestVO freeBalancesRequestVO
	 * 
	 * @return MyFreeBalancesReplyVO myFreeBalancesReplyVO
	 * 
	 * @author Nagendra Gundluru
	 */
	public MyFreeBalancesReplyVO getMyFreeBalance(FreeBalancesRequestVO freeBalancesRequestVO)
	{

		String methodName = "getMyFreeBalance";
		MyFreeBalancesReplyVO myFreeBalancesReplyVO = null;

		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(FREE_BALANCE_INQ_REQUEST_QUEUENAME);
		String replyQueueName = AppConfig.getInstance().get(FREE_BALANCE_INQ_REPLY_QUEUENAME);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : requestQueueName[" + requestQueueName + "]", clazz, methodName, null);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : replyQueueName[" + replyQueueName + "]", clazz, methodName, null);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setUserName(freeBalancesRequestVO.getReqUserName());
		mqAuditVO.setMsisdn(freeBalancesRequestVO.getMsisdn());
		mqAuditVO.setFunctionName("My Free Balance");

		String strXMLRequest = new FreeBenefitsXMLHandler().generateXMLRequestMessage(freeBalancesRequestVO);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : strXMLRequest: " + strXMLRequest, clazz, methodName, null);
		mqAuditVO.setMessage(strXMLRequest);
		
		// For Testing
//		String strXMLReply = "RESP:0:SUBSCRIBERNUMBER,966540510324:CUSTOMERTYPE,2:FreeMinutes,10:FreeSMS,5:FreeGPRS,30720:FreeMobilyMinutes,3:FreeMobilySMS,23:NationalMinutes,34:BlackBerry,1048576:BlackBerryRoaming,5120;";

		//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
		String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : strXMLReply: " + strXMLReply, clazz, methodName, null);
		mqAuditVO.setReply(strXMLReply);
		if (!FormatterUtility.isEmpty(strXMLReply))
		{
			myFreeBalancesReplyVO = new FreeBenefitsXMLHandler().parseMyFreeBalanceXMLReplyMessage(strXMLReply);
			mqAuditVO.setErrorMessage(myFreeBalancesReplyVO.getErrorCode());
			mqAuditVO.setServiceError(myFreeBalancesReplyVO.getErrorCode());
			MQUtility.saveMQAudit(mqAuditVO);
		}

		if (myFreeBalancesReplyVO == null)
		{
			String errorCode = ExceptionIfc.EXCEPTION_XML_ERROR;
			executionContext.log(Level.INFO, "FreeBenefitsJMSRepository > " + methodName + " : errorCode: " + errorCode, clazz, methodName, null);
			mqAuditVO.setErrorMessage(errorCode);
			mqAuditVO.setServiceError(errorCode);
			MQUtility.saveMQAudit(mqAuditVO);
			throw new BackEndException(ExceptionConstantIfc.FREE_BALANCE, errorCode);
		}
		return myFreeBalancesReplyVO;
	}
	
	
	
	
}