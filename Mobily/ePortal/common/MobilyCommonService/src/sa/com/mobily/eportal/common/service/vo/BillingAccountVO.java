/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author s.vathsavai.mit
 *
 */
public class BillingAccountVO implements Serializable {

	/**
	 * @author Mohamed Shalaby
	 */
	private static final long	serialVersionUID	= 1L;
	
	
	private String serviceAccNumber = null;
	private List<BillVO> billVOList = null;
	private String productType = null;
	private String networkConfiguration = null;
	private String kamEmail = null;
	private List<CutServiceSubAccountVO> cutServiceSubAccVOList = null;
	private List<BillingAccountVO> billingAccountVOList = null;
	private int errorCode = 0;
	private String errorMessage = null;
	
	private String name = null;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKamEmail() {
		return kamEmail;
	}
	public void setKamEmail(String kamEmail) {
		this.kamEmail = kamEmail;
	}
	public String getNetworkConfiguration() {
		return networkConfiguration;
	}
	public void setNetworkConfiguration(String networkConfiguration) {
		this.networkConfiguration = networkConfiguration;
	}
	public String getServiceAccNumber() {
		return serviceAccNumber;
	}
	public void setServiceAccNumber(String serviceAccNumber) {
		this.serviceAccNumber = serviceAccNumber;
	}
	public List<BillVO> getBillVOList() {
		return billVOList;
	}
	public void setBillVOList(List<BillVO> billVOList) {
		this.billVOList = billVOList;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	/**
	 * updated by Mohamed Shalaby to add Type Safety
	 * @return
	 */
	public List<CutServiceSubAccountVO> getCutServiceSubAccVOList() {
		return cutServiceSubAccVOList;
	}
	public void setCutServiceSubAccVOList(List<CutServiceSubAccountVO> cutServiceSubAccVOList) {
		this.cutServiceSubAccVOList = cutServiceSubAccVOList;
	}
	public List<BillingAccountVO> getBillingAccountVOList() {
		return billingAccountVOList;
	}
	public void setBillingAccountVOList(List<BillingAccountVO> billingAccountVOList) {
		this.billingAccountVOList = billingAccountVOList;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
