package sa.com.mobily.eportal.customerAccDetails.xml;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.customerAccDetails.constants.CustomerAccDetailsConstants;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsRequest.Body;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsRequest.GetRelatedAccountsInput;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsRequest.MOBILYBSLSR;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsRequest.SRHEADER;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsRequest.WSGatewayRequest;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsResponse.Account;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsResponse.Address;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsResponse.Contact;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsResponse.CustomerAccDetailsReplyVO;
import sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsResponse.CutServiceSubAccounts;
import sa.com.mobily.eportal.customerAccDetails.vo.AddressVO;
import sa.com.mobily.eportal.customerAccDetails.vo.ContactVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerAccDetailsRequestVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerAccDetailsVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerHeaderVO;
import sa.com.mobily.eportal.customerAccDetails.vo.CutServiceSubAccountsVO;
import sa.com.mobily.eportal.customerAccDetails.vo.ListOfAddressVO;
import sa.com.mobily.eportal.customerAccDetails.vo.ListOfContactVO;
import sa.com.mobily.eportal.customerAccDetails.vo.ListOfCutServiceSubAccountsVO;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.jms.CustomerInfoJMSRepository;

public class CustomerAccDetailsXMLHandler implements CustomerAccDetailsConstants
	{
	public static final String CUSTOMER_ACC_DETAILS_LOGGER_NAME ="MobilyCommonService/customerAccDetails";
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CUSTOMER_ACC_DETAILS_LOGGER_NAME);
	private static final String className = CustomerInfoJMSRepository.class.getName();

	String strDate = FormatterUtility.FormateDate(new Date());
	public  String toString(Date date, String formatDate) 
    { 
            if (date != null) 
            { 
                    SimpleDateFormat sdf = new SimpleDateFormat(formatDate, Locale.getDefault()); 
                    return sdf.format(date); 
            } 
            else 
            { 
                    return null; 
            } 
    }
	public  String generateCustomerAccDetailsXML(CustomerAccDetailsRequestVO requestVO)
	{
		String method = "generateCustomerAccDetailsXML";
		if(requestVO == null) {
			executionContext.audit(Level.SEVERE, "requestVO: " + requestVO, className, method);
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}
		String xmlRequest ="";
		try{	
			if(requestVO != null){
				MOBILYBSLSR mobilybslsr = new MOBILYBSLSR();
					SRHEADER srheader = new SRHEADER();
						srheader.setMsgFormat(FUNC_ID);
						srheader.setMsgVersion(MSG_VERSION);
						srheader.setRequestorUserId("Anonymous");
						srheader.setRequestorChannelId(REQUESTOR_CHANNEL_ID);
						srheader.setRequestorLanguage("E");
						srheader.setRequestorChannelFunction(FUNC_ID);
						srheader.setRequestorSecurityInfo(REQUESTOR_SECURITY_INFO);
						srheader.setChannelTransactionId("SR_"+strDate);
						//srheader.setSrDate(toString(Calendar.getInstance().getTime(), "yyyy-MM-dd'T'HH:mm:ss.SSSSSS+ss.ss"));
						srheader.setSrDate(strDate);
				mobilybslsr.setSRHEADER(srheader);
					WSGatewayRequest wsGatewayRequest = new WSGatewayRequest();
					Body body = new Body();
					GetRelatedAccountsInput relatedAccountsInput = new GetRelatedAccountsInput();
					relatedAccountsInput.setKeyType(FormatterUtility.checkNull(requestVO.getIdType()));
					relatedAccountsInput.setKeyValue(FormatterUtility.checkNull(requestVO.getIdNumber()));
					body.setGetRelatedAccountsInput(relatedAccountsInput);
					wsGatewayRequest.setBody(body);
					wsGatewayRequest.setWSName(WS_NAME);
				mobilybslsr.setWSGatewayRequest(wsGatewayRequest);
				
				/*try { 
	            JAXBContext jaxbContext = JAXBContext.newInstance(MOBILYBSLSR.class);
	            Marshaller jaxbMarshaller = jaxbContext.createMarshaller(); 
	            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
	            jaxbMarshaller.marshal(mobilybslsr, System.out); 
	            
	        } catch (JAXBException e) { 
	                System.out.println("JAXBException > "+e); 
	        }*/
				xmlRequest = JAXBUtilities.getInstance().marshal(mobilybslsr);	
						
			}
		}catch(Exception e){
			//executionContext.audit(Level.SEVERE, "Exception: " + e, className, method);
			throw new ServiceException(e.getMessage(), e);
		}
		//executionContext.audit(Level.INFO, "xmlRequest: " +xmlRequest, className, method);
		return xmlRequest;	
	}


	public  CustomerAccDetailsVO parseCustomerAccDetailsXML(String strXMLReply)
	{
		String method="parseCustomerAccDetailsXML";
		CustomerAccDetailsVO replyVO = new CustomerAccDetailsVO();
		CustomerAccDetailsReplyVO accDetailsReplyVO = new CustomerAccDetailsReplyVO();
		try{
			if(FormatterUtility.isNotEmpty(strXMLReply)){
				/*System.out.println("1111111");
				JAXBContext jaxbContext = JAXBContext.newInstance(CustomerAccDetailsReplyVO.class);
			    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			    System.out.println("2222222");
			    //We had written this file in marshalling example
			    accDetailsReplyVO = (CustomerAccDetailsReplyVO) jaxbUnmarshaller.unmarshal( new File("D:/newAccDetails.xml") );*/
				//for testing... ends here
			    
				//System.out.println(replyVO);
				
				//executionContext.audit(Level.INFO, "strXMLReply: " +strXMLReply, className, method);
				accDetailsReplyVO = (CustomerAccDetailsReplyVO) JAXBUtilities.getInstance().unmarshal(CustomerAccDetailsReplyVO.class, strXMLReply);
			    CustomerHeaderVO headerVO = new CustomerHeaderVO();
			    if(accDetailsReplyVO.getSRHEADERREPLY() != null){
			    	
			    	headerVO.setErrorCode(accDetailsReplyVO.getSRHEADERREPLY().getErrorCode());
			    	headerVO.setErrorMsg(accDetailsReplyVO.getSRHEADERREPLY().getErrorMsg());
			    }
			    replyVO.setHeaderVO(headerVO);
			    if(accDetailsReplyVO.getWSGatewayResponse()!= null && accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput() != null
			    		&& accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput() != null){
			    	replyVO.setErrorSpecCode(FormatterUtility.checkNull(accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput().getErrorSpcCode()));
			    	replyVO.setErrorSpecMessage(FormatterUtility.checkNull(accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput().getErrorSpcMessage()));
			    	
			    	if(accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput().getListOfEemblCustomerLightIo() != null && 
			    			accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput().getListOfEemblCustomerLightIo().getAccount()!= null){
			    		Account account = accDetailsReplyVO.getWSGatewayResponse().getGetRelatedAccountsOutput().getListOfEemblCustomerLightIo().getAccount();
			    		 
			    		replyVO.setBorderNumber(FormatterUtility.checkNull(account.getBorderNumber()));
			    		replyVO.setExceptionFlag(FormatterUtility.checkNull(account.getExceptionFlag()));
			    		replyVO.setFingerprintStatus(FormatterUtility.checkNull(account.getFingerprintStatus()));
			    		replyVO.setAccountNumber(FormatterUtility.checkNull(account.getAccountNumber()));
			    		replyVO.setEECCBlackList(FormatterUtility.checkNull(account.getEECCBlackList()));
			    		replyVO.setEECCCustomerCategory(FormatterUtility.checkNull(account.getEECCCustomerCategory()));
			    		replyVO.setEECCCustomerType(FormatterUtility.checkNull(account.getEECCCustomerType()));
			    		replyVO.setEECCEALanguagePreference(FormatterUtility.checkNull(account.getEECCEALanguagePreference()));
			    		replyVO.setEECCIDDocExpDate(FormatterUtility.checkNull(account.getEECCIDDocExpDate()));
			    		replyVO.setEECCIDDocExpHDate(FormatterUtility.checkNull(account.getEECCIDDocExpHDate()));
			    		replyVO.setEECCIDDocIssueDate(FormatterUtility.checkNull(account.getEECCIDDocIssueDate()));
			    		replyVO.setEECCIDDocIssueHDate(FormatterUtility.checkNull(account.getEECCIDDocIssueHDate()));
			    		replyVO.setEECCIDDocIssuePlace(FormatterUtility.checkNull(account.getEECCIDDocIssuePlace()));
			    		replyVO.setEECCIDDocType(FormatterUtility.checkNull(account.getEECCIDDocType()));
			    		replyVO.setEECCIDNumber(FormatterUtility.checkNull(account.getEECCIDNumber()));
			    		replyVO.setEEMBLAlelmLastSyncStatus(FormatterUtility.checkNull(account.getEEMBLAlelmLastSyncStatus()));
			    		replyVO.setAlelmOnlineOverallStatus(FormatterUtility.checkNull(account.getAlelmOnlineOverallStatus()));
			    		replyVO.setUpdatedByAlElm(FormatterUtility.checkNull(account.getUpdatedByAlElm()));
			    		replyVO.setHouseholdHeadOccupation(FormatterUtility.checkNull(account.getHouseholdHeadOccupation()));
			    		replyVO.setIntegrationId(FormatterUtility.checkNull(account.getIntegrationId()));
			    		replyVO.setLocation(FormatterUtility.checkNull(account.getLocation()));
			    		replyVO.setName(FormatterUtility.checkNull(account.getName()));
			    		replyVO.setPrimaryAddressId(FormatterUtility.checkNull(account.getPrimaryAddressId()));
			    		replyVO.setPrimaryContactId(FormatterUtility.checkNull(account.getPrimaryContactId()));
			    		replyVO.setPrimaryOrganization(FormatterUtility.checkNull(account.getPrimaryOrganization()));
			    		
			    		//Added for CCM JK19-9647
			    			//replyVO.setListOfCutServiceSubAccounts(FormatterUtility.checkNull(account.getListOfCutServiceSubAccounts()));
			    		
			    		if(account.getListOfCutServiceSubAccounts() !=null && account.getListOfCutServiceSubAccounts().getCutServiceSubAccounts()!=null &&
			    				account.getListOfCutServiceSubAccounts().getCutServiceSubAccounts().size()>0){
			    			ListOfCutServiceSubAccountsVO listOfCutServiceSubAccountsVO = new ListOfCutServiceSubAccountsVO();
			    			List<CutServiceSubAccounts> listCustomerSerSubAcc=account.getListOfCutServiceSubAccounts().getCutServiceSubAccounts();
			    			executionContext.audit(Level.INFO, "Customer list of sub accounts size["+account.getListOfCutServiceSubAccounts().getCutServiceSubAccounts().size()+"]", className, method);
			    			List<CutServiceSubAccountsVO> cutServiceSubAccountslist = new ArrayList<CutServiceSubAccountsVO>();
			    			for (CutServiceSubAccounts cutServiceSubAccounts : listCustomerSerSubAcc)
							{
			    				CutServiceSubAccountsVO serviceSubAccountsVO = new CutServiceSubAccountsVO();
			    				serviceSubAccountsVO.setAccountNumber(FormatterUtility.checkNull(cutServiceSubAccounts.getAccountNumber()));
			    				serviceSubAccountsVO.setAccountStatus(FormatterUtility.checkNull(cutServiceSubAccounts.getAccountStatus()));
			    				serviceSubAccountsVO.setCustomerAccountName(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCCustomerAccountName()));
			    				serviceSubAccountsVO.setCustomerCategory(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCCustomerCategory()));
			    				serviceSubAccountsVO.setCustomerType(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCCustomerType()));
			    				serviceSubAccountsVO.setEmailAddress(FormatterUtility.checkNull(cutServiceSubAccounts.getEmailAddress()));
			    				serviceSubAccountsVO.setFullName(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCFullName()));
			    				serviceSubAccountsVO.setIdDocType(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCIDDocType2()));
			    				serviceSubAccountsVO.setIdNumber(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCIDNumber()));
			    				serviceSubAccountsVO.setMsisdn(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCMSISDN()));
			    				serviceSubAccountsVO.setPackageId(FormatterUtility.checkNull(cutServiceSubAccounts.getEEMBLPackageId()));
			    				serviceSubAccountsVO.setPackageName(FormatterUtility.checkNull(cutServiceSubAccounts.getPackageName()));
			    				serviceSubAccountsVO.setPricingPlan(FormatterUtility.checkNull(cutServiceSubAccounts.getEECCPricingPlan()));
			    				serviceSubAccountsVO.setProductName(FormatterUtility.checkNull(cutServiceSubAccounts.getEEMBLProductName()));
			    				serviceSubAccountsVO.setRootProductCategory(FormatterUtility.checkNull(cutServiceSubAccounts.getRootProductCategory()));
			    				serviceSubAccountsVO.setRootProductLine(FormatterUtility.checkNull(cutServiceSubAccounts.getEEMBLRootProductLine()));
			    				
			    				cutServiceSubAccountslist.add(serviceSubAccountsVO);
							}
			    			listOfCutServiceSubAccountsVO.setCutServiceSubAccounts(cutServiceSubAccountslist);
			    			replyVO.setListOfCutServiceSubAccounts(listOfCutServiceSubAccountsVO);
			    		}
			    		
			    		if(account.getListOfAddress() != null && account.getListOfAddress().getAddress() != null &&
			    				account.getListOfAddress().getAddress().size() > 0){
			    			ListOfAddressVO listOfAddressVO = new ListOfAddressVO();
			    			List<Address> addresses = account.getListOfAddress().getAddress();
			    			List<AddressVO> addressVOs = new ArrayList<AddressVO>();
			    			for (Address address : addresses)
							{
								AddressVO addressVO = new AddressVO();
								addressVO.setId(FormatterUtility.checkNull(address.getId()));
								addressVO.setIsPrimaryMVG(FormatterUtility.checkNull(address.getIsPrimaryMVG()));
								addressVO.setStreetAddress(FormatterUtility.checkNull(address.getStreetAddress()));
								addressVO.setCountry(FormatterUtility.checkNull(address.getCountry()));
								addressVO.setCity(FormatterUtility.checkNull(address.getCity()));
								addressVO.setEECCPOBox(FormatterUtility.checkNull(address.getEECCPOBox()));
								addressVO.setEECCPostalCode(FormatterUtility.checkNull(address.getEECCPostalCode()));
								addressVOs.add(addressVO);
							}
			    			listOfAddressVO.setAddressVOs(addressVOs);
			    			replyVO.setListOfAddressVO(listOfAddressVO);
			    		}
			    		if(account.getListOfContact() != null && account.getListOfContact().getContact() != null &&
			    				account.getListOfContact().getContact().size() >0){
			    			ListOfContactVO listOfContactVO = new ListOfContactVO();
			    			List<Contact> contacts = account.getListOfContact().getContact();
			    			List<ContactVO> contactVOs = new ArrayList<ContactVO>();
			    			for (Contact contactVO : contacts)
							{
			    				ContactVO contact = new ContactVO();
			    				contact.setId(FormatterUtility.checkNull(contactVO.getId()));
			    				contact.setIsPrimaryMVG(FormatterUtility.checkNull(contactVO.getIsPrimaryMVG()));
			    				contact.setMM(FormatterUtility.checkNull(contactVO.getMM()));
			    				contact.setMF(FormatterUtility.checkNull(contactVO.getMF()));
			    				contact.setFirstName(FormatterUtility.checkNull(contactVO.getFirstName()));
			    				contact.setMiddleName(FormatterUtility.checkNull(contactVO.getMiddleName()));
			    				contact.setLastName(FormatterUtility.checkNull(contactVO.getLastName()));
			    				contact.setEECCNationality(FormatterUtility.checkNull(contactVO.getEECCNationality()));
			    				contact.setEECCDOBFormat(FormatterUtility.checkNull(contactVO.getEECCDOBFormat()));
			    				contact.setEECCBirthDateHijri(FormatterUtility.checkNull(contactVO.getEECCBirthDateHijri()));
			    				contact.setBirthDate(FormatterUtility.checkNull(contactVO.getBirthDate()));
			    				contact.setContactPreference(FormatterUtility.checkNull(contactVO.getContactPreference()));
			    				contact.setCellularPhone(FormatterUtility.checkNull(contactVO.getCellularPhone()));
			    				contact.setWorkPhone2(FormatterUtility.checkNull(contactVO.getWorkPhone2()));
			    				contactVOs.add(contact);
							}
			    			listOfContactVO.setContactVOs(contactVOs);
			    			replyVO.setListOfContactVO(listOfContactVO);
			    			
			    		}
			    	}
			    	
			    }
			   
			   
			    
			}
		}
		catch (Exception e)
		{
			//System.out.println(e);
			executionContext.log(Level.SEVERE, "Exception while parsing related numbers xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		return replyVO;
	}

	/*public static void main(String[] args){
		//CustomerAccDetailsVO replyVO = new CustomerAccDetailsVO();
		//System.out.println(xmlReply);
		CustomerAccDetailsRequestVO requestVO = new CustomerAccDetailsRequestVO();
		requestVO.setIdType("IQAMA");
		requestVO.setIdNumber("12345");
		
		//String xml = generateCustomerAccDetailsXML(requestVO);
		String xmlReply = "abc";
		
		System.out.println("replyVO:"+ parseCustomerAccDetailsXML(xmlReply));
	}*/
}
