/**
 * 
 */
package sa.com.mobily.eportal.cacheinstance.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * This is the General VO for Backend_Messages DB table
 * 
 * @author Mohamed Shalaby
 * 
 */
public class BackendMessage extends BaseVO
{

	/**
	 * The Default serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	private int id;

	private String key;

	private String message_en;

	private String message_ar;

	private String desc;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getMessage_en()
	{
		return message_en;
	}

	public void setMessage_en(String message_en)
	{
		this.message_en = message_en;
	}

	public String getMessage_ar()
	{
		return message_ar;
	}

	public void setMessage_ar(String message_ar)
	{
		this.message_ar = message_ar;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}
}
