//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import sa.com.mobily.eportal.core.api.CustomerProfile;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.core.service.Logger;

/**
 * Provides access to the customer profile
 * 
 * @author Hossam Omar
 * 
 */
public class CustomerProfileDAO extends AbstractDBDAO<CustomerProfile>
{
	private static final Logger log = Logger.getLogger(CustomerProfileDAO.class);

	private static final String CUSTOMER_INFO_MSISDN = "MSISDN";

	private static final String PROC_GET_CUSTOMER = "CALL SIEBEL.Customerinfo.GetCustomer(?, ?, ?)";

	/***
	 * Singleton instance of the class
	 */
	private static CustomerProfileDAO instance = new CustomerProfileDAO();

	protected CustomerProfileDAO()
	{
		super(DataSources.SIEBLE);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static CustomerProfileDAO getInstance()
	{
		return instance;
	}

	/***
	 * Gets the customer profile by MSISDN number
	 * 
	 * @param MSISDN
	 *            the MSISDN number
	 */
	public CustomerProfile getCustomerProfile(String MSISDN)
	{
		try
		{
			List<CustomerProfile> lstProfileResults = queryProcedure(PROC_GET_CUSTOMER, MSISDN, CUSTOMER_INFO_MSISDN);
			if (lstProfileResults != null && !lstProfileResults.isEmpty())
			{
				return lstProfileResults.get(0);
			}

		}
		catch (RuntimeException e)
		{
			log.error("Error retrieving customer profile for MSISDN [" + MSISDN + "]", e);
		}

		return null;
	}

	@Override
	protected CustomerProfile mapDTO(ResultSet rs) throws SQLException
	{
		CustomerProfile profile = new CustomerProfile();
		profile.setFullName(rs.getString("NAME"));
		profile.setFirstName(rs.getString("FIRSTNAME"));
		profile.setMiddleName(rs.getString("MIDNAME"));
		profile.setLastName(rs.getString("LASTNAME"));
		profile.setTitle(rs.getString("TITLE"));
		profile.setCustomerCategory(rs.getString("CUSTOMERCATEGORY"));
		profile.setCustomerType(rs.getString("CUSTOMERTYPE"));
		Date birthDate = rs.getDate("BIRTHDATE");
		if (birthDate != null)
		{
			profile.setBirthDate(new Date(birthDate.getTime()));
		}
		profile.setGender(rs.getString("GENDER"));
		profile.setNationality(rs.getString("NATIONALITY"));
		profile.setOccupation(rs.getString("OCCUPATION"));
		profile.setPackageID(rs.getString("PACKAGE_ID"));
		profile.setBillingLanguage(rs.getString("LANG_PREF"));
		profile.setIdNumber(rs.getString("IDNUMBER"));
		return profile;
	}

	public static void main(String[] args)
	{
		CustomerProfile customerProfile = CustomerProfileDAO.getInstance().getCustomerProfile("966540510267");
		// System.out.println(customerProfile);

	}
}