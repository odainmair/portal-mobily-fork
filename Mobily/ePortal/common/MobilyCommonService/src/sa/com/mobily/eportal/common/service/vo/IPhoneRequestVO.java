package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;


/**
 * 
 * @author n.gundluru.mit
 *
 */
public class IPhoneRequestVO {
	
	private String userId;
	private String password;
	private String functionId;
	private String versionId;
	private String originalHashCode;
	private String parentHashCode;
	private String parentMsisdn;
	private String originalMsisdn;
	private String customerType;
	private String packageID;
	private int lineType = 0;
	private String reqLanguage;
	private String isBlockedCustomer = "false";
	private String locale = "en"; 
	
	private String invoiceMechanism;
	private String email;
	private String action;
	private String serviceName;
	private String serviceType;
	private int itemCode;
	private String problemID;
	private String problemDesc = "";
	private String xAxis = "";
	private String yAxis = "";

	private String recipientMSISDN = "";
	private String transferredAmount = "";
	private String confirmationKey = "";	
	private String newsID;
	private String promotionId;
	private String id = "0";
	private String type = "";
	
	private String device;
	private String friendMsisdn;
	private String srId;
	private String range;
	private ArrayList partnerIdList;
	private ArrayList poiIdList;
	private String reply;
	private String channelId;
	private String contentId;
	private String pinCode;
	private String command;
	
	private String categoryId;
	private String startRow;
	private String endRow;
	private String toneId;
	private String contactNumber;
	private String contactName;
	private String toneSettingId;
	private ArrayList assignedRuleList;
	private String tonePersonalId;
	
	private String customerPlanType;
	private String serviceActNum="";
	private String fullName = "";
	private String gender = "";
	private String preferredLanguage = "";
	private String name = "";
	private String phoneNumber = "";
	private String otherPhoneNumber = "";
	private String inquiryType = "";
	private String subject = "";
	private String message = "";
	private String voucherNumber = "";
	private String deviceName;
	private String recipientId = "";
	private String infoRequired = "";
	
	private String simNumber = "";
	private String idType = "";
	private String expiryDate = "";
	private String attchedFileName = "";
	private String attchedFile = "";
	private String toneCntRequired = "";
	private String serialNo ="";
	private String serviceId = "";
	private String searchBy = "";
	private String searchCriteria = "";
	private String productId = "";
	private String contentType = "";
	private String isParent = ConstantIfc.VALUE_N;
	private String requesterChannel ="ePortal";
	private String quantity = "0";
	private String packageName = "";
	private String orderNumber = ""; 

	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getItemCode() {
	return itemCode;
	}

	public void setItemCode(int itemCode) {
	this.itemCode = itemCode;
	}

	public String getProblemDesc() {
	return problemDesc;
	}

	public void setProblemDesc(String problemDesc) {
	this.problemDesc = problemDesc;
	}

	public String getXAxis() {
	return xAxis;
	}

	public void setXAxis(String axis) {
	xAxis = axis;
	}

	public String getYAxis() {
	return yAxis;
	}

	public void setYAxis(String axis) {
	yAxis = axis;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFunctionId() {
		return functionId;
	}
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	public String getVersionId() {
		return versionId;
	}
	public void setVersionId(String versionId) {
		this.versionId = versionId;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getPackageID() {
		return packageID;
	}
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	public int getLineType() {
		return lineType;
	}
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	public String getReqLanguage() {
		return reqLanguage;
	}
	public void setReqLanguage(String reqLanguage) {
		this.reqLanguage = reqLanguage;
	}
	public String getIsBlockedCustomer() {
		return isBlockedCustomer;
	}
	public void setIsBlockedCustomer(String isBlockedCustomer) {
		this.isBlockedCustomer = isBlockedCustomer;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getOriginalHashCode() {
		return originalHashCode;
	}
	public void setOriginalHashCode(String originalHashCode) {
		this.originalHashCode = originalHashCode;
	}
	public String getParentHashCode() {
		return parentHashCode;
	}
	public void setParentHashCode(String parentHashCode) {
		this.parentHashCode = parentHashCode;
	}
	public String getParentMsisdn() {
		return parentMsisdn;
	}
	public void setParentMsisdn(String parentMsisdn) {
		this.parentMsisdn = parentMsisdn;
	}
	public String getOriginalMsisdn() {
		return originalMsisdn;
	}
	public void setOriginalMsisdn(String originalMsisdn) {
		this.originalMsisdn = originalMsisdn;
	}
	public String getInvoiceMechanism() {
		return invoiceMechanism;
	}
	public void setInvoiceMechanism(String invoiceMechanism) {
		this.invoiceMechanism = invoiceMechanism;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getRecipientMSISDN() {
		return recipientMSISDN;
	}

	public void setRecipientMSISDN(String recipientMSISDN) {
		this.recipientMSISDN = recipientMSISDN;
	}

	public String getTransferredAmount() {
		return transferredAmount;
	}

	public void setTransferredAmount(String transferredAmount) {
		this.transferredAmount = transferredAmount;
	}

	public String getConfirmationKey() {
		return confirmationKey;
	}

	public void setConfirmationKey(String confirmationKey) {
		this.confirmationKey = confirmationKey;
	}

	public String getNewsID() {
		return newsID;
	}

	public void setNewsID(String newsID) {
		this.newsID = newsID;
	}

	public String getProblemID() {
		return problemID;
	}

	public void setProblemID(String problemID) {
		this.problemID = problemID;
	}

	public String getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(String promotionId) {
		this.promotionId = promotionId;
	}

	/**
	 * @return the device
	 */
	public String getDevice() {
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(String device) {
		this.device = device;
	}

	/**
	 * @return the friendMsisdn
	 */
	public String getFriendMsisdn() {
		return friendMsisdn;
	}

	/**
	 * @param friendMsisdn the friendMsisdn to set
	 */
	public void setFriendMsisdn(String friendMsisdn) {
		this.friendMsisdn = friendMsisdn;
	}

	/**
	 * @return the srId
	 */
	public String getSrId() {
		return srId;
	}

	/**
	 * @param srId the srId to set
	 */
	public void setSrId(String srId) {
		this.srId = srId;
	}

	/**
	 * @return the range
	 */
	public String getRange() {
		return range;
	}

	/**
	 * @param range the range to set
	 */
	public void setRange(String range) {
		this.range = range;
	}

	/**
	 * @return the partnerIdList
	 */
	public ArrayList getPartnerIdList() {
		return partnerIdList;
	}

	/**
	 * @param partnerIdList the partnerIdList to set
	 */
	public void setPartnerIdList(ArrayList partnerIdList) {
		this.partnerIdList = partnerIdList;
	}

	/**
	 * @return the poiIdList
	 */
	public ArrayList getPoiIdList() {
		return poiIdList;
	}

	/**
	 * @param poiIdList the poiIdList to set
	 */
	public void setPoiIdList(ArrayList poiIdList) {
		this.poiIdList = poiIdList;
	}

	/**
	 * @return the reply
	 */
	public String getReply() {
		return reply;
	}

	/**
	 * @param reply the reply to set
	 */
	public void setReply(String reply) {
		this.reply = reply;
	}

	/**
	 * @return the channelId
	 */
	public String getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	/**
	 * @return the contentId
	 */
	public String getContentId() {
		return contentId;
	}

	/**
	 * @param contentId the contentId to set
	 */
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	/**
	 * @return the pinCode
	 */
	public String getPinCode() {
		return pinCode;
	}

	/**
	 * @param pinCode the pinCode to set
	 */
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}

	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * @return the startRow
	 */
	public String getStartRow() {
		return startRow;
	}

	/**
	 * @param startRow the startRow to set
	 */
	public void setStartRow(String startRow) {
		this.startRow = startRow;
	}

	/**
	 * @return the endRow
	 */
	public String getEndRow() {
		return endRow;
	}

	/**
	 * @param endRow the endRow to set
	 */
	public void setEndRow(String endRow) {
		this.endRow = endRow;
	}

	/**
	 * @return the toneId
	 */
	public String getToneId() {
		return toneId;
	}

	/**
	 * @param toneId the toneId to set
	 */
	public void setToneId(String toneId) {
		this.toneId = toneId;
	}

	/**
	 * @return the contactNumber
	 */
	public String getContactNumber() {
		return contactNumber;
	}

	/**
	 * @param contactNumber the contactNumber to set
	 */
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the toneSettingId
	 */
	public String getToneSettingId() {
		return toneSettingId;
	}

	/**
	 * @param toneSettingId the toneSettingId to set
	 */
	public void setToneSettingId(String toneSettingId) {
		this.toneSettingId = toneSettingId;
	}

	/**
	 * @return the contactNumbersList
	 */
	public ArrayList getAssignedRuleList() {
		return assignedRuleList;
	}

	/**
	 * @param contactNumbersList the contactNumbersList to set
	 */
	public void setAssignedRuleList(ArrayList assignedRuleList) {
		this.assignedRuleList = assignedRuleList;
	}

	/**
	 * @return the tonePersonalId
	 */
	public String getTonePersonalId() {
		return tonePersonalId;
	}

	/**
	 * @param tonePersonalId the tonePersonalId to set
	 */
	public void setTonePersonalId(String tonePersonalId) {
		this.tonePersonalId = tonePersonalId;
	}

	/**
	 * @return the customerPlanType
	 */
	public String getCustomerPlanType() {
		return customerPlanType;
	}

	/**
	 * @param customerPlanType the customerPlanType to set
	 */
	public void setCustomerPlanType(String customerPlanType) {
		this.customerPlanType = customerPlanType;
	}

	/**
	 * @return the serviceActNum
	 */
	public String getServiceActNum() {
		return serviceActNum;
	}

	/**
	 * @param serviceActNum the serviceActNum to set
	 */
	public void setServiceActNum(String serviceActNum) {
		this.serviceActNum = serviceActNum;
	}

	/**
	 * @return the fullName
	 */
	public String getFullName() {
		return fullName;
	}

	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the otherPhoneNumber
	 */
	public String getOtherPhoneNumber() {
		return otherPhoneNumber;
	}

	/**
	 * @param otherPhoneNumber the otherPhoneNumber to set
	 */
	public void setOtherPhoneNumber(String otherPhoneNumber) {
		this.otherPhoneNumber = otherPhoneNumber;
	}

	/**
	 * @return the inquiryType
	 */
	public String getInquiryType() {
		return inquiryType;
	}

	/**
	 * @param inquiryType the inquiryType to set
	 */
	public void setInquiryType(String inquiryType) {
		this.inquiryType = inquiryType;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the voucherNumber
	 */
	public String getVoucherNumber() {
		return voucherNumber;
	}

	/**
	 * @param voucherNumber the voucherNumber to set
	 */
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the recipientId
	 */
	public String getRecipientId() {
		return recipientId;
	}

	/**
	 * @param recipientId the recipientId to set
	 */
	public void setRecipientId(String recipientId) {
		this.recipientId = recipientId;
	}

	/**
	 * @return the infoRequired
	 */
	public String getInfoRequired() {
		return infoRequired;
	}

	/**
	 * @param infoRequired the infoRequired to set
	 */
	public void setInfoRequired(String infoRequired) {
		this.infoRequired = infoRequired;
	}

	/**
	 * @return the simNumber
	 */
	public String getSimNumber() {
		return simNumber;
	}

	/**
	 * @param simNumber the simNumber to set
	 */
	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}

	/**
	 * @return the idType
	 */
	public String getIdType() {
		return idType;
	}

	/**
	 * @param idType the idType to set
	 */
	public void setIdType(String idType) {
		this.idType = idType;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @param expiryDate the expiryDate to set
	 */
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	/**
	 * @return the attchedFileName
	 */
	public String getAttchedFileName() {
		return attchedFileName;
	}

	/**
	 * @param attchedFileName the attchedFileName to set
	 */
	public void setAttchedFileName(String attchedFileName) {
		this.attchedFileName = attchedFileName;
	}

	/**
	 * @return the attchedFile
	 */
	public String getAttchedFile() {
		return attchedFile;
	}

	/**
	 * @param attchedFile the attchedFile to set
	 */
	public void setAttchedFile(String attchedFile) {
		this.attchedFile = attchedFile;
	}

	/**
	 * @return the toneCntRequired
	 */
	public String getToneCntRequired() {
		return toneCntRequired;
	}

	/**
	 * @param toneCntRequired the toneCntRequired to set
	 */
	public void setToneCntRequired(String toneCntRequired) {
		this.toneCntRequired = toneCntRequired;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	/**
	 * @return the searchBy
	 */
	public String getSearchBy() {
		return searchBy;
	}

	/**
	 * @param searchBy the searchBy to set
	 */
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}

	/**
	 * @return the searchCriteria
	 */
	public String getSearchCriteria() {
		return searchCriteria;
	}

	/**
	 * @param searchCriteria the searchCriteria to set
	 */
	public void setSearchCriteria(String searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public String getxAxis()
	{
		return xAxis;
	}

	public void setxAxis(String xAxis)
	{
		this.xAxis = xAxis;
	}

	public String getyAxis()
	{
		return yAxis;
	}

	public void setyAxis(String yAxis)
	{
		this.yAxis = yAxis;
	}

	public String getProductId()
	{
		return productId;
	}

	public void setProductId(String productId)
	{
		this.productId = productId;
	}

	public String getContentType()
	{
		return contentType;
	}

	public void setContentType(String contentType)
	{
		this.contentType = contentType;
	}

	public String getIsParent()
	{
		return isParent;
	}

	public void setIsParent(String isParent)
	{
		this.isParent = isParent;
	}

	public String getRequesterChannel()
	{
		return requesterChannel;
	}

	public void setRequesterChannel(String requesterChannel)
	{
		this.requesterChannel = requesterChannel;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	public String getPackageName()
	{
		return packageName;
	}

	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}

	public String getOrderNumber()
	{
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber)
	{
		this.orderNumber = orderNumber;
	}
}