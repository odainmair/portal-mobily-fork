package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.core.api.LookupItem;
import sa.com.mobily.eportal.core.service.DataSources;

public class FavoriteCountryDAO extends AbstractDBDAO<LookupItem>
{
	private static final String QUERY_COUNTRY_CODE_EN = "SELECT TRIM(CODE) AS CODE,NAMEAR ,NAMEEN  FROM FAV_NO_CNTR_TRIF order by NAMEEN asc";
	private static final String QUERY_COUNTRY_CODE_AR = "SELECT TRIM(CODE) AS CODE,NAMEAR ,NAMEEN  FROM FAV_NO_CNTR_TRIF order by NAMEAR asc";
	
	/*
	 * private static final String  NAMEAR = "NAMEAR";
	 * private static final String  NAMEEN = "NAMEEN";
	*/
	private static FavoriteCountryDAO instance;

	protected FavoriteCountryDAO()
	{
		super(DataSources.DEFAULT);
	}

	public static synchronized FavoriteCountryDAO getInstance()
	{
		if (instance == null)
		{
			instance = new FavoriteCountryDAO();
		}
		return instance;
	}

	public List<LookupItem> getFavoriteCountries(String local)
	{
		return super.query((local == "ar" ? QUERY_COUNTRY_CODE_AR : QUERY_COUNTRY_CODE_EN ));
	}

	@Override
	protected LookupItem mapDTO(ResultSet rs) throws SQLException
	{
		LookupItem country = new LookupItem();
		if (rs.getString("CODE") != null && !rs.getString("CODE").startsWith("00"))
		{
			country.setId("00" + rs.getString("CODE").trim());// ID will be the
																// country code
		}
		else
		{
			country.setId(rs.getString("CODE"));// ID will be the country code
		}
		country.setName(rs.getString("NAMEEN"));
		country.setNameAr(rs.getString("NAMEAR"));
		return country;
	}

	/*public static void main(String[] args)
	{
		List<LookupItem> countries = FavoriteCountryDAO.getInstance().getFavoriteCountries();
		for (LookupItem lookupItem : countries)
		{
			// System.out.println(lookupItem);
		}
	}*/
}