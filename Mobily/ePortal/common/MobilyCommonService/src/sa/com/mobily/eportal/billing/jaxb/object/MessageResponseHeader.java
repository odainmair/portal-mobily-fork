package sa.com.mobily.eportal.billing.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "ReturnCode",
    "ReturnErrorMessage"
})
@XmlRootElement(name = "EE_EAI_HEADER")
public class MessageResponseHeader {

    @XmlElement(name = "ReturnCode",  required = true)
    protected String returnCode;
    @XmlElement(name = "ReturnErrorMessage")
    protected String returnErrorMessage;
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnErrorMessage() {
		return returnErrorMessage;
	}
	public void setReturnErrorMessage(String returnErrorMessage) {
		this.returnErrorMessage = returnErrorMessage;
	}


}
