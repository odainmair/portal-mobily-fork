/*
 * Created on Sep 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.io.IOException;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.SecondaryEmailReplyHeaderVO;
import sa.com.mobily.eportal.common.service.vo.SecondaryEmailReplyVO;
import sa.com.mobily.eportal.common.service.vo.SecondaryEmailRequestVO;



/**
 * @author abadr
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SkyXMLHandler implements TagIfc {

	public static String generateXMLRequestMessage(SecondaryEmailRequestVO objSecondaryEmailRequestVO) throws IOException{
		String requestXMLmessage = null;
		
        Document objDocument = new DocumentImpl();
        
        Element objElementTAG_SKY_EMAIL_REQUEST = objDocument.createElement(TAG_SKY_EMAIL_REQUEST);
        Element objElementTAG_SKY_HEADER = objDocument.createElement(TAG_SKY_HEADER);
        
        Element objElementTAG_SKY_FUNC_ID = objDocument.createElement(TAG_SKY_FUNC_ID);
        Element objElementTAG_SKY_CHANNEL_ID = objDocument.createElement(TAG_SKY_CHANNEL_ID);
        Element objElementTAG_SKY_REQUEST_DATE = objDocument.createElement(TAG_SKY_REQUEST_DATE);
        Element objElementTAG_SKY_EMAIL = objDocument.createElement(TAG_SKY_EMAIL);
        Element objElementTAG_SKY_GIVEN_NAME = objDocument.createElement(TAG_SKY_GIVEN_NAME);
        Element objElementTAG_SKY_FAMILY_NAME = objDocument.createElement(TAG_SKY_FAMILY_NAME);
        Element objElementTAG_SKY_PASSWORD = objDocument.createElement(TAG_SKY_PASSWORD);

        // Write Header Elements data

        objElementTAG_SKY_FUNC_ID.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getHeader().getFuncId()));
        objElementTAG_SKY_CHANNEL_ID.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getHeader().getChannelId()));
        objElementTAG_SKY_REQUEST_DATE.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getHeader().getRequestDate()));

        // Add HEADER sub tags
        objElementTAG_SKY_HEADER.appendChild(objElementTAG_SKY_FUNC_ID);
        objElementTAG_SKY_HEADER.appendChild(objElementTAG_SKY_CHANNEL_ID);
        objElementTAG_SKY_HEADER.appendChild(objElementTAG_SKY_REQUEST_DATE);

        // Add EE_EAI_MESSAGE sub tag
        objElementTAG_SKY_EMAIL_REQUEST.appendChild(objElementTAG_SKY_HEADER);

        // Write Element data
        objElementTAG_SKY_EMAIL.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getEmail()));
        objElementTAG_SKY_GIVEN_NAME.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getGivenName()));
        objElementTAG_SKY_FAMILY_NAME.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getFamiylName()));
        objElementTAG_SKY_PASSWORD.appendChild(objDocument.createTextNode(objSecondaryEmailRequestVO.getPassword()));

        // Add EE_EAI_MESSAGE sub tag
        objElementTAG_SKY_EMAIL_REQUEST.appendChild(objElementTAG_SKY_EMAIL);
        objElementTAG_SKY_EMAIL_REQUEST.appendChild(objElementTAG_SKY_GIVEN_NAME);
        objElementTAG_SKY_EMAIL_REQUEST.appendChild(objElementTAG_SKY_FAMILY_NAME);
        objElementTAG_SKY_EMAIL_REQUEST.appendChild(objElementTAG_SKY_PASSWORD);

       // 	Add to Document
        objDocument.appendChild(objElementTAG_SKY_EMAIL_REQUEST);

        try {
            //	Serialize
        	requestXMLmessage = XMLUtility.serializeRequest( objDocument );
        } catch(IOException e) {
            throw e;
            
        }

		return requestXMLmessage;
		
	}
	
	public static SecondaryEmailReplyVO parseXMLReplyMessage(String replyXMLmessage) throws Exception{
		SecondaryEmailReplyVO objSecondaryEmailReplyVO = new SecondaryEmailReplyVO();
		SecondaryEmailReplyHeaderVO objSecondaryEmailReplyHeaderVO = new SecondaryEmailReplyHeaderVO();
		
        Document objDocument = null;
        Element objNode = null;
	    String objTagName = null;
	    String objNodeValue = null;
	    
	    Node objRootNode = null;
	    NodeList objParentNodeList = null;
	    
	    NodeList objChildList = null;
	    Element objChildNode = null;
        String objChildNodeTagName = null;
        String objChildNodeValue = null;
        
        NodeList objSubChildList = null;
	    Element objSubChildNode = null;
        String objSubChildNodeTagName = null;
        String objSubChildNodeValue = null;
        
        //XMLException objXMLException = null;
        
	    try {
            // Get the XML object from the Reply String
            objDocument = XMLUtility.parseXMLString(replyXMLmessage);

    		objRootNode = objDocument.getElementsByTagName(TAG_SKY_EMAIL_REPLY).item(0);
    		objParentNodeList = objRootNode.getChildNodes(); 

    		for( int i = 0; i < objParentNodeList.getLength(); i++ ) {
    		    if( (objParentNodeList.item(i)).getNodeType() !=  Node.ELEMENT_NODE)
    		        continue;

    		    objNode = (Element)objParentNodeList.item(i);
    		    objTagName = objNode.getTagName();

    		    if(objNode.getFirstChild() != null) {    
    		        if( objTagName.equalsIgnoreCase(TAG_SKY_HEADER)) {
    		        	objNodeValue = objNode.getFirstChild().getNodeValue();   
    			        objChildList = objNode.getChildNodes();

    			        for(int j = 0; j < objChildList.getLength(); j++) {
    			            if((objChildList.item(j)).getNodeType() != Node.ELEMENT_NODE)
    			                continue;
    				    
    			            objChildNode = (Element)objChildList.item(j);
    			            objChildNodeTagName = objChildNode.getTagName();
    			            objChildNodeValue = "";
    			            
    					    if(objChildNode.getFirstChild() != null )
    					        objChildNodeValue = objChildNode.getFirstChild().getNodeValue();

    					    if(objChildNodeTagName.equalsIgnoreCase(TAG_SKY_FUNC_ID))
    					        objSecondaryEmailReplyHeaderVO.setFuncId(objChildNodeValue);
    					    
    					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_SKY_CHANNEL_ID))
    					        objSecondaryEmailReplyHeaderVO.setChannelId(objChildNodeValue);
    					    
    					    else if(objChildNodeTagName.equalsIgnoreCase(TAG_SKY_REQUEST_DATE)) 
    					        objSecondaryEmailReplyHeaderVO.setRequestDate(objChildNodeValue);
    					    
    			        }
    			        objSecondaryEmailReplyVO.setHeader(objSecondaryEmailReplyHeaderVO);
    			        
    	            } else if(objTagName.equalsIgnoreCase(TAG_SKY_ERROR_CODE)) {
    	            	objNodeValue = objNode.getFirstChild().getNodeValue();
    			        objSecondaryEmailReplyVO.setErrorCode(objNodeValue);
    			        
    	            }

    		    }
    		}

        } catch(Exception e) {
            throw e;
        }
		return objSecondaryEmailReplyVO;
		
	}
}
