package sa.com.mobily.eportal.common.service.mq.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AuditingAttributeVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private String columnName;
	private Object columnValue;
	private String columnType;
	
	public String getColumnName()
	{
		return columnName;
	}
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}
	public Object getColumnValue()
	{
		return columnValue;
	}
	public void setColumnValue(Object columnValue)
	{
		this.columnValue = columnValue;
	}
	public String getColumnType()
	{
		return columnType;
	}
	public void setColumnType(String columnType)
	{
		this.columnType = columnType;
	}
}
