package sa.com.mobily.eportal.common.service.valueobject.common;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}MsgFormat"/>
 *         &lt;element ref="{}MsgVersion"/>
 *         &lt;element ref="{}RequestorChannelId"/>
 *         &lt;element ref="{}RequestorChannelFunction"/>
 *         &lt;element ref="{}RequestorUserId"/>
 *         &lt;element ref="{}RequestorLanguage"/>
 *         &lt;element ref="{}RequestorSecurityInfo"/>
 *         &lt;element ref="{}ChannelTransId"/>
 *         &lt;element ref="{}Status"/>
 *         &lt;element ref="{}ErrorCode"/>
 *         &lt;element ref="{}ErrorMessage"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "msgFormat",
    "msgVersion",
    "requestorChannelId",
    "requestorChannelFunction",
    "requestorUserId",
    "requestorLanguage",
    "requestorSecurityInfo",
    "channelTransId",
    "status",
    "errorCode",
    "errorMessage"
})
@XmlRootElement(name = "SR_HEADER_REPLY")
public class ConsumerReplyHeaderVO {

    @XmlElement(name = "MsgFormat", required = true)
    protected String msgFormat;
    @XmlElement(name = "MsgVersion", required = true)
    protected String msgVersion;
    @XmlElement(name = "RequestorChannelId", required = true)
    protected String requestorChannelId;
    @XmlElement(name = "RequestorChannelFunction", required = true)
    protected String requestorChannelFunction;
    @XmlElement(name = "RequestorUserId", required = true)
    protected String requestorUserId;
    @XmlElement(name = "RequestorLanguage", required = true)
    protected String requestorLanguage;
    @XmlElement(name = "RequestorSecurityInfo", required = true)
    protected String requestorSecurityInfo;
    @XmlElement(name = "ChannelTransId", required = true)
    protected String channelTransId;
    @XmlElement(name = "Status", required = true)
    protected String status;
    @XmlElement(name = "ErrorCode", required = true)
    protected String errorCode;
    @XmlElement(name = "ErrorMessage", required = true)
    protected String errorMessage;

    /**
     * Gets the value of the msgFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgFormat() {
        return msgFormat;
    }

    /**
     * Sets the value of the msgFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgFormat(String value) {
        this.msgFormat = value;
    }

    /**
     * Gets the value of the msgVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgVersion() {
        return msgVersion;
    }

    /**
     * Sets the value of the msgVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgVersion(String value) {
        this.msgVersion = value;
    }

    /**
     * Gets the value of the requestorChannelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorChannelId() {
        return requestorChannelId;
    }

    /**
     * Sets the value of the requestorChannelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorChannelId(String value) {
        this.requestorChannelId = value;
    }

    /**
     * Gets the value of the requestorChannelFunction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorChannelFunction() {
        return requestorChannelFunction;
    }

    /**
     * Sets the value of the requestorChannelFunction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorChannelFunction(String value) {
        this.requestorChannelFunction = value;
    }

    /**
     * Gets the value of the requestorUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorUserId() {
        return requestorUserId;
    }

    /**
     * Sets the value of the requestorUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorUserId(String value) {
        this.requestorUserId = value;
    }

    /**
     * Gets the value of the requestorLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorLanguage() {
        return requestorLanguage;
    }

    /**
     * Sets the value of the requestorLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorLanguage(String value) {
        this.requestorLanguage = value;
    }

    /**
     * Gets the value of the requestorSecurityInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorSecurityInfo() {
        return requestorSecurityInfo;
    }

    /**
     * Sets the value of the requestorSecurityInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorSecurityInfo(String value) {
        this.requestorSecurityInfo = value;
    }

    /**
     * Gets the value of the channelTransId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelTransId() {
        return channelTransId;
    }

    /**
     * Sets the value of the channelTransId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelTransId(String value) {
        this.channelTransId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

}
