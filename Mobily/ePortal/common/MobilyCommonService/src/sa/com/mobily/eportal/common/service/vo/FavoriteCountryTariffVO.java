/*
 * Created on Mar 6, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FavoriteCountryTariffVO extends BaseVO
{
    private String countryCode;
    private String countryNameArabic;
    private String countryNameEnglish;
    private double tariff;
    
    
    
    public String getCountryCode()
    {
        return countryCode;
    }
    public void setCountryCode(String countryCode)
    {
        this.countryCode = countryCode;
    }
    public String getCountryNameArabic()
    {
        return countryNameArabic;
    }
    public void setCountryNameArabic(String countryNameArabic)
    {
        this.countryNameArabic = countryNameArabic;
    }
    public String getCountryNameEnglish()
    {
        return countryNameEnglish;
    }
    public void setCountryNameEnglish(String countryNameEnglish)
    {
        this.countryNameEnglish = countryNameEnglish;
    }
    public double getTariff()
    {
        return tariff;
    }
    public void setTariff(double tariff)
    {
        this.tariff = tariff;
    }
}
