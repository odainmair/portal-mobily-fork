package sa.com.mobily.eportal.billing.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER"/>
 *         &lt;element ref="{}MSISDN"/>
 *         &lt;element ref="{}ServiceAccountNumber"/>
 *         &lt;element ref="{}NumberOfBills"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "msisdn",
    "serviceAccountNumber",
    "numberOfBills"
})
@XmlRootElement(name = "MOBILY_BSL_SR")
public class LastBillRequestVO {

    @XmlElement(name = "SR_HEADER", required = true)
    protected ConsumerRequestHeaderVO headerVO;
    @XmlElement(name = "MSISDN", required = false)
    protected String msisdn;
    @XmlElement(name = "ServiceAccountNumber", required = false)
    protected String serviceAccountNumber;
    @XmlElement(name = "NumberOfBills", required = true)
    protected String numberOfBills;
    
	public ConsumerRequestHeaderVO getHeaderVO() {
		return headerVO;
	}
	public void setHeaderVO(ConsumerRequestHeaderVO headerVO) {
		this.headerVO = headerVO;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getServiceAccountNumber() {
		return serviceAccountNumber;
	}
	public void setServiceAccountNumber(String serviceAccountNumber) {
		this.serviceAccountNumber = serviceAccountNumber;
	}
	public String getNumberOfBills() {
		return numberOfBills;
	}
	public void setNumberOfBills(String numberOfBills) {
		this.numberOfBills = numberOfBills;
	}
}
