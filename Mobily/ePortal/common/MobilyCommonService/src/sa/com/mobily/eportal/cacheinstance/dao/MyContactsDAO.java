/**
 * 
 */
package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import sa.com.mobily.eportal.cacheinstance.valueobjects.ContactsVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

/**
 * @author n.gundluru.mit
 * 
 */
public class MyContactsDAO extends AbstractDBDAO<ContactsVO>
{
	public static final String FIND_ALL_BY_ACCOUNT_ID = "SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE USER_ACCT_ID = ? order by FIRST_NAME";

	public static final String FIND_ALL_BY_PHONE = "SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE PHONE LIKE '?%' OR PHONE LIKE '+?%' order by FIRST_NAME";

	private static MyContactsDAO instance = new MyContactsDAO();

	protected MyContactsDAO()
	{
		super(DataSources.DEFAULT);
	}

	public static MyContactsDAO getInstance()
	{
		return instance;
	}

	public ArrayList<ContactsVO> findContactsByAcountId(Long id)
	{
		return (ArrayList) query("SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE USER_ACCT_ID = ? order by FIRST_NAME",
				new Object[] { id });
	}

	public ArrayList<ContactsVO> findContactsByPhone(String phoneNumber)
	{
		return (ArrayList) query(
				"SELECT ID, FIRST_NAME, LAST_NAME, PHONE, EMAIL, CREATION_DATE, USER_ACCT_ID FROM CONTACTS WHERE PHONE LIKE '?%' OR PHONE LIKE '+?%' order by FIRST_NAME",
				new Object[] { phoneNumber });
	}

	protected ContactsVO mapDTO(ResultSet rs) throws SQLException
	{
		ContactsVO contact = new ContactsVO();
		contact.setId(Long.valueOf(rs.getLong("ID")));
		contact.setFirstName(rs.getString("FIRST_NAME"));
		contact.setLastName(rs.getString("LAST_NAME"));
		contact.setPhone(formatPhoneNumber(rs.getString("PHONE")));
		contact.setEmail(rs.getString("EMAIL"));
		contact.setCreationDate(rs.getTimestamp("CREATION_DATE"));
		contact.setUserAccountId(Long.valueOf(rs.getLong("USER_ACCT_ID")));
		return contact;
	}

	private String formatPhoneNumber(String phoneNumber)
	{
		phoneNumber = FormatterUtility.checkNull(phoneNumber);
		if (phoneNumber.startsWith("+"))
		{
			phoneNumber = phoneNumber.substring(1);
		}
		return phoneNumber;
	}
}
