package sa.com.mobily.eportal.common.service.ldapservice;

import java.lang.reflect.Method;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.ldap.LDAPManagerInterface;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;
import sa.com.mobily.eportal.common.service.vo.LDAPConfigVO;

public class LDAPManagerFactory
{


	private static LDAPManagerFactory instance;
	
	private static LDAPManagerFactory wifiInstance;

	private LDAPManagerInterface lDAPManagerImpl;
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	private String clazz = LDAPManagerFactory.class.getName();

	
	//Update By Rasha
	
	private LDAPManagerFactory()
	{
//		String managerType = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_MANAGER_TYPE);
		String className = "sa.com.mobily.eportal.common.util.puma.LDAPService";
//		if (managerType != null && managerType.equalsIgnoreCase(PORTAL_MODE))
//			className = "sa.com.mobily.eportal.common.util.puma.LDAPService";
//		else{
//			className = "sa.com.mobily.eportal.common.util.ldap.LDAPManager";
			//className = "sa.com.mobily.eportal.common.util.ldap.SpringLDAPManager";
//		}
		try
		{  
		    executionContext.log(Level.FINE, "loading LDAP implmentation class "+className, clazz, "LDAPManagerFactory");
			Class<?> c = Class.forName(className);
			executionContext.log(Level.FINE, "loading LDAP implmentation has been loaded successfully. ", clazz, "LDAPManagerFactory");
			Method factoryMethod = c.getDeclaredMethod("getInstance", null);
			executionContext.log(Level.FINE, "factory method has been loaded successfully "+factoryMethod, clazz, "LDAPManagerFactory");
			lDAPManagerImpl = (LDAPManagerInterface) factoryMethod.invoke(null, null);
			executionContext.log(Level.FINE, "factory method has been invoked successfully "+factoryMethod, clazz, "LDAPManagerFactory");
		}
		catch (Exception e)
		{
			executionContext.log(Level.FINE, e.getMessage(), clazz, "LDAPManagerFactory",e);
		}
	}
	
//	private LDAPManagerFactory(LDAPConfigVO ldapConfigVO)
//	{
//		String managerType = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(LDAP_MANAGER_TYPE);
//		String className = null;
//		if (managerType != null && managerType.equalsIgnoreCase(PORTAL_MODE))
//			className = "sa.com.mobily.eportal.common.util.puma.LDAPService";
//		else
//			className = "sa.com.mobily.eportal.common.util.ldap.LDAPManager";
//		try
//		{  
//		    executionContext.log(Level.FINE, "loading LDAP implmentation class "+className, clazz, "LDAPManagerFactory");
//			Class<?> c = Class.forName(className);
//			executionContext.log(Level.FINE, "loading LDAP implmentation has been loaded successfully. ", clazz, "LDAPManagerFactory");
//			  Class[] cArg = new Class[1];
//		      cArg[0] = LDAPConfigVO.class;
//			Method factoryMethod = c.getDeclaredMethod("getInstance", cArg);
//			executionContext.log(Level.FINE, "factory method has been loaded successfully "+factoryMethod, clazz, "LDAPManagerFactory");
//			lDAPManagerImpl = (LDAPManagerInterface) factoryMethod.invoke(c,ldapConfigVO);
//			executionContext.log(Level.FINE, "factory method has been invoked successfully "+factoryMethod, clazz, "LDAPManagerFactory");
//		}
//		catch (Exception e)
//		{
//			System.out.println(e);
//			executionContext.log(Level.FINE, e.getMessage(), clazz, "LDAPManagerFactory",e);
//		}
//	}
	public LDAPManagerInterface getLDAPManagerImpl()
	{
		return lDAPManagerImpl;
	}

	public static synchronized LDAPManagerFactory getInstance()
	{
		if (instance == null)
			instance = new LDAPManagerFactory();
		return instance;
	}
	
//	public static synchronized LDAPManagerFactory getInstance(LDAPConfigVO ldapConfigVO) 
//	{
//		if (wifiInstance == null) 
//			wifiInstance = new LDAPManagerFactory(ldapConfigVO);
//		return wifiInstance;
//	} 
}