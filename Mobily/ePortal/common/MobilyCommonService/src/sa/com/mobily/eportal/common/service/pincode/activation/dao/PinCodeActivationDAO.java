package sa.com.mobily.eportal.common.service.pincode.activation.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

 
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.pincode.activation.model.PinCodeActivation;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

/**
 * @author r.agarwal.mit
 */
public class PinCodeActivationDAO extends AbstractDBDAO<PinCodeActivation> {

	private static String clazz = PinCodeActivationDAO.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	
    private static PinCodeActivationDAO instance = new PinCodeActivationDAO();
    
    private static final String ACTIVATION_ADD = 
            "INSERT INTO SR_ACTIVATION_TBL " +
            "(MSISDN, ACTIVATION_CODE, PROJECT_ID, REQ_DATE) VALUES " +
            "(?, ?, ?, CURRENT_TIMESTAMP)";
    
    private static final String GET_ACTIVATION_CODE = "SELECT ACTIVATION_CODE,req_date " +
    		"FROM SR_ACTIVATION_TBL WHERE MSISDN=? AND PROJECT_ID=?";
    
    
    private static final String ACTIVATION_DELETE = 
            "DELETE FROM SR_ACTIVATION_TBL WHERE MSISDN=? AND PROJECT_ID=?";
    
    private static final String ACTIVATION_UPDATE = 
            " UPDATE SR_ACTIVATION_TBL SET REQ_DATE=CURRENT_TIMESTAMP,ACTIVATION_CODE=? WHERE MSISDN=? AND PROJECT_ID=?";
    
    protected PinCodeActivationDAO() {
        super(DataSources.DEFAULT);
    }
    
    public static synchronized PinCodeActivationDAO getInstance() {
        if (instance == null) {
            instance = new PinCodeActivationDAO();
        }
        return instance;
    }
    
    @Override
    protected PinCodeActivation mapDTO(ResultSet rs) throws SQLException {
    	LogEntryVO logEntryVO = new LogEntryVO(Level.INFO,
				"ActivationDAO > mapDTO > called", clazz, "mapDTO", null);
		executionContext.log(logEntryVO);
		
        PinCodeActivation dto = new PinCodeActivation();

        /*dto.setMsisdn(rs.getString("MSISDN"));*/
        dto.setActivationCode(rs.getString("ACTIVATION_CODE"));
        dto.setRequestDate(rs.getTimestamp("REQ_DATE"));
       /* dto.setProjectId(rs.getString("PROJECT_ID"));
        dto.setRequestDate(rs.getTimestamp("REQ_DATE"));*/
        
        logEntryVO = new LogEntryVO(Level.INFO,
				"ActivationDAO > mapDTO > dto::"+dto, clazz, "mapDTO", null);
		executionContext.log(logEntryVO);
        
        return dto;
    }
    
    public PinCodeActivation getActivationCode(String msisdn,String projectId) {
    	PinCodeActivation activationVO = null;
    	List<PinCodeActivation> activationVOList = query(GET_ACTIVATION_CODE, msisdn,projectId);
    	if(activationVOList != null && activationVOList.size() > 0) {
    		activationVO = activationVOList.get(0);
    	}
        return activationVO;
    }
   
    public int addActivationRequest(PinCodeActivation dto) {
        logger.entering("addActivationRequest");
		return update(ACTIVATION_ADD, dto.getMsisdn(), dto.getActivationCode(),
				dto.getProjectId());
    }
    
    public int updateActivationRequest(PinCodeActivation dto) {
        logger.entering("updateActivationRequest");
		int count = update(ACTIVATION_UPDATE, dto.getActivationCode(),
				dto.getMsisdn(), dto.getProjectId());
        logger.exiting("updateActivationRequest");
        return count;
    }
    
    public int deleteActivationRequest (String msisdn, String projectId) {
        return update(ACTIVATION_DELETE, new Object[] {msisdn,projectId});
    }

}
