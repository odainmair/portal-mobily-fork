/*
 * Created on Jun 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PackageTypeVO {
    private int id ;
    private String packageName_en;
    private String packageName_ar;
    
    
	public static void main(String[] args) {
	}
	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return Returns the packageName_ar.
	 */
	public String getPackageName_ar() {
		return packageName_ar;
	}
	/**
	 * @param packageName_ar The packageName_ar to set.
	 */
	public void setPackageName_ar(String packageName_ar) {
		this.packageName_ar = packageName_ar;
	}
	/**
	 * @return Returns the packageName_en.
	 */
	public String getPackageName_en() {
		return packageName_en;
	}
	/**
	 * @param packageName_en The packageName_en to set.
	 */
	public void setPackageName_en(String packageName_en) {
		this.packageName_en = packageName_en;
	}
}
