/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerStatusDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.CustomerStatusVO;
import sa.com.mobily.eportal.common.service.xml.CustomerStatusXMLHandler;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQCustomerStatusDAO implements CustomerStatusDAO {
    private static final Logger log = LoggerInterface.log;
    
    public static void main(String[] args) {
    }

    /* (non-Javadoc)
     * @see sa.com.mobily.eportal.common.service.dao.ifc.CustomerStatusDAO#getCustomerStatus(java.lang.String)
     */
    public String getCustomerStatus(String xmlMessage) {
    	String replyMessage = "";
		log.info("MQCustomerStatusDAO > getCustomerStatus > Called.................");
		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CUSTOMER_STATUS_REQUEST_QUEUENAME);
			log.debug("MQCustomerStatusDAO > getCustomerStatus > The request Queue Name Fro Customet status ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CUSTOMER_STATUS_REPLY_QUEUENAME);
			log.debug("MQCustomerStatusDAO > getCustomerStatus > The reply Queue Name Fro Customet status ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			CustomerStatusVO replyVO = null;
			  CustomerStatusXMLHandler  xmlHandler = new CustomerStatusXMLHandler();
			MQAuditVO mqAuditVO = new MQAuditVO();
			mqAuditVO.setFunctionName(ConstantIfc.CUSTSTATUS_FUNC_ID_VALUE);
			mqAuditVO.setRequestQueue(FormatterUtility.checkNull(strQueueName));
			mqAuditVO.setReplyQueue(FormatterUtility.checkNull(strReplyQueueName));
			mqAuditVO.setMessage(FormatterUtility.checkNull(xmlMessage));
			
			//replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			replyMessage= MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			
			mqAuditVO.setReply(FormatterUtility.checkNull(replyMessage));
			
			
			if (!FormatterUtility.isEmpty(replyMessage)){
				replyVO = xmlHandler.parsingXMLReplyMessage(replyMessage);
				mqAuditVO.setErrorMessage(FormatterUtility.checkNull(replyVO.getErrorMsg()));
				mqAuditVO.setServiceError(FormatterUtility.checkNull(replyVO.getErrorCode()));
			}
			
			
			if ( replyVO != null && !FormatterUtility.isEmpty(replyVO.getErrorCode())
					&& Integer.parseInt("" + replyVO.getErrorCode()) > 0) {
				String errorCode = FormatterUtility.checkNull(replyVO.getErrorCode());
				
				log.debug("MQCustomerStatusDAO > getCustomerStatus > errorCode [" + errorCode + "]");
				if(!FormatterUtility.isEmpty(replyVO.getErrorMsg())){
					errorCode = "-"+replyVO.getErrorMsg();
					mqAuditVO.setErrorMessage(errorCode);
				}
				
			}
			log.debug("MQCustomerStatusDAO > getCustomerStatus > mqAuditVO [" + mqAuditVO + "]");
			MQUtility.saveMQAudit(mqAuditVO);
			} catch (RuntimeException e) {
//			       AlarmService alarmService = new AlarmService();
//			       alarmService.RaiseAlarm(ServicesIfc.ALARM_MQ_CUSTOMER_TYPE);

				log.fatal("MQCustomerStatusDAO > getCustomerStatus > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQCustomerStatusDAO > getCustomerStatus > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQCustomerStatusDAO > getCustomerStatus > Done.................");
		return replyMessage;

    }
}
