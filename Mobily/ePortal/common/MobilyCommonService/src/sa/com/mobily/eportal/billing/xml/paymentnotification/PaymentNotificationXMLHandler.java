/**
 * 
 */
package sa.com.mobily.eportal.billing.xml.paymentnotification;

import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.BillPaymentConstants;
import sa.com.mobily.eportal.billing.jaxb.object.MyBalanceResponse;
import sa.com.mobily.eportal.billing.jaxb.paymentnotification.request.POSInfo;
import sa.com.mobily.eportal.billing.jaxb.paymentnotification.request.PaymentNotificationRequest;
import sa.com.mobily.eportal.billing.jaxb.paymentnotification.request.PaymentNotificationRequestHeader;
import sa.com.mobily.eportal.billing.jaxb.paymentnotification.response.PaymentNotificationReply;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyReplyVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyRequestVO;

/**
 * @author Suresh Vathsavai - MIT
 *
 */
public class PaymentNotificationXMLHandler implements BillPaymentConstants {

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String className = PaymentNotificationXMLHandler.class.getName();
	
	/**
	 * Description: Responsibelt to generate the payment notificaiton XML request
	 * Date: Nov 26, 2013
	 * @param requestVO
	 * @return xmlRequest as String
	 */
	public String generatePymtNotificationXMLRequest(PaymentNotifyRequestVO requestVO)	{
		String methodName = "generatePymtNotificationXMLRequest";
		executionContext.startMethod(className, methodName, null);
		if(requestVO == null) {
			executionContext.log(Level.SEVERE, "Invalid RequestVO:["+requestVO+']', className, methodName, null);
			return null;
		}
		String xmlRequest = null;
		try {
			PaymentNotificationRequest request = new PaymentNotificationRequest();
				PaymentNotificationRequestHeader header = new PaymentNotificationRequestHeader();
					header.setMsgFormat(PAYMENT_NOTIFY_MESSAGE_FORMAT_VALUE);
					header.setMsgVersion(MESSAGE_VER_VALUE);
					header.setRequestorChannelFunction(CHANNEL_FUNC_VALUE_PMTNOT);
					header.setRequestorChannelId(FormatterUtility.isEmpty(requestVO.getChannel()) ? CHANNEL_ID_VALUE_EPOR : requestVO.getChannel());
					header.setRequestorLanguage(FormatterUtility.isEmpty(requestVO.getLanguage()) ? "E" : LANG_VALUE);
					header.setRequestorSecurityInfo(SECURITY_INFO_VALUE);
					header.setRequestorUserId(FormatterUtility.checkNull(requestVO.getRequestorUserId()));
					header.setReturnCode(RETURN_CODE_VALUE);
				request.setEEEAIHEADER(header);
				
				request.setReferenceNumber(FormatterUtility.checkNull(requestVO.getReferenceNumber()));
				request.setDealerID(FormatterUtility.checkNull(requestVO.getDealerID()));
				request.setShopID(FormatterUtility.checkNull(requestVO.getShopID()));
				request.setAgentID(FormatterUtility.checkNull(requestVO.getAgentID()));
				request.setPaymentType(FormatterUtility.checkNull(requestVO.getPaymentType()));
				request.setPaymentMode(FormatterUtility.checkNull(requestVO.getPaymentMode()));
				request.setAccountNumber(FormatterUtility.checkNull(requestVO.getAccountNumber()));
				request.setBillNumber(FormatterUtility.checkNull(requestVO.getBillNumber()));
				request.setLineNumber(FormatterUtility.checkNull(requestVO.getLineNumber()));
				request.setAmount(FormatterUtility.checkNull(requestVO.getAmount()));
				request.setPaymentDateTime(FormatterUtility.checkNull(requestVO.getPaymentDateTime()));
				request.setComments(FormatterUtility.checkNull(requestVO.getComments()));
				request.setPaymentReason(FormatterUtility.checkNull(requestVO.getPaymentReason()));
				request.setDocIDNo(FormatterUtility.checkNull(requestVO.getDocIDNo()));
				request.setDocIDType(FormatterUtility.checkNull(requestVO.getDocIDType()));
				request.setCustomerName(FormatterUtility.checkNull(requestVO.getCustomerName()));
				request.setDiscountAmount(FormatterUtility.checkNull(requestVO.getDiscountAmount()));
				
				POSInfo posInfo = new POSInfo();
				posInfo.setTerminalID(FormatterUtility.checkNull(requestVO.getTerminalID()));
				posInfo.setStatusCode(FormatterUtility.checkNull(requestVO.getStatusCode()));
				posInfo.setCardNumber(FormatterUtility.checkNull(requestVO.getCardNumber()));
				posInfo.setAuthorizationCode(FormatterUtility.checkNull(requestVO.getAuthorizationCode()));
				posInfo.setRRN(FormatterUtility.checkNull(requestVO.getRrn()));
				posInfo.setCardSchemes(FormatterUtility.checkNull(requestVO.getCardSchemes()));
				
				request.setPOSInfo(posInfo);
				xmlRequest = JAXBUtilities.getInstance().marshal(request);
					
			}catch (Exception e) {
				executionContext.log(Level.SEVERE, "Exception while generatePymtNotificationXMLRequest["+e.getMessage()+"]", className, methodName, e);
				throw new XMLParserException(e.getMessage(), e);
			}
			
		executionContext.endMethod(className, methodName, null);
		return xmlRequest;
	}

	/**
	 * Description: Responsibel to parse the Payment Notification XML Reply.
	 * Date: Nov 26, 2013
	 * @param strXMLReply
	 * @return paymentNotifyReplyVO
	 */
	public PaymentNotifyReplyVO parsePymtNotificationReply(String strXMLReply) {
		String methodName = "parsePymtNotificationReply";
		executionContext.startMethod(className, methodName, null);
		PaymentNotificationReply replyObj = null;
		
		PaymentNotifyReplyVO paymentNotifyReplyVO = new PaymentNotifyReplyVO();
		try {
			if(FormatterUtility.isNotEmpty(strXMLReply)) {
				replyObj = (PaymentNotificationReply) JAXBUtilities.getInstance().unmarshal(PaymentNotificationReply.class, strXMLReply);
				if(replyObj != null) {
					if(replyObj.getEEEAIHEADER() != null)
						paymentNotifyReplyVO.setErrorCode(replyObj.getEEEAIHEADER().getReturnCode());
					
					paymentNotifyReplyVO.setPartnerRefNumber(replyObj.getPartnerReferenceNumber());
					paymentNotifyReplyVO.setReferenceNumber(replyObj.getReferenceNumber());
				}else {
					executionContext.log(Level.SEVERE, "Payment Notification XML Reply is null.", className, methodName, null);
					throw new XMLParserException();
				}
			}else {
				executionContext.log(Level.SEVERE, "Payment Notification XML Reply is null.", className, methodName, null);
				throw new XMLParserException();
			}
		} catch (Exception e){
			executionContext.log(Level.SEVERE, "Exception while parsing Payment Notification Reply:["+e.getMessage()+"]", className, methodName, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		
		executionContext.endMethod(className, methodName, null);
		return paymentNotifyReplyVO;
	}

}
