/**
 * 
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CoverageDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCoverageDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.CoverageVO;
import sa.com.mobily.eportal.common.service.vo.DestinationTypeVO;
import sa.com.mobily.eportal.common.service.xml.CoverageXMLHandler;




/**
 * 
 * @author n.gundluru.mit
 *
 */
public class CoverageCheckBO {
	
	private static final Logger log = LoggerInterface.log;
	
	/**
	 * 
	 * @param detailsVO
	 * @return
	 * @throws MobilyCommonException
	 * @throws SystemException
	 */
	public static CoverageVO getCoverageStatus(DestinationTypeVO detailsVO) throws MobilyCommonException,SystemException {

		log.info("CoverageCheckBO > getCoverageStatus > start");
		 
		CoverageVO objCoverageVO = new CoverageVO();
		int errorCode = 0;
		
        //generate XML request
        try {
            String xmlReply = "";
            CoverageXMLHandler  xmlHandler = new CoverageXMLHandler();
            String xmlRequest  = xmlHandler.generateRequestXMLMessage(detailsVO);
            log.debug("CoverageBO > getCoverageStatus > XML request > "+xmlRequest);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            CoverageDAO coverageDAO = (MQCoverageDAO)daoFactory.getMQCoverageDAO();
            
            xmlReply = coverageDAO.sendToMQWithReply(xmlRequest);
            
            //parsing the xml reply
            	objCoverageVO = xmlHandler.parsingReplyXMLMessage(xmlReply);
            
            if(objCoverageVO != null && MobilyUtility.containsOnlyNumbers(objCoverageVO.getErrorCode())){
            	errorCode = Integer.parseInt(objCoverageVO.getErrorCode());
            	log.debug("CoverageBO > getCoverageStatus > errorCode:"+objCoverageVO.getErrorCode());
            }
            log.debug("CoverageBO > getCoverageStatus > objCoverageVO.getErrorCode():"+objCoverageVO.getErrorCode());            	
            //check if the error code returned from backend greater than 0 then it will throw MobilyCommonexception object
			if( errorCode > 0 && objCoverageVO.getErrorCode()!="9999"){
				MobilyCommonException ex = new MobilyCommonException(objCoverageVO.getErrorCode());
				throw ex;
			}

        } catch (MobilyCommonException e) {
            log.fatal("CoverageBO > getCoverageStatus > MobilyCommonException > "+e.getMessage());
            throw e; 
        }catch (Exception e) {
            log.fatal("CoverageBO > getCoverageStatus >Exception > "+e.getMessage());
            throw new SystemException(e);
        }
		
        return objCoverageVO;
	}
}