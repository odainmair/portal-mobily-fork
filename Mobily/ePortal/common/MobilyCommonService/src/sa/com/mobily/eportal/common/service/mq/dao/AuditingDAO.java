package sa.com.mobily.eportal.common.service.mq.dao;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.AuditingAttributeVO;
import sa.com.mobily.eportal.common.service.mq.vo.AuditingVO;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

import com.google.gson.Gson;
import com.ibm.websphere.runtime.ServerName;

public class AuditingDAO{
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.MQ_CONNECTION_HANDLER_SERVICE_LOGGER_NAME);
	private static String serverHostAndNode = "";
	private static final String	QUEUE_NAME = "EPORTAL.AUDITING.REQUEST";
	static {
		
		try {
			InetAddress ip;
			ip = InetAddress.getLocalHost();
			serverHostAndNode=ServerName.getFullName().toString(); 
			
		  } catch (UnknownHostException e) {
			  e.printStackTrace();
		  }
	}

	/**
	 * Used to Call MDB to Audit MQ Requests
	 * @param mqAuditVO
	 */
	public void auditMQRequest(MQAuditVO mqAuditVO) {
		
		AuditingVO auditingVO = new AuditingVO();
		try {
			auditingVO.setTableName("EEDBUSR.SR_MQ_AUDIT_TBL");
			
			List<AuditingAttributeVO> auditingAttributeVOList = new ArrayList<AuditingAttributeVO>();
			
			AuditingAttributeVO auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("REQUEST_QUEUE_NAME");
			auditingAttributeVO.setColumnValue(mqAuditVO.getRequestQueue());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("REPLY_QUEUE_NAME");
			auditingAttributeVO.setColumnValue(mqAuditVO.getReplyQueue());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("MESAGE");
			auditingAttributeVO.setColumnValue(mqAuditVO.getMessage());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("CREATED_DATE");
			auditingAttributeVO.setColumnValue(new SimpleDateFormat("dd-MM-yy HH:mm:ss").format(new Date()));
			auditingAttributeVO.setColumnType("Timestamp");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("REPLY_MESSAGE");
			auditingAttributeVO.setColumnValue(mqAuditVO.getReply());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("ERROR_MESSAGE");
			auditingAttributeVO.setColumnValue(mqAuditVO.getErrorMessage());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("WAITING_TIME_SEC");
			auditingAttributeVO.setColumnValue(mqAuditVO.getWaitingTime());
			auditingAttributeVO.setColumnValue(mqAuditVO.getWaitingTime());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("ENVIRONMENT_ID");
			auditingAttributeVO.setColumnValue(serverHostAndNode);
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("ACCOUNT_NUMBER");
			auditingAttributeVO.setColumnValue(mqAuditVO.getMsisdn());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("USER_NAME");
			auditingAttributeVO.setColumnValue(mqAuditVO.getUserName());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("FUNCTION_NAME");
			auditingAttributeVO.setColumnValue(mqAuditVO.getFunctionName());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingAttributeVO = new AuditingAttributeVO();
			auditingAttributeVO.setColumnName("SERVICE_ERROR");
			auditingAttributeVO.setColumnValue(mqAuditVO.getServiceError());
			auditingAttributeVO.setColumnType("String");
			auditingAttributeVOList.add(auditingAttributeVO);
			
			auditingVO.setAuditingAttributeVOList(auditingAttributeVOList);
			
			auditRequest(auditingVO);
			
		} catch(Exception e) {
			executionContext.severe("Exception while Sending MQ request to Audit MQ::" + e);
		}

	}
	
	/**
	 * Used to Call MDB to Audit Requests
	 * @param auditingVO
	 */
	public void auditRequest(AuditingVO auditingVO) {
		Gson gson = new Gson();
		String auditRequestJsonStr = "";
		try {
			auditRequestJsonStr = gson.toJson(auditingVO);
			
			if(FormatterUtility.isNotEmpty(auditRequestJsonStr))
				MQConnectionHandler.getInstance().sendToMQWithNoAudit(auditRequestJsonStr, QUEUE_NAME);
		} catch(Exception e) {
			executionContext.severe("Exception while Sending MQ request to Audit::" + e);
		}
	}

}
