package sa.com.mobily.eportal.common.service.lookup.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * The persistent class for the SR_LOOKUP_TBL database table.
 *
 */
public class Lookup extends BaseVO {

    private static final long serialVersionUID = -123213890L;
    
    private long id;

	private long subId;
    
    private String item;
    
    private String itemDescriptionInArabic;
    
    private String itemDescriptionInEnglish;

    public Lookup() {
    }
    
    public Lookup(String item, int id, int subid) {
    	this.item = item;
    	this.id = (long)id;
    	this.subId = (long) subid;
    }
    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public long getSubId() {
        return this.subId;
    }

    public void setSubId(long subId) {
        this.subId = subId;
    }
    
    public String getCombinedId(){
    	return id+"_"+subId;
    }

    public String getItem() {
        return this.item;
    }

    public void setItem(String item) {
        this.item = item;
    }

	public String getItemDescriptionInArabic()
	{
		return itemDescriptionInArabic;
	}

	public void setItemDescriptionInArabic(String itemDescriptionInArabic)
	{
		this.itemDescriptionInArabic = itemDescriptionInArabic;
	}

	public String getItemDescriptionInEnglish()
	{
		return itemDescriptionInEnglish;
	}

	public void setItemDescriptionInEnglish(String itemDescriptionInEnglish)
	{
		this.itemDescriptionInEnglish = itemDescriptionInEnglish;
	}

	
}