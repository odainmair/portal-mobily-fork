package sa.com.mobily.eportal.corporate.model;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AccountFlags extends BaseVO
{
	private static final long serialVersionUID = 8288865088054622679L;

	private String ACCOUNT_ID;

	private String FLAG;

	private String VALUE;

	public String getACCOUNT_ID()
	{
		return ACCOUNT_ID;
	}

	public void setACCOUNT_ID(String aCCOUNT_ID)
	{
		ACCOUNT_ID = aCCOUNT_ID;
	}

	public String getFLAG()
	{
		return FLAG;
	}

	public void setFLAG(String fLAG)
	{
		FLAG = fLAG;
	}

	public String getVALUE()
	{
		return VALUE;
	}

	public void setVALUE(String vALUE)
	{
		VALUE = vALUE;
	}
}