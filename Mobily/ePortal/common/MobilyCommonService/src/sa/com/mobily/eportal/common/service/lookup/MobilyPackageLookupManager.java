/**
 * 
 */
package sa.com.mobily.eportal.common.service.lookup;

import java.util.List;
import java.util.logging.Level;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.corporate.dao.MobilyPackageCatalogLookUpDAO;
import sa.com.mobily.eportal.corporate.model.MobilyPackageCatalogLookUp;

import com.ibm.websphere.cache.DistributedMap;

/**
 * @author @moustafa.hassan.suliman - mhassan.suliman@gmail.com
 * 
 */

public class MobilyPackageLookupManager
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String clazz = MobilyPackageLookupManager.class.getName();
	private static DistributedMap  lookupsMap;
	private static MobilyPackageLookupManager instance;

	private MobilyPackageLookupManager()
	{
		try
		{
			lookupsMap=(DistributedMap) new InitialContext().lookup("mobily/package/catalog");
		}
		catch (NamingException e)
		{
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(),clazz, "getMobilyPackage", e);
		}
	}
	
	public static MobilyPackageLookupManager  getInstance(){
		if(instance==null)
			instance=new MobilyPackageLookupManager();
		return instance;
	}

	public  MobilyPackageCatalogLookUp getMobilyPackage(String packageGlobalId)
	{
		executionContext.log(Level.INFO, "Called for =["+packageGlobalId+"]", clazz, "getMobilyPackage");
		
		MobilyPackageCatalogLookUp   packageCatalog = null;
		if (null == packageGlobalId)
			return null;
		try
		{
			packageCatalog = (MobilyPackageCatalogLookUp) lookupsMap.get(packageGlobalId);
			if (null == packageCatalog)//data not found in cache 
			{
				List<MobilyPackageCatalogLookUp> lookupsList = MobilyPackageCatalogLookUpDAO.getInstance().getPackagesByPackageGlobalId(packageGlobalId);
				if (lookupsList != null && lookupsList.size() > 0)
				{ 
					packageCatalog=lookupsList.get(0);
					lookupsMap.put(packageGlobalId, packageCatalog);
				}
			}
			
		}
		catch (Exception e)
		{
			String message = FormatterUtility.checkNull(e.getMessage());
			executionContext.log(Level.SEVERE, "Exception =["+message+"]", clazz, "getMobilyPackage", e);
			
			if(message.indexOf("incompatible") > 0){
				packageCatalog = getMobilyPackageInfo(packageGlobalId);
				
			} else {
				
				throw new RuntimeException(e);
			}
		}

		return packageCatalog;
	}
	
	
	private  MobilyPackageCatalogLookUp getMobilyPackageInfo(String packageGlobalId)
	{
		executionContext.log(Level.INFO, "Called for =["+packageGlobalId+"]", clazz, "getMobilyPackageInfo");
		MobilyPackageCatalogLookUp   packageCatalog = null;
		try {
			
				List<MobilyPackageCatalogLookUp> lookupsList = MobilyPackageCatalogLookUpDAO.getInstance().getPackagesByPackageGlobalId(packageGlobalId);
				if (lookupsList != null && lookupsList.size() > 0)
				{ 
					packageCatalog=lookupsList.get(0);
					lookupsMap.put(packageGlobalId, packageCatalog);
				}
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception =[" + e.getMessage()+"]", clazz, "getMobilyPackageInfo", e);
			throw new RuntimeException(e);
		}

		return packageCatalog;
	}

}
