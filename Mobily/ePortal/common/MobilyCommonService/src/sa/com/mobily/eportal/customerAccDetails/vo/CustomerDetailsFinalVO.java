package sa.com.mobily.eportal.customerAccDetails.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CustomerDetailsFinalVO extends BaseVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomerHeaderVO headerVO;
	private String errorSpecCode = null;
	private String errorSpecMessage = null;
	private String BorderNumber = null;
	private String ExceptionFlag= null;
	private String FingerprintStatus= null;
	private String AccountNumber= null;
	private String EECCBlackList= null;
	private String EECCCustomerCategory= null;
	private String EECCCustomerType= null;
	private String EECCEALanguagePreference= null;
	private String EECCIDDocExpDate= null;
	private String EECCIDDocExpHDate= null;
	private String EECCIDDocIssueDate= null;
	private String EECCIDDocIssueHDate= null;
	private String EECCIDDocIssuePlace= null;
	private String EECCIDDocType= null;
	private String EECCIDNumber = null;
	private String EEMBLAlelmLastSyncStatus= null;
	private String AlelmOnlineOverallStatus= null;
	private String UpdatedByAlElm= null;
	private String HouseholdHeadOccupation= null;
	private String IntegrationId= null;
	private String Location= null;
	private String Name= null;
	private String PrimaryAddressId= null;
	private String PrimaryContactId= null;
	private String PrimaryOrganization= null;
	//private String ListOfCutServiceSubAccounts= null;
	private ListOfCutServiceSubAccountsVO ListOfCutServiceSubAccounts;
	private ListOfAddressVO listOfAddressVO;
	private ListOfContactVO listOfContactVO;
	
	
	
	public CustomerHeaderVO getHeaderVO()
	{
		return headerVO;
	}
	public void setHeaderVO(CustomerHeaderVO headerVO)
	{
		this.headerVO = headerVO;
	}
	
	public String getErrorSpecCode()
	{
		return errorSpecCode;
	}
	public void setErrorSpecCode(String errorSpecCode)
	{
		this.errorSpecCode = errorSpecCode;
	}
	public String getErrorSpecMessage()
	{
		return errorSpecMessage;
	}
	public void setErrorSpecMessage(String errorSpecMessage)
	{
		this.errorSpecMessage = errorSpecMessage;
	}
	public String getBorderNumber()
	{
		return BorderNumber;
	}
	public void setBorderNumber(String borderNumber)
	{
		BorderNumber = borderNumber;
	}
	public String getExceptionFlag()
	{
		return ExceptionFlag;
	}
	public void setExceptionFlag(String exceptionFlag)
	{
		ExceptionFlag = exceptionFlag;
	}
	public String getFingerprintStatus()
	{
		return FingerprintStatus;
	}
	public void setFingerprintStatus(String fingerprintStatus)
	{
		FingerprintStatus = fingerprintStatus;
	}
	public String getAccountNumber()
	{
		return AccountNumber;
	}
	public void setAccountNumber(String accountNumber)
	{
		AccountNumber = accountNumber;
	}
	public String getEECCBlackList()
	{
		return EECCBlackList;
	}
	public void setEECCBlackList(String eECCBlackList)
	{
		EECCBlackList = eECCBlackList;
	}
	public String getEECCCustomerCategory()
	{
		return EECCCustomerCategory;
	}
	public void setEECCCustomerCategory(String eECCCustomerCategory)
	{
		EECCCustomerCategory = eECCCustomerCategory;
	}
	public String getEECCCustomerType()
	{
		return EECCCustomerType;
	}
	public void setEECCCustomerType(String eECCCustomerType)
	{
		EECCCustomerType = eECCCustomerType;
	}
	public String getEECCEALanguagePreference()
	{
		return EECCEALanguagePreference;
	}
	public void setEECCEALanguagePreference(String eECCEALanguagePreference)
	{
		EECCEALanguagePreference = eECCEALanguagePreference;
	}
	public String getEECCIDDocExpDate()
	{
		return EECCIDDocExpDate;
	}
	public void setEECCIDDocExpDate(String eECCIDDocExpDate)
	{
		EECCIDDocExpDate = eECCIDDocExpDate;
	}
	public String getEECCIDDocExpHDate()
	{
		return EECCIDDocExpHDate;
	}
	public void setEECCIDDocExpHDate(String eECCIDDocExpHDate)
	{
		EECCIDDocExpHDate = eECCIDDocExpHDate;
	}
	public String getEECCIDDocIssueDate()
	{
		return EECCIDDocIssueDate;
	}
	public void setEECCIDDocIssueDate(String eECCIDDocIssueDate)
	{
		EECCIDDocIssueDate = eECCIDDocIssueDate;
	}
	public String getEECCIDDocIssueHDate()
	{
		return EECCIDDocIssueHDate;
	}
	public void setEECCIDDocIssueHDate(String eECCIDDocIssueHDate)
	{
		EECCIDDocIssueHDate = eECCIDDocIssueHDate;
	}
	public String getEECCIDDocIssuePlace()
	{
		return EECCIDDocIssuePlace;
	}
	public void setEECCIDDocIssuePlace(String eECCIDDocIssuePlace)
	{
		EECCIDDocIssuePlace = eECCIDDocIssuePlace;
	}
	public String getEECCIDDocType()
	{
		return EECCIDDocType;
	}
	public void setEECCIDDocType(String eECCIDDocType)
	{
		EECCIDDocType = eECCIDDocType;
	}
	public String getEECCIDNumber()
	{
		return EECCIDNumber;
	}
	public void setEECCIDNumber(String eECCIDNumber)
	{
		EECCIDNumber = eECCIDNumber;
	}
	public String getEEMBLAlelmLastSyncStatus()
	{
		return EEMBLAlelmLastSyncStatus;
	}
	public void setEEMBLAlelmLastSyncStatus(String eEMBLAlelmLastSyncStatus)
	{
		EEMBLAlelmLastSyncStatus = eEMBLAlelmLastSyncStatus;
	}
	public String getAlelmOnlineOverallStatus()
	{
		return AlelmOnlineOverallStatus;
	}
	public void setAlelmOnlineOverallStatus(String alelmOnlineOverallStatus)
	{
		AlelmOnlineOverallStatus = alelmOnlineOverallStatus;
	}
	public String getUpdatedByAlElm()
	{
		return UpdatedByAlElm;
	}
	public void setUpdatedByAlElm(String updatedByAlElm)
	{
		UpdatedByAlElm = updatedByAlElm;
	}
	public String getHouseholdHeadOccupation()
	{
		return HouseholdHeadOccupation;
	}
	public void setHouseholdHeadOccupation(String householdHeadOccupation)
	{
		HouseholdHeadOccupation = householdHeadOccupation;
	}
	public String getIntegrationId()
	{
		return IntegrationId;
	}
	public void setIntegrationId(String integrationId)
	{
		IntegrationId = integrationId;
	}
	public String getLocation()
	{
		return Location;
	}
	public void setLocation(String location)
	{
		Location = location;
	}
	public String getName()
	{
		return Name;
	}
	public void setName(String name)
	{
		Name = name;
	}
	public String getPrimaryAddressId()
	{
		return PrimaryAddressId;
	}
	public void setPrimaryAddressId(String primaryAddressId)
	{
		PrimaryAddressId = primaryAddressId;
	}
	public String getPrimaryContactId()
	{
		return PrimaryContactId;
	}
	public void setPrimaryContactId(String primaryContactId)
	{
		PrimaryContactId = primaryContactId;
	}
	public String getPrimaryOrganization()
	{
		return PrimaryOrganization;
	}
	public void setPrimaryOrganization(String primaryOrganization)
	{
		PrimaryOrganization = primaryOrganization;
	}
	
	public ListOfAddressVO getListOfAddressVO()
	{
		return listOfAddressVO;
	}
	public void setListOfAddressVO(ListOfAddressVO listOfAddressVO)
	{
		this.listOfAddressVO = listOfAddressVO;
	}
	public ListOfContactVO getListOfContactVO()
	{
		return listOfContactVO;
	}
	public void setListOfContactVO(ListOfContactVO listOfContactVO)
	{
		this.listOfContactVO = listOfContactVO;
	}
	/**
	 * @return the listOfCutServiceSubAccounts
	 */
	public ListOfCutServiceSubAccountsVO getListOfCutServiceSubAccounts()
	{
		return ListOfCutServiceSubAccounts;
	}
	/**
	 * @param listOfCutServiceSubAccounts the listOfCutServiceSubAccounts to set
	 */
	public void setListOfCutServiceSubAccounts(ListOfCutServiceSubAccountsVO listOfCutServiceSubAccounts)
	{
		ListOfCutServiceSubAccounts = listOfCutServiceSubAccounts;
	}
	
	
	

}
