package sa.com.mobily.eportal.common.service.vo;

public class LongVO extends FootPrintVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long value;

	public Long getValue()
	{
		return value;
	}

	public void setValue(Long value)
	{
		this.value = value;
	}
}