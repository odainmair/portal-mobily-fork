package sa.com.mobily.eportal.google.captcha.util;

public class GoogleCaptchaConstants{

	
	public final static String CAPTCHA_PRIVATE_KEY = "CAPTCHA_PRIVATE_KEY";
	public final static String CAPTCHA_PUBLIC_KEY = "CAPTCHA_PUBLIC_KEY";
	public final static String CAPTCHA_HOST_NAME = "CAPTCHA_HOST_NAME";
	public final static String CAPTCHA_PROXY_HOST_NAME = "CAPTCHA_PROXY_HOST_NAME";
	public final static String CAPTCHA_HOST_PORT = "CAPTCHA_HOST_PORT";
	public final static String CAPTCHA_KEYS_FROM_SERVER = "CAPTCHA_KEYS_FROM_SERVER";
	public final static String SERVER_DOMAIN_URL = "server.domain.url";
	
	public static final String RESOURCE_ENV_CAPTCHA_PUBLIC_KEY = "captchaPublicKey";
    public static final String RESOURCE_ENV_CAPTCHA_PRIVATE_KEY = "captchaPrivateKey";
    public static final String PUBLIC_KEY = "publicKey";
    
    public static final String CAPTCHA_CONFIGS = "google_config";
    
    //google recaptchav2
    public final static String CAPTCHA_V2_SITE_KEY = "CAPTCHA_V2_SITE_KEY";
    public final static String CAPTCHA_V2_SECRET_KEY = "CAPTCHA_V2_SECRET_KEY";
    public final static String CAPTCHA_V2_REMOTE_SERVER_IP = "CAPTCHA_V2_REMOTE_SERVER_IP";

    
}
