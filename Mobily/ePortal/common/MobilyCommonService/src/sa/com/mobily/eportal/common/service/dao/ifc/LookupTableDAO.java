/*
 * Created on Feb 26, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface LookupTableDAO {
    public  ArrayList getLookupByID(String id);
    
    public   Hashtable loadLookupFromDB() ;
    public Hashtable loadCountriesByColumn(String columnName);
}
