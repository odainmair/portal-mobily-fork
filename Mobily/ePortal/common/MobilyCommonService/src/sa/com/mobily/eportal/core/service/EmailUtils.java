package sa.com.mobily.eportal.core.service;

import java.io.IOException;
import java.util.List;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

import sa.com.mobily.eportal.core.api.ErrorCodes;
import sa.com.mobily.eportal.core.api.File;
import sa.com.mobily.eportal.core.api.MailMessage;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.service.EPortalUtils.EPortalProperty;

public class EmailUtils {
    
    /***
     * The default separator used between email addresses
     */
    public static final String DEFAULT_MAIL_SEPARATOR = ";";
    private static final String DEFAULT_MAIL_ENCODING = "UTF-8";
    
    private static final Logger log = Logger.getLogger(EmailUtils.class);

    /***
     * Sends the mail message
     * 
     * @param mailMsg
     *            The mail message
     * @throws ServiceException 
     */
    public static void sendMail(MailMessage mailMsg) throws ServiceException {
	sendMail(mailMsg, false);
    }
    
    /***
     * Sends the mail message as HTML
     * 
     * @param mailMsg
     *            the mail message
     * @throws ServiceException 
     */
    public static void sendHtmlMail(MailMessage mailMsg) throws ServiceException {
	sendMail(mailMsg, true);
    }

    /***
     * Handles sending simple mail, mail with attachment and Html mail
     * 
     * @param mailMessage
     *            the mail message
     * @param isHtmlMail
     *            if the mail message is HTML or not
     * @throws ServiceException 
     */
    protected static void sendMail(MailMessage mailMessage, boolean isHtmlMail) throws ServiceException {
	Email email;
	List<File> attachments = mailMessage.getAttachements();
	
	try {
	    
	    // if the mail has attachments
	    if (attachments != null && !attachments.isEmpty()) {
		MultiPartEmail emailWithAttachment = isHtmlMail? new HtmlEmail() : new MultiPartEmail();	// create an HTML Mail or a multi-part mail

		// add the attachments
		for (File attachment : attachments) {
		    ByteArrayDataSource dsAttachment = new ByteArrayDataSource(attachment.getData(), attachment.getContentType());
		    emailWithAttachment.attach(dsAttachment, attachment.getFileName(), attachment.getFileName());
		}
		
		email = emailWithAttachment;
	    } if(isHtmlMail) {	// Html mail without attachments
		email = new HtmlEmail();
	    } else {	// if mail has no attachments
		email = new SimpleEmail();	// create a simple mail
	    }
            
            email.setCharset(DEFAULT_MAIL_ENCODING);

            email.setHostName(EPortalUtils.getProperty(EPortalProperty.MAIL_SMTP_HOST));
            log.debug("ApacheMailSender > sendEmailWithAttachement > SMTP HOST : "+ email.getHostName());
            email.setSubject(mailMessage.getSubject());
            email.setFrom(mailMessage.getFrom());
            email.setMsg(mailMessage.getBody());

            // add the To mails
            if (mailMessage.getTo() != null) {
        	for (String toMail : mailMessage.getTo()) {
	            email.addTo(toMail);
                }
            }
            
            // add hte CC mails
            if (mailMessage.getCc() != null) {
        	for (String ccMail : mailMessage.getCc()) {
	            email.addTo(ccMail);
                }
            }

            email.send();
            log.info("ApacheMailSender > sendEmailWithAttachement > mail Sent successfuly");
        } catch (java.net.UnknownHostException e) {
            log.error("Failed to send mail (Unknown Host).", e);
            // TODO: send proper error codes
            throw new ServiceException("Failed to send mail (Unknown Host)", ErrorCodes.MAIL_SEND_FAILED, e);
        } catch (IOException e) {
            log.error("Failed to send mail (IOException)", e);
            // TODO: send proper error codes
            throw new ServiceException("Failed to send mail (IOException)", ErrorCodes.MAIL_SEND_FAILED, e);
        } catch (EmailException e) {
            log.error("Failed to send mail (EmailException)", e);
            // TODO: send proper error codes
            throw new ServiceException("Failed to send mail (EmailException)", ErrorCodes.MAIL_SEND_FAILED, e);
        } catch (Exception e) {
            log.error("Failed to send mail (General Exception)", e);
            // TODO: send proper error codes
            throw new ServiceException("Failed to send mail (General Exception)", ErrorCodes.MAIL_SEND_FAILED, e);
        }
    }

    /***
     * Verifies that the passed attachment is valid
     * 
     * @param attachment
     * 			the attachment to be validated
     * @return	true if the attachment is valid, false otherwise
     */
    public static boolean isAttachmentValid(File attachment) {
    
        String extension = "";
        String fileName = attachment.getFileName();
    
        // get the attachment extension
        int lastDotIndex = fileName.lastIndexOf(".");
        if (lastDotIndex != 0) {
            extension = fileName.substring(lastDotIndex) + 1;
        }
    
        // validate the extension is in the allowed list
        String[] allowedExtensions = EPortalUtils
                .getPropertyValues(EPortalProperty.ALLOWED_UPLOAD_EXTENSIONS);
        for (String ext : allowedExtensions) {
            if (ext.equals(extension)) {
        	return true;
            }
        }
    
        // extension is not in the allowed list
        return false;
    }
    
    

}
