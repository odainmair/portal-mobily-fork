package sa.com.mobily.eportal.common.exception.application;


public class BackEndException extends MobilyApplicationException {

	private static final long serialVersionUID = 9076037971591882203L;
	
	private String errorCode;
	
	private String errorMessage;
	
	private int serviceID;

	public BackEndException() {
	}

	public BackEndException(Throwable cause) {
		super(cause);
	}

	public BackEndException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public BackEndException(int serviceID,String errorCode) {
		this.errorCode = errorCode;
		this.serviceID = serviceID;
	}
	
	public BackEndException(int serviceID , String errorCode , String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.serviceID = serviceID;
	}
	
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public int getServiceID() {
		return serviceID;
	}

	public void setServiceID(int serviceID) {
		this.serviceID = serviceID;
	}
}