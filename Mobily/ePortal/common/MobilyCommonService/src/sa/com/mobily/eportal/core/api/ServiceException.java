//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;


/**
 * This is the base class of all exceptions thrown from the ResourceManager EJB.
 * @see sa.com.mobily.eportal.core.api.ResourceManager
 */
public class ServiceException extends Exception {

    private static final long serialVersionUID = 1L;

    private int errorCode;
    private Object[] messageParams;
    
    public ServiceException(String message) {
        this(message, ErrorCodes.GENERAL_ERROR);
    }
    
    public ServiceException(String message, Object[] messageParams) {
        this(message, ErrorCodes.GENERAL_ERROR);
        this.messageParams = messageParams;
    }

    public ServiceException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }
    
    public ServiceException(String message, int errorCode, Object[] messageParams) {
        super(message);
        this.errorCode = errorCode;
        this.messageParams = messageParams;
    }

    public ServiceException(String message, Throwable cause) {
        this(message, ErrorCodes.GENERAL_ERROR, cause);
    }

    public ServiceException(String message, int errorCode, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }
    
    public ServiceException(String message, int errorCode, Throwable cause, Object[] messageParams) {
        super(message, cause);
        this.errorCode = errorCode;
        this.messageParams = messageParams;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public Object[] getMessageParams() {
        return messageParams;
    }
}
