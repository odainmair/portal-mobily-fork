package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.io.Serializable;

public class ServiceAccountNumber extends AccountNumber implements Serializable, Cloneable {
	
	public ServiceAccountNumber(String serviceAccountNumber){
		this.number = serviceAccountNumber;
	}

	private String number;
	
	@Override
	public void setNumber(String accountNumber) {
		this.number = accountNumber;

	}

	@Override
	public String getNumber() {
		return number;
	}

}
