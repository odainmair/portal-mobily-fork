/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EBillReplyHeaderVO {
	private String FuncId = null;
	private String MsgVersion = null;
	private String RequestorChannelId = null; 
	private String SrDate = null;
	private String SrRcvDate = null;
	private String SrStatus = null;

	/**
	 * @return Returns the funcId.
	 */
	public String getFuncId() {
		return FuncId;
	}
	/**
	 * @param funcId The funcId to set.
	 */
	public void setFuncId(String funcId) {
		FuncId = funcId;
	}
	/**
	 * @return Returns the msgVersion.
	 */
	public String getMsgVersion() {
		return MsgVersion;
	}
	/**
	 * @param msgVersion The msgVersion to set.
	 */
	public void setMsgVersion(String msgVersion) {
		MsgVersion = msgVersion;
	}
	/**
	 * @return Returns the requestorChannelId.
	 */
	public String getRequestorChannelId() {
		return RequestorChannelId;
	}
	/**
	 * @param requestorChannelId The requestorChannelId to set.
	 */
	public void setRequestorChannelId(String requestorChannelId) {
		RequestorChannelId = requestorChannelId;
	}
	/**
	 * @return Returns the srDate.
	 */
	public String getSrDate() {
		return SrDate;
	}
	/**
	 * @param srDate The srDate to set.
	 */
	public void setSrDate(String srDate) {
		SrDate = srDate;
	}
	/**
	 * @return Returns the srRcvDate.
	 */
	public String getSrRcvDate() {
		return SrRcvDate;
	}
	/**
	 * @param srRcvDate The srRcvDate to set.
	 */
	public void setSrRcvDate(String srRcvDate) {
		SrRcvDate = srRcvDate;
	}
	/**
	 * @return Returns the srStatus.
	 */
	public String getSrStatus() {
		return SrStatus;
	}
	/**
	 * @param srStatus The srStatus to set.
	 */
	public void setSrStatus(String srStatus) {
		SrStatus = srStatus;
	}
}
