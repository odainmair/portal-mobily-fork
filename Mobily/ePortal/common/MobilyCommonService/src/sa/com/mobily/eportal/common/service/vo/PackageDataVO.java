/*
 * Created on Feb 25, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class PackageDataVO extends BaseVO {
	
	private String packageId = null;

	private String packageNameEn = null;
	private String packageNameAr = null;

	private int nationalFVNCount;
	private int internationalFVNCount;
	private int familyAndFriendsCount;
	private int default_package_id;
	private int mobilyFVNCount;
	private int mixFVNCount;
	private int countryFVNCount;


	

	/**
	 * @param packageId
	 * @param packageNameEn
	 * @param packageNameAr
	 * @param nationalFVNCount
	 * @param internationalFVNCount
	 * @param familyAndFriendsCount
	 */
	public PackageDataVO(String packageId, String packageNameEn,
			String packageNameAr, int nationalFVNCount,
			int internationalFVNCount, int familyAndFriendsCount) {
		super();
		this.packageId = packageId;
		this.packageNameEn = packageNameEn;
		this.packageNameAr = packageNameAr;
		this.nationalFVNCount = nationalFVNCount;
		this.internationalFVNCount = internationalFVNCount;
		this.familyAndFriendsCount = familyAndFriendsCount;
	}

	/**
	 * 
	 * @param packageId
	 * @param packageNameEn
	 * @param packageNameAr
	 * @param nationalFVNCount
	 * @param internationalFVNCount
	 * @param familyAndFriendsCount
	 * @param mobilyFVNCount
	 * @param mixFVNCount
	 */
	public PackageDataVO(String packageId, String packageNameEn,
			String packageNameAr, int nationalFVNCount,
			int internationalFVNCount, int familyAndFriendsCount,
			int default_package_id, int mobilyFVNCount, int mixFVNCount) {
		
		super();
		
		this.packageId				= packageId;
		this.packageNameEn			= packageNameEn;
		this.packageNameAr			= packageNameAr;
		this.nationalFVNCount		= nationalFVNCount;
		this.internationalFVNCount 	= internationalFVNCount;
		this.familyAndFriendsCount 	= familyAndFriendsCount;
		this.mobilyFVNCount 		= mobilyFVNCount;
		this.mixFVNCount 			= mixFVNCount;
		this.default_package_id 	= default_package_id;
	}
	
	
	/**
	 *  
	 */
	public PackageDataVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return Returns the familyAndFriendsCount.
	 */
	public int getFamilyAndFriendsCount() {
		return familyAndFriendsCount;
	}

	/**
	 * @param familyAndFriendsCount
	 *            The familyAndFriendsCount to set.
	 */
	public void setFamilyAndFriendsCount(int familyAndFriendsCount) {
		this.familyAndFriendsCount = familyAndFriendsCount;
	}

	/**
	 * @return Returns the internationalFVNCount.
	 */
	public int getInternationalFVNCount() {
		return internationalFVNCount;
	}

	/**
	 * @param internationalFVNCount
	 *            The internationalFVNCount to set.
	 */
	public void setInternationalFVNCount(int internationalFVNCount) {
		this.internationalFVNCount = internationalFVNCount;
	}

	/**
	 * @return Returns the nationalFVNCount.
	 */
	public int getNationalFVNCount() {
		return nationalFVNCount;
	}

	/**
	 * @param nationalFVNCount
	 *            The nationalFVNCount to set.
	 */
	public void setNationalFVNCount(int nationalFVNCount) {
		this.nationalFVNCount = nationalFVNCount;
	}

	/**
	 * @return Returns the packageId.
	 */
	public String getPackageId() {
		return packageId;
	}

	/**
	 * @param packageId
	 *            The packageId to set.
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * @return Returns the packageNameAr.
	 */
	public String getPackageNameAr() {
		return packageNameAr;
	}

	/**
	 * @param packageNameAr
	 *            The packageNameAr to set.
	 */
	public void setPackageNameAr(String packageNameAr) {
		this.packageNameAr = packageNameAr;
	}

	/**
	 * @return Returns the packageNameEn.
	 */
	public String getPackageNameEn() {
		return packageNameEn;
	}

	/**
	 * @param packageNameEn
	 *            The packageNameEn to set.
	 */
	public void setPackageNameEn(String packageNameEn) {
		this.packageNameEn = packageNameEn;
	}
	/**
	 * @return Returns the mixFVNCount.
	 */
	public int getMixFVNCount() {
		return mixFVNCount;
	}
	/**
	 * @param mixFVNCount The mixFVNCount to set.
	 */
	public void setMixFVNCount(int mixFVNCount) {
		this.mixFVNCount = mixFVNCount;
	}
	/**
	 * @return Returns the mobilyFVNCount.
	 */
	public int getMobilyFVNCount() {
		return mobilyFVNCount;
	}
	/**
	 * @param mobilyFVNCount The mobilyFVNCount to set.
	 */
	public void setMobilyFVNCount(int mobilyFVNCount) {
		this.mobilyFVNCount = mobilyFVNCount;
	}
	
	/**
	 * @return Returns the default_package_id.
	 */
	public int getDefault_package_id() {
		return default_package_id;
	}
	/**
	 * @param default_package_id The default_package_id to set.
	 */
	public void setDefault_package_id(int default_package_id) {
		this.default_package_id = default_package_id;
	}

	public int getCountryFVNCount() {
		return countryFVNCount;
	}

	public void setCountryFVNCount(int countryFVNCount) {
		this.countryFVNCount = countryFVNCount;
	}
	
	
}
