package sa.com.mobily.eportal.common.service.mq;

import javax.jms.JMSException;

import javax.naming.NamingException;

import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;

import com.mobily.exception.mq.MQSendException;



public interface MobilyJMS {
	
	/**
	 * This Method is responsible for Sending Message to Certain Queue Name
	 * and not waiting for the  reply on specific Queue Name in QueueManager Name
	 * @param msg
	 * @param queueName
	 * @param replyQueueName
	 * @param replyToQueueManagerName
	 * @return
	 * @throws MQSendException
	 */
	public void sendMessageOnly(String msg, String queueName, String replyQueueName, String replyToQueueManagerName);
	
	/**
	 * This Method is responsible for Sending Message to Certain Queue Name
	 * and waiting for the reply on specific Queue Name in QueueManager Name
	 * @param msg
	 * @param queueName
	 * @param replyQueueName
	 * @return
	 * @throws MQSendException
	 */
	public String sendToMQWithReply(String msg, String queueName, String replyQueueName);
	

	/**
	 * This Method is responsible for Sending Message with CorrelationID to Certain Queue Name
	 * 
	 * @param msg
	 * @param queueName (JMS Queue name contains the    queue Manager name eg: queue://QM2/Q2?persistence=2&priority=5
	 * @return
	 * @throws MQSendException
	 */	
	public void respondWithCorrelationID(String msg, String queueName, byte[] CorrelationID);
				
	/**
	 * This Method is responsible for Sending Message to Certain Queue Name
	 * and waiting for the reply on specific Queue Name in QueueManager Name
	 * @param msg
	 * @param queueName
	 * @param replyQueueName
	 * @return
	 * @throws MQSendException
	 */
	public String sendToMQWithReply(MQAuditVO mqAuditVO);


	/**
	 * this method is responsible for Sending Message to certain queue without
	 * waiting any reply 
	 * @param msg
	 * @param queuName
	 * @throws MQSendException
	 * @throws ServiceLocatorException 
	 * @throws NamingException 
	 * @throws JMSException 
	 */
	public void sendToMQ(String msg, String queuName);

	void respondWithCorrelationID(MQAuditVO mqAuditVO);

	void sendToMQ(MQAuditVO mqAuditVO);
	
	void sendToMQWithNoAudit(String msg, String queuName);
}
