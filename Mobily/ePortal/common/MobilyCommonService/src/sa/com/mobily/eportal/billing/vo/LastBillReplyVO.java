package sa.com.mobily.eportal.billing.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerReplyHeaderVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER_REPLY"/>
 *         &lt;element ref="{}MSISDN"/>
 *         &lt;element ref="{}ServiceAccountNumber"/>
 *         &lt;element ref="{}AccountPOID"/>
 *         &lt;element ref="{}Results"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "msisdn",
    "serviceAccountNumber",
    "accountPOID",
    "results"
})
@XmlRootElement(name = "MOBILY_BSL_SR_REPLY")
public class LastBillReplyVO {

    @XmlElement(name = "SR_HEADER_REPLY", required = true)
    protected ConsumerReplyHeaderVO headerVO;
    
    @XmlElement(name = "MSISDN", required = false)
    protected String msisdn;
    
    @XmlElement(name = "ServiceAccountNumber", required = false)
    protected String serviceAccountNumber;
    
    @XmlElement(name = "AccountPOID", required = false)
    protected String accountPOID;
    
    @XmlElement(name = "Results", required = false)
    protected LastBillResultsListVO results;
    
	public ConsumerReplyHeaderVO getHeaderVO() {
		return headerVO;
	}
	public void setHeaderVO(ConsumerReplyHeaderVO headerVO) {
		this.headerVO = headerVO;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getServiceAccountNumber() {
		return serviceAccountNumber;
	}
	public void setServiceAccountNumber(String serviceAccountNumber) {
		this.serviceAccountNumber = serviceAccountNumber;
	}
	public String getAccountPOID() {
		return accountPOID;
	}
	public void setAccountPOID(String accountPOID) {
		this.accountPOID = accountPOID;
	}
	public LastBillResultsListVO getResults() {
		return results;
	}
	public void setResults(LastBillResultsListVO results) {
		this.results = results;
	}
}
