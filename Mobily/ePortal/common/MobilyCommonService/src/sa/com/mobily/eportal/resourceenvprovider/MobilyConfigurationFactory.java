package sa.com.mobily.eportal.resourceenvprovider;

import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

/**
 * 
 * @author Muzammil Mohsin Shaikh
 * 
 */
public class MobilyConfigurationFactory implements ObjectFactory
{

	private static MobilyConfiguration config = null;

	@Override
	public Object getObjectInstance(Object object, Name name, Context context, Hashtable<?, ?> environment) throws Exception
	{
		if (config == null)
		{
			config = new MobilyConfiguration();
			Reference ref = (Reference) object;
			Enumeration<RefAddr> addrs = ref.getAll();
			while (addrs.hasMoreElements())
			{
				RefAddr addr = (RefAddr) addrs.nextElement();
				String entryName = addr.getType();
				String value = (String) addr.getContent();
				config.setAttribute(entryName, value);
			}
		}

		return config;
	}

}
