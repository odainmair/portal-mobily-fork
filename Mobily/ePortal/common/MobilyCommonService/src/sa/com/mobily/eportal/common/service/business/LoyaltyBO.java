/**
 * @date: Dec 28, 2011
 * @author: s.vathsavai.mit
 * @file name: LoyaltyBO.java
 * @description:  
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;


import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.LoyaltyDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.vo.LoyaltyRequestVO;
import sa.com.mobily.eportal.common.service.xml.LoyaltyXMLHandler;


/**
 * @author s.vathsavai.mit
 *
 */
public class LoyaltyBO {
	 
	private static final Logger log = LoggerInterface.log;
	
	/**
	 * @date: Dec 28, 2011
	 * @author: s.vathsavai.mit
	 * @description: 
	 * @param loyaltyRequestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public boolean doNeqatyPlusCorporateLevelInquiry(LoyaltyRequestVO loyaltyRequestVO) {
		log.info("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry :: start");
		boolean isSubscribed = false;
		
		try {
			
			if(loyaltyRequestVO == null) {
				log.fatal("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry : loyaltyRequestVO is null.");
				throw new SystemException();
			}
			
			if(FormatterUtility.isEmpty(loyaltyRequestVO.getBillingAccountNumber())) {
				log.fatal("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry : billing account number is null or empty.");
				throw new SystemException();
			}
			
			LoyaltyXMLHandler loyaltyXMLHandler = new LoyaltyXMLHandler();
			DAOFactory daoFactory = DAOFactory.getDAOFactory( DAOFactory.MQ );
			LoyaltyDAO loyaltyDAO = daoFactory.getLoyaltyDAO();
			
			String xmlRequest = loyaltyXMLHandler.generateCorporateLevelInquiryRequest(loyaltyRequestVO);
			log.debug("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry  xmlRequest="+xmlRequest);
			boolean testing = false;
			log.debug("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry  testing="+testing);
			String xmlReply = "";
			//testing
			if(testing)
				xmlReply = "<MOBILY_LOYALTY_SR_REPLY><SR_HEADER_REPLY><FuncId> LOYALTY_CORPORATE_INQUIRY</FuncId><SecurityKey></SecurityKey><MsgVersion>0000</MsgVersion><RequestorChannelId>XXX</RequestorChannelId><ServiceRequestId>SR_1234567890</ServiceRequestId>" +
						"<SrDate>20090904091557</SrDate><SrStatus>2</SrStatus></SR_HEADER_REPLY><ReturnCode><ErrorCode>0</ErrorCode></ReturnCode><NEQATYPLUS_INFO><RootAccountNumber>100013835312648</RootAccountNumber><TotalNoOfLines>120</TotalNoOfLines><CurrentBalance>1000</CurrentBalance>" +
						"<TotalPointsEarned>200.50</TotalPointsEarned><TotalPointsLost>100.30</TotalPointsLost><TotalPointsExpired>40.75</TotalPointsExpired></NEQATYPLUS_INFO></MOBILY_LOYALTY_SR_REPLY>";
			else
				xmlReply = loyaltyDAO.doNeqatyPlusCorporateLevelInquiry(xmlRequest);
		    log.debug("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry : xmlReply = "+xmlReply);
			
			String errorCode = loyaltyXMLHandler.parseCorporateLoyaltyReply(xmlReply);
		    log.debug("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry : errorCode = "+errorCode);
		    
		    if(!(Integer.parseInt(errorCode) > 0)) {
		    	isSubscribed = true;
	    	}
		}catch(SystemException systemException) {
			log.fatal("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry :: SystemException for billing acc num "+loyaltyRequestVO.getBillingAccountNumber()+" is "+systemException);
			throw systemException;
			
		}catch (Exception exception) {
			log.fatal("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry: Exception for billing acc num "+loyaltyRequestVO.getBillingAccountNumber()+" is "+exception);
			throw new SystemException(exception);
		}
	    log.info("LoyaltyBO > doNeqatyPlusCorporateLevelInquiry :: end: billing accountnumber:"+loyaltyRequestVO.getBillingAccountNumber()+"... isSubscribed :"+isSubscribed);
	    return isSubscribed;
	}
}
