/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CustomerTypeVO;
import sa.com.mobily.eportal.common.service.vo.MessageHeaderVO;

/**
 * @author msayed
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CustomerTypeXMLHandler {
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;

    /**
     * Generate the XML request for CustomerType
     * 
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     *             Feb 17, 2008 CustomerTypeXMLHandler.java msayed
     */
    public String generateXMLRequest(CustomerTypeVO requestVO)
            throws MobilyCommonException {
    	if(requestVO.getLineNumber()!=null)
    		log.info("CustomerTypeXMLHandler > generateXMLRequest > for Customer  ["+requestVO.getLineNumber()+"] Called");
    	else
    		log.info("CustomerTypeXMLHandler > generateXMLRequest > for Customer  ["+requestVO.getAccountNumber()+"] Called");
        String requestMessage = "";
        try {
//            if (requestVO.getLineNumber() == null
//                    || requestVO.getLineNumber().equalsIgnoreCase("")) {
//                log.fatal("CustomerTypeXMLHandler > generate XML Message > Number Not Found....");
//                throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
//            }
            Document doc = new DocumentImpl();
            Element root = doc.createElement(TagIfc.MAIN_EAI_TAG_NAME);
            Element header = XMLUtility.openParentTag(doc,TagIfc.EAI_HEADER_TAG);
            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_FORMAT,ConstantIfc.CUSTOMER_TYPE_MESSAGE_FORMAT_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.CUSTOMER_TYPE_MESSAGE_VER_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_FUNCTION,ConstantIfc.CUSTOMER_TYPE_CHANNEL_FUNC_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVO.getRequestorUserId());
            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_SECURITY_INFO,ConstantIfc.SECURITY_INFO_VALUE);
            	XMLUtility.generateElelemt(doc, header, TagIfc.RETURN_CODE,ConstantIfc.RETURN_CODE_VALUE);
            XMLUtility.closeParentTag(root, header);
            //body
            if(requestVO.getLineNumber()!=null && !requestVO.getLineNumber().equals(""))
            	XMLUtility.generateElelemt(doc, root, TagIfc.LINE_NUMBER, requestVO.getLineNumber());
            else if(requestVO.getAccountNumber()!=null && !requestVO.getAccountNumber().equals(""))
            	XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ACCOUNT_NUMBER, requestVO.getAccountNumber());
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_INCLUDE_ID, ConstantIfc.INCLUDE_ID_YES);
            XMLUtility.generateElelemt(doc, root, TagIfc.TAG_INCLUDE_CUSTOMERCATEGORY, ConstantIfc.INCLUDE_CATEGORY_YES);
            doc.appendChild(root);
            requestMessage = XMLUtility.serializeRequest(doc);
        } catch (java.io.IOException e) {
            log.fatal("CustomerTypeXMLHandler > generateXMLRequest >IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            log.fatal("CustomerTypeXMLHandler > generateXMLRequest > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        
        return requestMessage;
    }

    /**
     * Parsing the XML Reply
     * 
     * @param xmlMessage
     * @return Feb 17, 2008 CustomerTypeXMLHandler.java msayed
     */
    public CustomerTypeVO parsingXMLRreply(String xmlMessage) throws MobilyCommonException {
        CustomerTypeVO replyBean = null;
        if (xmlMessage == null || xmlMessage == "")
            return replyBean;
        try {
            replyBean = new CustomerTypeVO();

            Document doc = XMLUtility.parseXMLString(xmlMessage);

            int CustomerType = 0;
            Node rootNode = doc.getElementsByTagName(TagIfc.MAIN_EAI_TAG_NAME)
                    .item(0);

            NodeList nl = rootNode.getChildNodes();
            for (int j = 0; j < nl.getLength(); j++) {
                if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                    continue;

                Element l_Node = (Element) nl.item(j);
                String p_szTagName = l_Node.getTagName();
                String l_szNodeValue = null;
                if (l_Node.getFirstChild() != null)
                    l_szNodeValue = l_Node.getFirstChild().getNodeValue();

                if (p_szTagName.equalsIgnoreCase(TagIfc.EAI_HEADER_TAG)) {
                    MessageHeaderHandler headerParser = new MessageHeaderHandler();
                    MessageHeaderVO Header = headerParser
                            .parsingXMLHeaderReplyMessage(l_Node);
                    if (!Header.getReturnCode().equals(
                            ConstantIfc.RETURN_CODE_VALUE)) {
                        throw new MobilyCommonException(Header.getReturnCode());
                    }
                } else if (p_szTagName.equalsIgnoreCase(TagIfc.LINE_NUMBER)) {
                	if (l_szNodeValue != null)
                		replyBean.setLineNumber(l_szNodeValue);
                } else if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_ACCOUNT_NUMBER)) {
                	if (l_szNodeValue != null)
                		replyBean.setAccountNumber(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CUSTOMER_TYPE_PACKAGE_ID)) {
                    replyBean.setPackageID(l_szNodeValue);
                } else if (p_szTagName.equalsIgnoreCase(TagIfc.CUSTOMER_TYPE)) {
                    if (l_szNodeValue != null)
                        replyBean.setCustomerType(Integer
                                .parseInt(l_szNodeValue));
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.PACKAGE_DESCRIPTION)) {
                    if (l_szNodeValue != null)
                        replyBean.setPackageDescription(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_PACKAGE_NAME)) {
                    if (l_szNodeValue != null)
                        replyBean.setPackageDescription(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CREATION_TIME)) {
                    if (l_szNodeValue != null) {
                        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
                                "ddMMyyyyhhmmss");
                        replyBean.setCreationDate(sdf.parse(l_szNodeValue));
                    }
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_PACKAGE_CATEGORY)) {
                    if (l_szNodeValue != null)
                        replyBean.setPackageCategory(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CUST_CATEGORY)) {
                    if (l_szNodeValue != null)
                        replyBean.setCustomerCategory(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_ISMIX)) {
                    if (l_szNodeValue != null)
                        replyBean.setIsMix(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CORPORATE_PACKAGE)) {
                    if (l_szNodeValue != null)
                        replyBean.setCorporatePackage(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_ISNEW_CONTROL)) {
                    if (l_szNodeValue != null)
                        replyBean.setIsNewControl(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_ISCORPORATE_KA)) {
                    if (l_szNodeValue != null)
                        replyBean.setIsCorporateKA(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CUSTOMERBSL_ID)) {
                    if (l_szNodeValue != null)
                        replyBean.setCustomerBSLId(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_MNPNUMBER)) {
                    if (l_szNodeValue != null)
                        replyBean.setMNPNumber(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_IDTYPE)) {
                    if (l_szNodeValue != null)
                        replyBean.setIDType(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_IDNUMBER)) {
                    if (l_szNodeValue != null)
                        replyBean.setIDNumber(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CUSTOMER_ID)) {
                    if (l_szNodeValue != null)
                        replyBean.setCustomerId(l_szNodeValue);
                } else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_BILLING_ID)) {
                    if (l_szNodeValue != null)
                        replyBean.setBillingId(l_szNodeValue);
                }else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_SERVICE_ID)) {
                    if (l_szNodeValue != null)
                        replyBean.setServiceId(l_szNodeValue);
                }else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_DEAL_ID)) {
                    if (l_szNodeValue != null)
                        replyBean.setDealId(l_szNodeValue);
                }else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_ISSMSIM)) {
                    if (l_szNodeValue != null)
                        replyBean.setISMSIM(l_szNodeValue);
                }else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_IS_IUC)) {
                    if (l_szNodeValue != null){
                    	if(l_szNodeValue.equals("Y")){
                    		replyBean.setIUC(true);
                    	}
                    	else{
                    		replyBean.setIUC(false);
                    	}
                    }//end if
                }else if (p_szTagName
                        .equalsIgnoreCase(TagIfc.TAG_CPE_MAC_ADDRESS)) {
                    if (l_szNodeValue != null)
                        replyBean.setCPEMACAddress(l_szNodeValue);
                }
            }
        } catch (MobilyCommonException e) {
            throw e;
        } catch (Exception e) {
            log.fatal("Exception Occured When pasing Xml Customer Type Request , " + e);
            throw new SystemException(e);
        }
        log.info("CustomerTypeXMLHandler > parsingXMLRreply > done");
        return replyBean;

    }

    public static void main(String[] args) {
    	String xx="<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>HLR.INQUIRY.REQUEST</MsgFormat><MsgVersion>0001</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><RequestorChannelFunction>INQ</RequestorChannelFunction><RequestorUserId>IUCTestingUser</RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0000</ReturnCode></EE_EAI_HEADER><AccountNumber>1000112462088816</AccountNumber><UserGender>M</UserGender><LineNumber>966560986656</LineNumber><PackageId>1212</PackageId><CustomerType>2</CustomerType><PackageDescription>Business 500 Employee Program</PackageDescription><CustomerCategory>Consumer</CustomerCategory><CreationTime>03102012154657</CreationTime><IsMix>N</IsMix><CorporatePackage>No</CorporatePackage><IsNewControl>N</IsNewControl><IsCorporateKA>No</IsCorporateKA><CustomerBSLId>1-5Q3LT9S</CustomerBSLId><IDType>Passport</IDType><IDNumber>552211</IDNumber><IS_IUC>N</IS_IUC><ISMSIM>N</ISMSIM><PackageServiceType>GSM</PackageServiceType></EE_EAI_MESSAGE>";
    	CustomerTypeXMLHandler customer = new  CustomerTypeXMLHandler();
    	try {
			CustomerTypeVO customert= customer.parsingXMLRreply(xx);
//			System.out.println(customert.isIUC());
			
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
    	
    }
}
