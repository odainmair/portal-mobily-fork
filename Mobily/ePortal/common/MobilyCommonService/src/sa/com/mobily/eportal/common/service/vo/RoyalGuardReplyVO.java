/*
 * Created on March 31, 2010
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.List;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RoyalGuardReplyVO {
	String errorCode = "";
	String errorMessage = "";
	String totalUnConsumedMinutes = "";
	boolean isPending = false;
	double pendingMinutes = 0.0;
	List subscriberVOList = null;
	private String recordCount = "0";
	// Added for Reports CR
	private double totalAmount = 0.0;
	private double distributedAmount = 0.0;
	private double consumedAmount = 0.0;

	
	public String getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(String recordCount) {
		this.recordCount = recordCount;
	}
	public List getSubscriberVOList() {
		return subscriberVOList;
	}
	public void setSubscriberVOList(List subscriberVOList) {
		this.subscriberVOList = subscriberVOList;
	}

	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the errorMessage.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage The errorMessage to set.
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return Returns the totalUnConsumedMinutes.
	 */
	public String getTotalUnConsumedMinutes() {
		return totalUnConsumedMinutes;
	}
	/**
	 * @param totalUnConsumedMinutes The totalUnConsumedMinutes to set.
	 */
	public void setTotalUnConsumedMinutes(String totalUnConsumedMinutes) {
		this.totalUnConsumedMinutes = totalUnConsumedMinutes;
	}
	
	/**
	 * @return Returns the isPending.
	 */
	public boolean isPending() {
		return isPending;
	}
	/**
	 * @param isPending The isPending to set.
	 */
	public void setPending(boolean isPending) {
		this.isPending = isPending;
	}
	/**
	 * @return Returns the pendingMinutes.
	 */
	public double getPendingMinutes() {
		return pendingMinutes;
	}
	/**
	 * @param pendingMinutes The pendingMinutes to set.
	 */
	public void setPendingMinutes(double pendingMinutes) {
		this.pendingMinutes = pendingMinutes;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getDistributedAmount() {
		return distributedAmount;
	}
	public void setDistributedAmount(double distributedAmount) {
		this.distributedAmount = distributedAmount;
	}
	public double getConsumedAmount() {
		return consumedAmount;
	}
	public void setConsumedAmount(double consumedAmount) {
		this.consumedAmount = consumedAmount;
	}
	
}
