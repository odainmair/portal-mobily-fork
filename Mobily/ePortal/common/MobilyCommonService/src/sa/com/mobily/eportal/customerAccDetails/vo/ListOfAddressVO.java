package sa.com.mobily.eportal.customerAccDetails.vo;

import java.util.List;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class ListOfAddressVO extends BaseVO
{
	private static final long serialVersionUID = 1L;
	private List<AddressVO> address;
	public List<AddressVO> getAddressVOs()
	{
		return address;
	}
	public void setAddressVOs(List<AddressVO> address)
	{
		this.address = address;
	}
	
	 
}
