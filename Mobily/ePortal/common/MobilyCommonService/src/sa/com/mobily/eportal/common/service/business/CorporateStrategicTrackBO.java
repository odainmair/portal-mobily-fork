/**
 * 
 */
package sa.com.mobily.eportal.common.service.business;

import java.sql.Timestamp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.BillingAccountVO;
import sa.com.mobily.eportal.common.service.vo.EditAttributeVO;
import sa.com.mobily.eportal.common.service.vo.StrategicTrackVO;
import sa.com.mobily.eportal.common.service.xml.CorporateRevampXMLHandler;


/**
 * @author s.vathsavai.mit
 * 
 *
 */
public class CorporateStrategicTrackBO {

	 private static final Logger log = LoggerInterface.log;
	 
	 public BillingAccountVO doCorporateInquiry(String billingAccountNumber) {
		
		log.info("CorporateStrategicTrackBO > doCorporateInquiry :: start for billingAccountNumber = "+billingAccountNumber);
		
		/*
		 * Code Added for Alternate Solution */
		BillingAccountVO billingAccountVO = null;
		String xmlMessage = "";
		CorporateRevampXMLHandler handler = new CorporateRevampXMLHandler();
		
		OracleDAOFactory oracleDaoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		CorporateStrategicTrackDAO oracleDAO = oracleDaoFactory.getCorporateStrategicTrackDAO();
		
		MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
		CorporateStrategicTrackDAO mqDAO = daoFactory.getCorporateStrategicTrackDAO();
		
		long runInterval = Long.parseLong(MobilyUtility.getPropertyValue("strategic.track.timer.period.after.login")); // set as 2 minutes
		//long runInterval = 600000;
    	java.util.Date today = new java.util.Date();
        java.sql.Timestamp currentTimeStampDate = new Timestamp(today.getTime());
    	
		Timestamp newTimeStampDate = null;
		Timestamp receivedDate = null;
		Timestamp requestedDate = null;
		
		// check the customer status in EEDB
		StrategicTrackVO strategicTrackVO = oracleDAO.doInitialInquiryFromDB(billingAccountNumber);
		
		if(strategicTrackVO != null) { // Record is present in DB
			
			log.info("CorporateStrategicTrackBO > doCorporateInquiry :: status = "+strategicTrackVO.getStatus());
			log.info("CorporateStrategicTrackBO > doCorporateInquiry :: expired = "+strategicTrackVO.getExpired());
			
			if(ConstantIfc.EXPIRED_STATUS_TRUE.equalsIgnoreCase(strategicTrackVO.getExpired())) {
			
				// Check if the status is pending
				if(ConstantIfc.STATUS_PENDING.equalsIgnoreCase(strategicTrackVO.getStatus())) { // Status in Pending
					// Check the request time more than configured time
					requestedDate = MobilyUtility.getTimestampDate(strategicTrackVO.getRequestedTime());
					log.info("CorporateStrategicTrackBO > doCorporateInquiry :: requestedDate = "+requestedDate);	
					
					newTimeStampDate = new Timestamp(requestedDate.getTime() + runInterval);
					log.info("CorporateStrategicTrackBO > doCorporateInquiry :: newTimeStampDate(requested date + config time) = "+newTimeStampDate);
					log.info("CorporateStrategicTrackBO > doCorporateInquiry :: currentTimeStampDate = "+currentTimeStampDate);
					
					if(newTimeStampDate.before(currentTimeStampDate)) { // If Requested time more than configured time 
						log.info("CorporateStrategicTrackBO > doCorporateInquiry > Request is pending for more than desired time :: Do Sync BSL Enquiry");
						try {
							
							String xmlRequest = handler.generateCorporateInquiryXMLRequest(billingAccountNumber);
							//testing purpose
							//String xmlReply = "<MOBILY_BSL_SR_REPLY><SR_BK_HEADER><FuncId>CORP_INQ</FuncId><SecurityKey/><ServiceRequestId>TEST_SR_20100313100942</ServiceRequestId><MsgVersion>0000</MsgVersion><RequestorChannelId>EPORTAL</RequestorChannelId><SrDate>20100313070850</SrDate><BackendChannel>NETWORK</BackendChannel><SrRcvDate>20100313070842</SrRcvDate><SrStatus>6</SrStatus></SR_BK_HEADER><ErrorCode>0</ErrorCode><ErrorMsg/><ListOfBillingAccount><BillingAccount><AccountNumber>100013827238368</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238351</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013828988647</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238368</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013829159589</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType>DIA</ProductType><ContractPeriod>1 Year</ContractPeriod><NetworkConfiguration/><ParentAccountNumber>100013828988647</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts><CutServiceSubAccounts><AccountNumber>100013829180705</AccountNumber><AccountTypeCode>Service</AccountTypeCode><VPNID/><PackageName>DIA Backup</PackageName><Name>QATEST EPORTAL DIA TEST [100013829180705]</Name><Alias/><Items><Item><ItemName>DIA Backup</ItemName><Status>Active</Status><Attributes><Attribute><Name>Bandwidth</Name><TextValue>400 MB</TextValue></Attribute><Attribute><Name>Connection Mode</Name><TextValue>Silver</TextValue></Attribute><Attribute><Name>Access Network</Name><TextValue>GPON</TextValue></Attribute><Attribute><Name>NTU</Name><TextValue>ONT</TextValue></Attribute><Attribute><Name>Default CIDR Profile</Name><TextValue>/29</TextValue></Attribute></Attributes></Item><Item><ItemName>DIA Installation New</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA - Managed Connectivity</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>Dual Ethernet Security Router with V.92 Modem Backup</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>2821 Bundle w/AIM-VPN/SSL-2,Adv. IP Serv,10 SSL lic,64F/256D</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA SLA</ItemName><Status>Active</Status><Attributes><Attribute><Name>SLA</Name><TextValue>Silver</TextValue></Attribute></Attributes></Item></Items></CutServiceSubAccounts></ListOfServiceSubAccounts></BillingAccount></BillingAccount></BillingAccount></ListOfBillingAccount></MOBILY_BSL_SR_REPLY>";
							String xmlReply = mqDAO.doCorporateInquiry(xmlRequest);
							
							log.info("CorporateStrategicTrackBO > doCorporateInquiry :: Sync xmlReply = "+xmlReply);
							billingAccountVO = handler.parseCorporateInquiryReply(xmlReply);
							
							log.info("CorporateStrategicTrackBO > doCorporateInquiry :: Update the record in DB");
							oracleDAO.updateStrategicTrackRecordInDB(billingAccountNumber,xmlReply);
						} catch (Exception e) {
							log.fatal("CorporateStrategicTrackBO > doCorporateInquiry > Exception during Sync Request to BSL >"+e.getMessage());
							billingAccountVO = null;
						}
						
						if(billingAccountVO == null) {
							// Do Async BSL Enquiry
							log.info("CorporateStrategicTrackBO > doCorporateInquiry > billingAccountVO is null :: Do Async BSL Enquiry.");
							doCorporateInquiryAsync(billingAccountNumber);
	
							throw new SystemException();
						}
					} else { // If Requested time less than configured time 
						// wait for 10 seconds
						log.info("CorporateStrategicTrackBO > doCorporateInquiry > wait for 10 seconds");
						wait(10);
						
						log.info("CorporateStrategicTrackBO > doCorporateInquiry > Now again check the status in DB");
						strategicTrackVO = oracleDAO.doInitialInquiryFromDB(billingAccountNumber);
						if(strategicTrackVO != null && ConstantIfc.STATUS_PENDING.equalsIgnoreCase(strategicTrackVO.getStatus())) {
							// display error message to customer
							log.info("CorporateStrategicTrackBO > doCorporateInquiry > Status is still pending after waiting for 10 seconds :: Display error message to customer");
						} else if(strategicTrackVO != null && ConstantIfc.STATUS_COMPLETED.equalsIgnoreCase(strategicTrackVO.getStatus())) {
							// Get the xml request from DB and populate billingAccountVO
							xmlMessage = oracleDAO.getMessageFromDB(billingAccountNumber);
							
							// parse the xml message to populate billingAccountVO
							billingAccountVO = handler.parseCorporateInquiryReply(xmlMessage);
						}
					}
				} else if(ConstantIfc.STATUS_COMPLETED.equalsIgnoreCase(strategicTrackVO.getStatus())) { // Status is completed
					
					runInterval = Long.parseLong(MobilyUtility.getPropertyValue("strategic.track.timer.period")); // set as 1 hour
					
					// Check the receiving time more than configured time
					newTimeStampDate = new Timestamp(currentTimeStampDate.getTime() - runInterval);
					log.info("CorporateStrategicTrackBO > doCorporateInquiry :: newTimeStampDate(current date - config time) = "+newTimeStampDate);
					
					receivedDate = MobilyUtility.getTimestampDate(strategicTrackVO.getReceivedTime());
					log.info("CorporateStrategicTrackBO > doCorporateInquiry :: receivedDate = "+receivedDate);
					
					if(receivedDate.before(newTimeStampDate)) { // If Receiving time more than configured time 
						log.info("CorporateStrategicTrackBO > doCorporateInquiry > Reply received from BSL is quite old :: Do Sync BSL Enquiry.");
						
						try {
							
							String xmlRequest = handler.generateCorporateInquiryXMLRequest(billingAccountNumber);
							//testing purpose
							//String xmlReply = "<MOBILY_BSL_SR_REPLY><SR_BK_HEADER><FuncId>CORP_INQ</FuncId><SecurityKey/><ServiceRequestId>TEST_SR_20100313100942</ServiceRequestId><MsgVersion>0000</MsgVersion><RequestorChannelId>EPORTAL</RequestorChannelId><SrDate>20100313070850</SrDate><BackendChannel>NETWORK</BackendChannel><SrRcvDate>20100313070842</SrRcvDate><SrStatus>6</SrStatus></SR_BK_HEADER><ErrorCode>0</ErrorCode><ErrorMsg/><ListOfBillingAccount><BillingAccount><AccountNumber>100013827238368</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238351</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013828988647</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238368</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013829159589</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType>DIA</ProductType><ContractPeriod>1 Year</ContractPeriod><NetworkConfiguration/><ParentAccountNumber>100013828988647</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts><CutServiceSubAccounts><AccountNumber>100013829180705</AccountNumber><AccountTypeCode>Service</AccountTypeCode><VPNID/><PackageName>DIA Backup</PackageName><Name>QATEST EPORTAL DIA TEST [100013829180705]</Name><Alias/><Items><Item><ItemName>DIA Backup</ItemName><Status>Active</Status><Attributes><Attribute><Name>Bandwidth</Name><TextValue>400 MB</TextValue></Attribute><Attribute><Name>Connection Mode</Name><TextValue>Silver</TextValue></Attribute><Attribute><Name>Access Network</Name><TextValue>GPON</TextValue></Attribute><Attribute><Name>NTU</Name><TextValue>ONT</TextValue></Attribute><Attribute><Name>Default CIDR Profile</Name><TextValue>/29</TextValue></Attribute></Attributes></Item><Item><ItemName>DIA Installation New</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA - Managed Connectivity</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>Dual Ethernet Security Router with V.92 Modem Backup</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>2821 Bundle w/AIM-VPN/SSL-2,Adv. IP Serv,10 SSL lic,64F/256D</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA SLA</ItemName><Status>Active</Status><Attributes><Attribute><Name>SLA</Name><TextValue>Silver</TextValue></Attribute></Attributes></Item></Items></CutServiceSubAccounts></ListOfServiceSubAccounts></BillingAccount></BillingAccount></BillingAccount></ListOfBillingAccount></MOBILY_BSL_SR_REPLY>";
							String xmlReply = mqDAO.doCorporateInquiry(xmlRequest);
							
							log.info("CorporateStrategicTrackBO > doCorporateInquiry :: Sync xmlReply = "+xmlReply);
							billingAccountVO = handler.parseCorporateInquiryReply(xmlReply);
							
							log.info("CorporateStrategicTrackBO > doCorporateInquiry :: Update the record in DB");
							oracleDAO.updateStrategicTrackRecordInDB(billingAccountNumber,xmlReply);
						} catch (Exception e) {
							log.fatal("CorporateStrategicTrackBO > doCorporateInquiry > Exception during Sync Request to BSL >"+e.getMessage());
							billingAccountVO = null;
						}
						
						if(billingAccountVO == null) {
							// Do Async BSL Enquiry
							log.info("CorporateStrategicTrackBO > doCorporateInquiry > billingAccountVO is null :: Do ASync BSL Enquiry.");
							doCorporateInquiryAsync(billingAccountNumber);
							
							boolean updateStatus = oracleDAO.updateCorpRecordInDB(billingAccountNumber);
							log.info("CorporateStrategicTrackBO > doCorporateInquiry ::update requested time and status > updateStatus = "+updateStatus);
							
							log.info("CorporateStrategicTrackBO > doCorporateInquiry > billingAccountVO is null :: Get the record from DB and populate billingAccountVO.");
							xmlMessage = oracleDAO.getMessageFromDB(billingAccountNumber);
							
							// parse the xml message to populate billingAccountVO
							billingAccountVO = handler.parseCorporateInquiryReply(xmlMessage);
						}
	
					} else { // If Receiving time less than configured time 
						
						log.info("CorporateStrategicTrackBO > doCorporateInquiry > Get the xmlrequest from DB");
						xmlMessage = oracleDAO.getMessageFromDB(billingAccountNumber);
						
						// parse the xml message to populate billingAccountVO
						billingAccountVO = handler.parseCorporateInquiryReply(xmlMessage);
					}
				}
			} else {//Expired is FALSE
				// Again do BSL enquiry and update the Expired Status to TRUE
				log.info("CorporateStrategicTrackBO > doCorporateInquiry > Expired status is FALSE :: Again do Sync Enquiry");
				String xmlRequest = handler.generateCorporateInquiryXMLRequest(billingAccountNumber);
				
				String xmlReply = mqDAO.doCorporateInquiry(xmlRequest);
				billingAccountVO = handler.parseCorporateInquiryReply(xmlReply);
				
				log.info("CorporateStrategicTrackBO > doCorporateInquiry :: Update the record in DB");
				oracleDAO.updateStrategicTrackRecordInDB(billingAccountNumber,xmlReply);
				
				boolean updateStatus = updateExpiredStatusInDB(billingAccountNumber,ConstantIfc.EXPIRED_STATUS_TRUE);
				log.info("CorporateStrategicTrackBO > doCorporateInquiry ::updateStatus for Expiry column= "+updateStatus);		
			}
		}
		log.info("CorporateStrategicTrackBO > doCorporateInquiry :: End for billingAccountNumber = "+billingAccountNumber);
		return billingAccountVO;
		
		//return doCorporateInquirySync(billingAccountNumber);
	}

	/**
	 * This method is used to update the alias name
	 * @param editAttributeVO
	 * @return 
	 */
	public EditAttributeVO updateAliasName(EditAttributeVO editAttributeVO) {
	
		log.info("CorporateStrategicTrackBO > updateAliasName :: start for billingAccountNumber = "+editAttributeVO.getAccountNumber());
		MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
		CorporateStrategicTrackDAO revampDAO = daoFactory.getCorporateStrategicTrackDAO();
		CorporateRevampXMLHandler handler = new CorporateRevampXMLHandler();
		String xmlRequest = handler.generateUpdateAliasNameXMLRequest(editAttributeVO);
		System.out.println("*************************************************");
		System.out.println("xmlRequest : " + xmlRequest);
		
		//testing purpose
		//String xmlReply = "<MOBILY_BSL_SR_REPLY>	      <SR_BK_HEADER_Reply>	            <FuncID>AliasNameUpdate</FuncID>	            <SecurityKey />	            <ServiceRequestId>SR_1417310438</ServiceRequestId>	            <MsgVersion>0000</MsgVersion>	            <RequestorChannelId>ePortal</RequestorChannelId>	            <SrDate>20091030214708</SrDate>	            <BackendChannel>NETWORK</BackendChannel>	            <SrRcvDate>20091030214808</SrRcvDate>	            <SrStatus>1</SrStatus>	      </SR_BK_HEADER_Reply>	      <ErrorCode>0</ErrorCode>	      <ErrorMsg></ErrorMsg>		</MOBILY_BSL_SR_REPLY>";
		String xmlReply = revampDAO.updateCircuitAliasName(xmlRequest);
		System.out.println("xmlReply : " + xmlReply);
		System.out.println("*************************************************");
		log.info("CorporateStrategicTrackBO > updateAliasName :: xmlReply = "+xmlReply);
		editAttributeVO = handler.parseUpdateAliasNameXMLReply(xmlReply,editAttributeVO);
		log.info("CorporateStrategicTrackBO > updateAliasName :: end");
		return editAttributeVO;
	}
	
	/**
	 * This method is used to be called from My Account Filter during login.
	 * @param billingAccountNumber
	 */
	public void handleDIARequestAsync(String billingAccountNumber) {
		log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: start for billingAccountNumber = "+billingAccountNumber);
		
		OracleDAOFactory oracleDaoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		CorporateStrategicTrackDAO oracleDAO = oracleDaoFactory.getCorporateStrategicTrackDAO();
		
		long runInterval = Long.parseLong(MobilyUtility.getPropertyValue("strategic.track.timer.period")); // set as 1 hour
		//long runInterval = 600000;
    	java.util.Date today = new java.util.Date();
        java.sql.Timestamp currentTimeStampDate = new Timestamp(today.getTime());
    	
		Timestamp newTimeStampDate = null;
		Timestamp receivedDate = null;
		Timestamp requestedDate = null;

		// check the customer record is present in EEDB
		StrategicTrackVO strategicTrackVO = oracleDAO.doInitialInquiryFromDB(billingAccountNumber);
		
		if(strategicTrackVO != null) { // Record is present in DB
			
			log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: status = "+strategicTrackVO.getStatus());
			log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: expired = "+strategicTrackVO.getExpired());
			
			if(ConstantIfc.EXPIRED_STATUS_TRUE.equalsIgnoreCase(strategicTrackVO.getExpired())) {
			
				// Check if the status is pending
				if(ConstantIfc.STATUS_PENDING.equalsIgnoreCase(strategicTrackVO.getStatus())) { // Status in Pending
					
					// Check the request time more than configured time
					requestedDate = MobilyUtility.getTimestampDate(strategicTrackVO.getRequestedTime());
					log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: requestedDate = "+requestedDate);	
					
					newTimeStampDate = new Timestamp(requestedDate.getTime() + runInterval);
					log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: newTimeStampDate(requested date + config time) = "+newTimeStampDate);
					log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: currentTimeStampDate = "+currentTimeStampDate);
					
					if(newTimeStampDate.before(currentTimeStampDate)) { // If Requested time more than configured time 
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Request is pending for more than desired time :: Do BSL Enquiry Again.");
	
						doCorporateInquiryAsync(billingAccountNumber);
						
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Update Request Time and Status in DB Again.");
						boolean updateStatus = oracleDAO.updateCorpRecordInDB(billingAccountNumber);
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync ::updatedStatus = "+updateStatus);
					} else { // If Requested time less than configured time 
						// Don't do anything
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Requested time less than configured time :: Don't do anything.");
					}
				} else if(ConstantIfc.STATUS_COMPLETED.equalsIgnoreCase(strategicTrackVO.getStatus())) { // Status is completed
					// Check the receiving time more than configured time
					newTimeStampDate = new Timestamp(currentTimeStampDate.getTime() - runInterval);
					log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: newTimeStampDate(current date - config time) = "+newTimeStampDate);
					
					receivedDate = MobilyUtility.getTimestampDate(strategicTrackVO.getReceivedTime());
					log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: receivedDate = "+receivedDate);
					
					if(receivedDate.before(newTimeStampDate)) { // If Receiving time more than configured time 
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Reply received from BSL is quite old :: Do Fresh BSL Enquiry again.");
	
						// Resend a new request to backend and 
						doCorporateInquiryAsync(billingAccountNumber);
						
						// update request Time & status
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Update the new Requested Time and Status.");
						boolean updateStatus = oracleDAO.updateCorpRecordInDB(billingAccountNumber);
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync ::updateStatus = "+updateStatus);
					} else { // If Receiving time less than configured time 
						// Don't do anything
						log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Already received BSL reply which is in the configured time interval :: Don't do anything.");
					}
					
				}
			
			} else {
				log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Expiry Status is FALSE :: Do Async BSL enquiry");
				// The CAP has updated the Alias name due to which we need to do BSL enquiry again
				doCorporateInquiryAsync(billingAccountNumber);
				
				// Update the record with Expired status as TRUE
				log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Update Expired Status in DB to TRUE");
				boolean updateStatus = updateExpiredStatusInDB(billingAccountNumber,ConstantIfc.EXPIRED_STATUS_TRUE);
				log.info("CorporateStrategicTrackBO > handleDIARequestAsync ::updateStatus = "+updateStatus);				
			}
			
		} else { // There is no record present in DB. Send Async Request to BSL
			log.info("CorporateStrategicTrackBO > handleDIARequestAsync > There is no record in DB :: Do BSL enquiry");
			doCorporateInquiryAsync(billingAccountNumber);
			
			// Insert new record in DB with Status as Pending and Request Time to be current time
			log.info("CorporateStrategicTrackBO > handleDIARequestAsync > Insert the record in DB");
			boolean insertStatus = oracleDAO.insertCorpRecordInDB(billingAccountNumber);
			log.info("CorporateStrategicTrackBO > handleDIARequestAsync ::insertStatus = "+insertStatus);
		}
	}
	
	 public void doCorporateInquiryAsync(String billingAccountNumber) {
		log.info("CorporateStrategicTrackBO > doCorporateInquiryAsync :: start for billingAccountNumber = "+billingAccountNumber);
		MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
		CorporateStrategicTrackDAO revampDAO = daoFactory.getCorporateStrategicTrackDAO();
		CorporateRevampXMLHandler handler = new CorporateRevampXMLHandler();
		String xmlRequest = handler.generateCorporateInquiryXMLRequest(billingAccountNumber);
		log.info("CorporateStrategicTrackBO > doCorporateInquiryAsync :: xmlRequest = "+xmlRequest);
		revampDAO.doCorporateInquiryAsync(xmlRequest);

		log.info("CorporateStrategicTrackBO > doCorporateInquiryAsync :: end");
	}
	 
	 public BillingAccountVO doCorporateInquirySync(String billingAccountNumber) {
		MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
		CorporateStrategicTrackDAO revampDAO = daoFactory.getCorporateStrategicTrackDAO();
		CorporateRevampXMLHandler handler = new CorporateRevampXMLHandler();
		String xmlRequest = handler.generateCorporateInquiryXMLRequest(billingAccountNumber);
		//testing purpose
		//String xmlReply = "<MOBILY_BSL_SR_REPLY><SR_BK_HEADER><FuncId>CORP_INQ</FuncId><SecurityKey/><ServiceRequestId>TEST_SR_20100313100942</ServiceRequestId><MsgVersion>0000</MsgVersion><RequestorChannelId>EPORTAL</RequestorChannelId><SrDate>20100313070850</SrDate><BackendChannel>NETWORK</BackendChannel><SrRcvDate>20100313070842</SrRcvDate><SrStatus>6</SrStatus></SR_BK_HEADER><ErrorCode>0</ErrorCode><ErrorMsg/><ListOfBillingAccount><BillingAccount><AccountNumber>100013827238368</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238351</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013828988647</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238368</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013829159589</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType>DIA</ProductType><ContractPeriod>1 Year</ContractPeriod><NetworkConfiguration/><ParentAccountNumber>100013828988647</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts><CutServiceSubAccounts><AccountNumber>100013829180705</AccountNumber><AccountTypeCode>Service</AccountTypeCode><VPNID/><PackageName>DIA Backup</PackageName><Name>QATEST EPORTAL DIA TEST [100013829180705]</Name><Alias/><Items><Item><ItemName>DIA Backup</ItemName><Status>Active</Status><Attributes><Attribute><Name>Bandwidth</Name><TextValue>400 MB</TextValue></Attribute><Attribute><Name>Connection Mode</Name><TextValue>Silver</TextValue></Attribute><Attribute><Name>Access Network</Name><TextValue>GPON</TextValue></Attribute><Attribute><Name>NTU</Name><TextValue>ONT</TextValue></Attribute><Attribute><Name>Default CIDR Profile</Name><TextValue>/29</TextValue></Attribute></Attributes></Item><Item><ItemName>DIA Installation New</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA - Managed Connectivity</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>Dual Ethernet Security Router with V.92 Modem Backup</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>2821 Bundle w/AIM-VPN/SSL-2,Adv. IP Serv,10 SSL lic,64F/256D</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA SLA</ItemName><Status>Active</Status><Attributes><Attribute><Name>SLA</Name><TextValue>Silver</TextValue></Attribute></Attributes></Item></Items></CutServiceSubAccounts></ListOfServiceSubAccounts></BillingAccount></BillingAccount></BillingAccount></ListOfBillingAccount></MOBILY_BSL_SR_REPLY>";
		String xmlReply = revampDAO.doCorporateInquiry(xmlRequest);
		
		log.info("CorporateStrategicTrackBO > doCorporateInquirySync :: xmlReply = "+xmlReply);
		BillingAccountVO billingAccountVO = handler.parseCorporateInquiryReply(xmlReply);
		log.info("CorporateStrategicTrackBO > doCorporateInquirySync :: end");
		
		return billingAccountVO;
	 }
	 
	 public boolean updateExpiredStatusInDB(String billingAccountNumber, String expiredStatus) {
		 log.info("CorporateStrategicTrackBO > updateExpiredStatusInDB :: start for billingAccountNumber = "+billingAccountNumber+" and expired status ="+expiredStatus);
		 boolean updatedStatus = false;
		 
		 OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		 CorporateStrategicTrackDAO revampDAO = daoFactory.getCorporateStrategicTrackDAO();
		 
		 updatedStatus = revampDAO.updateExpiredStatusInDB(billingAccountNumber, expiredStatus);
		 log.info("CorporateStrategicTrackBO > updateExpiredStatusInDB :: updatedStatus = "+updatedStatus);
		 return updatedStatus;
		 
	 }
	 
	 public static void wait (int n) {
		 long t0,t1;
		 t0 = System.currentTimeMillis();
		 do {
			 t1=System.currentTimeMillis();
			 
		 }
		 while ((t1 - t0) < (n * 1000));
	 }
	 
	 public static void main(String args[]) {

		 new CorporateStrategicTrackBO().handleDIARequestAsync("100013835312648");
		 	
	 }

}
