package sa.com.mobily.eportal.common.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class MobilyException extends RuntimeException {

	private static final long serialVersionUID = 4195548809936370811L;

	public MobilyException() {
	}

	public MobilyException(String message) {
		super(message);
	}

	public MobilyException(Throwable cause) {
		super(cause);
	}

	public MobilyException(String message, Throwable cause) {
		super(message, cause);
	}
}