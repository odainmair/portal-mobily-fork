/**
 * 
 */
package sa.com.mobily.eportal.lp.util;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author n.gundluru.mit
 *
 */
public class FailureDataAuditVO extends BaseVO {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userTransId;
	private String serviceTransId;
	private String paymentTransId;
	private String notificationTransId;
	private String createDate;
	private int actionStatus;
	private String reason;
	private String msisdn;
	private int type;
	private String paymentAmount;
	
	
	/**
	 * @return the userTransId
	 */
	public String getUserTransId()
	{
		return userTransId;
	}
	/**
	 * @param userTransId the userTransId to set
	 */
	public void setUserTransId(String userTransId)
	{
		this.userTransId = userTransId;
	}
	/**
	 * @return the serviceTransId
	 */
	public String getServiceTransId()
	{
		return serviceTransId;
	}
	/**
	 * @param serviceTransId the serviceTransId to set
	 */
	public void setServiceTransId(String serviceTransId)
	{
		this.serviceTransId = serviceTransId;
	}
	/**
	 * @return the paymentTransId
	 */
	public String getPaymentTransId()
	{
		return paymentTransId;
	}
	/**
	 * @param paymentTransId the paymentTransId to set
	 */
	public void setPaymentTransId(String paymentTransId)
	{
		this.paymentTransId = paymentTransId;
	}
	/**
	 * @return the notificationTransId
	 */
	public String getNotificationTransId()
	{
		return notificationTransId;
	}
	/**
	 * @param notificationTransId the notificationTransId to set
	 */
	public void setNotificationTransId(String notificationTransId)
	{
		this.notificationTransId = notificationTransId;
	}
	/**
	 * @return the createDate
	 */
	public String getCreateDate()
	{
		return createDate;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(String createDate)
	{
		this.createDate = createDate;
	}
	/**
	 * @return the actionStatus
	 */
	public int getActionStatus()
	{
		return actionStatus;
	}
	/**
	 * @param actionStatus the actionStatus to set
	 */
	public void setActionStatus(int actionStatus)
	{
		this.actionStatus = actionStatus;
	}
	/**
	 * @return the reason
	 */
	public String getReason()
	{
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason)
	{
		this.reason = reason;
	}
	/**
	 * @return the msisdn
	 */
	public String getMsisdn()
	{
		return msisdn;
	}
	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public String getPaymentAmount()
	{
		return paymentAmount;
	}
	public void setPaymentAmount(String paymentAmount)
	{
		this.paymentAmount = paymentAmount;
	}
	
	
}
