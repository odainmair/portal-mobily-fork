//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import sa.com.mobily.eportal.core.api.ServiceException;

/**
 * A mapper class that uses the Jackson framework to read and write
 * Java objects to/from JSON-format strings.
 */
public class ResourceMapper {

    ObjectMapper mapper = new ObjectMapper();

    /**
     * Writes a Java objects to a JSON string
     * @param obj the Java object to serialize
     * @return JSON formatted string
     * @throws ServiceException when an error occurs during mapping
     */
    public String write(Object obj) throws ServiceException {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonGenerationException e) {
            throw new MappingException(e);
        } catch (JsonMappingException e) {
            throw new MappingException(e);
        } catch (IOException e) {
            throw new MappingException(e);
        }
    }

    /**
     * Reads a Java object from a JSON string
     * @param data JSON formatted string
     * @param clazz the class of the object to read
     * @return the Java object
     * @throws ServiceException when an error occurs during mapping
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Object read(String data, Class clazz) throws ServiceException {
        try {
            return mapper.readValue(data, clazz);
        } catch (JsonParseException e) {
            throw new MappingException(e);
        } catch (JsonMappingException e) {
            throw new MappingException(e);
        } catch (IOException e) {
            throw new MappingException(e);
        }
    }

    public static class MappingException extends ServiceException {

        private static final long serialVersionUID = 1L;

        public MappingException(Exception e) {
            super("Failed in JSON mapping", e);
        }
    }
}
