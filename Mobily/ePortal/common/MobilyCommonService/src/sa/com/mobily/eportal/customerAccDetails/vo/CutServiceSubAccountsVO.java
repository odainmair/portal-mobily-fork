/**
 * 
 */
package sa.com.mobily.eportal.customerAccDetails.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author 80057930
 *
 */
public class CutServiceSubAccountsVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String accountNumber;
	private String accountStatus;
	private String emailAddress;
	private String customerAccountName;
	private String customerCategory;
	private String customerType;
	private String fullName;
	private String idDocType;
	private String idNumber;
	private String msisdn;
	private String pricingPlan;
	private String rootProductLine;
	private String rootProductCategory;
	private String packageId;
	private String productName;
	private String packageName;
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber()
	{
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber)
	{
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the accountStatus
	 */
	public String getAccountStatus()
	{
		return accountStatus;
	}
	/**
	 * @param accountStatus the accountStatus to set
	 */
	public void setAccountStatus(String accountStatus)
	{
		this.accountStatus = accountStatus;
	}
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress()
	{
		return emailAddress;
	}
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}
	/**
	 * @return the customerAccountName
	 */
	public String getCustomerAccountName()
	{
		return customerAccountName;
	}
	/**
	 * @param customerAccountName the customerAccountName to set
	 */
	public void setCustomerAccountName(String customerAccountName)
	{
		this.customerAccountName = customerAccountName;
	}
	/**
	 * @return the customerCategory
	 */
	public String getCustomerCategory()
	{
		return customerCategory;
	}
	/**
	 * @param customerCategory the customerCategory to set
	 */
	public void setCustomerCategory(String customerCategory)
	{
		this.customerCategory = customerCategory;
	}
	/**
	 * @return the customerType
	 */
	public String getCustomerType()
	{
		return customerType;
	}
	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}
	/**
	 * @return the fullName
	 */
	public String getFullName()
	{
		return fullName;
	}
	/**
	 * @param fullName the fullName to set
	 */
	public void setFullName(String fullName)
	{
		this.fullName = fullName;
	}
	/**
	 * @return the idDocType
	 */
	public String getIdDocType()
	{
		return idDocType;
	}
	/**
	 * @param idDocType the idDocType to set
	 */
	public void setIdDocType(String idDocType)
	{
		this.idDocType = idDocType;
	}
	/**
	 * @return the idNumber
	 */
	public String getIdNumber()
	{
		return idNumber;
	}
	/**
	 * @param idNumber the idNumber to set
	 */
	public void setIdNumber(String idNumber)
	{
		this.idNumber = idNumber;
	}
	/**
	 * @return the msisdn
	 */
	public String getMsisdn()
	{
		return msisdn;
	}
	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	/**
	 * @return the pricingPlan
	 */
	public String getPricingPlan()
	{
		return pricingPlan;
	}
	/**
	 * @param pricingPlan the pricingPlan to set
	 */
	public void setPricingPlan(String pricingPlan)
	{
		this.pricingPlan = pricingPlan;
	}
	/**
	 * @return the rootProductLine
	 */
	public String getRootProductLine()
	{
		return rootProductLine;
	}
	/**
	 * @param rootProductLine the rootProductLine to set
	 */
	public void setRootProductLine(String rootProductLine)
	{
		this.rootProductLine = rootProductLine;
	}
	/**
	 * @return the rootProductCategory
	 */
	public String getRootProductCategory()
	{
		return rootProductCategory;
	}
	/**
	 * @param rootProductCategory the rootProductCategory to set
	 */
	public void setRootProductCategory(String rootProductCategory)
	{
		this.rootProductCategory = rootProductCategory;
	}
	/**
	 * @return the packageId
	 */
	public String getPackageId()
	{
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId)
	{
		this.packageId = packageId;
	}
	/**
	 * @return the productName
	 */
	public String getProductName()
	{
		return productName;
	}
	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	/**
	 * @return the packageName
	 */
	public String getPackageName()
	{
		return packageName;
	}
	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName)
	{
		this.packageName = packageName;
	}
	
	

}
