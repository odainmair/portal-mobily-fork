/*
 * Created on Feb 23, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import com.mobily.exception.mq.MQSendException;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.FavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQFavoriteNumberInquiryDAO implements FavoriteNumberInquiryDAO {

    private static final Logger log = LoggerInterface.log;
    
    public String getFavoriteNumberInquiry(String xmlMessage) {
		String replyMessage = "";
		log.info("MQFavoriteNumberInquiryDAO > getFavoriteNumberInquiry > Called.................");
		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.FAVORITE_NUMBER_INQ_REQUEST_QUEUENAME_V2);
			log.debug("MQFavoriteNumberInquiryDAO > getFavoriteNumberInquiry > The request Queue Name Fro Favoite Number Inquiry ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.FAVORITE_NUMBER_INQ_REPLY_QUEUENAME);
			log.debug("MQFavoriteNumberInquiryDAO > getFavoriteNumberInquiry > The reply Queue Name Fro Favoite Number Inquiry ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
//			       AlarmService alarmService = new AlarmService();
//			       alarmService.RaiseAlarm(ServicesIfc.ALARM_MQ_CUSTOMER_TYPE);

				log.fatal("MQFavoriteNumberInquiryDAO > getFavoriteNumberInquiry > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQFavoriteNumberInquiryDAO > getFavoriteNumberInquiry > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQFavoriteNumberInquiryDAO > getFavoriteNumberInquiry > Done.................");
		return replyMessage;

    
    }

	public static void main(String[] args) {
	}
}
