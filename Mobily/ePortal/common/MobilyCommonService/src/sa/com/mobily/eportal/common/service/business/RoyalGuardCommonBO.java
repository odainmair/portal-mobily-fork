/*
 * Created on Apr 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.RoyalGuardCommonDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.RoyalGuardReplyVO;
import sa.com.mobily.eportal.common.service.vo.RoyalGuardRequestVO;
import sa.com.mobily.eportal.common.service.xml.RoyalGuardXMLHandler;

/**
 * @author r.agarwal.mit
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class RoyalGuardCommonBO
{
	private static final Logger log = LoggerInterface.log;

	public RoyalGuardReplyVO msisdnInquiry(RoyalGuardRequestVO requestVO) throws MobilyCommonException
	{
		log.debug("RoyalGuardBO > msisdnInquiry > Called");
		RoyalGuardReplyVO replyVO = null;

		try
		{
			String xmlReply = "";
			RoyalGuardXMLHandler xmlHandler = new RoyalGuardXMLHandler();

			// generate XML request
			String xmlRequest = xmlHandler.generateXMLRequest(requestVO);

			// Send xml request to BSL
			log.debug("RoyalGuardBO > msisdnInquiry > XML request > " + xmlRequest);
			MQDAOFactory daoFactory = (MQDAOFactory) DAOFactory.getDAOFactory(DAOFactory.MQ);
			RoyalGuardCommonDAO royalGuardCommonDAO = daoFactory.getMQRoyalGuardCommonDAO();
			xmlReply = royalGuardCommonDAO.sendToMQWithReply(xmlRequest);
			log.debug("RoyalGuardBO > msisdnInquiry > XML Reply > " + xmlReply);

			// parsing the xml reply
			replyVO = xmlHandler.parseXMLReply(xmlReply);

			// check if the error code returned fom backend greater than 0 then
			// it will throw MobilyCommonexception object
			if (replyVO.getErrorCode() != null && !"".equals(replyVO.getErrorCode()) && Integer.parseInt(replyVO.getErrorCode()) > 0)
			{
				MobilyCommonException ex = new MobilyCommonException(replyVO.getErrorCode());
				throw ex;
			}

		}
		catch (MobilyCommonException e)
		{
			log.fatal("RoyalGuardBO > msisdnInquiry > MobilyCommonException > " + e.getErrorCode());
			throw e;
		}

		return replyVO;
	}
}
