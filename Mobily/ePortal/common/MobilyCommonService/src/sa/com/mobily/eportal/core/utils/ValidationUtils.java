/**
 * 
 */
package sa.com.mobily.eportal.core.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import sa.com.mobily.eportal.core.api.File;

/**
 * @author anas
 * 
 */
public final class ValidationUtils
{

	public static boolean isMatching(String text, String regex)
	{
		return Pattern.compile(regex, Pattern.CASE_INSENSITIVE).matcher(text).find();
	}

	public static boolean isNullOrEmpty(String text)
	{
		return text == null || "".equals(text.trim());
	}

	public static boolean isNullOrOld(Date date)
	{
		return date == null || date.compareTo(new Date()) < 0;
	}

	public static boolean isValidEmail(String text)
	{
		return isMatching(text, EMAIL_REGEX);
	}

	public static boolean isValidSize(File f, int size)
	{
		return f != null && f.getData().length <= size;
	}

	/***
	 * Checks if the passed obj is equal to any of the passed values<br/>
	 * values are compared sequentially, and compared using the
	 * <CODE>.equals()</CODE> method
	 * 
	 * @param obj
	 *            the object to be checked
	 * @param values
	 *            0 or more values to be compared against
	 * @return <b>true</b> if obj is equal to one of the values
	 *         <p>
	 *         <b>false</b> if obj didn't match any of the values or if obj or
	 *         values are null
	 *         </p>
	 */
	public static boolean equalsAny(Object obj, Object... values)
	{
		if (obj == null || values == null)
		{
			return false;
		}

		for (Object object : values)
		{
			if (obj.equals(object))
			{
				return true;
			}
		}

		return false;
	}

	public static String FormateDate(Date date)
	{
		String formatedDate = null;
		SimpleDateFormat sformat = new SimpleDateFormat("yyyyMMddHHmmss");

		formatedDate = sformat.format(date);
		return (formatedDate == null) ? "" : formatedDate;
	}

	private static final String EMAIL_REGEX = "[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}";

	public static void main(String[] args)
	{
		// System.out.println(isValidEmail("someone@eg.ibm.com"));
		// System.out.println(isMatching("12345", "\\d{3,9}"));
	}
}
