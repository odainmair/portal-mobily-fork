/*
 * Created on Jul 3, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;
import sa.com.mobily.eportal.common.service.vo.MessageHeaderVO;

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustServiceDetReqVO extends BaseVO {
	private MessageHeaderVO header = new MessageHeaderVO();
	private String          lineNumber;
    private String          userID;
    private String			internetBundle;	
    private String          statusOnly;
    
  
    
    
	
	/**
	 * @return Returns the header.
	 */
	public MessageHeaderVO getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(MessageHeaderVO header) {
		this.header = header;
	}

	
	/**
	 * @return Returns the lineNumber.
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber The lineNumber to set.
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return Returns the statusOnly.
	 */
	public String getStatusOnly() {
		return statusOnly;
	}
	/**
	 * @param statusOnly The statusOnly to set.
	 */
	public void setStatusOnly(String statusOnly) {
		this.statusOnly = statusOnly;
	}
	/**
	 * @return Returns the userID.
	 */
	public String getUserID() {
		return userID;
	}
	/**
	 * @param userID The userID to set.
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}
	/**
	 * @return Returns the internetBundle.
	 */
	public String getInternetBundle() {
		return internetBundle;
	}
	/**
	 * @param internetBundle The internetBundle to set.
	 */
	public void setInternetBundle(String internetBundle) {
		this.internetBundle = internetBundle;
	}
}
