package sa.com.mobily.eportal.common.service.vo;

import java.sql.Timestamp;
import java.util.List;


public class ProfileVO {
	
   private String profileId = "";
   
   private String profileName = "";
   
   private String profileStatus = "";
   
   private Timestamp creationTime = null;
   
   private String totalMinutes = "";
   
   private List profileContactVOList;
   
   private String dbOperation = "";
   
   private String corporateId = "";
   
   private int memberCount = 0;
   
   private String scheduleId = "";
   //private String profileId = "";
   private String scheduleType = "";
   //private Date creationTime;
   private Timestamp updatedTime;
   private String scheduleStatus = "";
   private Timestamp scheduleOneTime = null;
   private String schOneTime = "";
   
   private String scheduleMonthly = "";
   private int memberStatusCount = 0;
   private String memberStatusTotalMin = "";
   private String scheduleDescription = "";
 
   
	/**
	 * @return Returns the schOneTime.
	 */
	public String getSchOneTime() {
		return schOneTime;
	}
	/**
	 * @param schOneTime The schOneTime to set.
	 */
	public void setSchOneTime(String schOneTime) {
		this.schOneTime = schOneTime;
	}
	/**
	 * @return Returns the scheduleDescription.
	 */
	public String getScheduleDescription() {
		return scheduleDescription;
	}
	/**
	 * @param scheduleDescription The scheduleDescription to set.
	 */
	public void setScheduleDescription(String scheduleDescription) {
		this.scheduleDescription = scheduleDescription;
	}
	/**
	 * @return Returns the memberStatusTotalMin.
	 */
	public String getMemberStatusTotalMin() {
		return memberStatusTotalMin;
	}
	/**
	 * @param memberStatusTotalMin The memberStatusTotalMin to set.
	 */
	public void setMemberStatusTotalMin(String memberStatusTotalMin) {
		this.memberStatusTotalMin = memberStatusTotalMin;
	}
	/**
	 * @return Returns the memberStatusCount.
	 */
	public int getMemberStatusCount() {
		return memberStatusCount;
	}
	/**
	 * @param memberStatusCount The memberStatusCount to set.
	 */
	public void setMemberStatusCount(int memberStatusCount) {
		this.memberStatusCount = memberStatusCount;
	}
	/**
	 * @return Returns the scheduleId.
	 */
	public String getScheduleId() {
		return scheduleId;
	}
	/**
	 * @param scheduleId The scheduleId to set.
	 */
	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}
	/**
	 * @return Returns the scheduleMonthly.
	 */
	public String getScheduleMonthly() {
		return scheduleMonthly;
	}
	/**
	 * @param scheduleMonthly The scheduleMonthly to set.
	 */
	public void setScheduleMonthly(String scheduleMonthly) {
		this.scheduleMonthly = scheduleMonthly;
	}
	/**
	 * @return Returns the scheduleOneTime.
	 */
	public Timestamp getScheduleOneTime() {
		return scheduleOneTime;
	}
	/**
	 * @param scheduleOneTime The scheduleOneTime to set.
	 */
	public void setScheduleOneTime(Timestamp scheduleOneTime) {
		this.scheduleOneTime = scheduleOneTime;
	}
	/**
	 * @return Returns the scheduleStatus.
	 */
	public String getScheduleStatus() {
		return scheduleStatus;
	}
	/**
	 * @param scheduleStatus The scheduleStatus to set.
	 */
	public void setScheduleStatus(String scheduleStatus) {
		this.scheduleStatus = scheduleStatus;
	}
	/**
	 * @return Returns the scheduleType.
	 */
	public String getScheduleType() {
		return scheduleType;
	}
	/**
	 * @param scheduleType The scheduleType to set.
	 */
	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}
	/**
	 * @return Returns the updatedTime.
	 */
	public Timestamp getUpdatedTime() {
		return updatedTime;
	}
	/**
	 * @param updatedTime The updatedTime to set.
	 */
	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}
	/**
	 * @return Returns the memberCount.
	 */
	public int getMemberCount() {
		return memberCount;
	}
	/**
	 * @param memberCount The memberCount to set.
	 */
	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}
   
   
   
   
   
	/**
	 * @return Returns the corporateId.
	 */
	public String getCorporateId() {
		return corporateId;
	}
	/**
	 * @param corporateId The corporateId to set.
	 */
	public void setCorporateId(String corporateId) {
		this.corporateId = corporateId;
	}
	/**
	 * @return Returns the dbOperation.
	 */
	public String getDbOperation() {
		return dbOperation;
	}
	/**
	 * @param dbOperation The dbOperation to set.
	 */
	public void setDbOperation(String dbOperation) {
		this.dbOperation = dbOperation;
	}
	/**
	 * @return Returns the profileContactVOList.
	 */
	public List getProfileContactVOList() {
		return profileContactVOList;
	}
	/**
	 * @param profileContactVOList The profileContactVOList to set.
	 */
	public void setProfileContactVOList(List profileContactVOList) {
		this.profileContactVOList = profileContactVOList;
	}

	/**
	 * @return Returns the profileId.
	 */
	public String getProfileId() {
		return profileId;
	}
	/**
	 * @param profileId The profileId to set.
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	/**
	 * @return Returns the profileName.
	 */
	public String getProfileName() {
		return profileName;
	}
	/**
	 * @param profileName The profileName to set.
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	
	/**
	 * @return Returns the creationTime.
	 */
	public Timestamp getCreationTime() {
		return creationTime;
	}
	/**
	 * @param creationTime The creationTime to set.
	 */
	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}
	/**
	 * @return Returns the profileStatus.
	 */
	public String getProfileStatus() {
		return profileStatus;
	}
	/**
	 * @param profileStatus The profileStatus to set.
	 */
	public void setProfileStatus(String profileStatus) {
		this.profileStatus = profileStatus;
	}
	/**
	 * @return Returns the totalMinutes.
	 */
	public String getTotalMinutes() {
		return totalMinutes;
	}
	/**
	 * @param totalMinutes The totalMinutes to set.
	 */
	public void setTotalMinutes(String totalMinutes) {
		this.totalMinutes = totalMinutes;
	}
}
