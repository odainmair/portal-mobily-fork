package sa.com.mobily.eportal.common.service.vo;

public class LDAPConfigVO {

	private String ldapUserBase;
	private String ldapGroupBase;
	private String ldapUserName; 
	private String ldapPassword;
	
	public String getLdapUserBase() {
		return ldapUserBase;
	}
	public void setLdapUserBase(String ldapUserBase) {
		this.ldapUserBase = ldapUserBase;
	}
	public String getLdapGroupBase() {
		return ldapGroupBase;
	}
	public void setLdapGroupBase(String ldapGroupBase) {
		this.ldapGroupBase = ldapGroupBase;
	}
	public String getLdapUserName() {
		return ldapUserName;
	}
	public void setLdapUserName(String ldapUserName) {
		this.ldapUserName = ldapUserName;
	}
	public String getLdapPassword() {
		return ldapPassword;
	}
	public void setLdapPassword(String ldapPassword) {
		this.ldapPassword = ldapPassword;
	}
	
	
}
