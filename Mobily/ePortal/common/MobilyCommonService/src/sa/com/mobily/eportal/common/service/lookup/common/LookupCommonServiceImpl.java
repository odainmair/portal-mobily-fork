package sa.com.mobily.eportal.common.service.lookup.common;

import java.util.List;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.lookup.LookupDataManager;
import sa.com.mobily.eportal.common.service.lookup.vo.Lookup;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

public class LookupCommonServiceImpl implements LookupCommonService
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String clazz = LookupCommonServiceImpl.class.getName();

	@Override
	public List<Lookup> findById(String id, boolean isEnglishLocale)
	{
		LogEntryVO logEntry = null;
		try
		{
			logEntry = new LogEntryVO(Level.INFO, "findById > ID : " + id, clazz, "findById", null);
			executionContext.log(logEntry);
			List<Lookup> lookUpList = null;
			if(isNumaric(id))
				lookUpList = LookupDataManager.getGenericLookup(Long.valueOf(id), null, isEnglishLocale);
			else
				lookUpList = LookupDataManager.getCustomizedLookup(id, isEnglishLocale);
			if(lookUpList != null){
				logEntry = new LogEntryVO(Level.INFO, "findById > Records size : " + lookUpList.size(), clazz, "findById", null);
				executionContext.log(logEntry);
			}else{
				logEntry = new LogEntryVO(Level.INFO, "findById > Records size : 0 ", clazz, "findById", null);
				executionContext.log(logEntry);
			}
			return lookUpList;
		}
		catch (Exception e)
		{
			logEntry = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "findById", e);
			executionContext.log(logEntry);
			throw new RuntimeException(e);
		}

	}
	
	private static boolean isNumaric(String value){
		String numbers = "123456789";
		for (int i = 0; i < value.length(); i++)
		{
			if(numbers.indexOf(value.charAt(i)) < 0)
				return false;
		}
		return true;
	}
}
