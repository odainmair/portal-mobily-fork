package sa.com.mobily.eportal.billing.service.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.ConstantsIfc;
import sa.com.mobily.eportal.billing.util.AppConfig;
import sa.com.mobily.eportal.billing.vo.AccountPOIDReplyVO;
import sa.com.mobily.eportal.billing.vo.AccountPOIDRequestVO;
import sa.com.mobily.eportal.billing.xml.AccountPOIDXMLHandler;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.CustomerServiceUtil;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;

public class AccountPoidJMSRepository
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static final String className = AccountPoidJMSRepository.class.getName();

	public AccountPOIDReplyVO getAccountPOIDByMsisdn(String msisdn)
	{
		String method = "getAccountPOIDByMsisdn";
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getAccountPOID : start for msisdn [" + msisdn + "]", className, method);
		AccountPOIDRequestVO accountPOIDRequestVO = new AccountPOIDRequestVO();
		accountPOIDRequestVO.setMsisdn(msisdn);
		return getAccountPOID(accountPOIDRequestVO);
	}

	public AccountPOIDReplyVO getAccountPOIDByAcctNumber(String serviceAccountNumber)
	{
		String method = "getAccountPOIDByAcctNumber";
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getAccountPOID : start for serviceAccountNumber [" + serviceAccountNumber + "]", className, method);
		AccountPOIDRequestVO accountPOIDRequestVO = new AccountPOIDRequestVO();
		accountPOIDRequestVO.setServiceAccountNumber(serviceAccountNumber);
		return getAccountPOID(accountPOIDRequestVO);
	}

	public AccountPOIDReplyVO getAccountPOID(AccountPOIDRequestVO accountPOIDRequestVO)
	{
		String method = "getAccountPOID";

		AccountPOIDReplyVO replyVO = null;
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_ACCOUNT_POID);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_ACCOUNT_POID);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getAccountPOID : requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getAccountPOID : replyQueueName[" + replyQueueName + "]", className, method);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		if(accountPOIDRequestVO.getHeaderVO()!=null)
			mqAuditVO.setUserName(accountPOIDRequestVO.getHeaderVO().getRequestorUserId());
		if(FormatterUtility.isNotEmpty(accountPOIDRequestVO.getMsisdn())){
			mqAuditVO.setMsisdn(accountPOIDRequestVO.getMsisdn());
		}else{
			mqAuditVO.setMsisdn(accountPOIDRequestVO.getServiceAccountNumber());
		}
		mqAuditVO.setFunctionName(ConstantsIfc.FUN_GET_ACCOUNT_POID_INQ);
		
		// Populate Header
		ConsumerRequestHeaderVO headerVO = new CustomerServiceUtil().populateRequestHeaderVO(ConstantsIfc.FUN_GET_ACCOUNT_POID_INQ, ConstantsIfc.EPORTAL_USER);
		headerVO.setChannelTransId(RandomGenerator.generateTransactionID());
		accountPOIDRequestVO.setHeaderVO(headerVO);

		String strXMLRequest = new AccountPOIDXMLHandler().generateAccountPOIDXML(accountPOIDRequestVO);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getAccountPOID : strXMLRequest: " + strXMLRequest, className, method);
		mqAuditVO.setMessage(strXMLRequest);
		//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
		String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getAccountPOID : strXMLReply: " + strXMLReply, className, method);
		mqAuditVO.setReply(strXMLReply);
		if(FormatterUtility.isEmpty(strXMLReply)){
			mqAuditVO.setErrorMessage(ConstantsIfc.INVALID_REPLY);
			mqAuditVO.setServiceError(ConstantsIfc.INVALID_REPLY);
			MQUtility.saveMQAudit(mqAuditVO);
			
			throw new ServiceException();
		}
		replyVO = new AccountPOIDXMLHandler().parseAccountPOIDXML(strXMLReply);
		mqAuditVO.setErrorMessage(replyVO.getHeaderVO().getErrorMessage());
		mqAuditVO.setServiceError(replyVO.getHeaderVO().getErrorCode());
		MQUtility.saveMQAudit(mqAuditVO);
		
		return replyVO;
	}


	
}
