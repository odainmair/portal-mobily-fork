package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.SystemException;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface PaymentotifyDAO {
    public String processPaymentNotification(String xmlRequest) throws SystemException;
}
