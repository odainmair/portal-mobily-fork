/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;
import java.util.HashMap;


import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.valueobject.common.ManagedMSISDNDataVO;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface BBRegDAO{
    public String getIqamaIDforBB(String billingaccountNumber) throws SystemException;

	/**
	 * @param userMSISDN
	 * @return
	 */
	public ArrayList<ManagedMSISDNDataVO> getManagedWimaxNumbersList(String userMSISDN)throws SystemException;
	
	/**
	 * 
	 * @param cpeSerialNum
	 * @return
	 * @throws SystemException
	 */
	public String getInternetAccNumForCPE(String cpeSerialNum) throws SystemException;
	
	/**
	 * 
	 * @param intAccNumber
	 * @return
	 * @throws SystemException
	 */
	public HashMap getServicePackAttributes(String intAccNumber) throws SystemException;
}
