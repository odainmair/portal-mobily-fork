/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.TagIfc;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MessageHeaderVO  extends BaseVO{

    private String msgFormat;
    private String msgVersion;
    private String msgOption;
    private String requestorChannelID;
    private String replierChannelID;
    private String requestorChannelFunction;
    private String requestorUserID;
    private String lang;
    private String securityInfo;
    private String securityInfoKey;
    private String returnCode;
    private String errorCode;
    private String errorMessage;
    private String srDate;
    private String srRcvDate;
    private String srStatus = "0";
    private String funcId;
    private String securityKey;
    private String requestorLanguage;
    private String overwriteOpenOrder;
    private String chargeable; 
    
    public static void main(String[] args) {
    }
    /**
     * @return Returns the chargeable.
     */
    public String getChargeable() {
        return chargeable;
    }
    /**
     * @param chargeable The chargeable to set.
     */
    public void setChargeable(String chargeable) {
        this.chargeable = chargeable;
    }
    /**
     * @return Returns the funcId.
     */
    public String getFuncId() {
        return funcId;
    }
    /**
     * @param funcId The funcId to set.
     */
    public void setFuncId(String funcId) {
        this.funcId = funcId;
    }
    /**
     * @return Returns the lang.
     */
    public String getLang() {
        return lang;
    }
    /**
     * @param lang The lang to set.
     */
    public void setLang(String lang) {
        this.lang = lang;
    }
    /**
     * @return Returns the msgFormat.
     */
    public String getMsgFormat() {
        return msgFormat;
    }
    /**
     * @param msgFormat The msgFormat to set.
     */
    public void setMsgFormat(String msgFormat) {
        this.msgFormat = msgFormat;
    }
    /**
     * @return Returns the msgVersion.
     */
    public String getMsgVersion() {
        return msgVersion;
    }
    /**
     * @param msgVersion The msgVersion to set.
     */
    public void setMsgVersion(String msgVersion) {
        this.msgVersion = msgVersion;
    }
    /**
     * @return Returns the overwriteOpenOrder.
     */
    public String getOverwriteOpenOrder() {
        return overwriteOpenOrder;
    }
    /**
     * @param overwriteOpenOrder The overwriteOpenOrder to set.
     */
    public void setOverwriteOpenOrder(String overwriteOpenOrder) {
        this.overwriteOpenOrder = overwriteOpenOrder;
    }
    /**
     * @return Returns the replierChannelID.
     */
    public String getReplierChannelID() {
        return replierChannelID;
    }
    /**
     * @param replierChannelID The replierChannelID to set.
     */
    public void setReplierChannelID(String replierChannelID) {
        this.replierChannelID = replierChannelID;
    }
    /**
     * @return Returns the requestorChannelFunction.
     */
    public String getRequestorChannelFunction() {
        return requestorChannelFunction;
    }
    /**
     * @param requestorChannelFunction The requestorChannelFunction to set.
     */
    public void setRequestorChannelFunction(String requestorChannelFunction) {
        this.requestorChannelFunction = requestorChannelFunction;
    }
    /**
     * @return Returns the requestorChannelID.
     */
    public String getRequestorChannelID() {
        return requestorChannelID;
    }
    /**
     * @param requestorChannelID The requestorChannelID to set.
     */
    public void setRequestorChannelID(String requestorChannelID) {
        this.requestorChannelID = requestorChannelID;
    }
    /**
     * @return Returns the requestorLanguage.
     */
    public String getRequestorLanguage() {
        return requestorLanguage;
    }
    /**
     * @param requestorLanguage The requestorLanguage to set.
     */
    public void setRequestorLanguage(String requestorLanguage) {
        this.requestorLanguage = requestorLanguage;
    }
    /**
     * @return Returns the requestorUserID.
     */
    public String getRequestorUserID() {
        return requestorUserID;
    }
    /**
     * @param requestorUserID The requestorUserID to set.
     */
    public void setRequestorUserID(String requestorUserID) {
        this.requestorUserID = requestorUserID;
    }
    /**
     * @return Returns the returnCode.
     */
    public String getReturnCode() {
        return returnCode;
    }
    /**
     * @param returnCode The returnCode to set.
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    /**
     * @return Returns the securityInfo.
     */
    public String getSecurityInfo() {
        return securityInfo;
    }
    /**
     * @param securityInfo The securityInfo to set.
     */
    public void setSecurityInfo(String securityInfo) {
        this.securityInfo = securityInfo;
    }
    /**
     * @return Returns the securityKey.
     */
    public String getSecurityKey() {
        return securityKey;
    }
    /**
     * @param securityKey The securityKey to set.
     */
    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }
    /**
     * @return Returns the srDate.
     */
    public String getSrDate() {
        return srDate;
    }
    /**
     * @param srDate The srDate to set.
     */
    public void setSrDate(String srDate) {
        this.srDate = srDate;
    }
    /**
     * @return Returns the srRcvDate.
     */
    public String getSrRcvDate() {
        return srRcvDate;
    }
    /**
     * @param srRcvDate The srRcvDate to set.
     */
    public void setSrRcvDate(String srRcvDate) {
        this.srRcvDate = srRcvDate;
    }
    
	public MessageHeaderVO parseXMl(Element p_HeaderElement){
	    MessageHeaderVO header = new MessageHeaderVO();
		try {
			NodeList childList 		= p_HeaderElement.getChildNodes();
			
			for(int j = 0; j < childList.getLength(); j++){
			    if((childList.item(j)).getNodeType() != Node.ELEMENT_NODE)
			    	continue;
			    
			    Element l_Node         = (Element)childList.item(j);
			    String l_szNodeTagName = l_Node.getTagName();
			    String l_szNodeValue   = "";
			    if( l_Node.getFirstChild() != null )
			    	   l_szNodeValue   = l_Node.getFirstChild().getNodeValue();

			    if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_FUNC_ID))
			    	header.setFuncId(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.MSG_VERSION))
			    	header.setMsgVersion(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_CHANNEL_ID))
			    	header.setRequestorChannelID(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_CHANNEL_FUNCTION))
			    	header.setRequestorChannelFunction(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_USER_ID))
			    	header.setRequestorUserID(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_LANG))
			    	header.setRequestorLanguage(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_DATE))
			    	header.setSrDate(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_RCV_DATE))
			    	header.setSrRcvDate(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_STATUS))
			    	header.setSrStatus(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_REQ_SECURITY_INFO))
			    	header.setSecurityInfoKey(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SECURITY_KEY))
			    	header.setSecurityKey(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.MSG_FORMAT))
			    	header.setMsgFormat(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.RETURN_CODE))
			    	header.setReturnCode(l_szNodeValue);



			}//end for

		} catch (DOMException e) {
			// TODO Auto-generated catch block
			throw e;
		}		
		return header;
	}
	

    /**
     * @return Returns the srStatus.
     */
    public String getSrStatus() {
        return srStatus;
    }
    /**
     * @param srStatus The srStatus to set.
     */
    public void setSrStatus(String srStatus) {
        this.srStatus = srStatus;
    }
    /**
     * @return Returns the securityInfoKey.
     */
    public String getSecurityInfoKey() {
        return securityInfoKey;
    }
    /**
     * @param securityInfoKey The securityInfoKey to set.
     */
    public void setSecurityInfoKey(String securityInfoKey) {
        this.securityInfoKey = securityInfoKey;
    }
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the errorMessage.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage The errorMessage to set.
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return Returns the msgOption.
	 */
	public String getMsgOption() {
		return msgOption;
	}
	/**
	 * @param msgOption The msgOption to set.
	 */
	public void setMsgOption(String msgOption) {
		this.msgOption = msgOption;
	}
}
