package sa.com.mobily.eportal.common.service.vo;

public class StringVO extends FootPrintVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String value;

	
	private boolean includeAllInactive = false;
	private String userUID = null;
	
	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	
	/**
	 * @return the includeAllInactive
	 */
	public boolean isIncludeAllInactive()
	{
		return includeAllInactive;
	}

	/**
	 * @param includeAllInactive the includeAllInactive to set
	 */
	public void setIncludeAllInactive(boolean includeAllInactive)
	{
		this.includeAllInactive = includeAllInactive;
	}

	/**
	 * @return the userUID
	 */
	public String getUserUID()
	{
		return userUID;
	}

	/**
	 * @param userUID the userUID to set
	 */
	public void setUserUID(String userUID)
	{
		this.userUID = userUID;
	}
	
}