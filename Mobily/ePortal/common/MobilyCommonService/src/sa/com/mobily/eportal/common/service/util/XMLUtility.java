/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import sa.com.mobily.eportal.common.service.exception.SystemException;

/**
 * @author msayed
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class XMLUtility {

    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;

    /**
     * Responsble for serialize request
     * @param aDocument
     * @return
     * @throws IOException
     * Feb 17, 2008
     * XMLUtility.java
     * msayed
     */
    public static String serializeRequest(Document aDocument)throws IOException {
        log.debug("XMLUtility > serializeRequest > strat serializeRequest Method...");
        OutputFormat tempOutputFormat = new OutputFormat(aDocument);
        StringWriter tempStringWriter = new StringWriter();
        XMLSerializer tempXmlSerializer = new XMLSerializer(tempStringWriter,tempOutputFormat);
        tempXmlSerializer.serialize(aDocument.getDocumentElement());
        String tempString = tempStringWriter.toString();
        log.debug("XMLUtility > serializeRequest > serializeRequest Completed Successfuly");
        tempString = tempString.substring(tempString.indexOf('>') + 1);
        log.debug("XMLUtility > serializeRequest > serializeRequest new line index:"+tempString.indexOf('\n'));
        return tempString.charAt(0) == '\n' ? tempString.substring(1): tempString;
  }


    /**
     * This method is used to serialize the xml document object
     * @param aXMLString
     * @return
     * @throws Exception
     * Feb 17, 2008
     * XMLUtility.java
     * msayed
     */
    public static Document parseXMLString(String aXMLString) {
        log.debug("XMLUtility > parseXMLString > start parse xml string....");
        DOMParser tempDOMParser = new DOMParser();
        java.io.ByteArrayInputStream byteArrayInputStream = null;
        try {
        	
//            StringReader tempStringReader = new StringReader(aXMLString);
//            InputSource tempInputSource = new InputSource(tempStringReader);
//            tempDOMParser.parse(tempInputSource);
//            tempStringReader.close();
            
            //byteArrayInputStream = new java.io.ByteArrayInputStream(aXMLString.getBytes());
        	byteArrayInputStream = new java.io.ByteArrayInputStream(aXMLString.trim().getBytes("UTF-8"));
            org.xml.sax.InputSource inputSource = new org.xml.sax.InputSource(byteArrayInputStream);
            log.debug("XMLUtility > parseXMLString > before parsing..");
            tempDOMParser.parse(inputSource);
            log.debug("XMLUtility > parseXMLString >  parsing completed successfuly.....");
        } catch (IOException e) {
            log.fatal("XMLUtility > parseXMLString > IOException generated during parsing xml message..." + e.getMessage());
            throw new SystemException( e);
        } catch (SAXException e) {
            log.fatal("XMLUtility > parseXMLString > SAXException generated during parsing xml message..." + e.getMessage());
            throw new SystemException( e);
        }
        finally {
        	if(byteArrayInputStream != null)
				try {
					byteArrayInputStream.close();
				} catch (IOException e) {
					 log.fatal("XMLUtility > parseXMLString > IOException while closing the resources.." + e.getMessage());
				}
        }
        return tempDOMParser.getDocument();
    }

    /**
     * Geneare XML element like <MSISDN></MSISDN>
     * @param doc
     * @param parent
     * @param TagName
     * @param Tagvalue
     * Feb 17, 2008
     * XMLUtility.java
     * msayed
     */
    public static void generateElelemt(Document doc, Element parent,
            String TagName, String Tagvalue) {
        Element element = doc.createElement(TagName);
        if (Tagvalue != null)
            Tagvalue = Tagvalue.trim();
        else
            Tagvalue = "";
        element.appendChild(doc.createTextNode(Tagvalue));
        parent.appendChild(element);
    }

    /**
     * Generate XML parent tage
     * @param doc
     * @param tagName
     * @return
     * Feb 17, 2008
     * XMLUtility.java
     * msayed
     */
    public static Element openParentTag(Document doc, String tagName) {
        Element root = doc.createElement(tagName);
        return root;
    }
    

    public static void closeParentTag(Element parent, Element chield) {
        parent.appendChild(chield);
    }

}
