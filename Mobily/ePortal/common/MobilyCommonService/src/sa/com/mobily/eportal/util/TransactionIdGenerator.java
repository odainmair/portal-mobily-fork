package sa.com.mobily.eportal.util;

import java.util.Date;

import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;

/**
 * @author Suresh Vathsavai-MIT
 * @Description: 
 * @Date 30-Mar-2017
 */
public class TransactionIdGenerator
{
	
	public static String TRANSACTION_TYPE_PAYMENT="PAY";
	public static String TRANSACTION_TYPE_SERVICE="SR";
	
	public static String CHANNEL_NAME_EPOR="EPOR";
	public static String CHANNEL_NAME_EPR="EPR";
	
	
	
	
	
	/*
	 * return ID channel(max 5chars)_service(max 10 chars)_msisdn_randomnumber(8 digit alphanumeric)
	 */
	public static String generateTransactionID(TransactionIdInput input) {
		StringBuffer transactionId = new StringBuffer();
		if(input != null) {
			
			
			if(FormatterUtility.isNotEmpty(input.getTransactionType())) {
				transactionId.append(input.getTransactionType().length() <= 5 ? input.getTransactionType() : input.getTransactionType().substring(0,5));
				transactionId.append("_");
			}else {
				transactionId.append(TRANSACTION_TYPE_SERVICE);
				transactionId.append("_");
			}
			if(FormatterUtility.isNotEmpty(input.getChannelName())) {
				transactionId.append(input.getChannelName().length() <= 5 ? input.getChannelName() : input.getChannelName().substring(0,5));
				transactionId.append("_");
			}else {
				transactionId.append(CHANNEL_NAME_EPOR);
				transactionId.append("_");
			}
			/*if(FormatterUtility.isNotEmpty(input.getAccountNumber())) {
				transactionId.append(input.getAccountNumber());
				transactionId.append("_");
			}*/
			
			
			transactionId.append(RandomGenerator.generateNumericRandom(4)).append("_").append(System.currentTimeMillis());

		}
		
		
		return transactionId.toString();
		
	}
	
	/*
	 * return ID channel(max 5chars)_service(max 10 chars)_msisdn_randomnumber(8 digit alphanumeric)
	 */
	public static String generateTransactionID(String preFix) {
		StringBuffer transactionId = new StringBuffer();
		if(FormatterUtility.isNotEmpty(preFix)){
			transactionId.append(preFix);
		}else {
			transactionId.append(CHANNEL_NAME_EPR);
		}
		String srdate = MobilyUtility.FormateDate(new Date());
		transactionId.append("_"+srdate);
		transactionId.append("_"+RandomGenerator.generateNumericRandom(4));
		return transactionId.toString();
		
	}

	/**
	 * @Description: 
	 * @Date 30-Mar-2017
	 * @param args
	 */
	public static void main(String[] args)
	{
		TransactionIdInput transactionIdInput = new TransactionIdInput();
		transactionIdInput.setAccountNumber("96654323232323");
		//transactionIdInput.setChannelName(CHANNEL_NAME_EPOR);
		transactionIdInput.setTransactionType("TRANSACTION_TYPE_SERVICE");
		//System.out.println(generateTransactionID(transactionIdInput));
		System.out.println(generateTransactionID("EPOR"));

	}

}
