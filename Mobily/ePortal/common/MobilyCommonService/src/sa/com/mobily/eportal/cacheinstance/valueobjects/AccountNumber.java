package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.io.Serializable;

import sa.com.mobily.eportal.common.constants.MobilyConstants;
import sa.com.mobily.eportal.usermanagement.model.UserSubscription;

public class AccountNumber implements Serializable, Cloneable {
	
	public  String getNumber(){
		return null;
	}
	
	public  void setNumber(String accountNumber){
		
	}
	
	public static final AccountNumber getInstance( UserSubscription userSubscription ){
		AccountNumber accountNumber = null; 
//		switch (userSubscription.getSubscriptionType().intValue()) {
		switch (userSubscription.getSubscriptionType()) {
		case MobilyConstants.SERVICES.GSM:
			accountNumber = new MSISDNNumber(userSubscription.getMsisdn());
			break;
		case MobilyConstants.SERVICES.CONNECT:
			accountNumber = new MSISDNNumber(userSubscription.getMsisdn());
			break;
		case MobilyConstants.SERVICES.BROADBAND:
			accountNumber = new ServiceAccountNumber(userSubscription.getServiceAcctNumber());
			break;
		case MobilyConstants.SERVICES.ELIFE:
			accountNumber = new ServiceAccountNumber(userSubscription.getServiceAcctNumber());
			break;
		case MobilyConstants.SERVICES.WIMAX:
			accountNumber = new ServiceAccountNumber(userSubscription.getServiceAcctNumber());
			break;
		default:
			accountNumber = new AccountNumber();
			break;
		}
		
		return accountNumber;
	}

}
