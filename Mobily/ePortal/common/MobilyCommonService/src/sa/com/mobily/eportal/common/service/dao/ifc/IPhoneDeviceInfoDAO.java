package sa.com.mobily.eportal.common.service.dao.ifc;


import java.util.ArrayList;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.vo.IPhoneDeviceDetailsVO;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IPhoneDeviceInfoDAO {

	public ArrayList<IPhoneDeviceDetailsVO> getAllIPhoneDeviceInfo()throws SystemException;
}
