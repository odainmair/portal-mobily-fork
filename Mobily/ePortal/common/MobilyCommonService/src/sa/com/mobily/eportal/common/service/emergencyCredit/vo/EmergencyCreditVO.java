package sa.com.mobily.eportal.common.service.emergencyCredit.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class EmergencyCreditVO extends BaseVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String ecID;
	private String displayTextEn;
	private String displayTextAr;
	private String ecLevel;
	private String parentID;
	private String subscriptionConfig;
	private String status;
	private String priority;
	/**
	 * @return the ecID
	 */
	public String getEcID()
	{
		return ecID;
	}
	/**
	 * @param ecID the ecID to set
	 */
	public void setEcID(String ecID)
	{
		this.ecID = ecID;
	}
	/**
	 * @return the displayTextEn
	 */
	public String getDisplayTextEn()
	{
		return displayTextEn;
	}
	/**
	 * @param displayTextEn the displayTextEn to set
	 */
	public void setDisplayTextEn(String displayTextEn)
	{
		this.displayTextEn = displayTextEn;
	}
	/**
	 * @return the displayTextAr
	 */
	public String getDisplayTextAr()
	{
		return displayTextAr;
	}
	/**
	 * @param displayTextAr the displayTextAr to set
	 */
	public void setDisplayTextAr(String displayTextAr)
	{
		this.displayTextAr = displayTextAr;
	}
	/**
	 * @return the ecLevel
	 */
	public String getEcLevel()
	{
		return ecLevel;
	}
	/**
	 * @param ecLevel the ecLevel to set
	 */
	public void setEcLevel(String ecLevel)
	{
		this.ecLevel = ecLevel;
	}
	/**
	 * @return the parentID
	 */
	public String getParentID()
	{
		return parentID;
	}
	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(String parentID)
	{
		this.parentID = parentID;
	}
	/**
	 * @return the subscriptionConfig
	 */
	public String getSubscriptionConfig()
	{
		return subscriptionConfig;
	}
	/**
	 * @param subscriptionConfig the subscriptionConfig to set
	 */
	public void setSubscriptionConfig(String subscriptionConfig)
	{
		this.subscriptionConfig = subscriptionConfig;
	}
	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}
	/**
	 * @return the priority
	 */
	public String getPriority()
	{
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(String priority)
	{
		this.priority = priority;
	}
	
}
