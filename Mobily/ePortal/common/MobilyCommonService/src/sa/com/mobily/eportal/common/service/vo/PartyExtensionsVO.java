/*
 * Created on Feb 15, 2012
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PartyExtensionsVO extends BaseVO {
	
	private String tradingName = null;
	private String keyAccountManagerPF = null;
	private String keyAccountManagerEmail = null;
	private String noOfEmployees = null;
	private String companyAccountNumber = null;
	private String idType = null;
	private String noOfOffices = null;
	
	public String getTradingName() {
		return tradingName;
	}
	public void setTradingName(String tradingName) {
		this.tradingName = tradingName;
	}
	public String getKeyAccountManagerPF() {
		return keyAccountManagerPF;
	}
	public void setKeyAccountManagerPF(String keyAccountManagerPF) {
		this.keyAccountManagerPF = keyAccountManagerPF;
	}
	public String getKeyAccountManagerEmail() {
		return keyAccountManagerEmail;
	}
	public void setKeyAccountManagerEmail(String keyAccountManagerEmail) {
		this.keyAccountManagerEmail = keyAccountManagerEmail;
	}
	public String getNoOfEmployees() {
		return noOfEmployees;
	}
	public void setNoOfEmployees(String noOfEmployees) {
		this.noOfEmployees = noOfEmployees;
	}
	public String getCompanyAccountNumber() {
		return companyAccountNumber;
	}
	public void setCompanyAccountNumber(String companyAccountNumber) {
		this.companyAccountNumber = companyAccountNumber;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getNoOfOffices() {
		return noOfOffices;
	}
	public void setNoOfOffices(String noOfOffices) {
		this.noOfOffices = noOfOffices;
	}

}
