/*
 * Created on Apr 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;

/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ContactUSCacheDAO {
	public ArrayList getContactUsInquiryTypeItems();

	public ArrayList getContactUsBusinessUserInquiryTypeItems();
}
