package sa.com.mobily.eportal.google.captcha.v2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.logging.Level;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.google.gson.Gson;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.google.captcha.v2.vo.CaptchaResponseVO;
import sa.com.mobily.eportal.google.captcha.v2.vo.GoogleReCaptchaV2VO;

public class GoogleReCaptchaV2Impl implements GoogleReCaptchaV2{
	private static ExecutionContext ec = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.GOOGLE_RECAPTCHA_V2_LOGGER_NAME);
	private static HostnameVerifier allHostsValid = null;  
	private static String strURL = "https://www.google.com/recaptcha/api/siteverify?secret=";
	@Override
	public CaptchaResponseVO validateCaptcha(GoogleReCaptchaV2VO googleReCaptchaV2VO){
		
		 ec.log(Level.SEVERE, "GoogleReCaptchaV2VO::"+googleReCaptchaV2VO,null,null);
		try {
			  // Install the all-trusting host verifier
			    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
			    SocketAddress addr = new InetSocketAddress("proxy", 8080);
				Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
			    URL url = new URL(strURL+googleReCaptchaV2VO.getSecretKey()+"&response="+googleReCaptchaV2VO.getSiteResponse()+"&remoteip="+googleReCaptchaV2VO.getRemoteIp());
			    URLConnection con = url.openConnection(proxy);
			    ec.log(Level.SEVERE, "Connection::"+con,null,null);
			    String line, outputString = "";
			    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				while ((line = br.readLine()) != null) {
					outputString += line;
				}
				ec.log(Level.SEVERE, "outputString::"+outputString,null,null);
				CaptchaResponseVO capRes = new Gson().fromJson(outputString, CaptchaResponseVO.class);
				ec.log(Level.SEVERE, "CaptchaResponseVO::"+capRes,null,null);
				return capRes;
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return null;
		}
	
	static{
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };
    	 // Install the all-trusting trust manager
        SSLContext sc=null;
    	try {
    		sc = SSLContext.getInstance("TLSv1");
    		sc.init(null, trustAllCerts, new java.security.SecureRandom());
    	} catch (NoSuchAlgorithmException e) {
    		e.printStackTrace();
    	} catch (KeyManagementException e) {
    		e.printStackTrace();
    	}
        
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        allHostsValid  = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
	}
	
	private GoogleReCaptchaV2Impl(){
		
	}

	
	public static GoogleReCaptchaV2Impl getInstance(){
		
		return GoogleReCaptchaV2ImplHolder.googleReCaptchaV2ImplInstance;
	}
	
	private static class GoogleReCaptchaV2ImplHolder {
		private static final GoogleReCaptchaV2Impl googleReCaptchaV2ImplInstance = new GoogleReCaptchaV2Impl();
	}
	

}
