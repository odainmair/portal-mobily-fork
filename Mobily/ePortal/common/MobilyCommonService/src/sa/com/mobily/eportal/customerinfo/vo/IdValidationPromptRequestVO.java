/**
 * 
 */
package sa.com.mobily.eportal.customerinfo.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author Suresh Vathsavai - MIT
 * Date: Nov 28, 2013
 */
public class IdValidationPromptRequestVO extends BaseVO {
	
	
	private static final long serialVersionUID = 1L;
	
	private String msisdn="";
	private String channelId = null;
	/**
	 * @return the msisdn
	 */
	public String getMsisdn()
	{
		return msisdn;
	}
	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	/**
	 * @return the channelId
	 */
	public String getChannelId()
	{
		return channelId;
	}
	/**
	 * @param channelId the channelId to set
	 */
	public void setChannelId(String channelId)
	{
		this.channelId = channelId;
	}
	
	
}
