/*
 * Created on 24/07/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.ibm.math.BigDecimal;
//import sa.com.mobily.eportal.util.AppConfig;
//import sa.com.mobily.eportal.util.AppConfigKeysIfc;

/** 
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class FormatterUtility {
	
	private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    private static final Locale arabicLocale=new Locale("ar","","");
	
    public static String FormateDate(Date date){
		String formatedDate = "";
		if(date != null){
		SimpleDateFormat sformat=new SimpleDateFormat("yyyyMMddHHmmss");
		formatedDate=sformat.format(date);
		}
		return formatedDate;
	}
	
	public static String toLowerCase(String str){
		if (!isEmpty(str))
			 return str.toLowerCase();
		return str;
	}
	
	public static String getUserFriendlyFormat(String strMSISDN) {
		if (strMSISDN == null || strMSISDN.trim().equals(""))
			return strMSISDN;
		String eightDigitNumber=get8DigitFormat(strMSISDN);
		if (eightDigitNumber.length()<8)
			return  eightDigitNumber;
		else
			return "05" + get8DigitFormat(strMSISDN);
	}

	public static String getUniversalFormat(String strMSISDN) {
		if (strMSISDN == null || strMSISDN.trim().equals(""))
			return strMSISDN;
		else
			return "9665" + get8DigitFormat(strMSISDN);
	}

	private static String get8DigitFormat(String strMSISDN) {
		if (strMSISDN == null || strMSISDN.trim().equals(""))
			return strMSISDN;
		strMSISDN = strMSISDN.trim();
		if (strMSISDN.length() > 8)
			return strMSISDN.substring(strMSISDN.length() - 8);
		else
			return strMSISDN;
	}
	
	public static String checkNull(String aValue) {
		if(aValue == null)
			return "";
		return aValue;
			
	}
	public static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0
				|| str.trim().equalsIgnoreCase("null");
	}

	public static boolean isNotEmpty(String str) {
		return (str!=null && str.length()>0);
		
	}

	public static long parseLong(String str, long valueIfEmpty) {
		return isEmpty(str) ? valueIfEmpty : Long.parseLong(str.trim());
	}

	/**
	 * 
	 * to check is the string number or chars
	 */
	public static boolean isNumber(String str) {
		try {
			Double.parseDouble(str);
			return true;
		} catch (Throwable r) {
			return false;
		}
	}

	public static int parseInt(String str, int valueIfEmpty) {
		return isEmpty(str) ? valueIfEmpty : Integer.parseInt(str.trim());
	}

	public static double parseDouble(String str, double valueIfEmpty) {
		  if ( isEmpty(str)) return valueIfEmpty;
		    str=str.trim();
		    String tempStr="";
		    for (int i=0;i<str.length();i++){
		    	if (!(str.charAt(i)==','))
		    		tempStr+=str.charAt(i);
		    }
		    	
		    
		return isEmpty(tempStr) || !isNumber(tempStr)? valueIfEmpty : Double.parseDouble(tempStr.trim());
	}

	public static String numberFormatter(BigDecimal number) {
		NumberFormat numberFormat = NumberFormat.getNumberInstance();
		return numberFormat.format(number);
	}

	public static String numberFormatter(String numberStr) {
		String str = "";
		try {
			NumberFormat numberFormat = NumberFormat.getNumberInstance();
			str = numberFormat.format(Double.parseDouble(numberStr));
		} catch (Exception e) {
			log.error(e);
		}
		return str;
	}

	public static String numberFormatter(double number) {
		String str = "";
		try {
			NumberFormat numberFormat = NumberFormat.getNumberInstance();
			numberFormat.setMaximumFractionDigits(2);
			numberFormat.setMinimumFractionDigits(1);
			str = numberFormat.format(number);
		} catch (Exception e) {
			log.error(e);
		}
		return str;
	}

	
	public static String numberFormatter(double number,Locale locale) {
		String str = "";
		try {
			NumberFormat numberFormat = NumberFormat.getNumberInstance(locale);
			numberFormat.setMaximumFractionDigits(2);
			numberFormat.setMinimumFractionDigits(1);
			str = numberFormat.format(number);
		} catch (Exception e) {
			log.error(e);
		}
		return str;
	}
	
	public static java.util.Date stringToDate(String dateStr, String DateFormat)
			throws Exception {

		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(
				DateFormat);
		return sdf.parse(dateStr);

	}

	public static String DateToString(java.util.Date date, String format)
			throws Exception {
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(format);
		return sdf.format(date);
	}

	public static String DateToString(java.util.Date date, String format,Locale locale)
	 {  String tempStr="";
		try{
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(format,locale);
            tempStr=sdf.format(date);
		}catch(Exception e){}
		return tempStr;
}
	public static String DateFormatter(String dateStr) {
		String returnStr = "";
		try {
			Date date = stringToDate(dateStr, "yyyyMMdd");
			returnStr = "<table cellspacing='0' cellpadding='0' width='100%' border='0'><tr><td nowrap class=logintextn dir='ltr'>" + DateToString(date, "dd MMM yy") + "</td><td width='100%'></td></tr></table>";
		} catch (Exception e) {
			log.error("Error in DateFormatter(String dateStr) " + e);
		}
		return returnStr;
	} 

	
	
	
	public static String formateCallDates(Date date){
		try{
		return "<table cellspacing='0' cellpadding='0' width='100%' border='0'><tr><td class=logintextn nowrap dir='ltr'>" + DateToString(date,"dd MMM yy HH:mm:ss") + "</td><td width='100%'></td></tr></table>";
		}catch (Exception e){
		    return "";
		}
	}

	public static String formateCallDates(Date date,Locale locale){
		String returnStr = "";
		try {
              if(locale.getLanguage().equals(arabicLocale.getLanguage()))
        			returnStr = DateToString(date, AppConfig.getInstance().get(AppConfigKeysIfc.CALLS_DATE_FORMAT_AR),locale) ;
              else
            	returnStr = DateToString(date, AppConfig.getInstance().get(AppConfigKeysIfc.CALLS_DATE_FORMAT),locale) ;
		} catch (Exception e) {
			log.error(e);
		}
		return returnStr;
	}
	
	public static String formatLastLoginDate( Date   date,
	        								  Locale locale )
	{
		String returnStr = "";
		try {
              if(locale.getLanguage().equals(arabicLocale.getLanguage()))
                  returnStr = DateToString(date, AppConfig.getInstance().get(AppConfigKeysIfc.LAST_LOGIN_DATE_FORMAT_AR),locale) ;
              else
                  returnStr = DateToString(date, AppConfig.getInstance().get(AppConfigKeysIfc.LAST_LOGIN_DATE_FORMAT),locale) ;
		} 
		catch (Exception e) 
		{
			log.error(e);
		}
		return returnStr;
	}
	
	public static String DateFormatter(Date date) {
		String returnStr = "";
		try {

			returnStr = "<table cellspacing='0' cellpadding='0' width='100%' border='0'><tr><td nowrap class=logintextn dir='ltr'>" + DateToString(date, "dd MMM yy") + "</td><td width='100%'></td></tr></table>";
		} catch (Exception e) {
			log.error(e);
		}
		return returnStr;
	}

	
	
	public static String DateFormatter(Date date,Locale locale) {
		String returnStr = "";
		try {
              if(locale.getLanguage().equals(arabicLocale.getLanguage()))
        			returnStr = DateToString(date, AppConfig.getInstance().get(AppConfigKeysIfc.DATE_FORMAT_AR),locale) ;
              else
            	returnStr = DateToString(date, AppConfig.getInstance().get(AppConfigKeysIfc.DATE_FORMAT),locale) ;
		} catch (Exception e) {
			log.error(e);
		}
		return returnStr;
	}
	
	public static String formateMobileNumberToHidden(String mobileNumber) {

		if (isEmpty(mobileNumber))
			return "";
		if (mobileNumber.length() < 5)
			return mobileNumber;
		StringBuffer st = new StringBuffer(mobileNumber);
		st = st.replace(mobileNumber.length() - 4, mobileNumber.length(),
				"####");
		return st.toString();
	}
	
	//Changes to restrict customer inquiry for null MSISDN
	public static boolean isNumeric(String str) {
		for (char c : str.toCharArray())
		{
			if (!Character.isDigit(c)) return false;
		}
		return true;
	}
	
	public static String formatMobileNumber(String mobileNumber) {

		if (isEmpty(mobileNumber))
			return "";
		else if(isNotEmpty(mobileNumber)) {
			if(mobileNumber.startsWith("+9665") || mobileNumber.startsWith("009665")) {
				mobileNumber = mobileNumber.substring(mobileNumber.indexOf("9665"));
			} else if(mobileNumber.startsWith("05")) {
				mobileNumber = "9665" + mobileNumber.substring(mobileNumber.indexOf("05") + 2);
			}
		}

		return mobileNumber;
	}
}