package sa.com.mobily.eportal.common.service.vo;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class UserRedirectionVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private Long id;
    private Long userAcctId;
    private Timestamp creationDate;
    private String redirectURL;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserAcctId() {
		return userAcctId;
	}
	public void setUserAcctId(Long userAcctId) {
		this.userAcctId = userAcctId;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	public String getRedirectURL()
	{
		return redirectURL;
	}
	public void setRedirectURL(String redirectURL)
	{
		this.redirectURL = redirectURL;
	}
	

}