/**
 * 
 */
package sa.com.mobily.eportal.common.exception.portletfilter;

import java.io.IOException;
import java.util.logging.Level;
import java.util.Iterator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.ActionRequestWrapper;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.ResourceFilter;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.handler.ExceptionHandlingTemplate;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/**
 * @author 80057930
 *
 */
public class UploadPortletFilter implements ActionFilter, ResourceFilter
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.EX_HANDLER_FILTER_SERVICE_LOGGER_NAME);
    private static final String className=UploadPortletFilter.class.getName();
    String validate=null;
	
	public void destroy()
	{
		
	}

	public void init(FilterConfig filterConfig) throws PortletException{
		executionContext.audit(Level.INFO, "Started",className,"Init Method");
		if(filterConfig.getInitParameter("fileType")!=null){
			 validate=filterConfig.getInitParameter("fileType");
			executionContext.audit(Level.INFO, " UploadPortletFilter >> **** Portlet Filter **** [ "+ validate +"]", className,"Init Method");
		}
		
	}

	
	public void doFilter(ResourceRequest request, ResourceResponse response, FilterChain chain) throws IOException, PortletException
	{
		boolean exceptionOccured = false;
		String method="doFilterResource";
		boolean status=false;
		try{
			
			executionContext.audit(Level.INFO, " UploadPortletFilter >> ******************* Portlet Filter Pre-processing a Resource request ************", className, method);
			/*if(request.getAttribute("FileName")!=null){
				String fileName= (String) request.getAttribute("FileName");
				executionContext.audit(Level.INFO, "UploadPortletFilter >> File Name ["+fileName+"]",className, method);
				status=validateFileFormat(fileName);
				executionContext.audit(Level.INFO, "UploadPortletFilter >> Status ["+status+"]",className, method);
			}*/
			request.setAttribute("VALIDATION", validate);
			//request.setAttribute("Result", status);
			executionContext.audit(Level.INFO, "UploadPortletFilter >> Calling do Filter >> ResourceRequest",className, method);
			chain.doFilter(request, response);
			executionContext.audit(Level.INFO, " UploadPortletFilter >> ******************* Portlet Filter Post-processing a Resource request ************", className, method);
		 }catch(Throwable exception){
			    exceptionOccured = true;
				//String errorCode = ExceptionHandlingTemplate.handleException(exception, request);
			    executionContext.audit(Level.SEVERE, "EXCEPTION In doFilter >> " + exceptionOccured , className, method);
				executionContext.audit(Level.SEVERE, "EXCEPTION In doFilter >> " + exception.getMessage(), className, method);
		 }finally{
			 executionContext.endMethod(className, method, null);
		 }
		
	}


	public void doFilter(ActionRequest portletReq, ActionResponse portletResp, FilterChain chain) throws IOException, PortletException
	{
		String method="doFilterAction";
		boolean exceptionOccured = false;
		boolean checkStatus;
		//boolean status=false;
		try{
			
			executionContext.audit(Level.INFO, " UploadPortletFilter >> ******************* Portlet Filter Pre-processing a Action request ************", className, method);
			/*if(request.getAttribute("FileName")!=null){
				String fileName= (String) request.getAttribute("FileName");
				executionContext.audit(Level.INFO, "UploadPortletFilter >> File Name ["+fileName+"]",className, method);
				status=validateFileFormat(fileName);
				executionContext.audit(Level.INFO, "UploadPortletFilter >> Status ["+status+"]",className, method);
			}*/
			portletReq.setAttribute("VALIDATION", validate); //// forward the wrapped request to the next filter in the chain
			//request.setAttribute("Result", status);
			ActionRequestWrapper actionReq= new ActionRequestWrapper(portletReq);
			boolean isMultipart = PortletFileUpload.isMultipartContent(actionReq);
			if(isMultipart){
				// Create a factory for disk-based file items
				FileItemFactory factory = new DiskFileItemFactory();
				PortletFileUpload portletFile= new PortletFileUpload(factory);
				List item=portletFile.parseRequest(actionReq);
				executionContext.audit(Level.INFO, "UploadPortletFilter >> Calling do Filter >> item"+item,className, method);	
				portletReq.setAttribute("item", item);      //Setting the request to item attribute
				Iterator iter = item.iterator();
				
				while(iter.hasNext()){
					FileItem fileitem = (FileItem) iter.next();
					if (fileitem.isFormField()){
						executionContext.audit(Level.INFO, "UploadPortletFilter >> Calling do Filter >> isFormField",className, method);			
					}else {
						String fileName=fileitem.getName();
						long fileSize=fileitem.getSize();
						if(fileName!=null && fileName.length()>0){
							executionContext.audit(Level.INFO, "UploadPortletFilter >> Inside do Filter >> ActionRequest >> FileNmae ["+fileName+"] ",className, method);
							executionContext.audit(Level.INFO, "UploadPortletFilter >> Inside do Filter >> ActionRequest >> FileSize ["+fileSize+"] ",className, method);
							checkStatus=ValidatefileExtenstion(fileName);
							executionContext.audit(Level.INFO, "UploadPortletFilter >> CheckStatus ["+checkStatus+"]",className, method);
							   if(checkStatus){
								   executionContext.audit(Level.INFO, "UploadPortletFilter >> Valid Format ["+checkStatus+"]",className, method);
								   portletReq.setAttribute("Result", checkStatus);
							        }else{
							        	executionContext.audit(Level.INFO, "UploadPortletFilter >> InValid Format ["+checkStatus+"] ",className, method);
							        	portletReq.setAttribute("Result", checkStatus);
							        	break;
							          }
						}
						
					}
					
				}
			}
			
			executionContext.audit(Level.INFO, "UploadPortletFilter >> Calling do Filter >> ActionRequest",className, method);
			executionContext.audit(Level.INFO, "UploadPortletFilter >> Calling do Filter >> request"+portletReq,className, method);
			chain.doFilter(portletReq, portletResp);
			executionContext.audit(Level.INFO, " UploadPortletFilter >> ******************* Portlet Filter Post-processing a Action request ************", className, method);
		}catch(Throwable exception){
			exceptionOccured = true;
			//String errorCode = ExceptionHandlingTemplate.handleException(exception, request);
		    executionContext.audit(Level.SEVERE, "EXCEPTION In doFilter >> " + exceptionOccured , className, method);
			executionContext.audit(Level.SEVERE, "EXCEPTION In doFilter >> " + exception.getMessage(), className, method);
		}finally{
			executionContext.endMethod(className, method, null);
		}
		
		
	}

	private boolean ValidatefileExtenstion(String fileName)
	{
		executionContext.audit(Level.INFO, "UploadPortletFilter >> Inside Validation ",className, "ValidatefileExtenstion");
		//Checking for double extenstion
		int firstOccurance=fileName.indexOf(".");
		int lastIndex = fileName.lastIndexOf("."); 
		executionContext.audit(Level.INFO, "UploadPortletFilter >> ValidatefileExtenstion >> 1st Occurance ["+firstOccurance+"] ,lastOccurance ["+lastIndex+"]  ",className, "ValidatefileExtenstion");
		if(firstOccurance==lastIndex && lastIndex >=0){
			String fileExtenstion= fileName.substring(lastIndex+1);
			executionContext.audit(Level.INFO, "UploadPortletFilter >> Extenstion ["+fileExtenstion+"] ",className, "ValidatefileExtenstion");
			 //[ Xlsx|xls|JPG|JPEG|PDF|doc|docx|png] validate
			if(validate.contains(fileExtenstion.toLowerCase()) && !fileExtenstion.contains("|")){
				executionContext.audit(Level.INFO, "UploadPortletFilter >>  Valid extenstion >> Extenstion ["+fileExtenstion+"] ",className, "ValidatefileExtenstion");
				return true;
			}else{
				executionContext.audit(Level.INFO, "UploadPortletFilter >>  In Valid File extenstion >> Extenstion ["+fileExtenstion+"] ",className, "ValidatefileExtenstion");
				return false;
			}
		}else{
			executionContext.audit(Level.INFO, "UploadPortletFilter >> Not Valid extenstion >> Extenstion ["+fileName+"] ",className, "ValidatefileExtenstion");
			return false;
		}
		
		
	}
	
	

}
