package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.io.Serializable;

public class MSISDNNumber extends AccountNumber implements Serializable, Cloneable {
	private String number;
	
	public MSISDNNumber( String msisdnNumber ){
		this.number = msisdnNumber;
	}

	@Override
	public void setNumber(String accountNumber) {
		this.number = accountNumber;
	}

	@Override
	public String getNumber() {
		return this.number;
	}

}
