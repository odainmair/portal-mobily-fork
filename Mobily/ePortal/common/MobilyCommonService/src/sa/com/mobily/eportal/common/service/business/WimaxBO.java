/*
 * Created on Oct 29, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.BBRegDAO;
import sa.com.mobily.eportal.common.service.dao.imp.SeibelBBRegDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WimaxBO {
	private static final Logger log = LoggerInterface.log;
	OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	//SeibelBBRegDAO seibelBBRegDAO = (SeibelBBRegDAO) daoFactory.getBBRegDAO();
	BBRegDAO seibelBBRegDAO = (SeibelBBRegDAO) daoFactory.getBBRegDAO();
	
	
	public String getIqamaIdforBB(String bbAccountNumber) throws SystemException{
		String iqamaID = null;
		iqamaID = seibelBBRegDAO.getIqamaIDforBB(bbAccountNumber);
		log.debug(iqamaID + "   iqamaID in wimax BO ");
		return iqamaID;
		
	}
	
	public ArrayList getManagedWimaxNumbersList(String userMSISDN)throws SystemException{
		log.debug("wimax BO > getManagedWimaxNumbersList > called");
		return seibelBBRegDAO.getManagedWimaxNumbersList(userMSISDN);
	}
	/**
	 * 
	 * @param intAccNumber
	 * @return
	 * @throws SystemException
	 */
	public HashMap getServicePackAttributes(String intAccNumber)throws SystemException{
		log.debug("wimax BO > getServicePackAttributes > called");
		return seibelBBRegDAO.getServicePackAttributes(intAccNumber);
	}
    
	/**
	 * 
	 * @param cpeSerialNumber
	 * @return
	 * @throws SystemException
	 */
	public String getInternetAccNumForCPE(String cpeSerialNumber)throws SystemException{
		log.debug("wimax BO > getInternetAccNumForCPE > called");
		return seibelBBRegDAO.getInternetAccNumForCPE(cpeSerialNumber);
	}
}