package sa.com.mobily.eportal.cacheinstance.dao;

import javax.persistence.EntityManager;

import sa.com.mobily.eportal.usermanagement.model.UserSession;

public interface OldSessionVoDao
{
	UserSession findByUsername(String userName, EntityManager em);

	UserSession findBySessionID(String sessionID, EntityManager em);

	UserSession findBySubscriptionID(String subscriptionID, EntityManager em);

	void saveOrUpdate(UserSession instance, EntityManager em);
}