/*
 * Created on Feb 5, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.db;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

import com.ibm.websphere.ce.cm.StaleConnectionException;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class DataBaseConnectionHandler {
	public static final int MAX_TRIAL_STALE_CONNECTION=4;
	
	protected static final HashMap datasourcesHashMap = new HashMap();	
	private static final Logger log = LoggerInterface.log;
	
	
	
/**
 * get Connection from DataSource
 * @param strDataSourceName
 * @return Connection
 * @throws NamingException
 * @throws SQLException
 * Feb 17, 2008
 * DataBaseConnectionHandler.java
 * msayed
 */
public static Connection getDBConnection(String strDataSourceName) {
		log.info("DataBaseConnectionHandler > getDBConnection for datSource ["+strDataSourceName+"]");
		Connection connection = null;
		try {
			DataSource dataSource = getDataSource(strDataSourceName);
			dataSource.setLoginTimeout(30);
			boolean retry = false;
			int retryAttempts=0;
			do {
				try {
					connection=dataSource.getConnection();
				}catch (StaleConnectionException scException){
					if (retryAttempts++ < MAX_TRIAL_STALE_CONNECTION)
						retry = true;
					else
						throw new SystemException(scException);
				}
				} while (retry); 
//			connection = dataSource.getConnection();
		} catch(StaleConnectionException e){
			log.fatal("TDataBaseConnectionHandler > he DataBase Connection cached Object not valid");
//			try{
//			rePairExistDataSources(strDataSourceName);
//			return getDBConnection(strDataSourceName);
//			}catch(SystemException e1){
//				log.fatal("getDBConnection > Failed in rePairExistDataSources  >"+e.getMessage());
//			}
		}
		catch(SQLException e){
			log.fatal("DataBaseConnectionHandler > getDBConnection >SQLException while getConnection > "+e);
			throw new SystemException(e);
			}
		catch (NamingException e) {
		    log.fatal("DataBaseConnectionHandler > getDBConnection > NamingException in retriving DB Connection > "+e);
			// TODO Auto-generated catch block
			throw new SystemException(e);
		}catch(SystemException e){
			throw e;
		}
		return connection;
		}



/**
 *  for test only
 * @return
 */
public static Connection getLocalSiebelConnection() {
	Connection connection = null;
    try
	    {
        Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
        connection = DriverManager.getConnection( "jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=ford1)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=sblsit)))",
             						 "eportal",
        							 "eportal")  ;
    }catch (SQLException e)
    {
    	e.printStackTrace();
        // TODO Auto-generated catch block
    	log.fatal("DatBaseHandler > getBSlConnection >SQLException >" +e.getMessage());
    } catch (InstantiationException e) {
		// TODO Auto-generated catch block
    	throw new SystemException(e);
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		throw new SystemException(e);
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		throw new SystemException(e);
	}
    return connection;
	}

		
/**
 *  for test only
 * @return
 */
public static Connection getLocalConnection() {
	Connection connection = null;
    try
	    {
        Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
        connection = DriverManager.getConnection( "jdbc:oracle:thin:@10.6.60.36:1521:epp1",
             						 "eedbusr",
        							 "eedbusr")  ;
    }catch (SQLException e)
    {
    	e.printStackTrace();
        // TODO Auto-generated catch block
    	log.fatal("DatBaseHandler > getBSlConnection >SQLException >" +e.getMessage());
    } catch (InstantiationException e) {
		// TODO Auto-generated catch block
    	throw new SystemException(e);
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		throw new SystemException(e);
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		throw new SystemException(e);
	}
    return connection;
	}


public static Connection getLocalInConnection() {
	Connection connection = null;
    try
	    {
        Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
        connection = DriverManager.getConnection( "jdbc:oracle:thin:@10.14.11.203:1521:EPDEV",
             						 "eedbusr",
        							 "eedbusr")  ;
    }catch (SQLException e)
    {
    	e.printStackTrace();
        // TODO Auto-generated catch block
    	log.fatal("DatBaseHandler > getBSlConnection >SQLException >" +e.getMessage());
    } catch (InstantiationException e) {
		// TODO Auto-generated catch block
    	throw new SystemException(e);
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		throw new SystemException(e);
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		throw new SystemException(e);
	}
    return connection;
	}


	/**
	 * return the connection back to pool
	 * @param connection
	 * Feb 17, 2008
	 * DataBaseConnectionHandler.java
	 * msayed
	 */
	public static void closeDBConnection(Connection connection){
		if(connection !=null)
		 {
			try {
				connection.close();  
				connection=null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				log.fatal("Exception During Close Connection");
				
			}
			}
		
	}
	
	private static DataSource getDataSource( String aDatasourceName ) throws NamingException
	{
		log.debug("get DataSource Connection to DataSourceName ["+aDatasourceName+"]");
		DataSource dataSource = (DataSource) datasourcesHashMap.get( aDatasourceName );
		InitialContext context = null;
		//check if datasource exist in caching or not

		// get dataSource Instance from application server
		if(dataSource == null){
			  try {
				context = new InitialContext();
				  dataSource = (DataSource)context.lookup("jdbc/"+aDatasourceName); 
				  datasourcesHashMap.put(aDatasourceName,dataSource);
				  log.debug("DataSource ["+aDatasourceName+"] retrieved Successfuly....");
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				log.fatal("error in retrieving datasource name ["+aDatasourceName+"]");
				throw e;
    			} 
			}
		
		return dataSource;
		
	}
	
	private static void rePairExistDataSources(String aDatasourceName){
		log.debug("Repair Exist DataSource Object ["+aDatasourceName+"]");
		try {
			DataSource dataSource = getDataSource(aDatasourceName);
			datasourcesHashMap.put(aDatasourceName,dataSource);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			log.fatal("Fail in retrieve DataSource Object  "+e);
			throw new SystemException(e);
			
		}
		}
	
	
	public static void main(String[] args) {
	}
}
