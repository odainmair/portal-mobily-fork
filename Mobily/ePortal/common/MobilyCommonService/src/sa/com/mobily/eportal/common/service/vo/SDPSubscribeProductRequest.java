package sa.com.mobily.eportal.common.service.vo;

public class SDPSubscribeProductRequest {

	private String msisdn;
	private String productId;
	private int type; // 0 for SMS
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
