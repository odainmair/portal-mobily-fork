package sa.com.mobily.eportal.billing.xml;

import java.util.StringTokenizer;
import java.util.logging.Level;

import org.apache.commons.lang.StringUtils;

import sa.com.mobily.eportal.billing.vo.FreeBalancesReplyVO;
import sa.com.mobily.eportal.billing.vo.FreeBalancesRequestVO;
import sa.com.mobily.eportal.billing.vo.MyFreeBalancesReplyVO;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

public class FreeBenefitsXMLHandler implements TagIfc

{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static String clazz = FreeBenefitsXMLHandler.class.getName();

	/**
	 * @MethodName: generateXMLRequestMessage
	 * <p>
	 * This method generates the XML message for MQ in order to get the free benefits for user
	 * 
	 * @param FreeBalancesRequestVO freeBalancesRequestVO
	 * 
	 * @return String requestMessage
	 * 
	 * @author Abu Sharaf
	 */
	public String generateXMLRequestMessage(FreeBalancesRequestVO freeBalancesRequestVO)
	{
		String requestMessage = "";
		try
		{
			String isRG = "N";
			if (freeBalancesRequestVO.isRG())
			{
				isRG = "Y";
			}
			if (freeBalancesRequestVO.getCustomerType() != null)
				requestMessage = "FreeBalanceReq:MSISDN," + freeBalancesRequestVO.getMsisdn() + ":CustomerType," + freeBalancesRequestVO.getCustomerType() + ":RG," + isRG + ";";
			else
				requestMessage = "FreeBalanceReq:MSISDN," + freeBalancesRequestVO.getMsisdn() + ";";
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "generateXMLRequestMessage", e);
			throw new XMLParserException(e.getMessage(), e);
		}
		return requestMessage;
	}

	/**
	 * @param xmlreplyMessage
	 * @return
	 * @throws ConnectionErrorException
	 */
	public FreeBalancesReplyVO parsingXMLReplyMessage(String xmlreplyMessage)
	{

		FreeBalancesReplyVO replyVO = new FreeBalancesReplyVO();

		try
		{
			int respCode = getResponseCode(xmlreplyMessage);

			if (respCode != 0)
				throw new BackEndException(0,respCode+"");

			xmlreplyMessage = xmlreplyMessage.replace(';', ' ').trim();
			StringTokenizer tk = new StringTokenizer(xmlreplyMessage, ":");

			while (tk.hasMoreTokens())
			{
				String token = tk.nextToken();
				
				StringTokenizer subTK = new StringTokenizer(token, ",");
				
				String freeBalAttaributeVal ="";
				String freeBalAttaribute = "";
				if (subTK.countTokens() == 2) {
				 freeBalAttaribute = subTK.nextToken();	
				 freeBalAttaributeVal = subTK.nextToken();
				}

				
				// Get Free Mobily Minutes
				if (freeBalAttaribute.equalsIgnoreCase("FreeMobilyMinutes"))
				{
						try
						{
							int free = Integer.parseInt(freeBalAttaributeVal);
							replyVO.setFreeOnNetMinutes(free);
						}
						catch (NumberFormatException nfe)
						{
						}
				}
				
				// Get Free Minutes
				if (freeBalAttaribute.equalsIgnoreCase("FreeMinutes")){
						try{
							int free = Integer.parseInt(freeBalAttaributeVal);
							if(free != 0)
								replyVO.setFreeMinutes(free);
						}catch (NumberFormatException nfe){}
				}
				/*
				 * As per Naimi's feedback, following up on bug# 23531, we need to map also more two attributes [FreeCrossnetVoiceMinutes and FreeCrossNetVoiceSMS]
				 * */
				// Get Free Free Cross net Voice Minutes
				if (freeBalAttaribute.equalsIgnoreCase("FreeCrossnetVoiceMinutes")){
						try{
							int free = Integer.parseInt(freeBalAttaributeVal);
							if(free != 0)
								replyVO.setFreeMinutes(free);
						}catch (NumberFormatException nfe){}
				}
				
				// Get Free Mobily SMS
				if (freeBalAttaribute.equalsIgnoreCase("FreeMobilySMS"))
				{
						try
						{
							int free = Integer.parseInt(freeBalAttaributeVal);
							replyVO.setFreeOnNetSMS(free);
						}
						catch (NumberFormatException nfe)
						{
						}
				}
				
				// Get Free SMS
				if (freeBalAttaribute.equalsIgnoreCase("FreeSMS")){
						try{
							int free = Integer.parseInt(freeBalAttaributeVal);
							if(free != 0)
								replyVO.setFreeSMS(free);
						}catch (NumberFormatException nfe){}
				}
				/*
				 * As per Naimi's feedback, following up on bug# 23531, we need to map also more two attributes [FreeCrossnetVoiceMinutes and FreeCrossNetVoiceSMS]
				 * */
				// Get Free Cross Net Voice SMS
				if (freeBalAttaribute.equalsIgnoreCase("FreeCrossNetVoiceSMS")){
						try{
							int free = Integer.parseInt(freeBalAttaributeVal);
							if(free != 0)
								replyVO.setFreeSMS(free);
						}catch (NumberFormatException nfe){}
				}
					// Get Free Mobily MMS
					if (freeBalAttaribute.equalsIgnoreCase("FreeMobilyMMS")){
							try{
								int free = Integer.parseInt(freeBalAttaributeVal);
								if(free != 0)
									replyVO.setFreeOnNetMMS(free);
							}catch (NumberFormatException nfe){}
					}
					/*
					 * As per Mohammad Adel feedback [Defect Number 23531], we need to parse FreeVideoMinutes tag because it could have 
					 * free MMS on net
					 * /
					 */
					// Get Free Video Minutes
					if (freeBalAttaribute.equalsIgnoreCase("FreeVideoMinutes")){
							try{								
								int free = Integer.parseInt(freeBalAttaributeVal);
								if(free != 0)
									replyVO.setFreeOnNetMMS(free);
							}catch (NumberFormatException nfe){}
					}
				

				
				// Get Free MMS
				if (freeBalAttaribute.equalsIgnoreCase("FreeMMS"))
				{
						try
						{
							int free = Integer.parseInt(freeBalAttaributeVal);
							replyVO.setFreeMMS(free);
						}
						catch (NumberFormatException nfe)
						{
						}
				}

				// Get Free GPRS
				if (freeBalAttaribute.equalsIgnoreCase("FreeGPRS"))
				{
						try
						{
							replyVO.setFreeGPRS(freeBalAttaributeVal);
						}
						catch (NumberFormatException nfe)
						{
						}
				}
				
				// Get Free Roaming
				if (freeBalAttaribute.equalsIgnoreCase("FreeRoamingGPRS"))
				{
						try
						{
							replyVO.setFreeRoamingGPRS(freeBalAttaributeVal);
						}
						catch (NumberFormatException nfe)
						{
						}
				}
				
				// Get FreeRoamingMinutes
				if (freeBalAttaribute.equalsIgnoreCase("FreeRoamingMinutes")){
						try{
							int free = Integer.parseInt(freeBalAttaributeVal);
							if(free != 0)
								replyVO.setFreeRoamingMinutes(free);
						}catch (NumberFormatException nfe){}
				}
				
				// Get FreeRoamingMTMinutes
				if (freeBalAttaribute.equalsIgnoreCase("FreeRoamingMTMinutes")){
						try{
							int free = Integer.parseInt(freeBalAttaributeVal);
							if(free != 0)
								replyVO.setFreeRoamingMTMinutes(free);
						}catch (NumberFormatException nfe){}
					}
				
				//SR22516 - ePortal Fixed Voice -start
				
				if (freeBalAttaribute.equalsIgnoreCase("FIXEDONNET")){
					try{
						int free = Integer.parseInt(freeBalAttaributeVal);
						if(free != 0)
							replyVO.setFixedOnNet(free);
					}catch (NumberFormatException nfe){}
				}

				if (freeBalAttaribute.equalsIgnoreCase("FIXEDCROSSNET")){
					try{
						int free = Integer.parseInt(freeBalAttaributeVal);
						if(free != 0)
							replyVO.setFixedCrossNet(free);
					}catch (NumberFormatException nfe){}
				}
				
				//SR22516 - ePortal Fixed Voice -end
			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "parsingXMLReplyMessage", e);
			throw new XMLParserException(e.getMessage(), e);
		}

		return replyVO;
	}

	 public MyFreeBalancesReplyVO parseMyFreeBalanceXMLReplyMessage(String xmlreplyMessage)
	  {
	    MyFreeBalancesReplyVO replyVO = new MyFreeBalancesReplyVO();
	    try
	    {
	      int respCode = getResponseCode(xmlreplyMessage);
	      if (respCode != 0) {
	        throw new BackEndException(0, respCode+"");
	      }
	      xmlreplyMessage = xmlreplyMessage.replace(';', ' ').trim();
	      StringTokenizer tk = new StringTokenizer(xmlreplyMessage, ":");
	      while (tk.hasMoreTokens())
	      {
	        String token = tk.nextToken();
	        
	        StringTokenizer subTK = new StringTokenizer(token, ",");
	        

	        String freeBalAttaributeVal = "";
	        String freeBalAttaribute = "";
	        if (subTK.countTokens() == 2)
	        {
	          freeBalAttaribute = subTK.nextToken();
	          freeBalAttaributeVal = subTK.nextToken();
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeMobilyMinutes")) {
	          try
	          {
	            replyVO.setFreeOnNetMinutes(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeMinutes")) {
	          try
	          {
	            replyVO.setFreeMinutes(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException1) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeCrossnetVoiceMinutes")) {
	          try
	          {
	            replyVO.setFreeCrossnetVoiceMinutes(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException2) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeMobilySMS")) {
	          try
	          {
	            replyVO.setFreeOnNetSMS(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException3) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeSMS")) {
	          try
	          {
	            if ("0".equals(FormatterUtility.checkNull(replyVO.getFreeSMS()))) {
	              replyVO.setFreeSMS(validateValue(freeBalAttaributeVal));
	            }
	          }
	          catch (NumberFormatException localNumberFormatException4) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeCrossNetVoiceSMS")) {
	          try
	          {
	            if ("0".equals(FormatterUtility.checkNull(replyVO.getFreeSMS()))) {
	              replyVO.setFreeSMS(validateValue(freeBalAttaributeVal));
	            }
	          }
	          catch (NumberFormatException localNumberFormatException5) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeMobilyMMS")) {
	          try
	          {
	            replyVO.setFreeOnNetMMS(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException6) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeVideoMinutes")) {
	          try
	          {
	            replyVO.setFreeVideoMinutes(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException7) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeMMS")) {
	          try
	          {
	            replyVO.setFreeMMS(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException8) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeGPRS")) {
	          try
	          {
	            replyVO.setFreeGPRS(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException9) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeSocialMedia")) {
	          try
	          {
	            replyVO.setFreeSocialMedia(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException10) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeRoamingMinutes")) {
	          try
	          {
	            replyVO.setFreeRoamingMinutes(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException11) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeRoamingMTMinutes")) {
	          try
	          {
	            replyVO.setFreeRoamingMTMinutes(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException12) {}
	        }
	        if (freeBalAttaribute.equalsIgnoreCase("FreeRoamingGPRS")) {
	          try
	          {
	            replyVO.setFreeRoamingGPRS(validateValue(freeBalAttaributeVal));
	          }
	          catch (NumberFormatException localNumberFormatException13) {}
	        }
	        
			//SR22516 - ePortal Fixed Voice -start
			
			if (freeBalAttaribute.equalsIgnoreCase("FIXEDONNET")){
				try
		          {
		            replyVO.setFixedOnNet(validateValue(freeBalAttaributeVal));
		          }
		          catch (NumberFormatException localNumberFormatException13) {}
			}

			if (freeBalAttaribute.equalsIgnoreCase("FIXEDCROSSNET")){
				try
		          {
		            replyVO.setFixedCrossNet(validateValue(freeBalAttaributeVal));
		          }
		          catch (NumberFormatException localNumberFormatException13) {}
			}
			
			//SR22516 - ePortal Fixed Voice -end
	        
	      }
	    }
	    catch (Exception e)
	    {
	      executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), clazz, "parsingXMLReplyMessage", e);
	      throw new XMLParserException(e.getMessage(), e);
	    }
	    return replyVO;
	  }
	 

	/**
	 * @MethodName: getResponseCode
	 * <p>
	 * Extracts Response code from reply message
	 * 
	 * @param String resp
	 * 
	 * @return Int responseCode
	 * 
	 * @author Abu Sharaf
	 */
	private int getResponseCode(String resp) throws MobilyCommonException
	{

		if (resp == null || resp.length() == 0)
			throw new MobilyCommonException();

		int res = -1;
		try
		{

			StringTokenizer tk = new StringTokenizer(resp, ":");
			if (tk.countTokens() == 2)
			{
				tk.nextToken();
				String temp = tk.nextToken();

				String rest = temp.substring(0, temp.indexOf(";"));

				res = Integer.parseInt(rest);
			}
			else if (tk.countTokens() > 2)
			{

				tk.nextToken();
				String temp = tk.nextToken();
				res = Integer.parseInt(temp);
			}
		}
		catch (Exception e)
		{

			res = -1;
		}
		return res;
	}

	 private static String validateValue(String value)
	  {
	    value = FormatterUtility.checkNull(value).trim();
	    if (!"".equals(value))
	    {
	      if (("Unlimited".equalsIgnoreCase(value)) || (StringUtils.isNumeric(value))) {
	        return value;
	      }
	      return "0";
	    }
	    return "0";
	  }
}
