package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.sql.Timestamp;
import java.util.Map;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class SessionVO extends BaseVO{

	 
	private static final long serialVersionUID = 74842632838L;

	private long userAcctId;

	private String iqama;// iqama or national ID

	private String userName;

	private String customerType;

	private String packageId;

	private int subscriptionType;

	private String msisdn;

	private String serviceAccountNumber;
	
	private long subscriptionID;
	
	private Timestamp lastLoginTime;
	
    private String activeMSISDN;
    
    private int    lineType;
   
    private String rootBillingAccountNumber = null; // Root or department billing account number of corporate
    
   // Adding <Corporate> personalization attributes based on sub-accounts' subscriptions
    
//    // flag to identify if the Corporate account has sub-Accounts subscribed to DIA
//    boolean hasDiaSubscribers;
//    
//    // flag to identify if the Corporate account has sub-Accounts subscribed to Ethernet
//    boolean hasEthernetSubscribers;
//    
//    // flag to identify if the Corporate account has sub-Accounts subscribed to IPVPN
//    boolean hasIpVpnSubscribers;
    
    boolean hasIceCubeEligibility;
    
    private String vipTier;
    
    // Added for Connect
    private String contactNumber;
    private String packageType;
    
    // Added for Neqaty Partner
    private String partnerEmail;
    private String partnerNameEn;
    private String partnerNameAr;
    private String partnerActivationStatus;
    
    private int    roleId;
    
	public String getContactNumber()
	{
		return contactNumber;
	}

	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}

	public String getPackageType()
	{
		return packageType;
	}

	public void setPackageType(String packageType)
	{
		this.packageType = packageType;
	}

	public boolean isHasIceCubeEligibility()
	{
		return hasIceCubeEligibility;
	}

	public void setHasIceCubeEligibility(boolean hasIceCubeEligibility)
	{
		this.hasIceCubeEligibility = hasIceCubeEligibility;
	}

	public Timestamp getLastLoginTime()
	{
		return lastLoginTime;
	}
	
	public void setLastLoginTime(Timestamp lastLoginTime)
	{
		this.lastLoginTime = lastLoginTime;
	}

	public long getSubscriptionID()
	{
		return subscriptionID;
	}

	public void setSubscriptionID(long subscriptionID)
	{
		this.subscriptionID = subscriptionID;
	}

	public int getLineType() {
		return lineType;
	}

	public void setLineType(int lineType) {
		this.lineType = lineType;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getServiceAccountNumber() {
		return serviceAccountNumber;
	}

	public void setServiceAccountNumber(String serviceAccountNumber) {
		this.serviceAccountNumber = serviceAccountNumber;
	}

	private Map<String, SubscriptionVO> otherSubscriptions;

	public long getUserAcctId() {
		return userAcctId;
	}

	public void setUserAcctId(long userAcctId) {
		this.userAcctId = userAcctId;
	}

	public String getIqama() {
		return iqama;
	}

	public void setIqama(String iqama) {
		this.iqama = iqama;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public int getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(int subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public Map<String, SubscriptionVO> getOtherSubscriptions() {
		return otherSubscriptions;
	}

	public void setOtherSubscriptions(
			Map<String, SubscriptionVO> otherSubscriptions) {
		this.otherSubscriptions = otherSubscriptions;
	}

	 

	public String getActiveMSISDN()
	{
		return activeMSISDN;
	}

	public void setActiveMSISDN(String activeMSISDN)
	{
		this.activeMSISDN = activeMSISDN;
	}

	 
	 

	 
//	public boolean isHasDiaSubscribers()
//	{
//		return hasDiaSubscribers;
//	}
//
//	public void setHasDiaSubscribers(boolean hasDiaSubscribers)
//	{
//		this.hasDiaSubscribers = hasDiaSubscribers;
//	}
//
//	public boolean isHasEthernetSubscribers()
//	{
//		return hasEthernetSubscribers;
//	}
//
//	public void setHasEthernetSubscribers(boolean hasEthernetSubscribers)
//	{
//		this.hasEthernetSubscribers = hasEthernetSubscribers;
//	}
//
//	public boolean isHasIpVpnSubscribers()
//	{
//		return hasIpVpnSubscribers;
//	}
//
//	public void setHasIpVpnSubscribers(boolean hasIpVpnSubscribers)
//	{
//		this.hasIpVpnSubscribers = hasIpVpnSubscribers;
//	}

	public int getRoleId()
	{
		return roleId;
	}

	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	public String getRootBillingAccountNumber()
	{
		return rootBillingAccountNumber;
	}

	public void setRootBillingAccountNumber(String rootBillingAccountNumber)
	{
		this.rootBillingAccountNumber = rootBillingAccountNumber;
	}

	public String getVipTier()
	{
		return vipTier;
	}

	public void setVipTier(String vipTier)
	{
		this.vipTier = vipTier;
	}

	public String getPartnerEmail()
	{
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail)
	{
		this.partnerEmail = partnerEmail;
	}

	public String getPartnerNameEn()
	{
		return partnerNameEn;
	}

	public void setPartnerNameEn(String partnerNameEn)
	{
		this.partnerNameEn = partnerNameEn;
	}

	public String getPartnerNameAr()
	{
		return partnerNameAr;
	}

	public void setPartnerNameAr(String partnerNameAr)
	{
		this.partnerNameAr = partnerNameAr;
	}

	public String getPartnerActivationStatus()
	{
		return partnerActivationStatus;
	}

	public void setPartnerActivationStatus(String partnerActivationStatus)
	{
		this.partnerActivationStatus = partnerActivationStatus;
	}

	
	
}