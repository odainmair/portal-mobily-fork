/*
 * Created on Jun 16, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleLookupTableDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.LOVInquiryVO;
import sa.com.mobily.eportal.common.service.vo.LookupTableVO;
import sa.com.mobily.eportal.common.service.xml.CustomerProfileXMLHandler;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LookupTableUtility {

	private static final Logger log = LoggerInterface.log;
    private static Hashtable lookupHashTable  = null; 
    private static Hashtable cityLookupHashTable  = null;
    private static Hashtable cityLookupHashTableByName  = null;
    private static Hashtable countryLookupHashTableByName  = null;
    
    private static List countryLOVList  = null; // Added for MDM Migration
    private static List partyCategoryLOVList  = null; // Added for MDM Migration
    
	private static LookupTableUtility  utility = null;
	private LookupTableUtility(){
	   	
	}
	
	public static LookupTableUtility getInstance(){
		if(utility == null)
			utility = new LookupTableUtility();
	   
		return utility;
	}
	
	public ArrayList getLookupDataById(String ItemId) {
		ArrayList listOfLookup  = null;
		if(lookupHashTable == null) {
			log.debug("LookupTableUtility > getLookupDataById > load Data from DB");
			
	     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	     	OracleLookupTableDAO lookupTableDAO = (OracleLookupTableDAO)daoFactory.getLookupTableDAO();
			lookupHashTable =  lookupTableDAO.loadLookupFromDB();
		}
		if(lookupHashTable.containsKey(ItemId)) {
			listOfLookup = (ArrayList)lookupHashTable.get(ItemId);
		}
		return listOfLookup;
	}
	public LookupTableVO getLookupDataByIdAndSubId(String ItemId,String SubId) {
		LookupTableVO lookupTableVO  = null;
			
	     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	     	OracleLookupTableDAO lookupTableDAO = (OracleLookupTableDAO)daoFactory.getLookupTableDAO();
	     	lookupTableVO =  lookupTableDAO.getLookupByIdAndSubId(ItemId, SubId);
	     	return lookupTableVO;
	}
	public ArrayList getCityLookupDataById(String ItemId) {
		ArrayList listOfLookup  = null;
		if(cityLookupHashTable == null) {
			log.debug("LookupTableUtility > getCityLookupDataById > load Data from DB");
	     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	     	OracleLookupTableDAO lookupTableDAO = (OracleLookupTableDAO)daoFactory.getLookupTableDAO();
	     	cityLookupHashTable =  lookupTableDAO.loadCityLookupFromDB();
		}
		if(cityLookupHashTable.containsKey(ItemId)) {
			log.debug("LookupTableUtility > getCityLookupDataById > Get Item List from Lookup Table Hash");
			listOfLookup = (ArrayList)cityLookupHashTable.get(ItemId);
		}
		return listOfLookup;
	}


	public ArrayList getCityLookupDataByIdAndField(String ItemId, String columnName) {
		ArrayList listOfLookup  = null;
		if(cityLookupHashTableByName == null) {
			log.debug("LookupTableUtility > getCityLookupDataByIdAndField > load Data from DB");
			
	     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	     	OracleLookupTableDAO lookupTableDAO = (OracleLookupTableDAO)daoFactory.getLookupTableDAO();
	     	cityLookupHashTableByName =  lookupTableDAO.loadCityLookupFromDBByColumn(columnName);
		}
		if(cityLookupHashTableByName.containsKey(ItemId)) {
			log.debug("LookupTableUtility > getCityLookupDataByIdAndField > Get Item List from Lookup Table Hash");
			listOfLookup = (ArrayList)cityLookupHashTableByName.get(ItemId);
		}
		return listOfLookup;
	}

	public ArrayList getCountryLookupDataByIdAndField(String ItemId, String columnName) {
		ArrayList listOfLookup  = null;
		if(countryLookupHashTableByName == null) {
			log.debug("LookupTableUtility > getCountryLookupDataByIdAndField > load Data from DB");
			
	     	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
	     	OracleLookupTableDAO lookupTableDAO = (OracleLookupTableDAO)daoFactory.getLookupTableDAO();
	     	countryLookupHashTableByName =  lookupTableDAO.loadCountriesByColumn(columnName);
		}
		if(countryLookupHashTableByName.containsKey(ItemId)) {
			log.debug("LookupTableUtility > getCountryLookupDataByIdAndField > Get Item List from Lookup Table Hash");
			listOfLookup = (ArrayList)countryLookupHashTableByName.get(ItemId);
		}
		return listOfLookup;
	}	
	
	public List getCountryLOVLookupData(String lovType) {
		//countryLOVList = null;// for testing
		if(countryLOVList == null) {
			log.debug("LookupTableUtility > getCountryLOVLookupData > load Data from MQ");
			countryLOVList = getLOVLookupData(lovType);
		}
		
		return countryLOVList;
	}
	
	public List getPartyCatLOVLookupData(String lovType) {
		
		//partyCategoryLOVList = null;// for testing
		if(partyCategoryLOVList == null) {
			log.debug("LookupTableUtility > getPartyCatLOVLookupData > load Data from MQ");
			partyCategoryLOVList = getLOVLookupData(lovType);
		}
		
		return partyCategoryLOVList;
	}
	
	public List getLOVLookupData(String lovType) {
		log.debug("LookupTableUtility > getLOVLookupData > called");
		List lovList = null;
		StringBuffer xmlRequest = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        String xmlReply = "";
        
        String request = new CustomerProfileXMLHandler().generateLOVXMLRequest(lovType);
    	if(!FormatterUtility.isEmpty(request))
    		xmlRequest.append(request);        	
    	log.debug("LookupTableUtility > getLOVLookupData > xmlRequest ::"+xmlRequest);
		
		MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
     	CustomerProfileDAO customerProfileDAO = (MQCustomerProfileDAO)daoFactory.getMQCustomerProfileDAO();
		try {
			String requestQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MDM_LOV_INQUIRY_REQUEST_QUEUENAME);
			String replyQueueName   = MobilyUtility.getPropertyValue(ConstantIfc.MDM_LOV_INQUIRY_REPLY_QUEUENAME);
			xmlReply = customerProfileDAO.sendToMQWithReply(xmlRequest.toString(),requestQueueName,replyQueueName);
			
			LOVInquiryVO lovInquiryVO =  new CustomerProfileXMLHandler().parseLOVXMLReply(xmlReply);
			if(lovInquiryVO != null) {
				log.debug("LookupTableUtility > getLOVLookupData > Status Code::"+lovInquiryVO.getStatusCode());
				log.debug("LookupTableUtility > getLOVLookupData > Total Records Count::"+lovInquiryVO.getTotalRecordsCount());
				if("I000000".equalsIgnoreCase(lovInquiryVO.getStatusCode())) {
					lovList = lovInquiryVO.getLovInfoVOList();
				} 
			}
			
		} catch (MobilyCommonException e) {
			log.fatal("LookupTableUtility > getLOVLookupData > MobilyCommonException while doing inquiry with MDM::"+e.getMessage());
		} catch (Exception e) {
			log.fatal("LookupTableUtility > getLOVLookupData > Exception while doing inquiry with MDM::"+e.getMessage());
		}	
		
		return lovList;
	}
	
	public void clearCachedItems() {
		log.debug("LookupTableUtility > clearCachedItems > called");
		lookupHashTable = null;
		cityLookupHashTable = null;
		cityLookupHashTableByName  = null;
		countryLookupHashTableByName = null;
		
		countryLOVList = null;
		partyCategoryLOVList = null;
	}



	public static void main(String[] args) {
	}
}
