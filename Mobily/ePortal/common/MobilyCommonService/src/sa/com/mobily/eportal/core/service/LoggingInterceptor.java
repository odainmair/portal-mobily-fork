package sa.com.mobily.eportal.core.service;

import java.text.MessageFormat;
import java.util.Date;

import javax.interceptor.InvocationContext;

import sa.com.mobily.eportal.common.interceptor.ServiceInterceptor;
import sa.com.mobily.eportal.common.service.constant.FootPrintChannels;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class LoggingInterceptor {
	
	private static final String FOOT_PRINT_XML_MESSAGE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
										"<FOOTPRINT>" +
											"<USER_ID>{0}</USER_ID>" +
											"<EVENT_NAME>{1}</EVENT_NAME>" +
											"<KEY>{2}</KEY>" +
											"<SUB_KEY>{3}</SUB_KEY>" +
											"<COMMENTS>{4}</COMMENTS>" +
											"<REQUEST_TIME_STAMP>{5}</REQUEST_TIME_STAMP>" +
											"<SYSTEM_ERROR_MESSAGE>{6}</SYSTEM_ERROR_MESSAGE>" +
											"<USER_ERROR_MESSAGE>{7}</USER_ERROR_MESSAGE>" +
											"<ERROR_CODE>{8}</ERROR_CODE>" +
											"<REQUEST_CHANNEL_ID>{9}</REQUEST_CHANNEL_ID>" +
											"<SERVICE_ACCOUNT>{10}</SERVICE_ACCOUNT>" +
										"</FOOTPRINT>";

	private static final String FOOTPRINT_STATUS_PROPERTY = "FOOT_PRINT_STATUS";

	private static final String FOOTPRINT_STATUS_ON = "ON";
	
	private	Logger logger = null;
	
	public Object wrap(InvocationContext ctx) throws Exception {
		String clazz = null;
		String method = null;
		Exception ex = null;
		boolean exceptionOccured = false;
		try {
//			clazz = ctx.getMethod().getDeclaringClass().getName();
			clazz = ctx.getTarget().getClass().getName();
			method = ctx.getMethod().getName();
			logger = Logger.getLogger(clazz);
			logger.entering(method);
			Object object = ctx.proceed();
			return object;
		} catch (Exception  t) {
			exceptionOccured = true;
			t.printStackTrace();
			ex = t;
			throw t;//new MobilySystemException(t.getMessage(), t);
		} finally {
			footPrint(ctx, ex);
			logger.exiting(method);
			logger.flush(clazz, exceptionOccured);		
		}
	}
	
	private void footPrint(InvocationContext ctx, Exception e) {
		String status = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(FOOTPRINT_STATUS_PROPERTY);
		if (FOOTPRINT_STATUS_ON.equalsIgnoreCase(status) && ctx.getParameters() != null && ctx.getParameters().length > 1) {
			try {
					
				Object[] params = ctx.getParameters();
				String method = ctx.getMethod().getName();
				String comment = "";
				Context c = null;
				Object obj = ctx.getParameters()[0];
				if (obj != null && obj instanceof Context) {
					c = (Context) obj;
				}
				obj = ctx.getParameters()[1];
				if (obj != null) {
					
					if (params.length >= 1) {
						comment = params[1].toString();
					}
				}
				String sysErr = "";
				String usrErr = "";
				String errCode = "0";
				if (e != null) {
					sysErr = e.getMessage();
					usrErr = e.getMessage();
					if (e instanceof ServiceException) {
						errCode = "" + ((ServiceException) e).getErrorCode();
					} else {
						errCode = "-1";
					}
				}
				
				String userId = "";
				String msisdn = "";
				String san = "";
				if(c != null && c.getCurrentUser() != null){
					userId = c.getCurrentUser().getUserId();
					msisdn = c.getCurrentUser().getDefaultMSISDN();
					san = c.getCurrentUser().getServiceAccountNumber();
				}
				String xml = MessageFormat.format(FOOT_PRINT_XML_MESSAGE, 
						userId,
						comment,
						msisdn, 
						"", 
						method,
						EPortalUtils.formatDate(new Date(), EPortalConstants.SIEBEL_DATE_FORMAT),
						sysErr, 
						usrErr, 
						errCode,
						FootPrintChannels.WEB.getChannel(),
						san);
				System.out.println("FOOT_PRINT_XML ... " + xml);
//				ServiceInterceptor.sendFootPrint(xml);
				MQConnectionHandler.getInstance().sendToMQ(JAXBUtilities.getInstance().marshal(xml), "EPORTAL.FOOTPRINTING");

			} catch (Exception exp) {
				logger.error(exp.getMessage(), "footPrint", exp);
			}
		}

	}
}
