/*
 * Created on Jun 1, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author Suresh V - MIT
 * AuthenticateAndGoVO
 * Description: Requeast VO for Authenticate And Go application
 */
public class AuthenticateAndGoVO {

    private String moduleId = null; // id value of the modules like Rahati, SMS 250 Bundle etc
    private String mobilyNumber = null; // MSISDN
    private String activationCode = null; // Pin code
    
    private String serviceRequestId = null;// unique Id
    private String actionId = null; // id value of the Action like "Customer Enter his MSISDN number", "Customer Enter ACtivation_key" etc.. 
    private String serviceName = null; // Name of the Service ex: Rahati
    private String serviceDescription = null; // Short desctiprion of the Service.
    private int activationCodeStatus = 1; // id value of the modules like Rahati, SMS 250 Bundle etc
    
    public String getActivationCode() {
        return activationCode;
    }
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }
    public String getMobilyNumber() {
        return mobilyNumber;
    }
    public void setMobilyNumber(String mobilyNumber) {
        this.mobilyNumber = mobilyNumber;
    }
    public String getModuleId() {
        return moduleId;
    }
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }
    public String getActionId() {
        return actionId;
    }
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }
    public String getServiceDescription() {
        return serviceDescription;
    }
    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }
    public String getServiceName() {
        return serviceName;
    }
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    public String getServiceRequestId() {
        return serviceRequestId;
    }
    public void setServiceRequestId(String serviceRequestId) {
        this.serviceRequestId = serviceRequestId;
    }
	/**
	 * @return the activationCodeStatus
	 */
	public int getActivationCodeStatus() {
		return activationCodeStatus;
	}
	/**
	 * @param activationCodeStatus the activationCodeStatus to set
	 */
	public void setActivationCodeStatus(int activationCodeStatus) {
		this.activationCodeStatus = activationCodeStatus;
	}
}