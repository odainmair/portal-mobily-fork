package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CreditTransferDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

/**
 * @author a.abugharbieh
 *
 * This class is used to 
 */
public class OracleCreditTransferDAO  implements CreditTransferDAO{
	private static final Logger log = LoggerInterface.log;

	public ArrayList<String> getBlackListPackageList() {
		
		log.info("OracleCreditTransferDAO > getBlackListPackageList > started");
 	    Connection connection = null;
	    PreparedStatement statement = null;
	    ResultSet results = null;

		ArrayList<String> itemsList = new ArrayList<String>();
		
		try {
			String sqlQuery = "SELECT PACKAGE_ID FROM SR_CREDIT_TRANSFER_PACKAGE_BL";
					
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			statement  = connection.prepareStatement(sqlQuery.toString());
			results = statement.executeQuery();				
					
		      while(results.next()) { 
		      	  itemsList.add(results.getString(1));
		      }
		      
		      log.debug("itemsList.size() = " +itemsList.size());

		}
		catch(Exception e){
			throw new SystemException(e);
		}
		finally {
			JDBCUtility.closeJDBCResoucrs(connection , statement , results);
		}
		log.info("OracleCreditTransferDAO > getBlackListPackageList > ended");
		return itemsList;
	}
	
	public boolean isShowIdField(String msisdn, String packageId) {

		log.info("OracleCreditTransferDAO > isShowIdField : start");
		Connection connection = null;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;
		boolean isShowIdField = true;
		
		log.debug("OracleCreditTransferDAO > isShowIdField > msisdn = " + msisdn);
		log.debug("OracleCreditTransferDAO > isShowIdField > packageId = " + packageId);
		
		try{
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			callableStatement = connection.prepareCall("CALL CREDIT_TRANSFER_NEED_ID_FIELD(?,?,?)"); 
			callableStatement.setString (1, msisdn);
			callableStatement.setString (2, packageId);
			callableStatement.registerOutParameter (3, Types.VARCHAR);
			
			callableStatement.execute();
			
			String isShowIdFieldStr = callableStatement.getString(3);
			
			log.debug("OracleCreditTransferDAO > isShowIdField > isShowIdFieldStr = " + isShowIdFieldStr);
			
			if(isShowIdFieldStr.equalsIgnoreCase("NO")){
				isShowIdField = false;
			}
			
		}catch (SQLException e) {
			log.fatal("OracleCreditTransferDAO > isShowIdField : SQLException : "+e);
		}finally {
			JDBCUtility.closeJDBCResoucrs(connection, callableStatement, resultSet);
		}
		
		return isShowIdField;
	}
}
