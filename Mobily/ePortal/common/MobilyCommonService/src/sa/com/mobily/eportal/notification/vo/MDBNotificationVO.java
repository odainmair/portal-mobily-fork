package sa.com.mobily.eportal.notification.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * 
 * @author r.agarwal.mit
 *
 */
public class MDBNotificationVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private String queue;
	private String message;
	private int serviceId;
	private String transactionId;
	
	public String getQueue()
	{
		return queue;
	}
	public void setQueue(String queue)
	{
		this.queue = queue;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public int getServiceId()
	{
		return serviceId;
	}
	public void setServiceId(int serviceId)
	{
		this.serviceId = serviceId;
	}
	public String getTransactionId()
	{
		return transactionId;
	}
	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}
}
