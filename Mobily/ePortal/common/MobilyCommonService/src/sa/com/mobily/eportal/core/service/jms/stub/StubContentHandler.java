package sa.com.mobily.eportal.core.service.jms.stub;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import sa.com.mobily.eportal.core.service.Logger;

class StubContentHandler implements ContentHandler {

    private static final Logger log = Logger.getLogger(StubContentHandler.class);
    private static final String TAG_QUEUE_STUB = "QueueStub";
    private static final String TAG_QUEUE_NAME = "QueueName";
    private static final String TAG_RESPONSE = "Response";
    private static final String TAG_QUEUE_REQUEST_FILTER = "RequestFilter";

    private StringBuffer elementContent;
    private QueueStub currentQueueStub;

    private String stubFileName;
    private Map<String, List<QueueStub>> queueStubs;

    public StubContentHandler(String stubFileName, Map<String, List<QueueStub>> queueStubs) {
	this.stubFileName = stubFileName;
	this.queueStubs = queueStubs;
    }

    @Override
    public void characters(char[] ch, int start, int length)
	    throws SAXException {
	if(elementContent != null) {
	    elementContent.append(new String(ch, start, length));
	}
    }

    @Override
    public void startElement(String uri, String localName, String qName,
	    Attributes atts) throws SAXException {
	
	if (localName.equals(TAG_QUEUE_STUB)) {
	    currentQueueStub = new QueueStub();
	    currentQueueStub.setStubFileName(stubFileName);
	} else if (localName.equals(TAG_QUEUE_NAME) || localName.equals(TAG_QUEUE_REQUEST_FILTER)
		|| localName.equals(TAG_RESPONSE)) {
	    elementContent = new StringBuffer();
	}
	
    }

    @Override
    public void endElement(String uri, String localName, String qName)
	    throws SAXException {

	if (localName.equals(TAG_QUEUE_STUB)) {
	    List<QueueStub> lstStubs = queueStubs.get(currentQueueStub
		    .getQueueName());
	    if (lstStubs != null) {
		lstStubs.add(currentQueueStub);
	    } else {
		lstStubs = new Vector<QueueStub>();
		lstStubs.add(currentQueueStub);
		queueStubs.put(currentQueueStub.getQueueName(), lstStubs);
	    }

	    log.info("Queue Stub Added: " + currentQueueStub);

	    currentQueueStub = null;
	} else if (localName.equals(TAG_QUEUE_NAME)) {
	    currentQueueStub.setQueueName(elementContent.toString());
	} else if (localName.equals(TAG_RESPONSE)) {
	    currentQueueStub.setResponse(elementContent.toString());
	} else if (localName.equals(TAG_QUEUE_REQUEST_FILTER)) {
	    if(currentQueueStub.getRequestFilters() == null) {
		currentQueueStub.setRequestFilters(new Vector<String>());
	    }
	    currentQueueStub.getRequestFilters().add(elementContent.toString());
	}

    }

    @Override
    public void endPrefixMapping(String prefix) throws SAXException {
	// TODO Auto-generated method stub

    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length)
	    throws SAXException {
	// TODO Auto-generated method stub

    }

    @Override
    public void processingInstruction(String target, String data)
	    throws SAXException {
	// TODO Auto-generated method stub

    }

    @Override
    public void setDocumentLocator(Locator locator) {
	// TODO Auto-generated method stub

    }

    @Override
    public void skippedEntity(String name) throws SAXException {
	// TODO Auto-generated method stub

    }

    @Override
    public void startDocument() throws SAXException {
	// TODO Auto-generated method stub

    }

    @Override
    public void endDocument() throws SAXException {
	// TODO Auto-generated method stub

    }

    @Override
    public void startPrefixMapping(String prefix, String uri)
	    throws SAXException {
	// TODO Auto-generated method stub

    }

}
