package sa.com.mobily.eportal.customerinfo.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.CustomerServiceUtil;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.util.AppConfig;
import sa.com.mobily.eportal.customerinfo.util.ConsumerProfileUtil;
import sa.com.mobily.eportal.customerinfo.util.UserProfileUtil;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileRequestVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerRelatedNumbersReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.CorporateProfileReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.CustomerInfoVO;
import sa.com.mobily.eportal.customerinfo.xml.ConsumerProfileXMLHandler;
import sa.com.mobily.eportal.customerinfo.xml.CorporateProfileXMLHandler;
import sa.com.mobily.eportal.customerinfo.xml.CustomerInfoWithContactXMLHandler;
import sa.com.mobily.eportal.customerinfo.xml.CustomerInfoXMLHandler;

/**
 * This class is responsible for communicating with the back end to retrieve
 * customer information.
 * <p>
 * it contains methods for retrieve Customer Profile, Customer Profile, Related
 * Numbers and Corporate Profile.
 * 
 * @author Yousef Alkhalaileh
 */
public final class CustomerInfoJMSRepository
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);

	private static final String className = CustomerInfoJMSRepository.class.getName();

	/**
	 * This method retrieves customer profile by MSISDN
	 * 
	 * @param <code>String</code> msisdn for the customer
	 * @return <code>CustomerInfoVO</code> customer profile
	 * @author Yousef Alkhalaileh
	 */
	public CustomerInfoVO getCustProductProfileByMsisdn(String msisdn)
	{
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getCustProductProfileByMsisdn > msisdn::" + msisdn, className, "getCustProductProfileByMsisdn");		
		/*CustomerInfoVO customerInfoVO = new CustomerInfoVO();
		customerInfoVO.setSourceId(msisdn);
		customerInfoVO.setSourceSpecName(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN);
		customerInfoVO.setPreferredLanguage("En");
		return getCustomerProfile(customerInfoVO);*/
		
		UserProfileUtil userProfileUtil = new UserProfileUtil();
		return userProfileUtil.getUserProfile(msisdn);
	}
	
	
	
	

	/**
	 * This method retrieves customer profile by service Account Number
	 * 
	 * @param <code>String</code> service Account Number for the customer
	 * @return <code>CustomerInfoVO</code> customer profile
	 * @author Yousef Alkhalaileh
	 */
	public CustomerInfoVO getCustProductProfileByServAcctNumber(String serviceAcctNumber)
	{
		CustomerInfoVO customerInfoVO = new CustomerInfoVO();
		customerInfoVO.setSourceId(serviceAcctNumber);
		customerInfoVO.setSourceSpecName(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_BILLING_ACCOUNT_NUMBER);
		customerInfoVO.setPreferredLanguage("En");
		return getCustomerProfile(customerInfoVO);
	}

	/**
	 * This method retrieves customer profile by cpe
	 * 
	 * @param <code>String</code> cpe of the customer
	 * @return <code>CustomerInfoVO</code> customer profile
	 * @author Yousef Alkhalaileh
	 */
	public CustomerInfoVO getCustProductProfileByCPE(String cpe)
	{
		CustomerInfoVO customerInfoVO = new CustomerInfoVO();
		customerInfoVO.setSourceId(cpe);
		customerInfoVO.setSourceSpecName(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_CPE);
		customerInfoVO.setFuncId(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY);
		customerInfoVO.setPreferredLanguage("En");
		return getCustomerProfile(customerInfoVO);
	}

	/**
	 * This method retrieves customer profile
	 * 
	 * @param <code>CustomerInfoVO</code> customer information
	 * @return <code>CustomerInfoVO</code> customer profile
	 * @author Yousef Alkhalaileh
	 */
	public CustomerInfoVO getCustomerProfile(CustomerInfoVO customerInfoVO)
	{
		String method = "getCustomerProfile";
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustomerProfile : start for account: " + customerInfoVO.getSourceId(), className, method);

		if(customerInfoVO != null && customerInfoVO.getSourceId() != null && CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN.equals(customerInfoVO.getSourceSpecName()) && customerInfoVO.getSourceId().length() < 12)
		{
			customerInfoVO.setSourceId(FormatterUtility.getUniversalFormat(customerInfoVO.getSourceId()));
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustomerProfile : Account after universal format: " + customerInfoVO.getSourceId(), className, method);
		}
		MQAuditVO mqAuditVO = new MQAuditVO();
		//Changes to restrict customer inquiry for null MSISDN
		if(FormatterUtility.isNotEmpty(customerInfoVO.getSourceId())){
			if(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN.equals(customerInfoVO.getSourceSpecName())||CustomerInfoVO.SOURCE_SPECIFICATION_NAME_BILLING_ACCOUNT_NUMBER.equals(customerInfoVO.getSourceSpecName())){
				if((customerInfoVO.getSourceId().length()<10)||(!FormatterUtility.isNumeric(customerInfoVO.getSourceId()))){
					{
						executionContext.audit(Level.SEVERE, "Source Id is not in correct format....!", className, method);
						throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
					}
				}
			}
			String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_CUSTOMER_PROFILE);
			String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_CUSTOMER_PROFILE);
			// TODO check caching here
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustomerProfile : requestQueueName[" + requestQueueName + "]", className, method);
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustomerProfile : replyQueueName[" + replyQueueName + "]", className, method);
	
			String strXMLRequest = new CustomerInfoXMLHandler().generateCustomerInfoXML(customerInfoVO);
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustomerProfile : strXMLRequest: " + strXMLRequest, className, method);
	
			mqAuditVO.setRequestQueue(requestQueueName);
			mqAuditVO.setReplyQueue(replyQueueName);
			mqAuditVO.setUserName(customerInfoVO.getUsrId());
			if(FormatterUtility.isNotEmpty(customerInfoVO.getMSISDN())){
				mqAuditVO.setMsisdn(customerInfoVO.getMSISDN());
			}else{
				mqAuditVO.setMsisdn(customerInfoVO.getAccountNumber());
			}
			if(customerInfoVO.getFuncId()==-1){
				mqAuditVO.setFunctionName(String.valueOf(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION));
			}
			else if (customerInfoVO.getFuncId() != ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY){
				mqAuditVO.setFunctionName(String.valueOf(customerInfoVO.getFuncId()));
			}else{
				mqAuditVO.setFunctionName(String.valueOf(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY));
			}
			mqAuditVO.setMessage(strXMLRequest);
			
			try {
				String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
				executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustomerProfile : strXMLReply: " + strXMLReply, className, method);
				mqAuditVO.setReply(strXMLReply);
				if (!FormatterUtility.isEmpty(strXMLReply))
				{
					customerInfoVO = new CustomerInfoXMLHandler().parseCustomerInfoXML(strXMLReply);
		
					if (null != customerInfoVO.getStatusCode())
					{
						mqAuditVO.setErrorMessage(customerInfoVO.getStatusCode());
						mqAuditVO.setServiceError(customerInfoVO.getStatusCode());
						
						if (ConstantsIfc.STATUS_CODE_CUSTOMER_NOT_FOUND.equalsIgnoreCase(customerInfoVO.getStatusCode())
								|| ConstantsIfc.STATUS_CODE_CUSTOMER_NOT_SUBSCRIBED.equalsIgnoreCase(customerInfoVO.getStatusCode()))
						{
							
							throw new DataNotFoundException("Either customer is not found or is not Subscribed.");
						}
						// handling case there is no body in case of error code
						// (E500004,E020061,E200021,E500024,E500025,E020043,E810999,E510999,E226001,E950014,E950015,E999998,E999999)
						if (customerInfoVO.getStatusCode().indexOf("E") != -1)
							throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, customerInfoVO.getStatusCode());
					}
		
				} else {
					executionContext.log(Level.SEVERE, "CustomerInfoServiceImpl > getCustomerProfile : Empty reply from back end", className, method, null);
					mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
					mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
					
					throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				}
			} catch(ServiceException e) {
				executionContext.log(Level.SEVERE, "ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
				throw e;
			} catch(DataNotFoundException e) {
				throw e;
			} catch(BackEndException e) {
				throw e;
			} catch(Exception e) {
				executionContext.log(Level.SEVERE, "Exception in MQ Call::" + e.getMessage(), className, method, e);
				mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
				mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				
				throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			} finally {
				MQUtility.saveMQAudit(mqAuditVO);
			}
		} else {
			executionContext.audit(Level.SEVERE, "Source Id is EMPTY....!", className, method);
			/*mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			MQUtility.saveMQAudit(mqAuditVO);*/
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
		}
		
		return customerInfoVO;
	}

	/**
	 * This method retrieves Consumer profile by MSISDN
	 * 
	 * @param <code>String</code> msisdn for the customer
	 * @return <code>ConsumerProfileReplyVO</code> Consumer profile
	 * @author Yousef Alkhalaileh
	 */
	public ConsumerProfileReplyVO getConsumerProfileByMsisdn(String msisdn)
	{
		/*ConsumerProfileRequestVO requestVO = new ConsumerProfileRequestVO();
		requestVO.setKeyType(ConstantsIfc.KEY_TYPE_MSISDN);
		requestVO.setKeyValue(msisdn);
		return getConsumerProfile(requestVO);*/
		//JK-38561 
		ConsumerProfileUtil consumerProfileUtil = new ConsumerProfileUtil();
		return consumerProfileUtil.getConsumerProfile(msisdn);
	}

	/**
	 * This method retrieves Consumer profile by acctNumber
	 * 
	 * @param <code>String</code> acctNumber for the customer
	 * @return <code>ConsumerProfileReplyVO</code> Consumer profile
	 * @author Yousef Alkhalaileh
	 */
	public ConsumerProfileReplyVO getConsumerProfileByAcctNumber(String acctNumber)
	{
		ConsumerProfileRequestVO requestVO = new ConsumerProfileRequestVO();
		requestVO.setKeyType(ConstantsIfc.KEY_TYPE_SA);
		requestVO.setKeyValue(acctNumber);
		return getConsumerProfile(requestVO);
	}

	/**
	 * This method retrieves Consumer profile
	 * 
	 * @param <code>ConsumerProfileRequestVO</code> consumer information
	 * @return <code>ConsumerProfileReplyVO</code> Consumer profile
	 * @author Yousef Alkhalaileh
	 */
	public ConsumerProfileReplyVO getConsumerProfile(ConsumerProfileRequestVO requestVO)
	{
		String method = "getConsumerProfile";
		executionContext.audit(Level.INFO, "start for msisdn[" + requestVO.getKeyValue() + "]", className, method);

		ConsumerProfileReplyVO replyVO = null;
		MQAuditVO mqAuditVO = new MQAuditVO();
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_CONSUMER_PROFILE);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_CONSUMER_PROFILE);
		
		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(requestVO.getKeyValue());
		mqAuditVO.setFunctionName(ConstantsIfc.FUN_GET_CONSUMER_LINE_INFO);
		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", className, method);

		// Populate Header
		ConsumerRequestHeaderVO headerVO = new CustomerServiceUtil().populateRequestHeaderVO(ConstantsIfc.FUN_GET_CONSUMER_LINE_INFO, ConstantsIfc.EPORTAL_USER);

		requestVO.setHeaderVO(headerVO);

		String strXMLRequest = new ConsumerProfileXMLHandler().generateConsumerProfileXML(requestVO);
		executionContext.audit(Level.INFO, "strXMLRequest: " + strXMLRequest, className, method);
		mqAuditVO.setMessage(strXMLRequest);

		try {
			String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			executionContext.audit(Level.INFO, "strXMLReply: " + strXMLReply, className, method);
			mqAuditVO.setReply(strXMLReply);
			if (!FormatterUtility.isEmpty(strXMLReply)) {
				
				replyVO = new ConsumerProfileXMLHandler().parseConsumerProfileXML(strXMLReply);
				executionContext.audit(Level.INFO, "CustomerInfoJMSRepository >> getConsumerProfile>>MQ Audit Setting error code & error messages: " + strXMLReply, className, method);
				mqAuditVO.setErrorMessage(replyVO.getHeaderVO().getErrorMessage());
				mqAuditVO.setServiceError(replyVO.getHeaderVO().getErrorCode());
				
			} else {
				mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			}
		} catch(ServiceException e) {
			executionContext.log(Level.SEVERE, "ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
			throw e;
		} catch(Exception e) {
			executionContext.log(Level.SEVERE, "Exception in MQ Call::" + e.getMessage(), className, method, e);
			mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
		} finally {
			MQUtility.saveMQAudit(mqAuditVO);
		}
		
		return replyVO;
	}

	/**
	 * This method retrieves Consumer profile by msisdn
	 * 
	 * @param <code>String</code> customer msisdn
	 * @return <code>ConsumerRelatedNumbersReplyVO</code> customer related
	 *         numbers
	 * @author Yousef Alkhalaileh
	 */
	public ConsumerRelatedNumbersReplyVO getRelatedNumbersByMsisdn(String msisdn)
	{
		ConsumerProfileRequestVO requestVO = new ConsumerProfileRequestVO();
		requestVO.setKeyType(ConstantsIfc.KEY_TYPE_MSISDN);
		requestVO.setKeyValue(msisdn);
		return getRelatedNumbers(requestVO);
	}

	/**
	 * This method retrieves Related Numbers by serviceAccountNumber
	 * 
	 * @param <code>String</code> customer serviceAccountNumber
	 * @return <code>ConsumerRelatedNumbersReplyVO</code> customer related
	 *         numbers
	 * @author Yousef Alkhalaileh
	 */
	public ConsumerRelatedNumbersReplyVO getRelatedNumbersByAcctNumber(String serviceAccountNumber)
	{
		ConsumerProfileRequestVO requestVO = new ConsumerProfileRequestVO();
		requestVO.setKeyType(ConstantsIfc.KEY_TYPE_SA);
		requestVO.setKeyValue(serviceAccountNumber);
		return getRelatedNumbers(requestVO);
	}

	/**
	 * This method retrieves Related Numbers
	 * 
	 * @param <code>ConsumerProfileRequestVO</code> customer info
	 * @return <code>ConsumerRelatedNumbersReplyVO</code> customer related
	 *         numbers
	 * @author Yousef Alkhalaileh
	 */
	private ConsumerRelatedNumbersReplyVO getRelatedNumbers(ConsumerProfileRequestVO requestVO)
	{
		String method = "getRelatedNumbers";
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getRelatedNumbers : start for [" + requestVO.getKeyType() + "] having value [" + requestVO.getKeyValue()
				+ "]", className, method);

		ConsumerRelatedNumbersReplyVO replyVO = null;
		boolean isTesting=false;
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_RELATED_NUMBERS);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_RELATED_NUMBERS);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(requestVO.getKeyValue());
		mqAuditVO.setFunctionName(ConstantsIfc.FUN_GET_RELATED_NUMBERS_INQ);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getRelatedNumbers : requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getRelatedNumbers : replyQueueName[" + replyQueueName + "]", className, method);

		// Populate Header
		ConsumerRequestHeaderVO headerVO = new CustomerServiceUtil().populateRequestHeaderVO(ConstantsIfc.FUN_GET_RELATED_NUMBERS_INQ, ConstantsIfc.EPORTAL_USER);

		requestVO.setHeaderVO(headerVO);

		String strXMLRequest = new ConsumerProfileXMLHandler().generateConsumerProfileXML(requestVO);
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getRelatedNumbers : strXMLRequest: " + strXMLRequest, className, method);
		mqAuditVO.setMessage(strXMLRequest);
		String strXMLReply = ""; 
		if(isTesting){
			strXMLReply="<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><MsgFormat>RelatedAccountsInq</MsgFormat><MsgVersion>0001</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId>" +
					"<RequestorChannelFunction>RelatedAccountsInq</RequestorChannelFunction><RequestorUserId>ePortalUser</RequestorUserId><RequestorLanguage>E</RequestorLanguage>" +
					"<RequestorSecurityInfo>secure</RequestorSecurityInfo><ChannelTransId>EPOR_20171108162912</ChannelTransId><Status>6</Status><ErrorCode>0000</ErrorCode>" +
					"<ErrorMessage>Success</ErrorMessage></SR_HEADER_REPLY><RelatedAccountsList><RelatedAccountInfo><ServiceAccountNumber>1000114876869290</ServiceAccountNumber>" +
					"<BillingAccountNumber>1000114876891479</BillingAccountNumber><MSISDN/><IDDocType>Saudi National ID</IDDocType><IDDocNumber>1076120656</IDDocNumber>" +
					"<CPESerialNumber>SAM0318C5100WN-075900</CPESerialNumber><Status>Active</Status><Type>Prepaid Plan</Type><PackageID>5001</PackageID><PackageName>mobily wimax</PackageName>" +
					"<CustomerCategory>Individual</CustomerCategory><ProductLine>WiMax</ProductLine><ActivationDate>06/22/2011</ActivationDate>	</RelatedAccountInfo>" +
					"<RelatedAccountInfo><ServiceAccountNumber>100019954012575</ServiceAccountNumber><BillingAccountNumber>100019954012437</BillingAccountNumber>" +
					"<MSISDN>966541081154</MSISDN><IDDocType>Saudi National ID</IDDocType><IDDocNumber>1076120656</IDDocNumber><CPESerialNumber/><Status>Active</Status><Type>Postpaid Plan</Type>" +
					"<PackageID>1825</PackageID><PackageName>RAQI</PackageName><CustomerCategory>Individual</CustomerCategory><ProductLine>Post-Paid</ProductLine><ActivationDate>10/18/2009</ActivationDate>" +
					"</RelatedAccountInfo><RelatedAccountInfo><ServiceAccountNumber>100019954012577</ServiceAccountNumber><BillingAccountNumber>100019954012439</BillingAccountNumber><MSISDN>966110234567</MSISDN>" +
					"<IDDocType>Saudi National ID</IDDocType><IDDocNumber>1076120656</IDDocNumber><CPESerialNumber/><Status>Active</Status><Type>Postpaid Plan</Type><PackageID>1825</PackageID>" +
					"<PackageName>RAQI</PackageName><CustomerCategory>Individual</CustomerCategory><ProductLine>Fixed Voice</ProductLine><ActivationDate>10/18/2009</ActivationDate>" +
					"</RelatedAccountInfo></RelatedAccountsList></MOBILY_BSL_SR_REPLY>";
		}else{
			try {
				strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);

				
				executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getRelatedNumbers : strXMLReply: " + strXMLReply, className, method);
				mqAuditVO.setReply(strXMLReply);
				
				if (!FormatterUtility.isEmpty(strXMLReply))
				{
					replyVO = new ConsumerProfileXMLHandler().parseRelatedNumberXML(strXMLReply);
					mqAuditVO.setErrorMessage(replyVO.getHeaderVO().getErrorMessage());
					mqAuditVO.setServiceError(replyVO.getHeaderVO().getErrorCode());
					
				} else {
					executionContext.log(Level.SEVERE, "CustomerInfoServiceImpl > getRelatedNumbers : Empty reply from back end", className, method, null);
					mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
					mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
					
					throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				}
			} catch(ServiceException e) {
				executionContext.log(Level.SEVERE, "ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
				throw e;
			} catch(BackEndException e) {
				throw e;
			} catch(Exception e) {
				executionContext.log(Level.SEVERE, "Exception in MQ Call::" + e.getMessage(), className, method, e);
				mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				
				throw new BackEndException(ExceptionConstantIfc.ACTIVE_NUMBER_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			} finally {
				MQUtility.saveMQAudit(mqAuditVO);
			}
		}


		return replyVO;
	}

	/**
	 * This method retrieves corporate profile by billingAccountNumber
	 * 
	 * @param <code>String</code> billingAccountNumber
	 * @return <code>CorporateProfileReplyVO</code> corporate information
	 * @author Yousef Alkhalaileh
	 */
	public CorporateProfileReplyVO getCorporateProfile(String billingAccountNumber)
	{
		String method = "getCorporateProfile";
		executionContext.audit(Level.INFO, "start for billingAccountNumber[" + billingAccountNumber + "]", className, method);

		CorporateProfileReplyVO replyVO = null;
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_CORPORATE_PROFILE);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_CORPORATE_PROFILE);
		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", className, method);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(billingAccountNumber);
		mqAuditVO.setFunctionName(ConstantsIfc.CORP_PROFILE_FUNC_ID);
		
		String strXMLRequest = new CorporateProfileXMLHandler().generateCorporateProfileXML(billingAccountNumber);
		executionContext.audit(Level.INFO, "strXMLRequest: " + strXMLRequest, className, method);
		mqAuditVO.setMessage(strXMLRequest);
		
		try {
			String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			mqAuditVO.setReply(strXMLReply);
			executionContext.audit(Level.INFO, "strXMLReply: " + strXMLReply, className, method);
			
			if (!FormatterUtility.isEmpty(strXMLReply) && (strXMLReply.indexOf("<ErrorCode>SBL-MOB-0000</ErrorCode>") > 0 || strXMLReply.indexOf("<ErrorCode>0000</ErrorCode>") > 0) ){
				replyVO = new CorporateProfileXMLHandler().parseCorporateProfileXML(strXMLReply);
				mqAuditVO.setErrorMessage(strXMLReply.substring(strXMLReply.indexOf("<ErrorCode>")+11, strXMLReply.indexOf("</ErrorCode>")));
				mqAuditVO.setServiceError(mqAuditVO.getErrorMessage());
				
			} else if(FormatterUtility.isNotEmpty(strXMLReply)){
				mqAuditVO.setErrorMessage(strXMLReply.substring(strXMLReply.indexOf("<ErrorCode>")+11, strXMLReply.indexOf("</ErrorCode>")));
				mqAuditVO.setServiceError(mqAuditVO.getErrorMessage());
				
			}
		} catch(ServiceException e) {
			executionContext.log(Level.SEVERE, "ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
			throw e;
		} catch(Exception e) {
			executionContext.log(Level.SEVERE, "Exception in MQ Call::" + e.getMessage(), className, method, e);
			mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			
		} finally {
			MQUtility.saveMQAudit(mqAuditVO);
		}
		
		return replyVO;
	}
	
	/**
	 * Get Corporate Profile V01
	 * @param rootBillingAccountNumber
	 * @return
	 */
	public sa.com.mobily.eportal.corporateprofile.jaxb.reply.MOBILYSRMESSAGE getCorporateProfileV01(String rootBillingAccountNumber)
	{
		String method = "getCorporateProfileV01";
		executionContext.audit(Level.INFO, "start for billingAccountNumber[" + rootBillingAccountNumber + "]", className, method);

		sa.com.mobily.eportal.corporateprofile.jaxb.reply.MOBILYSRMESSAGE replyVO = null;
		MQAuditVO mqAuditVO = new MQAuditVO();
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_CORPORATE_PROFILE);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_CORPORATE_PROFILE);

		executionContext.audit(Level.INFO, "requestQueueName[" + requestQueueName + "]", className, method);
		executionContext.audit(Level.INFO, "replyQueueName[" + replyQueueName + "]", className, method);
		
		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(rootBillingAccountNumber);
		mqAuditVO.setFunctionName(ConstantsIfc.CORP_PROFILE_FUNC_ID);

		String strXMLRequest = new CorporateProfileXMLHandler().generateCorporateProfileXMLV01(rootBillingAccountNumber);
		executionContext.audit(Level.INFO, "strXMLRequest: " + strXMLRequest, className, method);
		
		mqAuditVO.setMessage(strXMLRequest);
		
		try {
		
			String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			executionContext.audit(Level.INFO, "strXMLReply: " + strXMLReply, className, method);
	
			if (!FormatterUtility.isEmpty(strXMLReply))
			{
				mqAuditVO.setReply(strXMLReply);
				replyVO = new CorporateProfileXMLHandler().parseCorporateProfileXMLV01(strXMLReply);
			}
		} catch(ServiceException e) {
			executionContext.log(Level.SEVERE, "ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
			throw e;
		} catch(Exception e) {
			executionContext.log(Level.SEVERE, "Exception in MQ Call::" + e.getMessage(), className, method, e);
			mqAuditVO.setErrorMessage(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			
		} finally {
			MQUtility.saveMQAudit(mqAuditVO);
		}
		
		return replyVO;
	}
	
	public CustomerInfoVO getCustContactDetails(CustomerInfoVO customerInfoVO)
	{
		String method = "getCustContactDetails";
		executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustContactDetails : start for account: " + customerInfoVO.getSourceId(), className, method);

		if(customerInfoVO != null && customerInfoVO.getSourceId() != null && CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN.equals(customerInfoVO.getSourceSpecName()) && customerInfoVO.getSourceId().length() < 12)
		{
			customerInfoVO.setSourceId(FormatterUtility.getUniversalFormat(customerInfoVO.getSourceId()));
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustContactDetails : Account after universal format: " + customerInfoVO.getSourceId(), className, method);
		}
		MQAuditVO mqAuditVO = new MQAuditVO();
		//Changes to restrict customer inquiry for null MSISDN
		if(FormatterUtility.isNotEmpty(customerInfoVO.getSourceId())){
			if(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN.equals(customerInfoVO.getSourceSpecName())/*||CustomerInfoVO.SOURCE_SPECIFICATION_NAME_BILLING_ACCOUNT_NUMBER.equals(customerInfoVO.getSourceSpecName())*/){
				if((customerInfoVO.getSourceId().length()<10)||(!FormatterUtility.isNumeric(customerInfoVO.getSourceId()))){
					{
						executionContext.audit(Level.SEVERE, "CustomerInfoServiceImpl > getCustContactDetails > Source Id is not in correct format....!", className, method);
						throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
					}
				}
			}
			String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.REQUEST_QUEUE_CUSTOMER_PROFILE);
			String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.REPLY_QUEUE_CUSTOMER_PROFILE);
			// TODO check caching here
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustContactDetails : requestQueueName[" + requestQueueName + "]", className, method);
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustContactDetails : replyQueueName[" + replyQueueName + "]", className, method);
	
			String strXMLRequest = new CustomerInfoWithContactXMLHandler().generateCustomerInfoWithContactXML(customerInfoVO);
			executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustContactDetails : strXMLRequest: " + strXMLRequest, className, method);
	
			mqAuditVO.setRequestQueue(requestQueueName);
			mqAuditVO.setReplyQueue(replyQueueName);
			mqAuditVO.setUserName(customerInfoVO.getUsrId());
			if(FormatterUtility.isNotEmpty(customerInfoVO.getMSISDN())){
				mqAuditVO.setMsisdn(customerInfoVO.getMSISDN());
			}else{
				mqAuditVO.setMsisdn(customerInfoVO.getAccountNumber());
			}
			if(customerInfoVO.getFuncId()==-1){
				mqAuditVO.setFunctionName(String.valueOf(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION));
			}
			else if (customerInfoVO.getFuncId() != ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY){
				mqAuditVO.setFunctionName(String.valueOf(customerInfoVO.getFuncId()));
			}else{
				mqAuditVO.setFunctionName(String.valueOf(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY));
			}
			mqAuditVO.setMessage(strXMLRequest);
			
			try {
				String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
				executionContext.audit(Level.INFO, "CustomerInfoServiceImpl > getCustContactDetails : strXMLReply: " + strXMLReply, className, method);
				mqAuditVO.setReply(strXMLReply);
				if (!FormatterUtility.isEmpty(strXMLReply))
				{
					customerInfoVO = new CustomerInfoWithContactXMLHandler().parseCustomerInfoWithContactXML(strXMLReply);
		
					if (null != customerInfoVO.getStatusCode())
					{
						mqAuditVO.setErrorMessage(customerInfoVO.getStatusCode());
						mqAuditVO.setServiceError(customerInfoVO.getStatusCode());
						
						if (ConstantsIfc.STATUS_CODE_CUSTOMER_NOT_FOUND.equalsIgnoreCase(customerInfoVO.getStatusCode())
								|| ConstantsIfc.STATUS_CODE_CUSTOMER_NOT_SUBSCRIBED.equalsIgnoreCase(customerInfoVO.getStatusCode()))
						{
							
							throw new DataNotFoundException("Either customer is not found or is not Subscribed.");
						}
						// handling case there is no body in case of error code
						// (E500004,E020061,E200021,E500024,E500025,E020043,E810999,E510999,E226001,E950014,E950015,E999998,E999999)
						if (customerInfoVO.getStatusCode().indexOf("E") != -1)
							throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, customerInfoVO.getStatusCode());
					}
		
				} else {
					executionContext.log(Level.SEVERE, "CustomerInfoServiceImpl > getCustContactDetails : Empty reply from back end", className, method, null);
					mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
					mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
					
					throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				}
			} catch(ServiceException e) {
				executionContext.log(Level.SEVERE, "CustomerInfoServiceImpl > getCustContactDetails > ServiceException while parsing xml reply::" + e.getMessage(), className, method, e);
				throw e;
			} catch(DataNotFoundException e) {
				throw e;
			} catch(BackEndException e) {
				throw e;
			} catch(Exception e) {
				executionContext.log(Level.SEVERE, "CustomerInfoServiceImpl > getCustContactDetails > Exception in MQ Call::" + e.getMessage(), className, method, e);
				mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
				mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
				
				throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			} finally {
				MQUtility.saveMQAudit(mqAuditVO);
			}
		} else {
			executionContext.audit(Level.SEVERE, "CustomerInfoServiceImpl > getCustContactDetails > Source Id is EMPTY....!", className, method);
			/*mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE));
			mqAuditVO.setServiceError(ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
			MQUtility.saveMQAudit(mqAuditVO);*/
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.ERROR_CODE_EMPTY_REPLY_XML);
		}
		executionContext.audit(Level.SEVERE, "CustomerInfoServiceImpl > getCustContactDetails > end > customerInfoVO::"+customerInfoVO,className, method);
		return customerInfoVO;
	}

	public CustomerInfoVO getCustContactDetailsByMsisdn(String msisdn)
	{
		executionContext.audit(Level.INFO, "CustomerInfoJMSRepository > getCustContactDetailsByMsisdn > msisdn::" + msisdn, className, "getCustProductProfileByMsisdn");		
		/*CustomerInfoVO customerInfoVO = new CustomerInfoVO();
		customerInfoVO.setSourceId(msisdn);
		customerInfoVO.setSourceSpecName(CustomerInfoVO.SOURCE_SPECIFICATION_NAME_MSISDN);
		customerInfoVO.setPreferredLanguage("En");
		return getCustomerProfile(customerInfoVO);*/
		
		UserProfileUtil userProfileUtil = new UserProfileUtil();
		return userProfileUtil.getContactDetails(msisdn);
	}
	public static void main(String[] args)
	{
		CustomerInfoVO customerInfoVO = new CustomerInfoVO();
		// String strXMLReply
		// ="<?xml version=\"1.0\" encoding=\"UTF-8\"?><NS1:retrieveCustomerProductConfigurationRs xmlns:NS1=\"http://www.ejada.com\"><MsgRsHdr xmlns=\"http://www.ejada.com\"><StatusCode>E500004</StatusCode><RqUID>20140105184045</RqUID></MsgRsHdr></NS1:retrieveCustomerProductConfigurationRs>";
		String strXMLReply = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><NS1:retrieveCustomerProductConfigurationRs xmlns:NS1=\"http://www.ejada.com\"><MsgRsHdr xmlns=\"http://www.ejada.com\"><StatusCode>I000000</StatusCode><RqUID>20130312112500101</RqUID></MsgRsHdr><Body><NS2:customerConfiguration xmlns:NS2=\"http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/CustomerConfigurationMessage\">" +
				"<Customer><fromTimestamp>2013-03-26T09:53:48</fromTimestamp><Party>" +
				"<Id>1000113406136308</Id><Specification><Category>P</Category><Type>Consumer</Type></Specification><PartyIdentification><Specification><Type>IQAMA</Type></Specification><Id>2228764391</Id></PartyIdentification><CharacteristicValue><Characteristic><Name>IsCorporateKA</Name></Characteristic>" +
				"<Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CustomerBSLId</Name></Characteristic><Value>1-69990EX</Value></CharacteristicValue><PartyExtensions><PartyChoice><Individual><Gender>F</Gender></Individual></PartyChoice></PartyExtensions></Party></Customer><Agreement><CustomerAccountId>" +
				"<Id>1000113620426585</Id></CustomerAccountId><AgreementItem><CharacteristicValue><Characteristic><Name>MSISDN</Name></Characteristic><Value>966540510212</Value></CharacteristicValue><CharacteristicValue>" +
				"<Characteristic><Name>PackageId</Name></Characteristic><Value>1785</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsIUC</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageName</Name></Characteristic><Value>Wajid Plus</Value></CharacteristicValue>" +
				"<CharacteristicValue><Characteristic><Name>BillingId</Name></Characteristic><Value>1000113620426572</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageDescription</Name></Characteristic><Value>Wajid Plus</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ServiceId</Name>" +
				"</Characteristic><Value>1000113620426585</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ServiceStatus</Name></Characteristic><Value>Active</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>DealName</Name></Characteristic><Value>Unknown Deal Name</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ISMSIM</Name></Characteristic><Value>N</Value>" +
				"</CharacteristicValue><CharacteristicValue><Characteristic><Name>PayType</Name></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>Duration</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>MAXUploadRate</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>MaxDownloadRate</Name>" +
				"</Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageServiceType</Name></Characteristic><Value>GSM</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PlanCategory</Name></Characteristic><Value>5</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ProductLine</Name></Characteristic><Value>Non UDP - UDP PC IUC FROM PACK</Value></CharacteristicValue>" +
				"<CharacteristicValue><Characteristic><Name>CustomerActualSegment</Name></Characteristic><Value>SILVER</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsNewControl</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageCategory</Name></Characteristic><Value>GSM</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CorporatePackage</Name>" +
				"</Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsMix</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsControlPlus</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsIPhone</Name></Characteristic><Value>N</Value></CharacteristicValue></AgreementItem></Agreement></NS2:customerConfiguration>" +
				"</Body></NS1:retrieveCustomerProductConfigurationRs>";
		try
		{
			customerInfoVO = new CustomerInfoXMLHandler().parseCustomerInfoXML(strXMLReply);
			// handling case there is no body in case of error code
			if (null != customerInfoVO.getStatusCode())
				if (customerInfoVO.getStatusCode().indexOf("E") != -1)
					throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, customerInfoVO.getStatusCode());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}