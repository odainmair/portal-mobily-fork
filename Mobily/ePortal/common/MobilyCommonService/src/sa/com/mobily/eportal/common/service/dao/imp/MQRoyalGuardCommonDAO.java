/*
 * Created on March 31, 2010
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.RoyalGuardCommonDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQRoyalGuardCommonDAO implements RoyalGuardCommonDAO{
	
	private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;

	public String sendToMQWithReply(String xmlRequestMessage) throws MobilyCommonException {
		String replyMessage = "";
		log.debug("MQRoyalGuardDAO > sendToMQWithReply > called");
		try {
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.RG_REQUEST_QUEUENAME);
			log.debug("MQRoyalGuardDAO > sendToMQWithReply >Request Queue Name  ["+ strQueueName + "]");

			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.RG_REPLY_QUEUENAME);
			log.debug("MQRoyalGuardDAO > sendToMQWithReply >Reply Queue Name ["+ strReplyQueueName + "]");
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, strQueueName,strReplyQueueName);
			
		} catch (RuntimeException e) {
			log.fatal("MQRoyalGuardDAO > sendToMQWithReply > MQSend Exception "+ e);
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("MQRoyalGuardDAO > sendToMQWithReply >Exception " + e);
			throw new SystemException(e);
		}
		return replyMessage;
	}	

}
