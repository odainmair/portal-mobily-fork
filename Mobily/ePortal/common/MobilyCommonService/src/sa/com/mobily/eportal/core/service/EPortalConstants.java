package sa.com.mobily.eportal.core.service;

public interface EPortalConstants {
    
    /***
     * The format used when sending dates to Siebel backend
     */
    public static final String SIEBEL_DATE_FORMAT = "yyyyMMddHHmmss";
    public static final String SIEBEL_DATE_ONLY_FORMAT = "yyyyMMdd";
    public static final String SIEBEL_TIME_FORMAT = "HHmm";
    public static final String PREFIX_CONNECT_ACCOUNT = "200";
    public static final String PREFIX_UNIVERSAL = "9665";
    
    public static final String PREFIX_BACKEND_CACHE = "/backend";

}
