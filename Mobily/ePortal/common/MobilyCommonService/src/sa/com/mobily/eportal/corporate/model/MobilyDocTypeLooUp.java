package sa.com.mobily.eportal.corporate.model;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MobilyDocTypeLooUp extends BaseVO{
 
	private static final long serialVersionUID = 1L;

	private int ID;
	private String DOCUMENT_TYPE;
	private String ENGLISH_NAME;
	private String ARABIC_NAME;
	private int STATUS;
	private int PRIORITY;
	private int LINEID;
	
	public int getID()
	{
		return ID;
	}
	public void setID(int iD)
	{
		ID = iD;
	}
	public String getDOCUMENT_TYPE()
	{
		return DOCUMENT_TYPE;
	}
	public void setDOCUMENT_TYPE(String dOCUMENT_TYPE)
	{
		DOCUMENT_TYPE = dOCUMENT_TYPE;
	}
	public String getENGLISH_NAME()
	{
		return ENGLISH_NAME;
	}
	public void setENGLISH_NAME(String eNGLISH_NAME)
	{
		ENGLISH_NAME = eNGLISH_NAME;
	}
	public String getARABIC_NAME()
	{
		return ARABIC_NAME;
	}
	public void setARABIC_NAME(String aRABIC_NAME)
	{
		ARABIC_NAME = aRABIC_NAME;
	}
	public int getSTATUS()
	{
		return STATUS;
	}
	public void setSTATUS(int sTATUS)
	{
		STATUS = sTATUS;
	}
	public int getPRIORITY()
	{
		return PRIORITY;
	}
	public void setPRIORITY(int pRIORITY)
	{
		PRIORITY = pRIORITY;
	}
	public int getLINEID()
	{
		return LINEID;
	}
	public void setLINEID(int lINEID)
	{
		LINEID = lINEID;
	}
}
