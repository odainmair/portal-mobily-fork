package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}IsPrimary"/>
 *         &lt;element ref="{}AuthorizedPersonFirstName"/>
 *         &lt;element ref="{}AuthorizedPersonMiddleName"/>
 *         &lt;element ref="{}AuthorizedPersonLastName"/>
 *         &lt;element ref="{}AuthorizedPersonGender"/>
 *         &lt;element ref="{}AuthorizedPersonNationality"/>
 *         &lt;element ref="{}AuthorizedPersonEmail"/>
 *         &lt;element ref="{}AuthorizedPersonFax"/>
 *         &lt;element ref="{}AuthorizedPersonMobile"/>
 *         &lt;element ref="{}AuthorizedPersonTelephone"/>
 *         &lt;element ref="{}BillingCellNumber"/>
 *         &lt;element ref="{}BillingEmail"/>
 *         &lt;element ref="{}BillingFaxNum"/>
 *         &lt;element ref="{}BillingFirstName"/>
 *         &lt;element ref="{}BillingMidName"/>
 *         &lt;element ref="{}BillingLastName"/>
 *         &lt;element ref="{}BillingGender"/>
 *         &lt;element ref="{}BillingWorkNumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "isPrimary",
    "authorizedPersonFirstName",
    "authorizedPersonMiddleName",
    "authorizedPersonLastName",
    "authorizedPersonGender",
    "authorizedPersonNationality",
    "authorizedPersonEmail",
    "authorizedPersonFax",
    "authorizedPersonMobile",
    "authorizedPersonTelephone",
    "billingCellNumber",
    "billingEmail",
    "billingFaxNum",
    "billingFirstName",
    "billingMidName",
    "billingLastName",
    "billingGender",
    "billingWorkNumber"
})
@XmlRootElement(name = "Contact")
public class ContactVO extends BaseVO{

    @XmlElement(name = "IsPrimary", required = true)
    protected String isPrimary;
    @XmlElement(name = "AuthorizedPersonFirstName", required = true)
    protected String authorizedPersonFirstName;
    @XmlElement(name = "AuthorizedPersonMiddleName", required = true)
    protected String authorizedPersonMiddleName;
    @XmlElement(name = "AuthorizedPersonLastName", required = true)
    protected String authorizedPersonLastName;
    @XmlElement(name = "AuthorizedPersonGender", required = true)
    protected String authorizedPersonGender;
    @XmlElement(name = "AuthorizedPersonNationality", required = true)
    protected String authorizedPersonNationality;
    @XmlElement(name = "AuthorizedPersonEmail", required = true)
    protected String authorizedPersonEmail;
    @XmlElement(name = "AuthorizedPersonFax", required = true)
    protected String authorizedPersonFax;
    @XmlElement(name = "AuthorizedPersonMobile", required = true)
    protected String authorizedPersonMobile;
    @XmlElement(name = "AuthorizedPersonTelephone", required = true)
    protected String authorizedPersonTelephone;
    @XmlElement(name = "BillingCellNumber", required = true)
    protected String billingCellNumber;
    @XmlElement(name = "BillingEmail", required = true)
    protected String billingEmail;
    @XmlElement(name = "BillingFaxNum", required = true)
    protected String billingFaxNum;
    @XmlElement(name = "BillingFirstName", required = true)
    protected String billingFirstName;
    @XmlElement(name = "BillingMidName", required = true)
    protected String billingMidName;
    @XmlElement(name = "BillingLastName", required = true)
    protected String billingLastName;
    @XmlElement(name = "BillingGender", required = true)
    protected String billingGender;
    @XmlElement(name = "BillingWorkNumber", required = true)
    protected String billingWorkNumber;

    /**
     * Gets the value of the isPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Sets the value of the isPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Gets the value of the authorizedPersonFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonFirstName() {
        return authorizedPersonFirstName;
    }

    /**
     * Sets the value of the authorizedPersonFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonFirstName(String value) {
        this.authorizedPersonFirstName = value;
    }

    /**
     * Gets the value of the authorizedPersonMiddleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonMiddleName() {
        return authorizedPersonMiddleName;
    }

    /**
     * Sets the value of the authorizedPersonMiddleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonMiddleName(String value) {
        this.authorizedPersonMiddleName = value;
    }

    /**
     * Gets the value of the authorizedPersonLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonLastName() {
        return authorizedPersonLastName;
    }

    /**
     * Sets the value of the authorizedPersonLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonLastName(String value) {
        this.authorizedPersonLastName = value;
    }

    /**
     * Gets the value of the authorizedPersonGender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonGender() {
        return authorizedPersonGender;
    }

    /**
     * Sets the value of the authorizedPersonGender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonGender(String value) {
        this.authorizedPersonGender = value;
    }

    /**
     * Gets the value of the authorizedPersonNationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonNationality() {
        return authorizedPersonNationality;
    }

    /**
     * Sets the value of the authorizedPersonNationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonNationality(String value) {
        this.authorizedPersonNationality = value;
    }

    /**
     * Gets the value of the authorizedPersonEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonEmail() {
        return authorizedPersonEmail;
    }

    /**
     * Sets the value of the authorizedPersonEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonEmail(String value) {
        this.authorizedPersonEmail = value;
    }

    /**
     * Gets the value of the authorizedPersonFax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonFax() {
        return authorizedPersonFax;
    }

    /**
     * Sets the value of the authorizedPersonFax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonFax(String value) {
        this.authorizedPersonFax = value;
    }

    /**
     * Gets the value of the authorizedPersonMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonMobile() {
        return authorizedPersonMobile;
    }

    /**
     * Sets the value of the authorizedPersonMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonMobile(String value) {
        this.authorizedPersonMobile = value;
    }

    /**
     * Gets the value of the authorizedPersonTelephone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizedPersonTelephone() {
        return authorizedPersonTelephone;
    }

    /**
     * Sets the value of the authorizedPersonTelephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizedPersonTelephone(String value) {
        this.authorizedPersonTelephone = value;
    }

    /**
     * Gets the value of the billingCellNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingCellNumber() {
        return billingCellNumber;
    }

    /**
     * Sets the value of the billingCellNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingCellNumber(String value) {
        this.billingCellNumber = value;
    }

    /**
     * Gets the value of the billingEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingEmail() {
        return billingEmail;
    }

    /**
     * Sets the value of the billingEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingEmail(String value) {
        this.billingEmail = value;
    }

    /**
     * Gets the value of the billingFaxNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingFaxNum() {
        return billingFaxNum;
    }

    /**
     * Sets the value of the billingFaxNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingFaxNum(String value) {
        this.billingFaxNum = value;
    }

    /**
     * Gets the value of the billingFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingFirstName() {
        return billingFirstName;
    }

    /**
     * Sets the value of the billingFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingFirstName(String value) {
        this.billingFirstName = value;
    }

    /**
     * Gets the value of the billingMidName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingMidName() {
        return billingMidName;
    }

    /**
     * Sets the value of the billingMidName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingMidName(String value) {
        this.billingMidName = value;
    }

    /**
     * Gets the value of the billingLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingLastName() {
        return billingLastName;
    }

    /**
     * Sets the value of the billingLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingLastName(String value) {
        this.billingLastName = value;
    }

    /**
     * Gets the value of the billingGender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingGender() {
        return billingGender;
    }

    /**
     * Sets the value of the billingGender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingGender(String value) {
        this.billingGender = value;
    }

    /**
     * Gets the value of the billingWorkNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillingWorkNumber() {
        return billingWorkNumber;
    }

    /**
     * Sets the value of the billingWorkNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillingWorkNumber(String value) {
        this.billingWorkNumber = value;
    }

}
