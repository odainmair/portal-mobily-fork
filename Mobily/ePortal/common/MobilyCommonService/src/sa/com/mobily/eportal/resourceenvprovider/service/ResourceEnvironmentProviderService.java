package sa.com.mobily.eportal.resourceenvprovider.service;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.AppConfig;
import sa.com.mobily.eportal.resourceenvprovider.MobilyConfiguration;

/**
 * Generic Service class for getting access to Resource Environment Entries.
 * Takes JNDI path in its constructor to get access to the defined Resource
 * Environment Provider and fetch its custom properties.
 * 
 * @author Muzammil Mohsin Shaikh
 */
public class ResourceEnvironmentProviderService
{
	private static ResourceEnvironmentProviderService instance;
	private MobilyConfiguration config;
	private static final String RESOURCE_ENV_PROVIDER_JNDI = "RESOURCE_ENV_PROVIDER_JNDI";
	
	private ResourceEnvironmentProviderService(){
		if(this.config == null){
			try
			{
				config = (MobilyConfiguration) new InitialContext().lookup(AppConfig.getInstance().get(RESOURCE_ENV_PROVIDER_JNDI));
			}
			catch (NamingException e)
			{
				throw new ServiceException(e.getMessage(), e.getCause());
			}
			catch (Exception e)
			{
				throw new ServiceException(e.getMessage(), e.getCause());
			}
			
		}
	}
	
	public static ResourceEnvironmentProviderService getInstance(){
		if(instance == null){
			instance = new ResourceEnvironmentProviderService();
		}
		return instance;
	}

	/**
	 * Returns mapped value for the custom property provided as argument.
	 * 
	 * @param property
	 *            Custom property defined in the resource environment entry.
	 * @return <CODE>Object</CODE> Custom property mapped value
	 */
	public Object getPropertyValue(String property){
	
		return config.getAttribute(property);
	}
	
	public String getStringProperty(String property){
			Object value=getPropertyValue(property);
			if(value!=null)
				return (String)value;
		 
		return null;
	}
}