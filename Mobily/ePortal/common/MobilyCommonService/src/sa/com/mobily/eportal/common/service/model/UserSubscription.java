package sa.com.mobily.eportal.common.service.model;

import java.sql.Timestamp;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.vo.BaseVO;
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserSubscription", propOrder = { "subscriptionId", "ccStatus",
		"createdDate", "customerType",
		"isActive", "isDefault", "defaultSubscription",
		"lastUpdatedTime","msisdn","packageId","serviceAcctNumber","subscriptionName","subscriptionType","lineType",
		"userAcctId","customerSegment","nationalIdOrIqamaNumber"})
public class UserSubscription extends BaseVO {

	private static final long serialVersionUID = -2397606794134120945L;
	@XmlElement(name = "subscriptionId")
	private Long subscriptionId;
	@XmlElement(name = "ccStatus")
	private Integer ccStatus;
	@XmlElement(name = "createdDate")
	private Date createdDate;
	@XmlElement(name = "customerType")
	private String customerType;
	@XmlElement(name = "isActive")
	private String isActive;
	@XmlElement(name = "isDefault")
	private String isDefault;//string variable for DB mapping contains 'Y' or 'N'
	@XmlElement(name = "defaultSubscription")
	private boolean defaultSubscription; //boolean variable for business logic  for the default subscription
	@XmlElement(name = "lastUpdatedTime")
	private Date lastUpdatedTime;
	@XmlElement(name = "msisdn")
	private String msisdn;
	@XmlElement(name = "packageId")
	private String packageId;
	@XmlElement(name = "serviceAcctNumber")
	private String serviceAcctNumber;
	@XmlElement(name = "subscriptionName")
	private String subscriptionName;
	@XmlElement(name = "subscriptionType")
	private Integer subscriptionType;
	@XmlElement(name = "lineType")
	private Integer lineType;
	@XmlElement(name = "userAcctId")
	private long userAcctId;
	@XmlElement(name = "customerSegment")
	private String customerSegment;// 1 for consumer ,2 for business  . this variable is not persisted
	@XmlElement(name = "nationalIdOrIqamaNumber")
	private String nationalIdOrIqamaNumber;

	//Added by r.agarwal.mit for ALSAMOU
	/*@XmlElement(name = "UserSubscriptionEligibility")
	private UserSubscriptionEligibilityVO userSubscriptionEligibilityVO;*/
	
/*	@XmlElement(name = "isSameIdNumber")
	private String isSameIdNumber;
	
	public String getIsSameIdNumber()
	{
		return isSameIdNumber;
	}
	public void setIsSameIdNumber(String isSameIdNumber) {
		this.isSameIdNumber = isSameIdNumber;
	}
	public void setIsSameIdNumber(boolean isSameIdNumber)
	{
		if(isSameIdNumber)
			this.isSameIdNumber="Y";
		else
			this.isSameIdNumber="N";
	}*/
	
	public Long getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}
	public Integer getCcStatus() {
		return ccStatus;
	}
	public void setCcStatus(Integer ccStatus) {
		this.ccStatus = ccStatus;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	public String getIsDefault() {
		return isDefault;
	}
	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
		if(isDefault!=null)
			if(isDefault.equals("Y"))
				 defaultSubscription=true;
	}
	public void setIsDefault(boolean isDefault){
		if(isDefault)
			this.isDefault="Y";
		else
			this.isDefault="N";
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getServiceAcctNumber() {
		return serviceAcctNumber;
	}
	public void setServiceAcctNumber(String serviceAcctNumber) {
		this.serviceAcctNumber = serviceAcctNumber;
	}
	public String getSubscriptionName() {
		return subscriptionName;
	}
	public void setSubscriptionName(String subscriptionName) {
		this.subscriptionName = subscriptionName;
	}
	public Integer getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(Integer subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public Integer getLineType() {
		return lineType;
	}
	public void setLineType(Integer lineType) {
		this.lineType = lineType;
	}
	public long getUserAcctId() {
		return userAcctId;
	}
	public void setUserAcctId(long userAcctId) {
		this.userAcctId = userAcctId;
	}
	public boolean isDefaultSubscription()
	{
		return defaultSubscription;
	}
	public void setDefaultSubscription(boolean defaultSubscription)
	{
		this.defaultSubscription = defaultSubscription;
	}
	public String getCustomerSegment()
	{
		return customerSegment;
	}
	public void setCustomerSegment(String customerSegment)
	{
		this.customerSegment = customerSegment;
	}
	public String getNationalIdOrIqamaNumber()
	{
		return nationalIdOrIqamaNumber;
	}
	public void setNationalIdOrIqamaNumber(String nationalIdOrIqamaNumber)
	{
		this.nationalIdOrIqamaNumber = nationalIdOrIqamaNumber;
	}
	public Date getCreatedDate()
	{
		return createdDate;
	}
	public Timestamp getCreatedDateAsTimestamp()
	{
		if(createdDate == null)
			return null;
		return new Timestamp(createdDate.getTime());
	}
	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}
	public Date getLastUpdatedTime()
	{
		return lastUpdatedTime;
	}
	public Timestamp getLastUpdatedTimeAsTimestamp()
	{
		if(lastUpdatedTime == null)
			return null;
		return new Timestamp(lastUpdatedTime.getTime());
	}
	public void setLastUpdatedTime(Date lastUpdatedTime)
	{
		this.lastUpdatedTime = lastUpdatedTime;
	}
/*	public UserSubscriptionEligibilityVO getUserSubscriptionEligibilityVO()
	{
		return userSubscriptionEligibilityVO;
	}
	public void setUserSubscriptionEligibilityVO(UserSubscriptionEligibilityVO userSubscriptionEligibilityVO)
	{
		this.userSubscriptionEligibilityVO = userSubscriptionEligibilityVO;
	}*/
	
	
}