/*
 * Created on Nov 20, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * 
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UserAccountVO {
	private int id;
	private String msisdn = null;
	private String userName = null; 
	private String fullName = null;
	private String privilege = null;
	private String usertype = null;
	private String lastLoginTime = null;
	private String gender = null;
	private String age = null;
	private String nationality = null;
	private String emailAddress = null;
	private String preferredLanguage = null;
	private String userActivated = null;
	private String registrationDate = null;
	private String activationDate = null;
	private String skyType = null;
	private String userType = null;
	private String skySuspendDate = null;
	private String blockStatus = null;
	private String blockChangeDate = null;
	private int lineType ;
	private String packageId;
	private String securityAnswer;
	private String firstName = null;
	private String lastName = null;
	
	public String getIqama() {
		return iqama;
	}
	public void setIqama(String iqama) {
		this.iqama = iqama;
	}

	private String iqama ="";
	private byte customerType ;
	
	
	private String serviceAccountNumber;
	public String getServiceAccountNumber() {
		return serviceAccountNumber;
	}
	public void setServiceAccountNumber(String serviceAccountNumber) {
		this.serviceAccountNumber = serviceAccountNumber;
	}
	
	private int securityQuesId ;
	private String securityQuesAns = null;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}


	public byte getCustomerType() {
		return customerType;
	}
	public void setCustomerType(byte customerType) {
		this.customerType = customerType;
	}
	public int getLineType() {
		return lineType;
	}
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	/**
	 * @return Returns the activationDate.
	 */
	public String getActivationDate() {
		return activationDate;
	}
	/**
	 * @param activationDate The activationDate to set.
	 */
	public void setActivationDate(String activationDate) {
		this.activationDate = activationDate;
	}
	/**
	 * @return Returns the age.
	 */
	public String getAge() {
		return age;
	}
	/**
	 * @param age The age to set.
	 */
	public void setAge(String age) {
		this.age = age;
	}
	/**
	 * @return Returns the blockChangeDate.
	 */
	public String getBlockChangeDate() {
		return blockChangeDate;
	}
	/**
	 * @param blockChangeDate The blockChangeDate to set.
	 */
	public void setBlockChangeDate(String blockChangeDate) {
		this.blockChangeDate = blockChangeDate;
	}
	/**
	 * @return Returns the blockStatus.
	 */
	public String getBlockStatus() {
		return blockStatus;
	}
	/**
	 * @param blockStatus The blockStatus to set.
	 */
	public void setBlockStatus(String blockStatus) {
		this.blockStatus = blockStatus;
	}
	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}
	/**
	 * @param emailAddress The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	/**
	 * @return Returns the fullName.
	 */
	public String getFullName() {
		return fullName;
	}
	/**
	 * @param fullName The fullName to set.
	 */
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	/**
	 * @return Returns the gender.
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * @param gender The gender to set.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * @return Returns the lastLoginTime.
	 */
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	/**
	 * @param lastLoginTime The lastLoginTime to set.
	 */
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the nationality.
	 */
	public String getNationality() {
		return nationality;
	}
	/**
	 * @param nationality The nationality to set.
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	/**
	 * @return Returns the preferredLanguage.
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}
	/**
	 * @param preferredLanguage The preferredLanguage to set.
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	/**
	 * @return Returns the privilege.
	 */
	public String getPrivilege() {
		return privilege;
	}
	/**
	 * @param privilege The privilege to set.
	 */
	public void setPrivilege(String privilege) {
		this.privilege = privilege;
	}
	/**
	 * @return Returns the registrationDate.
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}
	/**
	 * @param registrationDate The registrationDate to set.
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	/**
	 * @return Returns the skySuspendDate.
	 */
	public String getSkySuspendDate() {
		return skySuspendDate;
	}
	/**
	 * @param skySuspendDate The skySuspendDate to set.
	 */
	public void setSkySuspendDate(String skySuspendDate) {
		this.skySuspendDate = skySuspendDate;
	}
	/**
	 * @return Returns the skyType.
	 */
	public String getSkyType() {
		return skyType;
	}
	/**
	 * @param skyType The skyType to set.
	 */
	public void setSkyType(String skyType) {
		this.skyType = skyType;
	}
	/**
	 * @return Returns the userActivated.
	 */
	public String getUserActivated() {
		return userActivated;
	}
	/**
	 * @param userActivated The userActivated to set.
	 */
	public void setUserActivated(String userActivated) {
		this.userActivated = userActivated;
	}
	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return Returns the usertype.
	 */
	public String getUsertype() {
		return usertype;
	}
	/**
	 * @param usertype The usertype to set.
	 */
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	/**
	 * @return Returns the userType.
	 */
	public String getUserType() {
		return userType;
	}
	/**
	 * @param userType The userType to set.
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}
	/**
	 * @return the securityQuesId
	 */
	public int getSecurityQuesId() {
		return securityQuesId;
	}
	/**
	 * @param securityQuesId the securityQuesId to set
	 */
	public void setSecurityQuesId(int securityQuesId) {
		this.securityQuesId = securityQuesId;
	}
	/**
	 * @return the securityQuesAns
	 */
	public String getSecurityQuesAns() {
		return securityQuesAns;
	}
	/**
	 * @param securityQuesAns the securityQuesAns to set
	 */
	public void setSecurityQuesAns(String securityQuesAns) {
		this.securityQuesAns = securityQuesAns;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getSecurityAnswer() {
		return securityAnswer;
	}
	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
}