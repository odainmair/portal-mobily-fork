/*
 * Created on Feb 15, 2012
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ContactPersonVO extends BaseVO {
	
	
	private String givenName = null;
	private String middleName = null;
	private String thirdName = null;
	private String fourthName = null;
	private String familyName = null;
	private String gender = null;
	private String nationality = null;
	private String dateOfBirth = null;
	private String contactPreference = null;
	private String fullName = null;
	
	private String capEmailAddress = null;
	private String capFaxNumber = null;
	private String capPhoneNumber = null;
	private String capMobileNumber = null;
	
	
	public String getCapEmailAddress() {
		return capEmailAddress;
	}
	public void setCapEmailAddress(String capEmailAddress) {
		this.capEmailAddress = capEmailAddress;
	}
	public String getCapFaxNumber() {
		return capFaxNumber;
	}
	public void setCapFaxNumber(String capFaxNumber) {
		this.capFaxNumber = capFaxNumber;
	}
	public String getCapPhoneNumber() {
		return capPhoneNumber;
	}
	public void setCapPhoneNumber(String capPhoneNumber) {
		this.capPhoneNumber = capPhoneNumber;
	}
	public String getCapMobileNumber() {
		return capMobileNumber;
	}
	public void setCapMobileNumber(String capMobileNumber) {
		this.capMobileNumber = capMobileNumber;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getThirdName() {
		return thirdName;
	}
	public void setThirdName(String thirdName) {
		this.thirdName = thirdName;
	}
	public String getFourthName() {
		return fourthName;
	}
	public void setFourthName(String fourthName) {
		this.fourthName = fourthName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getContactPreference() {
		return contactPreference;
	}
	public void setContactPreference(String contactPreference) {
		this.contactPreference = contactPreference;
	}
}
