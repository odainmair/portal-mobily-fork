package sa.com.mobily.eportal.google.captcha.v2.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;
 public class CaptchaResponseVO extends BaseVO {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4480257808708908837L;
	
	
	private boolean success;
	private String challenge_ts;
	private String hostname;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

	public String getChallenge_ts()
	{
		return challenge_ts;
	}

	public void setChallenge_ts(String challenge_ts)
	{
		this.challenge_ts = challenge_ts;
	}

	public String getHostname()
	{
		return hostname;
	}

	public void setHostname(String hostname)
	{
		this.hostname = hostname;
	}
 }