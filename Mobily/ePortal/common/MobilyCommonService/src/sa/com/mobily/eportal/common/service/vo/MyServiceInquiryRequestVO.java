/*
 * Created on Feb 20, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MyServiceInquiryRequestVO {
    private String msisdn;
    private String requestorUserId;
    private String packageId = "";
    private String customerType = "";
    private boolean promotionInfo = false;
    private boolean internationalInfo = false;
    private boolean wifiServiceInfo = false;
    private boolean roamingCallServiceInfo = false;
    private boolean safeArrivalServiceInfo = false;
    
    
    public static void main(String[] args) {
    }
    /**
     * @return Returns the customerType.
     */
    public String getCustomerType() {
        return customerType;
    }
    /**
     * @param customerType The customerType to set.
     */
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
    /**
     * @return Returns the msisdn.
     */
    public String getMsisdn() {
        return msisdn;
    }
    /**
     * @param msisdn The msisdn to set.
     */
    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
    /**
     * @return Returns the packageId.
     */
    public String getPackageId() {
        return packageId;
    }
    /**
     * @param packageId The packageId to set.
     */
    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }
    /**
     * @return Returns the requestorUserId.
     */
    public String getRequestorUserId() {
        return requestorUserId;
    }
    /**
     * @param requestorUserId The requestorUserId to set.
     */
    public void setRequestorUserId(String requestoruserId) {
        this.requestorUserId = requestoruserId;
    }
	/**
	 * @return the promotionInfo
	 */
	public boolean isPromotionInfo() {
		return promotionInfo;
	}
	/**
	 * @param promotionInfo the promotionInfo to set
	 */
	public void setPromotionInfo(boolean promotionInfo) {
		this.promotionInfo = promotionInfo;
	}
	/**
	 * @return the internationalInfo
	 */
	public boolean isInternationalInfo() {
		return internationalInfo;
	}
	/**
	 * @param internationalInfo the internationalInfo to set
	 */
	public void setInternationalInfo(boolean internationalInfo) {
		this.internationalInfo = internationalInfo;
	}
	/**
	 * @return the wifiServiceInfo
	 */
	public boolean isWifiServiceInfo() {
		return wifiServiceInfo;
	}
	/**
	 * @param wifiServiceInfo the wifiServiceInfo to set
	 */
	public void setWifiServiceInfo(boolean wifiServiceInfo) {
		this.wifiServiceInfo = wifiServiceInfo;
	}
	/**
	 * @return the roamingCallServiceInfo
	 */
	public boolean isRoamingCallServiceInfo() {
		return roamingCallServiceInfo;
	}
	/**
	 * @param roamingCallServiceInfo the roamingCallServiceInfo to set
	 */
	public void setRoamingCallServiceInfo(boolean roamingCallServiceInfo) {
		this.roamingCallServiceInfo = roamingCallServiceInfo;
	}
	/**
	 * @return the safeArrivalServiceInfo
	 */
	public boolean isSafeArrivalServiceInfo() {
		return safeArrivalServiceInfo;
	}
	/**
	 * @param safeArrivalServiceInfo the safeArrivalServiceInfo to set
	 */
	public void setSafeArrivalServiceInfo(boolean safeArrivalServiceInfo) {
		this.safeArrivalServiceInfo = safeArrivalServiceInfo;
	}
	
}