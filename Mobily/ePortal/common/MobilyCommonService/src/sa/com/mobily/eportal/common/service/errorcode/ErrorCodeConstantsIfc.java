package sa.com.mobily.eportal.common.service.errorcode;

public interface ErrorCodeConstantsIfc {
	
	String MOBILE_IQAMA_MISMATCH = "200001";
	String USER_NOT_LOADED = "200002";
	String GENERAL_ERROR = "900999";
	String MOBILE_VERIFICATION_SMS_FAILED = "200003";
	String USER_RELATED_SUBS_NOT_LOADED = "200004";
	String DEFAULT_SUBS_EXISTS = "200005";
	String SUBS_EXISTS = "200006";
}