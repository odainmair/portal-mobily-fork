package sa.com.mobily.eportal.common.service.util.jaxb;

import org.apache.commons.pool.KeyedPoolableObjectFactory;

import org.apache.commons.pool.impl.GenericKeyedObjectPool;
 
 

import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

 
public class MarshalPool extends GenericKeyedObjectPool
{
	private static final String MAX_IDLE = "MarshalPool_MaxIdle";

	private static final String MAX_ACTIVE = "MarshalPool_MaxActive";

	private static final String MAX_WAIT = "MarshalPool_MaxWait";

	private static final String TIME_BETWEEN_EVICTION_RUNS = "MarshalPool_TimeForEvictionRuns";

	private static final String MIN_EVICTION_IDLE_TIME = "MarshalPool_MinEvictableIdleTimeMillis";

	 
	public MarshalPool(final KeyedPoolableObjectFactory factory)
	{
		super(factory);
		
		ResourceEnvironmentProviderService environmentProviderService = ResourceEnvironmentProviderService.getInstance();
		// max no of objects can sit idle in the pool. 3
		this.setMaxIdle(Integer.parseInt((String) environmentProviderService.getPropertyValue(MAX_IDLE)));
		// max no of objects can be created from the same type 5
		this.setMaxActive(Integer.parseInt((String) environmentProviderService.getPropertyValue(MAX_ACTIVE)));
		// how much to wait before throwing NoSuchElementException 5 sec
		this.setMaxWait(Integer.parseInt((String) environmentProviderService.getPropertyValue(MAX_WAIT)));
		// each 3 hours will run the eviction thread
		this.setTimeBetweenEvictionRunsMillis(Integer.parseInt((String) environmentProviderService.getPropertyValue(TIME_BETWEEN_EVICTION_RUNS)));
		// 1 minutes idle time then the object will be eligible for eviction
		this.setMinEvictableIdleTimeMillis(Integer.parseInt((String) environmentProviderService.getPropertyValue(MIN_EVICTION_IDLE_TIME)));

		this.setWhenExhaustedAction(WHEN_EXHAUSTED_BLOCK); // if exhausted wait
		this.setTestOnBorrow(false); // skip validating the objects while borrow
		this.setTestOnReturn(false); // skip validating the objects while return
		this.setTestWhileIdle(false); // skip validating the objects while idle
		this.setLifo(true);
	}

	public MarshalPool(MarshallerFactory objFactory, GenericKeyedObjectPool.Config config)
	{
		super(objFactory, config);
	}

	@Override
	public Object borrowObject(Object key) throws Exception
	{
		return super.borrowObject(key);
	}

	@Override
	public void returnObject(Object key, Object obj) throws Exception
	{
		super.returnObject(key, obj);
	}
}
