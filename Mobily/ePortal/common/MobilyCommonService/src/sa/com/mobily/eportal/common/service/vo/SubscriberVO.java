
package sa.com.mobily.eportal.common.service.vo;

import java.util.List;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SubscriberVO {
	private String msisdn = "";
	private String userName = "";
	private String accountNumber = "";
	private String type = "";
	double currentBalance = 0.0;
	double RGMinutes = 0.0;
	double ConsumedMinutes = 0.0;
	double RemainingMinutes = 0.0;
	double RechargedAmount = 0.0;
	double billedAmount = 0.0;
	double unbilledAmount = 0.0;
	List activityList = null;
	private String email = "";
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List getActivityList() {
		return activityList;
	}
	public void setActivityList(List activityList) {
		this.activityList = activityList;
	}
	public double getRGMinutes() {
		return RGMinutes;
	}
	public void setRGMinutes(double minutes) {
		RGMinutes = minutes;
	}
	public double getConsumedMinutes() {
		return ConsumedMinutes;
	}
	public void setConsumedMinutes(double consumedMinutes) {
		ConsumedMinutes = consumedMinutes;
	}
	public double getRemainingMinutes() {
		return RemainingMinutes;
	}
	public void setRemainingMinutes(double remainingMinutes) {
		RemainingMinutes = remainingMinutes;
	}
	public double getRechargedAmount() {
		return RechargedAmount;
	}
	public void setRechargedAmount(double rechargedAmount) {
		RechargedAmount = rechargedAmount;
	}
	public double getBilledAmount() {
		return billedAmount;
	}
	public void setBilledAmount(double billedAmount) {
		this.billedAmount = billedAmount;
	}
	public double getUnbilledAmount() {
		return unbilledAmount;
	}
	public void setUnbilledAmount(double unbilledAmount) {
		this.unbilledAmount = unbilledAmount;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}
	
	
}
