/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MyServiceInquiryVO {

    private HashMap serviceInfoMap = null;
    private String suppServiceStatus = null;
    private String promotionServiceStatus = null;
    private String internationalServiceStatus = null;
    private String wifiServiceStatus = null;

	private String lineNumber;
    private String requestorUserId;
    private String ServiceName;
    private String errorCode ="0000";
    private int rbt = -1;
    private int tv = -1;
    private int lbs = -1;
    private int gprs = -1;
    private String gprsPackage= "";
    private int internationalBarring = -1;
    private int homeZoneStatus = -1;
    private int ptt=-1;
    private int sms=-1;
    private int mms=-1;
    private int videoTellStatus = -1;
    private int videoStrmStatus = -1;
    private int gamingStatus = -1;
    private int voiceSMSStatus = -1;
    private int ics2Status = -1;
    
    //added for VOIP: soft phone
    private int consumerVOIPStatus = -1;
    private int corpConsumerVOIPStatus = -1;
    
    private int evmsStatus = -1;
    private int bmsStatus = -1;
    private String wifiServiceType = null;
    
    private String planType = "";
    private String subscriptionType = "";
    private String subscriptionUnit="";
    
    private int connectRoaming = -1;
    //private int connectRoamingStatus = -1;
    private String connectRoamingType = "";
    
    private int roamingCallStatus = 1;
    
	private ArrayList msisdnList = null;
	public int serviceEnableStatus = -1;
   

	/**
	 * @return the consumerVOIPStatus
	 */
	public int getConsumerVOIPStatus() {
		return consumerVOIPStatus;
	}

	/**
	 * @param consumerVOIPStatus the consumerVOIPStatus to set
	 */
	public void setConsumerVOIPStatus(int consumerVOIPStatus) {
		this.consumerVOIPStatus = consumerVOIPStatus;
	}

	/**
	 * @return the corpConsumerVOIPStatus
	 */
	public int getCorpConsumerVOIPStatus() {
		return corpConsumerVOIPStatus;
	}

	/**
	 * @param corpConsumerVOIPStatus the corpConsumerVOIPStatus to set
	 */
	public void setCorpConsumerVOIPStatus(int corpConsumerVOIPStatus) {
		this.corpConsumerVOIPStatus = corpConsumerVOIPStatus;
	}

	public String getConnectRoamingType() {
		return connectRoamingType;
	}

	public void setConnectRoamingType(String connectRoamingType) {
		this.connectRoamingType = connectRoamingType;
	}

	public int getConnectRoaming() {
		return connectRoaming;
	}

	public void setConnectRoaming(int connectRoaming) {
		this.connectRoaming = connectRoaming;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public String getSubscriptionUnit() {
		return subscriptionUnit;
	}

	public void setSubscriptionUnit(String subscriptionUnit) {
		this.subscriptionUnit = subscriptionUnit;
	}

//	public int getConnectRoamingStatus() {
//		return connectRoamingStatus;
//	}
//
//	public void setConnectRoamingStatus(int connectRoamingStatus) {
//		this.connectRoamingStatus = connectRoamingStatus;
//	}

    
    private java.util.ArrayList listOfSubscribedPromotion = null;
    private java.util.ArrayList listOfNotSubscribedpromotion= null;
    
    private int nationalsms=-1;


    public void reset(){
        rbt = 0;
        tv = 0;
        lbs = 0;
        gprs = 0;
        internationalBarring = 0;
        homeZoneStatus = 0;
        ptt=0;
        sms=0;
        mms=0;
        videoTellStatus = 0;
        videoStrmStatus = 0;
        gamingStatus = 0;
        voiceSMSStatus = 0;
        ics2Status = 0;
        evmsStatus = 0;
        bmsStatus = 0;
    }
    
    public int getNationalsms() {
		return nationalsms;
	}

	public void setNationalsms(int nationalsms) {
		this.nationalsms = nationalsms;
	}

	public static void main(String[] args) {
    }
    /**
     * @return Returns the lineNumber.
     */
    public String getLineNumber() {
        return lineNumber;
    }
    /**
     * @param lineNumber The lineNumber to set.
     */
    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }
    /**
     * @return Returns the requestorUserId.
     */
    public String getRequestorUserId() {
        return requestorUserId;
    }
    /**
     * @param requestorUserId The requestorUserId to set.
     */
    public void setRequestorUserId(String requestorUserId) {
        this.requestorUserId = requestorUserId;
    }
    /**
     * @return Returns the gamingStatus.
     */
    public int getGamingStatus() {
        return gamingStatus;
    }
    /**
     * @param gamingStatus The gamingStatus to set.
     */
    public void setGamingStatus(int gamingStatus) {
        this.gamingStatus = gamingStatus;
    }
    /**
     * @return Returns the gprs.
     */
    public int getGprs() {
        return gprs;
    }
    /**
     * @param gprs The gprs to set.
     */
    public void setGprs(int gprs) {
        this.gprs = gprs;
    }
    /**
     * @return Returns the homeZoneStatus.
     */
    public int getHomeZoneStatus() {
        return homeZoneStatus;
    }
    /**
     * @param homeZoneStatus The homeZoneStatus to set.
     */
    public void setHomeZoneStatus(int homeZoneStatus) {
        this.homeZoneStatus = homeZoneStatus;
    }
    /**
     * @return Returns the internationalBarring.
     */
    public int getInternationalBarring() {
        return internationalBarring;
    }
    /**
     * @param internationalBarring The internationalBarring to set.
     */
    public void setInternationalBarring(int internationalBarring) {
        this.internationalBarring = internationalBarring;
    }
    /**
     * @return Returns the lbs.
     */
    public int getLbs() {
        return lbs;
    }
    /**
     * @param lbs The lbs to set.
     */
    public void setLbs(int lbs) {
        this.lbs = lbs;
    }
    /**
     * @return Returns the listOfNotSubscribedpromotion.
     */
    public java.util.ArrayList getListOfNotSubscribedpromotion() {
        return listOfNotSubscribedpromotion;
    }
    /**
     * @param listOfNotSubscribedpromotion The listOfNotSubscribedpromotion to set.
     */
    public void setListOfNotSubscribedpromotion(
            java.util.ArrayList listOfNotSubscribedpromotion) {
        this.listOfNotSubscribedpromotion = listOfNotSubscribedpromotion;
    }
    /**
     * @return Returns the listOfSubscribedPromotion.
     */
    public java.util.ArrayList getListOfSubscribedPromotion() {
        return listOfSubscribedPromotion;
    }
    /**
     * @param listOfSubscribedPromotion The listOfSubscribedPromotion to set.
     */
    public void setListOfSubscribedPromotion(
            java.util.ArrayList listOfSubscribedPromotion) {
        this.listOfSubscribedPromotion = listOfSubscribedPromotion;
    }
    /**
     * @return Returns the ptt.
     */
    public int getPtt() {
        return ptt;
    }
    /**
     * @param ptt The ptt to set.
     */
    public void setPtt(int ptt) {
        this.ptt = ptt;
    }
    /**
     * @return Returns the rbt.
     */
    public int getRbt() {
        return rbt;
    }
    /**
     * @param rbt The rbt to set.
     */
    public void setRbt(int rbt) {
        this.rbt = rbt;
    }
    /**
     * @return Returns the sms.
     */
    public int getSms() {
        return sms;
    }
    /**
     * @param sms The sms to set.
     */
    public void setSms(int sms) {
        this.sms = sms;
    }
    /**
     * @return Returns the tv.
     */
    public int getTv() {
        return tv;
    }
    /**
     * @param tv The tv to set.
     */
    public void setTv(int tv) {
        this.tv = tv;
    }
    /**
     * @return Returns the videoStrmStatus.
     */
    public int getVideoStrmStatus() {
        return videoStrmStatus;
    }
    /**
     * @param videoStrmStatus The videoStrmStatus to set.
     */
    public void setVideoStrmStatus(int videoStrmStatus) {
        this.videoStrmStatus = videoStrmStatus;
    }
    /**
     * @return Returns the videoTellStatus.
     */
    public int getVideoTellStatus() {
        return videoTellStatus;
    }
    /**
     * @param videoTellStatus The videoTellStatus to set.
     */
    public void setVideoTellStatus(int videoTellStatus) {
        this.videoTellStatus = videoTellStatus;
    }
    /**
     * @return Returns the voiceSMSStatus.
     */
    public int getVoiceSMSStatus() {
        return voiceSMSStatus;
    }
    /**
     * @param voiceSMSStatus The voiceSMSStatus to set.
     */
    public void setVoiceSMSStatus(int voiceSMSStatus) {
        this.voiceSMSStatus = voiceSMSStatus;
    }
    /**
     * @return Returns the errorCode.
     */
    public String getErrorCode() {
        return errorCode;
    }
    /**
     * @param errorCode The errorCode to set.
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * @return Returns the gprsPackage.
     */
    public String getGprsPackage() {
        return gprsPackage;
    }
    /**
     * @param gprsPackage The gprsPackage to set.
     */
    public void setGprsPackage(String gprsPackage) {
        this.gprsPackage = gprsPackage;
    }    
	/**
	 * @return Returns the ics2Status.
	 */
	public int getIcs2Status() {
		return ics2Status;
	}
	/**
	 * @param ics2Status The ics2Status to set.
	 */
	public void setIcs2Status(int ics2Status) {
		this.ics2Status = ics2Status;
	}
	
	
	public int getEvmsStatus() {
		return evmsStatus;
	}
	public void setEvmsStatus(int evmsStatus) {
		this.evmsStatus = evmsStatus;
	}
	
	

	public int getMms() {
		return mms;
	}
	public void setMms(int mms) {
		this.mms = mms;
	}
	
	public int getBmsStatus() {
		return bmsStatus;
	}
	public void setBmsStatus(int bmsStatus) {
		this.bmsStatus = bmsStatus;
	}

	public String getWifiServiceType() {
		return wifiServiceType;
	}

	public void setWifiServiceType(String wifiServiceType) {
		this.wifiServiceType = wifiServiceType;
	}

	public String getServiceName() {
		return ServiceName;
	}

	public void setServiceName(String serviceName) {
		ServiceName = serviceName;
	}

	/**
	 * @return the serviceInfoMap
	 */
	public HashMap getServiceInfoMap() {
		return serviceInfoMap;
	}

	/**
	 * @param serviceInfoMap the serviceInfoMap to set
	 */
	public void setServiceInfoMap(HashMap serviceInfoMap) {
		this.serviceInfoMap = serviceInfoMap;
	}

	/**
	 * @return the suppServiceStatus
	 */
	public String getSuppServiceStatus() {
		return suppServiceStatus;
	}

	/**
	 * @param suppServiceStatus the suppServiceStatus to set
	 */
	public void setSuppServiceStatus(String suppServiceStatus) {
		this.suppServiceStatus = suppServiceStatus;
	}

	/**
	 * @return the promotionServiceStatus
	 */
	public String getPromotionServiceStatus() {
		return promotionServiceStatus;
	}

	/**
	 * @param promotionServiceStatus the promotionServiceStatus to set
	 */
	public void setPromotionServiceStatus(String promotionServiceStatus) {
		this.promotionServiceStatus = promotionServiceStatus;
	}

	/**
	 * @return the internationalServiceStatus
	 */
	public String getInternationalServiceStatus() {
		return internationalServiceStatus;
	}

	/**
	 * @param internationalServiceStatus the internationalServiceStatus to set
	 */
	public void setInternationalServiceStatus(String internationalServiceStatus) {
		this.internationalServiceStatus = internationalServiceStatus;
	}

	/**
	 * @return the wifiServiceStatus
	 */
	public String getWifiServiceStatus() {
		return wifiServiceStatus;
	}

	/**
	 * @param wifiServiceStatus the wifiServiceStatus to set
	 */
	public void setWifiServiceStatus(String wifiServiceStatus) {
		this.wifiServiceStatus = wifiServiceStatus;
	}

	/**
	 * @return the roamingCallStatus
	 */
	public int getRoamingCallStatus() {
		return roamingCallStatus;
	}

	/**
	 * @param roamingCallStatus the roamingCallStatus to set
	 */
	public void setRoamingCallStatus(int roamingCallStatus) {
		this.roamingCallStatus = roamingCallStatus;
	}

	/**
	 * @return the msisdnList
	 */
	public ArrayList getMsisdnList() {
		return msisdnList;
	}

	/**
	 * @param msisdnList the msisdnList to set
	 */
	public void setMsisdnList(ArrayList msisdnList) {
		this.msisdnList = msisdnList;
	}

	/**
	 * @return the serviceEnableStatus
	 */
	public int getServiceEnableStatus() {
		return serviceEnableStatus;
	}

	/**
	 * @param serviceEnableStatus the serviceEnableStatus to set
	 */
	public void setServiceEnableStatus(int serviceEnableStatus) {
		this.serviceEnableStatus = serviceEnableStatus;
	}
	
	
}