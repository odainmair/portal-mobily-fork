package sa.com.mobily.eportal.billing.xml;

import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.BillingConstantsIfc;
import sa.com.mobily.eportal.billing.vo.LastBillReplyVO;
import sa.com.mobily.eportal.billing.vo.LastBillRequestVO;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;

/**
 * 
 * @author r.agarwal.mit
 *
 */

public class GetLastBillsXMLHandler {

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String clazz = GetLastBillsXMLHandler.class.getName();
	
	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Consumer Profile
	 * @param requestVO
	 * @return xmlRequest
	 */
	
	public String generateLastBillsReqXML(LastBillRequestVO requestVO) throws Exception {
		   
	    String xmlRequest = "";
	   
	    if (requestVO == null ) {
            throw new MobilyCommonException(BillingConstantsIfc.INVALID_REQUEST);
        }

		executionContext.audit(Level.INFO, "Started for account::"+requestVO.getServiceAccountNumber(), clazz, "generateLastBillsReqXML");

        try {
    		xmlRequest = JAXBUtilities.getInstance().marshal(requestVO);
    		
        } catch (Exception e) {
        	executionContext.log(Level.SEVERE, "Exception while generating last bills xml::"+e.getMessage(), clazz, "generateLastBillsReqXML");
            throw new SystemException(e);
        }
        executionContext.audit(Level.INFO, "Ended for account::"+requestVO.getServiceAccountNumber(), clazz, "generateLastBillsReqXML");
        
        return xmlRequest;
	}
	
	
	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Consumer profile XML
	 * @param replyMessage
	 * @return LastBillReplyVO
	 */
	public LastBillReplyVO parseLastBillsReplyXML(String replyMessage) throws MobilyCommonException
	{
		executionContext.audit(Level.INFO, "parseLastBillsReplyXML > Started", clazz, "parseLastBillsReplyXML");
		LastBillReplyVO replyVO = null;
		try
		{
			replyVO = (LastBillReplyVO) JAXBUtilities.getInstance().unmarshal(LastBillReplyVO.class, replyMessage);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing last bills xml::" + e.getMessage(), clazz, "parseLastBillsReplyXML");
			throw new SystemException(e);
		}
		return replyVO;
	}
}
