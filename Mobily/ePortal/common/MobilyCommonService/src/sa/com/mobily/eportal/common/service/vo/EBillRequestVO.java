/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EBillRequestVO {
	private EBillRequestHeaderVO header = null;
	private String lineNumber = null;

	/**
	 * @return Returns the header.
	 */
	public EBillRequestHeaderVO getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(EBillRequestHeaderVO header) {
		this.header = header;
	}
	/**
	 * @return Returns the lineNumber.
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber The lineNumber to set.
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
}
