package sa.com.mobily.eportal.customerinfo.jaxb.favnumber.reply;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;any processContents='lax' namespace='##other'/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "productOrderItem"
})
@XmlRootElement(name = "CustomerOrder")
public class CustomerOrder {

	@XmlElement(name = "ProductOrderItem", required = true)
    protected ProductOrderItem productOrderItem;

	public ProductOrderItem getProductOrderItem()
	{
		return productOrderItem;
	}

	public void setProductOrderItem(ProductOrderItem productOrderItem)
	{
		this.productOrderItem = productOrderItem;
	}

    

}
