package sa.com.mobily.eportal.common.service.model;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * The primary key class for the SR_USER_SESSIONVO_TBL database table.
 * 
 */
public class UserSessionPK extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private String sessionid;
	
	private String username;

    public UserSessionPK() {
    }
	public String getSessionid() {
		return this.sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserSessionPK)) {
			return false;
		}
		UserSessionPK castOther = (UserSessionPK)other;
		return 
			this.sessionid.equals(castOther.sessionid)
			&& this.username.equals(castOther.username);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.sessionid.hashCode();
		hash = hash * prime + this.username.hashCode();
		
		return hash;
    }
}