package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.AccountHierarchyApprovals;

public class AccountHierarchyApprovalsDAO extends AbstractDBDAO<AccountHierarchyApprovals> {

	private static AccountHierarchyApprovalsDAO instance = new AccountHierarchyApprovalsDAO();

	private static final String GET_ACCOUNT_HIERARCHY_APPROVALS = "SELECT ID, ACCOUNT_HIERARCHY_ID, USER_DATA_DATE, SIM_DATA_DATE, EPORTAL_USER_NAME, NEW_USER_NAME, NEW_USER_DOC_TYPE, NEW_USER_DOC_ID, NEW_ID_EXPORY_DATE, SIM_SAWP_APPROVED FROM ACCOUNT_HIERARCHY_APPROVALS WHERE ACCOUNT_HIERARCHY_ID = ?";
	private static final String UPDATE_USER_DETAILS_APPROVAL = "UPDATE ACCOUNT_HIERARCHY_APPROVALS SET EPORTAL_USER_NAME = ?, NEW_USER_NAME = ?, NEW_USER_DOC_TYPE = ?, NEW_USER_DOC_ID = ?, NEW_ID_EXPORY_DATE = ?, USER_DATA_DATE = SYSDATE WHERE ID = ?";
	private static final String INSERT_USER_DETAILS_APPROVAL = "Insert INTO ACCOUNT_HIERARCHY_APPROVALS (ACCOUNT_HIERARCHY_ID, EPORTAL_USER_NAME, NEW_USER_NAME, NEW_USER_DOC_TYPE, NEW_USER_DOC_ID, NEW_ID_EXPORY_DATE, USER_DATA_DATE) values (?,?,?,?,?,?,SYSDATE)";
	private static final String INSERT_SIM_SWAP_DETAILS = "Insert INTO ACCOUNT_HIERARCHY_APPROVALS (ACCOUNT_HIERARCHY_ID, EPORTAL_USER_NAME,SIM_SAWP_APPROVED,SIM_DATA_DATE) values (?,?,?,SYSDATE)";
	private static final String UPDATE_SIM_SWAP_DETAILS = "UPDATE ACCOUNT_HIERARCHY_APPROVALS SET EPORTAL_USER_NAME=? ,SIM_SAWP_APPROVED=?, SIM_DATA_DATE=SYSDATE WHERE ID=?"; 
		
	protected AccountHierarchyApprovalsDAO() {
		super(DataSources.DEFAULT);
	}

	public static AccountHierarchyApprovalsDAO getInstance() {
		return instance;
	}
	
	public List<AccountHierarchyApprovals> getAccountHierarchyApprovals(String AccountHierarchyId) {
		
		List<AccountHierarchyApprovals> lstAccountHierarchyApprovals = query(GET_ACCOUNT_HIERARCHY_APPROVALS, AccountHierarchyId);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchyApprovals)) {
			return lstAccountHierarchyApprovals;
		}
		return null;
	}
	
	public int saveApprovalUserDetails(AccountHierarchyApprovals approvals) {
		List<AccountHierarchyApprovals> lstApprovals = getAccountHierarchyApprovals(approvals.getACCOUNT_HIERARCHY_ID());
		boolean isExist = false;
		int approvalsId = -1; 
		if (CollectionUtils.isNotEmpty(lstApprovals)) {
			for (int i = 0; i < lstApprovals.size(); i++) {
				if (lstApprovals.get(i).getUSER_DATA_DATE() != null) {
					isExist = true;
					approvalsId = lstApprovals.get(i).getID();
					if(FormatterUtility.isEmpty(approvals.getNEW_USER_NAME())) {
						approvals.setNEW_USER_NAME(lstApprovals.get(i).getNEW_USER_NAME());
					}
					if(approvals.getNEW_USER_DOC_TYPE() < 1) {
						approvals.setNEW_USER_DOC_TYPE(lstApprovals.get(i).getNEW_USER_DOC_TYPE());
					}
					if(FormatterUtility.isEmpty(approvals.getNEW_USER_DOC_ID())) {
						approvals.setNEW_USER_DOC_ID(lstApprovals.get(i).getNEW_USER_DOC_ID());
					}
					if(approvals.getNEW_ID_EXPORY_DATE() == null) {
						approvals.setNEW_ID_EXPORY_DATE(lstApprovals.get(i).getNEW_ID_EXPORY_DATE());
					}
					break;
				}
			}
		}

		if (isExist) {
			return update(UPDATE_USER_DETAILS_APPROVAL, approvals.getEPORTAL_USER_NAME(), approvals.getNEW_USER_NAME(), (approvals.getNEW_USER_DOC_TYPE() < 1) ? null : approvals.getNEW_USER_DOC_TYPE(), approvals.getNEW_USER_DOC_ID(), 
					approvals.getNEW_ID_EXPORY_DATE(), approvalsId);
		} else {
			return update(INSERT_USER_DETAILS_APPROVAL, approvals.getACCOUNT_HIERARCHY_ID(), approvals.getEPORTAL_USER_NAME(), approvals.getNEW_USER_NAME(), (approvals.getNEW_USER_DOC_TYPE() < 1) ? null : approvals.getNEW_USER_DOC_TYPE(), approvals.getNEW_USER_DOC_ID(), 
					approvals.getNEW_ID_EXPORY_DATE());
		}
	}
	
	public int saveSIMSwapDetails(AccountHierarchyApprovals approvals) {
		List<AccountHierarchyApprovals> lstApprovals = getAccountHierarchyApprovals(approvals.getACCOUNT_HIERARCHY_ID());
		boolean isExist=false;
		int accountHierarchyId=-1;
		if(lstApprovals!=null && lstApprovals.size()>0){
			for (AccountHierarchyApprovals accountHierarchyApprovals : lstApprovals)
			{
				if(FormatterUtility.isNotEmpty(accountHierarchyApprovals.getSIM_SAWP_APPROVED())){
					isExist= true;
					accountHierarchyId=accountHierarchyApprovals.getID();
				}
			}
		}
		if(isExist){
			return update(UPDATE_SIM_SWAP_DETAILS,approvals.getEPORTAL_USER_NAME(),approvals.getSIM_SAWP_APPROVED(),accountHierarchyId);
		}else {
			return update(INSERT_SIM_SWAP_DETAILS,approvals.getACCOUNT_HIERARCHY_ID(),approvals.getEPORTAL_USER_NAME(),approvals.getSIM_SAWP_APPROVED());
		}
	}
	
	@Override
	protected AccountHierarchyApprovals mapDTO(ResultSet rs) throws SQLException {
		AccountHierarchyApprovals hierarchyApprovalsVO = new AccountHierarchyApprovals();
		hierarchyApprovalsVO.setID(rs.getInt("ID"));
		hierarchyApprovalsVO.setACCOUNT_HIERARCHY_ID(rs.getString("ACCOUNT_HIERARCHY_ID"));
		hierarchyApprovalsVO.setUSER_DATA_DATE(rs.getTimestamp("USER_DATA_DATE"));
		hierarchyApprovalsVO.setSIM_DATA_DATE(rs.getTimestamp("SIM_DATA_DATE"));
		hierarchyApprovalsVO.setEPORTAL_USER_NAME(rs.getString("EPORTAL_USER_NAME"));
		hierarchyApprovalsVO.setNEW_USER_NAME(rs.getString("NEW_USER_NAME"));
		hierarchyApprovalsVO.setNEW_USER_DOC_TYPE(rs.getInt("NEW_USER_DOC_TYPE"));
		hierarchyApprovalsVO.setNEW_USER_DOC_ID(rs.getString("NEW_USER_DOC_ID"));
		hierarchyApprovalsVO.setNEW_ID_EXPORY_DATE(rs.getDate("NEW_ID_EXPORY_DATE"));
		hierarchyApprovalsVO.setSIM_SAWP_APPROVED(rs.getString("SIM_SAWP_APPROVED"));

		return hierarchyApprovalsVO;
	}
}
