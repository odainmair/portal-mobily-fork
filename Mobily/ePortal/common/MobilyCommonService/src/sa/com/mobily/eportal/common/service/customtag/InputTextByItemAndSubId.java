package sa.com.mobily.eportal.common.service.customtag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.LookupTableUtility;
import sa.com.mobily.eportal.common.service.vo.LookupTableVO;

public class InputTextByItemAndSubId extends TagSupport
{
	private String id = "";//

	private String type = "";//

	private String locale = null;//

	private String name = "";//

	private String style = "";//

	private String subId = "";//

	private String script = "";//

	private String objectId = "";

	private String readOnly = "";

	public int doStartTag() throws JspException
	{
		try
		{
			JspWriter out = pageContext.getOut();
			LookupTableVO lookupTableVO = (LookupTableVO) LookupTableUtility.getInstance().getLookupDataByIdAndSubId(id, subId);// etLookupDataById(id);
			StringBuffer strBuffer = new StringBuffer();
			if (lookupTableVO != null)
			{
				String tagText = "";
				if (FormatterUtility.isEmpty(objectId))
				{
					objectId = "";
				}
				else
				{
					objectId = " id =" + objectId;
				}

				if (FormatterUtility.isEmpty(readOnly))
				{
					readOnly = "";
				}
				else
				{
					readOnly = " readonly =" + readOnly;
				}
				String value = "";
				if ("en".equalsIgnoreCase(locale))
					value = lookupTableVO.getItemDesc_en();
				else
					value = lookupTableVO.getItemDesc_ar();
				if (FormatterUtility.isEmpty(type))
					tagText = value;
				else
				{
					tagText = "<input type='" + type + "' value='" + value + "'  name='" + name + "' id='" + objectId + "' " + readOnly + " class='" + style + "' " + script + " >";
				}

				strBuffer.append(tagText);
				out.println(strBuffer.toString());
				return SKIP_BODY;
			}
		}
		catch (SystemException e)
		{
			throw new SystemException(e);
		}
		catch (IOException e)
		{
			throw new SystemException(e);
		}

		return super.doStartTag();

	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getLocale()
	{
		return locale;
	}

	public void setLocale(String locale)
	{
		this.locale = locale;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getStyle()
	{
		return style;
	}

	public void setStyle(String style)
	{
		this.style = style;
	}

	public String getSubId()
	{
		return subId;
	}

	public void setSubId(String subId)
	{
		this.subId = subId;
	}

	public String getScript()
	{
		return script;
	}

	public void setScript(String script)
	{
		this.script = script;
	}

	public String getObjectId()
	{
		return objectId;
	}

	public void setObjectId(String objectId)
	{
		this.objectId = objectId;
	}

	public String getReadOnly()
	{
		return readOnly;
	}

	public void setReadOnly(String readOnly)
	{
		this.readOnly = readOnly;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}
}