package sa.com.mobily.eportal.common.service.vo;

public class AddOnVO {
  
	String addOnId;
	String addOnName;
	String minTierVal;
	String maxTierVal;
	Float rate;
	Float MonthlyFee;
	int payType;
	
	
	public String getAddOnId() {
		return addOnId;
	}
	public void setAddOnId(String addOnId) {
		this.addOnId = addOnId;
	}
	public String getAddOnName() {
		return addOnName;
	}
	public void setAddOnName(String addOnName) {
		this.addOnName = addOnName;
	}
	public String getMinTierVal() {
		return minTierVal;
	}
	public void setMinTierVal(String minTierVal) {
		this.minTierVal = minTierVal;
	}
	public String getMaxTierVal() {
		return maxTierVal;
	}
	public void setMaxTierVal(String maxTierVal) {
		this.maxTierVal = maxTierVal;
	}
	public Float getRate() {
		return rate;
	}
	public void setRate(Float rate) {
		this.rate = rate;
	}
	public Float getMonthlyFee() {
		return MonthlyFee;
	}
	public void setMonthlyFee(Float monthlyFee) {
		MonthlyFee = monthlyFee;
	}
	public int getPayType() {
		return payType;
	}
	public void setPayType(int payType) {
		this.payType = payType;
	}
	
	
}
