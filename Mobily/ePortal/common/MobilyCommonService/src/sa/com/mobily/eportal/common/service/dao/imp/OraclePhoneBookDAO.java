/*
 * Created on Apr 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.PhoneBookDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.PhoneBookContactVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookGroupVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookListVO;

/**
 * 
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OraclePhoneBookDAO implements PhoneBookDAO{
	
	private static final Logger log = LoggerInterface.log;

	/**
	 * Resopnsible for create new phone Book contact 
	 * @param contactVO
	 * 		- CONTACT_ID  
	 * 		- OWNER_MSISDN  
	 * 		- CONTACT_MSISDN
	 *  	- CONTACT_FULL_NAME
	 * 		- CONTACT_NICK_NAME
	 * 		- CONTACT_EMAIL_ADDRESS
	 * 		- FUNCTION_ID
	 * @throws MobilyCommonException
	 */
	public void createPhoneBookContact(PhoneBookContactVO contactVO) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > createPhoneBookContact > Called ");

		Connection connection   = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_MEMBER ( CONTACT_ID  , OWNER_MSISDN  , CONTACT_MSISDN  ,");
	   	  sqlStatament.append(" CONTACT_FULL_NAME  , CONTACT_NICK_NAME ,CONTACT_EMAIL_ADDRESS ,FUNCTION_ID  ) values ");
	   	  sqlStatament.append(" ( (SEQ_PB_CONTACT_ID.NEXTVAL), ?,?,?,?,?,?)");

	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setString(1 , contactVO.getMSISDN());
	      pstm.setString(2 , contactVO.getContactMobileNumber());
	      pstm.setString(3 , contactVO.getFullName());
	      pstm.setString(4 , contactVO.getNickName());
	      pstm.setString(5 , contactVO.getEmailAddress());
	      pstm.setInt   (6 , contactVO.getFunctionId());
	      
	      int updateedItem  = pstm.executeUpdate();
	      log.debug("OraclePhoneBookDAO > createPhoneBookContact > Add New Contact into PHONE BOOK WHERE MSISDN = "+contactVO.getMSISDN());
	   }catch(SQLException e){
	   		MobilyCommonException ex = new MobilyCommonException(e.getMessage());
	   		String message = e.getMessage();
		   	if(message.startsWith("ORA-00001")){
		   		ex.setErrorCode(ConstantIfc.PHONE_BOOK_ERROR_CONTACT_ALREADY_EXIST);
		   	}
	   	  log.fatal("OraclePhoneBookDAO > createPhoneBookContact > SQLException > "+e.getMessage());
	   	  throw ex;
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > createPhoneBookContact >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookContact >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	}
	
	/**
	  * Resopnsible for create new phone Book Group 
	 * * @param groupVO
	 * 		- GROUP_ID  
	 * 		- DISTRIBUTION_LIST_ID  
	 * 		- FUNCTION_ID
	 *  	- MSISDN
	 * 		- GROUP_NAME
	 * @throws MobilyCommonException
	 */
	public void createPhoneBookGroup(PhoneBookGroupVO groupVO) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > createPhoneBookGroup > Called ");

		Connection connection   = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_GROUP ( GROUP_ID  , DISTRIBUTION_LIST_ID  , FUNCTION_ID  ,");
	   	  sqlStatament.append(" MSISDN  , GROUP_NAME ) values ");
	   	  sqlStatament.append(" ( (SEQ_PBGROUP_ID.NEXTVAL), ?,?,?,?)");

	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setInt(1 , groupVO.getDistributionListId());
	      pstm.setInt(2 , groupVO.getFunctionId());
	      pstm.setString(3 , groupVO.getMSISDN());
	      pstm.setString(4 , groupVO.getGroupName());
	      
	      int updateedItem  = pstm.executeUpdate();
	      log.debug("OraclePhoneBookDAO > createPhoneBookGroup > New Group ["+groupVO.getGroupName()+"] add for Account  = "+groupVO.getMSISDN());
	   }catch(SQLException e){
		   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
	   		String message = e.getMessage();
		   	if(message.startsWith("ORA-00001")){
		   		ex.setErrorCode(ConstantIfc.PHONE_BOOK_ERROR_GROUP_ALREADY_EXIST);
		   	}
		   	log.fatal("OraclePhoneBookDAO > createPhoneBookGroup > SQLException > "+e.getMessage());
		   	throw ex;
	   	  
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	}
	
	/**
	 * Responsible for Delete Phone Book group from local DB , and it will affect the memebr of the group
	 * for example if the customer decide to delete group (1) from DB then the group memeber will not be a memeber of this group any more
	 * @param group_id
	 * @throws MobilyCommonException
	 */
	public void DeletePhoneBookGroup(int group_id) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > DeletePhoneBookGroup > Called ");

		Connection connection   = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" DELETE FROM  PHONE_BOOK_GROUP WHERE  GROUP_ID = ? ");
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setInt(1 , group_id);
	      
	      int updateedItem  = pstm.executeUpdate();
	      log.debug("OraclePhoneBookDAO > DeletePhoneBookGroup > Delete  Group ["+group_id+"]");
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > DeletePhoneBookGroup > SQLException > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > DeletePhoneBookGroup >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	}

	
	/**
	 * responsible fpr delete phone book group memeber
	 * @param groupVO
	 * 		- group id
	 *  	- list of contact we want to remove
	 * @throws MobilyCommonException
	 */
	public void DeletePhoneBookGroupMemeber(ArrayList listOfContactId , int groupId) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > DeletePhoneBookGroupMemeber > Called ");

		Connection connection   = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" DELETE FROM  PHONE_BOOK_GROUP_MEMBER WHERE  GROUP_ID = ? and contact_id in ( ");
	   	  String dele = "";
	   	  for(int i =0 ; i < listOfContactId.size(); i++) {
	   	  	sqlStatament.append(dele + (String)listOfContactId.get(i));
	   	  	dele = ",";
	   	  }
	   	  sqlStatament.append(" )");
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setInt(1 , groupId);
	      int updateedItem  = pstm.executeUpdate();
	      log.debug("OraclePhoneBookDAO > DeletePhoneBookGroupMemeber > Done");
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > DeletePhoneBookGroupMemeber > SQLException > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > DeletePhoneBookGroupMemeber >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	}

	/**
	 * Responsible for list all application group for the customer , for example list all Voice SMS group created by the customer
	 * @param groupVO
	 * 			- MSISDN
	 * 			- function ID
	 * @throws MobilyCommonException
	 * @return ArrayList of PhoneBookGroupVO object
	 */
	public ArrayList listCustomerPhoneBookGroup(String msisdn , int functionId) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > listCustomerPhoneBookGroup > Called ");
		java.util.ArrayList listOfGroups = null;
		Connection connection   = null;
	    PreparedStatement pstm  = null;
	    ResultSet resultSet = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" Select * from PHONE_BOOK_GROUP WHERE  MSISDN = ? and FUNCTION_ID = ? ");
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setString(1 , msisdn);
	      pstm.setInt(2 , functionId);
	      PhoneBookGroupVO groubObj = null;
	      resultSet = pstm.executeQuery();
	      listOfGroups = new ArrayList();
	      while(resultSet.next()) {
	      	groubObj = new PhoneBookGroupVO();
	      	groubObj.setGroupId(resultSet.getInt("GROUP_ID"));
	      	groubObj.setDistributionListId(resultSet.getInt("DISTRIBUTION_LIST_ID"));
	      	groubObj.setMSISDN(resultSet.getString("MSISDN"));
	      	groubObj.setGroupName(resultSet.getString("GROUP_NAME"));
	      	groubObj.setFunctionId(resultSet.getInt("FUNCTION_ID"));
	      	listOfGroups.add(groubObj);
	      }
	      log.debug("OraclePhoneBookDAO > listCustomerPhoneBookGroup > List all Group for the customer ["+msisdn+"] = ");
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > listCustomerPhoneBookGroup > SQLException > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > listCustomerPhoneBookGroup >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  if(resultSet != null){
	   	     resultSet.close();
	   	     resultSet = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	   return listOfGroups;
	}

	/**
	 * Responsible for list all Customer Contact 
	 * @param groupVO
	 * 			- MSISDN   [Mandatory]
	 * 			- function ID [not Manadatory = 0]
	 * @throws MobilyCommonException
	 * @return ArrayList of PhoneBookGroupVO object
	 */
	public ArrayList listCustomerPhoneBookMember(String msisdn , int functionId) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > listCustomerPhoneBookMember > Called ");
		java.util.ArrayList listOfContact = null;
		Connection connection   = null;
	    PreparedStatement pstm  = null;
	    ResultSet resultSet = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" Select * from PHONE_BOOK_MEMBER WHERE  owner_msisdn = ? ");
	   	  if(functionId != Integer.parseInt(ConstantIfc.PHONE_BOOK_FUNCTION_UNDEFINE)) {
	   	     sqlStatament.append(" AND FUNCTION_ID = "+functionId);	
	   	  }
	   	  
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setString(1 , msisdn);
	      resultSet = pstm.executeQuery();
	      listOfContact = new ArrayList();
	      PhoneBookContactVO contactObj = null;
	      while(resultSet.next()) {
	      	contactObj = new PhoneBookContactVO();
	      	contactObj.setContactId(resultSet.getInt("contact_id"));
	      	contactObj.setMSISDN(resultSet.getString("owner_msisdn"));
	      	contactObj.setContactMobileNumber(resultSet.getString("contact_msisdn"));
	      	contactObj.setFullName(resultSet.getString("contact_full_name"));
	      	contactObj.setNickName(resultSet.getString("contact_nick_name"));
	      	contactObj.setEmailAddress(resultSet.getString("contact_email_address"));
	      	contactObj.setFunctionId(resultSet.getInt("function_id"));
	      	listOfContact.add(contactObj);

	      }
	      log.debug("OraclePhoneBookDAO > listCustomerPhoneBookMember > List all Contact for MSISDN ["+msisdn+"] = ");
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > listCustomerPhoneBookMember > SQLException > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > listCustomerPhoneBookMember >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  if(resultSet != null){
	   	     resultSet.close();
	   	     resultSet = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	   return listOfContact;
	}
	
	
	/**
	 * List Customer contact and groups
	 * @param msisdn
	 * @param functionId
	 * @return
	 * @throws MobilyCommonException
	 */
	public PhoneBookListVO listPhoneBook(String msisdn , int functionId) throws MobilyCommonException{
		PhoneBookListVO objVO = new PhoneBookListVO();
		try {
			
		  ArrayList contactList = listCustomerPhoneBookMember(msisdn , functionId);
		  objVO.setContactList(contactList);
		  ArrayList groupList = listCustomerPhoneBookGroup(msisdn , functionId);
		  objVO.setGroupList(groupList);
		}catch(MobilyCommonException e) {
			throw e;
		}
		return objVO;
	
	}
	/**
	 * Respnisble for list all contact memebr in specific group
	 * @param groupVO
	 * 			- group_ID
	 * @throws MobilyCommonException
	 * @return PhoneBookGroupVO object which contact list of PhoneBookContactVO objects
	 */
	public PhoneBookGroupVO listCustomerPhoneBookGroupMember(int groupId) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > listCustomerPhoneBookGroup > Called ");
		java.util.ArrayList listOfGroups = null;
		Connection connection   = null;
	    PreparedStatement pstm  = null;
	    ResultSet resultSet = null;
	    PhoneBookGroupVO groubObj = null;
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" SELECT PHONE_BOOK_MEMBER.contact_id , PHONE_BOOK_MEMBER.owner_msisdn , PHONE_BOOK_MEMBER.contact_msisdn ,");
	   	  sqlStatament.append(" PHONE_BOOK_MEMBER.contact_full_name , PHONE_BOOK_MEMBER.contact_nick_name,") ;
	   	  sqlStatament.append(" PHONE_BOOK_MEMBER.contact_email_address , PHONE_BOOK_MEMBER.function_id ,PHONE_BOOK_GROUP.DISTRIBUTION_LIST_ID, PHONE_BOOK_GROUP.group_Name"); 
	   	  sqlStatament.append(" FROM PHONE_BOOK_MEMBER,PHONE_BOOK_GROUP_MEMBER , PHONE_BOOK_GROUP");
	   	  sqlStatament.append(" WHERE PHONE_BOOK_GROUP_MEMBER.group_id=? ");
	   	  sqlStatament.append(" AND PHONE_BOOK_GROUP_MEMBER.contact_id = PHONE_BOOK_MEMBER.contact_id ");
	   	  sqlStatament.append(" AND PHONE_BOOK_GROUP.group_id = PHONE_BOOK_GROUP_MEMBER.group_id ");
	      
	   	  connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      pstm.setInt(1 , groupId);
      
	      
	      PhoneBookContactVO contactObj = null;
	      resultSet = pstm.executeQuery();
	      groubObj = new PhoneBookGroupVO(); 
	      
	      groubObj.setListOfGroupMemeber(new ArrayList());
	      boolean groupData = false;
	      while(resultSet.next()) {
	      	if(!groupData) {
	      		groubObj.setDistributionListId(resultSet.getInt("DISTRIBUTION_LIST_ID"));
	      		groubObj.setGroupName(resultSet.getString("group_name"));
	      		groubObj.setGroupId(groupId);
	      		groupData = true;
	      	}
	      	contactObj = new PhoneBookContactVO();
	      	contactObj.setContactId(resultSet.getInt("contact_id"));
	      	contactObj.setMSISDN(resultSet.getString("owner_msisdn"));
	      	contactObj.setContactMobileNumber(resultSet.getString("contact_msisdn"));
	      	contactObj.setFullName(resultSet.getString("contact_full_name"));
	      	contactObj.setNickName(resultSet.getString("contact_nick_name"));
	      	contactObj.setEmailAddress(resultSet.getString("contact_email_address"));
	      	contactObj.setFunctionId(resultSet.getInt("function_id"));
	      	groubObj.getListOfGroupMemeber().add(contactObj);
	      }
	      log.debug("OraclePhoneBookDAO > listCustomerPhoneBookGroup > List Contact memebr for group ["+groupId+"] = "+groubObj.getListOfGroupMemeber().size());
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > listCustomerPhoneBookGroup > SQLException > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > listCustomerPhoneBookGroup >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  if(resultSet != null){
	   	     resultSet.close();
	   	     resultSet = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	   return groubObj;
	}
/**
	 * Responsible for Adding contact To Group
	 * @param groupVO
	 * 				- groupid
	 * 				- ArrayList of contact Id
	 * @throws MobilyCommonException
	 */
	public void joinContactToGroup(PhoneBookGroupVO groupVO) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > joinContactToGroup > Called ");

		Connection connection   = null;
	    PreparedStatement pstm  = null;
       	  
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_GROUP_MEMBER ( GROUP_ID  , CONTACT_ID");
	   	  sqlStatament.append("  ) values ");
	   	  sqlStatament.append(" ( ?,?)");
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      connection.setAutoCommit(false);
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      if(groupVO.getListOfGroupMemeber() != null) {
	      	for(int i=0 ; i < groupVO.getListOfGroupMemeber().size() ; i++) {
		      pstm.setInt(1 , groupVO.getGroupId());
		      pstm.setString(2 , ((String)groupVO.getListOfGroupMemeber().get(i)));
		      int updateedItem  = pstm.executeUpdate();
	      	 }
	      }
	      
	      connection.commit();
	      log.debug("OraclePhoneBookDAO > joinContactToGroup > Contacts Join group ["+groupVO.getGroupId()+"]");
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > joinContactToGroup > SQLException > "+e.getMessage());
	   	try {
			connection.rollback();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
		}
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > joinContactToGroup >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	
	   	try{
	   		connection.setAutoCommit(true);
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	}

	/**
	 * 
	 */
	public ArrayList getContactMsidnById(ArrayList listOfId) throws MobilyCommonException{
		log.debug("OraclePhoneBookDAO > getContactMsidnById > Called ");
		java.util.ArrayList listOfMSISDN = null;
		Connection connection   = null;
	    PreparedStatement pstm  = null;
	    ResultSet resultSet = null;
	    PhoneBookGroupVO groubObj = null;
	   try{
	   	  StringBuffer sqlStatament = new StringBuffer();
	   	  sqlStatament.append(" SELECT CONTACT_MSISDN FROM PHONE_BOOK_MEMBER");
	   	  sqlStatament.append(" WHERE CONTACT_ID in (") ;
	   	  String dele = "";
	   	  for(int i =0 ; i < listOfId.size(); i++) {
	   	  	sqlStatament.append(dele + (String)listOfId.get(i));
	   	  	dele = ",";
	   	  }
	   	  sqlStatament.append(" )");
	      
	   	  connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament.toString());
	      resultSet = pstm.executeQuery();
	      listOfMSISDN = new ArrayList();
	      while(resultSet.next()) {
	      	listOfMSISDN.add(resultSet.getString("CONTACT_MSISDN"));
	      }
	      log.debug("OraclePhoneBookDAO > getContactMsidnById > Done");
	   }catch(SQLException e){
	   	  log.fatal("OraclePhoneBookDAO > getContactMsidnById > SQLException > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());
	   	
	   }catch(Exception e){
	   	  log.fatal("OraclePhoneBookDAO > getContactMsidnById >  Exception > "+e.getMessage());
	   	  throw new MobilyCommonException(e.getMessage());  
	   }
	   finally{
	   	try{
	   	  if(connection!= null){
	   	     connection.close();
	   	     connection = null;
	   	     if(pstm != null){
	   	       pstm.close();
	   	       pstm = null;
	   	     }
	   	  if(resultSet != null){
	   	     resultSet.close();
	   	     resultSet = null;
	   	     }
	   	  }
	   	}catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > getContactMsidnById >Close DB Connection Exception > "+e.getMessage());
	   		
	   	} 
	   }
	   return listOfMSISDN;
	}
	
	

	//The following functions are created for VSMS project ----- START
	
		/**
		 * Resopnsible for getting the function id 
		 * * @param functionName
		 * @throws MobilyCommonException
		 */
		public int getFunctionId(String functionName) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > getFunctionId > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    ResultSet resultSet = null;
		    int functionId = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" SELECT FUNCTION_ID FROM PHONE_BOOK_FUNCTION_TYPE");
		   	  sqlStatament.append("  WHERE FUNCTION_NAME = '"+functionName+"'");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      resultSet = pstmt.executeQuery();
		      
		      while(resultSet.next()) {
		      	functionId = resultSet.getInt("FUNCTION_ID");
		      }
	
		      log.debug("OraclePhoneBookDAO > getFunctionId > functionId ["+functionId+"]");
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
			   	log.fatal("OraclePhoneBookDAO > getFunctionId > MobilyCommonException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > getFunctionId >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	
		   		JDBCUtility.closeJDBCResoucrs(connection,pstmt,resultSet);
		   	}catch(Exception e){
			   	  log.fatal("OraclePhoneBookDAO > getFunctionId >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return functionId;
		}
	
		/**
		 * Resopnsible for finding the given contact msisdn existance for a owner msisdn
		 * * @param contactVO
		 * 			- OWNER_MSISDN
		 * 			- CONTACT_MSISDN
		 * * @param contactMsisdn
		 * @throws MobilyCommonException
		 */
		public boolean isContactMsisdnAlreadyExist(PhoneBookContactVO contactVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > isContactMsisdnAlreadyExist > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    ResultSet resultSet = null;
		    boolean existance = false;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	
		   	  if(contactVO.getListType().equalsIgnoreCase(ConstantIfc.VSMS_BLACK_LIST_MANAGEMENT)){
	   	  		  sqlStatament.append(" SELECT * FROM PHONE_BOOK_MEMBER_BL WHERE OWNER_MSISDN = ? AND CONTACT_MSISDN = ?");
	   	  		
	   	  		  pstmt       = connection.prepareStatement(sqlStatament.toString());
	   	  		
			      pstmt.setString(1,contactVO.getMSISDN());
			      pstmt.setString(2,contactVO.getContactMobileNumber());
	   	  		
		   	  }else{
		   	  	  sqlStatament.append(" SELECT * FROM PHONE_BOOK_GROUP PBG, PHONE_BOOK_GROUP_MEMBER PBGM, PHONE_BOOK_MEMBER PBM ");
		   	  	  sqlStatament.append("  WHERE PBG.MSISDN = ? AND PBG.DISTRIBUTION_LIST_ID = ? AND PBG.GROUP_ID = PBGM.GROUP_ID ");
		   	  	  sqlStatament.append("  AND PBGM.CONTACT_ID = PBM.CONTACT_ID AND PBM.OWNER_MSISDN = ? AND PBM.CONTACT_MSISDN = ?");
		   	  	
		   	  	  pstmt       = connection.prepareStatement(sqlStatament.toString());
		   	  	
		   	  	  pstmt.setString(1,contactVO.getMSISDN());
		   	  	  pstmt.setInt(2 , Integer.parseInt(contactVO.getListId()));
				  pstmt.setString(3,contactVO.getMSISDN());
			      pstmt.setString(4,contactVO.getContactMobileNumber());
		   	  }

		   	  resultSet = pstmt.executeQuery();
		      
		      if(resultSet.next()) {
		      	existance = true;
		      }
	
		      log.debug("OraclePhoneBookDAO > isContactMsisdnAlreadyExist > existance = "+existance+"");
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
			   	log.fatal("OraclePhoneBookDAO > isContactMsisdnAlreadyExist > MobilyCommonException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > isContactMsisdnAlreadyExist >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   		JDBCUtility.closeJDBCResoucrs(connection,pstmt,resultSet);
		   	}catch(Exception e){
			   	  log.fatal("OraclePhoneBookDAO > isContactMsisdnAlreadyExist >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return existance;
		}
		
		/**
		 * Resopnsible for creating a new distributed list 
		 * * @param groupVO
		 * 		- GROUP_ID  
		 * 		- DISTRIBUTION_LIST_ID  
		 * 		- FUNCTION_ID
		 *  	- MSISDN
		 * 		- GROUP_NAME
		 * @throws MobilyCommonException
		 */
		public int createNewDistributionList(PhoneBookGroupVO groupVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > createNewDistributionList > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_GROUP ( GROUP_ID  , DISTRIBUTION_LIST_ID  , FUNCTION_ID  ,");
		   	  sqlStatament.append(" MSISDN  , GROUP_NAME ) values ");
		   	  sqlStatament.append(" ( (SEQ_PBGROUP_ID.NEXTVAL), ?,?,?,?)");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setInt(1 , groupVO.getDistributionListId());
		      pstmt.setInt(2 , groupVO.getFunctionId());
		      pstmt.setString(3 , groupVO.getMSISDN());
		      pstmt.setString(4 , groupVO.getGroupName());
		      
		      updatedItem  = pstmt.executeUpdate();
		      log.debug("OraclePhoneBookDAO > createNewDistributionList > New Group ["+groupVO.getGroupName()+"] add for Account  = "+groupVO.getMSISDN());
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
		   		String message = e.getMessage();
			   	if(message.startsWith("ORA-00001")){
			   		ex.setErrorCode(ConstantIfc.PHONE_BOOK_ERROR_GROUP_ALREADY_EXIST);
			   	}
			   	log.fatal("OraclePhoneBookDAO > createNewDistributionList > SQLException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > createNewDistributionList >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstmt != null){
		   	     pstmt.close();
		   	  pstmt = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > createNewDistributionList >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return updatedItem;
		}
		
		/**
		 * Resopnsible for creating new entry for the given list id
		 * @param contactVO
		 * 		- CONTACT_ID  
		 * 		- OWNER_MSISDN  
		 * 		- CONTACT_MSISDN
		 *  	- CONTACT_FULL_NAME
		 * 		- FUNCTION_ID
		 * @param distListId
		 * @throws MobilyCommonException
		 */
		public int createDistributionListEntry(PhoneBookContactVO contactVO,int distListId) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > createDistributionListEntry > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstm  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_MEMBER ( CONTACT_ID  , OWNER_MSISDN  , CONTACT_MSISDN  ,");
		   	  sqlStatament.append(" CONTACT_FULL_NAME  ,FUNCTION_ID  ) values ");
		   	  sqlStatament.append(" ( (SEQ_PB_CONTACT_ID.NEXTVAL),?,?,?,?)");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament.toString());
		      pstm.setString(1 , contactVO.getMSISDN());
		      pstm.setString(2 , contactVO.getContactMobileNumber());
		      pstm.setString(3 , contactVO.getFullName());
		      pstm.setInt(4 , contactVO.getFunctionId());
		      
		      updatedItem  = pstm.executeUpdate();
	
		      log.debug("OraclePhoneBookDAO > createDistributionListEntry > Member > updatedItem = "+updatedItem);
	
		      if(updatedItem > 0){
		      	  sqlStatament = new StringBuffer();
		      	  
			   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_GROUP_MEMBER ( GROUP_ID, CONTACT_ID )");
			   	  sqlStatament.append(" values ((SELECT GROUP_ID FROM PHONE_BOOK_GROUP WHERE FUNCTION_ID = ?");
			   	  sqlStatament.append(" AND DISTRIBUTION_LIST_ID = ? AND MSISDN = ?),");
			   	  sqlStatament.append(" (SELECT MAX(CONTACT_ID) FROM PHONE_BOOK_MEMBER WHERE FUNCTION_ID = ?");
			   	  sqlStatament.append(" AND OWNER_MSISDN = ? AND CONTACT_MSISDN = ?))");
	
			      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			      
			      pstm       = connection.prepareStatement(sqlStatament.toString());
	
			      pstm.setInt(1 , contactVO.getFunctionId());
			      pstm.setInt(2 , distListId);
			      pstm.setString(3 , contactVO.getMSISDN());
			      pstm.setInt(4 , contactVO.getFunctionId());
			      pstm.setString(5 , contactVO.getMSISDN());
			      pstm.setString(6 , contactVO.getContactMobileNumber());
			      
			      updatedItem  = pstm.executeUpdate();
			      
			      log.debug("OraclePhoneBookDAO > createDistributionListEntry > Group Member > updatedItem = "+updatedItem);
		      }
		      
		      log.debug("OraclePhoneBookDAO > createDistributionListEntry > Add New Contact into PHONE BOOK WHERE MSISDN = "+contactVO.getMSISDN());
		   }catch(SQLException e){
		   		MobilyCommonException ex = new MobilyCommonException(e.getMessage());
		   		log.fatal("OraclePhoneBookDAO > createDistributionListEntry > SQLException > "+e.getMessage());
		   		try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		   	  throw ex;
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > createDistributionListEntry >  Exception > "+e.getMessage());
		   		try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstm != null){
		   	       pstm.close();
		   	       pstm = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > createDistributionListEntry >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return updatedItem;
		}
		
		/**
		 * Resopnsible for updating a black list entry 
		 * * @param contactVO
		 * 		- CONTACT_ID  
		 * 		- CONTACT_MSISDN  
		 * 		- CONTACT_FULL_NAME
		 * @throws MobilyCommonException
		 */
		public int updateListEntry(PhoneBookContactVO contactVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > updateListEntry > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  
		   	  sqlStatament.append(" UPDATE PHONE_BOOK_MEMBER_BL SET CONTACT_MSISDN = ?, CONTACT_FULL_NAME = ?");
		   	  sqlStatament.append("  WHERE CONTACT_ID = ?");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setString(1 , contactVO.getContactMobileNumber());
		      pstmt.setString(2 , contactVO.getFullName());
		      pstmt.setInt(3 , contactVO.getContactId());
		      
		      updatedItem  = pstmt.executeUpdate();
		      log.debug("OraclePhoneBookDAO > updateListEntry > updatedItem ="+updatedItem);
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
			   	log.fatal("OraclePhoneBookDAO > updateListEntry > SQLException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > updateListEntry >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstmt != null){
		   	     pstmt.close();
		   	  pstmt = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > updateListEntry >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return updatedItem;
		}

		/**
		 * Resopnsible for updating a distribution list entry 
		 * * @param contactVO
		 * 		- CONTACT_ID  
		 * 		- CONTACT_MSISDN  
		 * 		- CONTACT_FULL_NAME
		 * @throws MobilyCommonException
		 */
		public int updateDistributionListEntry(PhoneBookContactVO contactVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > updateDistributionListEntry > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  
		   	  sqlStatament.append(" UPDATE PHONE_BOOK_MEMBER SET CONTACT_MSISDN = ?, CONTACT_FULL_NAME = ?");
		   	  sqlStatament.append("  WHERE CONTACT_ID = ?");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setString(1 , contactVO.getContactMobileNumber());
		      pstmt.setString(2 , contactVO.getFullName());
		      pstmt.setInt(3 , contactVO.getContactId());
		      
		      updatedItem  = pstmt.executeUpdate();
		      log.debug("OraclePhoneBookDAO > updateDistributionListEntry > updatedItem ="+updatedItem);
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
			   	log.fatal("OraclePhoneBookDAO > updateDistributionListEntry > SQLException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > updateDistributionListEntry >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstmt != null){
		   	     pstmt.close();
		   	  pstmt = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > updateListEntry >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return updatedItem;
		}

		/**
		 * Responsible for Delete Phone Book group from local DB , and it will affect the memebr of the group
		 * for example if the customer decide to delete group (1) from DB then the group memeber will not be a memeber of this group any more
		 * * @param phoneBookVo
		 * 		- DISTRIBUTION_LIST_ID  
		 * 		- FUNCTION_ID
		 *  	- MSISDN
		 * @throws MobilyCommonException
		 */
		public int deleteDistributinList(PhoneBookGroupVO phoneBookVo) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > deleteDistributinList > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstm  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" DELETE FROM PHONE_BOOK_GROUP WHERE DISTRIBUTION_LIST_ID = ? AND MSISDN = ? AND FUNCTION_ID = ?");
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament.toString());
	
		      pstm.setInt(1 , phoneBookVo.getDistributionListId());
		      pstm.setString(2 , phoneBookVo.getMSISDN());
		      pstm.setInt(3 , phoneBookVo.getFunctionId());
	
		      
		      updatedItem  = pstm.executeUpdate();
		      log.debug("OraclePhoneBookDAO > deleteDistributinList > updatedItem = "+updatedItem);
		   }catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > deleteDistributinList > SQLException > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > deleteDistributinList >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstm != null){
		   	       pstm.close();
		   	       pstm = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > deleteDistributinList >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return updatedItem;
		}
		
		/**
		 * Responsible for Delete Phone Book member from local DB 
		 * * @param contactId
		 * @throws MobilyCommonException
		 */
		public int deleteDistributionListMember(int contactId) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > deleteDistributionListMember > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstm  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" DELETE FROM PHONE_BOOK_GROUP_MEMBER WHERE CONTACT_ID = ?");
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament.toString());
	
		      pstm.setInt(1 , contactId);
		      
		      updatedItem  = pstm.executeUpdate();
		      log.debug("OraclePhoneBookDAO > deleteDistributionListMember > Child > updatedItem = "+updatedItem);
		      
		      //if the entry deleted succefully from the child table, then delete it from the parent table.
		      if(updatedItem > 0){
		      	  sqlStatament = new StringBuffer();
			   	  sqlStatament.append(" DELETE FROM PHONE_BOOK_MEMBER WHERE CONTACT_ID = ?");

			      pstm       = connection.prepareStatement(sqlStatament.toString());
			  	
	  		      pstm.setInt(1 , contactId);
	  		      
	  		      updatedItem  = pstm.executeUpdate();
	  		      log.debug("OraclePhoneBookDAO > deleteDistributionListMember > Parent > updatedItem = "+updatedItem);
		      }
		   }catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > deleteDistributionListMember > SQLException > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > deleteDistributionListMember >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstm != null){
		   	       pstm.close();
		   	       pstm = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > deleteDistributionListMember >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return updatedItem;
		}
		
		/**
		 * Responsible for list all members of a distribution list 
		 * @param groupVO
		 * 			- MSISDN   [Mandatory]
		 * 			- distribution list id
		 * 			- function ID [not Manadatory = 0]
		 * @throws MobilyCommonException
		 * @return ArrayList of PhoneBookContactVO object
		 */
		public ArrayList listDuistributionListMembers(PhoneBookGroupVO groupVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > listDuistributionListMembers > Called ");
			
			ArrayList listOfMembers = null;
			
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    ResultSet resultSet = null;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  
		   	  sqlStatament.append(" SELECT M.CONTACT_ID, M.CONTACT_FULL_NAME,M.CONTACT_MSISDN FROM ");
		   	  sqlStatament.append(" PHONE_BOOK_GROUP G, PHONE_BOOK_MEMBER M, PHONE_BOOK_GROUP_MEMBER GM");
		   	  sqlStatament.append(" WHERE G.GROUP_ID = GM.GROUP_ID AND GM.CONTACT_ID = M.CONTACT_ID AND");
		   	  sqlStatament.append(" M.OWNER_MSISDN = ? AND G.FUNCTION_ID = ? AND G. DISTRIBUTION_LIST_ID = ? ORDER BY M.CONTACT_FULL_NAME");
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setString(1 , groupVO.getMSISDN());
		      pstmt.setInt(2 , groupVO.getFunctionId());
		      pstmt.setInt(3 , groupVO.getDistributionListId());
		      
		      resultSet = pstmt.executeQuery();
		      
		      listOfMembers = new ArrayList();
		      PhoneBookContactVO contactObj = null;
		      while(resultSet.next()) {
		      	contactObj = new PhoneBookContactVO();
		      	contactObj.setContactId(resultSet.getInt("CONTACT_ID"));
		      	contactObj.setContactMobileNumber(resultSet.getString("CONTACT_MSISDN"));
		      	contactObj.setFullName(resultSet.getString("CONTACT_FULL_NAME"));
		      	listOfMembers.add(contactObj);
		      }
		      log.debug("OraclePhoneBookDAO > listDuistributionListMembers > Total Members Present = "+listOfMembers.size());
		   }catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > listDuistributionListMembers > SQLException > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > listDuistributionListMembers >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   			JDBCUtility.closeJDBCResoucrs(connection, pstmt, resultSet);
		   	}catch(Exception e){
			   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return listOfMembers;
		}
	
		/**
		 * Resopnsible for finding the given owner msisdn existance in group
		 * * @param groupVO
		 * 			- OWNER_MSISDN
		 * 			- FUNCTION_ID
		 * * @param contactMsisdn
		 * @throws MobilyCommonException
		 */
		public boolean isOwnerMsisdnAlreadyExist(PhoneBookGroupVO groupVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > isOwnerMsisdnAlreadyExist > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    ResultSet resultSet = null;
		    boolean existance = false;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" SELECT * FROM PHONE_BOOK_GROUP_BL WHERE MSISDN = ? AND FUNCTION_ID = ?");
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setString(1,groupVO.getMSISDN());
		      pstmt.setInt(2,groupVO.getFunctionId());
		      
		      resultSet = pstmt.executeQuery();
		      
		      if(resultSet.next()) {
		      	existance = true;
		      }
	
		      log.debug("OraclePhoneBookDAO > isOwnerMsisdnAlreadyExist > existance = "+existance);
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
			   	log.fatal("OraclePhoneBookDAO > isOwnerMsisdnAlreadyExist > MobilyCommonException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > isOwnerMsisdnAlreadyExist >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   		JDBCUtility.closeJDBCResoucrs(connection,pstmt,resultSet);
		   	}catch(Exception e){
			   	  log.fatal("OraclePhoneBookDAO > isOwnerMsisdnAlreadyExist >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return existance;
		}
	
		/**
		 * Resopnsible for creating a new black list 
		 * * @param groupVO
		 * 		- GROUP_ID  
		 * 		- FUNCTION_ID
		 *  	- MSISDN
		 * @throws MobilyCommonException
		 */
		public int createNewBlackList(PhoneBookGroupVO groupVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > createNewBlackList > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_GROUP_BL ( GROUP_ID  , FUNCTION_ID  , MSISDN )");
		   	  sqlStatament.append("  values ( (SEQ_PBGROUP_BL_ID.NEXTVAL),?,?)");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setInt(1 , groupVO.getFunctionId());
		      pstmt.setString(2 , groupVO.getMSISDN());
		      
		      updatedItem  = pstmt.executeUpdate();
		      
		      log.debug("OraclePhoneBookDAO > createNewBlackList > New Group ["+groupVO.getGroupName()+"] add for Account  = "+groupVO.getMSISDN());
		   }catch(SQLException e){
			   	MobilyCommonException ex = new MobilyCommonException(e.getMessage());
		   		String message = e.getMessage();
			   	if(message.startsWith("ORA-00001")){
			   		ex.setErrorCode(ConstantIfc.PHONE_BOOK_ERROR_GROUP_ALREADY_EXIST);
			   	}
			   	log.fatal("OraclePhoneBookDAO > createNewBlackList > SQLException > "+e.getMessage());
			   	throw ex;
		   	  
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > createNewBlackList >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstmt != null){
		   	     pstmt.close();
		   	  pstmt = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > createNewBlackList >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		return updatedItem;
		}
	
		/**
		 * Resopnsible for creating new entry for the given list id
		 * @param contactVO
		 * 		- CONTACT_ID  
		 * 		- OWNER_MSISDN  
		 * 		- CONTACT_MSISDN
		 *  	- CONTACT_FULL_NAME
		 * 		- FUNCTION_ID
		 * @throws MobilyCommonException
		 */
		public int createBlackListEntry(PhoneBookContactVO contactVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > createBlackListEntry > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstm  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_MEMBER_BL ( CONTACT_ID  , OWNER_MSISDN  , CONTACT_MSISDN  ,");
		   	  sqlStatament.append(" CONTACT_FULL_NAME  ,FUNCTION_ID  ) values ");
		   	  sqlStatament.append(" ( (SEQ_PB_CONTACT_ID_BL.NEXTVAL),?,?,?,?)");
	
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament.toString());
		      pstm.setString(1 , contactVO.getMSISDN());
		      pstm.setString(2 , contactVO.getContactMobileNumber());
		      pstm.setString(3 , contactVO.getFullName());
		      pstm.setInt(4 , contactVO.getFunctionId());
		      
		      updatedItem  = pstm.executeUpdate();
	
		      log.debug("OraclePhoneBookDAO > createBlackListEntry > Member > updatedItem = "+updatedItem);
	
		      if(updatedItem > 0){
		      	  sqlStatament = new StringBuffer();
		      	  
			   	  sqlStatament.append(" INSERT INTO PHONE_BOOK_GROUP_MEMBER_BL ( GROUP_ID, CONTACT_ID )");
			   	  sqlStatament.append(" values ((SELECT GROUP_ID FROM PHONE_BOOK_GROUP_BL WHERE FUNCTION_ID = ?");
			   	  sqlStatament.append(" AND MSISDN = ?), (SELECT CONTACT_ID FROM PHONE_BOOK_MEMBER_BL WHERE FUNCTION_ID = ?");
			   	  sqlStatament.append(" AND OWNER_MSISDN = ? AND CONTACT_MSISDN = ?))");
	
			      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			      
			      pstm       = connection.prepareStatement(sqlStatament.toString());
	
			      pstm.setInt(1 , contactVO.getFunctionId());
			      pstm.setString(2 , contactVO.getMSISDN());
			      pstm.setInt(3 , contactVO.getFunctionId());
			      pstm.setString(4 , contactVO.getMSISDN());
			      pstm.setString(5 , contactVO.getContactMobileNumber());
			      
			      updatedItem  = pstm.executeUpdate();
			      
			      log.debug("OraclePhoneBookDAO > createBlackListEntry > Group Member > updatedItem = "+updatedItem);
		      }
		      
		      log.debug("OraclePhoneBookDAO > createBlackListEntry > Add New Contact into PHONE BOOK WHERE MSISDN = "+contactVO.getMSISDN());
		   }catch(SQLException e){
		   		MobilyCommonException ex = new MobilyCommonException(e.getMessage());
		   		log.fatal("OraclePhoneBookDAO > createBlackListEntry > SQLException > "+e.getMessage());
		   		try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		   	  throw ex;
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > createBlackListEntry >  Exception > "+e.getMessage());
		   		try {
					connection.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstm != null){
		   	       pstm.close();
		   	       pstm = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > createBlackListEntry >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return updatedItem;
		}
	
		/**
		 * Responsible for list all members of a black list 
		 * @param groupVO
		 * 			- MSISDN   [Mandatory]
		 * 			- function ID [not Manadatory = 0]
		 * @throws MobilyCommonException
		 * @return ArrayList of PhoneBookContactVO object
		 */
		public ArrayList listBlackListMembers(PhoneBookGroupVO groupVO) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > listBlackListMembers > Called ");
			
			ArrayList listOfMembers = null;
			
			Connection connection   = null;
		    PreparedStatement pstmt  = null;
		    ResultSet resultSet = null;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  
		   	  sqlStatament.append(" SELECT M.CONTACT_ID, M.CONTACT_FULL_NAME,M.CONTACT_MSISDN FROM ");
		   	  sqlStatament.append(" PHONE_BOOK_GROUP_BL G, PHONE_BOOK_MEMBER_BL M, PHONE_BOOK_GROUP_MEMBER_BL GM");
		   	  sqlStatament.append(" WHERE G.GROUP_ID = GM.GROUP_ID AND GM.CONTACT_ID = M.CONTACT_ID AND");
		   	  sqlStatament.append(" M.OWNER_MSISDN = ? AND G.FUNCTION_ID = ? ORDER BY M.CONTACT_FULL_NAME");
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      
		      pstmt       = connection.prepareStatement(sqlStatament.toString());
		      
		      pstmt.setString(1 , groupVO.getMSISDN());
		      pstmt.setInt(2 , groupVO.getFunctionId());
		      
		      resultSet = pstmt.executeQuery();
		      
		      listOfMembers = new ArrayList();
		      PhoneBookContactVO contactObj = null;
		      while(resultSet.next()) {
		      	contactObj = new PhoneBookContactVO();
		      	contactObj.setContactId(resultSet.getInt("CONTACT_ID"));
		      	contactObj.setContactMobileNumber(resultSet.getString("CONTACT_MSISDN"));
		      	contactObj.setFullName(resultSet.getString("CONTACT_FULL_NAME"));
		      	listOfMembers.add(contactObj);
		      }
		      log.debug("OraclePhoneBookDAO > listBlackListMembers > Total Members Present = "+listOfMembers.size());
		   }catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > listBlackListMembers > SQLException > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > listBlackListMembers >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   			JDBCUtility.closeJDBCResoucrs(connection, pstmt, resultSet);
		   	}catch(Exception e){
			   	  log.fatal("OraclePhoneBookDAO > createPhoneBookGroup >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return listOfMembers;
		}

		/**
		 * Responsible for Delete black list member from local DB 
		 * * @param contactId
		 * @throws MobilyCommonException
		 */
		public int deleteBlackListMember(int contactId) throws MobilyCommonException{
			log.debug("OraclePhoneBookDAO > deleteBlackListMember > Called ");
	
			Connection connection   = null;
		    PreparedStatement pstm  = null;
		    int updatedItem = 0;
	       	  
		   try{
		   	  StringBuffer sqlStatament = new StringBuffer();
		   	  sqlStatament.append(" DELETE FROM PHONE_BOOK_GROUP_MEMBER_BL WHERE CONTACT_ID = ?");
		   	  
		      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		      pstm       = connection.prepareStatement(sqlStatament.toString());
	
		      pstm.setInt(1 , contactId);
		      
		      updatedItem  = pstm.executeUpdate();
		      log.debug("OraclePhoneBookDAO > deleteBlackListMember > Child > updatedItem = "+updatedItem);
		      
		      //if the entry deleted succefully from the child table, then delete it from the parent table. 
		      if(updatedItem > 0){
		      	  sqlStatament = new StringBuffer();
			   	  sqlStatament.append(" DELETE FROM PHONE_BOOK_MEMBER_BL WHERE CONTACT_ID = ?");

			      pstm       = connection.prepareStatement(sqlStatament.toString());
			  	
	  		      pstm.setInt(1 , contactId);
	  		      
	  		      updatedItem  = pstm.executeUpdate();
	  		      log.debug("OraclePhoneBookDAO > deleteBlackListMember > Parent > updatedItem = "+updatedItem);
		      }
		   }catch(SQLException e){
		   	  log.fatal("OraclePhoneBookDAO > deleteBlackListMember > SQLException > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());
		   	
		   }catch(Exception e){
		   	  log.fatal("OraclePhoneBookDAO > deleteBlackListMember >  Exception > "+e.getMessage());
		   	  throw new MobilyCommonException(e.getMessage());  
		   }
		   finally{
		   	try{
		   	  if(connection!= null){
		   	     connection.close();
		   	     connection = null;
		   	     if(pstm != null){
		   	       pstm.close();
		   	       pstm = null;
		   	     }
		   	  }
		   	}catch(SQLException e){
			   	  log.fatal("OraclePhoneBookDAO > deleteBlackListMember >Close DB Connection Exception > "+e.getMessage());
			   	  throw new MobilyCommonException(e.getMessage());  
		   	} 
		   }
		   return updatedItem;
		}

	//The above functions are created for VSMS project ----- END
		
		
		
}