/*
 * Created on Jul 3, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.AccountFilterServiceDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;




/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQAccountFilterServiceImpDAO implements AccountFilterServiceDAO{

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.customerinfo.dao.ifc.AccountFilterServiceDAO#getServiceInquiry(java.lang.String)
	 */
    private static final Logger log = LoggerInterface.log;
	public String getServiceInquiry(String xmlMessage) {
	   
	  
	    	String replyMessage = "";
			log.info("MQAccountFilterServiceImpDAO > getServiceInquiry > Called.................");
			try {
				/*
				 * Get QueuName
				 */
				String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MOBILY_INQ_CUST_DETAILS_REQUEST_QUEUENAME);
				log.debug("MQAccountFilterServiceImpDAO > getServiceInquiry > The request Queue Name ["+ strQueueName + "]");
				/*
				 * Get ReplyQueuName
				 */
				String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.EPORTAL_INQ_CUST_DETAILS_REPLY_QUEUENAME);
				log.debug("MQAccountFilterServiceImpDAO > getServiceInquiry > The reply Queue Name   ["+ strReplyQueueName + "]");
				/*
				 * Get reply Queu manager Name
				 */
				log.debug("MQAccountFilterServiceImpDAO > getServiceInquiry > XML request > "+xmlMessage);
				replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
				log.debug("MQAccountFilterServiceImpDAO > getServiceInquiry > XML Reply > "+replyMessage);
				
				} catch (RuntimeException e) {
					log.fatal("MQAccountFilterServiceImpDAO > getServiceInquiry > MQSend Exception > "+ e.getMessage());
					throw new SystemException(e);
				} catch (Exception e) {
				    log.fatal("MQAccountFilterServiceImpDAO > getServiceInquiry > Exception > "+ e.getMessage());
					throw new SystemException(e);
				}
			log.info("MQAccountFilterServiceImpDAO > getServiceInquiry > Done.................");
			return replyMessage;
	    }
	

}
