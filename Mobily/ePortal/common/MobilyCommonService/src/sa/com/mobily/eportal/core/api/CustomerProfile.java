//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

import java.util.Date;

/**
 * @author Hossam Omar
 *
 */
public class CustomerProfile {
    private String fullName;
    private String firstName;
    private String middleName;
    private String lastName;
    private String title;
    private String customerCategory;
    private String customerType;
    private Date birthDate;
    private String gender;
    private String nationality;
    private String occupation;
    private String packageID;
    private String billingLanguage;
    private String customerSegment;
    
    private String idDocType;
    private String idNumber;
    
    public String getFullName() {
        return fullName;
    }
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getCustomerCategory() {
        return customerCategory;
    }
    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }
    public String getCustomerType() {
        return customerType;
    }
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
    public Date getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getNationality() {
        return nationality;
    }
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
    public String getOccupation() {
        return occupation;
    }
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    public String getPackageID() {
        return packageID;
    }
    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }
    public String getBillingLanguage() {
        return billingLanguage;
    }
    public void setBillingLanguage(String billingLanguage) {
        this.billingLanguage = billingLanguage;
    }
    public String getCustomerSegment() {
        return customerSegment;
    }
    public void setCustomerSegment(String customerSegment) {
        this.customerSegment = customerSegment;
    }
    public String getIdDocType() {
        return idDocType;
    }
    public void setIdDocType(String idDocType) {
        this.idDocType = idDocType;
    }
    public String getIdNumber() {
        return idNumber;
    }
    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    @Override
    public String toString() {
	return "CustomerProfile [fullName=" + fullName + ", customerCategory=" + customerCategory + ", customerType="
	        + customerType + ", packageID=" + packageID + ", idDocType=" + idDocType + ", idNumber=" + idNumber
	        + "]";
    }
    
    
}
