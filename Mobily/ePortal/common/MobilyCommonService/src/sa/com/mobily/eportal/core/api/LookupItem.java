//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

import javax.xml.bind.annotation.XmlElement;

/**
 * A generic Value Object which holds the id, name and Arabic name of
 * a data item.
 */
public class LookupItem implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private String nameAr;
    protected String itemCategory;
    protected String itemSubCat;
    protected String itemUnit;
    protected String itemAmount;
    protected String pointsRequired;
    protected String eligible;
    
    /**
     * Retrieves the Id field.
     * @return the value of the id.
     */
    public final String getId() {
        return id;
    }

    /**
     * Sets the Id field.
     * @param value the new value of the id field.
     */
    public final void setId(String value) {
        this.id = value;
    }

    /**
     * Retrieves the Name field.
     * @return the value of the name.
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets the Name field.
     * @param value the new value of the name field.
     */
    public final void setName(String value) {
        this.name = value;
    }

    /**
     * Retrieves the Arabic Name field.
     * @return the value of the Arabic name.
     */
    public final String getNameAr() {
        return nameAr;
    }

    /**
     * Sets the Arabic Name field.
     * @param value the new value of the Arabic name field.
     */
    public final void setNameAr(String value) {
        this.nameAr = value;
    }

    
    /**
	 * @return the itemCategory
	 */
	public String getItemCategory()
	{
		return itemCategory;
	}

	/**
	 * @param itemCategory the itemCategory to set
	 */
	public void setItemCategory(String itemCategory)
	{
		this.itemCategory = itemCategory;
	}

	/**
	 * @return the itemSubCat
	 */
	public String getItemSubCat()
	{
		return itemSubCat;
	}

	/**
	 * @param itemSubCat the itemSubCat to set
	 */
	public void setItemSubCat(String itemSubCat)
	{
		this.itemSubCat = itemSubCat;
	}

	/**
	 * @return the itemUnit
	 */
	public String getItemUnit()
	{
		return itemUnit;
	}

	/**
	 * @param itemUnit the itemUnit to set
	 */
	public void setItemUnit(String itemUnit)
	{
		this.itemUnit = itemUnit;
	}

	/**
	 * @return the itemAmount
	 */
	public String getItemAmount()
	{
		return itemAmount;
	}

	/**
	 * @param itemAmount the itemAmount to set
	 */
	public void setItemAmount(String itemAmount)
	{
		this.itemAmount = itemAmount;
	}

	/**
	 * @return the pointsRequired
	 */
	public String getPointsRequired()
	{
		return pointsRequired;
	}

	/**
	 * @param pointsRequired the pointsRequired to set
	 */
	public void setPointsRequired(String pointsRequired)
	{
		this.pointsRequired = pointsRequired;
	}

	/**
	 * @return the eligible
	 */
	public String getEligible()
	{
		return eligible;
	}

	/**
	 * @param eligible the eligible to set
	 */
	public void setEligible(String eligible)
	{
		this.eligible = eligible;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "LookupItem [id=" + id + ", name=" + name + ", nameAr=" + nameAr + ", itemCategory=" + itemCategory + ", itemSubCat=" + itemSubCat + ", itemUnit=" + itemUnit
				+ ", itemAmount=" + itemAmount + ", pointsRequired=" + pointsRequired + ", eligible=" + eligible + "]";
	}

	

}
