/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.constant;


/**2
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ConstantIfc {

	public static final String PLUS_SIGN = "+";
    
    //DataSources For database
    public static final String DATA_SOURCE_PAYMENT = "payment.datasource";
    public static final String DATA_SOURCE_BILL = "billing.portal.datasource";
    public static final String DATA_SOURCE_SIEBEL = "siebel.datasource";
    public static final String DATA_SOURCE_BSL = "bsl.datasource";
    public static final String DATA_SOURCE_EEDB = "eedb.datasource";
    public static final String DATA_SOURCE_CMDS = "cmds.datasource"; // Caching Module DS
    public static final String DATA_SOURCE_CREDIT = "credit.datasource";
    public static final String DATA_SOURCE_EEDB_BILLING = "billing.eedb.datasource";
    public static final String DATA_SOURCE_MCR = "mcr.datasource";
    public static final String DATA_SOURCE_BRM = "brm.datasource";
    public static final String DATA_SOURCE_APPDB = "app.datasource";
    
    public static final String MQ_QUEUE_MANAGER_NAME= "mq.queue.manager.name";
    public static final String MQ_POOL_SIZE= "mq.pool.size";
    public static final String MQ_QUEUE_TIMEOUT= "mq.timeout";
    
    public static final String MQ_JMS_CONNECTION_FACTORY = "mobily.connection.factory.jndi";
    
    public static final String CUSTOMER_INQ_REQUEST_QUEUENAME = "mq.customerinfo.request.queue";
    public static final String CUSTOMER_INQ_REPLY_QUEUENAME = "mq.customerinfo.reply.queue";

	//Customer Status
	public static final String CUSTOMER_STATUS_REQUEST_QUEUENAME = "mq.customerstatus.request.queue";
	public static final String CUSTOMER_STATUS_REPLY_QUEUENAME = "mq.customerstatus.reply.queue";
    
    
	//Customer Type Specification
	public static final String CUSTOMER_TYPE_MESSAGE_FORMAT_VALUE = "HLR.INQUIRY.REQUEST";
	//public static final String CUSTOMER_TYPE_MESSAGE_VER_VALUE = "01";
	public static final String CUSTOMER_TYPE_MESSAGE_VER_VALUE = "0001";
	public static final String CUSTOMER_TYPE_CHANNEL_FUNC_VALUE = "INQ";
	public static final String LANG_VALUE = "E";
	public static final String SECURITY_INFO_VALUE = "secure";
	public static final String RETURN_CODE_VALUE = "0000";
	public static final String RETURN_CODE_VALUE_ZERO = "0";
	public static final String CHANNEL_ID_VALUE = "ePortal";
	public static final String INCLUDE_ID_YES = "Y";
	public static final String INCLUDE_CATEGORY_YES = "Y";

	public static final int CUSTOMER_PREPAID_TYPE =1;
	public static final int CUSTOMER_POSTPAID_TYPE =2;

	public static final String SERVICE_VALUE = "WIFI_Roaming";
	public static final String USER_NAME_SUFFIX = "@mobily.com.sa";
	
	// Customer Status
	public static final String CUSTSTATUS_FUNC_ID_VALUE ="CUSTOMER_STATUS";
	public static final String CUSTSTATUS_CHARGABLE_VALUE ="N";
    public static final String CUSTSTATUS_CHARGING_MODE ="Payment";
    public static final String CUSTSTATUS_CHARGE_AMOUNT ="0.0";
    
    //balance inquiry
    public static final String BALANCE_MESSAGE_FORMAT_VALUE = "BALANCE_INQ";
    public static final String BALANCE_CHANNEL_FUNC_VALUE = "INQ";
	public static final String BALANCE_CHARGABLE = "0";
	public static final String BALANCE_CHARGA_AMOUNT_VALUE = "0";
	public static final String BALANCE_GET_IN_VALUE = "0";
	public static final String BALANCE_INQ_REQUEST_QUEUENAME = "mq.balance.request.queue";
	public static final String BALANCE_INQ_REPLY_QUEUENAME = "mq.balance.reply.queue";
	
	//Free balance Inquiry
	public static final String FREE_BALANCE_INQ_REQUEST_QUEUENAME = "mq.free.balance.request.queue";
	public static final String FREE_BALANCE_INQ_REPLY_QUEUENAME = "mq.free.balance.reply.queue";
	
	
	public static final String SEC_KEY_VALUE ="kjasdhfjk48938jkdhf";
	public static final String MESSAGE_VERSION_VALUE = "0001";
	
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_4S = "yyyyMMddHHmmSSss";
    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static final String TIMESTAMP_FORMAT_MONTH = "yyyyMM";
    public static final String TIMESTAMP_FORMAT_PAYMENT = "YYMMDDHHMMSS";
    public static final String PAYMENT_CREATIONDATE_FORMAT ="dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_BIRTH_DATE = "yyyy-MM-dd";//"yyyyMMdd";

    public static final String CHANNEL_ID_BSL_VALUE = "BSL";
    
	//	Mobily Promotion
	public static final String MOBILY_PROMOTION_REQUEST_QUEUENAME = "mq.mobilypromotion.request.queue";
	public static final String MOBILY_PROMOTION_REPLY_QUEUENAME = "mq.mobilypromotion.reply.queue";

//  MOBILY SKY
    public static final String MOBILY_SKY_IP = "MOBILY_SKY_IP";
    public static final String MOBILY_SKY_PORT = "MOBILY_SKY_PORT";
    public static final String MOBILY_SKY_URL_PREFIX = "http://";
    public static final String MOBILY_SKY_URL_COLON = ":";
    public static final String MOBILY_SKY_URL_ROOT = "/GoogleWebForApache";
    public static final String MOBILY_SKY_URL_SERVLET = "/MobilyConnect";
    
    public static final String MOBILY_SKY_CHANNEL_ID = "ePortal";
    public static final String MOBILY_SKY_FUNC_ID_CREATE = "create";
    public static final String MOBILY_SKY_FUNC_ID_DELETE = "delete";
    public static final String MOBILY_SKY_FUNC_ID_SUSPEND = "suspend";
    public static final String MOBILY_SKY_FUNC_ID_RESTORE = "restore";
    public static final int SKY_EMAIL_ERROR_SUCCESS                  = 0;
	public static final int SKY_EMAIL_ERROR_CAPTCHA_REQUIRED         = 1600;
	public static final int SKY_EMAIL_ERROR_AUTHENTICATION_EXCEPTION = 1601;
	public static final int SKY_EMAIL_ERROR_SSL_HANDSAKE_EXCEPTION   = 1602;
	public static final int SKY_EMAIL_ERROR_Exception = 9999;
	public static final int SKY_EMAIL_SERVICE_FAIL                   = 1;
	public static final String SecondaryEmailStatus_PENDING = "PENDING";
	public static final String SecondaryEmailStatus_UNPENDING = "UNPENDING";
	
	//supplementary service inquiry
	public static final String SUPPLEMENTARY_SERVICE_INQUIRY_REQUEST_QUEUENAME = "mq.supplementary.service.inquiry.request.queue";
	public static final String SUPPLEMENTARY_SERVICE_INQUIRY_REPLY_QUEUENAME = "mq.supplementary.service.inquiry.reply.queue";
	public static final String SUPP_SERVICE_MSF_FORMATE="SUPPLEMENTARY_SERVICES_INQUIRY";
	public static final String SUPPLEMENTARY_SERVICE_CHANNEL_FUNC_VALUE = "INQ";
	public static final String SUPPLEMENTARY_SERVICE_RETURN_CODE_VALUE = "0";
	public static final int MOBILY_PROMOTION_INQUIRY = 2;
	public static final int MOBILY_PROMOTION_ACKNOWLADGE = 1;
	public static final String SUPPLEMENTARY_SERVICE_SECURITY_KEY = "12qweq342r2fwefw3";
	public static final String PROMOTION_OVERWRITE_VALUE = "N";
	public static final String PROMOTION_CHARGE_AMOUNT_VALUE = "0.0";
	public static final String SUPPLEMENTART_SERVICE_NAME = "ServiceName";
	public static final String SUPPLEMENTART_SERVICE_STATUS = "Status";
	public static final String SUBSCRIPTION_STATUS = "SubscriptionStatus";
	public static final String SUPPLEMENTART_SERVICE_TYPE = "ServiceType";
	public static final String SUPPLEMENTART_SERVICE_GAMING = "Gaming";
	public static final String SUPPLEMENTART_SERVICE_VEDIO_TELEPHONY = "Video Telephony";
	public static final String SUPPLEMENTART_SERVICE_VEDIO_STREAMING = "Video Streaming";
	public static final String SUPPLEMENTART_SERVICE_RBT = "RBT";
	public static final String SUPPLEMENTART_SERVICE_GPRS_20MB = "GPRS";
	public static final String SUPPLEMENTART_SERVICE_GPRS_5MB = "GPRS5";
	public static final String SUPPLEMENTART_SERVICE_GPRS_30MB = "GPRS30MB";
	public static final String SUPPLEMENTART_SERVICE_GPRS_100MB = "GPRS100MB";
	public static final String SUPPLEMENTART_SERVICE_GPRS_1G = "GPRS1G";
	public static final String SUPPLEMENTART_SERVICE_GPRS_5G = "GPRS5G";
	public static final String SUPPLEMENTART_SERVICE_GPRS_UNLIMETED = "GPRS_UNLIMITED";
	public static final String SUPPLEMENTART_SERVICE_GPRS_1GB_CORP = "GPRS1GB_CORP";
	public static final String SUPPLEMENTART_SERVICE_GPRS_5GB_CORP = "GPRS5GB_CORP";
	public static final String SUPPLEMENTART_SERVICE_GPRS_250MB_CON = "GPRS250MB_CON";
	public static final String SUPPLEMENTART_SERVICE_GPRS_500MB_CON = "GPRS500MB_CON";
	public static final String SUPPLEMENTART_SERVICE_GPRS_2GB_CON = "GPRS2GB_CON";
	public static final String SUPPLEMENTART_SERVICE_GPRS_5GB_CON = "GPRS5GB_CON";
	public static final String SUPPLEMENTART_SERVICE_GPRSCON_5GB = "GPRSCON_5GB";
	public static final String SUPPLEMENTART_SERVICE_GPRSCON_2GB_PREMIUM = "GPRSCON_2GB_PREMIUM";
	
	public static final String SUPPLEMENTART_SERVICE_GPRS_1GB_MUBASHER = "GPRS1GB_MUBASHER";
	public static final String SUPPLEMENTART_SERVICE_GPRS_5GB_MUBASHER = "GPRS5GB_MUBASHER";
	public static final String SUPPLEMENTART_SERVICE_GPRS_UNLIMITED_MUBASHER = "GPRSUNLIMITED_MUBASHER";
	
	public static final String SUPPLEMENTART_SERVICE_CONNECT_TOPUP_LAPTOP = "CONNECT_TOPUP_SKY";
	public static final String SUPPLEMENTART_SERVICE_CONNECT_TOPUP_MOBILE = "CONNECT_TOPUP_VOICE";
	public static final String SUPPLEMENTART_SERVICE_CONNECT_TOPUP_LTE = "CONNECT_TOPUP_LTE";
	
	public static final String SUPPLEMENTART_SERVICE_HOME_ZONE = "HOME_ZONE";
	public static final String SUPPLEMENTART_SERVICE_VOICE_SMS = "Voice SMS";
	public static final String SUPPLEMENTART_SERVICE_ICS2 = "ICS2";
	public static final String SUPPLEMENTART_SERVICE_ICS = "ICS";
	public static final String SUPPLEMENTART_SERVICE_EVMS = "EVMS";
	public static final String SUPPLEMENTART_SERVICE_CONNECT_ROAMING = "Connect Roaming";
	public static final String SUPPLEMENTART_SERVICE_PTT = "PTT";
	public static final String SUPPLEMENTART_SERVICE_LBS = "LBS";
	public static final String SUPPLEMENTART_SERVICE_SMS_BUNDLE = "SMS_Bundle";
	public static final String SUPPLEMENTARY_SERVICE_TWININGS = "TWININGS";
	public static final String SUPPLEMENTART_SERVICE_NATIONAL_SMS_BUNDLE = "National_SMS_Bundle";
	public static final String SUPPLEMENTART_SERVICE_TV = "MOBILY_TV";
	public static final int SUPPLEMENTART_SERVICE_UNAVILABLE_STATUS = -1;
	public static final int SUPPLEMENTART_SERVICE_DEACTIVE_STATUS   = 0;
	public static final int SUPPLEMENTARY_SERVICE_SMS_BUNDLE_250_VALUE = 1;
	public static final int SUPPLEMENTARY_SERVICE_SMS_BUNDLE_450_VALUE = 2;
	public static final String SUPPLEMENTARY_SERVICE_SMS_BUNDLE_250 = "SMS_250";
	public static final String SUPPLEMENTARY_SERVICE_SMS_BUNDLE_450 = "SMS_450";
	public static final String SUPPLEMENTARY_SERVICE_SMS_BUNDLE_50 = "N_SMS_50";
	public static final String SUPPLEMENTARY_SERVICE_SMS_BUNDLE_150 = "N_SMS_150";
	public static final int SUPPLEMENTARY_SERVICE_SMS_BUNDLE_50_VALUE = 1;
	public static final int SUPPLEMENTARY_SERVICE_SMS_BUNDLE_150_VALUE = 2;
	
	public static final String SUPPLEMENTARY_SERVICE_MMS_BUNDLE = "MMS_BUNDLE";
	public static final String SUPPLEMENTARY_SERVICE_MMS_BUNDLE_30 = "Bundle 30 MMS";
	public static final String SUPPLEMENTARY_SERVICE_MMS_BUNDLE_90 = "Bundle 90 MMS";
	public static final String SUPPLEMENTARY_SERVICE_MMS_BUNDLE_150 = "Bundle 150 MMS";
	public static final int SUPPLEMENTARY_SERVICE_MMS_BUNDLE_30_VALUE = 1;
	public static final int SUPPLEMENTARY_SERVICE_MMS_BUNDLE_90_VALUE = 2;
	public static final int SUPPLEMENTARY_SERVICE_MMS_BUNDLE_150_VALUE = 3;
	
	public static final String SUPPLEMENTARY_SERVICE_BMS = "BMS";
	public static final String SUPPLEMENTARY_SERVICE_CONSUMER_VOIP = "CON_VOIP";
	public static final String SUPPLEMENTARY_SERVICE_CORP_CONSUMER_VOIP = "CORP_VOIP";
	
	
	public static final int MEGA_BUNDLE_ID_5M=1;
	public static final int MEGA_BUNDLE_ID_20M=2;
	public static final int MEGA_BUNDLE_ID_UNLIMITED=3;
	public static final int MEGA_BUNDLE_ID_1G=4;
	public static final int MEGA_BUNDLE_ID_5G=5;
	public static final int MEGA_BUNDLE_ID_30M=6;
	public static final int MEGA_BUNDLE_ID_100M=7;
	public static final int MEGA_BUNDLE_ID_1G_CORP=8;
	public static final int MEGA_BUNDLE_ID_5G_CORP=9;
	public static final int MEGA_BUNDLE_ID_250M_CON=10;
	public static final int MEGA_BUNDLE_ID_500M_CON=11;
	public static final int MEGA_BUNDLE_ID_2G_CON=12;
	public static final int MEGA_BUNDLE_ID_5G_CON=13;
	public static final int MEGA_BUNDLE_ID_CON_2GB_PREMIUM=14;
	public static final int MEGA_BUNDLE_ID_1GB_MUBASHER=15;
	public static final int MEGA_BUNDLE_ID_5GB_MUBASHER=16;
	public static final int MEGA_BUNDLE_ID_UNLIMITED_MUBASHER=17;
	   
	   
	public static final String GPRS_OPERATION_5M = "5MB";
	public static final String GPRS_OPERATION_20M = "20MB";
	public static final String GPRS_OPERATION_30M = "30MB";
	public static final String GPRS_OPERATION_100M = "100MB";
	public static final String GPRS_OPERATION_UNLIMITED = "Unlimited";
	public static final String GPRS_OPERATION_1G = "1GB";
	public static final String GPRS_OPERATION_5G = "5GB";
    public static final String GPRS_OPERATION_1G_CORP = "1GB_CORP";
    public static final String GPRS_OPERATION_5G_CORP = "5GB_CORP";
    public static final String GPRS_OPERATION_250M_CON = "250MB_CON";
    public static final String GPRS_OPERATION_500M_CON = "500MB_CON";
    public static final String GPRS_OPERATION_2G_CON = "2GB_CON";
    public static final String GPRS_OPERATION_5G_CON = "5GB_CON";
    public static final String GPRS_OPERATION_CON_5G = "CON_5GB";
    public static final String GPRS_OPERATION_CON_2GB_PREMIUM = "CON_2GB_PREMIUM";
    public static final String GPRS_OPERATION_1GB_MUBASHER = "1GB_MUBASHER";
    public static final String GPRS_OPERATION_5GB_MUBASHER = "5GB_MUBASHER";
    public static final String GPRS_OPERATION_UNLIMITED_MUBASHER = "UNLIMITED_MUBASHER";
    
    public static final String CONNECT_TOPUP_500MB = "500MB";
    public static final String CONNECT_TOPUP_1GB = "1GB";
    public static final String CONNECT_TOPUP_2GB = "2GB";
    public static final String CONNECT_TOPUP_5GB = "5GB";
    
	public static final int CONNECT_TOPUP_500MB_VALUE = 1;
	public static final int CONNECT_TOPUP_1GB_VALUE = 2;
	public static final int CONNECT_TOPUP_2GB_VALUE = 3;
	public static final int CONNECT_TOPUP_5GB_VALUE = 4;

	
	public static final int CONNECT_TOPUP_DB_ID = 22;

	public static final int ONNECT_TOPUP_MOBILE_ID_500MB=11;
	public static final int ONNECT_TOPUP_MOBILE_ID_1GB=12;
	public static final int ONNECT_TOPUP_MOBILE_ID_2GB=13;
	public static final int ONNECT_TOPUP_MOBILE_ID_5GB=14;
	public static final int ONNECT_TOPUP_LAPTOP_ID_500MB=21;
	public static final int ONNECT_TOPUP_LAPTOP_ID_1GB=22;
	public static final int ONNECT_TOPUP_LAPTOP_ID_2GB=23;
	public static final int ONNECT_TOPUP_LAPTOP_ID_5GB=24;
	public static final int ONNECT_TOPUP_LTE_ID_500MB=31;
	public static final int ONNECT_TOPUP_LTE_ID_1GB=32;
	public static final int ONNECT_TOPUP_LTE_ID_2GB=33;
	public static final int ONNECT_TOPUP_LTE_ID_5GB=34;
	
	
    public static final String SUCCESS_MSG = "Success";
    public static final String FAILURE_MSG = "Failed";


	
	//	Corporate and normal lines types
    	public static final int LINE_TYPE_SECONDARY_EMAIL = 9;
    	public static final int LINE_TYPE_PORTALADMIN = 10;
	 	public static final int LINE_TYPE_CONSUMER = 11;
		public static final int LINE_TYPE_CORPORATE_LINE = 12;
		public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON = 13;
		public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON_CHANGE_IDENTITY = 14;
		public static final int LINE_TYPE_WIMAX = 15;
		public static final int LINE_TYPE_CONNECT = 16;
		public static final int LINE_TYPE_FTTH = 17;
		public static final int LINE_TYPE_APPSTORE_CUSTOMER = 18;
		public static final int LINE_TYPE_ECOMMERCE_B2B_CUSTOMER = 19;
		public static final int LINE_TYPE_ECOMMERCE_B2C_CUSTOMER = 20;
		public static final int LINE_TYPE_CAP_POWERUSER = 21;
		public static final int LINE_TYPE_SALES_ORDER_USER = 22;
		public static final int LINE_TYPE_IPTV = 23;
		public static final int LINE_TYPE_DOCTOR_ON_DEMAND = 24;
		public static final int LINE_TYPE_FTTH_WO = 25;
		public static final int LINE_TYPE_FTTH_MANAGEMENT = 26;
		public static final int LINE_TYPE_CONNECT_LTE = 27;
		public static final int LINE_TYPE_NON_MOBILY = 18;
		
		
		public static final int LINE_TYPE_DEFAULT = 0;
		
		// LDAP Group
		public static final String LDAP_GROUP_ECOMMERCE ="ecommerce";
		public static final String LDAP_GROUP_CORPORATE_CUSTOMER_CARE ="cap_cc";
		public static final String LDAP_GROUP_MOBILY_CONSUMER="mobily_consumer";
		public static final String LDAP_GROUP_MOBILY_CORPORATE ="mobily_corporate";
		public static final String LDAP_GROUP_MOBILY_APPSTORE ="mobily_appstore";
		public static final String LDAP_GROUP_MOBILY_SALES_OPERATOR ="sales-operator";
		public static final String LDAP_GROUP_MOBILY_SALES_ADMIN ="sales-admin";
		public static final String LDAP_GROUP_NON_MOBILY ="non-mobily";
		
		
		//inactive type
		public static final int INACTIVE_TYPE_CUSTOMER_NOT_EXIST = 0;
		public static final int INACTIVE_TYPE_DIFFERENT_IQAMA = 1;
		
		
		
	 	public static final int BASE_CONS_KEY = 1;
	 	public static final int BASE_CORP_KEY = 2;

		public static final int CUSTOMER_STATUS_ACTIVE =1; 
		public static final int CUSTOMER_STATUS_WARNED =2;
		
		//	 Favorite Number Inquiry
		public static final String FAVORITE_NUMBER_INQ_REQUEST_QUEUENAME_V2 = "mq.favorite.number.inquiry.request.queue.v2";
		public static final String FAVORITE_NUMBER_INQ_REPLY_QUEUENAME = "mq.favorite.number.inquiry.reply.queue";
		public static final String FN_Req_FAVORITE_NUMBER_INQUIRY = "FAVORITE_NUMBER_INQUIRY";
		public static final String FN_Req_MsgOption = "";
		public static final String FN_Req_RequestorChannelId = "SIEBEL";
		public static final String FN_Req_RequestorChannelFunction = "INQ";
		public static final String FN_Req_RequestorSecurityInfo = "secure";
	
	
		public static final String EMAIL_SMTP_HOST_NAME="EMAIL_SMTP_HOST_NAME";
		
		public static final int CP_ACTIVE = 0;
		public static final int CP_PENDING = 1;
		
		public static final String CONTENT_PROVIDER_FUNC_VALUE = "SUPPLEMENTARY_SERVICE";
		public static final String CONTENT_PROVIDER_SECURITY_KEY = "1234421211";
		public static final String CONTENT_PROVIDER_REQUESTOR_CHANNEL = "ePortal";
		public static final String CONTENT_PROVIDER_CHARGE_AMOUNT = "0.0";
		public static final String CONTENT_PROVIDER_CHARGEABLE = "Y";
		public static final String CONTENT_PROVIDER_OVERWRITE = "N";
		public static final String CONTENT_PROVIDER_SUPP_SERVICE = "ContentProvider";
		public static final String CONTENT_PROVIDER_REPLY_TO_REQUSTOR = "Y";
		public static final String CONTENT_PROVIDER_DATE_FORMATE = "yyyyMMdd";
		public static final int CONTENT_PROVIDER_COMMAND_LANG_ARABIC_VALUE=1;
		
		public static final String CONTENT_PROVIDER_COMMAND_ARABIC="A";
		public static final String CONTENT_PROVIDER_COMMAND_ENGLISH="E";
		
		public static final String CONTENT_PROVIDER_PERIOD_COUNT = "30";
		
		public static final String CP_REQUEST_QUEUENAME = "mq.cp.request.queue";
		public static final String CP_REPLY_QUEUENAME = "mq.cp.reply.queue";
		
		public static final String SMS_REQUEST_QUEUENAME  = "mq.sms.request.queue";
		public static final String SMS_RESPONSE_QUEUENAME = "mq.sms.response.queue";
		public static final String QUEUE_MANAGER_NAME_IN_CLUSTER = "mq.queue.manager.name";
		
		//	Top Ten specific
		public static final String TOP_TEN_FUNC_ID = "BILL_LIST";
		public static final String TOP_TEN_SECURITY_KEY = "";
		public static final String  TOP_TEN_TYPE_LOCAL = "1";
		public static final String  TOP_TEN_TYPE_INTERNATIONAL = "2";
		public static final int  TOP_TEN_COUNT = 10;
		
		//Phone Book
		public static final String PHONE_BOOK_FUNCTION_UNDEFINE = "0";
		public static final String PHONE_BOOK_FUNCTION_VSMS = "1";
		public static final String PHONE_BOOK_ERROR_CONTACT_ALREADY_EXIST = "10001";
		public static final String PHONE_BOOK_ERROR_GROUP_ALREADY_EXIST = "10002";

		public static final String VSMS_BLACK_LIST_MANAGEMENT = "Blacklist Management";
		public static final String VSMS_DISTRIBUTION_LIST_MANAGEMENT = "Distribution List Management";

		
		//Customer Details Service
		public static final String MOBILY_INQ_CUST_DETAILS_REQUEST_QUEUENAME = "mq.accountfilter.request.queue";
		public static final String EPORTAL_INQ_CUST_DETAILS_REPLY_QUEUENAME = "mq.accountfilter.reply.queue";
		
		
		public static final String CUST_INFO_MSG_FORMAT_VALUE = "CUSTOMER_DETAILS";
		public static final String CUST_INFO_MSG_VERSION_VALUE = "0001";
		public static final String CUST_INFO_SECURITY_VALUE = "asf2123aqws123123";
		public static final String CUST_INFO_REQUESTOR_CHANNEL_ID_VALUE="ePortal";
		public static final String CUST_INFO_REQUESTOR_USER_ID_VALUE="S1234";
		public static final String CUST_INFO_REQUESTOR_LANG_VALUE = "E";
		public static final String CUST_INFO_RETURN_CODE_VALUE="0000";
		public static final String CUST_INFO_GET_INTERNET_BUNDLE ="Y";
		public static final String CUST_INFO_SUCCESS_CODE = "0000";
		
		
		//Ebill Incentive
		public static final String INVOICE_MECHANISM ="POBOX";
		public static final String EBILL_RECHARGE_REQUEST_QUEUENAME="MOBILY.INQ.EMAIL.INVOICE.REQUEST";
		public static final String EBILL_REPLY_QUEUENAME ="EPORTAL.INQ.EMAIL.INVOICE.REPLY";	
		
		
		public static final String CUSTOMER_INFO_ACCOUNT ="ACCTNO";
		public static final String CUSTOMER_INFO_MSISDN ="MSISDN";

		// Panda
		public static final String PANDA_INQUIRY_REQUEST_QUEUENAME = "mq.panda.inq.balance.request.queue";
		public static final String PANDA_INQUIRY_REPLY_QUEUENAME = "mq.panda.inq.balance.reply.queue";

		public static final String PANDA_DISTRIBUTION_REQUEST_QUEUENAME = "mq.panda.distribution.request.queue";
		public static final String PANDA_DISTRIBUTION_REPLY_QUEUENAME = "mq.panda.distribution.reply.queue";
		
		public static final String PANDA_DISTRIBUTION_ASYNC_REPLY_QUEUENAME = "mq.panda.distribution.async.reply.queue";
		
		public static final String PANDA_REQUEST_TYPE_DISTRIBUTION = "distribution";
		public static final String PANDA_REQUEST_TYPE_INQUIRY = "inquiry";
	    public static final String PANDA_ACC_BAL_INQ_FUNC_VALUE = "PANDA_ACC_BAL_INQ";
	    public static final String PANDA_MSISDN_BAL_INQ_FUNC_VALUE = "PANDA_MSISDN_BAL_INQ";
	    public static final String PANDA_REQUESTOR_CHANNEL = "ePortal";
	    public static final String PANDA_DISTRIBUTION_FUNC_VALUE = "DISTRIBUTE_MINUTES";
	    public static final String PANDA_CHARGEABLE_NO = "N";
	    public static final String PANDA_MODE_PAYMENT = "Payment";
	    public static final String PANDA_CHANNEL_TRANS_ID = "SR_PANDA_";
	    public static final String PANDA_MESSAGE_VERSION = "0000";
	    public static final String PANDA_STATUS_SUCCESS = "6";
	    public static final String PANDA_SYNC_STATUS_SUCCESS = "4";
	    public static final String PANDA_STATUS_FAILURE = "3";
	    public static final String PANDA_SUCCESS_MSG = "Success";
	    public static final String PANDA_FAILURE_MSG = "Failed";
	    public static final String PANDA_FAILURE_DESCRIPTION = "Contract date of the bulk minutes has expired.";	    
	    
		//For sending Email - Panda
		
	    public static final String MINUTES_UNAVAILABLE = "14611";
	    public static final String SENDER_EMAIL = "panda.sender.email";
	    
	    public static final String REPLY_SUBJECT_EN = "panda.email.subject.en";
	    public static final String REPLY_SUBJECT_AR = "panda.email.subject.ar";
	    
	    public static final String REPLY_TITLE_EN = "panda.email.title.en";
	    public static final String REPLY_TITLE_AR = "panda.email.title.ar";
	    
	    public static final String REPLY_TRANSACTION_ADMIN_BODY1_EN = "panda.email.transaction.admin.body1.en";
	    public static final String REPLY_TRANSACTION_ADMIN_BODY2_EN = "panda.email.transaction.admin.body2.en";
	    public static final String REPLY_TRANSACTION_ADMIN_BODY3_EN = "panda.email.transaction.admin.body3.en";
	    public static final String REPLY_TRANSACTION_ADMIN_BODY1_AR = "panda.email.transaction.admin.body1.ar";
	    public static final String REPLY_TRANSACTION_ADMIN_BODY2_AR = "panda.email.transaction.admin.body2.ar";
	    public static final String REPLY_TRANSACTION_ADMIN_BODY3_AR = "panda.email.transaction.admin.body3.ar";
	    
	    public static final String REPLY_TRANSACTION_MANAGER_BODY1_EN = "panda.email.transaction.manager.body1.en";
	    public static final String REPLY_TRANSACTION_MANAGER_BODY2_EN = "panda.email.transaction.manager.body2.en";
	    public static final String REPLY_TRANSACTION_MANAGER_BODY3_EN = "panda.email.transaction.manager.body3.en";
	    public static final String REPLY_TRANSACTION_MANAGER_BODY4_EN = "panda.email.transaction.manager.body4.en";	    
	    public static final String REPLY_TRANSACTION_MANAGER_BODY1_AR = "panda.email.transaction.manager.body1.ar";
	    public static final String REPLY_TRANSACTION_MANAGER_BODY2_AR = "panda.email.transaction.manager.body2.ar";
	    public static final String REPLY_TRANSACTION_MANAGER_BODY3_AR = "panda.email.transaction.manager.body3.ar";	    	    
	    public static final String REPLY_TRANSACTION_MANAGER_BODY4_AR = "panda.email.transaction.manager.body4.ar";	    
	    
	    public static final String REPLY_ERROR_ADMIN_BODY1_EN = "panda.email.error.admin.body1.en";
	    public static final String REPLY_ERROR_ADMIN_BODY1_AR = "panda.email.error.admin.body1.ar";
	    
	    public static final String REPLY_ERROR_MANAGER_BODY1_EN = "panda.email.error.manager.body1.en";
	    public static final String REPLY_ERROR_MANAGER_BODY2_EN = "panda.email.error.manager.body2.en";
	    public static final String REPLY_ERROR_MANAGER_BODY1_AR = "panda.email.error.manager.body1.ar";
	    public static final String REPLY_ERROR_MANAGER_BODY2_AR = "panda.email.error.manager.body2.ar";
	    public static final String RAQI_PACKAGE_ID_FOR_REGISTRATION = "1267";

	    //Mobily Services
	    public static final String MOBILY_SERVICES_IP = "MOBILY_SERVICES_IP";
	    public static final String MOBILY_SERVICES_PORT = "MOBILY_SERVICES_PORT";
	    public static final String MOBILY_SERVICES_URL_PREFIX = "http://";
	    public static final String MOBILY_SERVICES_URL_COLON = ":";
	    public static final String MOBILY_SERVICES_URL_SERVLET = "/IntegerationServices";

	    
	    //Wifi Roaming Service inquiry
		public static final String WIFI_ROAMING_SERVICE_INQUIRY_REQUEST_QUEUENAME = "mq.wifiroaming.service.inquiry.request.queue";
		public static final String WIFI_ROAMING_SERVICE_INQUIRY_REPLY_QUEUENAME = "mq.wifiroaming.service.inquiry.reply.queue";
    
   		// Credit Card Payment Services
	    public static final String Messsage_Format = "CreditCardPaymentGateway";
		public static final String Requestor_ChannelId = "EPOR";
		public static final String Requestor_Channel_Function_Inquiry = "Enquiry";
		public static final String Requestor_Channel_Function_Update = "Update";
		public static final String Requestor_Channel_Function_Reset = "Reset";
		public static final String Requestor_Channel_Function_Cancellation = "Cancellation";
		public static final String Requestor_Channel_Function_Payment = "Payment";
		public static final String Requestor_Channel_Function_REGISTRATION = "Register";
		
		// Credit Card Payment Queue Names
		public static final String CREDIT_CARD_PAYMENT_REQUEST_QUEUENAME = "mq.creditCardPayment.request.queue";  
		public static final String CREDIT_CARD_ENQUIRY_REQUEST_QUEUENAME = "mq.creditCardEnquiry.request.queue";  
		public static final String CREDIT_CARD_PROCESS_REQUEST_QUEUENAME = "mq.creditCardProcess.request.queue";  

		public static final String CREDIT_CARD_PAYMENT_REPLAY_QUEUENAME = "mq.creditCardPayment.reply.queue";  
		public static final String CREDIT_CARD_ENQUIRY_REPLAY_QUEUENAME = "mq.creditCardEnquiry.reply.queue";  
		public static final String CREDIT_CARD_PROCESS_REPLAY_QUEUENAME = "mq.creditCardProcess.reply.queue"; 

		//Lookup Table
		public static final String LOOKUP_TABLE_REGION = "1";
		public static final String LOOKUP_TABLE_CITY = "2";
		public static final String LOOKUP_TABLE_PAYMENT_CHANNEL = "3";
		public static final String LOOKUP_TABLE_PAYMENT_STATUS = "4";
		public static final String LOOKUP_TABLE_DEVICE_MODEL = "5";
		public static final String LOOKUP_TABLE_PACKAGE_NAME = "6";
		public static final String LOOKUP_AGE_ID = "7";
		public static final String LOOKUP_GENDER_ID = "8";
		public static final String LOOKUP_COUNTRIES_ID = "9";
		public static final String LOOKUP_ACTIVATION_ID = "10";
		public static final int LOOKUP_CUSTOMER_PROFILE = 11;

		//For PackageConversion XMLHandler
		public static final String PACKAGE_CONVERSION_FUNC_ID_VALUE="PACKAGE CONVERSION";
		public static final String REQUESTOR_LANG_VALUE ="E";
		public static final String REQUESTOR_CHANNEL_ID_VALUE ="ePortal";
		public static final String MSG_VER_VALUE ="0000";
		public static final String SECURITY_KEY_VALUE ="123456";
		
		public static final int ERROR_CODE_COMMON            = 1;
		public static final int ERROR_CODE_PENDING_REQUEST   = 9980;
		public static final int ERROR_CODE_NOT_PREPAID       = 9984;
		public static final int ERROR_CODE_NO_ENOUGH_BALANCE = 9988;
		public static final int ERROR_CODE_BLOCKED           = 10000;
		public static final int ERROR_CODE_FULL_BARRED       = 10001;
		public static final int ERROR_CODE_OG_BARRED         = 10002;
		public static final int ERROR_CODE_NOT_SUITABLE=9987;//TO DO get error message from BSL
		public static final int ERROR_CODE_PENDING=2;

		//	packageConversion
		public static final String PACKAGE_CONVERSION_REPLY_ERROR_MESSAGE ="package.conversion.mq.connection.error";
		public static final String PACKAGE_CONVERSION_REQUEST_QUEUENAME = "mq.package.conversion.request.queue";
		public static final String PACKAGE_CONVERSION_REPLY_QUEUENAME = "mq.package.conversion.reply.queue";
		
		public static final String MSIDN_EXISTANCE_TRUE = "Y";
		public static final String MSIDN_EXISTANCE_FALSE = "N";
		
		
		// strategic track : start
		 public static final String SERVICE_REQUESTOR_ID = "TEST_SR_";
		 public static final String BACKEND_CHANNEL_VALUE = "NETWORK";
		 public static final String FUNC_ID_CORPORATE_INQUIRY = "CORP_INQ";
		 public static final String ITEM_NAME_DIA = "DIA - Circuit";
		 public static final String ITEM_NAME_ETHERNET = "Ethernet - Circuit";
		 public static final String ITEM_NAME_IPVPN = "IP VPN - Circuit";
		 
		 public static final String ITEM_NAME_DIA_BACKUP = "DIA Backup";
		 public static final String ITEM_NAME_ETHERNET_BACKUP = "Ethernet VPN - Backup";
		 public static final String ITEM_NAME_IPVPN_BACKUP = "IP VPN Backup";
		 public static final String REQUEST_QUEUENAME_CORPORATE_INQUIRY = "queuename.request.corporate.inquiry";
		 public static final String RESPONSE_QUEUENAME_CORPORATE_INQUIRY = "queuename.response.corporate.inquiry";
		 public static final String RESPONSE_QUEUENAME_CORPORATE_INQUIRY_ASYNC = "queuename.response.corporate.inquiry.async";
		 
		 public static final String REQUEST_QUEUENAME_UPDATE_CIRCUIT_ALIAS_NAME = "queuename.request.update.alias.name";
		 public static final String RESPONSE_QUEUENAME_UPDATE_CIRCUIT_ALIAS_NAME = "queuename.response.update.alias.name";
		 public static final String FUNC_ID_ALIAS_NAME_UPDATE = "AliasNameUpdate";
		 
		// strategic track : end
		 
		// RoyalGuard
		    public static final String ROYAL_GUARD_ALL_MSISDN_FUNC_VALUE = "FUNC_RG_SUBSCRIBER";
		    public static final String ROYAL_GUARD_BALANCE_MSISDN = "FUNC_RG_BALANCE";
		    public static final String FETCHTYPE_ALL = "ALL";
		    public static final String FETCHTYPE_SINGLE = "Single";
		    public static final String FETCHTYPE_RG = "RG";
		    public static final String FETCHTYPE_TEMP = "Temp";	  
		    public static final String INFO_BASIC = "BASIC";
		    
			public static final String RG_REQUEST_QUEUENAME = "rg.request.queue.name";
			public static final String RG_REPLY_QUEUENAME = "rg.reply.queue.name";
			
		//IPhone Message Keys and code
			public static final String ERROR_CODE_0000="0000"; //	Successful
			public static final String ERROR_CODE_10000="10000"; //	You are not authorized to view this page
			public static final String ERROR_CODE_10001="10001"; //	Unknown error please try again later
			public static final String ERROR_CODE_10002="10002"; // Customer not allow to login thru IPhone application
			public static final String ERROR_CODE_10004="10004"; // Not login in , please login first
			public static final String ERROR_CODE_10008="10008";// The request is invalid
			
			//public static final int CHANNEL_ID_MOBILY = 1;
			public static final int CHANNEL_ID_MOBILY = ConstantIfc.CHANNEL_MOBILY_WEBSITE;
			public static final int CHANNEL_ID_IPHONE = ConstantIfc.CHANNEL_IPHONE;
			
			public static final int CHANNEL_IPHONE 			= 0;	//	iPhone
			public static final int CHANNEL_IPAD 			= 1;	//	iPad
			public static final int CHANNEL_NOKIA 			= 2;	//	Nokia
			public static final int CHANNEL_HTC 			= 3;	//	HTC
			public static final int CHANNEL_GLOBYAS 		= 4;	//	Gloybas
			public static final int CHANNEL_WINDOWS7 		= 5;	//	Windows7
			public static final int CHANNEL_ANDROID 		= 6;	//	Android
			public static final int CHANNEL_FACEBOOK 		= 7;	//	Facebook
			public static final int CHANNEL_BLACKBERRY 		= 8;	//	Blackberry
		    public static final int CHANNEL_NGSDP 			= 9;	//	NGSDP
		    public static final int CHANNEL_NEQATY 			= 10;	//	Neqaty
		    public static final int CHANNEL_WINDOWS8		= 11;	//	Windows8
		    public static final int CHANNEL_TOOLBAR			= 12;	//	Toolbar
			public static final int CHANNEL_MOBILY_WEBSITE	= 100;	// Mobily Website
			
			public static final String NOT_AUTHARIZED_USER = "You are not authorized to view this page";
			public static final String NOT_LOGGEDIN = "Not login in, please login first.";
			public static final String UNKNOW_ERROR = "Unknown error please try again later";
			public static final String NOT_ALLOWED = "Customer not allow to login through IPhone application";
			public static final String USER_INVALID = "Invalid username or password.";
			
		//For DNS lookup
			public static final String DNS_INITIAL = "java.naming.factory.initial";
			public static final String DNS_FACTORY = "com.sun.jndi.dns.DnsContextFactory";
			public static final String DNS_MX = "MX";

		//Customer plan type
			public static final String CUSTOMER_PLAN_TYPE_POST = "Postpaid Plan";
			public static final String CUSTOMER_PLAN_TYPE_PRE = "Prepaid Plan";
			
			public static final String	VIEW_BALANCE_GPRS_UNLIMITED = "Unlimited";
			public static final String STATUS_PENDING = "PENDING";
			public static final String STATUS_COMPLETED = "COMPLETED";
			public static final String EXPIRED_STATUS_TRUE = "TRUE";
			public static final String EXPIRED_STATUS_FALSE = "FALSE";
		    public static final String AM = "AM";
		    public static final String PM = "PM";
		    
	    //Connect Bundle New Changes
			public static final String SUPPLEMENTART_SERVICE_PLAN_TYPE = "PlanType";
			public static final String SUPPLEMENTART_SERVICE_SUBSCRIPTION_UNIT = "SubscriptionUnit";
			public static final String SUPPLEMENTART_SERVICE_SUBSCRIPTION_TYPE = "SubscriptionType";
			public static final String SUPPLEMENTARY_SERVICE_CONNECT_PLAN_TYPE_1GB="1GB";
			public static final String SUPPLEMENTARY_SERVICE_CONNECT_PLAN_TYPE_5GB="5GB";
			public static final String SUPPLEMENTARY_SERVICE_CONNECT_PLAN_TYPE_UNLIMITED="Unlimited";
			//public static final String SUPPLEMENTARY_SERVICE_SUBSCRIPTION_UNIT="";
			public static final String SUPPLEMENTARY_SERVICE_SUBSCRIPTION_TYPE_DAY="Day";
			public static final String SUPPLEMENTARY_SERVICE_SUBSCRIPTION_TYPE_WEEK="Week";
			public static final String SUPPLEMENTARY_SERVICE_SUBSCRIPTION_TYPE_MONTH="Month";
			
			public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_1GB_DAY_VALUE 		= 1;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_1GB_WEEK_VALUE 	= 2;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_1GB_MONTH_VALUE 	= 3;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_5GB_DAY_VALUE 		= 4;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_5GB_WEEK_VALUE 	= 5;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_5GB_MONTH_VALUE 	= 6;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_UNLIMIT_DAY_VALUE 	= 7;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_UNLIMIT_WEEK_VALUE = 8;
		    public static final int SUPPLEMENTARY_SERVICE_CONNECT_ROAMING_BUNDLE_UNLIMIT_MONTH_VALUE = 9;
		    
	//For ConnectOTS
			public static final String FUNC_ID = "WiMAXOffShelf";
			public static final String SECURITY_KEY = "0000";
			public static final String REQUESTOR_CHANNEL_ID = "ePortal";
			public static final String REQUESTOR_CHANNEL_IVR = "IVR";
			public static final String MSG_VERSION = "0000";
			
			
			public static final String MSG_VERSION1 = "0001";
			
			public static final String ERROR_INVALID_CPE_NUMBER = "15701";
			public static final String ERROR_CPE_NUMBER_ALREADY_ACTIVATED = "15707";
			public static final String ERROR_UNDER_PROGRESS = "15708";
			
			public static final String CPE_INQUIRY_FUNC_ID = "WimaxCPESerialInquiry";
			public static final String ACC_BAL_ADPTR_FUNC_ID = "ACC_BAL_ADPTR";
			public static final String GENERAL_INQUIRY_FUNC_ID = "GENERAL_INQUIRY";
			public static final String FORCE_RENEWAL_FUNC_ID = "FORCE_RENEWAL";
			
			public static final String INQUIRY_TYPE_MSISDN_BY_SIM = "MSISDN_BY_SIM";
			
			public static final String HOME_PAGE_URL = "home.page.url";
			
		    public static final String PIN_CODE_SMS_ID_EN = "2413";
		    public static final String PIN_CODE_SMS_ID_AR = "2414";
		    
		    public static final String ERROR_CODE_DEVICE_EXPIRED = "16705";
		    public static final String ERROR_CODE_DEVICE_SUSPENDED = "16706";
		    public static final String ERROR_CODE_DEVICE_DOES_NOT_EXIST = "16707";
		    
		    public static final String ERROR_DEVICE_EXPIRED = "wimaxoffshelf.label.device.expired.error";
		    public static final String ERROR_DEVICE_SUSPENDED = "wimaxoffshelf.label.device.sumospended.error";
		    public static final String ERROR_DEVICE_DOES_NOT_EXIST = "wimaxoffshelf.label.device.not.exist.error";
		    public static final String ERROR_GENERIC_ERROR = "wimaxoffshelf.label.generic.error";
		    
		    public static final String CONNECT_VOUCHER_RECHARGE = "CONNECT_VOUCHER_RECHARGE";
		    public static final String GSM_VOUCHER_RECHARGE = "GSM_VOUCHER_RECHARGE";
		    public static final String KEY_SIM = "SIM";
		    
			// ConnectOTS Voucher re-charge
				public static final String CONNECT_VOUCHER_RECHARGE_REQUEST_QUEUENAME = "mq.connect.ots.voucher.recharge.request.queue";
				public static final String CONNECT_VOUCHER_RECHARGE_REPLY_QUEUENAME = "mq.connect.ots.voucher.recharge.reply.queue";

			// ConnectOTS get msisdn for sim number
				public static final String CONNECT_GET_MSISDN_REQUEST_QUEUENAME = "mq.connect.ots.get.msisdn.request.queue";
				public static final String CONNECT_GET_MSISDN_REPLY_QUEUENAME = "mq.connect.ots.get.msisdn.reply.queue";

				public static final String NUMBER_FORMAT = "05";
				public static final String UNIVERSAL_NUMBER_FORMAT = "966";
				
			//Added for MCR related account numbers
				public static final String FUNC_ID_MCR_RELATED = "GENERAL_INQUIRY";
				public static final String MCR_SR_ID_VALUE = "SR_1577393287";
				
				public static final String CUSTOMER_INFO_INQ_BY_ACCOUNT ="RELATED_LINES_SR3587_ACCTNO";
				public static final String CUSTOMER_INFO_INQ_BY_MSISDN ="RELATED_LINES_SR3587_MSISDN";
				
				public static final String SERVICE_ACCOUNT ="SERVICE_ACCOUNT";
				public static final String PACKAGE ="PACKAGE";
				public static final String PACKAGE_DATA_FLAG ="PACKAGE_DATA_FLAG";
				public static final String ID_DOC_NO ="ID_DOC_NO";
				public static final String BILLING_NO ="BILLING_NO";
				public static final String ID_DOC_TYPE ="ID_DOC_TYPE";
				public static final String MSISDN ="MSISDN";
				public static final String CPE_SERIAL_NO ="CPE_SERIAL_NO";
				public static final String PLAN_TYPE ="PLAN_TYPE";
				public static final String ACCOUNT_TYPE ="ACCOUNT_TYPE";
				
				public static final String MCR_RELATED_MSISDN_REQUEST_QUEUENAME = "mq.mcr.related.num.request.queue";
				public static final String MCR_RELATED_MSISDN_REPLY_QUEUENAME = "mq.mcr.related.num.reply.queue";
				
				public static final String KAM_EMAIL_ID = "kam.email.id";
           


			//Added for coverage check
				
				public static final String GET_GOVERNATE = "GET_GOVERNATE";
				public static final String GET_CITY = "GET_CITY";
				public static final String GET_DISTRICT = "GET_DISTRICT";
				public static final String GET_COVERAGE = "GET_COVERAGE";
				public static final int COVERAGE_TYPE_S = 1;	
				public static final int COVERAGE_TYPE_T = 2;
				public static final int COVERAGE_TYPE_TRY = 3;
				public static final String NOT_COVERED = "0";
				public static final String PARTIALLY_COVERED = "1";
				public static final String COVERED = "2";	
				public static final String ENGLISH_LOCALE = "en";	
				public static final String ARABIC_LOCALE = "ar";	
				public static final String COVERAGE_REQUEST_QUEUENAME ="mq.coverage.check.request.queue";
				public static final String COVERAGE_REPLY_QUEUENAME ="mq.coverage.check.reply.queue";
				public static final String MESSAGE_FORMAT_VALUE= "GIS Coverage";
				public static final String SECURITY_VALUE ="Secure";
				public static final String WIMAX_MESSAGE_VERSION_VALUE="0";
				public static final String REQUEST_CHANNEL_FUNC_VALUE ="GIS COVERAGE";
				public static final String LAYER_WIMAX ="01";
				public static final String LAYER_FTTH ="02";
				public static final String LAYER_MODE ="0";
				public static final String DB_ERROR_CODE="9997";
				public static final String GENARAL_ERROR_CODE="9997";
				
		  // Globys ebill  
				
				public static final String GLOBYS_EBILL_USER_NAME="ebill.globys.username"; 
				public static final String GLOBYS_EBILL_PASSWORD="ebill.globys.password";
				public static final String GLOBYS_INDIVIDULE_USER_STAT="fullAccess";
				public static final String GLOBYS_EBILL_USER_ACCESS_URL="ebill.globys.url.user.access";
				public static final String GLOBYS_EBILL_USER_CONFIG_URL="ebill.globys.url.user.config";
				public static final String GLOBYS_EBILL_ACC_ORG_ACC_URL="ebill.globys.url.account.org.access";
				
			//DR Update 
				public static final int DR_UPDATE_CREATE_USER=1;
				public static final int DR_UPDATE_DELETE_USER=2;

			//Connect Number format
				public static final String PREFIX_CONNECT_ACCOUNT = "200";
				public static final String PREFIX_GSM_ACCOUNT = "966";
				public static final String PREFIX_FTTH_ACCOUNT = "100";
				public static final String PREFIX_FIXED_VOICE_ACCOUNT = "9661";//SR22516 - ePortal Fixed Voice
		
		//Added for Supplementary service
				public static final String SERVICE_INTL_BARRING = "InternationalBarring";
				public static final String SERVICE_MAGIC_PROMOTION = "MagicPromotion";
				public static final String SERVICE_WIFI_ROAMING = "WiFiRoaming";
				public static final String SERVICE_ROAMING_CALL = "Roaming Calls Barring";
				
		//Added for Mobily Auth & Go Application
				public static final String FUNC_ID_MOBILY_AUTH_HAWWEL ="Hawwel"; 
				public static final String FUNC_ID_MOBILY_AUTH_RAHATI ="Rahati";
				public static final String FUNC_ID_MOBILY_AUTH_CALL_FWD ="Call_Forwarding";
				public static final String FUNC_ID_MOBILY_AUTH_VIDEO_MAIL ="Video_Mail";
				public static final String FUNC_ID_MOBILY_AUTH_VIDEO_CALL ="Video_Call";
				public static final String FUNC_ID_MOBILY_AUTH_AMAKEN ="Amaken";
				public static final String FUNC_ID_MOBILY_AUTH_ROAM_TALK ="Mobily_RoamTalk";
				public static final String FUNC_ID_MOBILY_AUTH_KHALIHA_ALIA ="Khaliha_Alia";
				public static final String FUNC_ID_MOBILY_AUTH_CREDIT_TRANSFER ="Credit_Transfer";
				public static final String FUNC_ID_MOBILY_AUTH_KALEMNI ="Kalemni";
				public static final String FUNC_ID_MOBILY_AUTH_EMER_CREDIT ="Emergency_Credit";
				public static final String FUNC_ID_MOBILY_AUTH_3LHAWA ="Mobily_3lhawa";
				public static final String FUNC_ID_MOBILY_AUTH_OSAMA_ALSAYED ="Osama_AlSayed";
				public static final String FUNC_ID_MOBILY_AUTH_NAJMA_WORLD ="Najma_World";
				public static final String FUNC_ID_MOBILY_AUTH_ARSAD ="Arsad";
				public static final String FUNC_ID_MOBILY_AUTH_RANA ="Mobily_Ranan";
				public static final String FUNC_ID_MOBILY_AUTH_VIDEO_PORTAL ="Video_Portal";
				public static final String FUNC_ID_MOBILY_AUTH_VIDEO_AUDIO_DEMAND ="Video_Audio_on_Demand";
				public static final String FUNC_ID_MOBILY_AUTH_TV ="Mobile_TV";
				
			    public  static final String AUTHENTICATE_AND_GO_ENTER_MSISDN = "1";
				public  static final String AUTHENTICATE_AND_GO_ENTER_PIN = "2";
				public static final String AUTHENTICATE_AND_GO_SUBSCRIBED ="3";
				public static final String AUTHENTICATE_AND_GO_PURCHASED ="4";
				
				public static final String MODULE_ID_ONLINE_MESSAGING ="24";
				
				public static final String CUSTOMER_CONSUMER_TYPE="Consumer";
				public static final String CUSTOMER_BUSINESS_TYPE="Business";

				//Added by Manav to handle new service name for National SMS Bundle
				public static final String SUPPLEMENTARY_SERVICE_TWININGS_NATIONAL = "TWININGS_NATIONAL";
			
			//For IPhone Ranan app
				public static final String FUNC_RANAN_CATEGORIES ="RANAN_CATEGORIES";
				public static final String FUNC_RANAN_TONES_LIST ="RANAN_TONES_LIST";
				public static final String FUNC_RANAN_PLAY_TONE ="RANAN_PLAY_TONE";
				public static final String FUNC_RANAN_PURCHASE_TONE ="RANAN_PURCHASE_TONE";
				public static final String FUNC_RANAN_PURCHASED_TONES_LIST ="RANAN_PURCHASED_TONES_LIST";
				public static final String FUNC_RANAN_ASSIGN_TONE ="RANAN_ASSIGN_TONE";
				public static final String FUNC_RANAN_SET_DEFAULT_TONE ="RANAN_SET_DEFAULT_TONE";
				public static final String FUNC_RANAN_DE_ASSIGN_TONE ="RANAN_DE_ASSIGN_TONE";
				public static final String FUNC_RANAN_DELETE_TONE ="RANAN_DELETE_TONE";
				public static final String FUNC_RANAN_TONES_SEARCH ="RANAN_TONES_SEARCH";

			//Devices    
			    public static final String DEVICE_IPHONE = "0";
			    public static final String DEVICE_IPAD = "1";
			    public static final String DEVICE_NOKAI = "2";
			    public static final String DEVICE_HTC = "3";	
			    public static final String DEVICE_GLOYBAS = "4";
			    public static final String DEVICE_WINDOWS7 = "5";
			    public static final String DEVICE_ANDROID = "6";
			    public static final String DEVICE_FACEBOOK = "7";
			    public static final String DEVICE_BLACKBERRY = "8";
			    public static final String DEVICE_NGSDP = "9";	
			    public static final String DEVICE_NEQATY = "10";
			    public static final String DEVICE_WINDOWS8	= "11";
			    public static final String DEVICE_TOOLBAR	= "12";
				public static final String DEVICE_MOBILY_WEBSITE	= "100";

			    
			//For Footprint
				static final int FOOT_PRINT_LOGIN_EVENT_ID = 142;
				static final int FOOT_PRINT_LOGOUT_EVENT_ID = 143;

			    
			    
			    

			// For SignIn Revamp
				public static final int SKY_EMAIL_STATUS_UNDEFINED            = -1;
				public static final int SKY_EMAIL_STATUS_NEVER_CREATED_BEFORE = 0;
				public static final int SKY_EMAIL_STATUS_ACTIVE               = 1;
				public static final int SKY_EMAIL_STATUS_GRACE                = 2;
				public static final int SKY_EMAIL_STATUS_SUSPENDED            = 3;
				public static final int SKY_EMAIL_STATUS_DELETED              = 4;
				public static final int SKY_EMAIL_STATUS_SCHEDULED_TO_CREATE  = 5;
				
				public static final int SKY_TYPE_UNDEFINED                 = -1;
				public static final int SKY_TYPE_NO_INTERNET_BUNDLE        = 0;
				public static final int SKY_TYPE_INTERNET_BUNDLE_5M        = 1;
				public static final int SKY_TYPE_INTERNET_BUNDLE_20M       = 2;
				public static final int SKY_TYPE_INTERNET_BUNDLE_UNLIMITED = 3;
				public static final int SKY_TYPE_INTERNET_BUNDLE_1G        = 4;
				public static final int SKY_TYPE_INTERNET_BUNDLE_5G        = 5;
				public static final int SKY_TYPE_INTERNET_BUNDLE_30M       = 6;
				public static final int SKY_TYPE_INTERNET_BUNDLE_100M      = 7;
				public static final int SKY_TYPE_MOBILY_CONNECT            = 8;
				
				//registration status
				public static final String USER_REGISTRATION_ACTIVE="1";
				public static final String USER_REGISTRATION_INACTIVE="2";
				
				// MDM
				public static final String MDM_INQUIRY_REQUEST_QUEUENAME = "mq.mdm.inq.request.queue";
				public static final String MDM_INQUIRY_REPLY_QUEUENAME = "mq.mdm.inq.reply.queue";

				public static final String COMPANY_CUSTOMER_CATEGORY_BUSINESS = "Business";
				public static final String PREFERRED_CONTACT_METHOD = "PreferredContactMethod";
				
				//Loyalty: neqaty plus
				public static final String REQUEST_QUEUE_CORPORATE_LEVEL_INQUIRY = "mq.loyalty.request.corporate.level.inquiry";
				public static final String REPLY_QUEUE_CORPORATE_LEVEL_INQUIRY = "mq.loyalty.reply.corporate.level.reply";
				public static final String SECURITY_KEY_123 = "123";
				public static final String FUNC_ID_LOYALTY_CORPORATE_INQUIRY = "LOYALTY_CORPORATE_INQUIRY";
				
				// MDM LOV
				public static final String MDM_LOV_INQUIRY_REQUEST_QUEUENAME = "mq.mdm.lov.inq.request.queue";
				public static final String MDM_LOV_INQUIRY_REPLY_QUEUENAME = "mq.mdm.lov.inq.reply.queue";
				
				public static final String CUSTOMER_CATEGORY_TYPE_O = "O";
				public static final String CUSTOMER_CATEGORY_BUSINESS = "Business";
				public static final String VALUE_Y = "Y";
				public static final String VALUE_N = "N";
				public static final String MDM_SUCCESS_STATUS_CODE = "I000000";
				
				public static final String CALL_FWD_SR_ID_VALUE = "SR_119999";
				
			    public static final String SHARED_FLASH_FOLDER = "shared.flash.file.path";
			    public static final String INTEGRATION_IP_ADDRESS = "integration.ip.address";
			    public static final String STAGING_IP_ADDRESS = "staging.ip.address";
			    public static final String INTEGRATION_HOST = "integration.host";
			    public static final String STAGING_HOST = "staging.host";
			    public static final String PRODUCTION_HOST = "production.host";
		
		//Roaming Call 
			    public static final String ROAMING_CALL_MSG_FORMAT ="RoamingManage";
			    public static final String ROAMING_CALL_MSG_VER ="0";
			    public static final String REQ_SECURITY_INFO_VALUE = "Secure";
			    
			    public static final String KEY_ERROR_MSG = "Error Message";
			    public static final String KEY_ERROR_CODE = "Error Code";
			    public static final String KEY_ROAMING_CALL = "Roaming Calls Barring";
			    public static final String ATTRIBUTE_FIELD= "field";
			    public static final String ROAMING_CALL_LOGIN_VAL = "Eportal";
		    	public static final String ROAMING_CALL_BINARY_OPERATOR = "Binary Operator";
	    		public static final String ROAMING_CALL_IDENTIFIER = "Identifier";
	   			public static final String ROAMING_CALL_CONSTANT = "Constant";
				public static final String ROAMING_CALL_TEXT = "TEXT";
				public static final String ROAMING_CALL_INQ_FUNC_ID = "1183";
			    public static final String ROAMING_CALL_OPERATOR_AND = "AND";
			    public static final String ROAMING_CALL_OPERATOR_EQUAL = "=";
			    public static final String ROAMING_CALL_FUNCTION_ID = "Function Id";
			    public static final String ROAMING_CALL_LOGIN_ID = "Login Id";
			    public static final String ROAMING_CALL_MSISDN = "MSISDN";
			    public static final String ROAMING_CALL_INQ_REQUEST_QUEUENAME="roaming.call.inq.service.queuename.request"; //MOBILY.INQ.IN.INFO.REQUEST
			    public static final String ROAMING_CALL_INQ_REPLY_QUEUENAME="roaming.call.inq.service.queuename.reply"; //EPORTAL.INQ.IN.INFO.REPLY

			    public static final String MASTER_BA_FLAG = "MasterBillingAccountFlag";
			    public static final String BILLING_ACCOUNT = "BillingAccount";
			    			    
			    // MDM - Is Master Billing Account
			    public static final String IS_MASTER_BA_INQUIRY_REQUEST_QUEUENAME = "is.ma.inq.request.queue";
			    public static final String IS_MASTER_BA_INQUIRY_REPLY_QUEUENAME = "is.ma.inq.reply.queue";
			    
			    // MDM - Is Valid ID
			    public static final String IS_VALID_ID_REQUEST_QUEUENAME = "is.valid.id.request.queue";
			    public static final String IS_VALID_ID_REPLY_QUEUENAME = "is.valid.id.reply.queue";
			    
				public static final String LANG_ENGLISH = "E";	
				public static final String LANG_ARABIC = "A";	

				public static final String FUNC_VALIDATE_MSISDN_SIM = "VALIDATE_MSISDN_SIM";
				public static final String FUNC_VERIFY_ACTIVATION_CODE = "VERIFY_ACTIVATION_CODE";
				public static final String FUNC_CUSTOMER_DATA_UPDATE = "CUSTOMER_DATA_UPDATE";
				public static final String GET_LATEST_PROMOTION = "GET_LATEST_PROMOTION";
				
				//For IPTV
					public static String PRODUCT_FTTH = "FTTH";
					public static String IPTV_SUBSCRIBED_PACKAGE = "elife Double-Play";
					public static String STATUS_ACTIVE ="Active";
					public static String STATUS_SUSPEND ="Suspended";
					
					public static String ATTRIBUTE_BANDWIDTH ="bandwidth";
					public static String ATTRIBUTE_LOGIN_ID = "LoginId";
					public static String ATTRIBUTE_DOMAIN = "Domain";
					public static String ATTRIBUTE_PACKAGE_ID ="PackageId";
					public static String ATTRIBUTE_PACKAGE_NAME ="PackageName";
					public static String ATTRIBUTE_SERVICE_NUMBER ="ServiceNumber";
					public static String ATTRIBUTE_MAC ="MAC";
					public static String ATTRIBUTE_SN ="SN";
					public static String ATTRIBUTE_DCN ="DCN";
					public static String ATTRIBUTE_STB ="STB";
					public static String ATTRIBUTE_Ordinal ="Ordinal";
					
				//Func Id's
					public static String FUNCID_ELIFE_IPTV_SERVICE_INQUIRY = "ELIFE_IPTV_SERVICE_INQUIRY";
					

				//IPTV Inq dao
					public static final String REQUEST_QUEUE_IPTV_INQ_INQUIRY = "mq.iptv.inq.request.queue";
					public static final String REPLY_QUEUE_IPTV_INQ_INQUIRY = "mq.iptv.inq.reply.reply.queue";
	
			//For Payment Notification
					public static final String PAYMENT_NOTIFY_MESSAGE_FORMAT_VALUE = "Payment Notification";
					public static final String MESSAGE_VER_VALUE = "01";
					public static final String CHANNEL_ID_VALUE_MPOS = "MPOS";
					public static final String CHANNEL_FUNC_VALUE_PMTNOT = "PMTNOT";
					public static final String CHANNEL_ID_VALUE_EPOR = "EPOR";
					
				    public static final String CUSTOMER_PAYMENT_NOTIFY_REQUEST_QUEUENAME = "mq.customer.payment.notify.request.queue";
				    public static final String CUSTOMER_PAYMENT_NOTIFY_REPLY_QUEUENAME = "mq.customer.payment.notify.reply.queue";
				    
			//Safe Arrival
				    public static final String SAFE_ARR_MMA_FUNC_ID_VALUE ="General_MMA_Inquiry";
				    public static final String SAFE_ARR_MMA_COMMAND ="GET_SASSUB_COMPREHENSIVE";
				    public static final String SERVICE_NAME_SAFE_ARRIVAL = "SAFE_ARRIVAL";
				    public static final String SAFE_ARRIVAL_INQ_REQUEST_QUEUENAME="mq.safe.arr.inq.request.queue";
				    public static final String SAFE_ARRIVAL_INQ_REPLY_QUEUENAME="mq.safe.arr.inq.reply.reply.queue";
				    
					public static final String SUCCESS = "SUCCESS";
					public static final String ERROR_MSG = "ERROR_MSG";
					public static final int NO_SERVICE = 0;
					public static final int GENERAL_ERROR = 1;
					public static final String PORTAL_DEV_EMAIL_ADDRESS = "emailHandler.portalDev.email.address";
					public static final String PORTAL_MQ_EMAIL_ADDRESS = "emailHandler.portalMQ.email.address";
					public static final String PORTAL_ADMIN_EMAIL_ADDRESS = "emailHandler.portalAdmin.email.address";
					public static final String PORTAL_DBA_EMAIL_ADDRESS = "emailHandler.portalDBA.email.address";
					public static final String PORTAL_SENDER_EMAIL_ADDRESS = "emailHandler.sender.email.address";
					public static final String PORTAL_EMAIL_SUBJECT = "emailHandler.email.subject";
					
				    //Whats App
					public static final String WHATSAPP_MONTHLY = "WHATSAPP_MONTHLY";
					public static final String WHATSAPP_PLUS = "WHATSAPP_PLUS";
					public static final String GPRS_MONTHLY = "GPRS_MONTHLY";
					public static final String VOICE_MONTHLY = "VOICE_MONTHLY";
					public static final String DATA_MONTHLY = "DATA_MONTHLY";
					public static final int WHATSAPP_PLUS_PACKAGE = 1;
					public static final int GPRS_MONTHLY_PACKAGE = 2;
					public static final int VOICE_MONTHLY_PACKAGE = 3;
					public static final int DATA_MONTHLY_PACKAGE = 4;
					
					public static final int WHATS_APP_WEEKLY_PACKAGE_FUNC_ID = 4;
					public static final String CHANNEL_ID_EAI_VALUE = "EAI";
					public static final String WHATS_APP_PROMOTION_OVERWRITE_VALUE = "N";
					public static final String WHATS_APP_CHARGEABLE = "N";
					public static final String WHATS_APP_CHARGEABLE_AMOUNT = "0.0";
					
					public static final String MOBILY_WHATS_APP_PROMOTION_REQUEST_QUEUENAME = "mq.whats.app.promotion.request.queue";
					public static final String MOBILY_WHATS_APP_PROMOTION_REPLY_QUEUENAME = "mq.whats.app.promotion.reply.queue";	
					public static final String WHATS_APP_SERVICE_PROMOTION = "WhatsAppPromotion";
					public static final String SUPPLEMENTARY_SERVICE_PROMOTION_WHATSAPP_WEEKLY_UNSUB = "UNSUB-Whatsapp weekly Package";
					public static final String SUPPLEMENTARY_SERVICE_PROMOTION_WHATSAPP_WEEKLY_SUB = "Whatsapp weekly Package";
					public static final String SMS_SUPPLEMENTARY_DATA_REQUEST_QUEUENAME  = "mq.sms.supplementry.data.request.queue";
					public static final String SUPPLEMENTARY_DATA_PIN_CODE_SMS_ID_EN = "825078";
					public static final String SUPPLEMENTARY_DATA_PIN_CODE_SMS_ID_AR = "825077";
					//status codes from sr_status_tbl 
					public static final String STATUS_INTIAL  = "1";
					public static final String STATUS_ACCEPTED  = "2";
					public static final String STATUS_FAILED  = "3";
					public static final String STATUS_PENDING_ACTION  = "4";
					public static final String STATUS_REJECTED  = "5";
					public static final String STATUS_SUCCEDED  = "6";
					public static final String STATUS_COMPLETED_ACTION  = "7";
					public static final String STATUS_ROLLBACK = "8";
					public static final String STATUS_TO_COMPLETE = "9";
					public static final String STATUS_EXPIRED = "10";
					public static final String STATUS_SADAD_PAYMENT_FAILED = "11";
					public static final String STATUS_CC_PAYMENT_FAILED = "12";
					public static final String STATUS_CANCELLED = "13";
					
					
}