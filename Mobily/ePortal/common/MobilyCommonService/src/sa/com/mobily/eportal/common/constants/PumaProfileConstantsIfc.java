package sa.com.mobily.eportal.common.constants;

public interface PumaProfileConstantsIfc
{
	String cn = "cn";

	String sn = "sn";

	String uid = "uid";

	String defaultMsisdn = "defaultMsisdn";

	String email = "email";

	String preferredLanguage = "preferredLanguage";

	String lineType = "lineType";

	String serviceAccountNumber = "serviceAccountNumber";

	String defaultSubscriptionType = "defaultSubscriptionType";
	
	String defaultSubSubscriptionType = "defaultSubSubscriptionType";

	String defaultSubId = "defaultSubId";

	String corpMasterAcctNo = "corpMasterAcctNo";

	String corpAPNumber = "corpAPNumber";

	String corpSurveyCompleted = "corpSurveyCompleted";

	String id = "id";

	String activationcode = "activationcode";

	String password = "password";

	String iqama = "iqama";

	String givenName = "givenName";

	String Status = "Status";

	String firstName = "firstName";

	String lastName = "lastName";

	String custSegment = "custSegment";

	String roleId = "roleId";

	String SecurityQuestion = "SecurityQuestion";

	String SecurityAnswer = "SecurityAnswer";

	String birthDate = "birthDate";

	String nationality = "nationality";

	String Gender = "Gender";

	String registerationDate = "registerationDate";

	String commercialRegisterationID = "commercialRegisterationID";

	String corpAPIDNumber = "corpAPIDNumber";
	
	String facebookId = "facebookId";
	
	String twitterId = "twitterId";
	
	String registrationSourceId = "registrationSourceId";
	
	String userAcctReference = "UserAcctReference";
	
	String contactNumber = "contactNumber";
	
	String passwordChangedTime = "pwdChangedTime";
	
	
}