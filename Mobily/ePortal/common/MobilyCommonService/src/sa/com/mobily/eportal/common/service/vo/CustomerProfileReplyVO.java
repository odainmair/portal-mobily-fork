package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;

/**
 * This class is a data bean of CustomerProfile Request
 */

public class CustomerProfileReplyVO extends BaseVO 
{
    

    protected AccountVO  account      = new AccountVO ();
    protected ArrayList  serviceLines = new ArrayList();
    
    //added by Suresh V on June 29 2010
    private String rootBillingAccountNumber = null; // Root or department billing account number of corporate account
    private String companyRowId = null; // row id of corporate account
    
	private ContactMediumVO contactMediumVO = null;
	private ContactPersonVO contactPersonVO = null;
	private PartyExtensionsVO partyExtensionsVO = null;
	private PartyIdentificationVO partyIdentificationVO = null;
	private SpecificationVO specificationVO = null;
	private CharacteristicsVO characteristicsVO = null;
	private String accountType = null;
	private String isMasterBilingAccount = null;
	private String customerId = null;
	
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public CharacteristicsVO getCharacteristicsVO() {
		return characteristicsVO;
	}
	public void setCharacteristicsVO(CharacteristicsVO characteristicsVO) {
		this.characteristicsVO = characteristicsVO;
	}
	public ContactMediumVO getContactMediumVO() {
		return contactMediumVO;
	}
	public void setContactMediumVO(ContactMediumVO contactMediumVO) {
		this.contactMediumVO = contactMediumVO;
	}
	public ContactPersonVO getContactPersonVO() {
		return contactPersonVO;
	}
	public void setContactPersonVO(ContactPersonVO contactPersonVO) {
		this.contactPersonVO = contactPersonVO;
	}
	public PartyExtensionsVO getPartyExtensionsVO() {
		return partyExtensionsVO;
	}
	public void setPartyExtensionsVO(PartyExtensionsVO partyExtensionsVO) {
		this.partyExtensionsVO = partyExtensionsVO;
	}
	public PartyIdentificationVO getPartyIdentificationVO() {
		return partyIdentificationVO;
	}
	public void setPartyIdentificationVO(PartyIdentificationVO partyIdentificationVO) {
		this.partyIdentificationVO = partyIdentificationVO;
	}
	public SpecificationVO getSpecificationVO() {
		return specificationVO;
	}
	public void setSpecificationVO(SpecificationVO specificationVO) {
		this.specificationVO = specificationVO;
	}
        
    /**
	 * @return the rootBillingAccountNumber
	 */
	public String getRootBillingAccountNumber() {
		return rootBillingAccountNumber;
	}
	/**
	 * @param rootBillingAccountNumber the rootBillingAccountNumber to set
	 */
	public void setRootBillingAccountNumber(String rootBillingAccountNumber) {
		this.rootBillingAccountNumber = rootBillingAccountNumber;
	}
	/**
	 * @return the companyRowId
	 */
	public String getCompanyRowId() {
		return companyRowId;
	}
	/**
	 * @param companyRowId the companyRowId to set
	 */
	public void setCompanyRowId(String companyRowId) {
		this.companyRowId = companyRowId;
	}
	/**
     * @return Returns the account.
     */
    public AccountVO getAccount()
    {
        return account;
    }
    /**
     * @param account The account to set.
     */
    public void setAccount(AccountVO account)
    {
        this.account = account;
    }
  
    /**
     * @return Returns the serviceLine.
     */
    public ArrayList getServiceLines()
    {
        return serviceLines;
    }
    /**
     * @param serviceLine The serviceLine to set.
     */
    public void setServiceLines(ArrayList serviceLines)
    {
        this.serviceLines = serviceLines;
    }
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getIsMasterBilingAccount() {
		return isMasterBilingAccount;
	}
	public void setIsMasterBilingAccount(String isMasterBilingAccount) {
		this.isMasterBilingAccount = isMasterBilingAccount;
	}
    
}
