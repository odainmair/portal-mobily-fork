package sa.com.mobily.eportal.corporate.model;

import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MobilyPackageCatalogLookUp extends BaseVO
{
	private static final long serialVersionUID = 2969204594998876584L;

	private String PACKAGE_ID;

	private String PACKAGE_NAME;

	private String PACKAGE_NAME_AR;

	private String PACKAGE_NAME_EN;

	private String LINE_OF_BUSINESS;

	private String PRODUCT_FAMILY;

	private String COMMERCIAL_PLAN_TYPE;

	private String PLAN_TYPE;

	private Date UPDATE_DATE;

	
	private String PACKAGE_GLOBAL_ID;
	
	public String getPACKAGE_ID()
	{
		return PACKAGE_ID;
	}

	public void setPACKAGE_ID(String pACKAGE_ID)
	{
		PACKAGE_ID = pACKAGE_ID;
	}

	public String getPACKAGE_NAME()
	{
		return PACKAGE_NAME;
	}

	public void setPACKAGE_NAME(String pACKAGE_NAME)
	{
		PACKAGE_NAME = pACKAGE_NAME;
	}

	public String getPACKAGE_NAME_AR()
	{
		return PACKAGE_NAME_AR;
	}

	public void setPACKAGE_NAME_AR(String pACKAGE_NAME_AR)
	{
		PACKAGE_NAME_AR = pACKAGE_NAME_AR;
	}

	public String getPACKAGE_NAME_EN()
	{
		return PACKAGE_NAME_EN;
	}

	public void setPACKAGE_NAME_EN(String pACKAGE_NAME_EN)
	{
		PACKAGE_NAME_EN = pACKAGE_NAME_EN;
	}

	public String getLINE_OF_BUSINESS()
	{
		return LINE_OF_BUSINESS;
	}

	public void setLINE_OF_BUSINESS(String lINE_OF_BUSINESS)
	{
		LINE_OF_BUSINESS = lINE_OF_BUSINESS;
	}

	public String getPRODUCT_FAMILY()
	{
		return PRODUCT_FAMILY;
	}

	public void setPRODUCT_FAMILY(String pRODUCT_FAMILY)
	{
		PRODUCT_FAMILY = pRODUCT_FAMILY;
	}

	public String getCOMMERCIAL_PLAN_TYPE()
	{
		return COMMERCIAL_PLAN_TYPE;
	}

	public void setCOMMERCIAL_PLAN_TYPE(String cOMMERCIAL_PLAN_TYPE)
	{
		COMMERCIAL_PLAN_TYPE = cOMMERCIAL_PLAN_TYPE;
	}

	public String getPLAN_TYPE()
	{
		return PLAN_TYPE;
	}

	public void setPLAN_TYPE(String pLAN_TYPE)
	{
		PLAN_TYPE = pLAN_TYPE;
	}

	public Date getUPDATE_DATE()
	{
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(Date uPDATE_DATE)
	{
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getPACKAGE_GLOBAL_ID()
	{
		return PACKAGE_GLOBAL_ID;
	}

	public void setPACKAGE_GLOBAL_ID(String pACKAGE_GLOBAL_ID)
	{
		PACKAGE_GLOBAL_ID = pACKAGE_GLOBAL_ID;
	}
	
	
}