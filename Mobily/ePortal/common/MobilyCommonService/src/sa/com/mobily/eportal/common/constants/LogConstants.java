package sa.com.mobily.eportal.common.constants;

public interface LogConstants{
	
	public static final String LOG_PORTLET_CLASS = "portlet-class";
	public static final String LOG_PORTLET_METHOD = "portlet-method";
	public static final String LOG_EXCEPTION_OCCURED = "exceptionOccured";
	public static final String YES = "YES";

}
