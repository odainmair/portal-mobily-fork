/*
 * Created on Sep 20, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;


import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.AppConfig;
import sa.com.mobily.eportal.common.service.util.AppConfigKeysIfc;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;


/**
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class BillingInfoDAO {

    private static final Logger log = LoggerInterface.log;
    
	public static String getBillingAccountPOID(String aMSISDN) throws Exception {
		log.info("BillingInfoDAO > getBillingAccountPOID(aMSISDN) > start :: aMSISDN :: "+aMSISDN);
		StringBuffer tempQuery = null;
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		Connection connection = null;
		ArrayList list = new ArrayList();
		String accountPOID = null;
		
		try {
			//connection = DataBaseHandler.getConnection(AppConfig.getInstance()
					//.get(AppConfigKeysIfc.BILLING_PORTAL_DATA_SOURCE_NAME));
		    //connection = DataBaseHandler.getConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BILL));
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BILL));
		
			log.debug("getBillingAccountPOID" + connection.toString());
		
			//	ACCOUNT POID
			tempQuery = new StringBuffer("SELECT st.account_obj_id0 ");
			tempQuery
					.append("FROM service_alias_list_t sa, service_t st ");
			tempQuery.append("WHERE sa.obj_id0 = st.poid_id0 ");
			tempQuery.append("AND st.poid_type = '/service/telco/gsm/telephony' ");
			tempQuery.append("AND sa.NAME = ? ");
		
			preparedStatement = connection.prepareStatement(tempQuery.toString());
			preparedStatement.setString(1, aMSISDN);
			rs = preparedStatement.executeQuery();
			if (rs.next()) {
				accountPOID = rs.getString("account_obj_id0");
				log.debug("Bill Summary account poid = " + accountPOID);
			}
		
			/*
			 * else { log.debug("AccountObjId was Not Found"); throw new
			 * BillNotFoundException();//BillNotFoundException }
			 */
			}catch(SQLException e){
	            log.fatal("SQLException " + e.getMessage());
	            throw new SystemException(e);
	             
	         }
	        catch(Exception e){
	            log.debug("Exception " + e.getMessage());
	           throw new SystemException(e);
	            
	        }
		finally{
		    JDBCUtility.closeJDBCResoucrs(connection , preparedStatement , rs);
		}
		return accountPOID;
	}
	
public static HashMap getBillingAccountPOIDList(ArrayList aMSISDNList) throws Exception {
		
		
		HashMap resultHash = new HashMap();
		
		log.info("BillingInfoDAO > getBillingAccountPOIDList(aMSISDNList) > start :: aMSISDNList :: "+aMSISDNList);
		if(aMSISDNList != null && aMSISDNList.size() > 0) {
		    StringBuffer tempQuery = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			Connection connection = null;
			//ArrayList list = new ArrayList();
			//String accountPOID = null;
			try {
			    
				//connection = DataBaseHandler.getConnection(AppConfig.getInstance()
						//.get(AppConfigKeysIfc.BILLING_PORTAL_DATA_SOURCE_NAME));
			   // connection = DataBaseHandler.getConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BILL));
				
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BILL));
				log.debug("getBillingAccountPOIDList" + connection.toString());
			
				//	ACCOUNT POID
				tempQuery = new StringBuffer("SELECT st.account_obj_id0, sa.NAME ");
				tempQuery
						.append("FROM service_alias_list_t sa, service_t st ");
				tempQuery.append("WHERE sa.obj_id0 = st.poid_id0 ");
				tempQuery
						.append("AND st.poid_type = '/service/telco/gsm/telephony' ");
				//tempQuery.append("AND sa.NAME in(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				
				tempQuery.append("AND sa.NAME IN(");
				
				if(aMSISDNList.size() > 0)
				    tempQuery.append("?");
				
				for(int i=1;i<aMSISDNList.size();i++)
				    tempQuery.append(",?");
				
				tempQuery.append(")");
				
				log.debug("BillingInfoDAO > getBillingAccountPOIDList(aMSISDNList) > query = "+tempQuery.toString());
			
				preparedStatement = connection.prepareStatement(tempQuery.toString());
				
				for(int i=1;i<=aMSISDNList.size();i++)
				    preparedStatement.setString(i, aMSISDNList.get(i-1).toString());
				resultSet = preparedStatement.executeQuery();
				log.debug("BillingInfoDAO > getBillingAccountPOIDList > Excuted");
				while(resultSet.next()) {
				    resultHash.put(resultSet.getString("NAME"), resultSet.getString("account_obj_id0"));
				}
				
				
				
	//			int trails = aMSISDNList.size() / 20;
	//			int i=0;
	//			int j=0;
	//			for(i=0;i<trails;i++)
	//			{
	//				for(j=1;j<=20;j++)
	//				{
	//					preparedStatement.setString(j, aMSISDNList.get((i*20)+j-1).toString());
	//				}
	//
	//				rs = preparedStatement.executeQuery();
	//				while(rs.next()) {
	//					resultHash.put(rs.getString("NAME"), rs.getString("account_obj_id0"));
	//				}
	//			}
	//			int remains = aMSISDNList.size() % 20;
	//			if(remains > 0)
	//			{
	//				int k=0;
	//				for(k=1;k<=remains; k++)
	//					preparedStatement.setString(k, aMSISDNList.get((i*20)+k-1).toString());
	//				for(j=k+1;j<=20;j++)
	//					preparedStatement.setString(j, aMSISDNList.get(aMSISDNList.size()-1).toString());
	//
	//				rs = preparedStatement.executeQuery();
	//				while(rs.next()) {
	//					resultHash.put(rs.getString("NAME"), rs.getString("account_obj_id0"));
	//				}				
	//			}
			
				
			}catch(SQLException e){
	            log.fatal("BillingInfoDAO > getBillingAccountPOIDList(aMSISDNList) > SQLException " + e);
	            throw new SystemException(e);
	             
	         }
	        catch(Exception e){
	            log.debug("BillingInfoDAO > getBillingAccountPOIDList(aMSISDNList) > Exception " + e);
	           throw new SystemException(e);
	        }
			finally {
				JDBCUtility.closeJDBCResoucrs(connection,preparedStatement,resultSet);
		
			}
		}
		log.info("BillingInfoDAO > getBillingAccountPOIDList(aMSISDNList) > end :: resultHash :: "+resultHash);
		return resultHash;
	}
	
//new method added for mobily raqi customers by geetha krishnamurthy on 16 June 09
/*** method to get the poid id from the billing DB
 * @param msisdn
 * @return account id
 * @throws Exception
 */
public static String getBillingAccountPOIDForRaqi(String aMSISDN) throws  Exception {
	log.info("BillingInfoDAO > getBillingAccountPOIDForRaqi(aMSISDN) > start :: aMSISDN :: "+aMSISDN);
	StringBuffer tempQuery = null;
	PreparedStatement preparedStatement = null;
	ResultSet rs = null;
	Connection connection = null;
	ArrayList list = new ArrayList();
	String accountPOID = null;
	
	try {
		//connection = DataBaseHandler.getConnection(AppConfig.getInstance()
				//.get(AppConfigKeysIfc.BILLING_PORTAL_DATA_SOURCE_NAME));
	    
	    //connection = DataBaseHandler.getConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BILL));
		
		connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_BILL)); 
	
		log.debug("getBillingAccountPOIDForRaqi" + connection.toString());
	
		//	ACCOUNT POID for RAQI customers
		

		tempQuery = new StringBuffer("SELECT substr(lineage,(instr(a.lineage,':',1,1)+1),instr(a.lineage,'/',2,1) - instr(a.lineage,':',1,1)-1)");
		tempQuery.append(" silver_parent_poid  FROM account_T a, service_alias_list_t sa, service_t st");
		tempQuery.append(" WHERE sa.obj_id0 = st.poid_id0 ");
		tempQuery.append(" and a.poid_id0 = st.account_obj_id0");
		tempQuery.append(" AND st.poid_type = '/service/telco/gsm/telephony' ");
		tempQuery.append(" AND sa.NAME = ? ");

		log.debug(tempQuery.toString());
		preparedStatement = connection.prepareStatement(tempQuery.toString());
		preparedStatement.setString(1, aMSISDN);
		rs = preparedStatement.executeQuery();
		if (rs.next()) {
			accountPOID = rs.getString("silver_parent_poid");
			log.debug("Bill Summary account poid for raqi customer " + accountPOID);
		}
		}
	catch(SQLException e){
        log.fatal("SQLException " + e.getMessage());
        throw new SystemException(e);
         
     }
    catch(Exception e){
        log.debug("Exception " + e.getMessage());
       throw new SystemException(e);
        
    }
	finally{
	    JDBCUtility.closeJDBCResoucrs(connection , preparedStatement , rs);
	}
	return accountPOID;
}
}
