package sa.com.mobily.eportal.billing.service.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.BillPaymentConstants;
import sa.com.mobily.eportal.billing.constants.BillingConstantsIfc;
import sa.com.mobily.eportal.billing.constants.ConstantsIfc;
import sa.com.mobily.eportal.billing.util.AppConfig;
import sa.com.mobily.eportal.billing.vo.LastBillReplyVO;
import sa.com.mobily.eportal.billing.vo.LastBillRequestVO;
import sa.com.mobily.eportal.billing.xml.GetLastBillsXMLHandler;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.CustomerServiceUtil;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;

public final class BillPaymentJMSRepository implements BillPaymentConstants
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.PAYMENT_SERVICE_LOGGER_NAME);

	private static final String className = BillPaymentJMSRepository.class.getName();

	public LastBillReplyVO getLastBills(LastBillRequestVO requestVO, String userId)
	{
		String method = "getLastBills";
		executionContext.log(Level.INFO,
				"BillPaymentJMSRepository > getLastBills : start for msisdn [" + requestVO.getMsisdn() + "] or service account number [" + requestVO.getServiceAccountNumber()
						+ "]", className, method);
		LastBillReplyVO replyVO = null;
		MQAuditVO mqAuditVO = new MQAuditVO();
		try
		{
			String requestQueueName = AppConfig.getInstance().get(BillingConstantsIfc.REQUEST_QUEUE_LAST_BILLS).trim();
			String replyQueueName = AppConfig.getInstance().get(BillingConstantsIfc.REPLY_QUEUE_LAST_BILLS).trim();
			executionContext.log(Level.INFO, "BillPaymentJMSRepository > getLastBills : requestQueueName[" + requestQueueName + "]", className, method);
			executionContext.log(Level.INFO, "BillPaymentJMSRepository > getLastBills : replyQueueName[" + replyQueueName + "]", className, method);
			mqAuditVO.setRequestQueue(requestQueueName);
			mqAuditVO.setReplyQueue(replyQueueName);
			mqAuditVO.setUserName(userId);
			if(FormatterUtility.isNotEmpty(requestVO.getMsisdn())){
				mqAuditVO.setMsisdn(requestVO.getMsisdn());
			}else{
				mqAuditVO.setMsisdn(requestVO.getServiceAccountNumber());
			}
			mqAuditVO.setFunctionName(BillingConstantsIfc.FUN_GET_LAST_BILLS_INQ);
			// Populate Header
			ConsumerRequestHeaderVO headerVO = new CustomerServiceUtil().populateRequestHeaderVO(BillingConstantsIfc.FUN_GET_LAST_BILLS_INQ, userId);
			headerVO.setChannelTransId(RandomGenerator.generateTransactionID());
			requestVO.setHeaderVO(headerVO);

			String strXMLRequest = new GetLastBillsXMLHandler().generateLastBillsReqXML(requestVO);
			executionContext.log(Level.INFO, "BillPaymentJMSRepository > getLastBills : strXMLRequest: " + strXMLRequest, className, method);
			mqAuditVO.setMessage(strXMLRequest);
			// For Testing
			// String strXMLReply =
			// "<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><MsgFormat>LastBillsInq</MsgFormat><MsgVersion>0000</MsgVersion><RequestorChannelId>EPortal</RequestorChannelId><RequestorChannelFunction>LastBillsInq</RequestorChannelFunction><RequestorUserId/><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ChannelTransId>EPOR_###############</ChannelTransId><Status>3/6</Status><ErrorCode>0000</ErrorCode><ErrorMessage>Success</ErrorMessage></SR_HEADER_REPLY><MSISDN>12345</MSISDN><ServiceAccountNumber>56789</ServiceAccountNumber><AccountPOID>09876</AccountPOID><Results><Result><BillPOID>3456</BillPOID><BillNumber>1122</BillNumber><CreatedDate>20131108123930</CreatedDate><StartDate>20131108123930</StartDate><EndDate>20131109123930</EndDate><AmountDue>100.50</AmountDue><TotalAmountDue>500.50</TotalAmountDue></Result><Result><BillPOID>9955</BillPOID><BillNumber>2354</BillNumber><CreatedDate>20131008123930</CreatedDate><StartDate>20131010123930</StartDate><EndDate>20131009123930</EndDate><AmountDue>200.50</AmountDue><TotalAmountDue>700.50</TotalAmountDue></Result><Result><BillPOID>8643</BillPOID><BillNumber>4444</BillNumber><CreatedDate>20130908123930</CreatedDate><StartDate>20130908123930</StartDate><EndDate>20130909123930</EndDate><AmountDue>800.50</AmountDue><TotalAmountDue>5090.50</TotalAmountDue></Result><Result><BillPOID>876765</BillPOID><BillNumber>2223</BillNumber><CreatedDate>20130808123930</CreatedDate><StartDate>20130808123930</StartDate><EndDate>20130809123930</EndDate><AmountDue>600.50</AmountDue><TotalAmountDue>5600.50</TotalAmountDue></Result></Results></MOBILY_BSL_SR_REPLY>";
			//String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
			String strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
			executionContext.log(Level.INFO, "BillPaymentJMSRepository > getLastBills : strXMLReply: " + strXMLReply, className, method);
			mqAuditVO.setReply(strXMLReply);
			if (!FormatterUtility.isEmpty(strXMLReply)){
				replyVO = new GetLastBillsXMLHandler().parseLastBillsReplyXML(strXMLReply);
				mqAuditVO.setErrorMessage(replyVO.getHeaderVO().getErrorMessage());
				mqAuditVO.setServiceError(replyVO.getHeaderVO().getErrorCode());
				MQUtility.saveMQAudit(mqAuditVO);
			}else{
				mqAuditVO.setErrorMessage(ConstantsIfc.INVALID_REPLY);
				mqAuditVO.setServiceError(ConstantsIfc.INVALID_REPLY);
				MQUtility.saveMQAudit(mqAuditVO);
			}

		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception " + e.getMessage(), className, method, e);
			throw new SystemException(e);
		}
		executionContext.endMethod(className, method, replyVO);

		return replyVO;
	}
}