package sa.com.mobily.eportal.common.jms.nativejms;

import java.util.Collections;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJBHome;
import javax.ejb.EJBLocalHome;
import javax.jms.ConnectionFactory;
import javax.jms.QueueConnectionFactory;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;

/**
 * This class is an implementation of the Service Locator pattern. It is used to
 * Lookup resources such as EJBHomes, JMS Destinations, etc. This
 * implementation uses the "singleton" strategy and also the "caching" strategy.
 * This implementation is intended to be used on the web tier and not on the ejb
 * tier.
 */
public class ServiceLocator {

	private InitialContext ic;
	private static ServiceLocator me;

	static {
		try {
			me = new ServiceLocator();
		} catch (Exception se) {
			System.err.println(se);
			se.printStackTrace(System.err);
		}
	}

	private ServiceLocator() throws MobilyApplicationException {
		try {
			ic = new InitialContext();
		} catch (NamingException ne) {
			throw new MobilyApplicationException(ne);
		} catch (Exception e) {
			throw new MobilyApplicationException(e);
		}
	}

	static public ServiceLocator getInstance() {
		return me;
	}

	/**
	 * @return the factory for the factory to get queue connections from
	 */
	public ConnectionFactory getQueueConnectionFactory(
			String qConnFactoryName) throws MobilyApplicationException {
		ConnectionFactory factory = null;
		try {
			factory = (ConnectionFactory) ic.lookup(qConnFactoryName);
		} catch (NamingException ne) {
			throw new MobilyApplicationException(ne);
		} catch (Exception e) {
			throw new MobilyApplicationException(e);
		}
		return factory;
	}

	/**
	 * @return the Queue Destination to send messages to
	 */
	public Queue getQueue(String queueName) throws MobilyApplicationException {
		Queue queue = null;
		try {
			queue = (Queue) ic.lookup(queueName);
		} catch (NamingException ne) {
			throw new MobilyApplicationException(ne);
		} catch (Exception e) {
			throw new MobilyApplicationException(e);
		}

		return queue;
	}
}