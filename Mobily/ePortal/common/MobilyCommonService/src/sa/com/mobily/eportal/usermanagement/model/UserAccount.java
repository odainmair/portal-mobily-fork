package sa.com.mobily.eportal.usermanagement.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

//import org.apache.openjpa.persistence.DataCache;


/**
 * The persistent class for the USER_ACCOUNTS database table.
 * 
 */
@Entity
@Table(name="USER_ACCOUNTS")
//@DataCache(enabled=false)
//@NamedQuery(name="UserAccount.findByContactId", query="SELECT acc from sa.com.mobily.eportal.usermanagement.model.UserAccount acc WHERE acc.id = :id")
@NamedQueries({
    @NamedQuery(name=UserAccount.FIND_USER_BY_NAME, query="SELECT userAccount FROM UserAccount userAccount WHERE LOWER(userAccount.userName) = LOWER(:UserName)")
})
public class UserAccount implements Serializable {
	
	public static final String FIND_USER_BY_NAME = "UserAccount.FindUserByName";
	
	public static final String USER_NAME = "UserName";

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_ACCOUNT_SEQ")
	@SequenceGenerator(name="USER_ACCOUNT_SEQ", sequenceName="USER_ACCOUNT_SEQ")
	@Column(name="USER_ACCT_ID")
	@NotNull
	private long userAcctId;

	@Column(name="ACTIVATION_DATE")
	private Timestamp activationDate;
	
	@Column(name="CREATED_DATE")
	@NotNull
	private Timestamp createdDate;
	
	@NotNull
	@Size(min=1, max=30,message="iqama can not be null and empty")
	private String iqama;

	@Column(name="LAST_LOGIN_TIME")
	private Timestamp lastLoginTime;

	@Column(name="USER_NAME")
	@NotNull
	@Size(min=1, max=100,message="userName can not be null and empty")
	private String userName;

	//bi-directional many-to-one association to UserSubscription
	@OneToMany(mappedBy="userAccount",cascade=CascadeType.ALL)
	
	private Set<UserSubscription> userSubscriptions;

    public UserAccount() {
    }

	public long getUserAcctId() {
		return this.userAcctId;
	}

	public void setUserAcctId(long userAcctId) {
		this.userAcctId = userAcctId;
	}

	public Timestamp getActivationDate() {
		return this.activationDate;
	}

	public void setActivationDate(Timestamp activationDate) {
		this.activationDate = activationDate;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getIqama() {
		return this.iqama;
	}

	public void setIqama(String iqama) {
		this.iqama = iqama;
	}

	public Timestamp getLastLoginTime() {
		return this.lastLoginTime;
	}

	public void setLastLoginTime(Timestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<UserSubscription> getUserSubscriptions() {
		return this.userSubscriptions;
	}

	public void setUserSubscriptions(Set<UserSubscription> userSubscriptions) {
		this.userSubscriptions = userSubscriptions;
	}
	
	@Override
	public String toString(){
		return "userAcctId "+ userAcctId + " iqama " + iqama;
	}

	@Override
	public boolean equals(Object o) {
		boolean equal = false;
		
		UserAccount target = (UserAccount)o;
		
		if ( target != null ){
			if ( target.getUserAcctId() == this.getUserAcctId() ){
				equal = true;
			}
		}
		return equal;
	}

	@Override
	public int hashCode() {
		return (int)this.getUserAcctId() * this.getIqama().hashCode();
	}
	
	
	
}