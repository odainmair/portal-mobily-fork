/*
 * Created on Jun 1, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.AuthenticateAndGoDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.AuthenticateAndGoVO;



/**
 * @author Suresh V - MIT
 * OracleAuthenticateAndGoDAO
 * Description: This calss provides the implementation of the AuthenticateAndGoDAO 
 */
public class OracleAuthenticateAndGoDAO implements AuthenticateAndGoDAO{

    private static final Logger log = LoggerInterface.log;
    
    
    /**
    	date: Jun 1, 2009
    	Description: To insert the activation code into Portal DB. This methods will insert the new record into AUTHENTICATE_GO_ACTIVATION_TBL.
        @param authenticateAndGoVO . Mobily number, module id and activation codes are mandatory in the parameter authenticateAndGoVO
        @return boolean. true if insertion is success else false.
     */
    
    public boolean insertActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
        
        log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> insertActivationCode : start");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean isInserted =  false;
		int status;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			String insertActivationCodeSql =  "INSERT INTO AUTHENTICATE_GO_ACTIVATION_TBL (MODULE_ID,MSISDN,ACTIVATION_CODE,EXPIRATION_DATE) VALUES (?,?,?,CURRENT_TIMESTAMP)";
			preparedStatement = connection.prepareStatement(insertActivationCodeSql);
			preparedStatement.setInt(1,Integer.parseInt(authenticateAndGoVO.getModuleId()));
			preparedStatement.setString(2,authenticateAndGoVO.getMobilyNumber());
			preparedStatement.setString(3,authenticateAndGoVO.getActivationCode());
			status = preparedStatement.executeUpdate();
			log.debug("MobilyCommonService-->OracleAuthenticateAndGoDAO-->insertActivationCode :status is :"+ status);
			if (status > 0) 
				isInserted =  true;
		} catch (SQLException e) {
			log.fatal("MobilyCommonService-->OracleAuthenticateAndGoDAO-->insertActivationCode : SQLException "+ e.getMessage());
			throw new SystemException(e);
		}catch (NullPointerException e) {
		    log.fatal("MobilyCommonService-->OracleAuthenticateAndGoDAO-->insertActivationCode : NullPointerException "+ e);
			throw new SystemException(e);
        }catch (NumberFormatException e) {
            log.fatal("MobilyCommonService-->OracleAuthenticateAndGoDAO-->insertActivationCode : NumberFormatException "+ e);
			throw new SystemException(e);
        } 
		finally {
				JDBCUtility.closeJDBCResoucrs(connection, preparedStatement,null);
		}
		log.info("MobilyCommonService-->OracleAuthenticateAndGoDAO-->insertActivationCode : end");
		return isInserted;
    }
    
    /**
    	date: Jun 1, 2009
    	Description: This method is used to update the activation code
        @param authenticateAndGoVO. Mobily number, module id and activation codes are mandatory in the parameter authenticateAndGoVO
        @return boolean . true if updated successfully else false.
     */
    
    public boolean updateActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
        log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCode : start");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean isUpdated =  false;
		int status = 0;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			String updateActivationCodeSql = "UPDATE AUTHENTICATE_GO_ACTIVATION_TBL SET ACTIVATION_CODE = ?, EXPIRATION_DATE = CURRENT_TIMESTAMP WHERE MSISDN = ? AND MODULE_ID = ?";
			
			preparedStatement = connection.prepareStatement(updateActivationCodeSql);
			preparedStatement.setString(1,authenticateAndGoVO.getActivationCode());
			preparedStatement.setString(2,authenticateAndGoVO.getMobilyNumber());
			preparedStatement.setInt(3,Integer.parseInt(authenticateAndGoVO.getModuleId()));
			status = preparedStatement.executeUpdate();
			log.debug("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCode :status ="+ status);
			if (status > 0) 
				isUpdated =  true;

		} catch (SQLException e) {
			log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCode :: SQLException "+ e);
			throw new SystemException(e);
		}catch (NullPointerException e) {
		    log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCode :: NullPointerException "+ e);
			throw new SystemException(e);
        }catch (NumberFormatException e) {
            log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCode :: NumberFormatException "+ e);
			throw new SystemException(e);
        }
		finally {
				JDBCUtility.closeJDBCResoucrs(connection, preparedStatement,null);
		}
		log.info("MobilyCommonService-->OracleAuthenticateAndGoDAO-->updateActivationKey : end");
		return isUpdated;
    }
    
    /**
	date: Jun 1, 2009
	Description: To delete the Activation code from AUTHENTICATE_GO_ACTIVATION_TBL of portal DB. This method is used for Authenticate and go applications.
    @param authenticateAndGoVO :: Mobily number and module id are mandatory values in the Parameter authenticateAndGoVO
    @return boolean . true if deleted successfully else false.
    @author Suresh MIT
 */
	public boolean deleteActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
	    log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> deleteActivationCode :: start for msisdn "+authenticateAndGoVO.getMobilyNumber());
		
	    Connection connection = null;
		PreparedStatement preparedStatement = null;
		boolean isDeleted =  false;
		int status = 0;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			String deleteActCodeSQL = "DELETE FROM AUTHENTICATE_GO_ACTIVATION_TBL WHERE MSISDN = ? AND MODULE_ID = ?";
			
			preparedStatement = connection.prepareStatement(deleteActCodeSQL);
			preparedStatement.setString(1,authenticateAndGoVO.getMobilyNumber());
			preparedStatement.setInt(2,Integer.parseInt(authenticateAndGoVO.getModuleId()));
			status = preparedStatement.executeUpdate();
			
			log.debug("MobilyCommonService --> OracleAuthenticateAndGoDAO --> deleteActivationCode :: status ="+ status);
			
			if (status > 0) 
			    isDeleted =  true;
			
	
		} catch (SQLException e) {
			log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> deleteActivationCode :: SQLException "+ e);
			throw new SystemException(e);
		}catch (NullPointerException e) {
		    log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> deleteActivationCode :: NullPointerException "+ e);
			throw new SystemException(e);
        }catch (NumberFormatException e) {
            log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> deleteActivationCode :: NumberFormatException "+ e);
			throw new SystemException(e);
        }
		finally {
				JDBCUtility.closeJDBCResoucrs(connection, preparedStatement,null);
		}
		
		log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> deleteActivationCode : end");
		return isDeleted;
	}
	
	/**
		date: Jun 1, 2009
		Description: This method is used to check weather activation code is already exists in DB for the provided MSISD againest module
	    @param authenticateAndGoVO. Mobily number and module id are mandatory in the parameter authenticateAndGoVO
	    @return boolean. true if exist else false.
	 */
	
	 public boolean isActivationCodeExist(AuthenticateAndGoVO authenticateAndGoVO) {
        log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> isActivationCodeExist : start");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean isAlreadyRegistered = false;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			String selectActivationCodeSql = "SELECT ACTIVATION_CODE FROM AUTHENTICATE_GO_ACTIVATION_TBL WHERE MSISDN = ? AND MODULE_ID = ? ";
			
			preparedStatement = connection.prepareStatement(selectActivationCodeSql);
			preparedStatement.setString(1,authenticateAndGoVO.getMobilyNumber());
			preparedStatement.setInt(2,Integer.parseInt(authenticateAndGoVO.getModuleId()));
			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next())
				isAlreadyRegistered = true;
			
		} catch (SQLException e) {
			log.fatal("MobilyCommonService --> OracleContentProviderAuthenticateDAO --> isActivationCodeExist > SQLException "+ e);
			throw new SystemException(e);
		}catch (NullPointerException e) {
		    log.fatal("MobilyCommonService --> OracleContentProviderAuthenticateDAO --> isActivationCodeExist > NullPointerException "+ e);
			throw new SystemException(e);
        }catch (NumberFormatException e) {
            log.fatal("MobilyCommonService --> OracleContentProviderAuthenticateDAO --> isActivationCodeExist > NumberFormatException "+ e);
			throw new SystemException(e);
        }
		finally {
				JDBCUtility.closeJDBCResoucrs(connection, preparedStatement, resultSet);
		}
		
			log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> isActivationCodeExist : end");
			return isAlreadyRegistered;
	    }

	 
	 /**
	 	 date: Jun 1, 2009
	 	 Description: This method is used to check weather the provided activation code is valid or not.
	     @param authenticateAndGoVO. Mobily number, module id and activation code are mandatory values in the Parameter authenticateAndGoVO
	     @return bolean . true if provided activation code is valid else false
	     @throws SystemException
	  */
	 
	 public boolean isValidActivationCode(AuthenticateAndGoVO authenticateAndGoVO){
	        log.info("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode : start");
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			ResultSet resultSet = null;
			boolean isValid = false;

			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));

				String selectSql = "SELECT ACTIVATION_CODE FROM AUTHENTICATE_GO_ACTIVATION_TBL WHERE MSISDN= ? AND MODULE_ID = ? AND ACTIVATION_CODE = ?";
				
				preparedStatement = connection.prepareStatement(selectSql);
				preparedStatement.setString(1 ,authenticateAndGoVO.getMobilyNumber());
				preparedStatement.setInt(2 ,Integer.parseInt(authenticateAndGoVO.getModuleId()));
				preparedStatement.setString(3 ,authenticateAndGoVO.getActivationCode().trim());
				resultSet = preparedStatement.executeQuery();
				
				if(resultSet.next())
				{
				    log.debug("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode :  MSISDN ["+FormatterUtility.getUniversalFormat(authenticateAndGoVO.getMobilyNumber() )+"] = Activation valid");
				    isValid = true;
				}else{
				    log.debug("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode : MSISDN ["+FormatterUtility.getUniversalFormat(authenticateAndGoVO.getMobilyNumber() )+"] = Activation not valid");
				}
				
			} catch (SQLException e) {
				log.fatal("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode : SQLException "+ e);
				throw new SystemException(e);
			}catch (NullPointerException e) {
			    log.fatal("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode : NullPointerException "+ e);
				throw new SystemException(e);
            }catch (NumberFormatException e) {
                log.fatal("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode : NumberFormatException "+ e);
				throw new SystemException(e);
            }
			finally {
					JDBCUtility.closeJDBCResoucrs(connection, preparedStatement, resultSet);
			}
			log.info("MobilyCommonService-->OracleAuthenticateAndGoDAO-->isValidActivationCode : end");
			return isValid;
	    }
	 
	 
	 
	 /**
	 	date: Jun 1, 2009
	 	Description: This method is used to insert the record in auditiong table of Authenticate and Go applications.
	    @param authenticateAndGoVO. Service request id, Mobily number, module id and action id are mandatory values in param authenticateAndGoVO.
	    @return booelan. true if inserted successfully else false.
	  */
	 
	 public boolean auditSubscription(AuthenticateAndGoVO authenticateAndGoVO) {
	        log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> auditSubscription : start");
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			boolean isAudited = false;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				
				String insertAuditingTblSql = "INSERT INTO AUTHONTICATE_GO_AUDITING_TBL (SR_ID,MSISDN,ACTION_TIME,MODULE_ID,ACTION_ID,SERVICE_NAME,SERVICE_DESCRIPTION,ID) values( ?,?,SYSDATE,?,?,?,?,AUTH_GO_AUDITING_SEQ.NEXTVAL)";  
				
				preparedStatement = connection.prepareStatement(insertAuditingTblSql);
				preparedStatement.setString(1,authenticateAndGoVO.getServiceRequestId());
				preparedStatement.setString(2,authenticateAndGoVO.getMobilyNumber());
				preparedStatement.setInt(3,Integer.parseInt(authenticateAndGoVO.getModuleId()));
				preparedStatement.setInt(4,Integer.parseInt(authenticateAndGoVO.getActionId()));
				preparedStatement.setString(5,authenticateAndGoVO.getServiceName());
				preparedStatement.setString(6,authenticateAndGoVO.getServiceDescription());
				
				int status = preparedStatement.executeUpdate();
				
				log.debug("MobilyCommonService --> OracleAuthenticateAndGoDAO --> auditSubscription :status is :"+ status);
				
				if (status > 0)
					isAudited = true;

			} catch (SQLException e) {
				log.fatal("MobilyCommonService-->OracleContentProviderAuthenticateDAO > auditSubscription > SQLException "+ e);
				throw new SystemException(e);
			}catch (NullPointerException e) {
			    log.fatal("MobilyCommonService-->OracleContentProviderAuthenticateDAO > auditSubscription > NullPointerException "+ e);
				throw new SystemException(e);
            }catch (NumberFormatException e) {
                log.fatal("MobilyCommonService-->OracleContentProviderAuthenticateDAO > auditSubscription > NumberFormatException "+ e);
				throw new SystemException(e);
            }
			finally {
					JDBCUtility.closeJDBCResoucrs(connection, preparedStatement, null);
			}
			log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> auditSubscription : end");
			return isAudited;
	    }
	 
	 
	 /**
	  * Gets the difference between the Expiry date(Pin entered) and current date in the form of minutes for the given ModuleId 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public int getDifferenceInMinutes(AuthenticateAndGoVO authenticateAndGoVO) {
		 log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> getDifferenceInMinutes : start");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		int diffInMinutes = 0;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
			String selectActivationCodeSql = "SELECT ((TO_DATE(TO_CHAR(CURRENT_TIMESTAMP,'DD/MM/YYYY HH24:MI'),'DD/MM/YYYY HH24:MI') - TO_DATE(TO_CHAR(EXPIRATION_DATE,'DD/MM/YYYY HH24:MI'),'DD/MM/YYYY HH24:MI'))*24*60) AS MINUTES FROM AUTHENTICATE_GO_ACTIVATION_TBL WHERE MODULE_ID = ? AND MSISDN = ?";
			
			preparedStatement = connection.prepareStatement(selectActivationCodeSql);
			preparedStatement.setInt(1,Integer.parseInt(authenticateAndGoVO.getModuleId()));
			preparedStatement.setString(2,authenticateAndGoVO.getMobilyNumber());
			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next()){
				String difference = resultSet.getString("MINUTES");
				if(!FormatterUtility.isEmpty(difference)){
					if(difference.trim().length() > 3){ 
						diffInMinutes = 999;
					}else{
						diffInMinutes = Integer.parseInt(difference);
					}
				}
			}
			
		} catch (SQLException e) {
			log.fatal("MobilyCommonService --> OracleContentProviderAuthenticateDAO --> getDifferenceInMinutes > SQLException "+ e);
			throw new SystemException(e);
		}catch (NullPointerException e) {
		    log.fatal("MobilyCommonService --> OracleContentProviderAuthenticateDAO --> getDifferenceInMinutes > NullPointerException "+ e);
			throw new SystemException(e);
		}catch (NumberFormatException e) {
         log.fatal("MobilyCommonService --> OracleContentProviderAuthenticateDAO --> getDifferenceInMinutes > NumberFormatException "+ e);
			throw new SystemException(e);
		}
		finally {
				JDBCUtility.closeJDBCResoucrs(connection, preparedStatement, resultSet);
		}
		
		log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> diffInMinutes : diffInMinutes =["+diffInMinutes+"] for Module Id =["+authenticateAndGoVO.getModuleId()+"]");
		return diffInMinutes;
	 }
	 
	 /**
	  * 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public boolean updateActivationCodeStatus(AuthenticateAndGoVO authenticateAndGoVO) {
	        log.info("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCodeStatus : start");
			Connection connection = null;
			PreparedStatement preparedStatement = null;
			boolean isUpdated =  false;
			int status = 0;
			try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
				
				StringBuffer updateActivationCodeStatusSql = new StringBuffer();
				
					updateActivationCodeStatusSql.append("UPDATE AUTHENTICATE_GO_ACTIVATION_TBL SET PIN_STATUS = ? ");
						if(ConstantIfc.AUTHENTICATE_AND_GO_ENTER_PIN.equals(authenticateAndGoVO.getActivationCodeStatus())){
							updateActivationCodeStatusSql.append(", EXPIRATION_DATE = CURRENT_TIMESTAMP ");
						}
						updateActivationCodeStatusSql.append("WHERE MSISDN = ? AND MODULE_ID = ?");
				
				preparedStatement = connection.prepareStatement(updateActivationCodeStatusSql.toString());
					preparedStatement.setInt(1,authenticateAndGoVO.getActivationCodeStatus());
					preparedStatement.setString(2,authenticateAndGoVO.getMobilyNumber());
					preparedStatement.setInt(3,Integer.parseInt(authenticateAndGoVO.getModuleId()));
				
				status = preparedStatement.executeUpdate();
				log.debug("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCodeStatus :status ="+ status);
				if (status > 0) 
					isUpdated =  true;

			} catch (SQLException e) {
				log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCodeStatus :: SQLException "+ e);
				throw new SystemException(e);
			}catch (NullPointerException e) {
			    log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCodeStatus :: NullPointerException "+ e);
				throw new SystemException(e);
	        }catch (NumberFormatException e) {
	            log.fatal("MobilyCommonService --> OracleAuthenticateAndGoDAO --> updateActivationCodeStatus :: NumberFormatException "+ e);
				throw new SystemException(e);
	        }
			finally {
					JDBCUtility.closeJDBCResoucrs(connection, preparedStatement,null);
			}
			log.info("MobilyCommonService-->OracleAuthenticateAndGoDAO-->updateActivationCodeStatus : end");
			return isUpdated;
    }

	 /**
	  * 
	  */
	 public boolean isActivationCodeValidated(AuthenticateAndGoVO authenticateAndGoVO){
        log.info("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode : start");
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		boolean isValid = false;

		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));

			String selectSql = "SELECT PIN_STATUS FROM AUTHENTICATE_GO_ACTIVATION_TBL WHERE MSISDN= ? AND MODULE_ID = ?";
			
			preparedStatement = connection.prepareStatement(selectSql);
			preparedStatement.setString(1 ,authenticateAndGoVO.getMobilyNumber());
			preparedStatement.setInt(2 ,Integer.parseInt(authenticateAndGoVO.getModuleId()));

			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next()){
				String pinStatus = FormatterUtility.checkNull(resultSet.getString("PIN_STATUS"));
			    log.debug("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode :  MSISDN ["+FormatterUtility.getUniversalFormat(authenticateAndGoVO.getMobilyNumber() )+"] , pinStatus =["+pinStatus+"]");
			    if(ConstantIfc.AUTHENTICATE_AND_GO_ENTER_PIN.equals(pinStatus)){
			    	isValid = true;
			    }
			}else{
			    log.debug("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode : MSISDN ["+FormatterUtility.getUniversalFormat(authenticateAndGoVO.getMobilyNumber() )+"] = Activation not valid");
			}
		} catch (SQLException e) {
			log.fatal("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode : SQLException "+ e);
			throw new SystemException(e);
		}catch (NullPointerException e) {
		    log.fatal("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode : NullPointerException "+ e);
			throw new SystemException(e);
        }catch (NumberFormatException e) {
            log.fatal("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode : NumberFormatException "+ e);
			throw new SystemException(e);
        }
		finally {
				JDBCUtility.closeJDBCResoucrs(connection, preparedStatement, resultSet);
		}
		log.info("MobilyCommonService-->isActivationCodeValidated-->isValidActivationCode : end");
		return isValid;
    }

}