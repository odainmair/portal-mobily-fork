//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.core.service.DataSources;

/**
 * Provides operations to get the related numbers for a given user
 * 
 * @author Hossam Omar
 * 
 */
public class RelatedNumbersDAO extends AbstractDBDAO<String>
{

	private static final String GET_BY_SERVICE_ACCOUNT_NUM = "CALL SIEBEL.Customerinfo.GetCustRelAccntsByAccntNum(? , ?)";

	private static final String GET_BY_MSISDN = "CALL SIEBEL.Customerinfo.GetCustomerRelAccntsByMsisdn(? , ?)";

	/***
	 * Singleton instance of the class
	 */
	private static RelatedNumbersDAO instance = new RelatedNumbersDAO();

	protected RelatedNumbersDAO()
	{
		super(DataSources.SIEBLE);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static RelatedNumbersDAO getInstance()
	{
		return instance;
	}

	/***
	 * Returns the list of related numbers for the passed MSISDN
	 * 
	 * @param MSISDN
	 *            user MSISDN
	 * @return a list of MSISDN representing the related numbers
	 */
	public List<String> getRelatedNumbers(String MSISDN)
	{
		return queryProcedure(GET_BY_MSISDN, MSISDN);
	}

	/***
	 * Returns the list of related numbers for the user using the passed service
	 * account number
	 * 
	 * @param serviceAccountNumber
	 *            the user service account number
	 * @return a list of MSISDNs representing the related numbers
	 */
	public List<String> getRelatedNumbersByServiceAccntNum(String serviceAccountNumber)
	{
		return queryProcedure(GET_BY_SERVICE_ACCOUNT_NUM, serviceAccountNumber);
	}

	@Override
	protected String mapDTO(ResultSet rs) throws SQLException
	{
		return rs.getString("MSISDN");
	}

	public static void main(String[] args) throws SQLException
	{
		RelatedNumbersDAO wiMaxDAO = RelatedNumbersDAO.getInstance();
		// List<String> relatedNumbers =
		// wiMaxDAO.getRelatedNumbers("966540510267");
		List<String> relatedNumbers = wiMaxDAO.getRelatedNumbersByServiceAccntNum("1000113635441263");
		for (String relNum : relatedNumbers)
		{
			// System.out.println("Found Related number [" + relNum + "]");
		}
	}
}