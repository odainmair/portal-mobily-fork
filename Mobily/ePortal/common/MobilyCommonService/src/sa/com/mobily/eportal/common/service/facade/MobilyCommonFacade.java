/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.facade;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.business.AuthenticateAndGoBO;
import sa.com.mobily.eportal.common.service.business.ConnectOTSBO;
import sa.com.mobily.eportal.common.service.business.CorporateStrategicTrackBO;
import sa.com.mobily.eportal.common.service.business.CoverageCheckBO;
import sa.com.mobily.eportal.common.service.business.CreditTransferBO;
import sa.com.mobily.eportal.common.service.business.CustomerInfoDetailsBO;
import sa.com.mobily.eportal.common.service.business.CustomerProfileBO;
import sa.com.mobily.eportal.common.service.business.CustomerStatusBO;
import sa.com.mobily.eportal.common.service.business.CustomerTypeBO;
import sa.com.mobily.eportal.common.service.business.EBillBO;
import sa.com.mobily.eportal.common.service.business.FavoriteNumberInquiryBO;
import sa.com.mobily.eportal.common.service.business.LoyaltyBO;
import sa.com.mobily.eportal.common.service.business.MCRDetailsBO;
import sa.com.mobily.eportal.common.service.business.MCRRelatedNumBO;
import sa.com.mobily.eportal.common.service.business.ManageCreditCardBO;
import sa.com.mobily.eportal.common.service.business.PackageConversionBO;
import sa.com.mobily.eportal.common.service.business.PandaCommonBO;
import sa.com.mobily.eportal.common.service.business.PaymentNotificationBO;
import sa.com.mobily.eportal.common.service.business.PhoneBookBO;
import sa.com.mobily.eportal.common.service.business.RoyalGuardCommonBO;
import sa.com.mobily.eportal.common.service.business.SupplementaryServiceInquiryBO;
import sa.com.mobily.eportal.common.service.business.WimaxBO;
import sa.com.mobily.eportal.common.service.business.ePortalDBBO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.vo.AuthenticateAndGoVO;
import sa.com.mobily.eportal.common.service.vo.BillingAccountVO;
import sa.com.mobily.eportal.common.service.vo.ConnectOTSVO;
import sa.com.mobily.eportal.common.service.vo.CorporateProfileVO;
import sa.com.mobily.eportal.common.service.vo.CoverageVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentReplyVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentRequestVO;
import sa.com.mobily.eportal.common.service.vo.CustServiceDetReqVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.CustomerServiceDetReplyVO;
import sa.com.mobily.eportal.common.service.vo.CustomerStatusVO;
import sa.com.mobily.eportal.common.service.vo.CustomerTypeVO;
import sa.com.mobily.eportal.common.service.vo.DestinationTypeVO;
import sa.com.mobily.eportal.common.service.vo.EBillReplyVO;
import sa.com.mobily.eportal.common.service.vo.EBillRequestVO;
import sa.com.mobily.eportal.common.service.vo.EditAttributeVO;
import sa.com.mobily.eportal.common.service.vo.FavoriteNumberInquiryVO;
import sa.com.mobily.eportal.common.service.vo.LoyaltyRequestVO;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersReplyVO;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersReqVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInquiryRequestVO;
import sa.com.mobily.eportal.common.service.vo.MyServiceInquiryVO;
import sa.com.mobily.eportal.common.service.vo.PackageConversionReplyVO;
import sa.com.mobily.eportal.common.service.vo.PackageConversionRequestVO;
import sa.com.mobily.eportal.common.service.vo.PandaEmailVO;
import sa.com.mobily.eportal.common.service.vo.PandaHistoryVO;
import sa.com.mobily.eportal.common.service.vo.PandaReportVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyReplyVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyRequestVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookContactVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookGroupVO;
import sa.com.mobily.eportal.common.service.vo.ProfileVO;
import sa.com.mobily.eportal.common.service.vo.RoyalGuardReplyVO;
import sa.com.mobily.eportal.common.service.vo.RoyalGuardRequestVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserRelatedNoVO;



/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MobilyCommonFacade {
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
    private static CustomerTypeBO customerTypeBO = new CustomerTypeBO();
    private static CustomerStatusBO customerStatusBO = new CustomerStatusBO();
    private static CustomerProfileBO customerProfileBO = new CustomerProfileBO();
    private static FavoriteNumberInquiryBO favoriteNumberInquiryBO = new FavoriteNumberInquiryBO();
    private static SupplementaryServiceInquiryBO supplementaryBO = new SupplementaryServiceInquiryBO();
    private static PhoneBookBO phoneBookBO = new PhoneBookBO();
    private static WimaxBO wimaxBO = new WimaxBO(); 
    private static CustomerInfoDetailsBO customerDetails = new CustomerInfoDetailsBO();
    public static EBillBO  eBillBO = new EBillBO();
    public static ManageCreditCardBO manageCreditCardBO = new ManageCreditCardBO();
    public static PackageConversionBO packageConversionBO = new PackageConversionBO();
    public static ePortalDBBO ePortalDbBo = new ePortalDBBO();
    public static RoyalGuardCommonBO royalGuardCommonBO = new RoyalGuardCommonBO();
    public static MCRDetailsBO mcrBO = new MCRDetailsBO();
    public static ConnectOTSBO connectOtsBo = new ConnectOTSBO();
    public static MCRRelatedNumBO mcrRelatedNumBO = new MCRRelatedNumBO();
    private static CoverageCheckBO coverageCheckBO = new CoverageCheckBO();
    private static AuthenticateAndGoBO authenticateAndGoBO = new AuthenticateAndGoBO();
    private static CreditTransferBO creditTransferBO = new CreditTransferBO();
    private static PaymentNotificationBO paymentNotifyBo = new PaymentNotificationBO();
    
    
    /**
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     * Feb 18, 2008
     * MobilyCommonFacade.java
     * msayed
     *      * Mandatory Fields:
     * 		1- LineNumber
     * 		2- requestorUserId

     */
    public CustomerTypeVO getCustomerType(CustomerTypeVO requestVO)throws MobilyCommonException{
        log.info("MobilyCommonFacade >getCustomerType > for Customer ["+requestVO.getLineNumber()+"] > called ");
        CustomerTypeVO  replyObj = null;
        //generate XML request
        try {
            replyObj = customerTypeBO.getCustomerType(requestVO);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
        log.info("MobilyCommonFacade >getCustomerType > for Customer ["+requestVO.getLineNumber()+"]  = ["+replyObj.getCustomerType()+"]> done ");
      return replyObj;    
    
    }

    /**
     * check is the customer postpaid
     * @return
     * @throws MobilyCommonException
     * Feb 18, 2008
     * MobilyCommonFacade.java
     * msayed
     * Mandatory Fields:
     * 		1- LineNumber
     * 		2- requestorUserId
     */
    public boolean isCustomerPostPaid(CustomerTypeVO requestVO)throws MobilyCommonException{
        log.info("MobilyCommonFacade >isCustomerPrePaid > for Customer ["+requestVO.getLineNumber()+"] >  Called");
        boolean isCustomer = false;
        try{
            isCustomer = customerTypeBO.isCustomerPostPaid(requestVO);
        }catch(MobilyCommonException e){
            throw e;
        }
        return isCustomer;    

      }


    /**
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     * Feb 18, 2008
     * MobilyCommonFacade.java
     * msayed
     *      * Mandatory Fields:
     * 		1- msisdn
     * 		2- requestorUserId

     */
    public FavoriteNumberInquiryVO getFavoriteNumberInquiry(FavoriteNumberInquiryVO  requestVO)throws MobilyCommonException{
        log.info("MobilyCommonFacade >getFavoriteNumberInquiry > for Customer ["+requestVO.getMsisdn()+"] > called ");
        FavoriteNumberInquiryVO  replyObj = null;
        //generate XML request
        try {
            replyObj = favoriteNumberInquiryBO.getFavoriteNumberInquiry(requestVO);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
        log.info("MobilyCommonFacade >getFavoriteNumberInquiry > for Customer ["+requestVO.getMsisdn()+"] done ");
      return replyObj;    
    
    }
    
    /**
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     * Feb 18, 2008
     * MobilyCommonFacade.java
     * msayed
     * Mandatory Fields:
     * 		1- LineNumber
     * 		2- requestorUserId
     */
    public CustomerStatusVO getCustomerStatus(CustomerStatusVO requestVO)throws MobilyCommonException{
        log.info("MobilyCommonFacade >getCustomerType > for Customer ["+requestVO.getLineNumber()+"] > called ");
        CustomerStatusVO  replyObj = null;
        //generate XML request
        try {
            replyObj = customerStatusBO.getCustomerStatus(requestVO);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
        log.info("MobilyCommonFacade >getCustomerType > for Customer ["+requestVO.getLineNumber()+"]  = ["+replyObj.getStatus()+"]> done ");
      return replyObj;    
    
    }

    
    /**
     * @param msisdn
     * @param fullInfo
     * @return
     * Feb 19, 2008
     * MobilyCommonFacade.java
     * msayed
     */
    public static CustomerProfileReplyVO getCustomerProfile(String msisdn , boolean fullInfo){
        CustomerProfileReplyVO replyVO = null;
        replyVO = customerProfileBO.getCustomerProfile(msisdn ,fullInfo );
        return replyVO;
    
    }
    
    /**
     * Gets the customer profile information for the given billing account number 
     * @param billingAccNumber
     * @return
     * Nov 12, 2008
     * Nagendra
     */
    public CustomerProfileReplyVO getCustomerProfile(String billingAccNumber){
        CustomerProfileReplyVO replyVO = null;
        replyVO = customerProfileBO.getCustomerProfile(billingAccNumber );
        return replyVO;
    
    }
    
    /**
	 * Responsible for get user account information for the given MSISDN
	 * @param msisdn
	 * @return CustomerProfileReplyVO
     */
	public static CustomerProfileReplyVO getCustomerProfileByMSISDN(String msisdn) throws Exception{
		return customerProfileBO.getCustomerProfileByMSISDN(msisdn);
	}

	
    /**
	 * Responsible for get user account information for the given MSISDN
	 * @param msisdn
	 * @return UserAccountVO
     */
	public UserAccountVO getUserAccountWebProfile(String msisdn) throws Exception{
		return customerProfileBO.getUserAccountWebProfile(msisdn);
	}

   /**
	 * Responsible for get user account information for the given name
	 * 
	 * @param name
	 * @return UserAccountVO
     */
	public UserAccountVO getUserAccountWebProfileByName(String userName) throws Exception{
		return customerProfileBO.getUserAccountWebProfileByName(userName);
	}

    /**
     * Responsible for getting the function id
     * @param groupVO
     * @return status
     * @throws MobilyCommonException
     */
    public  int getFunctionId(String functionName)throws MobilyCommonException{
	   	 log.debug("MobilyCommonFacade > getFunctionId > call");
	   	 int status;
		 try{
		 	status =  phoneBookBO.getFunctionId(functionName);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > getFunctionId > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > getFunctionId > end");
		 return status;
    }

    /**
     * Responsible for checking the presence of given contact msisdn
     * @param contactVO
     * @return status
     * @throws MobilyCommonException
     */
	public boolean isContactMsisdnAlreadyExist(PhoneBookContactVO contactVO)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > isContactMsisdnAlreadyExist > call");
		 boolean status = false;
		 try{
		 	status = phoneBookBO.isContactMsisdnAlreadyExist(contactVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > isContactMsisdnAlreadyExist > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > isContactMsisdnAlreadyExist > end");
		 return status;
	}
    
    /**
     * Responsible for creating a new distribution list
     * @param groupVO
     * @return status
     * @throws MobilyCommonException
     */
    public  int createNewDistributionList(PhoneBookGroupVO groupVO)throws MobilyCommonException{
   	 	 log.debug("MobilyCommonFacade > createNewDistributionList > call");
   	 	 int status;
   	 	 try{
   	 		status = phoneBookBO.createNewDistributionList(groupVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > createNewDistributionList > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > createNewDistributionList > end");
		 return status;
 	 }

    /**
     * Responsible for creating a new member for a distribution list
     * @param contactVO
     * @param distListId
     * @return status
     * @throws MobilyCommonException
     */
	public int createDistributionListEntry(PhoneBookContactVO contactVO,int distListId) throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > createDistributionListEntry > call");
		 int status;
		 try{
		 	status = phoneBookBO.createDistributionListEntry(contactVO,distListId);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > createDistributionListEntry > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > createDistributionListEntry > end");
		 return status;
	}

	/**
     * Responsible for updating member of black list
     * @param contactVO
     * @return status
     * @throws MobilyCommonException
     */
	public int updateListEntry(PhoneBookContactVO contactVO)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > updateListEntry > call");
		 int status;
		 try{
		 	status = phoneBookBO.updateListEntry(contactVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > updateListEntry > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > updateListEntry > end");
		 return status;
	}
	
	/**
     * Responsible for updating member of distribution list
     * @param contactVO
     * @return status
     * @throws MobilyCommonException
     */
	public int updateDistributionListEntry(PhoneBookContactVO contactVO)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > updateDistributionListEntry > call");
		 int status;
		 try{
		 	status = phoneBookBO.updateDistributionListEntry(contactVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > updateDistributionListEntry > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > updateDistributionListEntry > end");
		 return status;
	}
	

	/**
     * Responsible for deleting a distribution list
     * @param phoneBookVo
     * @return status
     * @throws MobilyCommonException
     */
	public int deleteDistributinList(PhoneBookGroupVO phoneBookVo)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > deleteDistributinList > call");
		 int status;
		 try{
		 	status = phoneBookBO.deleteDistributinList(phoneBookVo);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > deleteDistributinList > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > deleteDistributinList > end");
		 return status;
	}

	/**
     * Responsible for deleting a member of distribution list
     * @param contactId
     * @return status
     * @throws MobilyCommonException
     */
	public int deleteDistributionListMember(int contactId) throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > deleteDistributionListMember > call");
		 int status;
		 try{
		 	status = phoneBookBO.deleteDistributionListMember(contactId);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > deleteDistributionListMember > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > deleteDistributionListMember > end");
		 return status;
	}
	
	/**
     * Responsible for listing all the members of a distribution list
     * @param groupVO
     * @return listOfMembers
     * @throws MobilyCommonException
     */
	public ArrayList listDuistributionListMembers(PhoneBookGroupVO groupVO)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > listDuistributionListMembers > call");
		 ArrayList listOfMembers = null;
		 try{
		 	listOfMembers = phoneBookBO.listDuistributionListMembers(groupVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > listDuistributionListMembers > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > listDuistributionListMembers > end");
		 return listOfMembers;
	}

	/**
     * Responsible for checking the presence of a owner MSISDN
     * @param groupVO
     * @return status
     * @throws MobilyCommonException
     */
	public boolean isOwnerMsisdnAlreadyExist(PhoneBookGroupVO groupVO) throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > isOwnerMsisdnAlreadyExist > call");
		 boolean status = false;
		 try{
		 	status = phoneBookBO.isOwnerMsisdnAlreadyExist(groupVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > isOwnerMsisdnAlreadyExist > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > isOwnerMsisdnAlreadyExist > end");
		 return status;
	}

	/**
     * Responsible for creating a blacklist
     * @param groupVO
     * @return status
     * @throws MobilyCommonException
     */
	public int createNewBlackList(PhoneBookGroupVO groupVO)	throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > createNewBlackList > call");
		 int status;
		 try{
		 	status = phoneBookBO.createNewBlackList(groupVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > createNewBlackList > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > createNewBlackList > end");
		 return status;
	}

	/**
     * Responsible for creating a black lsit member
     * @param contactVO
     * @return status
     * @throws MobilyCommonException
     */
	public int createBlackListEntry(PhoneBookContactVO contactVO)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > createBlackListEntry > call");
		 int status;
		 try{
		 	status = phoneBookBO.createBlackListEntry(contactVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > createBlackListEntry > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > createBlackListEntry > end");
		 return status;
	}

	/**
     * Responsible for listing all the members of black list
     * @param groupVO
     * @return listOfMembers
     * @throws MobilyCommonException
     */
	public ArrayList listBlackListMembers(PhoneBookGroupVO groupVO)throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > listBlackListMembers > call");
		 ArrayList listOfMembers = null;
		 try{

		 	listOfMembers = phoneBookBO.listBlackListMembers(groupVO);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > listBlackListMembers > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > listBlackListMembers > end");
		 return listOfMembers;
    }
	
	/**
     * Responsible for deleting a member of black list
     * @param contactId
     * @return status
     * @throws MobilyCommonException
     */
	public int deleteBlackListMember(int contactId) throws MobilyCommonException{
		 log.debug("MobilyCommonFacade > deleteBlackListMember > call");
		 int status;
		 try{
		 	status = phoneBookBO.deleteBlackListMember(contactId);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > deleteBlackListMember > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 log.debug("MobilyCommonFacade > deleteBlackListMember > end");
		 return status;
	}
	
	public static String getIqamaIdforBB(String bbAccountNumber) throws SystemException{
		log.debug("MobilyCommonFacade > getIqamaIdforBB > called");
		String iqamaIDforBB = null;
		iqamaIDforBB = wimaxBO.getIqamaIdforBB(bbAccountNumber);
		log.debug(iqamaIDforBB + "   iqamaIDforBB in facade");
		return iqamaIDforBB;
		
	}

	
	
	public static ArrayList getManagedWimaxNumbersList(String userMSISDN) throws SystemException
    {
		log.debug("MobilyCommonFacade > getManagedWimaxNumbersList > called");
		return wimaxBO.getManagedWimaxNumbersList(userMSISDN);
    }
	
	/**
     * Responsible for creating a black lsit member
     * @param contactVO
     * @return status
     * @throws MobilyCommonException
     */
	public static  CustomerServiceDetReplyVO getCustomerServiceDetailsFromBSL(CustServiceDetReqVO custReq) throws MobilyCommonException, SystemException{
		 log.debug("MobilyCommonFacade > getcustomerServiceDetailsFromBSL > call");
		 CustomerServiceDetReplyVO replyVO = null;
		 try{
		     replyVO = customerDetails.getcustomerServiceDetailsFromBSL(custReq);
		 }catch(MobilyCommonException e){
		   	log.fatal("MobilyCommonFacade > getcustomerServiceDetailsFromBSL > MobilyCommonException, "+e.getMessage());
		   	 throw e;
		 }
		 catch(SystemException e){
			   	log.fatal("MobilyCommonFacade > getcustomerServiceDetailsFromBSL > SystemException, "+e.getMessage());
			   	 throw e;
			 }
		 log.debug("MobilyCommonFacade > getcustomerServiceDetailsFromBSL > end");
		 return replyVO;
	}
	
	public static EBillReplyVO subscribeEBill(
			EBillRequestVO EBillRequestVO) throws MobilyCommonException,SystemException {
		log.debug("EBill > EBillFacade > subscribeEBill > Called");
		  return  eBillBO.subscribeEBill(EBillRequestVO);
	}
	
	
	
// Methods related to Authenticate And Go functionality added by Suresh V on June 02 2009 :: start
	
	/**
		date: Jun 2, 2009
		Description: To insert the activation code into Portal DB. This methos will insert the new record into AUTHENTICATE_GO_ACTIVATION_TBL.
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX), module id and activation codes are mandatory in the parameter authenticateAndGoVO
	    @return boolean. true if insertion is success else false.
	 */
	public static boolean insertActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
	    
	    return authenticateAndGoBO.insertActivationCode(authenticateAndGoVO);
	}
	
	/**
		date: Jun 2, 2009
		Description: To delete the Activation code from AUTHENTICATE_GO_ACTIVATION_TBL of portal DB. This method is used for Authenticate and go applications. 
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX) and module id are mandatory values in the Parameter authenticateAndGoVO
	    @return boolean . true if deleted successfully else false.
	 */
	public static boolean deleteActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
	    return authenticateAndGoBO.deleteActivationCode(authenticateAndGoVO);
	}
	
	
	/**
		date: Jun 2, 2009
		Description: This method is used to update the activation code
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX), module id and activation codes are mandatory in the parameter authenticateAndGoVO
	    @return boolean . true if updated successfully else false.
	 */
	public static boolean updateActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
	    return authenticateAndGoBO.updateActivationCode(authenticateAndGoVO);
	}
	
	
	/**
		date: Jun 2, 2009
		Description: This method is used to check weather activation code is already exists in DB for the provided MSISD againest module
	    @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX) and module id are mandatory in the parameter authenticateAndGoVO
	    @return boolean. true if exist else false.
	 */
	 public static boolean isActivationCodeExist(AuthenticateAndGoVO authenticateAndGoVO) {
		    return authenticateAndGoBO.isActivationCodeExist(authenticateAndGoVO);
	 }
	 
	 
	 /**
	 	 date: Jun 1, 2009
	 	 Description: This method is used to check weather the provided activation code is valid or not.
	     @param authenticateAndGoVO. Mobily number(in the universial format 9665XXXXXXXX), module id and activation code are mandatory values in the Parameter authenticateAndGoVO
	     @return bolean . true if provided activation code is valid else false
	     @throws SystemException
	  */
	 public static boolean isValidActivationCode(AuthenticateAndGoVO authenticateAndGoVO) {
		    return authenticateAndGoBO.isValidActivationCode(authenticateAndGoVO);
	 }
	 
	 /**
	 	date: Jun 2, 2009
	 	Description: This method is used to insert the record in auditiong table of Authenticate and Go applications.
	    @param authenticateAndGoVO. Service request id, Mobily number(in the universial format 9665XXXXXXXX), module id and action id are mandatory values in param authenticateAndGoVO.
	    @return booelan. true if inserted successfully else false.
	  */
	 public static boolean auditSubscription(AuthenticateAndGoVO authenticateAndGoVO) {
		    return authenticateAndGoBO.auditSubscription(authenticateAndGoVO);
	 }
	//	 Methods related to Authenticate And Go functionality added by Suresh V on June 02 2009 :: end

	 /**
	 	date: Jun 8, 2009
	 	Description: This method is used to do Panda Service Inquiry from MQ for getting the Total Contract Minutes and Total Available Minutes.
	 	@param corporateid
	    @param pandaHistoryVO 
	    @return PandaHistoryVO
	  */
	 public PandaHistoryVO getPandaInquiry(String corporateId,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException {
	     PandaCommonBO pandaCommonBO = new PandaCommonBO();
		 return pandaCommonBO.getPandaInquiry(corporateId,pandaHistoryVO);
	 }
	 
	 /**
	 	date: July 02, 2009
	 	Description: This method is used to do Panda Service Inquiry for MSISDN from MQ for getting the Distributed Minutes and Available Minutes.
	 	@param profileContactVO
	    @param pandaHistoryVO 
	    @return PandaHistoryVO
	  */
	 public PandaReportVO getPandaMSISDNInquiry(PandaReportVO pandaReportVO) throws MobilyCommonException {
	     PandaCommonBO pandaCommonBO = new PandaCommonBO();
		 return pandaCommonBO.getPandaMSISDNInquiry(pandaReportVO);	 	
	 }
	 
	 /**
	 	date: Jun 8, 2009
	 	Description: This method is used to do call MQ for distributing minutes to the MSISDNs.
	 	@param profileVO
	    @param pandaHistoryVO 
	    @return PandaHistoryVO
	  */
	 public PandaHistoryVO pandaDistribution(ProfileVO profileVO,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException {
	     PandaCommonBO pandaCommonBO = new PandaCommonBO();
		 return pandaCommonBO.pandaDistribution(profileVO,pandaHistoryVO);
	 }
	 
	 /**
	 	date: Jun 17, 2009
	 	Description: This method is used to send English and Arabic emails to the CAP and the Account Manager for the Exception occured in Distribution.
	 	@param pandaEmailVO
	  */
	 public void sendEmail(PandaEmailVO pandaEmailVO){
	     PandaCommonBO pandaCommonBO = new PandaCommonBO();
		 pandaCommonBO.sendEmail(pandaEmailVO);
	 }
	 
	   /**
	    * date: Aug 31, 2009
		 * Responsible for getting profile information for the billing account number
		 * @author r.agarwal.mit
		 * @param name
		 * @return UserAccountVO
	     */
	 public CustomerProfileReplyVO getCustomerProfileFromSiebel(String billingAccNumber) {
	 	return customerProfileBO.getCustomerProfileFromSiebel(billingAccNumber);
	}
	 
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to call MQ for credit card management(Enquiry,Payment,Change PIN,Change Prime MSISDN,Cancel CC)
	  * @author r.agarwal.mit
	  * @param objCreditCardPaymentRequestVO
	  * @param requesterChannelFunction
	  * @return CreditCardPaymentReplyVO
	 * @throws MobilyCommonException 
	  */
	 public CreditCardPaymentReplyVO manageCreditCards(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO,String requesterChannelFunction) throws MobilyCommonException {
	 	return manageCreditCardBO.manageCreditCards(objCreditCardPaymentRequestVO,requesterChannelFunction);
	 }
	 
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the user managed number from EEDB and then check the validity of the numbers with Siebel
	  * @author r.agarwal.mit
	  * @param aMsisdn
	  * @return List
	  */
	 public List getUserManagedNumbersList(String aMsisdn){
	 	return manageCreditCardBO.getUserManagedNumbersList(aMsisdn);
	 }
	 
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the next transaction id from the Sequence created in EEDB
	  * @author r.agarwal.mit
	  * @return String
	  */
	 public String getThirdGenerationTransId() {
	 	return manageCreditCardBO.getThirdGenerationTransId();
	 }
	 
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to audit the credit card payment transaction in EEDB
	  * @author r.agarwal.mit
	  * @param objCreditCardPaymentRequestVO
	  */
	 public void auditCreditCardPayment(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO) {
	 	manageCreditCardBO.auditCreditCardPayment(objCreditCardPaymentRequestVO);
	 }
	 
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the Corporate Profile from Siebel
	  * @author r.agarwal.mit
	  * @param aBillingNo
	  * @return CorporateProfileVO
	  */
	 public CorporateProfileVO findCorporateProfile(String aBillingNo) {
	 	return customerProfileBO.findCorporateProfile(aBillingNo);
	 }
	 
	 /**
	  * Date: July 20, 2010
	  * Description: This method should be used by Corporate Registration Revamp,BillingInfo Revamp and Credit Card Payment Revamp
	  * @author r.agarwal.mit
	  * @param aBillingAccount
	  * @return List
	  */
	 public List getBillingAccountMsisdnList(String aBillingAccount) {
		 return customerProfileBO.getBillingAccountMsisdnList(aBillingAccount);
	 }

	 /**
	  * Date: Dec 09, 2009
	  * Description: This method is used to handle the package conversion request
	  * @author n.gundluru.mit
	  * 
	  * @param packageConversionReqVO
	  * @return
	  */
	 public PackageConversionReplyVO convertPackage(PackageConversionRequestVO packageConversionReqVO) throws MobilyCommonException {
		 return packageConversionBO.handlePackageConversion(packageConversionReqVO);
	 }
	 
	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SystemException
	 */
	public boolean hasPackageConversionPendingRequest(String msisdn) throws SystemException {
		return packageConversionBO.hasPackageConversionPendingRequest(msisdn);
	}
	
	
	/**
	 * @return
	 * @throws SystemException
	 */
	public Hashtable getArabicProfile() throws SystemException {
		return ePortalDbBo.getArabicProfile();
	}

	/**
	 * this is will be a common activation module where it will insert the activation code based on the project id
	 * @param String accountNumber
	 * @param String activation Code
	 * @param int project id [SR_PROJECT_TBL]
	 * @return void
	 * */
	public static void setActivationCode(String accountNumber , String activationCode , int projectId) throws SystemException{
		log.debug("ePortalDBBO > setActivationCode > Called");
		ePortalDbBo.setActivationCode(accountNumber, activationCode, projectId);
	}
	
	/**
	 * this is will be a common activation module where it will reponsible for retrivig the activation code based on MSISDN / Project ID
	 * @param String accountNumber
	 * @param int project id [SR_PROJECT_TBL]
	 * @return String activation code
	 * */
	public static String getActivationCode(String accountNumber ,  int projectId) throws SystemException{
			log.debug("ePortalDBBO > getActivationCode > Called");
			return ePortalDbBo.getActivationCode(accountNumber, projectId);		
		}
	/**
	 * 
	 * @param objUserAccountVO
	 * @return
	 * @throws SystemException
	 */
	public boolean updateActivatedUserAccount(UserAccountVO objUserAccountVO) throws SystemException {
		return ePortalDbBo.updateActivatedUserAccount(objUserAccountVO);
	}

	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SystemException
	 */
	public UserAccountVO getUserEportalDBWebProfile(String msisdn) throws SystemException {
		return ePortalDbBo.getUserEportalDBWebProfile(msisdn);
	}
	
	/**
	 * 
	 * @param msisdn
	 * @return
	 * @throws SystemException
	 */
	public boolean isWebProfileUpdated(String msisdn) throws SystemException {
		return ePortalDbBo.isWebProfileUpdated(msisdn);
	}

	/**
	 * 
	 * @return
	 * @throws SystemException
	 */
	public static HashMap getChargeServiceAmount() throws SystemException {
		return ePortalDbBo.getChargeServiceAmount();
	}
	/**
	 * 
	 * @return
	 * @throws SystemException
	 */
	public static String fetchPOIDIdforMsisdn(String msisdn, String packType) throws SystemException {
		return ePortalDbBo.fetchPOIDIdforMsisdn(msisdn,packType);
	}
	/**
	 * 
	 * @return
	 * @throws SystemException
	 */
	public static String fetchPOIDIdforMsisdnfromDB(String msisdn) throws SystemException {
		return ePortalDbBo.fetchPOIDIdforMsisdnfromDB(msisdn);
	}
	/**
	 * 
	 * 
	 * @throws SystemException
	 */
	public static void updatePOIDForMsisdninDB(String msisdn, String poid) throws SystemException {
		ePortalDbBo.updatePOIDForMsisdninDB(msisdn, poid);
	}	
	/**
	 * 
	 * 
	 * @throws SystemException
	 */
	public static void insertPOIDForMsisdninDB(String msisdn) throws SystemException {
		ePortalDbBo.insertPOIDForMsisdninDB(msisdn);
	}
	
	public int getUserAccountID(String msisdn) throws Exception {
		return ePortalDbBo.getUserAccountID(msisdn);
	}
	public int getUserAccountIDFromManagedTbl(String msisdn) throws Exception {
		return ePortalDbBo.getUserAccountIDFromManagedTbl(msisdn);
	}

	public  void updatePOIDListforRegistration(String msisdn, String packType) throws Exception {
		ePortalDbBo.updatePOIDListforRegistration(msisdn, packType);
	}
	public  int updatePOIDforCCM(String msisdn, String packType) throws Exception {
		return ePortalDbBo.updatePOIDforCCM(msisdn, packType);
	}
	
	/**
	 * This method is used to do the corporate inquiry.
	 * @param billingAccountNumber
	 * @return
	 */
	public BillingAccountVO doCorporateInquiry(String billingAccountNumber) {
		return new CorporateStrategicTrackBO().doCorporateInquiry(billingAccountNumber);
	}
	
	/**
	 * This method is used to update the alias name
	 * @param editAttributeVO
	 * @return 
	 */
	public static EditAttributeVO updateAliasName(EditAttributeVO editAttributeVO) {
		return new CorporateStrategicTrackBO().updateAliasName(editAttributeVO);
	}
	
	/**
	 * This method is used to get the list of all MSISDN or the MSISDN balance based on the operation type
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public static RoyalGuardReplyVO msisdnInquiry(RoyalGuardRequestVO requestVO) throws MobilyCommonException {
		return royalGuardCommonBO.msisdnInquiry(requestVO);
	}
	
	/**
	 * This method is used to do the async corporate inquiry.
	 * @param billingAccountNumber
	 * @return
	 */
	public static void handleDIARequestAsync(String billingAccountNumber) {
		new CorporateStrategicTrackBO().handleDIARequestAsync(billingAccountNumber);
	}

	/**
	 * Responsible for getting the details from MCR database
	 *  
	 * @param mode
	 * @param billAcctNumber
	 * @param eligiblePackages
	 * @return ArrayList
	 */
    public static ArrayList getMCRDetails(int mode, String billAcctNumber, String eligiblePackages)  {
    	return mcrBO.getMCRDetails(mode, billAcctNumber, eligiblePackages);
	}

	/**
	 * Request xml for voucher recharge
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public static ConnectOTSVO doVoucherRecharge(ConnectOTSVO requestVO) throws MobilyCommonException {
    	return connectOtsBo.doVoucherRecharge(requestVO);
	}
	
	public static ConnectOTSVO doVoucherRecharge(ConnectOTSVO requestVO, boolean isValidateCustomerID) throws MobilyCommonException {
    	return connectOtsBo.doVoucherRecharge(requestVO,isValidateCustomerID);
	}

	/**
	 * Used to generate xml request to get the msisdn for a sim number
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public static ConnectOTSVO getMsisdnForSim(ConnectOTSVO requestVO) throws MobilyCommonException {
		return connectOtsBo.getMsisdnForSim(requestVO);
	}

	public static String getServiceAccount(String msisdn) throws SystemException{
		return ePortalDbBo.getServiceAccount(msisdn);
	}

	/**
	 * Request xml for get related numbers from MCR
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public static MCRRelatedNumbersReplyVO getRelatedMsisdnFromMCR(MCRRelatedNumbersReqVO requestVO) throws MobilyCommonException {
		return mcrRelatedNumBO.getRelatedMsisdnFromMCR(requestVO);
	}
	
	/**
	 * This method is used to update the Expired Status when the CAP updates the Alias name successfully
	 * @param billingAccountNumber
	 * @param expiredStatus
	 * @return
	 */
	public static boolean updateExpiredStatusInDB(String billingAccountNumber, String expiredStatus) {
		return new CorporateStrategicTrackBO().updateExpiredStatusInDB(billingAccountNumber,expiredStatus);
	}
	
	/**
	 * 
	 * @param intAccNumber
	 * @return
	 * @throws SystemException
	 */
	public static HashMap getServicePackAttributes(String intAccNumber)throws SystemException{
		return wimaxBO.getServicePackAttributes(intAccNumber);
	}
    
	/**
	 * 
	 * @param cpeSerialNumber
	 * @return
	 * @throws SystemException
	 */
	public  static String getInternetAccNumForCPE(String cpeSerialNumber)throws SystemException{
		return wimaxBO.getInternetAccNumForCPE(cpeSerialNumber);
	}
	
	/**
	 * 
	 * @param detialsVO
	 * @return
	 * @throws MobilyCommonException
	 * @throws SystemException
	 */
	public static CoverageVO getCoverageStatus(DestinationTypeVO detialsVO) throws MobilyCommonException,SystemException{
		return coverageCheckBO.getCoverageStatus(detialsVO);
	}

	/**
	 * This service used to check is the account number need to added by the customer already exist under the same customer id or not
	 * @param object of UserRelatedNoVO where the following attribute is mandatory
	 * ID , destination msisdn
	 * */
	public static boolean isUserRelatedNumExist(UserRelatedNoVO relatedVO) throws SystemException {
		log.debug("ePortalDBBO > isUserRelatedNoExist > Called");
		return ePortalDbBo.isUserRelatedNoExist(relatedVO);
	}
	
	 /**
     * this service is responsible for getting the customer related account for specific MSISDN /Service Account Number
     * @param userMSISDN
     * @return
     * @throws SystemException
     */
	public ArrayList getCustomerRelatedAccountByNumber(String accountNumber)throws SystemException{
		return customerProfileBO.getCustomerRelatedAccountByNumber(accountNumber);
	}
	
	/**
	 * this methed is responsible for adding related number into the customer account
	 * @param UserAccountVO(id , msisdn , username , service account number , line type)
	 * @throws Exception
	 * @throws SQLException
	 */
	public void addingNewRelatedNumber(UserAccountVO userAccount) throws SystemException{
			log.debug("ePortalDBBO > addingNewRelatedNumber > Called");
			ePortalDbBo.addingNewRelatedNumber(userAccount);		
		}
	
	public static boolean pinRequest(CreditCardPaymentRequestVO requestVO) {
		return manageCreditCardBO.pinRequest(requestVO);
	}
	public static boolean validateActivationCode(CreditCardPaymentRequestVO requestVO) {
		return manageCreditCardBO.validateActivationCode(requestVO);
	}
	public static CreditCardPaymentReplyVO capCreditCardCancellation(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO) throws MobilyCommonException {
		return manageCreditCardBO.capCreditCardCancellation(objCreditCardPaymentRequestVO);
	}
	
	public static CreditCardPaymentReplyVO capCreditCardPayment(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO) throws MobilyCommonException {
		return manageCreditCardBO.capCreditCardPayment(objCreditCardPaymentRequestVO);
	}
	
	public static void clearInvalidRecord(CreditCardPaymentRequestVO requestVO) {
		manageCreditCardBO.clearInvalidRecord(requestVO);
	}
	public static boolean resendPIN(CreditCardPaymentRequestVO requestVO) {
		return manageCreditCardBO.resendPIN(requestVO);
	}
	
	/**
	 * @date: Dec 28, 2011
	 * @author: s.vathsavai.mit
	 * @description: responsibel to do the Neqaty Plus corporate level inquiry with EAI.
	 * @param loyaltyRequestVO : billing account number is mandatory in input.
	 * @return : boolean true if user subscribed to service, else false.
	 */
	public static boolean doNeqatyPlusCorporateLevelInquiry(LoyaltyRequestVO loyaltyRequestVO) {
		return new LoyaltyBO().doNeqatyPlusCorporateLevelInquiry(loyaltyRequestVO);
	}
	
	 /**
	  * Gets the difference between the Expiry date(Pin entered) and current date in the form of minutes for the given ModuleId 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public static int getDifferenceInMinutes(AuthenticateAndGoVO authenticateAndGoVO) {
		    return authenticateAndGoBO.getDifferenceInMinutes(authenticateAndGoVO);
	 }
	 
	 /**
	  * 
	  * @param authenticateAndGoVO
	  * @return
	  */
	 public static boolean updateActivationCodeStatus(AuthenticateAndGoVO authenticateAndGoVO) {
		 return authenticateAndGoBO.updateActivationCodeStatus(authenticateAndGoVO);	 }
	 
	 /**
	  * 
	  */
	 public static boolean isActivationCodeValidated(AuthenticateAndGoVO authenticateAndGoVO){
		 return authenticateAndGoBO.isActivationCodeValidated(authenticateAndGoVO);
	 }

	 public static CustomerProfileReplyVO getCustomerProfileFromMDM(String billingAccNumber) {
		 	return customerProfileBO.getCustomerProfileFromMDM(billingAccNumber);
	}
	 
	 /**
		 * @date: Mar 8, 2012
		 * @author: s.vathsavai.mit
		 * @description: Responsibel to get the list of portal page unique names from ePortal DB.
		 * @return List
		 * @throws SystemException
		 */
	 public static List getResetPortletRequiredPages() throws SystemException {
		return ePortalDbBo.getResetPortletRequiredPages();
	 }
	 
	 public static CustomerProfileReplyVO isMasterBillingAccount(String billingAccNumber) {
		 return customerProfileBO.isMasterBillingAccount(billingAccNumber);
	 }
	 
	 
	 /**
		 * check if the power user (user created for specifc team like sales , call center empowerment) updated his password after first login or not
		 * 
		 * @param username
		 * @return boolean
		 */
	 public  boolean isPowerUsr_UpdatedHisPassword(String userName) {
		 return ePortalDbBo.isPowerUsr_UpdatedHisPassword(userName);
	 }
	 
	 public static String isValidCustomerId(String msisdn,String idNumber) throws MobilyCommonException {
		 return customerProfileBO.isValidCustomerId(msisdn,idNumber);
	}

		/**
		 * insert a record in the table to know later is the power user customer updated his password or not
		 * 
		 * @param username
		 * @return nothing
		 * 
		 */
	 public  void updatePowerUsrPassword(String userName ) {
		 ePortalDbBo.updatePowerUsrPassword(userName);
	 }
	 
	 /**
	  * 
	  * @param msisdn
	  * @param packageId
	  * @return
	  */
	 public boolean isShowIdField(String msisdn, String packageId) {
		 return creditTransferBO.isShowIdField(msisdn, packageId);
	 }
	 
	 public UserAccountVO getUserAccountInfoFromManageTable(int id, String msisdn)	{
		 return ePortalDbBo.getUserAccountInfoFromManageTable(id,msisdn);
	 }

	 public boolean updateLineTypeInUserManagedNo(int id, String msisdn, int lineType) {
		 return ePortalDbBo.updateLineTypeInUserManagedNo(id, msisdn, lineType);
	 }
	 
	 public boolean updateLineTypeInUserAccount(String msisdn, int lineType)	{
		 return ePortalDbBo.updateLineTypeInUserAccount(msisdn, lineType);
	 }
	 
    /**
     * This method is responsible for sending a payment notification
     * 
     * @param requestVO
     */
    public static PaymentNotifyReplyVO sendPaymentNotification(PaymentNotifyRequestVO requestVO) {
    	return paymentNotifyBo.sendPaymentNotification(requestVO);	
    }
    
    /**
     *  responsible for get my sertvice page status [supplementary service , magic promotion , internation status]
     * @param obj
     * @param getPromotionStatus
     * @param getInternationalStatus
     * @return
     * @throws MobilyCommonException
     * Feb 20, 2008
     * MobilyCommonFacade.java
     * msayed
     */
    public MyServiceInquiryVO getMyServiceInquiryPage(MyServiceInquiryRequestVO obj , boolean getPromotionStatus , boolean getInternationalStatus) throws MobilyCommonException{
        log.info("MobilyCommonFacade >getSupplementaryServiceInquiry > for Customer ["+obj.getMsisdn()+"] > called ");
        MyServiceInquiryVO  replyObj = null;

        //generate XML request
         replyObj = supplementaryBO.getSupplementaryServiceInquiryPage(obj , getPromotionStatus , getInternationalStatus);
        
        log.info("MobilyCommonFacade >getSupplementaryServiceInquiry > for Customer ["+obj.getMsisdn()+"]   done ");
      return replyObj;    
    
    }
}