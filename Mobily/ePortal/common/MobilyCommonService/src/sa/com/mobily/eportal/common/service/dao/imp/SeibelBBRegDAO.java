/*
 * Created on Oct 29, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.BBRegDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.valueobject.common.ManagedMSISDNDataVO;


/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SeibelBBRegDAO implements BBRegDAO{

	private static final Logger log = LoggerInterface.log;


	/**
	 * Gets Internet Account Number for the given CPE serial number 
	 */
	public String getInternetAccNumForCPE(String cpeSerialNum) throws SystemException {
	    log.info("SeibelBBRegDAO > getInternetAccNumForCPE > called for CPE serial number = "+cpeSerialNum);

	    CallableStatement  cstmt = null;
		ResultSet rset = null;
		String intAccNumber = "";
		Connection connection = null;
		
		try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));

				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.GET_CPE_ACCNT_NUM(?, ?)}");

				log.debug("SeibelBBRegDAO > getInternetAccNumForCPE > sqlStatement = "+sqlStatement.toString());

	            cstmt = connection.prepareCall(sqlStatement.toString());
	            
	            cstmt.setString(1 , cpeSerialNum);
	            cstmt.registerOutParameter(2,Types.VARCHAR);
	            
	            cstmt.execute();
	            
	            intAccNumber = cstmt.getString(2);
		 } catch (SQLException e) {
				log.fatal("SeibelBBRegDAO > getInternetAccNumForCPE > SQLException "+e.getMessage());
				if((e.getMessage().indexOf("ORA-01403") == -1) || (e.getMessage().indexOf("no data found") == -1)){
					throw new SystemException(e);
				}
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.info("SeibelBBRegDAO > getInternetAccNumForCPE > intAccNumber = "+intAccNumber);

		return intAccNumber;
	}

	
	/**
	 * Gets the attribute values for the given internet account number like
	 * Bandwidth, Commitment Months, Duration, Is Committed and ODB ID
	 *   
	 */
	public HashMap getServicePackAttributes(String intAccNumber) throws SystemException {
	    log.info("SeibelBBRegDAO > getServicePackAttributes > called for intAccNumber = "+intAccNumber);

	    CallableStatement  cstmt = null;
		ResultSet rset = null;
		HashMap attributesMap = new HashMap();
		Connection connection = null;
		
		try {
				connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));

				StringBuffer sqlStatement = new StringBuffer("{ CALL SIEBEL.GET_SERVPACK_ATTR(?, ?)}");

				log.debug("SeibelBBRegDAO > getServicePackAttributes > sqlStatement = "+sqlStatement.toString());

	            cstmt = connection.prepareCall(sqlStatement.toString());
	            
	            cstmt.setString(1 , intAccNumber);
	            cstmt.registerOutParameter(2,OracleTypes.CURSOR);
	            
	            cstmt.execute();
	            rset=(ResultSet)cstmt.getObject(2);
				
	            while (rset.next()){
	            	attributesMap.put(rset.getString("ATTR_NAME"), rset.getString("CHAR_VAL")+"~"+rset.getString("STATUS_CD"));
				}
		 } catch (SQLException e) {
				log.fatal("SeibelBBRegDAO > getServicePackAttributes > SQLException "+e.getMessage());
		} finally {
		    	JDBCUtility.closeJDBCResoucrs(connection,cstmt,rset);			
		}
		log.info("SeibelBBRegDAO > getServicePackAttributes > attributesMap size= "+attributesMap.size());

		return attributesMap;
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.BBRegDAO#getIqamaIDforBB(java.lang.String)
	 */
	public String getIqamaIDforBB(String billingaccountNumber)  {
		// TODO Auto-generated method stub
		log.debug("SeibelBBRegDAO > getIqamaIDforBB > calling SeibelBBRegDAO getIqamaIDforBB for billing number ->  " + billingaccountNumber);
		String iqamaID = null;
		StringBuffer str = new StringBuffer("{ CALL Siebel.Customerinfo.GetCustRelAccntsByAccntNum (? , ?) }");
		CallableStatement  cs = null;
		Connection conn = null;
		ResultSet rs = null;
		try{
			conn = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
			cs = conn.prepareCall(str.toString());
			log.debug(cs + "  CallableStatement " + "  billingaccountNumber " + billingaccountNumber);
			cs.setString(1 , billingaccountNumber);
			cs.registerOutParameter(2,OracleTypes.CURSOR);
			cs.execute();
			rs=(ResultSet)cs.getObject(2);
			while(rs.next()){

				//if(rs.getString("SERVICE_ACCOUNT_NUMBER").equals("100011975203305")){
				if(rs.getString("SERVICE_ACCOUNT_NUMBER").equals(billingaccountNumber)){

					iqamaID = rs.getString("ID_DOC_NUMBER");

					log.debug("  iqamaID   ======================================  " + iqamaID);

				}

			}



		}catch(Exception e){
			throw new SystemException(e.getMessage(),e);
		}finally{
			JDBCUtility.closeJDBCResoucrs(conn , cs , rs);
		}
		return iqamaID;
	}

	public ArrayList<ManagedMSISDNDataVO> getManagedWimaxNumbersList(String msisdn) {
		// TODO Auto-generated method stub
		log.debug("SeibelBBRegDAO > getManagedWimaxNumbersList > calling SeibelBBRegDAO getManagedWimaxNumbersList for msisdn ->  " + msisdn);
		ArrayList<ManagedMSISDNDataVO> wimaxNumberList = new ArrayList();

		StringBuffer strBuf = new StringBuffer();

		if(msisdn.length()>12){
			log.debug("SeibelBBRegDAO > getManagedWimaxNumbersList > wimax  / FTTH ");
			strBuf = strBuf.append("{ CALL SIEBEL.Customerinfo.GetCustRelAccntsByAccntNum(? , ?) }");
		}else{
			log.debug("SeibelBBRegDAO > getManagedWimaxNumbersList > msisdn");
			strBuf = strBuf.append("{ CALL SIEBEL.Customerinfo.GETCUSTOMERRELACCNTSBYMSISDN(? , ?) }");   
		}
		log.debug("SeibelBBRegDAO > getManagedWimaxNumbersList > STORED PROCEDURE ----- > " + strBuf);
		CallableStatement  cs = null;
		Connection conn = null;
		ResultSet rs = null;
		try{
			conn = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_SIEBEL));
			//Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver" ).newInstance();
			//conn = DriverManager.getConnection( "jdbc:oracle:thin:@10.6.11.14:1521:SIEBTEST", "eportal", "eportal");
			cs = conn.prepareCall(strBuf.toString());
			//cs.setString(1 , "100011975203305");
			cs.setString(1 , msisdn);
			cs.registerOutParameter(2,OracleTypes.CURSOR);
			cs.execute();
			rs=(ResultSet)cs.getObject(2);
			log.debug("result set "  +  rs);
			while(rs.next()){

				ManagedMSISDNDataVO tempRelatedNum = new ManagedMSISDNDataVO();


				if(rs.getString("CPE_SERIAL_NUMBER")!=null&&!"".equals(rs.getString("CPE_SERIAL_NUMBER"))){
					log.debug("SeibelBBRegDAO > getManagedWimaxNumbersList > iqamaID   ======================================  " + rs.getString("ID_DOC_NUMBER"));
					tempRelatedNum.setMSISDN(rs.getString("SERVICE_ACCOUNT_NUMBER"));
					tempRelatedNum.setType("BroadBand@Home");
				}
				else if(rs.getString("CPE_SERIAL_NUMBER")==null || "".equals(rs.getString("CPE_SERIAL_NUMBER"))){
					log.debug("SeibelBBRegDAO > getManagedWimaxNumbersList > iqamaID   ======================================  " + rs.getString("ID_DOC_NUMBER"));
					tempRelatedNum.setMSISDN(rs.getString("MSISDN"));
					tempRelatedNum.setType("Mobile");
				}
				tempRelatedNum.setServiceAccountNumber(rs.getString("SERVICE_ACCOUNT_NUMBER"));
				tempRelatedNum.setBillingAccountNumber(rs.getString("BILLING_NUMBER"));
				tempRelatedNum.setId(rs.getString("ID_DOC_NUMBER"));
				if(msisdn.length()>13)
					tempRelatedNum.setCpeSerialNumber(rs.getString("CPE_SERIAL_NUMBER"));
				tempRelatedNum.setStatus(rs.getString("STATUS"));
				tempRelatedNum.setCustomertype(rs.getString("TYPE"));
				tempRelatedNum.setPackageName(rs.getString("PACKAGE"));
				
				
				
				if(msisdn.length()<13)
				tempRelatedNum.setPromotion(rs.getString("PROMOTION"));
				wimaxNumberList.add(tempRelatedNum);
				//}

			}
		}catch(Exception e){
			throw new SystemException(e.getMessage(),e);
		}finally{
			JDBCUtility.closeJDBCResoucrs(conn , cs , rs);
		}
		return wimaxNumberList;
	}

	public static void main(String args[]){
		SeibelBBRegDAO seibelBBRegDAO = new SeibelBBRegDAO();
		ArrayList array = seibelBBRegDAO.getManagedWimaxNumbersList("966540510182");
	}
}