package sa.com.mobily.eportal.billing.xml;

import java.util.Date;
import java.util.logging.Level;
import sa.com.mobily.eportal.billing.constants.BealIfc;
import sa.com.mobily.eportal.billing.constants.BillingConstantsIfc;
import sa.com.mobily.eportal.billing.jaxb.object.MessageRequestHeader;
import sa.com.mobily.eportal.billing.jaxb.object.MyBalanceRequest;
import sa.com.mobily.eportal.billing.jaxb.object.MyBalanceResponse;
import sa.com.mobily.eportal.billing.vo.BalanceInquiryReplyVO;
import sa.com.mobily.eportal.billing.vo.BalanceInquiryRequestVO;
import sa.com.mobily.eportal.billing.vo.MessageHeaderVO;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;

import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MyBalanceXMLHandler implements TagIfc

{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static String clazz = MyBalanceXMLHandler.class.getName();
	
	public String generateRequestXML(BalanceInquiryRequestVO requestVO){
				   
	    if (requestVO == null ) {
	        
	        return null;
	    }
	    
		 String xmlRequest ="";
//		 final ByteOutputStream outputStream = new ByteOutputStream();
//			XMLStreamWriter xmlStreamWriter = null;
		   
	       
	        try {
	        	
	        	MyBalanceRequest objectAsXML = new MyBalanceRequest();
	        	MessageRequestHeader messageRequestHeader = new MessageRequestHeader();
	        	
			if (requestVO.getHeader() != null
					&& requestVO.getHeader().getMsgFormat() != null
					&& requestVO
							.getHeader()
							.getMsgFormat()
							.equalsIgnoreCase(
									BillingConstantsIfc.MQ_WIMAX_MESSAGE_FORMAT)){

				messageRequestHeader
						.setMsgFormat(BillingConstantsIfc.MQ_WIMAX_MESSAGE_FORMAT);
			}else {
				messageRequestHeader
						.setMsgFormat(BealIfc.BALANCE_MESSAGE_FORMAT_VALUE);
			}
			
	        	messageRequestHeader.setMsgVersion(BealIfc.MESSAGE_VERSION_VALUE);
	        	messageRequestHeader.setChannelId(BealIfc.CHANNEL_ID_VALUE);
	        	messageRequestHeader.setChannelFunction(BealIfc.BALANCE_CHANNEL_FUNC_VALUE);
	        	messageRequestHeader.setUserId(requestVO.getUserID());
	        	messageRequestHeader.setLanguage(BealIfc.LANG_VALUE);
	        	messageRequestHeader.setSecurityInfo(BealIfc.SECURITY_INFO_VALUE);
	        	messageRequestHeader.setReturnCode(BealIfc.RETURN_CODE_VALUE);
	        	        	
	        	objectAsXML.setMsgRqHdr(messageRequestHeader);
	        	
	        	/*if(requestVO.getHeader() != null && requestVO.getHeader().getMsgFormat() != null && 
	    				requestVO.getHeader().getMsgFormat().equalsIgnoreCase(BillingConstantsIfc.MQ_WIMAX_MESSAGE_FORMAT)) {	    			
	    			objectAsXML.setAccountNumber(requestVO.getLineNumber());
	    		} else {*/
	    			executionContext.log(Level.INFO, "MyBalanceXMLHandler>> generateRequestXML >> MSISDN/SAN::"+requestVO.getLineNumber() , clazz,"generateRequestXML" );
	    			if("100".equals(requestVO.getLineNumber().substring(0,3))){
	    				objectAsXML.setAccountNumber(requestVO.getLineNumber());
	    			}else{
	    				objectAsXML.setLineNumber(requestVO.getLineNumber());
	    			}
	    			
	    		//}
	    		
	        	
	        	objectAsXML.setChargeable(BealIfc.BALANCE_CHARGABLE);
	        	objectAsXML.setChargeAmount(BealIfc.BALANCE_CHARGA_AMOUNT_VALUE);
	        	objectAsXML.setInValue(BealIfc.BALANCE_GET_IN_VALUE);
	        	
	        	

	    		if(requestVO.getHeader() != null && requestVO.getHeader().getMsgFormat() != null && 
	    				requestVO.getHeader().getMsgFormat().equalsIgnoreCase(BillingConstantsIfc.MQ_WIMAX_MESSAGE_FORMAT)) {
	    			
	    			// do nothing
	    			
	    		} else {
	    			objectAsXML.setIsClosed(BealIfc.VALUE_N);
	    			objectAsXML.setIncludeAdditionalRental(BealIfc.VALUE_Y);
	    		
	    		}
	    		
	        	
	        	
//	        	final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
//	    		xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream,"UTF-8");
//
//	        	JAXBContext jaxbContext = JAXBContext.newInstance(MyBalanceRequest.class);
//	    		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//	    		jaxbMarshaller.marshal(objectAsXML,  xmlStreamWriter);
//	    		outputStream.flush();
//	    		
//	    		xmlRequest = new String(outputStream.getBytes(),"UTF-8").trim();
	    		
	    		xmlRequest = JAXBUtilities.getInstance().marshal(objectAsXML);
	    		
	        }catch (Exception e)
			{
				LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "generateRequestXML", e);
				executionContext.log(logEntryVO);
				throw new XMLParserException(e.getMessage(), e);
			}
	        return xmlRequest;
	
	}
	
	public BalanceInquiryReplyVO parseReplyXML(String replyMessage){
		BalanceInquiryReplyVO balanceInquiryReplyVO = null;
		
			
			try {
//				JAXBContext jc = JAXBContext
//						.newInstance(MyBalanceResponse.class);
//				Unmarshaller unmarshaller = jc.createUnmarshaller();
//				XMLInputFactory xmlif = XMLInputFactory.newInstance();
//
//				
//
//				XMLStreamReader xmler = xmlif
//						.createXMLStreamReader(new StringReader(replyMessage));
//				MyBalanceResponse obj = (MyBalanceResponse) unmarshaller
//						.unmarshal(xmler);
				
				MyBalanceResponse obj = (MyBalanceResponse) JAXBUtilities.getInstance().unmarshal(MyBalanceResponse.class, replyMessage);
				
				if (obj != null) {
					balanceInquiryReplyVO = new BalanceInquiryReplyVO();
					MessageRequestHeader messageHeader = obj.getMsgRsHdr();
					
					if(messageHeader!=null){
                	MessageHeaderVO messageHeaderVO = new MessageHeaderVO();
                	messageHeaderVO.setReturnCode(messageHeader.getReturnCode());
                	messageHeaderVO.setErrorMessage(messageHeader.getReturnCode());
					
                	balanceInquiryReplyVO.setHeader(messageHeaderVO);
					}
					
					balanceInquiryReplyVO.setLineNumber(obj.getLineNumber());
					balanceInquiryReplyVO.setCustomerType(obj.getCustomerType());
					balanceInquiryReplyVO.setPackageID(obj.getPackageID());
					balanceInquiryReplyVO.setBalance(obj.getBalance());
					balanceInquiryReplyVO.setExpirationDate(obj.getExpirationDate());
					balanceInquiryReplyVO.setBilledAmount(obj.getBilledAmount());
					balanceInquiryReplyVO.setUnbilledAmount(obj.getUnbilledAmount());
					balanceInquiryReplyVO.setUnalocatedAmount(obj.getUnallocatedAmount());
					
					balanceInquiryReplyVO.setInValue(obj.getInValue());
					balanceInquiryReplyVO.setAccountNumber(obj.getAccountNumber());
					balanceInquiryReplyVO.setAmountDue(obj.getAmountDue());
					balanceInquiryReplyVO.setOutstandingAmount(obj.getOutstandingAmount());
					balanceInquiryReplyVO.setPackageName(obj.getPackageName());
					balanceInquiryReplyVO.setMonthlySubscriptionFee(obj.getMonthlySubscriptionFee());
					balanceInquiryReplyVO.setLastBillDate(obj.getLastBillDate());
					balanceInquiryReplyVO.setLastUpdateTime(obj.getLastUpdateTime());
					balanceInquiryReplyVO.setAdditionalRentalFees(obj.getAdditionalRentalFees());
					balanceInquiryReplyVO.setNcraBalance(obj.getNcraBalance());
				}
			}catch (Exception e)
			{
				LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "parseReplyXML", e);
				executionContext.log(logEntryVO);
				throw new XMLParserException(e.getMessage(), e);
			}
			
		return balanceInquiryReplyVO;
		
	}
	
	public  String generateFTTHRenewInqXMLRequest(BalanceInquiryRequestVO requestVO) {
	    String xmlRequest = "";
	    try {
	        Document doc = new DocumentImpl();

	        Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);

			//gernerate Header element
			Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
			    XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,BillingConstantsIfc.FUNC_ID_FTTH_RENEW_INQ);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,ConstantIfc.SUPPLEMENTARY_SERVICE_SECURITY_KEY);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION,ConstantIfc.PANDA_MESSAGE_VERSION);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.CHANNEL_ID_VALUE);
				String srdate = MobilyUtility.FormateDate(new Date());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID,requestVO.getUserID());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE,ConstantIfc.LANG_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_OVERWRITE_OPEN_ORDER,ConstantIfc.VALUE_N);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CONTENT_PROVIDER_Chargeable,ConstantIfc.VALUE_N.toLowerCase());
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGING_MODE,ConstantIfc.CUSTSTATUS_CHARGING_MODE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHARGE_AMOUNT,ConstantIfc.CUSTSTATUS_CHARGE_AMOUNT);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_DEALER_ID,BillingConstantsIfc.DUMMY_VAL_111111);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SHOP_ID,BillingConstantsIfc.DUMMY_VAL_111111);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_AGENT_ID,requestVO.getUserID());
			XMLUtility.closeParentTag(root, header);
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_CHANNEL_TRANS_ID,ConstantIfc.CHANNEL_ID_VALUE+srdate);
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ACCOUNT_NUMBER, requestVO.getLineNumber());
		    
		    doc.appendChild(root);
		    
			xmlRequest = XMLUtility.serializeRequest(doc);
					
	    }catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "generateFTTHRenewInqXMLRequest", e);
			executionContext.log(logEntryVO);
			throw new XMLParserException(e.getMessage(), e);
		}
		return xmlRequest;
	 }
	
	/**
	 * parsing xml reply message for FTTH renew amount inquiry
	 * 
	 * @return BalanceInquiryReplyVO
	 * @throws MobilyCommonException 
	 */
	public BalanceInquiryReplyVO parsingFTTHRenewInqReplyMessage(String replyMessage) {
		BalanceInquiryReplyVO replyBean = null;
		if(replyMessage == null || replyMessage =="")
			return replyBean;
		try {
			 replyBean = new BalanceInquiryReplyVO();
			 
			 Document doc = XMLUtility.parseXMLString(replyMessage);
			 
			 Node rootNode = doc.getElementsByTagName(TAG_MOBILY_BSL_SR_REPLY).item(0);
			 
			 NodeList nl	 = rootNode.getChildNodes(); 
			 
			 for(int j = 0; j < nl.getLength(); j++){
                if((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
                	continue;
                
                Element l_Node       = (Element)nl.item(j);
                String p_szTagName   = l_Node.getTagName();
                
                String l_szNodeValue = null;
                if( l_Node.getFirstChild() != null )
                	l_szNodeValue 	 =l_Node.getFirstChild().getNodeValue();
                
                
                if(p_szTagName.equalsIgnoreCase(TAG_ERROR_CODE)){
                	replyBean.setErrorCode(l_szNodeValue);
                } else if(p_szTagName.equalsIgnoreCase(TAG_ERROR_MSG)){
                	replyBean.setErrorMessage(l_szNodeValue);
                } else if(p_szTagName.equalsIgnoreCase(TAG_RENEWAL_AMOUNT)){
                	replyBean.setRenewalAmount(!FormatterUtility.isEmpty(l_szNodeValue)?Double.parseDouble(l_szNodeValue):0.0);
                } else if(p_szTagName.equalsIgnoreCase(TAG_EXPIRATION_DATE)){
                   replyBean.setTempFtthRenewexpDate(FormatterUtility.checkNull(l_szNodeValue));
                } else if(p_szTagName.equalsIgnoreCase(TAG_CUST_BALANCE)){
                	replyBean.setBalance(!FormatterUtility.isEmpty(l_szNodeValue)?Double.parseDouble(l_szNodeValue):0.0);
                }
			 }
		}catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception " + e.getMessage(), clazz, "parsingFTTHRenewInqReplyMessage", e);
			executionContext.log(logEntryVO);
			throw new XMLParserException(e.getMessage(), e);
		}
		return replyBean;
	}
}
