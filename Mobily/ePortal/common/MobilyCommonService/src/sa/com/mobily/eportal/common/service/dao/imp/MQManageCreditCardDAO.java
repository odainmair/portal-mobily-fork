/*
 * Created on Sept 16, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.util.List;

import org.apache.log4j.Logger;

import com.mobily.exception.mq.MQSendException;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentRequestVO;


/**
 * @author r.agrawal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQManageCreditCardDAO implements ManageCreditCardDAO {
	
	private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;

	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to call MQ for credit card management(Enquiry,Payment,Change PIN,Change Prime MSISDN,Cancel CC)
	  * @author r.agarwal.mit
	  * @param objCreditCardPaymentRequestVO
	  * @param requesterChannelFunction
	  * @return CreditCardPaymentReplyVO
	  */
	public String manageCreditCards(String xmlRequestMessage,String requsterChannelFunction) throws MobilyCommonException {
		log.debug("MQManageCreditCardDAO > manageCreditCards > called");
		String replyMessage = "";
        try {
			log.debug("MQManageCreditCardDAO > manageCreditCards > requsterChannelFunction = "+requsterChannelFunction);
			
			String reqQNameStr = "";
			String repQNameStr ="";
			if (requsterChannelFunction.trim().equalsIgnoreCase(ConstantIfc.Requestor_Channel_Function_Inquiry)){
				reqQNameStr = ConstantIfc.CREDIT_CARD_ENQUIRY_REQUEST_QUEUENAME;
				repQNameStr=ConstantIfc.CREDIT_CARD_ENQUIRY_REPLAY_QUEUENAME;
			}else if(requsterChannelFunction.trim().equalsIgnoreCase(ConstantIfc.Requestor_Channel_Function_Payment)){
				reqQNameStr = ConstantIfc.CREDIT_CARD_PAYMENT_REQUEST_QUEUENAME;
				repQNameStr=ConstantIfc.CREDIT_CARD_PAYMENT_REPLAY_QUEUENAME;
			}else if(requsterChannelFunction.trim().equalsIgnoreCase(ConstantIfc.Requestor_Channel_Function_Cancellation)){
				reqQNameStr = ConstantIfc.CREDIT_CARD_PROCESS_REQUEST_QUEUENAME;
				repQNameStr=ConstantIfc.CREDIT_CARD_PROCESS_REPLAY_QUEUENAME;
			}else if(requsterChannelFunction.trim().equalsIgnoreCase(ConstantIfc.Requestor_Channel_Function_Update)){
				reqQNameStr = ConstantIfc.CREDIT_CARD_PROCESS_REQUEST_QUEUENAME;
				repQNameStr=ConstantIfc.CREDIT_CARD_PROCESS_REPLAY_QUEUENAME;
			}else if(requsterChannelFunction.trim().equalsIgnoreCase(ConstantIfc.Requestor_Channel_Function_Reset)){
				reqQNameStr = ConstantIfc.CREDIT_CARD_PROCESS_REQUEST_QUEUENAME;
				repQNameStr=ConstantIfc.CREDIT_CARD_PROCESS_REPLAY_QUEUENAME;
			}
			
			// Get the Request Queue Name
			String requestQueueName = MobilyUtility.getPropertyValue(reqQNameStr);
			log.debug("MQManageCreditCardDAO > manageCreditCards > Request Queue Name: "+requestQueueName);
					
			// Get the Reply Queue Name
			String replyQueueName = MobilyUtility.getPropertyValue(repQNameStr);
			log.debug("MQManageCreditCardDAO > manageCreditCards > Reply Queue Name: "+replyQueueName);
						
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, requestQueueName,replyQueueName);
			//log.debug("MQManageCreditCardDAO > manageCreditCards > Reply Message: "+replyMessage);
			
        } catch (RuntimeException e) {
			log.fatal("MQManageCreditCardDAO > getCreditCardPayment > MQSendException " + e.getMessage());
			throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
		} catch (Exception e) {
			log.fatal("MQManageCreditCardDAO > getCreditCardPayment > Exception " + e);
			throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
		}
        return replyMessage;

	}	

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO#getThirdGenerationTransId()
	 */
	public String getThirdGenerationTransId() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO#AuditCreditCardTransactionInDB(sa.com.mobily.eportal.common.service.valueobject.CreditCardPaymentRequestVO)
	 */
	public void auditCreditCardTransactionInDB(CreditCardPaymentRequestVO requestVO) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO#getRelatedMSISDNList(java.lang.String)
	 */
	public List getRelatedMSISDNList(String aMsisdn) {
		// TODO Auto-generated method stub
		return null;
	}

	public void clearInvalidRecord(CreditCardPaymentRequestVO requestVO) {
		// TODO Auto-generated method stub
		
	}

	public boolean insertTransactionPIN(CreditCardPaymentRequestVO requestVO) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isValidActivationCode(CreditCardPaymentRequestVO requestVO) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isValidRecord(CreditCardPaymentRequestVO requestVO) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getValidPINCode(CreditCardPaymentRequestVO requestVO) {
		// TODO Auto-generated method stub
		return null;
	}
	


}
