//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.17 at 05:41:54 PM AST 
//


package sa.com.mobily.eportal.corporateprofile.jaxb.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER"/>
 *         &lt;element ref="{}Operation"/>
 *         &lt;element ref="{}CustomerAccountNo"/>
 *         &lt;element ref="{}DocType"/>
 *         &lt;element ref="{}DocNo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "srheader",
    "operation",
    "customerAccountNo",
    "docType",
    "docNo"
})
@XmlRootElement(name = "MOBILY_BSL_SR")
public class MOBILYBSLSR {

    @XmlElement(name = "SR_HEADER", required = true)
    protected SRHEADER srheader;
    @XmlElement(name = "Operation", required = true)
    protected String operation;
    @XmlElement(name = "CustomerAccountNo", required = true)
    protected String customerAccountNo;
    @XmlElement(name = "DocType", required = true)
    protected String docType;
    @XmlElement(name = "DocNo", required = true)
    protected String docNo;

    /**
     * Gets the value of the srheader property.
     * 
     * @return
     *     possible object is
     *     {@link SRHEADER }
     *     
     */
    public SRHEADER getSRHEADER() {
        return srheader;
    }

    /**
     * Sets the value of the srheader property.
     * 
     * @param value
     *     allowed object is
     *     {@link SRHEADER }
     *     
     */
    public void setSRHEADER(SRHEADER value) {
        this.srheader = value;
    }

    /**
     * Gets the value of the operation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

    /**
     * Gets the value of the customerAccountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerAccountNo() {
        return customerAccountNo;
    }

    /**
     * Sets the value of the customerAccountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerAccountNo(String value) {
        this.customerAccountNo = value;
    }

    /**
     * Gets the value of the docType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the docNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocNo() {
        return docNo;
    }

    /**
     * Sets the value of the docNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocNo(String value) {
        this.docNo = value;
    }

}
