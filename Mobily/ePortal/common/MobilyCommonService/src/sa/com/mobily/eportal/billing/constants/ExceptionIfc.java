/*
 * Created on Jan 29, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.billing.constants;

/**
 * @author msayed
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public interface ExceptionIfc
{
	public static final String NO_ERROR = "0000";

	public static final String EXCEPTION_CONNECTION_ERROR = "1001";

	public static final String EXCEPTION_INVALID_REQUEST_MESSAGE = "1002";

	public static final String EXCEPTION_NO_RECORD_FOUND = "1003";

	public static final String EXCEPTION_DBCONNECTION_ERROR = "1004";

	public static final String EXCEPTION_UNKNOW_ERROR = "1005";

	public static final String EXCEPTION_XML_ERROR = "1006";

	public static final String EXCEPTION_SYSTEM_ERROR = "1007";

	public static final String EXCEPTION_NUMBER_NOT_FOUND = "1008";

	public static final String EXCEPTION_MSISDN_NOT_FOUND_ERROR = "1009";

	public static final String EXCEPTION_NO_BILL_LIST_FOUND_ERROR = "1010";

	public static final String EXCEPTION_MQ_EXCEPTION = "1011";

	public static final String EXCEPTION_INVALID_INPUT = "1012";

	public static final String EXCEPTION_NO_SERVICE_ORDERS_LIST_FOUND = "1013";

	public static final String EXCEPTION_NO_CALL_DETAILS_FOUND = "1014";

	public static final String EXCEPTION_NO_CALLED_TO_LIST_FOUND = "1015";

	public static final String EXCEPTION_BILL_NOT_FOUND = "1016";

	public static final String EXCEPTION_BILL_PDF_FILE_NOT_FOUND = "1017";

	public static final String EXCEPTION_BILL_XML_FILE_NOT_FOUND = "1018";

	public static final int LOYALTY_ERROR_CODE_SUCCESS = 0;

	public static final int LOYALTY_ERROR_CODE_MSISDN_NOT_FOUND = 10181;

	public static final int LOYALTY_ERROR_CODE_NOT_ENOUGH_POINTS_FOR_REDEMPTION = 10191;

	public static final int LOYALTY_ERROR_CODE_REDEMPTION_ITEM_NOT_AVAILABLE = 10192;

	public static final int LOYALTY_ERROR_CODE_CUSTOMER_OG_BARRED = 10193;

	public static final int LOYALTY_ERROR_CODE_CUSTOMER_BLOCKED_IN_CREDIT_CONTROL = 10194;

	public static final int LOYALTY_ERROR_CODE_CUSTOMER_FULL_BARRED = 10195;

	public static final int LOYALTY_ERROR_CODE_CUSTOMER_MSISDN_IS_BLACK_LIST = 10196;

	public static final int LOYALTY_ERROR_CODE_CUSTOMER_PACKAGE_NOT_ELIGIBLE_FOR_REDEMPTION = 10197;

	public static final int LOYALTY_ERROR_CODE_FAILED_TO_PERCHASE_DEAL = 10185;

	public static final int LOYALTY_ERROR_CODE_FAILED_TO_COMPLETE_REDEMPTION = 10186;

	public static final int LOYALTY_ERROR_CODE_FAILED_TO_SEND_ACTIVATION_KEY = 10188;

	public static final int LOYALTY_ERROR_CODE_FAILED_TO_GENERATE_ACTIVATION_KEY = 10189;

	public static final int LOYALTY_ERROR_CODE_CUSTOMER_EXCEEDED_NO_OF_REDEMPTION_ATTEMPTS = 10203;

	public static final int LOYALTY_ERROR_CODE_COMMON = -1;

	// Payment History
	public static final String ERROR_NO_RECORDS = "9997";
}
