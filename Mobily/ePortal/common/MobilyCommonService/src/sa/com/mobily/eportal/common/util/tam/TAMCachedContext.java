package sa.com.mobily.eportal.common.util.tam;

import java.net.URL;
import java.util.Locale;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.constants.MobilyConstants;
import sa.com.mobily.eportal.common.constants.MobilyResEnvKeysConstants;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

import com.tivoli.pd.jutil.PDContext;

public class TAMCachedContext
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	private static final String className = TAMCachedContext.class.getName();

	private static TAMCachedContext instance;

	private static String tamAdminName = null;

	private static String tamAdminPassword = null;

	private static String tamConfigFile = null;

	private PDContext tamContext = null;

	private TAMCachedContext()
	{
		try
		{
			if (tamAdminName == null)
			{
			
				
				tamAdminName=ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.TAM_ADMIN_USER_NAME);
				if(tamAdminName==null)
				tamAdminName = AppConfig.getInstance().get(MobilyConstants.TAM_ADMIN_USER_NAME);
			}
			if (tamAdminPassword == null)
			{
				tamAdminPassword =ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.TAM_ADMIN_USER_PASSWORD);
				if(tamAdminPassword==null)
				tamAdminPassword = AppConfig.getInstance().get(MobilyConstants.TAM_ADMIN_USER_PASSWORD);
			}
			if (tamConfigFile == null)
			{
				tamConfigFile =ResourceEnvironmentProviderService.getInstance().getStringProperty(MobilyResEnvKeysConstants.TAM_CONFIG_FILE_URL);
				if(tamConfigFile==null)
				tamConfigFile = AppConfig.getInstance().get(MobilyConstants.TAM_CONFIG_FILE_URL);
			}

			URL configURL = new URL(tamConfigFile);
			this.tamContext = new PDContext(Locale.getDefault(), tamAdminName, tamAdminPassword.toCharArray(), "Default", configURL);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, "TAMCachedContext", e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	public PDContext getTamContext()
	{
		return tamContext;
	}

	public static synchronized TAMCachedContext getInstance()
	{
		if (instance == null)
		{
			instance = new TAMCachedContext();
		}
		return instance;
	}
}