package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.dao.constants.DAOConstants;
import sa.com.mobily.eportal.common.service.model.UserAccount;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
/**
 * This DAO class is responsible for DB operations on USER_ACCOUNTS table
 * <p> 
 * it contains methods for retrieve, add and update.
 *  
 * @author Yousef Alkhalaileh
 */

public class UserAccountDAO extends AbstractDBDAO<UserAccount>
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static String clazz = UserAccountDAO.class.getName();

	private static UserAccountDAO instance = new UserAccountDAO();

	private static final String UA_ADD = "INSERT INTO USER_ACCOUNTS(USER_ACCT_ID, USER_NAME, CREATED_DATE, "
			+ "EMAIL, STATUS, FIRST_NAME, LAST_NAME, CUST_SEGMENT, BIRTHDATE, NATIONALITY, GENDER, REGISTRATION_DATE," +
			"ACTIVATION_URL,REGISTRATION_SOURCE_ID,CONTACT_NUMBER,ROLE_ID) "
			+ "VALUES(?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP,?,?,?,?) ";
	
	/*private static final String UA_ADD_ACTIVATION_URL = "INSERT INTO USER_ACCOUNTS_EMAIL_ACTIVATION(USER_ACCT_ID,ACTIVATION_URL) "
			+ "VALUES(?, ?) ";*/
	

	private static final String UA_UPD = "UPDATE USER_ACCOUNTS SET ACTIVATION_DATE = CURRENT_TIMESTAMP, STATUS = ? WHERE USER_ACCT_ID = ? ";
	
	private static final String UA_UPDATE_ALL = "UPDATE EEDBUSR.USER_ACCOUNTS SET FIRST_NAME=?, LAST_NAME=?, BIRTHDATE=?, " +
			"NATIONALITY=?, GENDER=? WHERE USER_ACCT_ID = ?";
	
	private static final String UA_UPD_CUST_SEG_BY_USERACCTID = "UPDATE USER_ACCOUNTS SET CUST_SEGMENT = ? WHERE USER_ACCT_ID = ? ";
	
	private static final String UA_LAST_LOGIN_TIME = "UPDATE USER_ACCOUNTS SET LAST_LOGIN_TIME = CURRENT_TIMESTAMP WHERE USER_ACCT_ID = ? ";
	
	private static final String UA_UPD_CUST_SEG_ROLE_ID_BY_USERACCTID = "UPDATE USER_ACCOUNTS SET CUST_SEGMENT = ?, role_id = ? WHERE USER_ACCT_ID = ?";
	
	private static final String DELETE_USER_ACCOUNT_BY_USERACCID = "DELETE FROM USER_ACCOUNTS WHERE USER_ACCT_ID=?";

	private static final String UA_QUERY_BY_USERNAME = "SELECT USER_ACCT_ID, USER_NAME, CREATED_DATE, " +
			" ACTIVATION_DATE, LAST_LOGIN_TIME, EMAIL, STATUS, FIRST_NAME, LAST_NAME, CUST_SEGMENT, BIRTHDATE, " +
			" NATIONALITY, GENDER, REGISTRATION_DATE, ACTIVATION_URL, REGISTRATION_SOURCE_ID,CONTACT_NUMBER,ROLE_ID" +
			"  FROM USER_ACCOUNTS WHERE UPPER(USER_NAME) =?";
	
	private static final String UA_QUERY_BY_EMAIL = "SELECT USER_ACCT_ID, USER_NAME, CREATED_DATE, " +
			" ACTIVATION_DATE, LAST_LOGIN_TIME, EMAIL, STATUS, FIRST_NAME, LAST_NAME, CUST_SEGMENT, BIRTHDATE, " +
			" NATIONALITY, GENDER, REGISTRATION_DATE, ACTIVATION_URL, REGISTRATION_SOURCE_ID,CONTACT_NUMBER,ROLE_ID" +
			"  FROM USER_ACCOUNTS WHERE UPPER(EMAIL) =?";	

	private static final String UA_QUERY_BY_USERACCTID = "select USER_ACCT_ID, USER_NAME, CREATED_DATE, " +
			" ACTIVATION_DATE, LAST_LOGIN_TIME, EMAIL, STATUS, FIRST_NAME, LAST_NAME, CUST_SEGMENT, BIRTHDATE, " +
			" NATIONALITY, GENDER, REGISTRATION_DATE, ACTIVATION_URL, REGISTRATION_SOURCE_ID,CONTACT_NUMBER,ROLE_ID FROM USER_ACCOUNTS " +
			"   WHERE  USER_ACCT_ID = ?";

	private static final String GET_SEQ_USER_ACCT_ID = "SELECT USER_ACCOUNT_SEQ.NEXTVAL userAcctId FROM DUAL";
	
	private static final String UA_UPD_CUST_DETAILS_BY_USERACCTID = "UPDATE USER_ACCOUNTS SET FIRST_NAME = ?, " +
			"LAST_NAME = ?, CUST_SEGMENT = ?, BIRTHDATE = ?, NATIONALITY = ?, GENDER = ?, CONTACT_NUMBER = ?, ROLE_ID = ? " +
			"WHERE USER_ACCT_ID = ? ";

	private static final String SR_DR_UPDATE_ADD = "INSERT INTO SR_DR_UPDATE_TBL VALUES(?,?,sysdate)";
	
	private static final String UA_UPD_ROLE_ID_BY_USERACCTID = "UPDATE USER_ACCOUNTS SET CUST_SEGMENT = ?, ROLE_ID = ? WHERE USER_ACCT_ID = ? " ;
	
	private static final String UA_UPDATE_EMAIL = "UPDATE EEDBUSR.USER_ACCOUNTS SET FIRST_NAME=?, LAST_NAME=?, BIRTHDATE=?, " +
			"NATIONALITY=?, GENDER=?,EMAIL=? WHERE USER_ACCT_ID = ?";
	
	
	protected UserAccountDAO()
	{
		super(DataSources.DEFAULT);
	}

	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>UserAccountDAO</code>
	 * @author Yousef Alkhalaileh
	 */
	public static synchronized UserAccountDAO getInstance()
	{
		return instance;
	}
	
	/**
	 * This method saves UserAccount for a specific account in the USER_ACCOUNTS table
	 * 
	 * @param request <code>UserAccount</code> user account to be saved
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Yousef Alkhalaileh
	 */
	public int saveUserAccount(UserAccount userAccount)
	{

		 return update(UA_ADD, userAccount.getUserAcctId(), FormatterUtility.checkNull(userAccount.getUserName()).trim().toLowerCase(), 
				FormatterUtility.checkNull(userAccount.getEmail()).trim().toLowerCase(), userAccount.getStatus(),userAccount.getFirstName(), userAccount.getLastName(), 
				userAccount.getCustSegment(), userAccount.getBirthDateAsTimestamp(), userAccount.getNationality(),
				userAccount.getGender(), userAccount.getActivationURL(), new Integer(userAccount.getRequesterChannelId()), 
				userAccount.getContactNumber(), userAccount.getRoleId());
		 //UA_ADD_ACTIVATION_URL
		 //return update(UA_ADD_ACTIVATION_URL, userAccount.getUserAcctId(), userAccount.getActivationURL());
	}
	
	/**
	 * This method is user to insert a username upon creation / deletion from eportal system
	 * so the DR batch can use these information to syncronzie the users between production and DR
	 * @param  username
	 * @param  action (1= create / 2 = delete)
	 * */
	public int updateUserInDR(String userName, int action)
	{
		return update(SR_DR_UPDATE_ADD, userName, action);
	}
	
	/**
	 * This method update UserAccount for a specific account in the USER_ACCOUNTS table
	 * 
	 * @param request <code>UserAccount</code> user account to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Yousef Alkhalaileh
	 */
	public int updateUserAccount(UserAccount userAccount)
	{
		return update(UA_UPD, new Object[] { userAccount.getStatus(), userAccount.getUserAcctId() });
	}
	
	public int updateUserAccountInfo(UserAccount userAccount)
	{
		return update(UA_UPDATE_ALL, new Object[] { userAccount.getFirstName(), userAccount.getLastName(), userAccount.getBirthDate(), userAccount.getNationality(), userAccount.getGender(), userAccount.getUserAcctId()});
	}
	
	/**
	 * This method update last login time  by accountId in the USER_ACCOUNTS table
	 * 
	 * @param request <code>UserAccount</code> user account to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Yousef Alkhalaileh
	 */
	public int updateLastLoginTime(Long userAccountId)
	{
		return update(UA_LAST_LOGIN_TIME, new Object[] { userAccountId });
	}
	
	/**
	 * This method update UserAccount  custumer segment by user account id
	 * 
	 * @param request <code>UserAccount</code> user account to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Umer Rasheed
	 */
	public int updateCustomerSegmentByAccountID(String custSegment, Long userAccountId)
	{
		return update(UA_UPD_CUST_SEG_BY_USERACCTID, new Object[] { custSegment, userAccountId });
	}
	
	/**
	 * This method update UserAccount  custumer segment by user account id
	 * 
	 * @param request <code>UserAccount</code> user account to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author Umer Rasheed
	 */
	public int updateCustomerSegmentAndRoleId(String custSegment, String roleId, Long userAccountId)
	{
		return update(UA_UPD_CUST_SEG_ROLE_ID_BY_USERACCTID, new Object[] { custSegment, roleId, userAccountId });
	}
	
	/**
	 * This method returns a list UserAccount for a specific user name
	 * 
	 * @param request <code>String</code> User name
	 * @return <code>ArrayList<UserAccount></code> a list of UserAccount
	 * @author Yousef Alkhalaileh
	 */
	public List<UserAccount> findByUserName(String userName)
	{
		return query(UA_QUERY_BY_USERNAME, new Object[] { userName.toUpperCase() });
	}
	
	public List<UserAccount> findByEmail(String email)
	{
		return query(UA_QUERY_BY_EMAIL, new Object[] { email.toUpperCase() });
	}
	
	
	/**
	 * This method returns a list UserAccount for a specific user id
	 * 
	 * @param request <code>Integer</code> User ID
	 * @return <code>ArrayList<UserAccount></code> a list of UserAccount
	 * @author Yousef Alkhalaileh
	 */
	public List<UserAccount> findByUserId(Integer userId)
	{
		return query(UA_QUERY_BY_USERACCTID, new Object[] { userId });
	}
	
	/**
	 * This method returns a user account ID from the sequence
	 * 
	 * @return <code>Long</code> user account ID
	 * @author Yousef Alkhalaileh
	 */
	public Long getUserAcctIdSeq()
	{
		executionContext.log(Level.INFO, "getUserAcctIdSeq called", clazz, "mapDTO", null);

		UserAccount dto = null;
		List<UserAccount> userAccountList = query(GET_SEQ_USER_ACCT_ID);
		executionContext.log(Level.INFO, "getUserAcctIdSeq > userAccountList::" + userAccountList, clazz, "getUserAcctIdSeq", null);

		if (userAccountList != null && userAccountList.size() > 0)
		{
			dto = userAccountList.get(0);
		}
		return dto.getUserAcctId();
	}
	
	/***
	 * Delete Corporate Subscription by user account id
	 * @param userAccountId
	 * @return
	 */
	public int deleteUserAccount(Long userAccountId) {
        return update(DELETE_USER_ACCOUNT_BY_USERACCID, new Object[] {userAccountId});
    }
	
	/**
	 * This method returns a UserAccount from DB
	 * 
	 * @param request <code>ResultSet</code> from DB
	 * @return <code>UserAccount</code> mapped user account
	 * @author Yousef Alkhalaileh
	 */
	@Override
	protected UserAccount mapDTO(ResultSet rs) throws SQLException
	{
		UserAccount userAccount = new UserAccount();

		if (rs.getMetaData().getColumnCount() >= 10)
		{
			userAccount.setUserAcctId(rs.getLong(DAOConstants.UserAccount.USER_ACCT_ID));
			userAccount.setUserName(rs.getString(DAOConstants.UserAccount.USER_NAME));
			//userAccount.setIqama(rs.getString(DAOConstants.UserAccount.IQAMA));
			userAccount.setCreatedDate(rs.getTimestamp(DAOConstants.UserAccount.CREATED_DATE));
			userAccount.setActivationDate(rs.getTimestamp(DAOConstants.UserAccount.ACTIVATION_DATE));
			userAccount.setEmail(rs.getString(DAOConstants.UserAccount.EMAIL));
			userAccount.setStatus(rs.getInt(DAOConstants.UserAccount.STATUS));
			userAccount.setFirstName(rs.getString(DAOConstants.UserAccount.FIRST_NAME));
			userAccount.setLastName(rs.getString(DAOConstants.UserAccount.LAST_NAME));
			userAccount.setCustSegment(rs.getString(DAOConstants.UserAccount.CUST_SEGMENT));
			userAccount.setBirthDate(rs.getDate(DAOConstants.UserAccount.BIRTHDATE));
			userAccount.setNationality(rs.getString(DAOConstants.UserAccount.NATIONALITY));
			userAccount.setGender(rs.getString(DAOConstants.UserAccount.GENDER));
			userAccount.setRegistrationDate(rs.getTimestamp(DAOConstants.UserAccount.REGISTRATION_DATE));
			userAccount.setLastLoginTime(rs.getTimestamp(DAOConstants.UserAccount.LAST_LOGIN_TIME));
			userAccount.setActivationURL(rs.getString(DAOConstants.UserAccount.ACTIVATION_URL));
			userAccount.setContactNumber(rs.getString(DAOConstants.UserAccount.CONTACT_NUMBER));
			userAccount.setRoleId(rs.getInt(DAOConstants.UserAccount.ROLE_ID));
		}
		else
		{
			userAccount.setUserAcctId(rs.getLong(DAOConstants.UserAccount.USERACCTID));
		}

		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "dto >>>>" + userAccount.toString(), clazz, "mapDTO", null);
		executionContext.audit(logEntryVO);

		return userAccount;
	}
	
	/**
	 * This method update UserAccount for a specific account in the USER_ACCOUNTS table
	 * 
	 * @param request <code>UserAccount</code> user account to be updated
	 * @return <code>int</code> number of records affected (in case successful execution 1)
	 * @author r.agarwal.mit
	 */
	public int updateUserDetails(UserAccount userAccount)
	{
		return update(
				UA_UPD_CUST_DETAILS_BY_USERACCTID,
				new Object[] {userAccount.getFirstName(), userAccount.getLastName(), userAccount.getCustSegment(), userAccount.getBirthDate(),
						userAccount.getNationality(), userAccount.getGender(), userAccount.getContactNumber(), userAccount.getRoleId(), userAccount.getUserAcctId() });
	}
	
	public int updateUserRoledID(UserAccount userAccount){
		return update(UA_UPD_ROLE_ID_BY_USERACCTID, new Object[]{userAccount.getCustSegment(), userAccount.getRoleId(), userAccount.getUserAcctId()});
	}
	public int updateUserAccountWithEmail(UserAccount userAccount)
	{
		return update(UA_UPDATE_EMAIL, new Object[] { userAccount.getFirstName(), userAccount.getLastName(), userAccount.getBirthDate(), userAccount.getNationality(), userAccount.getGender(),userAccount.getEmail(), userAccount.getUserAcctId()});
	}
	
}