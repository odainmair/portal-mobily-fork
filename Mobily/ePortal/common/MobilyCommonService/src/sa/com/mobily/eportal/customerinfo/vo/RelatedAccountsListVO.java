package sa.com.mobily.eportal.customerinfo.vo;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}RelatedAccountInfo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "relatedAccountVOList"
})
@XmlRootElement(name = "RelatedAccountsList")
public class RelatedAccountsListVO  extends BaseVO{

    /**
	 * 
	 */
	private static final long serialVersionUID = 5296009384292397730L;
	@XmlElement(name = "RelatedAccountInfo", required = true)
    protected List<RelatedAccountVO> relatedAccountVOList;

    public List<RelatedAccountVO> getRelatedAccountVOList() {
        if (relatedAccountVOList == null) {
        	relatedAccountVOList = new ArrayList<RelatedAccountVO>();
        }
        return this.relatedAccountVOList;
    }

}
