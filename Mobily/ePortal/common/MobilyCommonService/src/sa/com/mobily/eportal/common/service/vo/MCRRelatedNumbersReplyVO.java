package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;


/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MCRRelatedNumbersReplyVO {
    
	private String errorCode ;
    private String errorMessage;
    private ArrayList mcrRelatedNumbersVoList = null;
    
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ArrayList getMcrRelatedNumbersVoList() {
		return mcrRelatedNumbersVoList;
	}
	public void setMcrRelatedNumbersVoList(ArrayList mcrRelatedNumbersVoList) {
		this.mcrRelatedNumbersVoList = mcrRelatedNumbersVoList;
	}
}