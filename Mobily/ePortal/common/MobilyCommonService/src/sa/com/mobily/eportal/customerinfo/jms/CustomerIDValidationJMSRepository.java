
package sa.com.mobily.eportal.customerinfo.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.util.AppConfig;
import sa.com.mobily.eportal.customerinfo.vo.IdValidationPromptReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.IdValidationPromptRequestVO;
import sa.com.mobily.eportal.customerinfo.xml.idvalidationpromptinq.IdValidationXMLHandler;
/**
 * @author Suresh Vathsavai - MIT
 * Date: Nov 28, 2013
 */
public class CustomerIDValidationJMSRepository {

	private static String className = CustomerIDValidationJMSRepository.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	/**
	 * Description: Responsible to do the customer ID validation prompt inquiry, means to check wether ID validation is reuired or not.
	 * Date: Nov 28, 2013
	 * @param requestVO
	 * @return IdValidationPromptReplyVO
	 */
	public IdValidationPromptReplyVO doIdValidationPromptInquiry(IdValidationPromptRequestVO requestVO) {
		String methodName = "doIdValidationPromptInquiry";
		executionContext.startMethod(className, methodName, null);
		
		String strXMLRequest = null;
		String strXMLReply = null;
		IdValidationPromptReplyVO replyVO = null;
		boolean isTesting = false;
		if(requestVO == null) {
			executionContext.audit(Level.SEVERE, "IdValidationPromptRequestVO is null in doIdValidationPromptInquiry", className, methodName);
			throw new BackEndException(ExceptionConstantIfc.ID_VALIDATION_PROMPT_INQUIRY_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}
		MQAuditVO mqAuditVO = new MQAuditVO();
		
		IdValidationXMLHandler xmlHandler = new IdValidationXMLHandler();
		String requestQueueName = AppConfig.getInstance().get(ConstantsIfc.QUEUE_REQUEST_ID_VALIDATION_PROMPT_INQUIRY);
		String replyQueueName = AppConfig.getInstance().get(ConstantsIfc.QUEUE_REPLY_ID_VALIDATION_PROMPT_INQUIRY);

		mqAuditVO.setRequestQueue(requestQueueName);
		mqAuditVO.setReplyQueue(replyQueueName);
		mqAuditVO.setMsisdn(requestVO.getMsisdn());
		mqAuditVO.setFunctionName(ConstantsIfc.ID_VALIDATE_MESSAGE_FORMAT_VALUE);
		executionContext.audit(Level.INFO, "CustomerIDValidationJMSRepository: doIdValidationPromptInquiry > Request Queue Name[" + requestQueueName + "], Reply Queue Name["+replyQueueName+"]", className, methodName);
		
		strXMLRequest = xmlHandler.generateIdValidationPromptXMLRequest(requestVO);
		executionContext.audit(Level.INFO, " doIdValidationPromptInquiry XML Request[" + strXMLRequest + "]", className, methodName);
		mqAuditVO.setMessage(strXMLRequest);
		if(isTesting) {
			strXMLReply = "<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><MsgFormat>ValidationPromptInquiry</MsgFormat><MsgVersion>0</MsgVersion><RequestorChannelId>PCPM/MPAY</RequestorChannelId><RequestorChannelFunction>ValidationPromptInquiry</RequestorChannelFunction><RequestorSecurityInfo>Secure</RequestorSecurityInfo><ChannelTransactionId></ChannelTransactionId><SrDate>20091125001911</SrDate><ErrorCode>0000</ErrorCode><ErrorMsg></ErrorMsg></SR_HEADER_REPLY><Prompt>Y</Prompt></MOBILY_BSL_SR_REPLY>";
		}else {
			//strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);
			strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
		}
		mqAuditVO.setReply(strXMLReply);
		executionContext.audit(Level.INFO, " doIdValidationPromptInquiry XML Reply[" + strXMLReply + "]", className, methodName);
		
		if(FormatterUtility.isNotEmpty(strXMLReply)) {
			replyVO = xmlHandler.parseIdValidationPromptXMLReply(strXMLReply);
		}else {
			mqAuditVO.setErrorMessage(String.valueOf(ExceptionConstantIfc.ID_VALIDATION_PROMPT_INQUIRY_SERVICE));
			mqAuditVO.setServiceError(ConstantsIfc.INVALID_REPLY);
			MQUtility.saveMQAudit(mqAuditVO);
			executionContext.log(Level.SEVERE, "CustomerIDValidationJMSRepository :doIdValidationPromptInquiry >Invalid XML Reply["+strXMLReply+"]", className, methodName, new BackEndException());
			throw new BackEndException(ExceptionConstantIfc.ID_VALIDATION_PROMPT_INQUIRY_SERVICE, ConstantsIfc.INVALID_REPLY);
		}
		
		if(replyVO != null) {
			executionContext.log(Level.SEVERE, "doIdValidationPromptInquiry : Return code["+replyVO.getErrorCode()+"]", className, methodName,null);
			mqAuditVO.setErrorMessage(replyVO.getErrorMessage());
			mqAuditVO.setServiceError(replyVO.getErrorCode());
			MQUtility.saveMQAudit(mqAuditVO);

			if(FormatterUtility.isNotEmpty(replyVO.getErrorCode()) && Integer.parseInt(replyVO.getErrorCode()) > 0)
				throw new BackEndException(ExceptionConstantIfc.ID_VALIDATION_PROMPT_INQUIRY_SERVICE, replyVO.getErrorCode());
		}else {
			executionContext.log(Level.SEVERE, "CustomerIDValidationJMSRepository :doIdValidationPromptInquiry >Invalid Reply VO["+replyVO+"]", className, methodName, new BackEndException());
			throw new BackEndException(ExceptionConstantIfc.ID_VALIDATION_PROMPT_INQUIRY_SERVICE, ConstantsIfc.INVALID_REPLY);
		}
		executionContext.endMethod(className, methodName, null);
		return replyVO;
	}
	
}
