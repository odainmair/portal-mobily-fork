package sa.com.mobily.eportal.customerinfo.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerReplyHeaderVO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}SR_HEADER_REPLY"/>
 *         &lt;element ref="{}MSISDN"/>
 *         &lt;element ref="{}BillingAccintNo"/>
 *         &lt;element ref="{}ParentAccountNumber"/>
 *         &lt;element ref="{}GenderChangeCounter"/>
 *         &lt;element ref="{}CustomerCategory"/>
 *         &lt;element ref="{}CustomerType"/>
 *         &lt;element ref="{}Plan"/>
 *         &lt;element ref="{}Status"/>
 *         &lt;element ref="{}Package"/>
 *         &lt;element ref="{}PackageID"/>
 *         &lt;element ref="{}Name"/>
 *         &lt;element ref="{}IDDocType"/>
 *         &lt;element ref="{}IDNumber"/>
 *         &lt;element ref="{}IDexpirydate"/>
 *         &lt;element ref="{}Nationality"/>
 *         &lt;element ref="{}StreetAddress"/>
 *         &lt;element ref="{}City"/>
 *         &lt;element ref="{}PObox"/>
 *         &lt;element ref="{}ContactPreference"/>
 *         &lt;element ref="{}ZIPCode"/>
 *         &lt;element ref="{}Mobile"/>
 *         &lt;element ref="{}HomeTelephone"/>
 *         &lt;element ref="{}WorkTelphone"/>
 *         &lt;element ref="{}Fax"/>
 *         &lt;element ref="{}Email"/>
 *         &lt;element ref="{}Gender"/>
 *         &lt;element ref="{}DOBformat"/>
 *         &lt;element ref="{}Birthdate"/>
 *         &lt;element ref="{}TimeCreated"/>
 *         &lt;element ref="{}MBAccountID"/>
 *         &lt;element ref="{}ParentAccountID"/>
 *         &lt;element ref="{}FirstName"/>
 *         &lt;element ref="{}MidName"/>
 *         &lt;element ref="{}LastName"/>
 *         &lt;element ref="{}Title"/>
 *         &lt;element ref="{}LangPref"/>
 *         &lt;element ref="{}CustomerSegment"/>
 *         &lt;element ref="{}SIMNumber"/>
 *         &lt;element ref="{}IMSI"/>
 *         &lt;element ref="{}Puk1"/>
 *         &lt;element ref="{}Puk2"/>
 *         &lt;element ref="{}ServMobile"/>
 *         &lt;element ref="{}CPESerialNumber"/>
 *         &lt;element ref="{}DigitalCertificateNumber"/>
 *         &lt;element ref="{}WIMAXExpDate"/>
 *         &lt;element ref="{}WIMAXDuration"/>
 *         &lt;element ref="{}VanityType"/>
 *         &lt;element ref="{}ProductLine"/>
 *         &lt;element ref="{}PUK"/>
 *         &lt;element ref="{}PIN1"/>
 *         &lt;element ref="{}PIN2"/>
 *         &lt;element ref="{}FingerPrintFlag"/>
 *         &lt;element ref="{}IDExpiryFlag"/>
 *         &lt;element ref="{}PayType"/>
 *         &lt;element ref="{}PackageDataType"/>
 *         &lt;element ref="{}PackageCategory"/>         
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "headerVO",
    "msisdn",
    "serviceAccountNo",
    "billingAccountNumber",
    "parentAccountNumber",
    "genderChangeCounter",
    "customerCategory",
    "customerType",
    "plan",
    "status",
    "packageName",
    "packageID",
    "name",
    "idDocType",
    "idNumber",
    "idExpirydate",
    "nationality",
    "streetAddress",
    "city",
    "poBox",
    "contactPreference",
    "zipCode",
    "mobile",
    "homeTelephone",
    "workTelphone",
    "fax",
    "email",
    "gender",
    "dobFormat",
    "birthDate",
    "timeCreated",
    "mbAccountID",
    "parentAccountID",
    "firstName",
    "midName",
    "lastName",
    "title",
    "langPref",
    "customerSegment",
    "simNumber",
    "imsi",
    "puk1",
    "puk2",
    "servMobile",
    "cpeSerialNumber",
    "digitalCertificateNumber",
    "wimaxExpDate",
    "wimaxDuration",
    "vanityType",
    "productLine",
    "puk",
    "pin1",
    "pin2",
    "fingerPrintFlag",
    "idExpiryFlag",
    "payType",
    "packageDataType",
    "packageCategory"
})
@XmlRootElement(name = "MOBILY_BSL_SR_REPLY")
public class ConsumerProfileReplyVO {

    @XmlElement(name = "SR_HEADER_REPLY", required = true)
    protected ConsumerReplyHeaderVO headerVO;
    
    @XmlElement(name = "MSISDN", required = true)
    protected String msisdn;
    
    @XmlElement(name = "ServiceAccountNo", required = true)
    protected String serviceAccountNo;
    
    @XmlElement(name = "BillingAccountNo", required = true)
    protected String billingAccountNumber;
    
    @XmlElement(name = "ParentAccountNumber", required = false)
    protected String parentAccountNumber;
    
    @XmlElement(name = "GenderChangeCounter", required = false)
    protected String genderChangeCounter;
    
    @XmlElement(name = "CustomerCategory", required = false)
    protected String customerCategory;
    
    @XmlElement(name = "CustomerType", required = false)
    protected String customerType;
    
    @XmlElement(name = "Plan", required = false)
    protected String plan;
    
    @XmlElement(name = "Status", required = false)
    protected String status;
    
    @XmlElement(name = "Package", required = false)
    protected String packageName;
    
    @XmlElement(name = "PackageID", required = false)
    protected String packageID;
    
    @XmlElement(name = "Name", required = false)
    protected String name;
    
    @XmlElement(name = "IDDocType", required = false)
    protected String idDocType;
    
    @XmlElement(name = "IDNumber", required = false)
    protected String idNumber;
    
    @XmlElement(name = "IDexpirydate", required = false)
    protected String idExpirydate;
    
    @XmlElement(name = "Nationality", required = false)
    protected String nationality;
    
    @XmlElement(name = "StreetAddress", required = false)
    protected String streetAddress;
    
    @XmlElement(name = "City", required = false)
    protected String city;
    
    @XmlElement(name = "PObox", required = false)
    protected String poBox;
    
    @XmlElement(name = "ContactPreference", required = false)
    protected String contactPreference;
    
    @XmlElement(name = "ZIPCode", required = false)
    protected String zipCode;
    
    @XmlElement(name = "Mobile", required = false)
    protected String mobile;
    
    @XmlElement(name = "HomeTelephone", required = false)
    protected String homeTelephone;
    
    @XmlElement(name = "WorkTelphone", required = false)
    protected String workTelphone;
    
    @XmlElement(name = "Fax", required = false)
    protected String fax;
    
    @XmlElement(name = "Email", required = false)
    protected String email;
    
    @XmlElement(name = "Gender", required = false)
    protected String gender;
    
    @XmlElement(name = "DOBformat", required = false)
    protected String dobFormat;
    
    @XmlElement(name = "Birthdate", required = false)
    protected String birthDate;
    
    @XmlElement(name = "TimeCreated", required = false)
    protected String timeCreated;
    
    @XmlElement(name = "MBAccountID", required = false)
    protected String mbAccountID;
    
    @XmlElement(name = "ParentAccountID", required = false)
    protected String parentAccountID;
    
    @XmlElement(name = "FirstName", required = false)
    protected String firstName;
    
    @XmlElement(name = "MidName", required = false)
    protected String midName;
    
    @XmlElement(name = "LastName", required = false)
    protected String lastName;
    
    @XmlElement(name = "Title", required = false)
    protected String title;
    
    @XmlElement(name = "LangPref", required = false)
    protected String langPref;
    
    @XmlElement(name = "CustomerSegment", required = false)
    protected String customerSegment;
    
    @XmlElement(name = "SIMNumber", required = false)
    protected String simNumber;
    
    @XmlElement(name = "IMSI", required = false)
    protected String imsi;
    
    @XmlElement(name = "Puk1", required = false)
    protected String puk1;
    
    @XmlElement(name = "Puk2", required = false)
    protected String puk2;
    
    @XmlElement(name = "ServMobile", required = false)
    protected String servMobile;
    
    @XmlElement(name = "CPESerialNumber", required = false)
    protected String cpeSerialNumber;
    
    @XmlElement(name = "DigitalCertificateNumber", required = false)
    protected String digitalCertificateNumber;
    
    @XmlElement(name = "WIMAXExpDate", required = false)
    protected String wimaxExpDate;
    
    @XmlElement(name = "WIMAXDuration", required = false)
    protected String wimaxDuration;
    
    @XmlElement(name = "VanityType", required = false)
    protected String vanityType;
    
    @XmlElement(name = "ProductLine", required = false)
    protected String productLine;
    
    @XmlElement(name = "PUK", required = false)
    protected String puk;
    
    @XmlElement(name = "PIN1", required = false)
    protected String pin1;
    
    @XmlElement(name = "PIN2", required = false)
    protected String pin2;
    
    @XmlElement(name = "FingerPrintFlag", required = false)
    protected String fingerPrintFlag;//Y|N
    
    @XmlElement(name = "IDExpiryFlag", required = false)
    protected String idExpiryFlag;//Y|N
    
    @XmlElement(name = "PayType", required = false)
    protected String payType; //1: Prepaid | 2: Postpaid 
    
    @XmlElement(name = "PackageDataType", required = false)
    protected String packageDataType;//GSM, LTE, etc
    
    @XmlElement(name = "PackageCategory", required = false)
    protected String packageCategory; //control, Voice (Control) or Control Plus, etc..

    public ConsumerReplyHeaderVO getHeaderVO() {
		return headerVO;
	}

	public void setHeaderVO(ConsumerReplyHeaderVO headerVO) {
		this.headerVO = headerVO;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getServiceAccountNo()
	{
		return serviceAccountNo;
	}

	public void setServiceAccountNo(String serviceAccountNo)
	{
		this.serviceAccountNo = serviceAccountNo;
	}
	
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}

	public void setBillingAccountNumber(String billingAccountNumber) {
		this.billingAccountNumber = billingAccountNumber;
	}

	public String getParentAccountNumber() {
		return parentAccountNumber;
	}

	public void setParentAccountNumber(String parentAccountNumber) {
		this.parentAccountNumber = parentAccountNumber;
	}

	public String getGenderChangeCounter() {
		return genderChangeCounter;
	}

	public void setGenderChangeCounter(String genderChangeCounter) {
		this.genderChangeCounter = genderChangeCounter;
	}

	public String getCustomerCategory() {
		return customerCategory;
	}

	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPackageID() {
		return packageID;
	}

	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdDocType() {
		return idDocType;
	}

	public void setIdDocType(String idDocType) {
		this.idDocType = idDocType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getIdExpirydate() {
		return idExpirydate;
	}

	public void setIdExpirydate(String idExpirydate) {
		this.idExpirydate = idExpirydate;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPoBox() {
		return poBox;
	}

	public void setPoBox(String poBox) {
		this.poBox = poBox;
	}

	public String getContactPreference() {
		return contactPreference;
	}

	public void setContactPreference(String contactPreference) {
		this.contactPreference = contactPreference;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getHomeTelephone() {
		return homeTelephone;
	}

	public void setHomeTelephone(String homeTelephone) {
		this.homeTelephone = homeTelephone;
	}

	public String getWorkTelphone() {
		return workTelphone;
	}

	public void setWorkTelphone(String workTelphone) {
		this.workTelphone = workTelphone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDobFormat() {
		return dobFormat;
	}

	public void setDobFormat(String dobFormat) {
		this.dobFormat = dobFormat;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getMbAccountID() {
		return mbAccountID;
	}

	public void setMbAccountID(String mbAccountID) {
		this.mbAccountID = mbAccountID;
	}

	public String getParentAccountID() {
		return parentAccountID;
	}

	public void setParentAccountID(String parentAccountID) {
		this.parentAccountID = parentAccountID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLangPref() {
		return langPref;
	}

	public void setLangPref(String langPref) {
		this.langPref = langPref;
	}

	public String getCustomerSegment() {
		return customerSegment;
	}

	public void setCustomerSegment(String customerSegment) {
		this.customerSegment = customerSegment;
	}

	public String getSimNumber() {
		return simNumber;
	}

	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getPuk1() {
		return puk1;
	}

	public void setPuk1(String puk1) {
		this.puk1 = puk1;
	}

	public String getPuk2() {
		return puk2;
	}

	public void setPuk2(String puk2) {
		this.puk2 = puk2;
	}

	public String getServMobile() {
		return servMobile;
	}

	public void setServMobile(String servMobile) {
		this.servMobile = servMobile;
	}

	public String getCpeSerialNumber() {
		return cpeSerialNumber;
	}

	public void setCpeSerialNumber(String cpeSerialNumber) {
		this.cpeSerialNumber = cpeSerialNumber;
	}

	public String getDigitalCertificateNumber() {
		return digitalCertificateNumber;
	}

	public void setDigitalCertificateNumber(String digitalCertificateNumber) {
		this.digitalCertificateNumber = digitalCertificateNumber;
	}

	public String getWimaxExpDate() {
		return wimaxExpDate;
	}

	public void setWimaxExpDate(String wimaxExpDate) {
		this.wimaxExpDate = wimaxExpDate;
	}

	public String getWimaxDuration() {
		return wimaxDuration;
	}

	public void setWimaxDuration(String wimaxDuration) {
		this.wimaxDuration = wimaxDuration;
	}

	public String getVanityType() {
		return vanityType;
	}

	public void setVanityType(String vanityType) {
		this.vanityType = vanityType;
	}

	public String getProductLine() {
		return productLine;
	}

	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	
	

	public String getPuk()
	{
		return puk;
	}

	public void setPuk(String puk)
	{
		this.puk = puk;
	}

	public String getPin1()
	{
		return pin1;
	}

	public void setPin1(String pin1)
	{
		this.pin1 = pin1;
	}

	public String getPin2()
	{
		return pin2;
	}

	public void setPin2(String pin2)
	{
		this.pin2 = pin2;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	
	
	public String getFingerPrintFlag()
	{
		return fingerPrintFlag;
	}

	public void setFingerPrintFlag(String fingerPrintFlag)
	{
		this.fingerPrintFlag = fingerPrintFlag;
	}

	public String getIdExpiryFlag()
	{
		return idExpiryFlag;
	}

	public void setIdExpiryFlag(String idExpiryFlag)
	{
		this.idExpiryFlag = idExpiryFlag;
	}

	public String getPayType()
	{
		return payType;
	}

	public void setPayType(String payType)
	{
		this.payType = payType;
	}

	public String getPackageDataType()
	{
		return packageDataType;
	}

	public void setPackageDataType(String packageDataType)
	{
		this.packageDataType = packageDataType;
	}

	public String getPackageCategory()
	{
		return packageCategory;
	}

	public void setPackageCategory(String packageCategory)
	{
		this.packageCategory = packageCategory;
	}

public static void main(String[] args)
{
	String reply="<?xml version=\"1.0\" encoding=\"UTF-8\"?><NS1:retrieveCustomerProductConfigurationRs xmlns:NS1=\"http://www.ejada.com\"><MsgRsHdr xmlns=\"http://www.ejada.com\"><StatusCode>I000000</StatusCode><RqUID>20131030101252</RqUID></MsgRsHdr><Body><NS2:customerConfiguration xmlns:NS2=\"http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/CustomerConfigurationMessage\"><Customer><fromTimestamp>2013-03-26T09:53:48</fromTimestamp><Party><Id>1000113406136308</Id><Specification><Category>P</Category><Type>Consumer</Type></Specification><PartyIdentification><Specification><Type>IQAMA</Type></Specification><Id>2228764391</Id></PartyIdentification><CharacteristicValue><Characteristic><Name>IsCorporateKA</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CustomerBSLId</Name></Characteristic><Value>1-69990EX</Value></CharacteristicValue><PartyExtensions><PartyChoice><Individual><Gender>F</Gender></Individual></PartyChoice></PartyExtensions></Party></Customer><Agreement><CustomerAccountId><Id>1000113620426585</Id></CustomerAccountId><AgreementItem><CharacteristicValue><Characteristic><Name>MSISDN</Name></Characteristic><Value>966540510212</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageId</Name></Characteristic><Value>1785</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsIUC</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageName</Name></Characteristic><Value>Wajid Plus</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>BillingId</Name></Characteristic><Value>1000113620426572</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageDescription</Name></Characteristic><Value>Wajid Plus</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ServiceId</Name></Characteristic><Value>1000113620426585</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ServiceStatus</Name></Characteristic><Value>Active</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>DealName</Name></Characteristic><Value>Unknown Deal Name</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ISMSIM</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PayType</Name></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>Duration</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>MAXUploadRate</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>MaxDownloadRate</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageServiceType</Name></Characteristic><Value>GSM</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PlanCategory</Name></Characteristic><Value>5</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ProductLine</Name></Characteristic><Value>Non UDP - UDP PC IUC FROM PACK</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CustomerActualSegment</Name></Characteristic><Value>SILVER</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsNewControl</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageCategory</Name></Characteristic><Value>GSM</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CorporatePackage</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsMix</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsControlPlus</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsIPhone</Name></Characteristic><Value>N</Value></CharacteristicValue></AgreementItem></Agreement></NS2:customerConfiguration></Body></NS1:retrieveCustomerProductConfigurationRs>";//("<NS1:retrieveCustomerProductConfigurationRs><MsgRsHdr><StatusCode>I000000</StatusCode><RqUID>20130312112500101</RqUID></MsgRsHdr><Body><NS2:customerConfiguration><Customer><fromTimestamp>2013-03-26T09:53:48</fromTimestamp><Party><Id>1000113406136308</Id><Specification><Category>P</Category><Type>Consumer</Type></Specification><PartyIdentification><Specification><Type>IQAMA</Type></Specification><Id>2228764391</Id></PartyIdentification><CharacteristicValue><Characteristic><Name>IsCorporateKA</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CustomerBSLId</Name></Characteristic><Value>1-69990EX</Value></CharacteristicValue><PartyExtensions><PartyChoice><Individual><Gender>F</Gender></Individual></PartyChoice></PartyExtensions></Party></Customer><Agreement><CustomerAccountId><Id>1000113620426585</Id></CustomerAccountId><AgreementItem><CharacteristicValue><Characteristic><Name>MSISDN</Name></Characteristic><Value>966540510212</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageId</Name></Characteristic><Value>1785</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsIUC</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageName</Name></Characteristic><Value>Wajid Plus</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>BillingId</Name></Characteristic><Value>1000113620426572</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageDescription</Name></Characteristic><Value>Wajid Plus</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ServiceId</Name></Characteristic><Value>1000113620426585</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ServiceStatus</Name></Characteristic><Value>Active</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>DealName</Name></Characteristic><Value>Unknown Deal Name</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ISMSIM</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PayType</Name></Characteristic><Value>2</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>Duration</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>MAXUploadRate</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>MaxDownloadRate</Name></Characteristic><Value/></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageServiceType</Name></Characteristic><Value>GSM</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PlanCategory</Name></Characteristic><Value>5</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>ProductLine</Name></Characteristic><Value>Non UDP - UDP PC IUC FROM PACK</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CustomerActualSegment</Name></Characteristic><Value>SILVER</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsNewControl</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>PackageCategory</Name></Characteristic><Value>GSM</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>CorporatePackage</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsMix</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsControlPlus</Name></Characteristic><Value>N</Value></CharacteristicValue><CharacteristicValue><Characteristic><Name>IsIPhone</Name></Characteristic><Value>N</Value></CharacteristicValue></AgreementItem></Agreement></NS2:customerConfiguration></Body></NS1:retrieveCustomerProductConfigurationRs>");
	
	try
	{
//		System.out.println(JAXBUtilities.getInstance().unmarshal(RetrieveCustomerProductConfigurationRs.class, reply));
		
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
}	
}
