package sa.com.mobily.eportal.common.service.vo;

import java.util.List;


public class ProfileContactVO {
	
   private String msisdn = "";
   private String contactName = "";
   private String profileId = "";
   private String profileName = "";
   private String allocatedMinutes = "";
   private int memberCount = 0;
   private String packageName = "";
   private String customerType = "";
   private String oldProfileMsisdn = "";
   private String newProfileMsisdn = "";
   private String accountNumber = "";
   private String emailAddress = "";
   
   private List profileContactVOList;

   
	public String getAccountNumber() {
	return accountNumber;
}
public void setAccountNumber(String accountNumber) {
	this.accountNumber = accountNumber;
}
public String getEmailAddress() {
	return emailAddress;
}
public void setEmailAddress(String emailAddress) {
	this.emailAddress = emailAddress;
}
	/**
	 * @return Returns the customerType.
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * @param customerType The customerType to set.
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * @return Returns the packageName.
	 */
	public String getPackageName() {
		return packageName;
	}
	/**
	 * @param packageName The packageName to set.
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	/**
	 * @return Returns the memberCount.
	 */
	public int getMemberCount() {
		return memberCount;
	}
	/**
	 * @param memberCount The memberCount to set.
	 */
	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}
	/**
	 * @return Returns the profileContactVOList.
	 */
	public List getProfileContactVOList() {
		return profileContactVOList;
	}
	/**
	 * @param profileContactVOList The profileContactVOList to set.
	 */
	public void setProfileContactVOList(List profileContactVOList) {
		this.profileContactVOList = profileContactVOList;
	}
	/**
	 * @return Returns the contactName.
	 */
	public String getContactName() {
		return contactName;
	}
	/**
	 * @param contactName The contactName to set.
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	
	/**
	 * @return Returns the allocatedMinutes.
	 */
	public String getAllocatedMinutes() {
		return allocatedMinutes;
	}
	/**
	 * @param allocatedMinutes The allocatedMinutes to set.
	 */
	public void setAllocatedMinutes(String allocatedMinutes) {
		this.allocatedMinutes = allocatedMinutes;
	}
	/**
	 * @return Returns the profileId.
	 */
	public String getProfileId() {
		return profileId;
	}
	/**
	 * @param profileId The profileId to set.
	 */
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	/**
	 * @return Returns the profileName.
	 */
	public String getProfileName() {
		return profileName;
	}
	/**
	 * @param profileName The profileName to set.
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	
	/**
	 * @return Returns the newProfileMsisdn.
	 */
	public String getNewProfileMsisdn() {
	    return newProfileMsisdn;
	}
	/**
	 * @param newProfileMsisdn The newProfileMsisdn to set.
	 */
	public void setNewProfileMsisdn(String newProfileMsisdn) {
	    this.newProfileMsisdn = newProfileMsisdn;
	}
	/**
	 * @return Returns the oldProfileMsisdn.
	 */
	public String getOldProfileMsisdn() {
	    return oldProfileMsisdn;
	}
	/**
	 * @param oldProfileMsisdn The oldProfileMsisdn to set.
	 */
	public void setOldProfileMsisdn(String oldProfileMsisdn) {
	    this.oldProfileMsisdn = oldProfileMsisdn;
	}
}
