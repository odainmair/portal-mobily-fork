package sa.com.mobily.eportal.common.service.util.context;

import java.util.List;
import java.util.logging.Level;

public interface ExecutionContext {
	
	public final static String LOG_LEVEL_INFO = "INFO";
	void log(LogEntryVO executionContextVO);
	void log(Level logLevel, String message, String className, String methodName, Throwable error);
	void audit(LogEntryVO executionContextVO);
	void audit(Level logLevel, String message, String className, String methodName);
	//public List<ExecutionContextVO> getErrorCaseLog();
	void clearExecutionContext();
	void startMethod(String className, String methodName, List<Object> parameters);
	void endMethod(String className, String methodName, Object result);
	void print(String fileName , boolean exception);
	public void log(Level logLevel, String message, String className, String methodName);
	public void fine(String message);
	public void severe(String message);
	public void info(String message);
	
	
}