/**
 * 
 */
package sa.com.mobily.eportal.action.auditing.service.xml;

import java.text.SimpleDateFormat;
import java.util.logging.Level;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import sa.com.mobily.eportal.action.audit.vo.ActionAuditingRequestVO;
import sa.com.mobily.eportal.action.auditing.jaxb.object.ActionAuditingRequest;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.core.service.InvalidParametersException;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

/**
 * @author m.hassan.dar
 * 
 */
public class ActionAuditingXMLHandler
{
	private static final String className = ActionAuditingXMLHandler.class.getName();

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("action-auditing/actionAudit");

	public ActionAuditingXMLHandler()
	{
		// TODO Auto-generated constructor stub
	}

	public static ActionAuditingRequestVO parseActionAuditingRequestXML(String xmlRequest)
	{
		String method = "parseActionAuditingRequestXML";
		ActionAuditingRequestVO requestVO = null;

		
		executionContext.log(Level.INFO, "parseActionAuditingRequestXML -> " + xmlRequest, className, method);
		
		try
		{

			ActionAuditingRequest actionAuditingRequest = (ActionAuditingRequest) JAXBUtilities.getInstance().unmarshal(ActionAuditingRequest.class, xmlRequest);
			/*
			 * JAXBContext jaxbContext =
			 * JAXBContext.newInstance(ActionAuditingRequest.class);
			 * Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			 * ActionAuditingRequest actionAuditingRequest =
			 * (ActionAuditingRequest) unmarshaller.unmarshal(new
			 * StringReader(xmlRequest));
			 */

			if (null != actionAuditingRequest)
			{
				requestVO = new ActionAuditingRequestVO();

				requestVO.setActionId(new Integer(actionAuditingRequest.getActionId()));

				

				if (null != actionAuditingRequest.getSan())
					requestVO.setSan(actionAuditingRequest.getSan());

				requestVO.setStatus(new Integer(actionAuditingRequest.getStatus()));

				if (null != actionAuditingRequest.getUsername())
					requestVO.setUsername(actionAuditingRequest.getUsername());

				if (null != actionAuditingRequest.getErrorMessage())
					requestVO.setErrorMessage(actionAuditingRequest.getErrorMessage());


				if (null != actionAuditingRequest.getErrorCode())
					requestVO.setErrorCode(actionAuditingRequest.getErrorCode());

				
				
				requestVO.setActionDate(new SimpleDateFormat("yyyyMMddHHmmss").parse(actionAuditingRequest.getActionDate()));

			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "XMLStreamException::" + e.getMessage(), className, method, e);
		}

		return requestVO;

	}

	public static String generateActionAuditingRequestXML(ActionAuditingRequestVO requestVO)
	{

		String method = "generateActionAuditingRequestXML";
		String xmlRequest = "";

		if (null == requestVO)
		{
			throw new InvalidParametersException("ActionAuditingRequestVO -> IS NULL ");
		}

		if (null == requestVO.getActionDate())
		{
			throw new InvalidParametersException("ActionAuditingRequestVO -> actionDate -> IS NULL ");
		}

		if (null == requestVO.getActionId())
		{
			throw new InvalidParametersException("ActionAuditingRequestVO -> actionId -> IS NULL ");
		}

		/*
		if (null == requestVO.getErrorCode())
		{
			throw new InvalidParametersException("ActionAuditingRequestVO -> errorCode -> IS NULL ");
		}
		*/
		
		
		
		if (null == requestVO.getStatus())
		{
			throw new InvalidParametersException("ActionAuditingRequestVO -> status -> IS NULL ");
		}

		ActionAuditingRequest actionAuditingRequest = new ActionAuditingRequest();

		actionAuditingRequest.setActionId(String.valueOf(requestVO.getActionId()));
		if (null != requestVO.getSan())
			actionAuditingRequest.setSan(requestVO.getSan());

		if (null != requestVO.getUsername())
			actionAuditingRequest.setUsername(requestVO.getUsername());

		actionAuditingRequest.setStatus(String.valueOf(requestVO.getStatus()));
		
		if (null != requestVO.getErrorCode())
		actionAuditingRequest.setErrorCode(requestVO.getErrorCode());
		
		actionAuditingRequest.setActionDate(new SimpleDateFormat("yyyyMMddHHmmss").format(requestVO.getActionDate()));

		if (null != requestVO.getErrorMessage())
			actionAuditingRequest.setErrorMessage(requestVO.getErrorMessage());

		final ByteOutputStream outputStream = new ByteOutputStream();
		XMLStreamWriter xmlStreamWriter = null;
		try
		{
			/*
			 * final XMLOutputFactory outputFactory =
			 * XMLOutputFactory.newInstance();
			 * 
			 * xmlStreamWriter =
			 * outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
			 * JAXBContext jaxbContext =
			 * JAXBContext.newInstance(ActionAuditingRequest.class); Marshaller
			 * jaxbMarshaller = jaxbContext.createMarshaller();
			 * jaxbMarshaller.marshal(actionAuditingRequest, xmlStreamWriter);
			 * outputStream.flush();
			 * 
			 * // xmlRequest = new String(outputStream.getBytes(),
			 * "UTF-8").trim();
			 */

			xmlRequest = JAXBUtilities.getInstance().marshal(actionAuditingRequest);
			
			executionContext.log(Level.INFO, "generateActionAuditingRequestXML -> " + xmlRequest, className, method);

		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception -> " + e.getMessage(), className, method, e);

		}
		finally
		{
			if (xmlStreamWriter != null)
			{
				try
				{
					xmlStreamWriter.close();
				}
				catch (XMLStreamException e)
				{
					executionContext.log(Level.SEVERE, "XMLStreamException::" + e.getMessage(), className, method, e);
				}
			}
			if (outputStream != null)
			{
				outputStream.close();
			}
		}

		return xmlRequest;
	}

}
