/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EBillRequestHeaderVO {
	private String funcId = null;
	private String securityKey = null;
	private String msgVersion = null;
	private String requestorChannelId = null;
	private String srDate = null;
	private String requestorUserId = null;
	private String requestorLanguage = null;
	private String header =null;
	/**
	 * @return Returns the header.
	 */
	public String getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(String header) {
		this.header = header;
	}
	/**
	 * @return Returns the funcId.
	 */
	public String getFuncId() {
		return funcId;
	}
	/**
	 * @param funcId The funcId to set.
	 */
	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}
	/**
	 * @return Returns the msgVersion.
	 */
	public String getMsgVersion() {
		return msgVersion;
	}
	/**
	 * @param msgVersion The msgVersion to set.
	 */
	public void setMsgVersion(String msgVersion) {
		this.msgVersion = msgVersion;
	}
	/**
	 * @return Returns the requestorChannelId.
	 */
	public String getRequestorChannelId() {
		return requestorChannelId;
	}
	/**
	 * @param requestorChannelId The requestorChannelId to set.
	 */
	public void setRequestorChannelId(String requestorChannelId) {
		this.requestorChannelId = requestorChannelId;
	}
	/**
	 * @return Returns the requestorLanguage.
	 */
	public String getRequestorLanguage() {
		return requestorLanguage;
	}
	/**
	 * @param requestorLanguage The requestorLanguage to set.
	 */
	public void setRequestorLanguage(String requestorLanguage) {
		this.requestorLanguage = requestorLanguage;
	}
	/**
	 * @return Returns the requestorUserId.
	 */
	public String getRequestorUserId() {
		return requestorUserId;
	}
	/**
	 * @param requestorUserId The requestorUserId to set.
	 */
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	/**
	 * @return Returns the securityKey.
	 */
	public String getSecurityKey() {
		return securityKey;
	}
	/**
	 * @param securityKey The securityKey to set.
	 */
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	/**
	 * @return Returns the srDate.
	 */
	public String getSrDate() {
		return srDate;
	}
	/**
	 * @param srDate The srDate to set.
	 */
	public void setSrDate(String srDate) {
		this.srDate = srDate;
	}
}
