/*
 * Created on Sep 14, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CreditCardDetailsVO extends BaseVO{
	
	public String id = null;
	public String creditCardNumber = null;
	public String creditCardType = null;
	public String primeMSISDN = null;
	public String creditCardStatus = null;

	/**
	 * @return Returns the creditCardNumber.
	 */
	public String getCreditCardNumber() {
		return creditCardNumber;
	}
	/**
	 * @param creditCardNumber The creditCardNumber to set.
	 */
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}
	/**
	 * @return Returns the creditCardType.
	 */
	public String getCreditCardType() {
		return creditCardType;
	}
	/**
	 * @param creditCardType The creditCardType to set.
	 */
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}
	/**
	 * @return Returns the iD.
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id The iD to set.
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return Returns the primeMSISDN.
	 */
	public String getPrimeMSISDN() {
		return primeMSISDN;
	}
	/**
	 * @param primeMSISDN The primeMSISDN to set.
	 */
	public void setPrimeMSISDN(String primeMSISDN) {
		this.primeMSISDN = primeMSISDN;
	}
	
	/**
	 * @return Returns the creditCardStatus.
	 */
	public String getCreditCardStatus() {
		return creditCardStatus;
	}
	/**
	 * @param creditCardStatus The creditCardStatus to set.
	 */
	public void setCreditCardStatus(String creditCardStatus) {
		this.creditCardStatus = creditCardStatus;
	}
}
