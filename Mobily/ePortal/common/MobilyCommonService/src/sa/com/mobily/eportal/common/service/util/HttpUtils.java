/*
 * Created on Sep 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HttpUtils {

	private static final Logger log = LoggerInterface.log;

	private static final String UTF8 = "UTF8";

	private static final String HTTP_PROPERTY_CONTENT_LENGTH = "Content-Length";

	private static final String HTTP_PROPERTY_CONTENT_TYPE = "Content-Type";

	private static final String HTTP_PROPERTY_TEXT_XML = "text/xml";

	/**
	 * 
	 * @param aURL
	 * @param aXMLRequest
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws Exception
	 */
	public static String sendXmlTOAction(String aURL, String aXMLRequest)throws SystemException {

		URL tempURL = null;
		StringBuffer tempStringBuffer = new StringBuffer();
		int tempCounter = 0;

		log.fatal("HttpUtils > MC > sendXmlTOAction > aURL >"+ aURL);
		log.fatal("HttpUtils > MC > sendXmlTOAction > aXMLRequest >"+ aXMLRequest);
		
		InputStreamReader tempInputStreamReader = null;
		try {
			tempURL = new URL(aURL);
			tempInputStreamReader = sendXmlRpc(tempURL, aXMLRequest);

			while ((tempCounter = tempInputStreamReader.read()) != -1) {
				tempStringBuffer.append((char) tempCounter);
			}
		
		} catch (IOException tempIOException) {
			log.fatal("HttpUtils >  MC > sendXmlTOAction > IOException occurred in connecting to Mobily Services over Http >"+ tempIOException.getMessage());
			throw new SystemException(tempIOException);
		} catch (Exception tempException) {
			log.fatal("HttpUtils >  MC > sendXmlTOAction > Exception occurred in connecting to Mobily Services over Http >"+tempException.getMessage());
			throw new SystemException(tempException);
		}
		return tempStringBuffer.toString();
	}

	/**
	 * This method is used to manage the connection, sending and receiving of XML over Http
	 * @param aURL The URL representing the URL of the Back-end
	 * @param aXML The String representing the XML to be sent
	 * 
	 * @return String The XML reply document
	 * 
	 * @exception IOException
	 */
	private static InputStreamReader sendXmlRpc(URL aURL, String aXML) throws SystemException {

		URLConnection tempURLConnection = null;
		OutputStream tempOutputStream = null;
		OutputStreamWriter tempOutputStreamWriter = null;
		InputStreamReader tempInputStreamReader = null;

		try {
			tempURLConnection = aURL.openConnection();
			tempURLConnection.setDoOutput(true);
			tempURLConnection.setUseCaches(false);
			tempURLConnection.setAllowUserInteraction(false);
			tempURLConnection.setRequestProperty(HTTP_PROPERTY_CONTENT_LENGTH, String.valueOf(aXML.length()));
			tempURLConnection.setRequestProperty(HTTP_PROPERTY_CONTENT_TYPE, HTTP_PROPERTY_TEXT_XML);
			tempOutputStream = tempURLConnection.getOutputStream();
			tempOutputStreamWriter = new OutputStreamWriter(tempOutputStream,UTF8);
			tempOutputStreamWriter.write(aXML);
			tempOutputStreamWriter.flush();
			tempOutputStreamWriter.close();
			tempInputStreamReader = new InputStreamReader(tempURLConnection.getInputStream(), UTF8);
		} catch (IOException tempIOException) {
			log.fatal("HttpUtils >  MC > sendXmlRpc > IOException occurred in connecting to Mobily Services over Http >"+ tempIOException.getMessage());
			throw new SystemException(tempIOException);
		} catch (Exception tempException) {
			log.fatal("HttpUtils >  MC > sendXmlRpc > Exception occurred in connecting to Mobily Services over Http >"+ tempException.getMessage());
			throw new SystemException(tempException);
		}

		return tempInputStreamReader;
	}
	
	/**
	 * This method is used to send and receive XMLs over Http
	 * @param aURL The String representing the URL of the Back-end
	 * @param aXML The String representing the XML to be sent
	 * 
	 * @return String The XML reply document
	 * 
	 * @exception SystemException
	 */
    
	public static String sendXMLtoIS( String aURL, String aXML ) throws SystemException{

		log.debug(" HttpUtils > MC >sending to url :> " + aURL);
    	log.debug(" HttpUtils > MC >sending xml :>" + aXML);
        URL tempURL = null;
	    StringBuffer tempStringBuffer = new StringBuffer();
	    int tempCounter = 0;
	    InputStreamReader tempInputStreamReader = null;
	    
	    try {
            tempURL = new URL(aURL);	    
	    	tempInputStreamReader = sendXmlRpc(tempURL, aXML);
	    	
	    	while((tempCounter = tempInputStreamReader.read()) != -1) {    
	    	    tempStringBuffer.append( (char)tempCounter );
	    	}
        }catch(Exception tempException){
            log.fatal("HttpUtilsBO > sendXMLtoIS > Exception occurred in connecting to Mobily Services over Http");
            throw new SystemException(tempException);
  		}
        return tempStringBuffer.toString();

	}

	
	/**
	 * Retrieves the XMLRequest message from the HTTP Header
	 * 
	 * @param httpRequest
	 * @return
	 * @throws SystemException
	 */
	public static String receiveHttpRequestInBody(HttpServletRequest httpRequest) throws SystemException {
		log.info("HttpUtils > MC > receiveHttpRequestInBody > called");
		int tempContentLength = httpRequest.getContentLength();
		
		if (tempContentLength < 0)
			tempContentLength = 0; // Added by Shady
		char[] xmlRequest = new char[tempContentLength];
		
		try {
			BufferedReader tempBufferReader = httpRequest.getReader();
			int tempCounter = 0;
			int tempBytesRead = 0;

			do {
				tempCounter = tempBufferReader.read(xmlRequest, tempBytesRead,xmlRequest.length - tempBytesRead);
				if (tempCounter > 0) {
					tempBytesRead += tempCounter;
				}
			} while ((tempCounter != -1) && (tempBytesRead < tempContentLength));
		} catch (Exception e) {
			log.fatal("HttpUtils > MC > receiveHttpRequestInBody > Exception >"+e.getMessage()); 
			throw new SystemException(e);
		}
		
		return new String(xmlRequest);
	}

}