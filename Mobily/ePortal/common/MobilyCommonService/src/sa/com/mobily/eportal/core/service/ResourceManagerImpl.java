//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.LookupItem;
import sa.com.mobily.eportal.core.api.MethodNotSupportedException;
import sa.com.mobily.eportal.core.api.PinCode;
import sa.com.mobily.eportal.core.api.ResourceManager;
import sa.com.mobily.eportal.core.api.ResourceNotFoundException;
import sa.com.mobily.eportal.core.api.ServiceException;

/**
 * The EJB implementation for all the functional services provided by the
 * ResourceManager.
 * <p>
 * The ResourceManagerImpl class has generic CRUD functions, and provides
 * the set of backend services needed by the inherited modules.
 * It provides CRUD operations to manage backend resources.
 * It contains the basic REST-based methods (get, post, put, delete).
 * It also contains the common services that include retrieval of generic
 * items and creation of pin code.
 * </p>
 */
public class ResourceManagerImpl implements ResourceManager {

    private URLResolver<ResourceHandler> resolver;
    private CacheManager cache;
    protected ResourceMapper mapper;

    CountryHandler countryHandler;
    CityHandler cityHandler;
    RegionsHandler regionsHandler;
    EmiratesHandler emiratesHandler;
    GovernerateHandler governerateHandler;
    PinCodeHandler pinCodeHandler;

    public ResourceManagerImpl() {

        resolver = new URLResolver<ResourceHandler>();
        cache = new CacheManager();
        mapper = new ResourceMapper();

        countryHandler = new CountryHandler();
        cityHandler = new CityHandler();
        regionsHandler = new RegionsHandler();
        emiratesHandler = new EmiratesHandler();
        governerateHandler = new GovernerateHandler();
        pinCodeHandler = new PinCodeHandler();

        addResourceHandler("/countries/{id?}", new ResourceHandler(false, true, true, false) {

            public Object find(Context ctx, Map<String, String> params) throws ServiceException {
                return countryHandler.find(ctx, params);
            }
        });

        addResourceHandler("/cities/{id?}", new ResourceHandler(false, true, true, false) {

            public Object find(Context ctx, Map<String, String> params) throws ServiceException {
                return cityHandler.find(ctx, params);
            }
        });

        addResourceHandler("/regions/{id?}", new ResourceHandler(false, true, true, false) {

            public Object find(Context ctx, Map<String, String> params) throws ServiceException {
                return regionsHandler.find(ctx, params);
            }
        });

        addResourceHandler("/emirates/{id?}", new ResourceHandler(false, true, true, false) {

            public Object find(Context ctx, Map<String, String> params) throws ServiceException {
                return emiratesHandler.find(ctx, params);
            }
        });

        addResourceHandler("/governerate/{id?}", new ResourceHandler(false, true, true, false) {

            public Object find(Context ctx, Map<String, String> params) throws ServiceException {
                return governerateHandler.find(ctx, params);
            }
        });

        addResourceHandler("/pincode/{id?}", new ResourceHandler(false, false, false, false) {

            public Object create(Context ctx, String data) throws ServiceException {
                return pinCodeHandler.create(ctx, (PinCode) mapper.read(data, PinCode.class));
            }
        });
    }

	/**
	 * Retrieves a resource object.
	 * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be retrieved
     * @return the resource object
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
	 */
    public Object get(Context ctx, String uri) throws ServiceException {

        Map<String, String> params = new HashMap<String, String>();
        ResourceHandler handler = resolver.resolve(uri, params);
        if (handler == null) { throw new ResourceNotFoundException(uri); }

        String id = params.get("id");
        Object resource = null;

        if (id != null || handler.sigeleton) {
            resource = (handler.cacheRead) ? cache.get(ctx, uri, handler.cacheGlobal) : null;
            if (resource == null) {
                resource = handler.get(ctx, id);
                if (handler.cacheRead) {
                    cache.add(ctx, uri, handler.cacheGlobal, resource);
                }
            }
        } else {
            resource = (handler.cacheFind) ? cache.get(ctx, uri, handler.cacheGlobal) : null;
            if (resource == null) {
                resource = handler.find(ctx, params);
                if (handler.cacheFind) {
                    cache.add(ctx, uri, handler.cacheGlobal, resource);
                }
            }
        }
        return resource;
    }

    /**
     * Submits a resource creation request.
     * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be created
     * @param data the data to be submitted to the backend
     * @return the created resource object
     * @throws ServiceException when an error occurs while processing the
     * submission request
     * 
     * @see Context
     */
    public Object post(Context ctx, String uri, String data) throws ServiceException {

        Map<String, String> params = new HashMap<String, String>();
        ResourceHandler handler = resolver.resolve(uri, params);
        if (handler == null) { throw new ResourceNotFoundException(uri); }

        return handler.create(ctx, data);
    }

    /**
     * Updates a resource the with new data.
     * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be updated
     * @param data the data to be submitted to the backend
     * @throws ServiceException when an error occurs while processing the
     * submission request
     * 
     * @see Context
     */
    public void put(Context ctx, String uri, String data) throws ServiceException {

        Map<String, String> params = new HashMap<String, String>();
        ResourceHandler handler = resolver.resolve(uri, params);
        if (handler == null) { throw new ResourceNotFoundException(uri); }

        String id = params.get("id");
        if (!handler.sigeleton &&  id == null)  {
            throw new ResourceNotFoundException(uri); //todo
        }

        handler.update(ctx, id, data);
        cache.invalidate(ctx, uri, handler.cacheGlobal);
    }

    /**
     * Submits a request to delete a resource.
     * 
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param uri the unique identification of the resource to be deleted
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     */
    public void delete(Context ctx, String uri) throws ServiceException {

        Map<String, String> params = new HashMap<String, String>();
        ResourceHandler handler = resolver.resolve(uri, params);
        if (handler == null) { throw new ResourceNotFoundException(uri); }

        String id = params.get("id");
        if (id == null) { throw new ResourceNotFoundException(uri); } //todo

        handler.delete(ctx, id);
        cache.invalidate(ctx, uri, handler.cacheGlobal);
    }

    /**
     * Retrieves the list of Country resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findCountry(Context ctx, Map<String, String> params) throws ServiceException {
        return countryHandler.find(ctx, params);
    }

    /**
     * Retrieves the list of City resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findCity(Context ctx, Map<String, String> params) throws ServiceException {
        return cityHandler.find(ctx, params);
    }

    /**
     * Retrieves the list of Regions resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findRegions(Context ctx, Map<String, String> params) throws ServiceException {
        return regionsHandler.find(ctx, params);
    }

    /**
     * Retrieves the list of Emirates resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findEmirates(Context ctx, Map<String, String> params) throws ServiceException {
        return emiratesHandler.find(ctx, params);
    }

    /**
     * Retrieves the list of Governerate resources
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param params the list of name/value pair parameters associated with the query
     * @return the list of of resources
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see LookupItem
     */
    public List<LookupItem> findGovernerate(Context ctx, Map<String, String> params) throws ServiceException {
        return governerateHandler.find(ctx, params);
    }

    /**
     * Submits a Pin Code creation request.
     *
     * @param ctx an implementation of the context interface that provides
     * the necessary information about the current user request
     * @param data the PinCode data to be submitted to the backend
     * @return the new PinCode resource
     * @throws ServiceException when an error occurs while processing the
     * submission request
     *
     * @see Context
     * @see PinCode
     * @see PinCode
     */
    public PinCode createPinCode(Context ctx, PinCode data) throws ServiceException {
        return pinCodeHandler.create(ctx, data);
    }

    protected void addResourceHandler(String uri, ResourceHandler handler) {
        resolver.addURLMapping(uri, handler);
    }

    protected static class ResourceHandler {
        private boolean cacheRead;
        private boolean cacheFind;
        private boolean cacheGlobal;
        private boolean sigeleton;

        protected ResourceHandler(boolean cacheRead, boolean cacheFind, boolean cacheGlobal, boolean sigeleton) {
            this.cacheRead = cacheRead;
            this.cacheFind = cacheFind;
            this.cacheGlobal = cacheGlobal;
            this.sigeleton = sigeleton;
        }

        public Object create(Context ctx, String data) throws ServiceException {
            throw new MethodNotSupportedException("create method is not supported for this resource");
        }

        public void update(Context ctx, String id, String data) throws ServiceException {
            throw new MethodNotSupportedException("update method is not supported for this resource");
        }

        public void delete(Context ctx, String id) throws ServiceException {
            throw new MethodNotSupportedException("delete method is not supported for this resource");
        }

        public Object get(Context ctx, String id) throws ServiceException {
            throw new MethodNotSupportedException("get method is not supported for this resource");
        }

        public Object find(Context ctx, Map<String, String> params) throws ServiceException {
            throw new MethodNotSupportedException("find method is not supported for this resource");
        }
    }
}
