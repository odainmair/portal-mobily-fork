package sa.com.mobily.eportal.common.service.util.context;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.service.util.LoggerHelper;
import sa.com.mobily.eportal.common.service.util.StackTraceUtil;

public class ExecutionContextLogger implements ExecutionContext
{
	private static SimpleFormatter formatter;
    private   Logger logger;

	public ExecutionContextLogger(String loggername){
		logger=LoggerHelper.getLogger(loggername);
	}

	@Override
	public void log(LogEntryVO executionContextVO)
	{
		//executionContextVO.logger.log
		log(executionContextVO.getLogLevel(), executionContextVO.getMessage(), executionContextVO.getClassName(), executionContextVO.getMethodName(), executionContextVO.getError());
	}
	
	public void log(Level logLevel, String message){
		logger.log(logLevel,message);
	}
	@Override
	public void log(Level logLevel, String message, String className, String methodName, Throwable throwable)
	{
		if(throwable==null)
		logger.logp(logLevel,className,methodName,message);
		else 
			logger.logp(logLevel,className,methodName,message,throwable);
	}

	@Override
	public void log(Level logLevel, String message, String className, String methodName)
	{
		logger.logp(logLevel,className,methodName,message);
	}
	@Override
	public void fine(String message){
		 logger.fine(message);
	}
	@Override
	public void severe(String message){
		 logger.severe(message);
	}
	@Override
	public void info(String message){
		 logger.info(message);
	}
	
	@Override
	public void audit(LogEntryVO executionContextVO)
	{
		audit(executionContextVO.getLogLevel(), executionContextVO.getMessage(), executionContextVO.getClassName(), executionContextVO.getMethodName());
	}
	
	@Override
	public void audit(Level logLevel, String message, String className, String methodName)
	{
		logger.logp(logLevel,className,methodName,message);
	}

	
		@Override
	public void startMethod(String className, String methodName, List<Object> parameters)
	{
		if(parameters!=null)
		logger.entering(className, methodName, parameters.toArray());
		else 
			logger.entering(className, methodName);
	
	}

	@Override
	public void endMethod(String className, String methodName, Object result)
	{
		logger.exiting(className, methodName, result);
      //logger.exfine("method  "+methodName+" has been ended in class "+className);	
	}

	@Override
	@Deprecated
	public void print(String fileName, boolean exception)
	{
		//Logger logger = LoggerHelper.getLogger(fileName);
		//logger.setLevel(Level.FINEST);
	}

	private void print(Logger logger, StringBuffer messages)
	{
		if (messages != null && !messages.toString().trim().equals(""))
		{
			logger.logp(Level.INFO, logger.getName(), "", messages.toString());
		}
	}
	
	
	
	private static SimpleFormatter getFormatter(){
		if(formatter == null)
			formatter = new SimpleFormatter();
		return formatter;
	}

	@Override
	public void clearExecutionContext()
	{
		// TODO Auto-generated method stub
		
	}
}