/*
 * Created on Nov 11, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;


import javax.portlet.PortletRequest;

import org.apache.log4j.Logger;


import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.AccountFilterServiceDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.CustServiceDetReqVO;
import sa.com.mobily.eportal.common.service.vo.CustomerServiceDetReplyVO;
import sa.com.mobily.eportal.common.service.vo.SessionVO;
import sa.com.mobily.eportal.common.service.xml.CustServiceDetailsXMLHandler;

/**
 * @author m.elbastawisi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerInfoDetailsBO {

    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    
	public CustomerServiceDetReplyVO getcustomerServiceDetailsFromBSL(CustServiceDetReqVO custReq) throws MobilyCommonException{
		log.info("CustomerInfoDetailsBO > CustomerServiceDetReplyVO > Called");
		String xmlReply = "";
		CustServiceDetailsXMLHandler xmlhandler = new CustServiceDetailsXMLHandler();
		String xmlMessage = xmlhandler.getcustomerServiceDetailsFromBSL(custReq);
		log.debug("CustomerInfoDetailsBO > CustomerServiceDetReplyVO > xmlMessage >"+xmlMessage);
		//		get DAO object
		MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
		AccountFilterServiceDAO mqAccountFilterServiceDAO = daoFactory.getCustomerServiceDetails();
		xmlReply = mqAccountFilterServiceDAO.getServiceInquiry(xmlMessage);
		log.debug("CustomerInfoDetailsBO > CustomerServiceDetReplyVO > xmlReply >"+xmlReply);
//		xmlReply="<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>CUSTOMER_DETAILS</MsgFormat><MsgVersion>0001</MsgVersion><SecurityKey>asf2123aqws123123</SecurityKey><RequestorChannelId>ePortal</RequestorChannelId><RequestorUserId>corporate2</RequestorUserId><RequestorLanguage>E</RequestorLanguage><ReturnCode>0000</ReturnCode></EE_EAI_HEADER><LineNumber>966540510106</LineNumber><PackageId>1126</PackageId><CustomerType>2</CustomerType><CustomerCategory>0</CustomerCategory><PackageDescription>Business 500</PackageDescription><CreationTime>03082008040714</CreationTime><CorporatePackage>Yes</CorporatePackage><Status>1</Status><SupplementaryServices><SupplementaryService><ServiceName>GPRS30MB</ServiceName><Status>1</Status><SubscriptionType>MONTHLY</SubscriptionType></SupplementaryService><SupplementaryService><ServiceName>ICS</ServiceName><Status>0</Status></SupplementaryService><SupplementaryService><ServiceName>LBS</ServiceName><Status>0</Status></SupplementaryService><SupplementaryService><ServiceName>CUG</ServiceName><Status>0</Status></SupplementaryService><SupplementaryService><ServiceName>ICS2</ServiceName><Status>0</Status><ServiceType>null</ServiceType><PlanType>null</PlanType></SupplementaryService><SupplementaryService><ServiceName>EVMS</ServiceName><Status>0</Status><ServiceType>null</ServiceType><PlanType>null</PlanType></SupplementaryService><SupplementaryService><ServiceName>MMS_BUNDLE</ServiceName><Status>0</Status><ServiceType>null</ServiceType><PlanType>null</PlanType></SupplementaryService></SupplementaryServices></EE_EAI_MESSAGE>";
		    
		
		//parsing the xml reply
		CustomerServiceDetReplyVO replyVO = xmlhandler.parsingcustomerServiceDetails(xmlReply);
		if(!replyVO.getReturnCode().equals(ConstantIfc.CUST_INFO_SUCCESS_CODE))
		{
		    log.fatal("CustomerInfoDetailsBO > CustomerServiceDetReplyVO > ErrorCode ="+replyVO.getReturnCode()+" then trow mobilyException");
			MobilyCommonException e = new MobilyCommonException();
			e.setErrorCode(replyVO.getReturnCode());
			throw e;
		}
		log.debug("CustomerInfoDetailsBO > CustomerServiceDetReplyVO > end");
		return replyVO;
		
	}

	 }
