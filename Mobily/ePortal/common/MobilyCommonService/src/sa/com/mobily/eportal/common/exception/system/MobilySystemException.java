package sa.com.mobily.eportal.common.exception.system;

import sa.com.mobily.eportal.common.exception.MobilyException;


public class MobilySystemException extends MobilyException  {
	
	private static final long serialVersionUID = -5928110500019692780L;

	public MobilySystemException() {
	}

	public MobilySystemException(String message) {
		super(message);
	}

	public MobilySystemException(Throwable cause) {
		super(cause);
	}

	public MobilySystemException(String message, Throwable cause) {
		super(message, cause);
	}
}