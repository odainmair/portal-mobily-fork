/**
 * 
 */
package sa.com.mobily.eportal.customerinfo.xml.idvalidationpromptinq;

import java.util.Date;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.jaxb.idvalidationpromtinq.reply.ReplyMessage;
import sa.com.mobily.eportal.customerinfo.jaxb.idvalidationpromtinq.request.RequestHEADER;
import sa.com.mobily.eportal.customerinfo.jaxb.idvalidationpromtinq.request.RequestMessage;
import sa.com.mobily.eportal.customerinfo.vo.IdValidationPromptReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.IdValidationPromptRequestVO;

/**
 * @author Suresh Vathsavai - MIT
 * Date: Nov 28, 2013
 */
public class IdValidationXMLHandler {

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	private static String className = IdValidationXMLHandler.class.getName();
	
	/**
	 * Description: Generate the Id validation prompt inquiry XML Request
	 * Date: Nov 28, 2013
	 * @param requestVO
	 * @return
	 * <MOBILY_BSL_SR>
			<SR_HEADER>
				<MsgFormat>ValidationPromptInquiry</MsgFormat>
				<MsgVersion>0</MsgVersion>
				<RequestorChannelId>MPAY/PCPM</RequestorChannelId>
				<RequestorChannelFunction>ValidationPromptInquiry </RequestorChannelFunction>
				<RequestorSecurityInfo>Secure</RequestorSecurityInfo>
				<ChannelTransactionId></ChannelTransactionId>
				<SrDate>20091125001911</SrDate>
			</SR_HEADER>
			<MSISDN>966540510251</MSISDN>
		</MOBILY_BSL_SR>
	 */
	public String generateIdValidationPromptXMLRequest(IdValidationPromptRequestVO requestVO) {
		
		String methodName = "generateIdValidationPromptXMLRequest";
		executionContext.startMethod(className, methodName, null);
		if(requestVO == null) {
			executionContext.log(Level.SEVERE, "Invalid RequestVO:["+requestVO+']', className, methodName, null);
			throw new BackEndException(ExceptionConstantIfc.ID_VALIDATION_PROMPT_INQUIRY_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}
		String xmlRequest = null;
		try{
			RequestMessage requestMessage = new RequestMessage();
			RequestHEADER requestHEADER = new RequestHEADER();
			String srDate = FormatterUtility.FormateDate(new Date());
				requestHEADER.setChannelTransactionId("EPOR_"+srDate);
				requestHEADER.setMsgFormat(ConstantsIfc.ID_VALIDATE_MESSAGE_FORMAT_VALUE);
				requestHEADER.setMsgVersion(ConstantsIfc.MSG_VER_0);
				requestHEADER.setRequestorChannelFunction(ConstantsIfc.ID_VALIDATE_MESSAGE_FORMAT_VALUE);
				requestHEADER.setRequestorChannelId(FormatterUtility.isEmpty(requestVO.getChannelId()) ?  ConstantsIfc.REQUESTOR_CHANNEL_ePortal : requestVO.getChannelId());
				requestHEADER.setRequestorSecurityInfo(ConstantsIfc.SECURITY_INFO_VALUE);
				requestHEADER.setSrDate(srDate);
			requestMessage.setRequestHEADER(requestHEADER);
			requestMessage.setMSISDN(requestVO.getMsisdn());
			
			xmlRequest = JAXBUtilities.getInstance().marshal(requestMessage);
			
		}catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception while generateIdValidationPromptXMLRequest["+e.getMessage()+"]", className, methodName, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		executionContext.endMethod(className, methodName, null);
		return xmlRequest;
	}

	/**
	 * Description: Parse Id validation prompt inquiry Reply
	 * Date: Nov 28, 2013
	 * @param strXMLReply
	 * <MOBILY_BSL_SR_REPLY>
		<SR_HEADER_REPLY>
			<MsgFormat>ValidationPromptInquiry</MsgFormat>
			<MsgVersion>0</MsgVersion>
			<RequestorChannelId>ePortal</RequestorChannelId>
			<RequestorChannelFunction>ValidationPromptInquiry</RequestorChannelFunction>
			<RequestorSecurityInfo>Secure</RequestorSecurityInfo>
			<ChannelTransactionId></ChannelTransactionId>
			<SrDate>20091125001911</SrDate>		
			<ErrorCode>0000</ErrorCode>
			<ErrorMsg></ErrorMsg>
		</SR_HEADER_REPLY>
		<Prompt>Y|N</Prompt>
	</MOBILY_BSL_SR_REPLY>
	 * @return
	 */
	public IdValidationPromptReplyVO parseIdValidationPromptXMLReply(String strXMLReply) {
		
		String methodName = "parseIdValidationPromptXMLReply";
		executionContext.startMethod(className, methodName, null);
		IdValidationPromptReplyVO replyVO = new IdValidationPromptReplyVO();
		
		ReplyMessage replyMessage = null;
		
		try{
			if(FormatterUtility.isNotEmpty(strXMLReply)) {
				replyMessage = (ReplyMessage) JAXBUtilities.getInstance().unmarshal(ReplyMessage.class, strXMLReply);
				if(replyMessage != null) {
					if(replyMessage.getSRHEADERREPLY() != null) {
						replyVO.setErrorCode(replyMessage.getSRHEADERREPLY().getErrorCode());
						replyVO.setErrorMessage(replyMessage.getSRHEADERREPLY().getErrorMsg());
					}
					replyVO.setPrompt(replyMessage.getPrompt());
				}else {
					executionContext.log(Level.SEVERE, "Id validation prompt inquiry XML Reply Object is null.", className, methodName, null);
					throw new XMLParserException();
				}
			}else {
				executionContext.log(Level.SEVERE, "Id validation prompt inquiry Reply is null.", className, methodName, null);
				throw new XMLParserException();
			}
		}catch (Exception e) {
			executionContext.log(Level.SEVERE, "Exception while parsing Id validation prompt inquiry Reply:["+e.getMessage()+"]", className, methodName, e);
			throw new XMLParserException(e.getMessage(), e);
		}
		executionContext.endMethod(className, methodName, null);
		return replyVO;
	}

}
