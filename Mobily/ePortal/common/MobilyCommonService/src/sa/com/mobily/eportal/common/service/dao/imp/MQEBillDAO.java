/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.EBillDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;


import com.mobily.exception.mq.MQSendException;

/**
 * @author r.bandi.mit
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class MQEBillDAO implements EBillDAO {
	  private static final Logger log = LoggerInterface.log;

	public String sendToMQWithReply(String xmlRequestMessage)
			throws MobilyCommonException {
		log.debug("EBill > MQEBillDAO > sendToMQWithReply > start");
		String replyMessage = "";
		try {
			/*
			 * Get QueuName
			 */
			// WiMaxRechargeUtil.getPropertyValue(ConstantIfc.WIMAX_RECHARGE_REQUEST_QUEUENAME);
			log.debug("MQEBillDAO > sendToMQWithReply >Request Queue Name ["+ ConstantIfc.EBILL_RECHARGE_REQUEST_QUEUENAME + "]");
			/*
			 * Get ReplyQueuName
			 */
			// String strReplyQueueName =
			// WiMaxRechargeUtil.getPropertyValue(ConstantIfc.WIMAX_RECHARGE_REPLY_QUEUENAME);
			log.debug("MQEBillDAO > sendToMQWithReply >Reply Queue Name ["+ ConstantIfc.EBILL_REPLY_QUEUENAME + "]");
			/*
			 * Get reply Queu manager Name
			 */

			// replyMessage =
			// MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage,
			// strQueueName,strReplyQueueName);
			//xmlRequestMessage =
			// "<MOBILY_EAI_SR><SR_BK_HEADER><FuncId>WIMAX_VOUCHER_RECHARGE</FuncId><RequestorChannelId>EPortal</RequestorChannelId><SrDate>20080305072210</SrDate><ChannelTransId>20080305072210</ChannelTransId
			// ></SR_BK_HEADER><MSISDN></MSISDN><VoucherInfo><AccountNumber>100011802564032</AccountNumber><VoucherNumber>12121212121212</VoucherNumber></VoucherInfo></MOBILY_EAI_SR>";
			log.debug("EBill > MQEBillDAO > sendToMQWithReply >xmlRequestMessage"+ xmlRequestMessage);
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(
					xmlRequestMessage,
					ConstantIfc.EBILL_RECHARGE_REQUEST_QUEUENAME,
					ConstantIfc.EBILL_REPLY_QUEUENAME);
			log.debug("EBill > MQEBillDAO > sendToMQWithReply >replyMessage"
					+ replyMessage);

		} catch (RuntimeException e) {
			log.fatal("EBill > MQEBillDAO > sendToMQWithReply > MQSend Exception :"+ e.getMessage());

			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("EBill >MQEBillDAO > sendToMQWithReply > Exception "
					+ e.getMessage());
			throw new SystemException(e);
		}
		log.debug("EBill > MQEBillDAO > sendToMQWithReply > end");
		return replyMessage;
	}

}