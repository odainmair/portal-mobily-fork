/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.constant;

import org.w3c.dom.Element;
 
/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface TagIfc {

	public static final String TAG_MSG_FORMAT = "MsgFormat";
	public static final String TAG_GET_INTERNET_BUNDLE ="SupplementaryInfo";
	public static final String TAG_PACKAGE_ID = "PackageId";
	public static final String TAG_PACKAGE_DESCRIPTION= "PackageDescription";
	public static final String TAG_CORPORATE_PACKAGE = "CorporatePackage";
	public static final String TAG_SUPPLEMENTARYSERVICES = "SupplementaryServices";
	public static final String TAG_SUPPLEMENTARYSERVICE = "SupplementaryService";
	public static final String TAG_SERVICENAME = "ServiceName";
	
    
    public static final String MAIN_EAI_TAG_NAME = "EE_EAI_MESSAGE";
    public static final String EAI_HEADER_TAG = "EE_EAI_HEADER";
	public static final String MSG_FORMAT = "MsgFormat";
	public static final String MSG_VERSION = "MsgVersion";
	public static final String REQUESTOR_CHANNEL_ID = "RequestorChannelId";
	public static final String REPLIER_CHANNEL_ID = "ReplierChannelId";
	public static final String REQUESTOR_USER_ID = "RequestorUserId";
	public static final String REQUESTOR_LANG = "RequestorLanguage";
	public static final String REQUESTOR_SECURITY_INFO = "RequestorSecurityInfo";
	public static final String RETURN_CODE = "ReturnCode";
	public static final String RETURN_DESC = "ReturnDesc";
	public static final String LINE_NUMBER = "LineNumber";
	public static final String TAG_INCLUDE_ID = "IncludeID";
	public static final String NEW_MSISDN = "NewMSISDN";
	public static final String ORIGINAL_MSISDN = "OriginalMSISDN";	
	public static final String TAG_BACKEND_CHANNEL = "BackendChannel";	
	public static final String IN_VALUE = "GetINValue";
	public static final String PACKAGE_ID = "PackageID";
	public static final String BALANCE = "Balance";
	public static final String EXPIRATION_DATE = "ExpirationDate";
	public static final String BILLED_AMOUNT = "BilledAmount";
	public static final String UNBILLED_AMOUNT = "UnbilledAmount";
	//public static final String UNALOCATED_AMOUNT = "UnallocatedAmount";
	public static final String UNALOCATED_AMOUNT = "UnAllocatedAmount";
	public static final String REQUESTOR_CHANNEL_FUNCTION = "RequestorChannelFunction";
	public static final String USER_NAME = "UserName";
	public static final String TAG_SERVICE_NAME= "ServiceName";
	
	public static final String TAG_SR_DATE = "SrDate";
	public static final String TAG_FUNC_ID = "FuncId";
	
	
	public static final String TAG_CUSTOMER_TYPE_PACKAGE_ID = "PackageId";
	public static final String PACKAGE_DESCRIPTION = "PackageDescription";
	public static final String CUSTOMER_TYPE = "CustomerType";
	public static final String TAG_CREATION_TIME = "CreationTime";
	
	public static final String TAG_PACKAGE_CATEGORY = "PackageCategory";
	public static final String TAG_ISMIX = "IsMix";
	public static final String TAG_ISNEW_CONTROL = "IsNewControl";
	public static final String TAG_ISCORPORATE_KA = "IsCorporateKA";
	public static final String TAG_CUSTOMERBSL_ID = "CustomerBSLId";
	public static final String TAG_MNPNUMBER = "MNPNumber";
	public static final String TAG_CUSTOMER_ID = "CustomerId";
	public static final String TAG_BILLING_ID = "BillingId";
	public static final String TAG_SERVICE_ID = "ServiceId";
	public static final String TAG_DEAL_ID = "DealId";
	public static final String TAG_ISSMSIM = "ISMSIM";
	public static final String TAG_IS_IUC = "IS_IUC";
	
	//public static final String TAG_CPE_MAC_ADDRESS = "CPEMACAddress";
	public static final String TAG_IDTYPE = "IDType";
	public static final String TAG_IDNUMBER ="IDNumber";
	
	
	
	public static final String TAG_MOBILY_BSL_SR_REPLY = "MOBILY_BSL_SR_REPLY";
	public static final String TAG_SR_HEADER_REPLY = "SR_HEADER_REPLY";
	public static final String TAG_SR_HEADER = "SR_HEADER";
	public static final String TAG_MOBILY_BSL_SR = "MOBILY_BSL_SR";
	public static final String TAG_SECURITY_KEY = "SecurityKey";
	
	public static final String TAG_CHARGEABLE = "Chargeable";
	public static final String TAG_CHARGE_AMOUNT = "ChargeAmount";
	public static final String TAG_CHARGING_MODE = "ChargingMode";
	public static final String CC_STATUS_ONLY = "CCStatusOnly";
	
	public static final String TAG_SR_RCV_DATE = "SrRcvDate";
	public static final String TAG_SR_STATUS = "SrStatus";
	public static final String TAG_STATUS = "Status";
	public static final String TAG_REASON = "Reason";
	public static final String TAG_ERR_CODE = "ErrCode";
	public static final String TAG_ERROR_CODE = "ErrorCode";
	public static final String TAG_ERROR_MSG = "ErrorMsg";
	public static final String  TAG_MSISDN = "MSISDN" ;
	public static final String  TAG_OVERWRITE_OPEN_ORDER = "OverwriteOpenOrder" ;
	public static final String TAG_GENDER = "Gender";
	public static final String  TAG_CHANNEL_TRANS_ID = "ChannelTransId" ;
	
	public static final String TAG_ACCOUNT_NUMBER = "AccountNumber" ;
	//public static final String TAG_NAME = "Name" ;
	
	public static final String TAG_RETURN_CODE = "ReturnCode";
	//supplementary
	public static final String MOBILY_PROMOTION_ID = "PromoID";
	public static final String MOBILY_PROMOTION_ID_INQUIRY = "PromotionId";
	public static final String TAG_PROMOTION_SUBSCRIBED_LIST= "AlreadySubscribedList";
	public static final String TAG_PROMOTION_NOT_SUBSCRIBED_LIST= "NotSubscribedList";
	public static final String MOBILY_PROMOTION_SIM_TYPE = "SimType";
	public static final String MOBILY_PROMOTION_VANITY = "Vanity";
	public static final String MOBILY_PROMOTION_ISACKNOWLEDGABLE = "IsAcknowledgable";
	public static final String MOBILY_PROMOTION_EPLY_TO_QUEUE_NAME = "ReplyToQueueName";
	public static final String MOBILY_PROMOTION_EPLY_TO_QUEUE_MANAGER_NAME = "ReplyToQueueManagerName";
	public static final String MOBILY_PROMOTION_ACTIVATION_TIME = "ActivtionTime";
	public static final String MOBILY_PROMOTION_PRICING_PLAN= "Pricing_Plan";
	public static final String MOBILY_PROMOTION_RETURN_CODES= "ReturnCodes";
	public static final String MOBILY_PROMOTION_BACK_END= "BackendChannel";
	public static final String MOBILY_PROMOTIONS= "Promotions";
	public static final String  TAG_SUPPLEMENTARY_SERVICES = "SupplementaryServices" ;
	public static final String TAG_GPRS_STATUS= "GprsStatus";
	public static final String TAG_RBT_STATUS= "RbtStatus";
	public static final String TAG_VIDEO_TEL_STATUS= "VideoTelStatus";
	public static final String TAG_VIDEO_STREAM_STATUS= "VideoStrmStatus";
	public static final String TAG_GAMING_STATUS= "GamingStatus";
	public static final String TAG_INTERNATIONAL_STATUS= "BarringStatus";
	public static final String TAG_LIST_MOBILY_PROMOTION= "ListMobilyPromotion";
	public static final String TAG_PTT_STATUS= "PTTStatus";
	public static final String TAG_TV_STATUS= "TVStatus";
	public static final String TAG_SMS_STATUS= "SMSStatus";
	public static final String TAG_SMS_TYPE= "ServiceType";
	public static final String TAG_SMS_BUNDLE= "SMSBundle";
	public static final String TAG_HOME_ZONE_STATUS= "HomeZoneStatus";
	public static final String TAG_LBS_STATUS= "LBSStatus";
	public static final String TAG_VSMS_STATUS= "VSMSStatus";
	public static final String TAG_REQ_SECURITY_INFO = "RequestorSecurityInfo";
	public static final String TAG_MSG_OPTION = "MsgOption";
	public static final String TAG_INTERNATIONAL_FAVORITE_NUMBER = "InternationalFavouriteNumber";
	public static final String TAG_NATIONAL_FAVORITE_NUMBER = "NationalFavouriteNumber";
	public static final String TAG_COUNTRY_FAVORITE_NUMBER = "FavouriteCountry";
	public static final String TAG_FAVORITE_MCC = "FavouriteMCC";
//	Customer Profile
	//	Request and Reply Tags
	
	public static final String TAG_MOBILY_BSL_MESSAGE = "MOBILY_BSL_MESSAGE";
	public static final String TAG_MOBILY_BSL_HEADER = "BSL_HEADER";
	public static final String TAG_SUPP_SERVICES = "SuppServices" ;
	
	public static final String TAG_MOBILY_EPORTAL_SR="MOBILY_EPORTAL_SR";
	public static final String TAG_MOBILY_EPORTAL_SR_REPLY ="MOBILY_EPORTAL_SR_REPLY";
	
	
	public static final String TAG_MSISDN_LIST = "MSISDN_LIST";
	
	public static final String TAG_SECURITY_INFO = "RequestorSecurityInfo";
	public static final String TAG_MSG_VERSION = "MsgVersion";
	public static final String TAG_REQUESTOR_CHANNEL_ID = "RequestorChannelId";
	
	
	public static final String TAG_FULL_INFO = "FullInfo";
	public static final String TAG_LINE_NUMBER = "LineNumber";
	public static final String TAG_ID_NUMBER = "IdNumber";
	public static final String TAG_ID_DOC_TYPE = "IDDocType";
	public static final String TAG_ACCOUNT_INFO = "AccountInfo";
	public static final String TAG_TITLE = "Title";
	public static final String TAG_FULL_NAME = "FullName";
	public static final String TAG_FIRST_NAME = "FirstName";
	public static final String TAG_MIDDLE_NAME = "MiddleName";
	public static final String TAG_LAST_NAME = "LastName";
	public static final String TAG_CUSTOMER_CATEGORY = "CustomerCategory";
	public static final String TAG_CUSTOMER_TYPE = "CustomerType";
	public static final String TAG_BIRTH_DATE = "BirthDate";
	public static final String TAG_BIRTH_DATE_FORMAT = "BirthDateFormat";
	
	public static final String TAG_NATIONALITY = "Nationality";
	public static final String TAG_OCCUPATION = "Occupation";
	public static final String TAG_BILLING_LANGUAGE = "BillingLanguage";
	public static final String TAG_ID = "ID";
	public static final String TAG_CONTACT = "Contact";
	public static final String TAG_CONTACT_PREFERENCE = "ContactPreference";
	public static final String TAG_STREET_ADDRESS = "StreetAddress";
	public static final String TAG_PO_BOX = "PoBox";
	public static final String TAG_MOBILE = "Mobile";
	public static final String TAG_PHONE = "Phone";
	public static final String TAG_FAX = "Fax";
	public static final String TAG_EMAIL = "Email";
	public static final String TAG_SERVICE_LINE = "ServiceLine";
	public static final String TAG_BILLING_NUMBER = "BillingNumber";
	public static final String TAG_PLAN = "Plan";
	public static final String TAG_PACKAGE = "Package";
	public static final String TAG_REGISTRATION = "Registeration";
	public static final String TAG_CREATED_BY = "CreatedBy";
	public static final String TAG_CREATED_DATE = "CreatedDate";
	


	public static final String TAG_REQUESTOR_USER_ID = "RequestorUserId";
	public static final String TAG_REQUESTOR_LANGUAGE = "RequestorLanguage";
	public static final String TAG_OVER_WRITE_OPEN_ORDER_TAG = "OverwriteOpenOrder";
	public static final String TAG_CONTENT_PROVIDER_Chargeable = "Chargeable";
	public static final String CHARGE_AMOUNT = "ChargeAmount";
	public static final String TAG_SUPPLEMENTARY_SERVICE = "SupplementaryService";
	public static final String TAG_OPERATION = "Operation";
	public static final String TAG_REPLY_TO_REQUESTER = "ReplyToRequester";
	public static final String TAG_CONTENT_PROVIDER_CREATION_TIME = "CreationTime";
	public static final String TAG_CONTENT_PROVIDER = "ContentProvider";
	public static final String TAG_CONTENT_PROVIDER_SERVICE_CODE = "ServiceCode";
	public static final String TAG_CONTENT_PROVIDER_ACTION_NAME = "ActionName";
	public static final String TAG_CONTENT_PROVIDER_PERIOD = "period";
	public static final String TAG_CONTENT_PROVIDER_FEE = "Fee";
	public static final String TAG_CONTENT_PROVIDER_NAME = "ContentProviderName";
	public static final String TAG_CONTENT_PROVIDER_LIST = "ListOfContentProviders";
	public static final String TAG_CONTENT_PROVIDER_CODE = "ContentProviderCode";
	public static final String TAG_CONTENT_PROVIDER_SUBSCRIPTION_FEE = "Fee";
	public static final String TAG_CONTENT_PROVIDER_Expiration_TIME = "ExpirationTime";
	public static final String TAG_CONTENT_PROVIDER_IS_ARABIC_COMMAND ="IsArabicCommand";
	public static final String TAG_CONTENT_PROVIDER_STATUS ="Status";

	
	//Added for customer info detail
	public static final int SKY_EMAIL_STATUS_ACTIVE               = 1;
	public static final int SKY_EMAIL_STATUS_GRACE                = 2;
	public static final int SKY_SECONDARY_EMAILS_ACTION_REASON_NO_REASON = 0;
	
	public static final int SKY_EMAIL_STATUS_UNDEFINED            = -1;
	public static final int SKY_EMAIL_STATUS_NEVER_CREATED_BEFORE = 0;
	public static final int SKY_EMAIL_STATUS_SUSPENDED            = 3;
	public static final int SKY_EMAIL_STATUS_DELETED              = 4;
	public static final int SKY_EMAIL_STATUS_SCHEDULED_TO_CREATE  = 5;
	
	public static final int SKY_TYPE_UNDEFINED                 = -1;
	public static final int SKY_TYPE_NO_INTERNET_BUNDLE        = 0;
	public static final int SKY_TYPE_INTERNET_BUNDLE_5M        = 1;
	public static final int SKY_TYPE_INTERNET_BUNDLE_20M       = 2;
	public static final int SKY_TYPE_INTERNET_BUNDLE_UNLIMITED = 3;
	public static final int SKY_TYPE_INTERNET_BUNDLE_1G        = 4;
	public static final int SKY_TYPE_INTERNET_BUNDLE_5G        = 5;
	public static final int SKY_TYPE_INTERNET_BUNDLE_30M       = 6;
	public static final int SKY_TYPE_INTERNET_BUNDLE_100M      = 7;
	public static final int SKY_TYPE_MOBILY_CONNECT            = 8;
	public static final String GPRS_OPERATION_5M = "5MB";
	public static final String GPRS_OPERATION_20M = "20MB";
	public static final String GPRS_OPERATION_30M = "30MB";
	public static final String GPRS_OPERATION_100M = "100MB";
	public static final String GPRS_OPERATION_UNLIMITED = "Unlimited";
	public static final String GPRS_OPERATION_1G = "1GB";
	public static final String GPRS_OPERATION_5G = "5GB";
	


	public static final String TAG_FAVORITE_NUMBER_DESCRIPTION = "FavoriteDesc";
	public static final String TAG_FAVORITE_NUMBER_TYPE = "Type";
	
	
	//Ebill incentive
	
	public static final int LINE_TYPE_POSTPAID = 0;
	public static final String TAG_EI_MOBILY_BSL_SR = "MOBILY_BSL_SR";
	public static final String TAG_EI_SR_HEADER = "SR_HEADER";
	public static final String TAG_EI_FuncId = "FuncId";
	public static final String TAG_EI_SecurityKey = "SecurityKey";
	public static final String TAG_EI_ServiceRequestId = "ServiceRequestId";
	public static final String TAG_EI_MsgVersion = "MsgVersion";
	public static final String TAG_EI_RequestorChannelId = "RequestorChannelId";
	public static final String TAG_EI_SrDate = "SrDate";
	public static final String TAG_EI_SrRcvDate = "SrRcvDate";
	public static final String TAG_EI_SrStatus = "SrStatus";
	public static final String TAG_EI_RequestorUserId = "RequestorUserId";
	public static final String TAG_EI_RequestorLanguage = "RequestorLanguage";
	public static final String TAG_EI_LineNumber = "LineNumber";
	public static final String TAG_EI_OverwriteOpenOrder = "OverwriteOpenOrder";
	public static final String TAG_EI_Chargeable = "Chargeable";
	public static final String TAG_EI_ChargingMode = "ChargingMode";
	public static final String TAG_EI_ChargeAmount = "ChargeAmount";
	public static final String TAG_EI_DealerId = "DealerId";
	public static final String TAG_EI_ShopId = "ShopId";
	public static final String TAG_EI_AgentId = "AgentId";
	public static final String TAG_EI_Operation = "Operation";
	public static final String TAG_EI_ChannelTransId = "ChannelTransId";
	public static final String TAG_EI_MSISDN = "MSISDN";
	public static final String TAG_EI_InvoiceMechanism = "InvoiceMechanism";
	public static final String TAG_EI_Email = "Email";
	
	public static final String TAG_EI_MOBILY_BSL_SR_REPLY = "MOBILY_BSL_SR_REPLY";
	public static final String TAG_EI_SR_HEADER_REPLY = "SR_HEADER_REPLY";
	public static final String TAG_EI_ErrorMsg ="ErrorMsg";
	public static final String TAG_EI_ErrorMessage = "ErrorMessage";
	public static final String TAG_EI_ErrorCode = "ErrorCode";
	public static final String TAG_EI_BackendChannel = "BackendChannel";
	
	public static final String TAG_EI_ActivationCode = "ActivationCode";
	public static final String EI_Inquiry_FuncId = "Email_Invoice_Inquiry";
	
	public static final String EI_Inquiry_MsgVersion = "0000";
	public static final String EI_Inquiry_RequestorChannelId = "eportal";
	public static final String EI_Inquiry_RequestorLanguage = "E";
	public static final String DATE_FORMAT = "yyyyMMddHHmmss";
	
//	 Sky Email
	public static final String TAG_SKY_EMAIL_REQUEST = "SKY_EMAIL_REQUEST";
	public static final String TAG_SKY_EMAIL_REPLY = "SKY_EMAIL_REPLY";
	public static final String TAG_SKY_HEADER = "SKY_HEADER";
	public static final String TAG_SKY_FUNC_ID = "FUNC_ID";
	public static final String TAG_SKY_CHANNEL_ID= "CHANNEL_ID";
	public static final String TAG_SKY_REQUEST_DATE = "REQUEST_DATE";
	public static final String TAG_SKY_EMAIL = "EMAIL";
	public static final String TAG_SKY_GIVEN_NAME = "GIVEN_NAME";
	public static final String TAG_SKY_FAMILY_NAME = "FAMILY_NAME";
	public static final String TAG_SKY_PASSWORD = "PASSWORD";
	public static final String TAG_SKY_ERROR_CODE = "ERROR_CODE";
	
	// Panda
	public static final String TAG_DEALER_ID = "DealerId";
	public static final String TAG_SHOP_ID = "ShopId";
	public static final String TAG_AGENT_ID = "AgentId";
	public static final String TAG_ROOT_ACCOUNT_NUMBER = "RootAccountNumber";
	public static final String TAG_DISTRIBUTION_SCHEMA = "DistributionSchema";
	public static final String TAG_DISTRIBUTION_MINUTES = "Minutes";
	public static final String TAG_SR_INFO = "SrInfo";
	public static final String TAG_BALANCE_MINUTES = "BalanceMinutes";
	public static final String TAG_TOTAL_MINUTES = "TotalContractMinutes";
	public static final String TAG_CONTRACT_END_DATE = "ContractEndDate";	
	public static final String TAG_REPLY_QUEUE_MANAGER = "ReplyQueueMgr";
	public static final String TAG_REPLY_QUEUE = "ReplyQueue";
	public static final String TAG_FROM_DATE = "FromDt";
	public static final String TAG_TO_DATE = "ToDt";
	public static final String TAG_DISTRIBUTED_MINUTES = "DistributedMints";
	public static final String TAG_CURRENT_BALANCE_MINUTES = "CurrentBalMints";
	
	//Family And Friends
	public static final String TAG_MSISDN_CHILD = "MSISDN_CHILD";
	public static final String TAG_COMMAND = "Command";
	public static final String TAG_NUMBER_LIST = "NumbersList";
	public static final String TAG_NUMBER = "Number";
	
	//Credit Card
	public static final String TAG_IsCorpAP = "IsCorpAP";
	public static final String TAG_TransactionID = "TransactionID";
	public static final String TAG_TransactionDate = "TransactionDate";
	public static final String TAG_MerchantID = "MerchantID";
	public static final String TAG_MerchantName = "MerchantName";
	public static final String TAG_PaymentDateTime = "PaymentDateTime";
	public static final String TAG_Amount = "Amount";
	public static final String TAG_CreditCardDetails = "CreditCardDetails";
	public static final String TAG_CrediCardID = "ID";
	public static final String TAG_CreditCardNumber = "CreditCardNumber";
	public static final String TAG_CreditCardType = "CreditCardType";
	public static final String TAG_CreditCardStatus = "CreditCardStatus";
	public static final String TAG_Name = "Name";
	public static final String TAG_BILLING_ADDRESS = "Address";
	public static final String TAG_CustomerType = "CustomerType";
	public static final String TAG_AccountNumber = "AccountNumber";
	public static final String TAG_PrimeMSISDN = "PrimeMSISDN";
	public static final String TAG_NewPrimeMSISDN = "NewPrimeMSISDN";
	public static final String TAG_TransactionPIN = "TransactionPIN";
	public static final String TAG_NewTransactionPIN = "NewTransactionPIN";
	public static final String TAG_CreditCardMSISDN = "MSISDN";
	public static final String TAG_ExpiryDate = "ExpiryDate";
	public static final String TAG_CVV = "CVV";
	public static final String TAG_RequestorIpAddress = "RequestorIpAddress";
	public static final String TAG_SMS_TO = "SMS_TO";

	//For PACKAHER Conversion XMLHandler
	public static final String TAG_CHANGE_TYPE = "ChangeType";
	public static final String TAG_SR_BK_HEADER_REPLY = "SR_BK_HEADER_Reply";
	
	
	// corporate revamp strategic track related: start
	public static final String TAG_SR_BK_HEADER = "SR_BK_HEADER";
	public static final String TAG_SERVICE_REQUEST_ID = "ServiceRequestId";
	public static final String TAG_LIST_OF_BILLING_ACCOUNT = "ListOfBillingAccount";
	public static final String TAG_BILLING_ACCOUNT = "BillingAccount";
	public static final String TAG_BILLS_INFO = "BillInfo";
	
	public static final String TAG_BILLS = "Bills";
	public static final String TAG_BILL = "Bill";
	public static final String TAG_BILL_NUMBER = "BillNumber";
	public static final String TAG_BILL_END_DATE = "EndDate";
	public static final String TAG_PRODUCT_TYPE = "ProductType";
	public static final String TAG_NETWORK_CONFIGURATION = "NetworkConfiguration";
	public static final String TAG_KAM = "KAM";
	
	public static final String TAG_LIST_OF_SERVICE_SUB_ACCOUNTS = "ListOfServiceSubAccounts";
	public static final String TAG_CUT_SERVICE_SUB_ACCOUNTS = "CutServiceSubAccounts";
	public static final String TAG_VPN_ID = "VPNID";
	public static final String TAG_PACKAGE_NAME = "PackageName";
	public static final String TAG_NAME = "Name";
	public static final String TAG_ALIAS = "Alias";
	public static final String TAG_ITEMS = "Items";
	public static final String TAG_ITEM = "Item";
	public static final String TAG_ITEM_NAME = "ItemName";
	public static final String TAG_ATTRIBUTES = "Attributes";
	public static final String TAG_ATTRIBUTE = "Attribute";
	public static final String TAG_TEXT_VALUE = "TextValue";
	public static final String TAG_ALIAS_NAME = "AliasName";
	//public static final String TAG_STATUS = "Status";
	//corporate revamp strategic track related: end
	
	//IPhone Request XML tags
		public static final String TAG_START_IPHONE_REQ = "MOBILY_IPHONE_REQUEST";
		public static final String TAG_USER_ID = "UserName";
		public static final String TAG_PASSWORD = "Password";
		public static final String TAG_HASH_CODE = "HashCode";
		public static final String TAG_LINE_TYPE = "LineType";
		public static final String TAG_FUNCTIONID = "FunctionId";
		public static final String TAG_VERSION_ID = "VersionId";
		public static final String TAG_TIME_STAMP = "TimeStamp";
		public static final String TAG_REQUESTOR_LANG = "RequestorLanguage";
		public static final String TAG_ACTION = "Action";
		public static final String TAG_SERVICE_TYPE = "ServiceType";
		public static final String TAG_PLAN_TYPE = "PlanType";
		public static final String TAG_ITEM_CODE = "ItemCode";
		public static final String TAG_X_AXIS = "X-Axis";
		public static final String TAG_Y_AXIS = "Y-Axis";
		public static final String TAG_PROBLEM_ID = "ProblemID";
		public static final String TAG_NEWS_ID = "NewsID";
		public static final String TAG_PROMOTION_ID = "PromotionId";
		public static final String TAG_TYPE = "Type";
		public static final String TAG_DEVICE = "device";
		public static final String TAG_RECIPIENT_MSISDN = "RecipientMSISDN";
		public static final String TAG_TRANSFER_AMOUNT = "TransferredAmount";
		public static final String TAG_CONFIMATION_KEY = "ConfirmationKey";
		public static final String TAG_RANGE = "Range";
		public static final String TAG_FRIEND_MSISDN = "FRIEND_MSISDN";
		public static final String TAG_SR_ID = "SR_ID";
		public static final String TAG_PARTNERS = "Partners";
		public static final String TAG_PARTNER = "Partner";
		public static final String TAG_POIS = "POIs";
		public static final String TAG_POI = "POI";
		public static final String TAG_REPLY = "REPLY";
		public static final String TAG_CHANNEL_ID = "CHANNEL_ID";
		public static final String TAG_CONTENT_ID = "CONTENT_ID";
		public static final String TAG_PIN_CODE = "PIN_CODE";
		public static final String TAG_CONTENT_COMMAND = "COMMAND";
		public static final String TAG_CATEGORIES ="Categories";
		public static final String TAG_CATEGORY ="Category";
		public static final String TAG_CATEGORY_ID ="Category_Id";
		public static final String TAG_SEARCH_BY ="SearchBy";
		public static final String TAG_SEARCH_CRITERIA ="SearchCriteria";
		public static final String TAG_START_ROW ="Start_Row";
		public static final String TAG_END_ROW ="End_Row";
		public static final String TAG_TONES ="Tones";
		public static final String TAG_TONE ="Tone";
		public static final String TAG_TONE_ID ="Tone_Id";
		public static final String TAG_TONE_CODE ="Tone_Code";
		public static final String TAG_TONE_NAME ="Tone_Name";
		public static final String TAG_TONE_PRICE ="Tone_Price";
		public static final String TAG_SINGER_NAME ="Singer_Name";
		public static final String TAG_TONE_URL ="Tone_Url";
		public static final String TAG_TONE_PERSONAL_ID ="Tone_Personal_Id";
		public static final String TAG_ASSIGNED_INFO ="Assigned_Info";
		public static final String TAG_ASSIGNED_RULE ="Assigned_Rule";
		public static final String TAG_SETTING_ID ="Tone_Setting_Id";
		public static final String TAG_CONTACT_NUMBER ="Contact_Number";
		public static final String TAG_CONTACT_NAME ="Contact_Name";
		public static final String TAG_TONE_SETTING_ID ="Tone_Setting_Id";
		public static final String TAG_CONTACTS ="Contacts";
		public static final String TAG_INFO_REQUIRED ="InfoRequired";
		
		
	//IPhone Reply XML tags
		public static final String TAG_START_IPHONE_REP = "MOBILY_IPHONE_REPLY";
		public static final String TAG_ERROR = "ERROR";
		
		// Added for RoyalGuard
		public static final String TAG_FETCHTYPE = "FETCHTYPE";
		public static final String TAG_INFO = "INFO";
		public static final String TAG_SUBSCRIBER_LIST = "SubscriberList";
		public static final String TAG_SUBSCRIBER = "Subscriber";
		public static final String TAG_USER_NAME = "UserName";
		public static final String TAG_COUNT = "COUNT";
		
		public static final String TAG_CURRENT_BALANCE = "CurrentBalance";
		
		public static final String WIMAX_UNALOCATED_AMOUNT = "UnAllocated";
		
	//Added for ConnectOTS
		public static final String TAG_ID_TYPE = "IdType";
		public static final String TAG_LOGIN_ID = "LoginID";
		public static final String TAG_SERIAL_NUMBER = "SerialNumber";
		public static final String TAG_MOBILE_NUMBER = "MobileNumber";
		public static final String TAG_IP = "IP";
		public static final String TAG_CPE_SERIAL_NUMBER = "CPESerialNumber";
		public static final String TAG_VOUCHER_NUMBER = "VoucherNumber";
		public static final String TAG_CR_REQUEST_MESSAGE = "CR_REQUEST_MESSAGE";
		public static final String TAG_CR_HEADER = "CR_HEADER";
		public static final String TAG_INQUIRY_TYPE = "InquiryType";
		public static final String TAG_PARAMS = "Params";
		public static final String TAG_PARAM = "Param";
		public static final String TAG_KEY = "Key";
		public static final String TAG_VALUE = "Value";
		public static final String TAG_RESULTS = "Results";
		public static final String TAG_RESULT = "Result";
		public static final String TAG_CR_REPLY_MESSAGE = "CR_REPLY_MESSAGE";
		public static final String TAG_REQUESTOR_SR_ID = "RequestorSrId";
		public static final String TAG_PRICING_PLAN = "PricingPlan";
	//Added for MCR related numbers
	
	// Added for User Names and Service Control
		public static final String TAG_RG_EMAIL = "email";
	//For FTTH
		public static final String TAG_CPE_SERIAL_NO = "CPESerialNo";
		public static final String TAG_MAX_UPLOAD_RATE = "MAXUploadRate";
		public static final String TAG_MAX_DOWNLOAD_RATE = "MAXDownloadRate";
		public static final String TAG_DURATION = "Duration";
		public static final String TAG_CUST_CATEGORY = "CustomerCategory";
		
		public static final String TAG_CPE_MAC_ADDRESS = "CPEMACAddress";
	// Added for coverage check
		public static final String TAG_WCRG_MOBILY_BSL_SR = "MOBILY_BSL_SR";
		public static final String TAG_WCRG_SR_HEADER = "SR_HEADER";
		public static final String TAG_WCRGM_MESSAGE_FORAMT= "MsgFormat";
		public static final String TAG_WCRGM_MESSAGE_VERSION = "MsgVersion";
		public static final String TAG_WCRGM_REQUETOR_CHANNEL_ID = "RequestorChannelId";
		public static final String TAG_WCRGM_REQUETOR_CHANNEL_FUNCTION ="RequestorChannelFunction";
		public static final String TAG_WCRGM_REQUETOR_SECURITY_INFO ="RequestorSecurityInfo";
		public static final String TAG_WCRGM_CHANNEL_TRANSACTION_ID= "ChannelTransactionId";
		public static final String TAG_WCRGM_SR_DATE = "SrDate";
		public static final String TAG_WCRGM_SR_INFO ="Sr_Info";
		public static final String TAG_WCRGM__LOCATION ="Location";
		public static final String TAG_WCRGM_LONGITUDE = "LongitudeX";
		public static final String TAG_WCRGM_LATITUDE= "LatitudeY";
		public static final String TAG_WCRGM_TYPE="Type";
		public static final String TAG_WCRGM_LAYER= "Layer";
		public static final String TAG_WCRGM_MODE="Mode";
		
		public static final String TAG_WCRG_MOBILY_BSL_SR_REPLY="MOBILY_BSL_SR_REPLY";
		public static final String TAG_WCRG_SR_HEADER_REPLY ="SR_HEADER_REPLY";
		public static final String TAG_WCRG_ERROR_CODE ="ErrorCode";
		public static final String TAG_WCRG_ERROR_MESSAGE ="ErrorMsg";
		public static final String TAG_WCRG_COVERAGE ="Covarage";
		public static final String TAG_WCRG_ODBNUMBER ="ODBNumber";
		public static final String TAG_INCLUDE_CUSTOMERCATEGORY="IncludeCustomerCategory";
		
		public static final String TAG_MOBILY_AUTH_AND_GO_REQUEST ="MOBILY_AUTH_AND_GO_REQUEST";
		public static final String TAG_MOBILY_AUTH_AND_GO_REPLY ="MOBILY_AUTH_AND_GO_REPLY";
		public static final String TAG_SOURCE_MSISDN ="SourceMSISDN";
		public static final String TAG_TARGET_MSISDN ="TargetMSISDN";
		public static final String TAG_PINCODE ="PinCode";
		public static final String TAG_FWD_OPTION ="ForwardOption";
		public static final String TAG_COVERAGE_RFS ="RFS";
		
		public static final String TAG_CORPORATE_ACCOUNT_NUMBER = "CorporateAccountNumber";
		public static final String TAG_TotalBilledAmount = "TotalBilledAmount";
		
		// Added for MDM Migration
		public static final String TAG_TradingName = "TradingName";
		public static final String TAG_Id = "Id";
		public static final String TAG_PartyIdentification = "PartyIdentification";
		public static final String TAG_Party = "Party";
		public static final String TAG_IBM_CUSTOMER = "ibm:customer";
		public static final String TAG_CrossReferenceIds = "CrossReferenceIds";
		public static final String TAG_PartyExtensions = "PartyExtensions";
		public static final String TAG_Customer = "Customer";
		public static final String TAG_IBM_CUSTOMERCONFIGURATION = "ibm:customerConfiguration";
		public static final String TAG_AddressLineOne = "AddressLineOne";
		public static final String TAG_AddressLineTwo = "AddressLineTwo";
		public static final String TAG_AddressLineThree = "AddressLineThree";
		public static final String TAG_StateOrProvince = "StateOrProvince";
		public static final String TAG_Country = "Country";
		public static final String TAG_Postcode = "Postcode";
		public static final String TAG_POBox = "POBox";
		public static final String TAG_GivenName = "GivenName";
		public static final String TAG_MiddleName = "MiddleName";
		public static final String TAG_ThirdName = "ThirdName";
		public static final String TAG_FourthName = "FourthName";
		public static final String TAG_FamilyName = "FamilyName";
		public static final String TAG_LegalName = "LegalName";
		public static final String TAG_Gender = "Gender";
		public static final String TAG_Nationality = "Nationality";
		public static final String TAG_EmailAddress = "EmailAddress";
		public static final String TAG_Number = "Number";
		public static final String TAG_Type = "Type";
		public static final String TAG_Specification = "Specification";
		public static final String TAG_Fax = "Fax";
		public static final String TAG_Mobile = "Mobile";
		public static final String TAG_Phone = "Phone";
		public static final String TAG_DateOfBirth = "DateOfBirth";
		public static final String TAG_Value = "Value";
		public static final String TAG_PeferredContactFlag = "PeferredContactFlag";
		public static final String TAG_KeyAccountManagerPF = "KeyAccountManagerPF";
		public static final String TAG_KeyAccountManagerEmail = "KeyAccountManagerEmail";
		public static final String TAG_IdType = "IdType";
		public static final String TAG_VALUE_1 = "1";
		public static final String TAG_Category = "Category";
		public static final String TAG_Employees = "Employees";
		public static final String TAG_NumberofOffices = "NumberofOffices";
		
		public static final String TAG_Body = "Body";
		public static final String TAG_CharacteristicValue = "CharacteristicValue";
		public static final String TAG_Characteristic = "Characteristic";
		
		public static final String TAG_BillingAddress = "BillingAddress";
		public static final String TAG_PostalContact = "PostalContact";
		public static final String TAG_ContactMedium = "ContactMedium";
		
		public static final String TAG_EmailContact = "EmailContact";
		public static final String TAG_ContactPerson = "ContactPerson";
		public static final String TAG_TelephoneContact = "TelephoneContact";
		
		//Loyalty related
		public static final String TAG_MOBILY_LOYALTY_SR = "MOBILY_LOYALTY_SR";
		public static final String TAG_SECURITYKEY ="SecurityKey";
		public static final String TAG_MOBILY_LOYALTY_SR_REPLY = "MOBILY_LOYALTY_SR_REPLY";
		
		public static final String TAG_LOVInqRq = "LOVInqRq";
		public static final String TAG_xmlns = "xmlns";
		public static final String TAG_EJADA_URL = "http://www.ejada.com";
		public static final String TAG_MsgRqHdr = "MsgRqHdr";
		public static final String TAG_RqUID = "RqUID";
		public static final String TAG_SCId = "SCId";
		public static final String TAG_FuncId_50010000 = "50010000";
		public static final String TAG_RqMode = "RqMode";
		public static final String TAG_LOVType = "LOVType";
		public static final String TAG_LOVInqRs = "LOVInqRs";
		public static final String TAG_MsgRsHdr = "MsgRsHdr";
		public static final String TAG_StatusCode = "StatusCode";
		public static final String TAG_SentRecs = "SentRecs";
		public static final String TAG_LOVList = "LOVList";
		public static final String TAG_LOVInfo = "LOVInfo";
		public static final String TAG_RecTypeCode = "RecTypeCode";
		public static final String TAG_RecDesc = "RecDesc";
		public static final String TAG_Attribute1 ="Attribute1";
		public static final String TAG_EJD = "ejd";
		public static final String TAG_queryCustomerRq = "queryCustomerRq";
		public static final String TAG_mtosi_xsd = "mtosi_xsd";
		public static final String TAG_IBM_SCHEMA_URL1 = "http://www.ibm.com/telecom/common/schema/mtosi/v3_0";
		public static final String TAG_ibm = "ibm";
		public static final String TAG_IBM_SCHEMA_URL2 = "http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/CustomerMessage";
		public static final String TAG_SCHEMA_URL = "http://www.w3.org/2001/XMLSchema-instance";
		public static final String TAG_xsi = "xsi";
		public static final String TAG_FuncId_40210000 = "40210000";
		public static final String TAG_PreferredAddressFlag = "PreferredAddressFlag";
		public static final String TAG_SolicitationIndicatorFlag = "SolicitationIndicatorFlag";
		public static final String TAG_ReplyToQ = "ReplyToQ";
		public static final String TAG_ReplyToQ_MNGR = "ReplyToQmgr";
		public static final String TAG_SETTING_MODE = "SettingMode";
		public static final String TAG_DIVERT_TO = "DivertTo";
		public static final String TAG_PREFERRED_LANG = "PreferredLanguage";
		public static final String TAG_PHONE_NUMBER = "PhoneNumber";		
		public static final String TAG_OTHER_PHONE_NUMNER = "OtherPhoneNumber";
		public static final String TAG_CUST_TYPE = "Customer_Type";
		public static final String TAG_INQ_TYPE = "Inquiry_Type";
		public static final String TAG_SUBJECT = "Subject";
		public static final String TAG_MESSAGE = "Message";
		

		public static final String TAG_CHANNEL_TRANSACTION_ID= "ChannelTransactionId";
		public static final String TAG_FLAG= "Flag";
		
		public static final String TAG_SIEBEL_XML_EXT_QUERY_RET= "siebel-xmlext-query-ret";
		public static final String TAG_ROW= "row";
		public static final String TAG_VALUE_SIEBEIL= "value";
		public static final String TAG_SIEBEL_XML_EXT_QUERY_REQ= "siebel-xmlext-query-req";
		public static final String TAG_SEARCH_SPEC= "search-spec";
		public static final String TAG_NODE= "node"; 
		public static final String TAG_NODE_TYPE= "node-type";
		public static final String TAG_VALUE_TYPE= "value-type";

		public static final String TAG_queryBillingAccountRq = "queryBillingAccountRq";
		public static final String TAG_LangPref = "LangPref";
		public static final String TAG_customerAccount = "customerAccount";
		public static final String TAG_AgreementItem = "AgreementItem";
		public static final String TAG_CustomerAccountId = "CustomerAccountId";
		public static final String TAG_queryBillingAccountRs = "queryBillingAccountRs";
		public static final String TAG_FuncId_40210010 = "40210010";
		public static final String TAG_CTReq_RecipientID = "RecipientID";
		
		public static final String TAG_SIM_NUMBER = "SimNumber";
		public static final String TAG_ATTACHED_FILE_NAME = "AttchedFileName";
		public static final String TAG_ATTACHED_FILE = "AttchedFile";
		public static final String TAG_TONE_COUNT_REQ = "ToneCntRequired";
		
		public static final String TAG_SIEBEL ="Siebel";
		public static final String TAG_HEADER ="Header";
		public static final String TAG_FUNCID = "FuncId";
		public static final String TAG_MSGVERSION = "MsgVersion";
		public static final String TAG_REQUESTORCHANNELID = "RequestorChannelId";
		public static final String TAG_DATE ="Date";
		public static final String TAG_SERVICE_ACCOUNT ="ServiceAccount";
		public static final String TAG_SERVICE_ACCOUNT_NUM ="ServiceAccountNumber";
		public static final String TAG_CUSTOMER_CONTACT ="CustoemrContact";
		public static final String TAG_BILLING_ADD_INFO ="BillingAddressInfo";
		public static final String TAG_BILLING_ACC_NUM ="BillingAccountNumber";
		public static final String TAG_SERVICE_ACC_ID ="ServiceAccountId";
		public static final String TAG_SERVICE_LINE_ITEM ="ServiceLineItem";
		public static final String TAG_ID_SMALL ="id";
		public static final String TAG_PRODUCT_NAME ="productName";
		public static final String TAG_NAME_SMALL ="name";
		public static final String TAG_VALUE_SMALL ="value";
		public static final String TAG_PENDING_ORDERS ="PendingOrders";
		public static final String TAG_ERRORCODE = "ErrorCode";
		public static final String TAG_ERROR_MESSAGE ="ErrorMessage";
		
		public static final String TAG_ORDER ="order";
		public static final String TAG_ORDER_HEADER ="orderHeader";
		public static final String TAG_ORDER_SERVICE_ACC ="orderServiceAccount";
		public static final String TAG_ORDER_LINE_ITEM ="orderLineItem";
		public static final String TAG_ACTION_CODE="ActionCode";
		public static final String TAG_ORDER_NUMBER = "OrderNumber";
		public static final String TAG_REQUEST_ID = "RequestId";
		
		public static final String TAG_SERIAL_NO = "SerialNo";
		public static final String TAG_REFERENCE_NUMBER = "ReferenceNumber";
		public static final String TAG_PAYMENT_TYPE = "PaymentType";
		public static final String TAG_PAYMENT_MODE = "PaymentMode";
		public static final String TAG_COMMENTS = "Comments";
		public static final String TAG_PAYMENT_REASON = "PaymentReason";
		public static final String TAG_DOC_ID_NUM ="DocIDNo";
		public static final String TAG_DOC_ID_TYPE ="DocIDType";
		public static final String TAG_CUSTOMER_NAME ="CustomerName";
		public static final String TAG_DISC_AMOUNT ="DiscountAmount";
		public static final String TAG_POS_INFO ="POSInfo";
		public static final String TAG_TERMINAL_ID ="TerminalID";
		public static final String TAG_CARD_NUMBER ="CardNumber";
		public static final String TAG_AUTH_CODE ="AuthorizationCode";
		public static final String TAG_RRN ="RRN";
		public static final String TAG_CARD_SCHEMES ="CardSchemes";
		public static final String TAG_PARTNER_REF_NUMBER ="PartnerReferenceNumber";
		public static final String TAG_INQ_PARAM ="InquiryParam";
		public static final String TAG_param ="param";
		public static final String TAG_SUBSCRIPTION_STATUS ="Subscriptionstatus";
		public static final String TAG_SERVICE_STATUS ="ServiceStatus";
		public static final String TAG_DESTINATION_LIST ="DestinationList";
		public static final String TAG_DESTINATION ="Destination";
		
		public static final String TAG_RENEWAL_AMOUNT ="RenewalAmount";
		public static final String TAG_EXPIRATION_DATE ="ExpiryDate";
		public static final String TAG_CUST_BALANCE ="CustomerBalance";

		// SDP Management
		public static final String TAG_SDP_MGMT_PRODUCT_ID = "ProductId";		
		public static final String TAG_SDP_MGMT_CONTENT_TYPE = "ContentType";
		public static final String TAG_SDP_MGMT_SERVICES = "Services";
		public static final String TAG_SDP_MGMT_SERVICE = "Service";
		public static final String TAG_SDP_MGMT_PARENT_ID = "ParentId";
		public static final String TAG_SDP_MGMT_PRODUCT_NAME = "ProductName";
		
		public static final String TAG_QUANTITY = "Quantity";
		
		public static final String TAG_PACKAGE_CLASSIFICATION = "PackageClassification";
}