/*
 * Created on Jun 08, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.PandaDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mail.ApacheMailSender;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.EmailVO;
import sa.com.mobily.eportal.common.service.vo.PandaEmailVO;
import sa.com.mobily.eportal.common.service.vo.PandaHistoryVO;
import sa.com.mobily.eportal.common.service.vo.PandaReportVO;
import sa.com.mobily.eportal.common.service.vo.ProfileContactVO;
import sa.com.mobily.eportal.common.service.vo.ProfileVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;
import sa.com.mobily.eportal.common.service.xml.PandaXMLHandler;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PandaCommonBO {

    private static final Logger log = LoggerInterface.log;
    
	 /**
	  date: Jun 8, 2009
	  Description: This method is used to do call MQ for distributing minutes to the MSISDNs.
	  @param profileVO
	  @param pandaHistoryVO 
	  @return PandaHistoryVO
    */
    public PandaHistoryVO pandaDistribution(ProfileVO profileVO,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException{
    	log.debug("PandaCommonBO > pandaDistribution > Called");

    	String xmlReply = "";
    	PandaXMLHandler  xmlHandler = new PandaXMLHandler();
    	PandaHistoryVO  pandaHistoryReplyVO = new PandaHistoryVO();

        try {
           
            //generate XML request
            String xmlMessage  = xmlHandler.generateXMLRequestForDistribution(profileVO,pandaHistoryVO);
            
            log.debug("PandaCommonBO > pandaDistribution >  xml message > "+xmlMessage);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            PandaDAO pandaDAO = daoFactory.getMQPandaDAO();
            xmlReply = pandaDAO.sendToMQWithReply(xmlMessage,ConstantIfc.PANDA_REQUEST_TYPE_DISTRIBUTION);
            log.debug("PandaCommonBO > pandaDistribution > parsing the reply message  > "+xmlReply);
            
            //parsing the xml reply
            pandaHistoryReplyVO = xmlHandler.parseDistributionXMLReply(xmlReply,pandaHistoryVO);
        } catch (MobilyCommonException e) {
            log.fatal("PandaCommonBO > pandaDistribution > MobilyCommonException > "+e.getErrorCode());
            throw e;
        }
      
        return pandaHistoryReplyVO;    
    
    }

	 /**
	  date: Jun 8, 2009
	  Description: This method is used to do Panda Service Inquiry from MQ for getting the Total Contract Minutes and Total Available Minutes.
	  @param corporateid
	  @param pandaHistoryVO 
	  @return PandaHistoryVO
    */    
    public PandaHistoryVO getPandaInquiry(String corporateId,PandaHistoryVO pandaHistoryVO) throws MobilyCommonException {
    	log.debug("PandaCommonBO > getPandaInquiry > Called");
        
    	String xmlReply = "";
        PandaXMLHandler  xmlHandler = new PandaXMLHandler();
        
        try {
            
            //generate XML request
            String xmlMessage  = xmlHandler.generateXMLRequestForAccInquiry(corporateId,pandaHistoryVO.getSrDate());
            log.debug("PandaCommonBO > getPandaInquiry > Request xml message > "+xmlMessage);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
            PandaDAO pandaDAO = daoFactory.getMQPandaDAO();
            xmlReply = pandaDAO.sendToMQWithReply(xmlMessage,ConstantIfc.PANDA_REQUEST_TYPE_INQUIRY);
            log.debug("PandaCommonBO > getPandaInquiry > Reply xml message > "+xmlReply);
            
            //parsing the xml reply
            pandaHistoryVO = xmlHandler.parseAccInquiryXMLReply(xmlReply,pandaHistoryVO);
            
        } catch (MobilyCommonException e) {
            log.fatal("PandaCommonBO > getPandaInquiry > MobilyCommonException > "+e.getErrorCode());
            throw e;
        }
      
        return pandaHistoryVO;    
    
    }
    
		 /**
		  date: July 02, 2009
		  Description: This method is used to do Panda Service Inquiry for MSISDN from MQ for getting the Distributed Minutes and Available Minutes.
		  @param pandaReportVO 
		  @return PandaReportVO
	  */    
	  public PandaReportVO getPandaMSISDNInquiry(PandaReportVO pandaReportVO) throws MobilyCommonException {
	  	log.debug("PandaCommonBO > getPandaMSISDNInquiry > Called");
	      
	  	  String xmlReply = "";
	      PandaXMLHandler  xmlHandler = new PandaXMLHandler();
      
	      try {
	          
	          //generate XML request
	          String xmlMessage  = xmlHandler.generateXMLRequestForMSISDNInquiry(pandaReportVO);
	          log.debug("PandaCommonBO > getPandaMSISDNInquiry > Request xml message > "+xmlMessage);
	          //get DAO object
	          MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
	          PandaDAO pandaDAO = daoFactory.getMQPandaDAO();
	          xmlReply = pandaDAO.sendToMQWithReply(xmlMessage,ConstantIfc.PANDA_REQUEST_TYPE_INQUIRY);
	          log.debug("PandaCommonBO > getPandaMSISDNInquiry > Reply xml message > "+xmlMessage);
	          
	          //parsing the xml reply
	          pandaReportVO = xmlHandler.parseMSISDNInquiryXMLReply(xmlReply,pandaReportVO);
	          
	      } catch (MobilyCommonException e) {
	          log.fatal("PandaCommonBO > getPandaMSISDNInquiry > MobilyCommonException > "+e.getErrorCode());
	          throw e;
	      }
	    
	      return pandaReportVO;    
	  
	  }
    
	 /**
	  date: Jun 17, 2009
	  Description: This method is used to send English and Arabic emails to the CAP and the Account Manager for the Exception occured in Distribution.
	  @param pandaEmailVO
 	*/
    public void sendEmail(PandaEmailVO pandaEmailVO){
    	
    	log.info("PandaCommonBO > sendEmail > corporateId > "+pandaEmailVO.getCorporateId());
    	
		String emailAddress = "";
		String customerName = "";
		String accountManagerName = "";		
		String accountManagerEmailId = "";	
		Map customerDetailsMap = null;
		
        //get DAO object
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
        CustomerProfileDAO customerProfileDAO = daoFactory.getCustomerProfileDAO();        
	
	//	 Check if the email is present in User_Account table
        UserAccountVO userAccountVO = customerProfileDAO.getUserAccountWebProfile(pandaEmailVO.getCorporateId());
        if(userAccountVO != null) {
			customerName = userAccountVO.getFullName();
			emailAddress = userAccountVO.getEmailAddress();
        }
	
	//	 If email is not present then get it from Siebel
		CustomerProfileReplyVO customerProfileReplyVO = customerProfileDAO.getCustomerProfileFromSiebel(pandaEmailVO.getCorporateId());
		if(customerProfileReplyVO != null) {
			accountManagerName = customerProfileReplyVO.getAccount().getAccountManagerName();
			if(accountManagerName == null) {
				accountManagerName = "";
			}
			accountManagerEmailId = customerProfileReplyVO.getAccount().getAccountManagerEmail();
			
			if(emailAddress == null || "".equals(emailAddress)) {
				customerName = customerProfileReplyVO.getAccount().getFirstName();
				emailAddress = customerProfileReplyVO.getAccount().getContact().getEmail();
			}
		}
		
		if(customerName == null) {
			customerName = "";
		}		
		
		String sender = MobilyUtility.getPropertyValue(ConstantIfc.SENDER_EMAIL);
		
	//	 If email address is present then send email to the CAP
		if(emailAddress != null && !"".equals(emailAddress)) {

			if(ConstantIfc.MINUTES_UNAVAILABLE.equals(pandaEmailVO.getErrorCode())){
				
				//Email for Admin (CAP) - English
				EmailVO emailVO = new EmailVO();
				emailVO.setSender(sender);
				ArrayList toAddress = new ArrayList();
				toAddress.add(emailAddress);					
				emailVO.setRecipients(toAddress);
				String subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_EN);
				emailVO.setSubject(subject);
				String title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_EN);					
				//	set messsage body
				StringBuffer msgBuffer = new StringBuffer();					
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(customerName);			
				msgBuffer.append(",");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_ADMIN_BODY1_EN));
				msgBuffer.append(pandaEmailVO.getDistributedMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_ADMIN_BODY2_EN));
				msgBuffer.append(pandaEmailVO.getAvailableMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_ADMIN_BODY3_EN));
				emailVO.setMailBody(msgBuffer.toString());
				
				ApacheMailSender mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
				//Email for Admin (CAP) - Arabic
				emailVO = new EmailVO();
				emailVO.setSender(sender);
				toAddress = new ArrayList();
				toAddress.add(emailAddress);										
				emailVO.setRecipients(toAddress);
				subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_AR);
				emailVO.setSubject(subject);
				title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_AR);					
				//	set messsage body
				msgBuffer = new StringBuffer();	
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(customerName);			
				msgBuffer.append(",");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_ADMIN_BODY1_AR));
				msgBuffer.append(" ");
				msgBuffer.append(pandaEmailVO.getDistributedMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_ADMIN_BODY2_AR));
				msgBuffer.append(" ");
				msgBuffer.append(pandaEmailVO.getAvailableMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_ADMIN_BODY3_AR));
				emailVO.setMailBody(msgBuffer.toString());
				
				mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
			} else {
				
				//Email for Admin (CAP) - English
				EmailVO emailVO = new EmailVO();
				emailVO.setSender(sender);
				ArrayList toAddress = new ArrayList();
				toAddress.add(emailAddress);										
				emailVO.setRecipients(toAddress);
				String subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_EN);
				emailVO.setSubject(subject);
				String title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_EN);					
				//	set messsage body
				StringBuffer msgBuffer = new StringBuffer();					
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(customerName);	
				msgBuffer.append(",");
				msgBuffer.append("\n\n");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_ERROR_ADMIN_BODY1_EN));
				emailVO.setMailBody(msgBuffer.toString());
				
				ApacheMailSender mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
				//Email for Admin (CAP) - Arabic
				emailVO = new EmailVO();
				emailVO.setSender(sender);
				toAddress = new ArrayList();
				toAddress.add(emailAddress);										
				emailVO.setRecipients(toAddress);
				subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_AR);
				emailVO.setSubject(subject);
				title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_AR);					
				//	set messsage body
				msgBuffer = new StringBuffer();					
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(customerName);	
				msgBuffer.append(",");
				msgBuffer.append("\n\n");					
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_ERROR_ADMIN_BODY1_AR));
				emailVO.setMailBody(msgBuffer.toString());
				
				mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
			}
		}
		
		//	 If email address is present then send email to the Account Manager
		if(accountManagerEmailId != null && !"".equals(accountManagerEmailId)) {
			
			if(ConstantIfc.MINUTES_UNAVAILABLE.equals(pandaEmailVO.getErrorCode())){
				
				//Email for Manager - English
				EmailVO emailVO = new EmailVO();
				emailVO.setSender(sender);
				ArrayList toAddress = new ArrayList();
				toAddress.add(accountManagerEmailId);					
				emailVO.setRecipients(toAddress);
				String subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_EN);
				emailVO.setSubject(subject);
				//	set messsage body
				String title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_EN);	
				StringBuffer msgBuffer = new StringBuffer();
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(accountManagerName);
				msgBuffer.append(",");
				msgBuffer.append("\n\n");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY1_EN));
				msgBuffer.append(" ");
				msgBuffer.append(customerName);	
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY2_EN));	
				msgBuffer.append(pandaEmailVO.getDistributedMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY3_EN));
				msgBuffer.append(pandaEmailVO.getAvailableMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY4_EN));
				msgBuffer.append(customerName+"\n\n");	
				msgBuffer.append("</body></html>");
				emailVO.setMailBody(msgBuffer.toString());
				
				ApacheMailSender mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
				//Email for Manager - Arabic
				emailVO = new EmailVO();
				emailVO.setSender(sender);
				toAddress = new ArrayList();
				toAddress.add(accountManagerEmailId);										
				emailVO.setRecipients(toAddress);
				subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_AR);
				emailVO.setSubject(subject);
				//	set messsage body
				title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_AR);				
				msgBuffer = new StringBuffer();					
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(accountManagerName);
				msgBuffer.append(",");
				msgBuffer.append("\n\n");						
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY1_AR));
				msgBuffer.append(" ");
				msgBuffer.append(customerName);	
				msgBuffer.append(" ");					
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY2_AR));	
				msgBuffer.append(pandaEmailVO.getDistributedMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY3_AR));
				msgBuffer.append(pandaEmailVO.getAvailableMinutes());
				msgBuffer.append(" ");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TRANSACTION_MANAGER_BODY4_AR));
				msgBuffer.append(customerName+"\n\n");	
				msgBuffer.append("</body></html>");
				emailVO.setMailBody(msgBuffer.toString());
				
				mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
			} else {
				
				//Email for Manager - English
				EmailVO emailVO = new EmailVO();
				emailVO.setSender(sender);
				ArrayList toAddress = new ArrayList();
				toAddress.add(accountManagerEmailId);										
				emailVO.setRecipients(toAddress);
				String subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_EN);
				emailVO.setSubject(subject);
				//	set messsage body
				String title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_EN);	
				StringBuffer msgBuffer = new StringBuffer();	
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(accountManagerName);	
				msgBuffer.append(",");
				msgBuffer.append("\n\n");
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_ERROR_MANAGER_BODY1_EN));
				msgBuffer.append(customerName);	
				msgBuffer.append(" ");		
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_ERROR_MANAGER_BODY2_EN));					
				emailVO.setMailBody(msgBuffer.toString());
				
				ApacheMailSender mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);					
				
				//Email for Manager - Arabic
				emailVO = new EmailVO();
				emailVO.setSender(sender);
				toAddress = new ArrayList();
				toAddress.add(accountManagerEmailId);										
				emailVO.setRecipients(toAddress);
				subject = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_SUBJECT_AR);
				emailVO.setSubject(subject);
				//	set messsage body
				title = MobilyUtility.getPropertyValue(ConstantIfc.REPLY_TITLE_AR);				
				msgBuffer = new StringBuffer();					
				msgBuffer.append(title);
				msgBuffer.append(" ");
				msgBuffer.append(accountManagerName);
				msgBuffer.append(",");
				msgBuffer.append("\n\n");						
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_ERROR_MANAGER_BODY1_AR));
				msgBuffer.append(customerName);	
				msgBuffer.append(" ");					
				msgBuffer.append(MobilyUtility.getPropertyValue(ConstantIfc.REPLY_ERROR_MANAGER_BODY2_AR));					
				emailVO.setMailBody(msgBuffer.toString());
				
				mailSender = new ApacheMailSender();
				mailSender.sendEmailAsHtml(emailVO);										
				
			}
		}
		
		
	}
   
}
