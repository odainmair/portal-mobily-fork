package sa.com.mobily.eportal.common.exception.application;


public class InvalidActivationCodeException extends MobilyApplicationException {

 
	private static final long serialVersionUID = -3419462789848440657L;

	public InvalidActivationCodeException() {
	}

	public InvalidActivationCodeException(String message) {
		super(message);
	}

	public InvalidActivationCodeException(Throwable cause) {
		super(cause);
	}

	public InvalidActivationCodeException(String message, Throwable cause) {
		super(message, cause);
	}
}