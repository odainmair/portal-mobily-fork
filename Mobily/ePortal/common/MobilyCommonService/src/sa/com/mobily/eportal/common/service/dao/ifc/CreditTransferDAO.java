package sa.com.mobily.eportal.common.service.dao.ifc;

import java.util.ArrayList;

public interface CreditTransferDAO {
	public ArrayList<String> getBlackListPackageList();
	public boolean isShowIdField(String msisdn, String packageId);
}
