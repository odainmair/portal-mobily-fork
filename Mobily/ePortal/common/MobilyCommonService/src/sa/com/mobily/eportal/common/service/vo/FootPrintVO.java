package sa.com.mobily.eportal.common.service.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.constant.FootPrintEvents;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "userId", "eventName", "key", "subKey", "comments", "requestTimeStamp", 
		"systemErrorMessage", "userErrorMessage", "errorCode", "requesterChannelId", "serviceAccount"})
@XmlRootElement(name = "FOOTPRINT")
public class FootPrintVO extends BaseVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(name = "USER_ID")
	private String 	userId;				//The portal user name
	@XmlElement(name = "EVENT_NAME")
	private String 	eventName;			//EventId represent the event like view bill summary event
	@XmlElement(name = "KEY")
	private String 	key;				//The key to the event: e.g. Active Msisdn in most cases
	@XmlElement(name = "SUB_KEY")
	private String 	subKey;				//e.g. bill id in view bill summary event
	@XmlElement(name = "COMMENTS")
	private String 	comments;			//Extra information
	@XmlElement(name = "REQUEST_TIME_STAMP")
	private Date 	requestTimeStamp;			//Which maps to system time stamp
	@XmlElement(name = "SYSTEM_ERROR_MESSAGE")
	private String 	systemErrorMessage;	//internal error message like exception, error message, exception.getMessage()
	@XmlElement(name = "USER_ERROR_MESSAGE")
	private String 	userErrorMessage;	//End User Error Message
	@XmlElement(name = "ERROR_CODE")
	private String 	errorCode;			//general error code to the system
	@XmlElement(name = "REQUEST_CHANNEL_ID")
	private int 	requesterChannelId; // 1-Mobily and 2-IPhone
	@XmlElement(name = "SERVICE_ACCOUNT")
	private String 	serviceAccount;
	
	public FootPrintVO(){
		super();
		this.userId = "";
		this.key = "";
		this.subKey = "";
		this.comments = "";
		this.systemErrorMessage = "";
		this.userErrorMessage = "";
		this.errorCode = "";
		this.requesterChannelId = 1;
		this.serviceAccount = null;
	}

	public FootPrintVO(String userId, String eventName, String key, String subKey, String comments, Date requestTimeStamp, String systemErrorMessage,
			String userErrorMessage, String errorCode, int requesterChannelId, String serviceAccount)
	{
		super();
		this.userId = userId;
		this.eventName = eventName;
		this.key = key;
		this.subKey = subKey;
		this.comments = comments;
		this.requestTimeStamp = requestTimeStamp;
		this.systemErrorMessage = systemErrorMessage;
		this.userErrorMessage = userErrorMessage;
		this.errorCode = errorCode;
		this.requesterChannelId = requesterChannelId;
		this.serviceAccount = serviceAccount;
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	
	public String getEventName()
	{
		return eventName;
	}

	public void setEventName(String eventName)
	{
		this.eventName = eventName;
	}
	
	public void setEventName(FootPrintEvents eventName)
	{
		this.eventName = eventName.toString();
	}

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	public String getSubKey()
	{
		return subKey;
	}

	public void setSubKey(String subKey)
	{
		this.subKey = subKey;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(String comments)
	{
		this.comments = comments;
	}

	public Date getRequestTimeStamp()
	{
		return requestTimeStamp;
	}

	public void setRequestTimeStamp(Date requestTimeStamp)
	{
		this.requestTimeStamp = requestTimeStamp;
	}

	public String getSystemErrorMessage()
	{
		return systemErrorMessage;
	}

	public void setSystemErrorMessage(String systemErrorMessage)
	{
		this.systemErrorMessage = systemErrorMessage;
	}
	
	public String getUserErrorMessage()
	{
		return userErrorMessage;
	}

	public void setUserErrorMessage(String userErrorMessage)
	{
		this.userErrorMessage = userErrorMessage;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}

	public int getRequesterChannelId()
	{
		return requesterChannelId;
	}

	public void setRequesterChannelId(int requesterChannelId)
	{
		this.requesterChannelId = requesterChannelId;
	}

	public String getServiceAccount()
	{
		return serviceAccount;
	}

	public void setServiceAccount(String serviceAccount)
	{
		this.serviceAccount = serviceAccount;
	}
}