package sa.com.mobily.eportal.common.service.dao.ifc;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ConnectOTSDAO {
	 public String doVoucherRecharge(String xmlMessage);
	 public String getMsidnForSim(String xmlMessage);
}
