/*
 * Created on Sep 4, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.imp.OraclePhoneBookDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.PhoneBookContactVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookGroupVO;
import sa.com.mobily.eportal.common.service.vo.PhoneBookListVO;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PhoneBookBO {
	
	private static final Logger log = LoggerInterface.log;

	/**
	 * @param groupVO -
	 *            DISTRIBUTION_LIST_ID - FUNCTION_ID - MSISDN - GROUP_NAME
	 * @throws MobilyCommonException
	 */
	public void createNewGroup(PhoneBookGroupVO groupVO)throws MobilyCommonException {

		log.debug("PhoneBookIntegration > createNewGroup > Called");

		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();

			phoneBookDAO.createPhoneBookGroup(groupVO);
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}

	/**
	 * Create new contact into local DB
	 * @param contactVO
	 * @throws MobilyCommonException
	 */
	public void createNewPhoneBookContact(PhoneBookContactVO contactVO) throws MobilyCommonException{
		log.debug("PhoneBookIntegration > createNewPhoneBookContact > Called");
		
		//calling DB DAO to create new Rule
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();

			phoneBookDAO.createPhoneBookContact(contactVO);
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}
		
	}
	
	
	/**
	 * @param msidn
	 * @return
	 * @throws MobilyCommonException
	 */
	public ArrayList listCustomerContacts(String msisdn , int funcId) throws MobilyCommonException{
		ArrayList listOfContact = null;
		log.debug("PhoneBookIntegration > listCustomerContacts > Called");
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();

			listOfContact = phoneBookDAO.listCustomerPhoneBookMember(msisdn ,funcId );
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}

		return listOfContact;
	
	}
	
	/**
	 * @param msisdn
	 * @param funcId
	 * @return
	 * @throws MobilyCommonException
	 */
	public ArrayList listCustomerPhoneBookGroup(String msisdn , int funcId) throws MobilyCommonException{
		ArrayList listOfGroups = null;
		log.debug("PhoneBookIntegration > listCustomerPhoneBookGroup > Called");
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
			
			listOfGroups = phoneBookDAO.listCustomerPhoneBookGroup(msisdn ,funcId );
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}

		return listOfGroups;
	}
	
	
	/**
	 * This Function responisble for get list of contact exist in customer specific group
	 * @param groupId
	 * 
	 * @return
	 * @throws MobilyCommonException
	 */
	public PhoneBookGroupVO listCustomerPhoneBookGroupMember(int groupId) throws MobilyCommonException{
		PhoneBookGroupVO groupVO = null;
		log.debug("PhoneBookIntegration > listCustomerPhoneBookGroupMember > Called");
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
			
			groupVO = phoneBookDAO.listCustomerPhoneBookGroupMember(groupId);
		} catch (MobilyCommonException e) {
			throw e;
		}
		return groupVO;
	}

	
	/**
	 * @param groupSequenceId
	 * @throws MobilyCommonException
	 */
	public void deleteExistGroup(int groupId) throws MobilyCommonException{
		log.debug("PhoneBookIntegration > deleteExistGroup  > Called for GroupId ["+groupId+"]");
		
		//calling DB DAO to Delete exist group
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();

			phoneBookDAO.DeletePhoneBookGroup(groupId);
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}

		
	}
	
	
	/**
	 * Responsible for Adding contact to group 
	 * @param PhoneBookGroupVO
	 * 			 - group id 
	 * 			 - ArrayList of phoneBookContact , contactId
	 * @throws MobilyCommonException
	 */
	public void addMemebrToGroup(PhoneBookGroupVO groupVO)throws MobilyCommonException{
		ArrayList listOfGroups = null;
		log.debug("PhoneBookIntegration > addMemebrToGroup > Called");
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
			
			phoneBookDAO.joinContactToGroup(groupVO);
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}
	
	/**
	 * @param groupVO
	 * @throws MobilyCommonException
	 */
	public void removeMemberFromGroup(PhoneBookGroupVO groupVO)throws MobilyCommonException{
		ArrayList listOfGroups = null;
		log.debug("PhoneBookIntegration > addMemebrToGroup > Called");
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
			
			phoneBookDAO.DeletePhoneBookGroupMemeber(groupVO.getListOfGroupMemeber() , groupVO.getGroupId());
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}
	
	}
	
	
	/**
	 * @param groupVO
	 * @throws MobilyCommonException
	 */
	public PhoneBookListVO listPhoneBook(String msisdn , int functionId)throws MobilyCommonException{
		PhoneBookListVO phoneBook  =  null;
		log.debug("PhoneBookIntegration > addMemebrToGroup > Called");
		try {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
			OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
			
			phoneBook  = phoneBookDAO.listPhoneBook(msisdn , functionId);
		} catch (MobilyCommonException e) {
			// TODO Auto-generated catch block
			throw e;
		}
	  return phoneBook;
	}

	//The following functions are created for VSMS project ----- START	

		/**
	     * Responsible for getting the function id
	     * @param groupVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int getFunctionId(String functionName)throws MobilyCommonException {
	
			log.debug("PhoneBookIntegration > getFunctionId > Start");
			int functionId = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				functionId = phoneBookDAO.getFunctionId(functionName);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > getFunctionId > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > getFunctionId > End");
	
			return functionId;
		}
	
	    /**
	     * Responsible for checking the presence of given contact msisdn
	     * @param contactVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public boolean isContactMsisdnAlreadyExist(PhoneBookContactVO contactVO)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > isContactMsisdnAlreadyExist > Start");
	
			boolean status = false;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.isContactMsisdnAlreadyExist(contactVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > isContactMsisdnAlreadyExist > MobilyCommonException > "+e.getMessage());
				throw e;
			}
			log.debug("PhoneBookIntegration > isContactMsisdnAlreadyExist > End");
	
			return status;
		}
	    
	    /**
	     * Responsible for creating a new distribution list
	     * @param groupVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int createNewDistributionList(PhoneBookGroupVO groupVO)throws MobilyCommonException {
	
			log.debug("PhoneBookIntegration > createNewDistributionList > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.createNewDistributionList(groupVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > createNewDistributionList > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > createNewDistributionList > End");
	
			return status;
		}
	
	    /**
	     * Responsible for creating a new member for a distribution list
	     * @param contactVO
	     * @param distListId
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int createDistributionListEntry(PhoneBookContactVO contactVO,int distListId) throws MobilyCommonException{
			log.debug("PhoneBookIntegration > createDistributionListEntry > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.createDistributionListEntry(contactVO,distListId);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > createDistributionListEntry > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > createDistributionListEntry > End");
	
			return status;
		}
	
		/**
	     * Responsible for updating member of black list
	     * @param contactVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int updateListEntry(PhoneBookContactVO contactVO)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > updateListEntry > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.updateListEntry(contactVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > updateListEntry > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > updateListEntry > End");
	
			return status;
		}

		/**
	     * Responsible for updating member of distribution list
	     * @param contactVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int updateDistributionListEntry(PhoneBookContactVO contactVO)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > updateDistributionListEntry > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.updateDistributionListEntry(contactVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > updateDistributionListEntry > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > updateDistributionListEntry > End");
	
			return status;
		}

		/**
	     * Responsible for deleting a distribution list
	     * @param phoneBookVo
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int deleteDistributinList(PhoneBookGroupVO phoneBookVo)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > deleteDistributinList > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.deleteDistributinList(phoneBookVo);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > deleteDistributinList > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > deleteDistributinList > End");
	
			return status;
		}
	
		/**
	     * Responsible for deleting a member of distribution list
	     * @param contactId
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int deleteDistributionListMember(int contactId) throws MobilyCommonException{
			log.debug("PhoneBookIntegration > deleteDistributionListMember > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.deleteDistributionListMember(contactId);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > deleteDistributionListMember > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > deleteDistributionListMember > End");
	
			return status;
		}
		
		/**
	     * Responsible for listing all the members of a distribution list
	     * @param groupVO
	     * @return listOfMembers
	     * @throws MobilyCommonException
	     */
		public ArrayList listDuistributionListMembers(PhoneBookGroupVO groupVO)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > listDuistributionListMembers > Start");
			ArrayList listOfMembers;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				listOfMembers = phoneBookDAO.listDuistributionListMembers(groupVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > listDuistributionListMembers > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > listDuistributionListMembers > End");
	
			return listOfMembers;
		}
	
		/**
	     * Responsible for checking the presence of a owner MSISDN
	     * @param groupVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public boolean isOwnerMsisdnAlreadyExist(PhoneBookGroupVO groupVO)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > isOwnerMsisdnAlreadyExist > Start");
			boolean status = false;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.isOwnerMsisdnAlreadyExist(groupVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > isOwnerMsisdnAlreadyExist > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > isOwnerMsisdnAlreadyExist > End");
	
			return status;
		}
	
		/**
	     * Responsible for creating a blacklist
	     * @param groupVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int createNewBlackList(PhoneBookGroupVO groupVO)throws MobilyCommonException{
			log.debug("PhoneBookIntegration > createNewBlackList > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.createNewBlackList(groupVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > createNewBlackList > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > createNewBlackList > End");
	
			return status;
		}
	
		/**
	     * Responsible for creating a black lsit member
	     * @param contactVO
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int createBlackListEntry(PhoneBookContactVO contactVO) throws MobilyCommonException{
			log.debug("PhoneBookIntegration > createBlackListEntry > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.createBlackListEntry(contactVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > createBlackListEntry > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > createBlackListEntry > End");
	
			return status;
		}
	
		/**
	     * Responsible for listing all the members of black list
	     * @param groupVO
	     * @return listOfMembers
	     * @throws MobilyCommonException
	     */
		public ArrayList listBlackListMembers(PhoneBookGroupVO groupVO)	throws MobilyCommonException{
			log.debug("PhoneBookIntegration > listBlackListMembers > Start");
			ArrayList listOfMembers;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				listOfMembers = phoneBookDAO.listBlackListMembers(groupVO);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > listBlackListMembers > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > listBlackListMembers > End");
	
			return listOfMembers;
		}
		/**
	     * Responsible for deleting a member of black list
	     * @param contactId
	     * @return status
	     * @throws MobilyCommonException
	     */
		public int deleteBlackListMember(int contactId) throws MobilyCommonException{
			log.debug("PhoneBookIntegration > deleteBlackListMember > Start");
			int status = 0;
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				OraclePhoneBookDAO phoneBookDAO = (OraclePhoneBookDAO) daoFactory.getPhoneBookDAO();
	
				status = phoneBookDAO.deleteBlackListMember(contactId);
			} catch (MobilyCommonException e) {
			   	log.fatal("PhoneBookBO > deleteBlackListMember > MobilyCommonException > "+e.getMessage());
				throw e;
			}
	
			log.debug("PhoneBookIntegration > deleteBlackListMember > End");
	
			return status;
		}
			
	//The Above functions are created for VSMS project ----- END	
}
