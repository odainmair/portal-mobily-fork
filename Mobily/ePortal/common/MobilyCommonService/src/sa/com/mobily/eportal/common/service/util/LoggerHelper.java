package sa.com.mobily.eportal.common.service.util;


import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class LoggerHelper {
	
    public static Logger getLogger(String fileName) {
    	ResourceBundleHelper loggerBundle = new ResourceBundleHelper();
        Logger logger = Logger.getLogger(fileName);

        try {
        	
        	if (logger.getUseParentHandlers() || logger.getHandlers().length == 0) {
            	FileHandler fileHandler = null;
            	fileName = "/"+fileName+"_%g.log";
                //StringBuffer loggerOutFile = new StringBuffer(loggerBundle.getKeyValue("logger.pattern"));
                StringBuffer loggerRootDir = new StringBuffer(System.getProperty("MOBILY_LOG_ROOT"));
                //StringBuffer loggerAppsDir = new StringBuffer(loggerBundle.getKeyValue("logger.dir"));
                int limit = Integer.valueOf(loggerBundle.getKeyValue("logger.limit").trim());
                int count = Integer.valueOf(loggerBundle.getKeyValue("logger.count").trim());
                boolean useParentHandlers = Boolean.getBoolean(loggerBundle.getKeyValue("logger.useParentHandler"));
                
            	logger.setUseParentHandlers(useParentHandlers);
            	fileHandler = new FileHandler(loggerRootDir.append(fileName).toString(), limit, count, true);
                SimpleFormatter formatter = new SimpleFormatter();
                fileHandler.setFormatter(formatter);
                logger.addHandler(fileHandler);
            }
        } catch (IOException ex) {
            Logger.getLogger(LoggerHelper.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(LoggerHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

        return logger;
    }

}