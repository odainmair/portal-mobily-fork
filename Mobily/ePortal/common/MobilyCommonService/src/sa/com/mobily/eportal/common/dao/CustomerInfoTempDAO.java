package sa.com.mobily.eportal.common.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;


import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.valueobject.common.OTPUserSubscription;

public class CustomerInfoTempDAO extends AbstractDBDAO<OTPUserSubscription> {

	//private static final String CLASS_NAME = LogDAO.class.getName();
	//private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(SiteManagementLoggerConstantsIfc.MQ_DETAILS_LOGGER_NAME);

	private static CustomerInfoTempDAO instance = new CustomerInfoTempDAO();
	
	private static final String SUBSCRIPTION_BY_MSISDN = "SELECT * FROM CUSTOMER_PROFILE_TMP WHERE MSISDN = ?"; 
	private static final String DELETE_BY_MSISDN = "DELETE FROM CUSTOMER_PROFILE_TMP WHERE MSISDN = ?"; 
	
	private static final String USER_SUBSCRIPTION_ADD = "INSERT INTO CUSTOMER_PROFILE_TMP "
			+ "(SUBSCRIPTION_TYPE,MSISDN,SERVICE_ACCT_NUMBER,IQAMA, CUSTOMER_TYPE,PACKAGE_ID,"
			+ "CREATED_DATE,IS_ACTIVE,SUB_SUBSCRIPTION_TYPE) VALUES " + "(?, ?, ?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?)";
	
	private static final String USER_SUBSCRIPTION_UPDATE = "UPDATE CUSTOMER_PROFILE_TMP SET "
			+ "SUBSCRIPTION_TYPE = ?,SERVICE_ACCT_NUMBER = ?,IQAMA = ?, CUSTOMER_TYPE = ?,PACKAGE_ID = ?,"
			+ "CREATED_DATE = CURRENT_TIMESTAMP,IS_ACTIVE = ?,SUB_SUBSCRIPTION_TYPE = ? WHERE MSISDN = ?";

	
	
	/**
	 * constructor
	 */
	private CustomerInfoTempDAO() {
		super(DataSources.DEFAULT);
	}
	
	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>CustomerInfoTempDAO</code>
	 * @author r.agarwal.mit
	 */
	public static CustomerInfoTempDAO getInstance() {
		return instance;
	}

	public int saveUserSubscriptionTemp(OTPUserSubscription dto)
	{
		return update(USER_SUBSCRIPTION_ADD, dto.getSubscriptionType(), dto.getMsisdn(), dto.getServiceAcctNumber(),
				dto.getNationalIdOrIqamaNumber(), dto.getCustomerType(), dto.getPackageId(), dto.getIsActive(), dto.getLineType());
	}
	
	public int updateUserSubscriptionTemp(OTPUserSubscription dto)
	{
		return update(USER_SUBSCRIPTION_UPDATE, dto.getSubscriptionType(), dto.getServiceAcctNumber(),
				dto.getNationalIdOrIqamaNumber(), dto.getCustomerType(), dto.getPackageId(), dto.getIsActive(), dto.getLineType(),dto.getMsisdn());
	}
	
	/**
	 * 
	 * @return
	 */
	public List<OTPUserSubscription> getTempSubscriptionByMsisdn(String msisdn){
		return query(SUBSCRIPTION_BY_MSISDN, msisdn);
	}

	
	 public int deleteTempSubscriptionByMsisdn(String msisdn) {
		 executionContext.audit(Level.INFO, "deleteTempSubscriptionByMsisdn >  msisdn=["+msisdn+"]", "", "CustomerInfoTempDAO");
		 return update(DELETE_BY_MSISDN,msisdn);
	}
	/**
	 * 
	 */
	@Override
	protected OTPUserSubscription mapDTO(ResultSet rs) throws SQLException {
		OTPUserSubscription userSubscription = new OTPUserSubscription();

		userSubscription.setSubscriptionType(rs.getInt("SUBSCRIPTION_TYPE"));
		userSubscription.setMsisdn(rs.getString("MSISDN"));
		userSubscription.setServiceAcctNumber(rs.getString("SERVICE_ACCT_NUMBER"));
		userSubscription.setCustomerType(rs.getString("CUSTOMER_TYPE"));
		userSubscription.setPackageId(rs.getString("PACKAGE_ID"));
		userSubscription.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
		userSubscription.setIsActive(rs.getString("IS_ACTIVE"));
		userSubscription.setLineType(rs.getInt("SUB_SUBSCRIPTION_TYPE"));
		userSubscription.setNationalIdOrIqamaNumber(rs.getString("IQAMA"));
		
		return userSubscription;
	}

}
