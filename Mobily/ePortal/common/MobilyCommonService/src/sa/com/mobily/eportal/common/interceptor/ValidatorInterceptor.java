package sa.com.mobily.eportal.common.interceptor;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.Resource;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.exception.BusinessValidationException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.BaseVO;
 

public class ValidatorInterceptor {
    private static final Logger log = LoggerInterface.log;
    
	@Resource Validator _validator; 
	
	@AroundInvoke
	public Object validate(InvocationContext ctx) throws BusinessValidationException {

		log.debug("ValidatorInterceptor > validate > started");
		Object[] parameters = ctx.getParameters();
		BaseVO inputVO = (BaseVO) parameters[0];
		Set<ConstraintViolation<BaseVO>> constraintViolations = _validator
				.validate(inputVO);
		Set<String> violationMessages = new HashSet<String>();
		Iterator<ConstraintViolation<BaseVO>> it = constraintViolations
				.iterator();
		if (constraintViolations.size() > 0) {
			while (it.hasNext()) {
				ConstraintViolation<BaseVO> cc = it.next();
				violationMessages.add(cc.getPropertyPath() + ": "
						+ cc.getMessage());
			}
			
			log.debug("ValidatorInterceptor > validate > exception : " + violationMessages.size());
			
			throw new BusinessValidationException(violationMessages);
			
		}
		
		log.debug("ValidatorInterceptor > validate > end");
		
		try {
			return ctx.proceed();
		} catch (Exception e) {
			// logger.warning("Error calling ctx.proceed in modifyGreeting()");
			return null;
		}
		
		
	}
	    
}
 
