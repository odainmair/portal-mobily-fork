package sa.com.mobily.eportal.wcm.faqs.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "faqName", "faqType", "counter" })
@XmlRootElement(name = "FAQ")
public class FaqVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@XmlElement(name = "FAQ_NAME")
	private String faq_name;

	@XmlElement(name = "FAQ_TYPE")
	private String faq_type;

	@XmlElement(name = "COUNTER")
	private int counter;

	public FaqVO()
	{
		super();
		this.faq_name = "";
		this.faq_type = "";
		this.counter = 0;

	}

	public FaqVO(String faqName, String faqType, int counter)
	{
		super();
		this.faq_name = faqName;
		this.faq_type = faqType;
		this.counter = counter;
	}

	public String getFaqName()
	{
		return faq_name;
	}

	public void setFaqName(String faqName)
	{
		this.faq_name = faqName;
	}

	public String getFaqType()
	{
		return faq_type;
	}

	public void setFaqType(String faqType)
	{
		this.faq_type = faqType;
	}

	public int getCounter()
	{
		return counter;
	}

	public void setCounter(int counter)
	{
		this.counter = counter;
	}

}