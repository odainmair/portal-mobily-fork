/**
 * @date: Dec 28, 2011
 * @author: s.vathsavai.mit
 * @file name: LoyaltyXMLHandler.java
 * @description:  
 */
package sa.com.mobily.eportal.common.service.xml;

import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.LoyaltyRequestVO;

/**
 * @author s.vathsavai.mit
 *
 */
public class LoyaltyXMLHandler {

	private static final Logger log = LoggerInterface.log;
	
	/**
	 * @date: Dec 28, 2011
	 * @author: s.vathsavai.mit
	 * @description: This method is used to generate the request for Corporate level inquiry request from ePortal to Loyalty.
	 * @param strBillingAccoutnNumber
	 * @return
	 * <MOBILY_LOYALTY_SR>
	        <SR_HEADER>
                <FuncId>LOYALTY_CORPORATE_INQUIRY</FuncId>
                <SecurityKey></SecurityKey>
                <MsgVersion>0000</MsgVersion>
                <RequestorChannelId>XXX</RequestorChannelId>
                <ServiceRequestId>SR_1234567890</ServiceRequestId>
                <SrDate>20090921094217</SrDate>
                <RequestorUserId>PF Number </RequestorUserId>
                <RequestorLanguage>E</RequestorLanguage>
	        </SR_HEADER>
	        <RootAccountNumber>1000111120860475</RootAccountNumber>
		</MOBILY_LOYALTY_SR>
	 */
	public String generateCorporateLevelInquiryRequest(LoyaltyRequestVO loyaltyRequestVO) {
			String xmlRequest = "";
			log.info("LoyaltyXMLHandler > generateCorporateLevelInquiryRequest : start");
			try {
				String srdate = FormatterUtility.FormateDate(new Date());
				Document doc = new DocumentImpl();
				Element root = doc.createElement(TagIfc.TAG_MOBILY_LOYALTY_SR);
				//header
				Element header = generateLoyaltyHeaderRequest(doc,loyaltyRequestVO);
				XMLUtility.closeParentTag(root, header);
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ROOT_ACCOUNT_NUMBER,FormatterUtility.checkNull(loyaltyRequestVO.getBillingAccountNumber()));
				doc.appendChild(root);
				xmlRequest = XMLUtility.serializeRequest(doc);
			}catch (Exception e) {
				log.fatal("LoyaltyXMLHandler > generateCorporateLevelInquiryRequest :: Exception ,"+ e);
				throw new SystemException(e);
			}
			log.info("LoyaltyXMLHandler > generateCorporateLevelInquiryRequest : end");
			return xmlRequest;
		}
	
	
	/**
	 * @date: Dec 28, 2011
	 * @author: s.vathsavai.mit
	 * @description: Responsible for generate xml header tag , this method is generate header tag thats common for all ePortal to Loyalty
	 * @param doc
	 * @param loyaltyRequestVO
	 * @return Element
	 * @throws SystemException
	 */
	private  Element generateLoyaltyHeaderRequest(Document doc,LoyaltyRequestVO loyaltyRequestVO) throws SystemException{
		log.debug("LoyaltyXMLHandler > generateLoyaltyHeaderRequest :: start");
		Element header = null;
		try{
			String srdate = FormatterUtility.FormateDate(new Date());
			header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.FUNC_ID_LOYALTY_CORPORATE_INQUIRY);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITYKEY,ConstantIfc.SECURITY_KEY_123);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION,ConstantIfc.SECURITY_KEY_123);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.REQUESTOR_CHANNEL_ID);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SERVICE_REQUEST_ID,"SR_"+srdate);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_USER_ID,FormatterUtility.checkNull(loyaltyRequestVO.getUserId()));
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_LANGUAGE,FormatterUtility.checkNull(loyaltyRequestVO.getLanguage()));
		
		}catch(Exception e){
			log.fatal("LoyaltyXMLHandler > generateLoyaltyHeaderRequest :: Exception "+e);
			throw new SystemException(e);
		}
		log.debug("LoyaltyXMLHandler > generateLoyaltyHeaderRequest :: end");
		return header;
	}


	/**
	 * @date: Dec 28, 2011
	 * @author: s.vathsavai.mit
	 * @description: 
	 * @param xmlReply
	 * @return
	 * <MOBILY_LOYALTY_SR_REPLY>
            <SR_HEADER_REPLY>
                <FuncId> LOYALTY_CORPORATE_INQUIRY</FuncId>
                 <SecurityKey></SecurityKey>
                <MsgVersion>0000</MsgVersion>
                <RequestorChannelId>XXX</RequestorChannelId>
                <ServiceRequestId>SR_1234567890</ServiceRequestId>
                <SrDate>20090904091557</SrDate>
                <SrStatus>2</SrStatus>
            </SR_HEADER_REPLY>
            <ReturnCode>
                <ErrorCode>0</ErrorCode>
            </ReturnCode>
            <NEQATYPLUS_INFO>
                <RootAccountNumber>1000111120860475</RootAccountNumber>                                           
                <TotalNoOfLines>15</TotalNoOfLines>   
                <CurrentBalance>12085.25</CurrentBalance>  		   
                <TotalPointsEarned>200.50</TotalPointsEarned>  
	   			<TotalPointsLost>100.30</TotalPointsLost>  
	   			<TotalPointsExpired>40.75</TotalPointsExpired>  
       </NEQATYPLUS_INFO>
</ MOBILY_LOYALTY_SR_REPLY>

	 */
public String parseCorporateLoyaltyReply(String xmlReply) {
		String errorCode = "0";
		log.info("LoyaltyXMLHandler > parseCorporateLoyaltyReply :: start ");
		try {
			if (xmlReply == null || xmlReply == "")
				throw new SystemException();

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			if(doc != null) {
				NodeList nodeList = doc.getElementsByTagName(TagIfc.TAG_MOBILY_LOYALTY_SR_REPLY);
				if(nodeList != null && nodeList.getLength() > 0) {
					Node rootNode = nodeList.item(0);
					NodeList nl = rootNode.getChildNodes();
					for (int j = 0; j < nl.getLength(); j++) {
						if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
							continue;
				
						Element l_Node = (Element) nl.item(j);
						String p_szTagName = l_Node.getTagName();
						String l_szNodeValue = null;
						if (l_Node.getFirstChild() != null)
							l_szNodeValue = l_Node.getFirstChild().getNodeValue();
						//log.debug("p_szTagName = "+p_szTagName);
						if(TagIfc.TAG_RETURN_CODE.equalsIgnoreCase(p_szTagName)){
							NodeList errorNode = l_Node.getChildNodes();
							Element nodeElement = null;
							String nodeTagName = "";
							String nodeTageValue = "";
							
							for(int k=0; k< errorNode.getLength(); k++) {
								if ((errorNode.item(k)).getNodeType() != Node.ELEMENT_NODE)
									continue;
								
								nodeTageValue = "";
								nodeElement = (Element)errorNode.item(k);
								nodeTagName =nodeElement.getTagName();
								
								if (nodeElement.getFirstChild() != null)
									nodeTageValue = nodeElement.getFirstChild().getNodeValue();
								
								if(TagIfc.TAG_ERROR_CODE.equalsIgnoreCase(nodeTagName)) {
									log.debug("LoyaltyXMLHandler > parseCorporateLoyaltyReply : error code is "+nodeTageValue);
									errorCode = nodeTageValue;
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			log.fatal("LoyaltyXMLHandler > parseCorporateLoyaltyReply :: Exception :: "	+ e);
			throw new SystemException(e);
		}
		log.info("LoyaltyXMLHandler > parseCorporateLoyaltyReply :: end");
		return errorCode;
	}
	
}
