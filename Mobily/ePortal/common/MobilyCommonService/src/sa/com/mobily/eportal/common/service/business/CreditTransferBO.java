package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CreditTransferDAO;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

public class CreditTransferBO {
	private static final Logger log = LoggerInterface.log;
	
	public boolean isShowIdField(String msisdn, String packageId) {
		log.debug("CreditTransferBO > isShowIdField > Called");
		OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		CreditTransferDAO creditTransferDAO = (CreditTransferDAO)daoFactory.getCreditTransferDAO();
		return creditTransferDAO.isShowIdField(msisdn, packageId);
	}
}
