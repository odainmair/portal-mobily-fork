/*
 * Created on Dec 2, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;



/**
 * @author 
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class GroupVO extends BaseVO
{
    protected String    name;
    protected String    oldName;
    protected ArrayList memberList;
    public ArrayList getMemberList()
    {
        return memberList;
    }
    public void setMemberList(ArrayList memberList)
    {
        this.memberList = memberList;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getOldName()
    {
        return oldName;
    }
    public void setOldName(String oldName)
    {
        this.oldName = oldName;
    }
}
