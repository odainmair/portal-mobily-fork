/*
 * Created on Feb 23, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.FavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQFavoriteNumberInquiryDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.FavoriteNumberInquiryVO;
import sa.com.mobily.eportal.common.service.xml.FavoriteNumberInquiryXMLHandler;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class FavoriteNumberInquiryBO {

	 private static final Logger log = LoggerInterface.log;
	 
	/**
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public FavoriteNumberInquiryVO getFavoriteNumberInquiry(FavoriteNumberInquiryVO requestVO) throws MobilyCommonException {
        
        FavoriteNumberInquiryVO replyVO = null;
        //generate XML request
        try {
            String xmlReply = "";
            FavoriteNumberInquiryXMLHandler  xmlHandler = new FavoriteNumberInquiryXMLHandler();
            String xmlMessage  = xmlHandler.generateXMLRequest(requestVO);
            
            log.info("FavoriteNumberInquiryVO >getFavoriteNumberInquiry > generate xml message for Customer ["+requestVO.getMsisdn()+"] > ");
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
         	FavoriteNumberInquiryDAO favoriteNumberInquiryDAO = (MQFavoriteNumberInquiryDAO)daoFactory.getFavoriteNumberInquiryDAO();
         	xmlReply= favoriteNumberInquiryDAO.getFavoriteNumberInquiry(xmlMessage);
            log.info("FavoriteNumberInquiryVO >getFavoriteNumberInquiry > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > ");
            
            //parsing the xml reply
            replyVO = xmlHandler.parsingFNInquiryReplyMessage(xmlReply);
        } catch (MobilyCommonException e) {
            // TODO Auto-generated catch block
            throw e;
        }
      return replyVO;    

    }
	
	
	public static void main(String[] args) {
	}
}
