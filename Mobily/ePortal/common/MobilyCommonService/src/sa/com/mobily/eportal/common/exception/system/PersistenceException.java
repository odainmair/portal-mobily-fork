package sa.com.mobily.eportal.common.exception.system;


public class PersistenceException extends MobilySystemException {

	private static final long serialVersionUID = 652049843925485741L;

	public PersistenceException() {
	}

	public PersistenceException(String message) {
		super(message);
	}

	public PersistenceException(Throwable cause) {
		super(cause);
	}

	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}
}