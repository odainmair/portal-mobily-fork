/*
 * Created on Nov 25, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.imp.ePortalDBDao;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.vo.InactiveObject;
import sa.com.mobily.eportal.common.service.vo.SessionVO;
import sa.com.mobily.eportal.common.service.vo.SkyUserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;
import sa.com.mobily.eportal.common.service.vo.UserRelatedNoVO;

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ePortalDBDaoIfc {
	
	public ArrayList getRelatedMSISDNList(String aMsisdn)throws MobilyCommonException;
	public Hashtable getArabicProfile();
	public boolean updateActivatedUserAccount(UserAccountVO objUserAccountVO);
	public UserAccountVO getUserAccountWebProfile(String msisdn);
	public boolean isWebProfileUpdated(String msisdn);
	public HashMap getChargeServiceAmount() ;
	public String fetchPOIDIdforMsisdn(String msisdn, String packType) ;
	public String fetchPOIDIdforMsisdnfromDB(String msisdn) ;
	public void updatePOIDForMsisdninDB(String msisdn, String poid);
	public void updatePOIDList(String msisdn) ;
	public String getTempActivationData(String msisdn);
	public void createTempActivationData(String aMsisdn, String aActivationCode);
	public void deleteTempActivationData(String aMsisdn);
	public  Hashtable loadLoyaltyReasonCodeFromDB();
	public int getUserAccountID(String aMsisdn)throws Exception; 
	public int getUserAccountIDFromManagedTbl (String aMsisdn)throws Exception; 
	public void updatePOIDListforRegistration(String aMsisdn, String packType) throws Exception;
	public int updatePOIDforCCM(String aMsisdn, String packType) throws Exception;
	public String getServiceAccount(String msisdn) throws SystemException;
	public  int getGloybasGroupId();
	public void UpdateUserInDR(String username , int action);
	public void setActivationCode(String accountNumber , String activationCode , int projectId) throws SystemException;
	public String getActivationCode(String accountNumber ,  int projectId) throws SystemException;
	public boolean  isWhiteListUsers(String msisdn) throws MobilyCommonException;
	public boolean isUserRelatedNoExist(UserRelatedNoVO relatedVO) throws SystemException;
	public void addingNewRelatedNumber(UserAccountVO userAccount) throws SystemException;
	public Date getLastLoginTime(String msisdn) throws SystemException;
	public void updateLastLoginTime(String msisdn) throws SystemException;
	public SkyUserAccountVO getSkyType( String aMsisdn ) throws SystemException;
	public Date getLastLoginTimeCorp(String aBillingNo, String aUsername) throws SystemException;
	public void updateLastLoginTimeCorp(String aUsername) throws SystemException;
	public void clearSessionVORow(String username) ;
	public void clearRelatedSessionVORow(String MSISDN) ;
	public void insertNewSessionVORow(SessionVO sessionVO) ;
	public void insertNewRelatedSessionVORow(SessionVO sessionVO) ;
	public SessionVO  retrieveRelatedSessionVORow(String msisdn);
	public SessionVO  retrieveOriginalSessionVORow(String msisdn);
	public void deleteActivationCode(String accountNumber ,  int projectId) throws SystemException;
	public void auditInactiveAccount(InactiveObject inactiveObject);
	public boolean isPowerUsr_UpdatedHisPassword(String userName);
	public void updatePowerUsrPassword(String userName );
	
	/**
	 * @date: Mar 8, 2012
	 * @author: s.vathsavai.mit
	 * @description: Responsible to get the list of page unique names required resetPortlet parameter.
	 * @return
	 */
	public List getResetPortletrequiredPages();

	public UserAccountVO getUserAccountInfoFromManageTable(int id, String msisdn);
	public boolean updateLineTypeInUserManagedNo(int id, String msisdn, int lineType);
	public boolean updateLineTypeInUserAccount(String msisdn, int lineType);
}
