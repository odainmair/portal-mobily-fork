package sa.com.mobily.eportal.common.service.xml;

import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.ConnectOTSVO;



/**
 * @author r.agarwal.mit
 * 
 * WiMaxOffShelfXMLHandler
 * Description: This class contains the utility methods for xml request generation and xml response parsing
 */
public class ConnectOTSXMLHandler {

    private static final Logger log = LoggerInterface.log;
    
	
	/**
	 * Request xml for voucher re-charge
	 * 
	 * @param connectOTSVO
	 * @return
	 */
    public static String generateVoucherPymtRequest(ConnectOTSVO connectOTSVO) {
    	
    	log.debug("ConnectOTSXMLHandler > generateVoucherPymtRequest > called");  
		String xmlRequest = "";	
		try {
			Document doc = new DocumentImpl();

			String operation = ConstantIfc.CONNECT_VOUCHER_RECHARGE;
			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
			String srdate = FormatterUtility.FormateDate(new Date());
			
	        //gernerate Header element
			Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_HEADER);
			
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.ACC_BAL_ADPTR_FUNC_ID);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,ConstantIfc.SECURITY_KEY);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION,ConstantIfc.MSG_VERSION);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.REQUESTOR_CHANNEL_ID);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_CHANNEL_TRANS_ID,ConstantIfc.Requestor_ChannelId+"_"+srdate);

			XMLUtility.closeParentTag(root, header);

			//body
				if(!"".equals(FormatterUtility.checkNull(connectOTSVO.getOperation()))){
					operation = FormatterUtility.checkNull(connectOTSVO.getOperation()).trim();
				}
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_OPERATION, operation);
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_MSISDN, connectOTSVO.getMsisdn());
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_VOUCHER_NUMBER, connectOTSVO.getVoucherNumber());
			
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
		} catch (java.io.IOException e) {
            log.fatal("ConnectOTSXMLHandler > generateVoucherPymtRequest > IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            log.fatal("ConnectOTSXMLHandler > generateVoucherPymtRequest > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
		return xmlRequest;
    }
    
    /**
     * Reply xml for voucher recharge
     * @param xmlReply
     * @return
     */
	public static ConnectOTSVO parseVoucherPymtReply(String xmlReply) {
		
		log.debug("ConnectOTSXMLHandler > parseVoucherPymtReply :: start");
		
		ConnectOTSVO connectOTSVO = new ConnectOTSVO();
	   	try {
			if (xmlReply == null || xmlReply == "")
					throw new SystemException();

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_SR_HEADER)) {
					// Do Nothing
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)){
					connectOTSVO.setErrorCode(l_szNodeValue);
				} 
			}
		} catch (Exception e) {
			log.fatal("ConnectOTSXMLHandler > parseVoucherPymtReply :: Exception :: "	+ e.getMessage());
			throw new SystemException(e);
		}
		
		log.debug("ConnectOTSXMLHandler > parseVoucherPymtReply :: end");
		return connectOTSVO;
	
	}
	
	/**
	 * Used to generate xml request to get the msisdn for a sim number
	 * @param connectOTSVO
	 * @return
	 */
    public static String generateGetMsisdnRequest(ConnectOTSVO connectOTSVO) {
    	
    	log.debug("ConnectOTSXMLHandler > generateGetMsisdnRequest > called");  
		String xmlRequest = "";	
		try {
			Document doc = new DocumentImpl();

			Element root = doc.createElement(TagIfc.TAG_CR_REQUEST_MESSAGE);
			String srdate = FormatterUtility.FormateDate(new Date());
			
	        //gernerate Header element
			Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_CR_HEADER);
			
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.GENERAL_INQUIRY_FUNC_ID);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION,ConstantIfc.MSG_VERSION);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.REQUESTOR_CHANNEL_ID);
			XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_SR_ID,"SR_"+srdate);
			XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,"");
			XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,"");
			
			XMLUtility.closeParentTag(root, header);

			//body
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_INQUIRY_TYPE, ConstantIfc.INQUIRY_TYPE_MSISDN_BY_SIM);
			
			//gernerate Params element
			Element params = XMLUtility.openParentTag(doc, TagIfc.TAG_PARAMS);
	    	//for(int i=0; i <requestVO.getDistributionVOs().size(); i++ ){
	    		Element paramElement = XMLUtility.openParentTag(doc, TagIfc.TAG_PARAM);
	    		XMLUtility.generateElelemt(doc, paramElement, TagIfc.TAG_KEY, ConstantIfc.KEY_SIM);
	    		XMLUtility.generateElelemt(doc, paramElement, TagIfc.TAG_VALUE, connectOTSVO.getSimNumber());
	    		XMLUtility.closeParentTag(params, paramElement);
	    	//}			    		
	    	XMLUtility.closeParentTag(root, params);
			
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
		} catch (java.io.IOException e) {
            log.fatal("ConnectOTSXMLHandler > generateGetMsisdnRequest > IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            log.fatal("ConnectOTSXMLHandler > generateGetMsisdnRequest > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
		return xmlRequest;
    }
    
	public static ConnectOTSVO parseGetMsisdnReply(String xmlReply) {
		
		log.debug("ConnectOTSXMLHandler > parseGetMsisdnReply :: start");
		
		ConnectOTSVO connectOTSVO = new ConnectOTSVO();
	   	try {
			if (xmlReply == null || xmlReply == "")
					throw new SystemException();

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_CR_REPLY_MESSAGE).item(0);

			NodeList nl = rootNode.getChildNodes();

			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CR_HEADER)) {
					// Do Nothing
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)){
					connectOTSVO.setErrorCode(l_szNodeValue);
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG)){
					connectOTSVO.setErrorMessage(l_szNodeValue);
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_RESULTS)){
					NodeList paramsNodeList = l_Node.getChildNodes();	
					for(int i=0; paramsNodeList != null && i< paramsNodeList.getLength(); i++){
						Element s_Node = (Element) paramsNodeList.item(i);
						String s_szNodeTagName = s_Node.getTagName();
						if (s_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_RESULT)) {
							NodeList resultNodeList = s_Node.getChildNodes();				
							for (int k = 0; k < resultNodeList.getLength(); k++) {					
								if ((resultNodeList.item(k)).getNodeType() != Node.ELEMENT_NODE){
									continue;
								}
								Element c_Node = (Element) resultNodeList.item(k);
								String p_szTagName1 = c_Node.getTagName();
								String l_szNodeValue1 = null;
								if (c_Node.getFirstChild() != null){
									l_szNodeValue1 = c_Node.getFirstChild().getNodeValue();
								}
								if (p_szTagName1.equalsIgnoreCase(TagIfc.TAG_KEY)) {						
									log.debug("ConnectOTSXMLHandler > parseGetMsisdnReply > key >"+l_szNodeValue1);					
								} else if (p_szTagName1.equalsIgnoreCase(TagIfc.TAG_VALUE)) {	
									connectOTSVO.setMsisdn(l_szNodeValue1);						
								} 
							}
						}
					}
				} 
			}
		} catch (Exception e) {
			log.fatal("ConnectOTSXMLHandler > parseGetMsisdnReply :: Exception :: "	+ e.getMessage());
			throw new SystemException(e);
		}
		
		log.debug("ConnectOTSXMLHandler > parseGetMsisdnReply :: end");
		return connectOTSVO;
	}
	
	public static void main(String ar[]) {
		//System.out.println(WiMaxOffShelfXMLHandler.generatePoolInquiryRequest(new LoyaltyPoolRequestVO()));
		
		//String xmlReply = "<?xml version='1.0'?><MOBILY_EPORTAL_SR_REPLY><SR_HEADER_REPLY><FuncId>LOYALTY_ACCOUNT_POOL_INQUIRY</FuncId><SecurityKey>sedasfsdfwertwsfgwer</SecurityKey><MsgVersion>0000</MsgVersion><RequestorChannelId>EPORTAL</RequestorChannelId><ServiceRequestId>SR_20100113121620</ServiceRequestId><SrDate>20100113121620</SrDate><SrStatus>2</SrStatus></SR_HEADER_REPLY><ReturnCode><ErrorCode>10319</ErrorCode></ReturnCode><JOINT_POOL_INFO><CustomerId>2423546546</CustomerId><MasterLine></MasterLine><SecondaryLine></SecondaryLine></JOINT_POOL_INFO></MOBILY_EPORTAL_SR_REPLY>";
		/*LoyaltyPoolRequestVO poolRequestVO = new LoyaltyPoolRequestVO();
		poolRequestVO.setCustomerId("123");
		poolRequestVO.setMasterLine("456");
		poolRequestVO.setSecondaryLine("789");
		poolRequestVO.setLineNumber(poolRequestVO.getSecondaryLine());//for confirmation SMS
		poolRequestVO.setFuncId(ConstantIfc.FUNCTION_ID_LOYALTY_ACCOUNT_DELETE_POOL);
		poolRequestVO.setActionCode(ConstantIfc.LOYALTY_ACCOUNT_POOL);
		String xmlRequest = WiMaxOffShelfXMLHandler.generateRemoveAccountPoolRequest(poolRequestVO);
		System.out.println("xmlRequest="+xmlRequest);
		//WiMaxOffShelfXMLHandler.parseJointPoolReply(xmlReply);*/
	}
	
}
