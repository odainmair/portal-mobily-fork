package sa.com.mobily.eportal.core.api;

import java.io.Serializable;

public class BackEndServiceError implements Serializable {

	private static final long serialVersionUID = -5611719303420901741L;

	private long serviceId;

	private long errorCode;

	private String errorMsgEn;

	private String errorMsgAr;

	public long getServiceId() {
		return serviceId;
	}

	public void setServiceId(long serviceId) {
		this.serviceId = serviceId;
	}

	public long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsgEn() {
		return errorMsgEn;
	}

	public void setErrorMsgEn(String errorMsgEn) {
		this.errorMsgEn = errorMsgEn;
	}

	public String getErrorMsgAr() {
		return errorMsgAr;
	}

	public void setErrorMsgAr(String errorMsgAr) {
		this.errorMsgAr = errorMsgAr;
	}
}