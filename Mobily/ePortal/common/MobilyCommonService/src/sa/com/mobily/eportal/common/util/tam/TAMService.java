/*
 * Created on April 05, 2013
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.util.tam;

import java.util.logging.Level;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.constants.MobilyConstants;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.exception.ExceptionUtil;
import sa.com.mobily.eportal.common.service.ldapservice.MobilyUser;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

import com.tivoli.pd.jadmin.PDAdmin;
import com.tivoli.pd.jadmin.PDUser;
import com.tivoli.pd.jutil.PDContext;
import com.tivoli.pd.jutil.PDMessages;
import com.tivoli.pd.jutil.PDRgyUserName;

/**
 * @author r.agarwal.mit
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class TAMService
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static final String className = TAMService.class.getName();
	
	private static TAMService instance;
	
	private TAMService(){}
	
	public static TAMService getInstance(){
		if(instance == null)
			instance = new TAMService();
		return instance;
	}
	/**
	 * 
	 * @param ePortalUser
	 * @throws Exception
	 */
	public void createNewUserOnTAM(@NotNull MobilyUser ePortalUser)
	{
		executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM > Remove the TAM reference based on mobily feedback",className, "createNewUserOnTAM", null);
//		String method = "createNewUserOnTAM";
//		PDContext tamContext = TAMCachedContext.getInstance().getTamContext();
//		
//		executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM > tamContext::" + tamContext, className, method, null);
//		String LDAP_SUFFIX = AppConfig.getInstance().get(MobilyConstants.LDAP_SUFFIX);
//		PDMessages tamMessages = new PDMessages();
//		String userID = FormatterUtility.checkNull(ePortalUser.getUid()).trim().toLowerCase();
//		executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM > userID =["+ userID+"]", className, method, null);
//		
//		String rgyName = null;
//		rgyName = "uid=" + userID + "," + LDAP_SUFFIX;
//		
//		executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM > rgyName:: " + rgyName, className, method, null);
//		PDRgyUserName rgyUserName = new PDRgyUserName(rgyName, userID, userID);
//
//		boolean ssoUser = true;
//		try
//		{
//			PDAdmin.initialize("Create TAM User", tamMessages);
//			PDUser.importUser(tamContext, userID, rgyUserName, null, ssoUser, tamMessages);
//			PDUser.setAccountValid(tamContext, userID, true, tamMessages);
//			PDUser.setPasswordValid(tamContext, userID, true, tamMessages);
//			
//			executionContext.log(Level.INFO, "TAM User created!", className, method, null);
//			PDAdmin.shutdown(tamMessages);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
//			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
//			throw se;
//		}
	}
	
	/**
	 * Creating the MyConnect User in TAM
	 * @param ePortalUser
	 * @param LDAP_SUFFIX
	 * @throws Exception
	 */
	public void createNewUserOnTAM(@NotNull MobilyUser ePortalUser, String LDAP_SUFFIX )
	{
		executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM with LDAP Suffix> Remove the TAM reference based on mobily feedback",className, "createNewUserOnTAM", null);
//		String method = "createNewUserOnTAM";
//		PDContext tamContext = TAMCachedContext.getInstance().getTamContext();
//		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "TAMService > createNewUserOnTAM() with LDAP Suffix> tamContext::" + tamContext, className, method, null);
//		executionContext.log(logEntryVO);
//		//String LDAP_SUFFIX = AppConfig.getInstance().get(MobilyConstants.LDAP_SUFFIX);
//		PDMessages tamMessages = new PDMessages();
//		String userID = FormatterUtility.checkNull(ePortalUser.getUid()).trim().toLowerCase();
//		String rgyName = null;
//		rgyName = "uid=" + userID + "," + LDAP_SUFFIX;
//		logEntryVO = new LogEntryVO(Level.INFO, "TAMService > createNewUserOnTAM() with LDAP Suffix > rgyName:: " + rgyName, className, method, null);
//		executionContext.log(logEntryVO);
//		PDRgyUserName rgyUserName = new PDRgyUserName(rgyName, userID, userID);
//
//		boolean ssoUser = true;
//		try
//		{
//			PDAdmin.initialize("Create TAM User for MyConnect", tamMessages);
//			PDUser.importUser(tamContext, userID, rgyUserName, null, ssoUser, tamMessages);
//			PDUser.setAccountValid(tamContext, userID, true, tamMessages);
//			PDUser.setPasswordValid(tamContext, userID, true, tamMessages);
//			logEntryVO = new LogEntryVO(Level.INFO, "TAM User created for MyConnect!", className, method, null);
//			executionContext.log(logEntryVO);
//			PDAdmin.shutdown(tamMessages);
//		}
//		catch (Exception e)
//		{
//			logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
//			executionContext.log(logEntryVO);
//			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
//			throw se;
//		}
	}

	
	/**
	 * 
	 * @param userName
	 * @param newPassword
	 * @throws Exception
	 */
	public void updatePasswordOnTAM(String userName, String newPassword)
	{
		executionContext.log(Level.INFO, "TAMService > updatePasswordOnTAM >  Remove the TAM reference based on mobily feedback",className, "updatePasswordOnTAM", null);
//		String method = "updatePasswordOnTAM";
//		executionContext.log(Level.INFO, "TAMService > updatePasswordOnTAM > started for username::" + userName, className, method, null);
//		PDContext tamContext = TAMCachedContext.getInstance().getTamContext();
//		PDMessages tamMessages = new PDMessages();
//		try
//		{
//			PDAdmin.initialize("reset password", tamMessages);
//			PDUser.setPassword(tamContext, userName, FormatterUtility.checkNull(newPassword).trim().toCharArray(), tamMessages);
//			PDUser.setPasswordValid(tamContext, userName, true, tamMessages);
//			PDAdmin.shutdown(tamMessages);
//		}
//		catch (Exception e)
//		{
//			executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
//			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
//			throw se;
//		}
	}

	/**
	 * 
	 * @param user
	 * @throws Exception
	 */
	public void deleteTamUser(String userName)
	{
		executionContext.log(Level.INFO, "TAMService > deleteTamUser >  Remove the TAM reference based on mobily feedback",className, "deleteTamUser", null);
//		String method = "updatePasswordOnTAM";
//		try
//		{
//			PDContext tamContext = TAMCachedContext.getInstance().getTamContext();
//			PDMessages tamMessages = new PDMessages();
//			PDAdmin.initialize("delete TAM user", tamMessages);
//			PDUser.deleteUser(tamContext, userName, true, tamMessages);
//			LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "TAMService > deleteTamUser > userName::" + userName, className, method, null);
//			executionContext.log(logEntryVO);
//			PDAdmin.shutdown(tamMessages);
//		}
//		catch (Exception e)
//		{
//			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
//			executionContext.log(logEntryVO);
//			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
//			throw se;
//		}
	}
	
	// to get user account is valid or invalid
	public boolean isValidAccount(String name)
	{
		executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM > Remove the TAM reference based on mobily feedback",className, "createNewUserOnTAM", null);
		return false;
//		boolean valid = false;
//
//		try
//		{
//			// Initialize administration API Context
//			PDContext tamContext = TAMCachedContext.getInstance().getTamContext();
//			PDMessages msgs = new PDMessages();
//			PDAdmin.initialize("inquiry user", msgs);
//			PDUser pdUser = new PDUser(tamContext, name, msgs);
//			valid = pdUser.isAccountValid();
//			executionContext.log(Level.INFO, "TAMService > createNewUserOnTAM > isAccountValid:: " + valid, className, "isAccountValid", null);
//
//			PDAdmin.shutdown(msgs);
//		}
//		catch (Exception ex)
//		{
//			executionContext.severe("Error Occured in getValidAccount" + ex.getMessage());
//		}
//		return valid;
	}
}