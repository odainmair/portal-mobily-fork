package sa.com.mobily.eportal.billing.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author g.krishnamurthy.mit
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class FreeBalancesRequestVO extends BaseVO
{
	private String msisdn = "";

	private String customerType = "";

	private boolean isRG = true;
	
	private String reqUserName = "";

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public String getCustomerType()
	{
		return customerType;
	}

	public void setCustomerType(String customerType)
	{
		this.customerType = customerType;
	}

	public boolean isRG()
	{
		return isRG;
	}

	public void setRG(boolean isRG)
	{
		this.isRG = isRG;
	}

	/**
	 * @return the reqUserName
	 */
	public String getReqUserName()
	{
		return reqUserName;
	}

	/**
	 * @param reqUserName the reqUserName to set
	 */
	public void setReqUserName(String reqUserName)
	{
		this.reqUserName = reqUserName;
	}
	
}

