//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.ibm.websphere.cache.DistributedMap;
import com.ibm.websphere.cache.EntryInfo;
import sa.com.mobily.eportal.core.api.Context;

/**
 * This class is used to store value objects on the WebSphere Dynacache. The
 * value objects stored are identified by their URIs. The cache provides
 * time-based expiration with default expiration of 24 hours.
 */
public class CacheManager
{

	private static final String CACHE_PROPERTIES = "/WEB-INF/cache.properties";

	private static final String RESOURCE_CACHE_MAP = "cache/object/mobily_eportal";

	private static final String DEFAULT_RESOURCE_CACHE_TIMEOUT_PROPERTY = "cache.resource.timeout.default";

	private DistributedMap fResourceCacheMap;

	private int fDefaultResourceCacheTimeout = 86400;// 24 hours

	private Map<String, Integer> fResourcesTimeouts = new HashMap<String, Integer>();

	public CacheManager()
	{
		initResourceCache();
	}

	/**
	 * Invalidates a resource object from the cache.
	 * 
	 * @param ctx
	 *            an implementation of the context interface that provides the
	 *            necessary information about the current user request
	 * @param uri
	 *            the unique identification of the resource to be ivalidated
	 * @param global
	 *            true if this is cross-user (global) resource object, and false
	 *            if it is user-specific (not shared)
	 */
	public synchronized void invalidate(Context ctx, String uri, boolean global)
	{
		if (fResourceCacheMap != null)
		{
			if (global)
			{
				fResourceCacheMap.invalidate(uri);
			}
			else
			{
				if (ctx != null && ctx.getCurrentUser() != null)
				{
					fResourceCacheMap.invalidate(uri + ctx.getCurrentUser().getDefaultMSISDN());
				}
			}
		}
	}

	/**
	 * Puts a resource object in the cache.
	 * 
	 * @param ctx
	 *            an implementation of the context interface that provides the
	 *            necessary information about the current user request
	 * @param uri
	 *            the unique identification of the resource to be added to the
	 *            cache
	 * @param global
	 *            true if this is cross-user (global) resource object, and false
	 *            if it is user-specific (not shared)
	 * @param resource
	 *            the object to be added in the cache
	 */
	public void add(Context ctx, String uri, boolean global, Object resource)
	{
		if (fResourceCacheMap != null && resource != null)
		{
			int resourceTimeout = fDefaultResourceCacheTimeout;
			String cacheKey = uri;

			if (!global)
			{
				cacheKey = uri + ctx.getCurrentUser().getDefaultMSISDN();
			}

			if (fResourcesTimeouts.get(uri) != null)
			{
				resourceTimeout = ((Integer) fResourcesTimeouts.get(uri)).intValue();
			}

			fResourceCacheMap.put(cacheKey, resource, 1, resourceTimeout, EntryInfo.SHARED_PUSH, null);
		}
	}

	/*
	 * public void add(Context ctx, String uri, boolean global, Object resource,
	 * boolean serializeInByteArray) { if (resource != null){ if
	 * (serializeInByteArray == true) { try { byte[] bytes =
	 * Serializer.serialize(resource);
	 * 
	 * add(ctx, uri, global, bytes); } catch (IOException e) { throw new
	 * RuntimeException(e); } } else { add(ctx, uri, global, resource); } } }
	 */

	/**
	 * Retrieves a resource object from the cache.
	 * 
	 * @param ctx
	 *            an implementation of the context interface that provides the
	 *            necessary information about the current user request
	 * @param uri
	 *            the unique identification of the resource to be retrieved from
	 *            the cache
	 * @param global
	 *            true if this is cross-user (global) resource object, and false
	 *            if it is user-specific (not shared)
	 * @return resource the object to be retrieved from the cache
	 */
	public synchronized Object get(Context ctx, String uri, boolean global)
	{
		if (fResourceCacheMap != null)
		{
			String cacheKey = uri;

			if (!global)
			{
				cacheKey = uri + ctx.getCurrentUser().getDefaultMSISDN();
			}

			return fResourceCacheMap.get(cacheKey);
		}
		else
		{
			return null;
		}
	}

	/*
	 * public synchronized Object get(Context ctx, String uri, boolean global,
	 * boolean deserializeAsByteArray) { Object result = get(ctx, uri, global);
	 * 
	 * if (result != null && deserializeAsByteArray) { try { return
	 * Serializer.deserialize((byte[]) result); } catch (IOException e) { throw
	 * new RuntimeException(e); } catch (ClassNotFoundException e) { throw new
	 * RuntimeException(e); } }
	 * 
	 * return result; }
	 */

	private void initResourceCache()
	{
		// init the object cache map
		InputStream cacheInputStream = null;
		try
		{
			// 1. lookup the cache instance
			InitialContext ic = new InitialContext();
			fResourceCacheMap = (DistributedMap) ic.lookup(RESOURCE_CACHE_MAP);

			// 2. load resources cache timeouts
			cacheInputStream = getResourceAsStream(CACHE_PROPERTIES);
			if (cacheInputStream != null)
			{
				Properties properties = new Properties();
				properties.load(cacheInputStream);

				fDefaultResourceCacheTimeout = Integer.parseInt(properties.getProperty(DEFAULT_RESOURCE_CACHE_TIMEOUT_PROPERTY));

				// load the timeout of the resources specifically
				for (Iterator<Object> iter = properties.keySet().iterator(); iter.hasNext();)
				{
					String key = (String) iter.next();
					fResourcesTimeouts.put(key, new Integer((String) properties.get(key)));
				}
			}
		}
		catch (IOException e)
		{
			// Fail safe in case the properties file is not found
		}
		catch (NamingException e)
		{
			// the cache instance is not found
			// System.out.println("Object cache map " + RESOURCE_CACHE_MAP +
			// " not found. Resource caching will be disabled.");

		}
		finally
		{
			try
			{
				if (cacheInputStream != null)
					cacheInputStream.close();
			}
			catch (IOException e)
			{
			}
		}

	}

	private static InputStream getResourceAsStream(String webResource)
	{
		InputStream in = CacheManager.class.getResourceAsStream(webResource);
		if (in == null)
		{
			String webResource2 = "/../.." + webResource;
			in = CacheManager.class.getResourceAsStream(webResource2);
		}
		return in;
	}
}