/*
 * Created on Jan 05, 2012
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.List;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class LOVInquiryVO {
	
	private List lovInfoVOList = null;
	private String totalRecordsCount = "0";
	private String statusCode = "";

	public List getLovInfoVOList() {
		return lovInfoVOList;
	}

	public void setLovInfoVOList(List lovInfoVOList) {
		this.lovInfoVOList = lovInfoVOList;
	}

	public String getTotalRecordsCount() {
		return totalRecordsCount;
	}

	public void setTotalRecordsCount(String totalRecordsCount) {
		this.totalRecordsCount = totalRecordsCount;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
}
