package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.MobilyCatalogLookUp;

public class MobilyCatalogLookUpDAO extends AbstractDBDAO<MobilyCatalogLookUp>
{
	private static MobilyCatalogLookUpDAO instance = new MobilyCatalogLookUpDAO();

	private static final String GET_ALL_SERVICES = "SELECT * FROM MOBILY_CATALOG_LOOKUP_TBL WHERE TYPE = 'LOB'";

	private static final String GET_ALL_SERVICES_BY_TYPE = "SELECT * FROM MOBILY_CATALOG_LOOKUP_TBL WHERE TYPE = ?";
	
	protected MobilyCatalogLookUpDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static MobilyCatalogLookUpDAO getInstance()
	{
		return instance;
	}

	public List<MobilyCatalogLookUp> getServices()
	{
		List<MobilyCatalogLookUp> lstLookUp = query(GET_ALL_SERVICES);
		if (CollectionUtils.isNotEmpty(lstLookUp))
		{
			return lstLookUp;
		}
		return null;
	}
	
	public List<MobilyCatalogLookUp> getServicesByType(String type)
	{
		List<MobilyCatalogLookUp> lstLookUp = query(GET_ALL_SERVICES_BY_TYPE, type);
		if (CollectionUtils.isNotEmpty(lstLookUp))
		{
			return lstLookUp;
		}
		return null;
	}

	@Override
	protected MobilyCatalogLookUp mapDTO(ResultSet rs) throws SQLException
	{
		MobilyCatalogLookUp mobilyCatalogLookUp = new MobilyCatalogLookUp();
		mobilyCatalogLookUp.setARABIC_NAME(rs.getString("ARABIC_NAME"));
		mobilyCatalogLookUp.setCODE(rs.getString("CODE"));
		mobilyCatalogLookUp.setENGLISH_NAME(rs.getString("ENGLISH_NAME"));
		mobilyCatalogLookUp.setPARENT_ID(rs.getInt("PARENT_ID"));
		mobilyCatalogLookUp.setSHORT_NAME(rs.getString("SHORT_NAME"));
		mobilyCatalogLookUp.setTYPE(rs.getString("TYPE"));
		mobilyCatalogLookUp.setUPDATE_DATE(rs.getDate("UPDATE_DATE"));
		return mobilyCatalogLookUp;
	}
}
