package sa.com.mobily.eportal.common.service.vo;

import java.util.HashMap;

/**
 * 
 * @author n.gundluru.mit
 *
 */
public class MobilyAuthNGoReplyVO {

	private String functionId;
	private String sourceMSISDN;
	private String targetMSISDN;
	private String pinNumber;
	private String status;
	private String srId;
	private String errorCode = "";
	private String errorMessage = "";
	private String replyMessage = "";
	private HashMap callDivertInqMap = null;
	private boolean isWhiteListUser = false;

	
	/**
	 * @return the functionId
	 */
	public String getFunctionId() {
		return functionId;
	}
	/**
	 * @param functionId the functionId to set
	 */
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	/**
	 * @return the sourceMSISDN
	 */
	public String getSourceMSISDN() {
		return sourceMSISDN;
	}
	/**
	 * @param sourceMSISDN the sourceMSISDN to set
	 */
	public void setSourceMSISDN(String sourceMSISDN) {
		this.sourceMSISDN = sourceMSISDN;
	}
	/**
	 * @return the targetMSISDN
	 */
	public String getTargetMSISDN() {
		return targetMSISDN;
	}
	/**
	 * @param targetMSISDN the targetMSISDN to set
	 */
	public void setTargetMSISDN(String targetMSISDN) {
		this.targetMSISDN = targetMSISDN;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return the pinNumber
	 */
	public String getPinNumber() {
		return pinNumber;
	}
	/**
	 * @param pinNumber the pinNumber to set
	 */
	public void setPinNumber(String pinNumber) {
		this.pinNumber = pinNumber;
	}
	/**
	 * @return the srId
	 */
	public String getSrId() {
		return srId;
	}
	/**
	 * @param srId the srId to set
	 */
	public void setSrId(String srId) {
		this.srId = srId;
	}
	/**
	 * @return the replyMessage
	 */
	public String getReplyMessage() {
		return replyMessage;
	}
	/**
	 * @param replyMessage the replyMessage to set
	 */
	public void setReplyMessage(String replyMessage) {
		this.replyMessage = replyMessage;
	}
	/**
	 * @return the callDivertInqMap
	 */
	public HashMap getCallDivertInqMap() {
		return callDivertInqMap;
	}
	/**
	 * @param callDivertInqMap the callDivertInqMap to set
	 */
	public void setCallDivertInqMap(HashMap callDivertInqMap) {
		this.callDivertInqMap = callDivertInqMap;
	}
	/**
	 * @return the isWhiteListUser
	 */
	public boolean isWhiteListUser() {
		return isWhiteListUser;
	}
	/**
	 * @param isWhiteListUser the isWhiteListUser to set
	 */
	public void setWhiteListUser(boolean isWhiteListUser) {
		this.isWhiteListUser = isWhiteListUser;
	}
	
	
}