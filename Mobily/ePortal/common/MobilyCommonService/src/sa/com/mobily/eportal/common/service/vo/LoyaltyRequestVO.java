/**
 * @date: Dec 28, 2011
 * @author: s.vathsavai.mit
 * @file name: LoyaltyRequestVO.java
 * @description:  
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author s.vathsavai.mit
 *
 */
public class LoyaltyRequestVO {

	private String billingAccountNumber = null;
	private String funcId = null;
	private String userId = null;
	private String language = null;
	/**
	 * @return the billingAccountNumber
	 */
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}
	/**
	 * @param billingAccountNumber the billingAccountNumber to set
	 */
	public void setBillingAccountNumber(String billingAccountNumber) {
		this.billingAccountNumber = billingAccountNumber;
	}
	/**
	 * @return the funcId
	 */
	public String getFuncId() {
		return funcId;
	}
	/**
	 * @param funcId the funcId to set
	 */
	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the language
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language the language to set
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	
	
}
