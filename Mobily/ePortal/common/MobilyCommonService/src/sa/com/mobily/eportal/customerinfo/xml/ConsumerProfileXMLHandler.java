package sa.com.mobily.eportal.customerinfo.xml;

import java.io.StringReader;

import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.CustomerServiceUtil;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.constant.TagIfc;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileReplyVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileRequestVO;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerRelatedNumbersReplyVO;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class ConsumerProfileXMLHandler implements TagIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);

	private static final String className = CustomerInfoXMLHandler.class.getName();

	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Consumer
	 *               Profile
	 * @param requestVO
	 * @return xmlRequest
	 */

	public String generateConsumerProfileXML(ConsumerProfileRequestVO requestVO)
	{
		String method = "generateConsumerProfileXML";
		String xmlRequest = "";

		if (requestVO == null)
		{
			// log.fatal("CustomerProfileXMLHandler > generateCustomerProfileXML : ActiveLinesInquiryVO is null.");
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}

		executionContext.audit(Level.INFO, "CustomerProfileXMLHandler > generateConsumerProfileXML : started for account: " + requestVO.getKeyValue(), className, method);

//		final ByteOutputStream outputStream = new ByteOutputStream();
//		XMLStreamWriter xmlStreamWriter = null;
		try
		{
//			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
//			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
//
//			JAXBContext jaxbContext = JAXBContext.newInstance(ConsumerProfileRequestVO.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//			jaxbMarshaller.marshal(requestVO, xmlStreamWriter);
//			outputStream.flush();

//			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();
			
			xmlRequest = JAXBUtilities.getInstance().marshal(requestVO);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while generating customer profile xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
//			if (xmlStreamWriter != null)
//			{
//				try
//				{
//					xmlStreamWriter.close();
//				}
//				catch (XMLStreamException e)
//				{
//					executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
//				}
//			}
//			if (outputStream != null)
//			{
//				outputStream.close();
//			}
		}
		return xmlRequest;
	}

	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Consumer profile XML
	 * @param replyMessage
	 * @return CustomerInfoVO
	 */
	public ConsumerProfileReplyVO parseConsumerProfileXML(String replyMessage)
	{
		String method = "parseConsumerProfileXML";
		ConsumerProfileReplyVO replyVO = null;
		try
		{
//			JAXBContext jc = JAXBContext.newInstance(ConsumerProfileReplyVO.class);
//			Unmarshaller unmarshaller = jc.createUnmarshaller();
//			XMLInputFactory xmlif = XMLInputFactory.newInstance();

			// for testing -- START
			// FileReader fr = new
			// FileReader("G:\\Portal 8\\ConsumerReply.xml");
			// XMLStreamReader xmler = xmlif.createXMLStreamReader(fr);

			// for Testing -- END

//			XMLStreamReader xmler = xmlif.createXMLStreamReader(new StringReader(replyMessage));
//			System.out.println("replyMessage == "+replyMessage);
//			replyVO = (ConsumerProfileReplyVO) unmarshaller.unmarshal(xmler);

			replyVO = (ConsumerProfileReplyVO) JAXBUtilities.getInstance().unmarshal(ConsumerProfileReplyVO.class, replyMessage);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing customer info xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		return replyVO;
	}
	
	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the related numbers reply XML
	 * @param replyMessage
	 * @return ConsumerRelatedNumbersReplyVO
	 */
	public ConsumerRelatedNumbersReplyVO parseRelatedNumberXML(String replyMessage)
	{
		String method = "parseRelatedNumberXML";
		ConsumerRelatedNumbersReplyVO replyVO = null;
		try
		{
//			JAXBContext jc = JAXBContext.newInstance(ConsumerRelatedNumbersReplyVO.class);
//			Unmarshaller unmarshaller = jc.createUnmarshaller();
//			XMLInputFactory xmlif = XMLInputFactory.newInstance();

			// for testing -- START
			// FileReader fr = new
			// FileReader("G:\\Portal 8\\ConsumerReply.xml");
			// XMLStreamReader xmler = xmlif.createXMLStreamReader(fr);

			// for Testing -- END

//			XMLStreamReader xmler = xmlif.createXMLStreamReader(new StringReader(replyMessage));
//			replyVO = (ConsumerRelatedNumbersReplyVO) unmarshaller.unmarshal(xmler);
			
			replyVO = (ConsumerRelatedNumbersReplyVO) JAXBUtilities.getInstance().unmarshal(ConsumerRelatedNumbersReplyVO.class, replyMessage);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing related numbers xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		return replyVO;
	}

	public static void main(String agrs[])
	{
		ConsumerProfileRequestVO requestVO = new ConsumerProfileRequestVO();
		requestVO.setKeyType(ConstantsIfc.KEY_TYPE_MSISDN);
		requestVO.setKeyValue("966540510111");

		String userId = "rupesh";

		// Populate Header
		ConsumerRequestHeaderVO headerVO = new CustomerServiceUtil().populateRequestHeaderVO(ConstantsIfc.FUN_GET_CONSUMER_LINE_INFO, userId);

		requestVO.setHeaderVO(headerVO);

		ConsumerProfileXMLHandler handler = new ConsumerProfileXMLHandler();
		try
		{
			String requestXml = handler.generateConsumerProfileXML(requestVO);
//			System.out.println("requestXml::" + requestXml);

			ConsumerProfileReplyVO replyVO = handler.parseConsumerProfileXML(requestXml);
			if (replyVO != null)
			{
//				System.out.println("msg format=" + replyVO.getHeaderVO().getMsgFormat());
//				System.out.println("status=" + replyVO.getHeaderVO().getStatus());
//				System.out.println("msisdn=" + replyVO.getMsisdn());
//				System.out.println("billing account number=" + replyVO.getBillingAccountNumber());
			}

		}
		catch (Exception e)
		{
//			System.out.println("here=" + e.getMessage());
//			e.printStackTrace();
		}
	}
}