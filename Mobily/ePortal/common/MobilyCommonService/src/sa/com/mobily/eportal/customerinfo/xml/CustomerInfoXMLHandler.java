package sa.com.mobily.eportal.customerinfo.xml;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.constant.TagIfc;
import sa.com.mobily.eportal.customerinfo.jaxb.object.Agreement;
import sa.com.mobily.eportal.customerinfo.jaxb.object.AgreementItem;
import sa.com.mobily.eportal.customerinfo.jaxb.object.Characteristic;
import sa.com.mobily.eportal.customerinfo.jaxb.object.CharacteristicValue;
import sa.com.mobily.eportal.customerinfo.jaxb.object.ContactMedium;
import sa.com.mobily.eportal.customerinfo.jaxb.object.Customer;
import sa.com.mobily.eportal.customerinfo.jaxb.object.CustomerAccountId;
import sa.com.mobily.eportal.customerinfo.jaxb.object.CustomerConfiguration;
import sa.com.mobily.eportal.customerinfo.jaxb.object.Individual;
import sa.com.mobily.eportal.customerinfo.jaxb.object.Party;
import sa.com.mobily.eportal.customerinfo.jaxb.object.PartyChoice;
import sa.com.mobily.eportal.customerinfo.jaxb.object.PartyExtensions;
import sa.com.mobily.eportal.customerinfo.jaxb.object.PartyIdentification;
import sa.com.mobily.eportal.customerinfo.jaxb.object.RetrieveCustomerProductConfiguration;
import sa.com.mobily.eportal.customerinfo.jaxb.object.RetrieveCustomerProductConfiguration.Body;
import sa.com.mobily.eportal.customerinfo.jaxb.object.RetrieveCustomerProductConfiguration.Body.SourceCriteria;
import sa.com.mobily.eportal.customerinfo.jaxb.object.RetrieveCustomerProductConfiguration.MsgRqHdr;
import sa.com.mobily.eportal.customerinfo.jaxb.object.RetrieveCustomerProductConfigurationRs;
import sa.com.mobily.eportal.customerinfo.jaxb.object.Specification;
import sa.com.mobily.eportal.customerinfo.jaxb.object.targetCriteria;
import sa.com.mobily.eportal.customerinfo.vo.CustomerInfoVO;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class CustomerInfoXMLHandler implements TagIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);

	private static final String className = CustomerInfoXMLHandler.class.getName();

	/**
	 * @date: April 05, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Customer
	 *               Profile
	 * @param requestVO
	 * @return xmlRequest <retrieveCustomerProductConfiguration> <MsgRqHdr>
	 *         <FuncId>40200200</FuncId> <RqMode>0</RqMode>
	 *         <RqUID>20130312112500101</RqUID> <LangPref>En</LangPref>
	 *         <ClientDt>2010-10-27T16:50:03</ClientDt>
	 *         <CallerService>SubmitProductOrder</CallerService> </MsgRqHdr>
	 *         <Body> <sourceCriteria> <Id>1000112459996085</Id> <Specification>
	 *         <Name>BillingAccountNumber</Name> </Specification>
	 *         </sourceCriteria> </Body> </retrieveCustomerProductConfiguration>
	 */

	public String generateCustomerInfoXML(CustomerInfoVO requestVO)
	{
		String method = "generateCustomerInfoXML";
		String xmlRequest = "";

		if (requestVO == null)
		{
			// log.fatal("CustomerProfileXMLHandler > generateCustomerProfileXML : ActiveLinesInquiryVO is null.");
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}

		LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "CustomerProfileXMLHandler > generateCustomerInfoXML : start for account: " + requestVO.getSourceId(), className,
				method, null);
		executionContext.log(logEntryVO);

//		final ByteOutputStream outputStream = new ByteOutputStream();
//		XMLStreamWriter xmlStreamWriter = null;
		try
		{
			RetrieveCustomerProductConfiguration objectAsXML = new RetrieveCustomerProductConfiguration();
			MsgRqHdr msgRqHdr = new MsgRqHdr();
			if(requestVO.getFuncId()==-1)//default 
				msgRqHdr.setFuncId(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION);
			else if (requestVO.getFuncId() != ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY)	
				msgRqHdr.setFuncId(requestVO.getFuncId());
			else
				msgRqHdr.setFuncId(ConstantsIfc.FUNC_ID_CUSTOMER_PRODUCT_INFORMATION_ACCT_NUMBER_INQUIRY);
			
			msgRqHdr.setRqMode(ConstantsIfc.REQ_MODE_0);
			
			//msgRqHdr.setRqUID(new BigDecimal(new SimpleDateFormat(ConstantsIfc.DATE_FORMAT).format(new Date()))); 
			msgRqHdr.setRqUID(new BigDecimal(System.currentTimeMillis()+RandomGenerator.generateNumericRandom(3))); //PBI000000023851
			msgRqHdr.setLangPref(requestVO.getPreferredLanguage());
			msgRqHdr.setClientDt(new SimpleDateFormat(ConstantsIfc.DATE_FORMAT).format(new Date()));
			msgRqHdr.setCallerService(ConstantsIfc.CALLER_SERVICE_SUBMIT_PRODUCT_ORDER);
			
			if(40202000 == requestVO.getFuncId()){
				msgRqHdr.setUsrId(FormatterUtility.checkNull(requestVO.getUsrId()));
				msgRqHdr.setsCId(FormatterUtility.checkNull(requestVO.getsCId()));
			}
			

			objectAsXML.setMsgRqHdr(msgRqHdr);

			Body body = new Body();

			SourceCriteria sourceCriteria = new SourceCriteria();
			sourceCriteria.setId(requestVO.getSourceId());

			RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification specification = new RetrieveCustomerProductConfiguration.Body.SourceCriteria.Specification();
			specification.setName(requestVO.getSourceSpecName());

			sourceCriteria.setSpecification(specification);

			body.setSourceCriteria(sourceCriteria);
			
			//if FuncId=40202000 below tag will be come into picture.
			
			if(40202000 == requestVO.getFuncId()){
				targetCriteria targetCriteria = new targetCriteria();
				targetCriteria.setName(ConstantsIfc.AllContractsWithProduct);
				List<CharacteristicValue> cvList = new ArrayList<CharacteristicValue>();
				
				CharacteristicValue cv = new CharacteristicValue();
				Characteristic chart = new Characteristic(); 
				chart.setName(ConstantsIfc.PartyInquiryLevel);
				cv.setValue("4");
				cv.setCharacteristic(chart);
				cvList.add(cv);
				cv = new CharacteristicValue();
				chart = new Characteristic(); 
				chart.setName(ConstantsIfc.ContractInquiryLevel);
				cv.setValue("4");
				cv.setCharacteristic(chart);
				cvList.add(cv);
				targetCriteria.setCharacteristicValue(cvList);
				
				body.setTargetCriteria(targetCriteria);
			}
			
			objectAsXML.setBody(body);

//			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
//			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");
//
//			JAXBContext jaxbContext = JAXBContext.newInstance(RetrieveCustomerProductConfiguration.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//			jaxbMarshaller.marshal(objectAsXML, xmlStreamWriter);
//			outputStream.flush();
//
//			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();
			
			
			
			
			xmlRequest = JAXBUtilities.getInstance().marshal(objectAsXML);

		}
		catch (Exception e)
		{
			logEntryVO = new LogEntryVO(Level.SEVERE, "Exception while generating customer info xml" + e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
//			if (xmlStreamWriter != null)
//			{
//				try
//				{
//					xmlStreamWriter.close();
//				}
//				catch (XMLStreamException e)
//				{
//					logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
//					executionContext.log(logEntryVO);
//				}
//			}
//			if (outputStream != null)
//			{
//				outputStream.close();
//			}
		}
		return xmlRequest;
	}

	/**
	 * @date: April 05 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Customer profile XML
	 * @param replyMessage
	 * @return CustomerInfoVO
	 */
	public CustomerInfoVO parseCustomerInfoXML(String replyMessage)
	{
		String methodName = "parseCustomerInfoXML";
		CustomerInfoVO customerInfoVO = null;
		try
		{
//			JAXBContext jc = JAXBContext.newInstance(RetrieveCustomerProductConfigurationRs.class);
//			Unmarshaller unmarshaller = jc.createUnmarshaller();
//			XMLInputFactory xmlif = XMLInputFactory.newInstance();

			// for testing -- START
			// FileReader fr = new
			// FileReader("G:\\Portal 8\\RCPC response.xml");
			// XMLStreamReader xmler = xmlif.createXMLStreamReader(fr);
			// for Testing -- END

//			XMLStreamReader xmler = xmlif.createXMLStreamReader(new StringReader(replyMessage));
//			RetrieveCustomerProductConfigurationRs obj = (RetrieveCustomerProductConfigurationRs) unmarshaller.unmarshal(xmler);
			
			
			
			
			RetrieveCustomerProductConfigurationRs obj = (RetrieveCustomerProductConfigurationRs) JAXBUtilities.getInstance().unmarshal(RetrieveCustomerProductConfigurationRs.class, replyMessage);

			if (obj != null)
			{
				customerInfoVO = new CustomerInfoVO();
				customerInfoVO.setStatusCode(obj.getMsgRsHdr() != null ? obj.getMsgRsHdr().getStatusCode() : "");

				if (obj.getBody() != null)
				{
					CustomerConfiguration customerConfiguration = obj.getBody().getCustomerConfiguration();
					if (customerConfiguration != null)
					{
						Customer customer = customerConfiguration.getCustomer();
						Agreement agreement = customerConfiguration.getAgreement();

						// Fetch Items related to customer
						if (customer != null)
						{
							Party party = customer.getParty();
							if (party != null)
							{
								Specification specification = party.getSpecification();
								if (specification != null)
								{
									// Customer Category
									customerInfoVO.setCustomerCategory(specification.getCategory());
									// Customer Type
									customerInfoVO.setCustomerType(specification.getType());
								}

								PartyIdentification partyIdentification = party.getPartyIdentification();
								if (partyIdentification != null)
								{
									// Customer ID Number
									customerInfoVO.setCustomerIdNumber(partyIdentification.getId());
									Specification partySpecification = partyIdentification.getSpecification();
									// Customer ID Type
									customerInfoVO.setCustomerIdType(partySpecification.getType());
								}

								PartyExtensions partyExtensions = party.getPartyExtensions();
								if (partyExtensions != null)
								{
									PartyChoice partyChoice = partyExtensions.getPartyChoice();
									if (partyChoice != null)
									{
										Individual individual = partyChoice.getIndividual();
										// Customer Gender
										customerInfoVO.setCustomerGender(individual != null ? individual.getGender() : "");
									}
								}
								
								ContactMedium contactMedium = party.getContactMedium();
								if(contactMedium !=null){
									if(contactMedium.getEmailContact()!=null)
										customerInfoVO.setEmailAddress(FormatterUtility.checkNull(contactMedium.getEmailContact().getEmailAddress()));
									if(contactMedium.getTelephoneContact()!=null)
										customerInfoVO.setTelephoneNumber(FormatterUtility.checkNull(contactMedium.getTelephoneContact().getNumber()));
								}

								List<CharacteristicValue> list = party.getCharacteristicValue();

								if (list != null && list.size() > 0)
								{
									Method[] methodArray = customerInfoVO.getClass().getMethods();
									Method method = null;
									for (CharacteristicValue characteristicValue : list)
									{
										if (methodArray != null)
										{
											int count = methodArray.length;
											for (int k = 0; k < count; k++)
											{
												method = methodArray[k];
												if (method != null && method.getName().equalsIgnoreCase("set" + characteristicValue.getCharacteristic().getName()))
												{
													method.invoke(customerInfoVO, new Object[] { characteristicValue.getValue() });
													break;
												}
											}
										}
									}
								}

							}
						}

						// Fetch Items related to Agreement
						if (agreement != null)
						{
							AgreementItem agreementItem = agreement.getAgreementItem();
							if (agreementItem != null)
							{
								List<CharacteristicValue> list = agreementItem.getCharacteristicValue();

								if (list != null && list.size() > 0)
								{
									Method[] methodArray = customerInfoVO.getClass().getMethods();
									Method method = null;
									for (CharacteristicValue characteristicValue : list)
									{
										if (methodArray != null)
										{
											int count = methodArray.length;
											for (int k = 0; k < count; k++)
											{
												method = methodArray[k];
												if (method != null && method.getName().equalsIgnoreCase("set" + characteristicValue.getCharacteristic().getName()))
												{
													method.invoke(customerInfoVO, new Object[] { characteristicValue.getValue() });
													break;
												}
											}
										}
									}
								}
							}
							
							 CustomerAccountId  customerAccountId = agreement.getCustomerAccountId();
							 if(customerAccountId != null){
								 customerInfoVO.setAccountNumber(customerAccountId.getId());
							 }
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "Exception while parsing customer info xml" + e.getMessage(), className, methodName, e);
			executionContext.log(logEntryVO);
			throw new ServiceException(e.getMessage(), e);
		}
		return customerInfoVO;
	}
}