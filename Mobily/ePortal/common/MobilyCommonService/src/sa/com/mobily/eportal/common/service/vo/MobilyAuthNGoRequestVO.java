package sa.com.mobily.eportal.common.service.vo;

/**
 * 
 * @author n.gundluru.mit
 *
 */
public class MobilyAuthNGoRequestVO {
	
	private String functionId;
	private String sourceMSISDN;
	private String targetMSISDN;
	private String recipientID = null;
	private String amount;
	private String requestorLanguage;
	private String pinCode;
	private String serviceName;
	private String serviceType;
	private String planType;
	private String operation;
	private String forwardOption;
	private int customerType;
	private String packageID;
	private String packageDescription;
	private int lineType;
	private String requestorUserId;
	private String settingMode;
	private String command;
	
	
	/**
	 * @return the functionId
	 */
	public String getFunctionId() {
		return functionId;
	}
	/**
	 * @param functionId the functionId to set
	 */
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	/**
	 * @return the sourceMSISDN
	 */
	public String getSourceMSISDN() {
		return sourceMSISDN;
	}
	/**
	 * @param sourceMSISDN the sourceMSISDN to set
	 */
	public void setSourceMSISDN(String sourceMSISDN) {
		this.sourceMSISDN = sourceMSISDN;
	}
	/**
	 * @return the targetMSISDN
	 */
	public String getTargetMSISDN() {
		return targetMSISDN;
	}
	/**
	 * @param targetMSISDN the targetMSISDN to set
	 */
	public void setTargetMSISDN(String targetMSISDN) {
		this.targetMSISDN = targetMSISDN;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the requestorLanguage
	 */
	public String getRequestorLanguage() {
		return requestorLanguage;
	}
	/**
	 * @param requestorLanguage the requestorLanguage to set
	 */
	public void setRequestorLanguage(String requestorLanguage) {
		this.requestorLanguage = requestorLanguage;
	}
	/**
	 * @return the pinCode
	 */
	public String getPinCode() {
		return pinCode;
	}
	/**
	 * @param pinCode the pinCode to set
	 */
	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}
	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	/**
	 * @return the forwardOption
	 */
	public String getForwardOption() {
		return forwardOption;
	}
	/**
	 * @param forwardOption the forwardOption to set
	 */
	public void setForwardOption(String forwardOption) {
		this.forwardOption = forwardOption;
	}
	/**
	 * @return the customerType
	 */
	public int getCustomerType() {
		return customerType;
	}
	/**
	 * @param customerType the customerType to set
	 */
	public void setCustomerType(int customerType) {
		this.customerType = customerType;
	}
	/**
	 * @return the packageID
	 */
	public String getPackageID() {
		return packageID;
	}
	/**
	 * @param packageID the packageID to set
	 */
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	/**
	 * @return the packageDescription
	 */
	public String getPackageDescription() {
		return packageDescription;
	}
	/**
	 * @param packageDescription the packageDescription to set
	 */
	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}
	/**
	 * @return the lineType
	 */
	public int getLineType() {
		return lineType;
	}
	/**
	 * @param lineType the lineType to set
	 */
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	/**
	 * @return the planType
	 */
	public String getPlanType() {
		return planType;
	}
	/**
	 * @param planType the planType to set
	 */
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	/**
	 * @return the requestorUserId
	 */
	public String getRequestorUserId() {
		return requestorUserId;
	}
	/**
	 * @param requestorUserId the requestorUserId to set
	 */
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	/**
	 * @return the settingMode
	 */
	public String getSettingMode() {
		return settingMode;
	}
	/**
	 * @param settingMode the settingMode to set
	 */
	public void setSettingMode(String settingMode) {
		this.settingMode = settingMode;
	}
	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}
	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}
	/**
	 * @return the recipientID
	 */
	public String getRecipientID() {
		return recipientID;
	}
	/**
	 * @param recipientID the recipientID to set
	 */
	public void setRecipientID(String recipientID) {
		this.recipientID = recipientID;
	}
	
}