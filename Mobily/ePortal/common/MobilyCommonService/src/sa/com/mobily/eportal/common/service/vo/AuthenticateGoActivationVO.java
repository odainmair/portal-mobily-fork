/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author @moustafa.hassan.suliman - mhassan.suliman@gmail.com
 * 
 */
public class AuthenticateGoActivationVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int moduleId;

	private String msisdn;

	private String activationCode;

	private Date expirationDate;

	private int pinStatus;

	public int getModuleId()
	{
		return moduleId;
	}

	public void setModuleId(int moduleId)
	{
		this.moduleId = moduleId;
	}

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public int getPinStatus() {
		return pinStatus;
	}

	public void setPinStatus(int pinStatus) {
		this.pinStatus = pinStatus;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activationCode == null) ? 0 : activationCode.hashCode());
		result = prime * result
				+ ((expirationDate == null) ? 0 : expirationDate.hashCode());
//		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		result = prime * result + pinStatus;

		return result;
	}

	@PrePersist
	@PreUpdate
	public void updateAuditInfo() {
		setExpirationDate(Calendar.getInstance().getTime());
	}

}
