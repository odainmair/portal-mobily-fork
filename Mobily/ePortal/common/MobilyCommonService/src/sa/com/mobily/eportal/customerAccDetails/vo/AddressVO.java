package sa.com.mobily.eportal.customerAccDetails.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AddressVO extends BaseVO
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String isPrimaryMVG;
	private String streetAddress;
	private String country;
	private String city;
	private String EECCPOBox;
	private String EECCPostalCode;
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getIsPrimaryMVG()
	{
		return isPrimaryMVG;
	}
	public void setIsPrimaryMVG(String isPrimaryMVG)
	{
		this.isPrimaryMVG = isPrimaryMVG;
	}
	public String getStreetAddress()
	{
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress)
	{
		this.streetAddress = streetAddress;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getEECCPOBox()
	{
		return EECCPOBox;
	}
	public void setEECCPOBox(String eECCPOBox)
	{
		EECCPOBox = eECCPOBox;
	}
	public String getEECCPostalCode()
	{
		return EECCPostalCode;
	}
	public void setEECCPostalCode(String eECCPostalCode)
	{
		EECCPostalCode = eECCPostalCode;
	}

}
