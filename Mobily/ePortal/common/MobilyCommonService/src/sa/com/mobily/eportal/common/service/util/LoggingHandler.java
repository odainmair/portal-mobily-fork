package sa.com.mobily.eportal.common.service.util;

import java.io.IOException;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

/**
 * MyCustomHandler outputs contents to a specified file
 */
public class LoggingHandler extends FileHandler {

	List<LogRecord> records = new ArrayList<LogRecord>();
	
	public LoggingHandler(String filename, int limit, int count, boolean append) throws IOException
	{
		super(filename, limit, count, append);
	}

	/* (non-API documentation)
	 * @see java.util.logging.Handler#publish(java.util.logging.LogRecord)
	 */
	public void publish(LogRecord record) {
        
		// ensure that this log record should be logged by this Handler
		if (!isLoggable(record))
			return;
		
		if(record != null && record.getMessage() != null && record.getMessage().equals("End")){
			StringBuffer buffer = new StringBuffer();
			for (Iterator<LogRecord> iterator = records.iterator(); iterator.hasNext();)
			{
				buffer.append(getFormatter().format((LogRecord) iterator.next()) + "\n");
			}
			super.publish(new LogRecord(Level.INFO, buffer.toString()));
			records.clear();
		} else {
			records.add(record);
		}
	}
}
