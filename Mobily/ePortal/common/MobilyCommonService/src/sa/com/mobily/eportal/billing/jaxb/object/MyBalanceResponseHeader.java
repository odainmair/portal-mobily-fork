package sa.com.mobily.eportal.billing.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EE_EAI_HEADER", propOrder = { "MsgFormat", "MsgVersion",
		"RequestorChannelId", "RequestorChannelFunction", "RequestorUserId",
		"RequestorLanguage", "RequestorSecurityInfo", "ReturnCode" })
public class MyBalanceResponseHeader {
	@XmlElement(name = "MsgFormat")
	protected String msgFormat;
	@XmlElement(name = "MsgVersion")
	protected String msgVersion;

	@XmlElement(name = "RequestorChannelId")
	protected String channelId;
	@XmlElement(name = "RequestorChannelFunction")
	protected String channelFunction;
	@XmlElement(name = "RequestorUserId")
	protected String userId;
	@XmlElement(name = "RequestorLanguage")
	protected String language;
	@XmlElement(name = "RequestorSecurityInfo")
	protected String securityInfo;
	@XmlElement(name = "ReturnCode")
	protected String returnCode;

	public String getMsgFormat() {
		return msgFormat;
	}

	public void setMsgFormat(String msgFormat) {
		this.msgFormat = msgFormat;
	}

	public String getMsgVersion() {
		return msgVersion;
	}

	public void setMsgVersion(String msgVersion) {
		this.msgVersion = msgVersion;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelFunction() {
		return channelFunction;
	}

	public void setChannelFunction(String channelFunction) {
		this.channelFunction = channelFunction;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSecurityInfo() {
		return securityInfo;
	}

	public void setSecurityInfo(String securityInfo) {
		this.securityInfo = securityInfo;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

}