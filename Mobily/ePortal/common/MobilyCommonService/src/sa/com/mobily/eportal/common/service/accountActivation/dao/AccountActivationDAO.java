package sa.com.mobily.eportal.common.service.accountActivation.dao;

/**
 * This class is DAO responsible for add/delete/update/inquiry of  SR_ACCOUNT_ACTIVATION_TBL which used for storing user activation code during registratoin process.
 * <p> 

 *  
 * @author Yousef Alkhalaileh
 */


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.common.service.accountActivation.model.AccountActivation;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
 

public class AccountActivationDAO extends AbstractDBDAO<AccountActivation> {

    private static AccountActivationDAO instance = new AccountActivationDAO();
    
    private static final String AA_QUERY_BY_UESRACCID = " SELECT ID, ACTIVATION_CODE, PROJECT_ID, REQ_DATE, USER_ACCT_ID " +
    		" FROM SR_ACCOUNT_ACTIVATION_TBL WHERE USER_ACCT_ID = ? ";
    private static final String AA_QUERY_BY_ACTIVATION_CODE = "select A.ID, A.ACTIVATION_CODE, A.PROJECT_ID, A.REQ_DATE, A.USER_ACCT_ID from SR_ACCOUNT_ACTIVATION_TBL  A, USER_ACCOUNTS B"
    		+" WHERE A.USER_ACCT_ID=B.USER_ACCT_ID  AND a.activation_code=? AND LOWER(B.USER_NAME)=? ";
    private static final String AA_ADD = " INSERT INTO SR_ACCOUNT_ACTIVATION_TBL(ACTIVATION_CODE, PROJECT_ID, REQ_DATE, USER_ACCT_ID, ID)  VALUES(?, ?, ?, ?, ACTIVATION_SEQ.nextval)";
    private static final String AA_DEL = " DELETE FROM SR_ACCOUNT_ACTIVATION_TBL WHERE ID = ? ";
 


    protected AccountActivationDAO() {
    	super(DataSources.DEFAULT);
    }

    /**
     * Method for obtaining the singleton instance
     * 
     * @return
     */
    public static synchronized AccountActivationDAO getInstance() {
    	return instance;
    }
	
    public List<AccountActivation> findByUserAccId(Long userAccId) {
        return query(AA_QUERY_BY_UESRACCID, new Object[] {userAccId} );
    }
    public AccountActivation findByActivationCode(Long activationCode,String username) {
    	List<AccountActivation> list= query(AA_QUERY_BY_ACTIVATION_CODE, new Object[] {activationCode,username.toLowerCase()} );
    	if(list!=null&&list.size()>0){
    		return list.get(0);
    	}
    	 return null;
    }
    public int saveAccountActivation(AccountActivation accountActivation) {
    	return update(AA_ADD, accountActivation.getActivationCode(), accountActivation.getProject_id(), accountActivation.getReqDate(), accountActivation.getUserAcctId());
    }

    public int delete(Long id) {
    	return update(AA_DEL, new Object[] {id});
    }

    protected AccountActivation mapDTO(ResultSet rs) throws SQLException {
		AccountActivation accountActivation = new AccountActivation();
		
		accountActivation.setActivationCode(rs.getLong("ACTIVATION_CODE"));
		accountActivation.setId(rs.getLong("ID"));
		accountActivation.setProject_id(rs.getInt("PROJECT_ID"));
		accountActivation.setReqDate(rs.getTimestamp("REQ_DATE"));
		accountActivation.setUserAcctId(rs.getLong("USER_ACCT_ID"));
		
		return accountActivation;
    }
}