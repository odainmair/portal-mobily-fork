package sa.com.mobily.eportal.common.constants;

public interface MobilyResEnvKeysConstants{
	
  public static final String SERVER_NAME="SERVER_NAME";
  public static final String EBILL_GLOBYUS_URL_USER_ACCESS="ebill.globys.url.user.access";
  public static final String EBILL_GLOBYUS_URL_USER_CONFIG="ebill.globys.url.user.config";
  public static final String EBILL_GLOBYUS_URL_ACCT_ACCESS="ebill.globys.url.account.org.access";
  public static final String EBILL_GLOBYUS_PASSWORD="ebill.globys.password";
  public static final String EBILL_GLOBYUS_USERNAME="ebill.globys.username";
  public static final String TAM_ADMIN_USER_NAME="TAM.ADMIN.USER.NAME";
  public static final String TAM_ADMIN_USER_PASSWORD="TAM.ADMIN.PASSWORD";
  public static final String TAM_CONFIG_FILE_URL="TAM.CONFIG.FILE.URL";
  public static final String GIS_DOMAIN="GIS_DOMAIN";
  public static final String GIS_DOMAIN_WIRELESS="GIS_DOMAIN_WIRELESS";
  public static final String GIS_DOMAIN_STORE="GIS_DOMAIN_STORE";
  public static final String GIS_DOMAIN_IPTV="GIS_DOMAIN_IPTV";
  public static final String GIS_DOMAIN_ELIFE="GIS_DOMAIN_ELIFE";
  public static final String SHARE_ROOT_DIRECTORY = "SHARE_ROOT_DIRECTORY";
  
}
