package sa.com.mobily.eportal.common.exception.portletfilter;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;
import javax.portlet.filter.ResourceFilter;

public class XSSHandlingPortletFilter implements RenderFilter, ActionFilter, ResourceFilter
{
	@Override
	public void destroy(){}

	@Override
	public void init(FilterConfig filterConfig) throws PortletException{}

	@Override
	public void doFilter(ResourceRequest request, ResourceResponse response, FilterChain chain) throws IOException, PortletException
	{
	    System.out.println("################ ########## DEBUG: M Portlet Filter Pre-processing a Resource request");
	    
	    chain.doFilter(new XSSResourceRequestWrapper(request), response);
	    System.out.println("$$$$$$$$$$$$$$$$ $$$$$$$$$$ DEBUG: M Portlet Filter Post-processing a Resource request");
	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain) throws IOException, PortletException
	{
	    System.out.println("################ ########## DEBUG: M Portlet Filter Pre-processing a Action request");
	    
	    chain.doFilter(new XSSActionRequestWrapper(request), response);
	    System.out.println("$$$$$$$$$$$$$$$$ $$$$$$$$$$ DEBUG: M Portlet Filter Post-processing a Action request");
	}

	@Override
	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException
	{
	    System.out.println("################ ########## DEBUG: M Portlet Filter Pre-processing a Render request");
	    
	    chain.doFilter(new XSSRenderRequestWrapper(request), response);
	    System.out.println("$$$$$$$$$$$$$$$$ $$$$$$$$$$ DEBUG: M Portlet Filter Post-processing a Render request");
	}
}