package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.valueobjects.UserAccountVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class UserAccountDAO extends AbstractDBDAO<UserAccountVO>{
	
	protected UserAccountDAO() {
		super(DataSources.DEFAULT);
	}

	private static UserAccountDAO INSTANCE = new UserAccountDAO();
	private static final String COL_USER_ACCT_ID = "USER_ACCT_ID";
	private static final String COL_USER_NAME = "USER_NAME";
	//private static final String COL_IQAMA = "IQAMA";
	private static final String COL_CREATED_DATE = "CREATED_DATE";
	private static final String COL_ACTIVATION_DATE = "ACTIVATION_DATE";
	private static final String COL_LAST_LOGIN_TIME = "LAST_LOGIN_TIME";
	private static final String COL_EMAIL = "EMAIL";
	private static final String COL_STATUS = "STATUS";
	private static final String COL_FIRST_NAME = "FIRST_NAME";
	private static final String COL_LAST_NAME = "LAST_NAME";
	private static final String COL_CUST_SEGMENT = "CUST_SEGMENT";
	private static final String COL_BIRTHDATE = "BIRTHDATE";
	private static final String COL_NATIONALITY = "NATIONALITY";
	private static final String COL_GENDER = "GENDER";
	private static final String COL_REGISTRATION_DATE = "REGISTRATION_DATE";
	private static final String COL_ROLE_ID = "ROLE_ID";
	
	private static final String UA_FIND_ACCOUNT_BY_USERNAME = "SELECT USER_ACCT_ID,USER_NAME,CREATED_DATE,ACTIVATION_DATE,LAST_LOGIN_TIME,EMAIL,STATUS,FIRST_NAME,LAST_NAME,CUST_SEGMENT,BIRTHDATE,NATIONALITY,GENDER,REGISTRATION_DATE,ROLE_ID FROM USER_ACCOUNTS WHERE lower(USER_NAME) = ?";
	private static final String UA_FIND_ACCOUNT_BY_ACCOUNT_ID = "SELECT USER_ACCT_ID,USER_NAME,CREATED_DATE,ACTIVATION_DATE,LAST_LOGIN_TIME,EMAIL,STATUS,FIRST_NAME,LAST_NAME,CUST_SEGMENT,BIRTHDATE,NATIONALITY,GENDER,REGISTRATION_DATE,ROLE_ID FROM USER_ACCOUNTS WHERE USER_ACCT_ID = ?";
	
	public static UserAccountDAO getInstance(){
		return INSTANCE;
	}
	
	public List<UserAccountVO> getUserAccountVOByUserName(String userName){
		return query(UA_FIND_ACCOUNT_BY_USERNAME,userName.toLowerCase());
	}
	
	public List<UserAccountVO> getUserAccountVOByAccountId(String accountId){
		return query(UA_FIND_ACCOUNT_BY_ACCOUNT_ID,accountId);
	}
	
	@Override
	protected UserAccountVO mapDTO(ResultSet rs) throws SQLException {
		UserAccountVO userAccountVO = new UserAccountVO();
		userAccountVO.setActivationDate(rs.getTimestamp(COL_ACTIVATION_DATE));
		userAccountVO.setBirthDate(rs.getDate(COL_BIRTHDATE));
		userAccountVO.setCreatedDate(rs.getTimestamp(COL_CREATED_DATE));
		userAccountVO.setCustSegment(rs.getInt(COL_CUST_SEGMENT));
		userAccountVO.setEmail(rs.getString(COL_EMAIL));
		userAccountVO.setFirstName(rs.getString(COL_FIRST_NAME));
		userAccountVO.setGender(rs.getString(COL_GENDER));
		//userAccountVO.setIqama(rs.getString(COL_IQAMA));
		userAccountVO.setLastLoginTime(rs.getTimestamp(COL_LAST_LOGIN_TIME));
		userAccountVO.setLastName(rs.getString(COL_LAST_NAME));
		userAccountVO.setNationality(rs.getString(COL_NATIONALITY));
		userAccountVO.setRegistrationDate(rs.getTimestamp(COL_REGISTRATION_DATE));
		userAccountVO.setStatus(rs.getString(COL_STATUS));
		userAccountVO.setUserAccountId(rs.getLong(COL_USER_ACCT_ID));
		userAccountVO.setUserName(rs.getString(COL_USER_NAME));
		userAccountVO.setRoleId(rs.getInt(COL_ROLE_ID));
		return userAccountVO;
	}
}
