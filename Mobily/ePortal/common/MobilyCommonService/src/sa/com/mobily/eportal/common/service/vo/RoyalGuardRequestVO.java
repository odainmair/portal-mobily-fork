/*
 * Created on March 31, 2010
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RoyalGuardRequestVO {
	String msisdn = "";
	String requestorUserId = "";
	String operation = "";
	String billingAccount = "";
	Date startDate = null;
	Date endDate = null;
	private String fetchType = "";
	private String info = "";
	private List addList = null;
	private List removeList = null;
	
	private List newRGList = null;
	private List newTempList = null;
	
	private List oldRGList = null;
	private List oldTempList = null;
	private List distributionVOs = null;
	
	private String monthYear = "";
	private String strStartDate = null;
	private String strEndDate = null;
	private String count = "";
	
	private String startRecord = "";
	private String endRecord = "";
	
	public String getStartRecord() {
		return startRecord;
	}
	public void setStartRecord(String startRecord) {
		this.startRecord = startRecord;
	}
	public String getEndRecord() {
		return endRecord;
	}
	public void setEndRecord(String endRecord) {
		this.endRecord = endRecord;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getStrStartDate() {
		return strStartDate;
	}
	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}
	public String getStrEndDate() {
		return strEndDate;
	}
	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	public String getMonthYear() {
		return monthYear;
	}
	public void setMonthYear(String monthYear) {
		this.monthYear = monthYear;
	}
	public List getDistributionVOs() {
		return distributionVOs;
	}
	public void setDistributionVOs(List distributionVOs) {
		this.distributionVOs = distributionVOs;
	}
	public List getNewRGList() {
		return newRGList;
	}
	public void setNewRGList(List newRGList) {
		this.newRGList = newRGList;
	}
	public List getNewTempList() {
		return newTempList;
	}
	public void setNewTempList(List newTempList) {
		this.newTempList = newTempList;
	}
	public List getOldRGList() {
		return oldRGList;
	}
	public void setOldRGList(List oldRGList) {
		this.oldRGList = oldRGList;
	}
	public List getOldTempList() {
		return oldTempList;
	}
	public void setOldTempList(List oldTempList) {
		this.oldTempList = oldTempList;
	}
	public List getAddList() {
		return addList;
	}
	public void setAddList(List addList) {
		this.addList = addList;
	}
	public List getRemoveList() {
		return removeList;
	}
	public void setRemoveList(List removeList) {
		this.removeList = removeList;
	}
	public String getFetchType() {
		return fetchType;
	}
	public void setFetchType(String fetchType) {
		this.fetchType = fetchType;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	/**
	 * @return Returns the billingAccount.
	 */
	public String getBillingAccount() {
		return billingAccount;
	}
	/**
	 * @param billingAccount The billingAccount to set.
	 */
	public void setBillingAccount(String billingAccount) {
		this.billingAccount = billingAccount;
	}

	/**
	 * @return Returns the endDate.
	 */
	public Date getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate The endDate to set.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * @return Returns the requestorUserId.
	 */
	public String getRequestorUserId() {
		return requestorUserId;
	}
	/**
	 * @param requestorUserId The requestorUserId to set.
	 */
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	/**
	 * @return Returns the startDate.
	 */
	public Date getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate The startDate to set.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
    /**
     * The purpose of the toString method is to return the contents of this
     * object as a String.
     * 
     * @return A String containing the contents of this class.
     */
    public String toString() {
    	
        String EOL = System.getProperty("line.separator"); 
        StringBuffer sb = new StringBuffer("-------------------------------------------");  
        sb.append("RoyalGuardRequestVO");
        sb.append("-------------------------------------------"); 
        sb.append(EOL);
        sb.append("MSISDN               : "); 
        sb.append(this.getMsisdn());
        sb.append(EOL);
        sb.append("RequestorUserId      : "); 
        sb.append(this.getRequestorUserId());
        sb.append(EOL);
        sb.append("Operation            : "); 
        sb.append(this.getOperation());
        sb.append(EOL);
        sb.append("BillingAccount       : "); 
        sb.append(this.getBillingAccount());
        sb.append(EOL);
        sb.append("StartDate            : "); 
        sb.append(this.getStartDate());
        sb.append(EOL);
        sb.append("EndDate              : "); 
        sb.append(this.getEndDate());
        sb.append(EOL);
        
       /* if (this.getDistributionVOs() != null && this.getDistributionVOs().size() > 0) {

            sb.append("-------------------------------------------"); 
            sb.append(EOL);
            sb.append("Distribution VOs Below");
            sb.append(EOL);
            sb.append("-------------------------------------------"); 
            sb.append(EOL);
            for (int idx = 0; idx < this.getDistributionVOs().size(); idx++) {
                sb.append(((DistributionVO)this.getDistributionVOs().get(idx)).toString());
                sb.append(EOL);
            }
            sb.append(EOL);
            sb.append(EOL);
        } else {
            sb.append("Distribution VOs     : NONE");
            sb.append(EOL);
        }*/
        sb.append("-------------------------------------------"); 
        return sb.toString();
    }
}
