package sa.com.mobily.eportal.common.exception.application;


public class DataNotFoundException extends MobilyApplicationException {

	private static final long serialVersionUID = -454949953040043310L;

	public DataNotFoundException() {
	}

	public DataNotFoundException(String message) {
		super(message);
	}

	public DataNotFoundException(Throwable cause) {
		super(cause);
	}

	public DataNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}