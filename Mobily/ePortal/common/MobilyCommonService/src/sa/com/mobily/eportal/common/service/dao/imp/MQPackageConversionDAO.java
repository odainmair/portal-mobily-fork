package sa.com.mobily.eportal.common.service.dao.imp;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.PackageConversionDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.PackageTypeVO;

import com.mobily.exception.mq.MQSendException;

public class MQPackageConversionDAO implements PackageConversionDAO{
	public static final Logger log = LoggerInterface.log;
   
	public String handlePackageConversion(String xmlRequestMessage){
	
		String replyMessage = "";
		log.debug("MQPackageConversionDAO > handlePackageConversion > called");
		try {
			/*
			 * Get QueuName
			 */
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.PACKAGE_CONVERSION_REQUEST_QUEUENAME);
			log.debug("MQPackageConversionDAO > handlePackageConversion >Request Queue Name  ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.PACKAGE_CONVERSION_REPLY_QUEUENAME);
			log.debug("MQPackageConversionDAO > handlePackageConversion >Reply Queue Name ["+ strReplyQueueName + "]");
			
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQCustomerStatusDAO > getCustomerStatus > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQCustomerStatusDAO > getCustomerStatus > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQCustomerStatusDAO > getCustomerStatus > Done.................");
		return replyMessage;

	}

	public PackageTypeVO getDefaultPackageByDefaultId(int defaultPackageId) {
		// TODO Auto-generated method stub
		return null;
	}

	public PackageTypeVO getDefaultPackageById(String packageId) {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList loadPackageTypesFromDB(int customerType) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean hasPendingRequest(String msisdn) {
		// TODO Auto-generated method stub
		return false;
	}
}