/*
 * Created on Dec 01, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.xml;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.CharacteristicsVO;
import sa.com.mobily.eportal.common.service.vo.ContactMediumVO;
import sa.com.mobily.eportal.common.service.vo.ContactPersonVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.LOVInfoVO;
import sa.com.mobily.eportal.common.service.vo.LOVInquiryVO;
import sa.com.mobily.eportal.common.service.vo.PartyExtensionsVO;
import sa.com.mobily.eportal.common.service.vo.PartyIdentificationVO;
import sa.com.mobily.eportal.common.service.vo.SpecificationVO;

/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerProfileXMLHandler {
    private static final Logger log = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
    public static String solicitationIndicator = "";
    public static String preferredAddressIndicator = "";
    public static String streetAddress = "";
    public static String authPersonMiddleName = "";
    public static String contactNumber = "";
    public static String contactType = "";
    public static String preferredContactFlag = "";
    public static String companyAccountNumber = "";
    public static String companyRowId = "";
    public static String authorizedPersonName = "";
    public static String masterBillingAccountNumberFlag = "";
    
    public String generateLOVXMLRequest(String lovType) {
    	log.debug("CustomerProfileXMLHandler > generateLOVXMLRequest > called for lovType::"+lovType);
    	String requestMessage = "";
   	 	try {
           Document doc = new DocumentImpl();
           
           Element root = doc.createElement(TagIfc.TAG_LOVInqRq);
           root.setAttribute(TagIfc.TAG_xmlns, TagIfc.TAG_EJADA_URL);
                      
           String srdate = FormatterUtility.FormateDate(new Date());
           
           Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_MsgRqHdr);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_RqUID,TagIfc.EI_Inquiry_RequestorChannelId + srdate);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SCId,TagIfc.EI_Inquiry_RequestorChannelId);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID, TagIfc.TAG_FuncId_50010000);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_RqMode, "0");
	       XMLUtility.closeParentTag(root, header);
           	
           //body
           	Element body = XMLUtility.openParentTag(doc, TagIfc.TAG_Body);
        	XMLUtility.generateElelemt(doc, body, TagIfc.TAG_LOVType,lovType);
           	XMLUtility.closeParentTag(root, body);
           
           doc.appendChild(root);
           requestMessage = XMLUtility.serializeRequest(doc);

   	 	} catch(Exception e) {
   	 		log.debug("CustomerProfileXMLHandler > generateLOVXMLRequest > Exception::"+e.getMessage());
   	 		throw new SystemException(e);    
   	 	}
   	 	log.debug("CustomerProfileXMLHandler > generateLOVXMLRequest > ended for lovType::"+lovType);
   	 	return requestMessage;
       
    }
    
	public LOVInquiryVO parseLOVXMLReply(String xmlReply) {
		log.info("CustomerProfileXMLHandler > parseLOVXMLReply > Called");
		LOVInquiryVO lOVInquiryVO = null;
		try {
			
			if (xmlReply == null || xmlReply == "")
				throw new MobilyCommonException();

			Document doc = XMLUtility.parseXMLString(xmlReply);

			//Node rootNode = doc.getElementsByTagName(TagIfc.TAG_LOVInqRs).item(0);
			Node rootNode = doc.getDocumentElement();

			NodeList nl = rootNode.getChildNodes();
			
			lOVInquiryVO = new LOVInquiryVO();
			
			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null){
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();
				}					
				if (TagIfc.TAG_MsgRsHdr.equalsIgnoreCase(p_szTagName)) {
					NodeList headerNodeList = l_Node.getChildNodes();			
					for(int i = 0; headerNodeList != null && i< headerNodeList.getLength(); i++){
						Element s_Node = (Element) headerNodeList.item(i);
						String s_szNodeTagName = s_Node.getTagName();
						
						if (TagIfc.TAG_StatusCode.equalsIgnoreCase(s_szNodeTagName)) {
							String statusCodeNodeValue = null;
							if (s_Node.getFirstChild() != null) {
								statusCodeNodeValue = s_Node.getFirstChild().getNodeValue();
								log.info("CustomerProfileXMLHandler > parseLOVXMLReply > statusCodeNodeValue::"+statusCodeNodeValue);
								lOVInquiryVO.setStatusCode(statusCodeNodeValue);
							}
						}
					}
				} else if (TagIfc.TAG_Body.equalsIgnoreCase(p_szTagName)) {
					NodeList bodyNodeList = l_Node.getChildNodes();			
					for(int i=0; bodyNodeList != null && i< bodyNodeList.getLength(); i++){
						Element s_Node = (Element) bodyNodeList.item(i);
						String s_szNodeTagName = s_Node.getTagName();
						if (TagIfc.TAG_SentRecs.equalsIgnoreCase(s_szNodeTagName)) {
							String totalRecordsNodeValue = null;
							if (s_Node.getFirstChild() != null) {
								totalRecordsNodeValue = s_Node.getFirstChild().getNodeValue();
								log.info("CustomerProfileXMLHandler > parseLOVXMLReply > totalRecordsNodeValue::"+totalRecordsNodeValue);
								lOVInquiryVO.setTotalRecordsCount(totalRecordsNodeValue);
							}
						} else if (TagIfc.TAG_LOVList.equalsIgnoreCase(s_szNodeTagName)) {
							List lovList = getLovInfoList(s_Node);
							lOVInquiryVO.setLovInfoVOList(lovList);
						}
					}
				} 
			}
		}  catch (Exception e) {
			log.fatal("CustomerProfileXMLHandler > parseLOVXMLReply > Exception , "	+ e);
			throw new SystemException(e);
		}
		return lOVInquiryVO;
	}
	
	private List getLovInfoList(Element l_Node) throws SystemException {
		List lovInfoVOList = new ArrayList();
		log.info("CustomerProfileXMLHandler > getLovInfoList > called....");
		LOVInfoVO objectVO  = null;
		try {
			NodeList lovInfoNodeList = l_Node.getChildNodes();			
			for(int i=0; lovInfoNodeList != null && i< lovInfoNodeList.getLength(); i++) {
				Element s_Node = (Element) lovInfoNodeList.item(i);
				String s_szNodeTagName = s_Node.getTagName();
				objectVO = new LOVInfoVO();
				if (TagIfc.TAG_LOVInfo.equalsIgnoreCase(s_szNodeTagName)) {
					NodeList minutesNodeList = s_Node.getChildNodes();				
					for (int j = 0; j < minutesNodeList.getLength(); j++) {					
						if ((minutesNodeList.item(j)).getNodeType() != Node.ELEMENT_NODE){
							continue;
						}
						Element c_Node = (Element) minutesNodeList.item(j);
						String p_szTagName = c_Node.getTagName();
						String l_szNodeValue = null;
						if (c_Node.getFirstChild() != null) {
							l_szNodeValue = c_Node.getFirstChild().getNodeValue();
						}
						if (TagIfc.TAG_RecTypeCode.equalsIgnoreCase(p_szTagName)) {						
							objectVO.setRecTypeCode(l_szNodeValue);						
						} else if (TagIfc.TAG_RecDesc.equalsIgnoreCase(p_szTagName)) {	
							objectVO.setRecDesc(l_szNodeValue);						
						} else if (TagIfc.TAG_Attribute1.equalsIgnoreCase(p_szTagName)) {						
							objectVO.setAttribute1(l_szNodeValue);						
						} 				
					}
					lovInfoVOList.add(objectVO);			   	
				}
			}
		} catch (DOMException e) {
			throw new SystemException(e);
		}
		if(lovInfoVOList != null)
			log	.debug("CustomerProfileXMLHandler > getLovInfoList > Done , List size = "+lovInfoVOList.size());
		
		return lovInfoVOList;
	}	
    
    public String generateMDMXMLRequest(String accountType,String accountNumber) {
    	log.debug("CustomerProfileXMLHandler > generateMDMXMLRequest > called for accountNumber::"+accountNumber);
    	String requestMessage = "";
   	 	try {
           Document doc = new DocumentImpl();
           
           Element root = doc.createElement(TagIfc.TAG_EJD + ":" + TagIfc.TAG_queryCustomerRq);
           root.setAttribute(TagIfc.TAG_xmlns + ":"+ TagIfc.TAG_EJD, TagIfc.TAG_EJADA_URL);
           root.setAttribute(TagIfc.TAG_xmlns + ":"+ TagIfc.TAG_mtosi_xsd, TagIfc.TAG_IBM_SCHEMA_URL1);
           root.setAttribute(TagIfc.TAG_xmlns + ":"+ TagIfc.TAG_ibm, TagIfc.TAG_IBM_SCHEMA_URL2);
           root.setAttribute(TagIfc.TAG_xmlns + ":"+ TagIfc.TAG_xsi, TagIfc.TAG_SCHEMA_URL);
           
           String srdate = FormatterUtility.FormateDate(new Date());
           
           Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_EJD + ":" + TagIfc.TAG_MsgRqHdr);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_RqUID,TagIfc.EI_Inquiry_RequestorChannelId+"_"+srdate);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_SCId,TagIfc.EI_Inquiry_RequestorChannelId);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_FUNC_ID, TagIfc.TAG_FuncId_40210000);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_RqMode, "0");
	       XMLUtility.closeParentTag(root, header);
           	
           //body
           	Element body = XMLUtility.openParentTag(doc, TagIfc.TAG_Body);
           		Element customer = XMLUtility.openParentTag(doc, TagIfc.TAG_IBM_CUSTOMER);
           			Element party = XMLUtility.openParentTag(doc, TagIfc.TAG_Party);
           				Element characteristicValue = XMLUtility.openParentTag(doc, TagIfc.TAG_CharacteristicValue);
           					Element characteristic = XMLUtility.openParentTag(doc, TagIfc.TAG_Characteristic);
           						XMLUtility.generateElelemt(doc, characteristic, TagIfc.TAG_Name,accountType);
           					XMLUtility.closeParentTag(characteristicValue,characteristic);
           					XMLUtility.generateElelemt(doc, characteristicValue, TagIfc.TAG_Value,accountNumber);
           				XMLUtility.closeParentTag(party, characteristicValue);
           			XMLUtility.closeParentTag(customer, party);
           		XMLUtility.closeParentTag(body, customer);
           	XMLUtility.closeParentTag(root, body);
           
           doc.appendChild(root);
           requestMessage = XMLUtility.serializeRequest(doc);

   	 	} catch(Exception e) {
   	 		log.debug("CustomerProfileXMLHandler > generateMDMXMLRequest > Exception::"+e.getMessage());
   	 		throw new SystemException(e);    
   	 	}
   	 	log.debug("CustomerProfileXMLHandler > generateMDMXMLRequest > ended for accountNumber::"+accountNumber);
   	 	return requestMessage;
       
    }
    
    public CustomerProfileReplyVO parseCustomerProfile (String xmlReply) {
		Document doc = XMLUtility.parseXMLString(xmlReply);
 		 		
 		Node rootNode = doc.getDocumentElement();

		NodeList nl = rootNode.getChildNodes();
		CustomerProfileReplyVO customerProfileReplyVO = new CustomerProfileReplyVO();
		CharacteristicsVO characteristicsVO = null;
		PartyExtensionsVO partyExtensionsVO = null;
		ContactMediumVO contactMediumVO = null;
		ContactPersonVO contactPersonVO = null;
		SpecificationVO specificationVO = null;
		PartyIdentificationVO partyIdentificationVO = null;
		
		for (int j = 0; j < nl.getLength(); j++) {
			if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
				continue;

			Element l_Node = (Element) nl.item(j);
			String p_szTagName = l_Node.getTagName();
			String l_szNodeValue = null;
			if (l_Node.getFirstChild() != null){
				l_szNodeValue = l_Node.getFirstChild().getNodeValue();
			}					
			if (TagIfc.TAG_MsgRsHdr.equalsIgnoreCase(p_szTagName)) {
				NodeList headerNodeList = l_Node.getChildNodes();			
				for(int i = 0; headerNodeList != null && i< headerNodeList.getLength(); i++){
					Element s_Node = (Element) headerNodeList.item(i);
					String s_szNodeTagName = s_Node.getTagName();
					
					if (TagIfc.TAG_StatusCode.equalsIgnoreCase(s_szNodeTagName)) {
						String statusCodeNodeValue = null;
						if (s_Node.getFirstChild() != null) {
							statusCodeNodeValue = s_Node.getFirstChild().getNodeValue();
							log.info("CustomerProfileXMLHandler > traverse > statusCodeNodeValue::"+statusCodeNodeValue);
							if(!ConstantIfc.MDM_SUCCESS_STATUS_CODE.equalsIgnoreCase(statusCodeNodeValue)) {
								throw new SystemException();
							}
						}
					}
				}
			} else if (TagIfc.TAG_Body.equalsIgnoreCase(p_szTagName)) {
				NodeList headerNodeList = l_Node.getChildNodes();			
				for(int i = 0; headerNodeList != null && i< headerNodeList.getLength(); i++){
					Element s_Node = (Element) headerNodeList.item(i);
									
					NodeList bodyNodeList = s_Node.getChildNodes();			
					for(int k = 0; bodyNodeList != null && k< bodyNodeList.getLength(); k++) {
						Element b_Node = (Element) bodyNodeList.item(k);
						if(TagIfc.TAG_Party.equalsIgnoreCase(b_Node.getTagName())) {
							NodeList partyNodeList = b_Node.getChildNodes();		
							
							partyExtensionsVO = new PartyExtensionsVO();
							contactMediumVO = new ContactMediumVO();
							contactPersonVO = new ContactPersonVO();
							characteristicsVO = new CharacteristicsVO();
							
							for(int m = 0; partyNodeList != null && m < partyNodeList.getLength(); m++) {
								
								Element p_Node = (Element) partyNodeList.item(m);
								String p_szNodeTagName = p_Node.getTagName();
													
								if (TagIfc.TAG_Id.equalsIgnoreCase(p_szNodeTagName)) {
									customerProfileReplyVO.setCustomerId(p_Node.getFirstChild().getNodeValue());
								} else if (TagIfc.TAG_CharacteristicValue.equalsIgnoreCase(p_szNodeTagName)) {
									characteristicsVO = parseCharacteristicNode(p_Node,characteristicsVO);
								} else if (TagIfc.TAG_Specification.equalsIgnoreCase(p_szNodeTagName)) {
									specificationVO = parseSpecificationNode(p_Node);
								} else if (TagIfc.TAG_PartyIdentification.equalsIgnoreCase(p_szNodeTagName)) {
									partyIdentificationVO = parsePartyIdentificationNode(p_Node);
								} else if (TagIfc.TAG_ContactMedium.equalsIgnoreCase(p_szNodeTagName)) {
									contactMediumVO = parseContactMediumNode(p_Node,contactMediumVO);
								} else if (TagIfc.TAG_ContactPerson.equalsIgnoreCase(p_szNodeTagName)) {
									contactPersonVO = parseContactPersonNode(p_Node,contactPersonVO);
								} else if (TagIfc.TAG_PartyExtensions.equalsIgnoreCase(p_szNodeTagName)) {
									partyExtensionsVO = parsePartyExtensionsNode(p_Node,partyExtensionsVO);
								}
							}
						}
					}
				}
			}
		}
		
		customerProfileReplyVO.setCharacteristicsVO(characteristicsVO);
		customerProfileReplyVO.setSpecificationVO(specificationVO);
		customerProfileReplyVO.setPartyIdentificationVO(partyIdentificationVO);
		
		String fullAddress = contactMediumVO.getAddressLineOne();
		if(!FormatterUtility.isEmpty(contactMediumVO.getAddressLineTwo())) {
			if(FormatterUtility.isEmpty(fullAddress))
				fullAddress = FormatterUtility.checkNull(contactMediumVO.getAddressLineTwo());
			else
				fullAddress = fullAddress + " "+FormatterUtility.checkNull(contactMediumVO.getAddressLineTwo());
		}
		
		if(!FormatterUtility.isEmpty(contactMediumVO.getAddressLineThree())) {
			if(FormatterUtility.isEmpty(fullAddress))
				fullAddress = FormatterUtility.checkNull(contactMediumVO.getAddressLineThree());
			else
				fullAddress = fullAddress + " "+FormatterUtility.checkNull(contactMediumVO.getAddressLineThree());
		}
		contactMediumVO.setFullAddress(fullAddress);
		customerProfileReplyVO.setContactMediumVO(contactMediumVO);
		
		
		String fullName = contactPersonVO.getGivenName();
		if(!FormatterUtility.isEmpty(contactPersonVO.getMiddleName())) {
			if(FormatterUtility.isEmpty(fullName))
				fullName = FormatterUtility.checkNull(contactPersonVO.getMiddleName());
			else
				fullName = fullName + " "+FormatterUtility.checkNull(contactPersonVO.getMiddleName());
		}
		
		if(!FormatterUtility.isEmpty(contactPersonVO.getThirdName())) {
			if(FormatterUtility.isEmpty(fullName))
				fullName = FormatterUtility.checkNull(contactPersonVO.getThirdName());
			else
				fullName = fullName + " "+FormatterUtility.checkNull(contactPersonVO.getThirdName());
		}
		
		if(!FormatterUtility.isEmpty(contactPersonVO.getFourthName())) {
			if(FormatterUtility.isEmpty(fullName))
				fullName = FormatterUtility.checkNull(contactPersonVO.getFourthName());
			else
				fullName = fullName + " "+FormatterUtility.checkNull(contactPersonVO.getFourthName());
		}
		
		contactPersonVO.setFullName(fullName);
		customerProfileReplyVO.setContactPersonVO(contactPersonVO);
		customerProfileReplyVO.setPartyExtensionsVO(partyExtensionsVO);
		
		
		return customerProfileReplyVO;
    } 
    
    public CharacteristicsVO parseCharacteristicNode(Node n,CharacteristicsVO characteristicsVO) {
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";

            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
    				if(TagIfc.TAG_Name.equalsIgnoreCase(p_szTagName)) {
        				preferredContactFlag = element.getFirstChild().getNodeValue();
        			} else if(TagIfc.TAG_Value.equalsIgnoreCase(p_szTagName)) {
        				if(ConstantIfc.PREFERRED_CONTACT_METHOD.equalsIgnoreCase(preferredContactFlag)) {
        					characteristicsVO.setPreferredContactMethod(element.getFirstChild().getNodeValue());
        				}
        			} 
    			}
    			
    			parseCharacteristicNode(nl.item(i),characteristicsVO);
            }
         }
    	
    	return characteristicsVO;
    }
    
    public SpecificationVO parseSpecificationNode (Node n) {
    	SpecificationVO specificationVO = null;
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";
            specificationVO = new SpecificationVO();
            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
   					if (TagIfc.TAG_Type.equalsIgnoreCase(p_szTagName)) {
   						specificationVO.setCustomerType(element.getFirstChild().getNodeValue());
	    			} else if (TagIfc.TAG_Category.equalsIgnoreCase(p_szTagName)) {
	    				
	    				String category = element.getFirstChild().getNodeValue();
	  					if(ConstantIfc.CUSTOMER_CATEGORY_TYPE_O.equalsIgnoreCase(category)) {
	  						category = ConstantIfc.CUSTOMER_CATEGORY_BUSINESS;
	  					}
	  					specificationVO.setCustomerCategory(category);
	    				
	    			} 
    			}
            }
         }
    	
    	return specificationVO;
    }
    
    public PartyIdentificationVO parsePartyIdentificationNode (Node n) {
    	PartyIdentificationVO partyIdentificationVO = null;
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";
            
            partyIdentificationVO = new PartyIdentificationVO();

            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
   					if (TagIfc.TAG_Id.equalsIgnoreCase(p_szTagName)) {
   						partyIdentificationVO.setCompanyIDNumber(element.getFirstChild().getNodeValue());
	    			} 
    			}
            }
         }
    	
    	return partyIdentificationVO;
    }
    
    public ContactMediumVO parseContactMediumNode (Node n, ContactMediumVO contactMediumVO) {
    	
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";

            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
    				//System.out.println("parsecontact medium node="+p_szTagName);
    				if(TagIfc.TAG_Name.equalsIgnoreCase(p_szTagName)) {
		  				if (element.getFirstChild() != null) {
		  					preferredContactFlag = element.getFirstChild().getNodeValue();
		  				} 
    				} else if(TagIfc.TAG_Value.equalsIgnoreCase(p_szTagName)) {
        				if(TagIfc.TAG_PreferredAddressFlag.equalsIgnoreCase(preferredContactFlag)) {
    						preferredAddressIndicator = element.getFirstChild().getNodeValue();
    					} else if(TagIfc.TAG_SolicitationIndicatorFlag.equalsIgnoreCase(preferredContactFlag)) {
        					solicitationIndicator = element.getFirstChild().getNodeValue();
        				}
        			} else if(TagIfc.TAG_AddressLineOne.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
    					contactMediumVO.setAddressLineOne(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_AddressLineTwo.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
        				contactMediumVO.setAddressLineTwo(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_AddressLineThree.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
        				contactMediumVO.setAddressLineThree(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_StateOrProvince.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
        				contactMediumVO.setStateOrProvince(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_Country.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
        				contactMediumVO.setCountry(element.getFirstChild().getNodeValue());			
        			} else if(TagIfc.TAG_Postcode.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
        				contactMediumVO.setPostCode(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_POBox.equalsIgnoreCase(p_szTagName) && ConstantIfc.VALUE_Y.equalsIgnoreCase(preferredAddressIndicator)) {
        				contactMediumVO.setPoBox(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_EmailAddress.equalsIgnoreCase(p_szTagName)) {
        				contactMediumVO.setEmailAddress(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_Number.equalsIgnoreCase(p_szTagName)) {
        				contactNumber = element.getFirstChild().getNodeValue();
          			} else if(TagIfc.TAG_Type.equalsIgnoreCase(p_szTagName)) {
          				if(ConstantIfc.VALUE_N.equalsIgnoreCase(solicitationIndicator)) {
        					contactType = element.getFirstChild().getNodeValue();
        					if(TagIfc.TAG_Fax.equalsIgnoreCase(contactType)) {
        						contactMediumVO.setFaxNumber(contactNumber);
  		  					} else if(TagIfc.TAG_Mobile.equalsIgnoreCase(contactType)) {
  		  						contactMediumVO.setMobileNumber(contactNumber);
  		  					} else if(TagIfc.TAG_Phone.equalsIgnoreCase(contactType)) {
  		  						contactMediumVO.setPhoneNumber(contactNumber);
  		  					}
          				}
        			} 
    			}
    			
    			parseContactMediumNode(nl.item(i),contactMediumVO);
            }
         }
    	
    	return contactMediumVO;
    }
    
    public ContactPersonVO parseContactPersonNode (Node n,ContactPersonVO contactPersonVO) {
    	
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";

            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
    				//System.out.println("parsecontact medium node="+p_szTagName);
    				if(TagIfc.TAG_Name.equalsIgnoreCase(p_szTagName)) {
        				preferredContactFlag = element.getFirstChild().getNodeValue();
        			} else if(TagIfc.TAG_Value.equalsIgnoreCase(p_szTagName)) {
        				if(TagIfc.TAG_SolicitationIndicatorFlag.equalsIgnoreCase(preferredContactFlag)) {
        					solicitationIndicator = element.getFirstChild().getNodeValue();
        				}
        			} else if(TagIfc.TAG_GivenName.equalsIgnoreCase(p_szTagName)) {
    					contactPersonVO.setGivenName(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_MiddleName.equalsIgnoreCase(p_szTagName)) {
          				contactPersonVO.setMiddleName(element.getFirstChild().getNodeValue());
          			} else if(TagIfc.TAG_ThirdName.equalsIgnoreCase(p_szTagName)) {
          				contactPersonVO.setThirdName(element.getFirstChild().getNodeValue());
          			} else if(TagIfc.TAG_FourthName.equalsIgnoreCase(p_szTagName)) {
          				contactPersonVO.setFourthName(element.getFirstChild().getNodeValue());
          			} else if(TagIfc.TAG_FamilyName.equalsIgnoreCase(p_szTagName)) {
          				contactPersonVO.setFamilyName(element.getFirstChild().getNodeValue());
          			} else if(TagIfc.TAG_Gender.equalsIgnoreCase(p_szTagName)) {
          				contactPersonVO.setGender(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_Nationality.equalsIgnoreCase(p_szTagName)) {
        				contactPersonVO.setNationality(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_DateOfBirth.equalsIgnoreCase(p_szTagName)) {
        				contactPersonVO.setDateOfBirth(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_EmailAddress.equalsIgnoreCase(p_szTagName)) {
        				contactPersonVO.setCapEmailAddress(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_Number.equalsIgnoreCase(p_szTagName)) {
        				contactNumber = element.getFirstChild().getNodeValue();
        				//System.out.println("contactNumber="+contactNumber);
          			} else if(TagIfc.TAG_Type.equalsIgnoreCase(p_szTagName)) {
          				//System.out.println("solicitationIndicator="+solicitationIndicator);
          				if(ConstantIfc.VALUE_N.equalsIgnoreCase(solicitationIndicator)) {
        					contactType = element.getFirstChild().getNodeValue();
        					if(TagIfc.TAG_Fax.equalsIgnoreCase(contactType)) {
        						contactPersonVO.setCapFaxNumber(contactNumber);
  		  					} else if(TagIfc.TAG_Mobile.equalsIgnoreCase(contactType)) {
  		  						contactPersonVO.setCapMobileNumber(contactNumber);
  		  					} else if(TagIfc.TAG_Phone.equalsIgnoreCase(contactType)) {
  		  						contactPersonVO.setCapPhoneNumber(contactNumber);
  		  					}
          				}
        			} 
    			}
    			
    			parseContactPersonNode(nl.item(i),contactPersonVO);
            }
         }
    	
    	return contactPersonVO;
    }    
    
    public PartyExtensionsVO parsePartyExtensionsNode (Node n,PartyExtensionsVO partyExtensionsVO) {
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";

            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
   					
    				if(TagIfc.TAG_TradingName.equalsIgnoreCase(p_szTagName)) {
    					partyExtensionsVO.setTradingName(element.getFirstChild().getNodeValue());
        			} else if(TagIfc.TAG_Id.equalsIgnoreCase(p_szTagName)) {
        				companyAccountNumber = element.getFirstChild().getNodeValue();
          			} else if(TagIfc.TAG_IdType.equalsIgnoreCase(p_szTagName)) {
		  				if(TagIfc.TAG_VALUE_1.equalsIgnoreCase(element.getFirstChild().getNodeValue())) {
		  					partyExtensionsVO.setCompanyAccountNumber(companyAccountNumber);
	  					}
 				
        			} else if(TagIfc.TAG_Employees.equalsIgnoreCase(p_szTagName)) {
    					String numberOfEmployees = element.getFirstChild().getNodeValue();
    					if(!FormatterUtility.isEmpty(numberOfEmployees) && numberOfEmployees.indexOf(".") != -1) {
    						numberOfEmployees = numberOfEmployees.substring(0, numberOfEmployees.indexOf("."));
    					}
    					partyExtensionsVO.setNoOfEmployees(numberOfEmployees);
        			} else if(TagIfc.TAG_KeyAccountManagerPF.equalsIgnoreCase(p_szTagName)) {
        				partyExtensionsVO.setKeyAccountManagerPF(element.getFirstChild().getNodeValue());
          			} else if(TagIfc.TAG_KeyAccountManagerEmail.equalsIgnoreCase(p_szTagName)) {
          				partyExtensionsVO.setKeyAccountManagerEmail(element.getFirstChild().getNodeValue());
          			} else if(TagIfc.TAG_NumberofOffices.equalsIgnoreCase(p_szTagName)) {
    					String numberOfOffices = element.getFirstChild().getNodeValue();
    					if(!FormatterUtility.isEmpty(numberOfOffices) && numberOfOffices.indexOf(".") != -1) {
    						numberOfOffices = numberOfOffices.substring(0, numberOfOffices.indexOf("."));
    					}
    					partyExtensionsVO.setNoOfOffices(numberOfOffices);
        			}
    			}
    			
    			parsePartyExtensionsNode(nl.item(i),partyExtensionsVO);
            }
         }
    	
    	return partyExtensionsVO;
    }        

    public String generateXMLRequestForMasterBillingAcc(String accountNumber) {
    	log.debug("CustomerProfileXMLHandler > generateXMLRequestForMasterBillingAcc > called for accountNumber::"+accountNumber);
    	String requestMessage = "";
   	 	try {
           Document doc = new DocumentImpl();
           
           Element root = doc.createElement(TagIfc.TAG_queryBillingAccountRq);
           
           String srdate = FormatterUtility.FormateDate(new Date());
           
           Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_MsgRqHdr);
           XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID, "41202040");
           XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SCId,TagIfc.EI_Inquiry_RequestorChannelId);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_RqUID,"HMG"+srdate);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_LangPref,"En");

	       XMLUtility.closeParentTag(root, header);
           	
           //body
           	Element body = XMLUtility.openParentTag(doc, TagIfc.TAG_Body);
           		Element customer = XMLUtility.openParentTag(doc, TagIfc.TAG_customerAccount);
           			Element agreementItem = XMLUtility.openParentTag(doc, TagIfc.TAG_AgreementItem);
           				Element customerAccountId = XMLUtility.openParentTag(doc, TagIfc.TAG_CustomerAccountId);
           						XMLUtility.generateElelemt(doc, customerAccountId, TagIfc.TAG_Id,accountNumber);
           				XMLUtility.closeParentTag(agreementItem, customerAccountId);
           			XMLUtility.closeParentTag(customer, agreementItem);
           		XMLUtility.closeParentTag(body, customer);
           	XMLUtility.closeParentTag(root, body);
           
           doc.appendChild(root);
           requestMessage = XMLUtility.serializeRequest(doc);

   	 	} catch(Exception e) {
   	 		log.debug("CustomerProfileXMLHandler > generateXMLRequestForMasterBillingAcc > Exception::"+e.getMessage());
   	 		throw new SystemException(e);    
   	 	}
   	 	log.debug("CustomerProfileXMLHandler > generateXMLRequestForMasterBillingAcc > ended for accountNumber::"+accountNumber);
   	
   	 	return requestMessage;
       
    }
    
    public CustomerProfileReplyVO parseAgreementItemNode(Node n,CustomerProfileReplyVO customerProfileReplyVO) {
    	
    	if (n.hasChildNodes()) {
            // Get the children in a list.
            NodeList nl = n.getChildNodes();
            int size = nl.getLength();
            String p_szTagName = "";

            for (int i = 0; i < size; i++) {
            	if (nl.item(i).getNodeType() != Node.ELEMENT_NODE)
    				continue;

            	Element element = (Element) nl.item(i);
    			p_szTagName = element.getTagName();
    			if (element.getFirstChild() != null) {
    				if(TagIfc.TAG_Name.equalsIgnoreCase(p_szTagName)) {
    					masterBillingAccountNumberFlag = element.getFirstChild().getNodeValue();
        			} else if(TagIfc.TAG_Value.equalsIgnoreCase(p_szTagName)) {
        				if(ConstantIfc.MASTER_BA_FLAG.equalsIgnoreCase(masterBillingAccountNumberFlag))
        					customerProfileReplyVO.setIsMasterBilingAccount(element.getFirstChild().getNodeValue());
        			} 
    			}
    			
    			parseAgreementItemNode(nl.item(i),customerProfileReplyVO);
            }
         }
    	
    	return customerProfileReplyVO;
    }
    
    public CustomerProfileReplyVO parseXMLForMasterBillingAcc (String xmlReply) throws MobilyCommonException {
		Document doc = XMLUtility.parseXMLString(xmlReply);
 		 		
 		Node rootNode = doc.getDocumentElement();

		NodeList nl = rootNode.getChildNodes();
		CustomerProfileReplyVO customerProfileReplyVO = new CustomerProfileReplyVO();
		
		
		for (int j = 0; j < nl.getLength(); j++) {
			if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
				continue;

			Element l_Node = (Element) nl.item(j);
			String p_szTagName = l_Node.getTagName();
			String l_szNodeValue = null;
			if (l_Node.getFirstChild() != null){
				l_szNodeValue = l_Node.getFirstChild().getNodeValue();
			}					
			if (TagIfc.TAG_MsgRsHdr.equalsIgnoreCase(p_szTagName)) {
				NodeList headerNodeList = l_Node.getChildNodes();			
				for(int i = 0; headerNodeList != null && i< headerNodeList.getLength(); i++){
					Element s_Node = (Element) headerNodeList.item(i);
					String s_szNodeTagName = s_Node.getTagName();
					
					if (TagIfc.TAG_StatusCode.equalsIgnoreCase(s_szNodeTagName)) {
						String statusCodeNodeValue = null;
						if (s_Node.getFirstChild() != null) {
							statusCodeNodeValue = s_Node.getFirstChild().getNodeValue();
							log.info("CustomerProfileXMLHandler > parseXMLForMasterBillingAcc > statusCodeNodeValue::"+statusCodeNodeValue);
							if(!ConstantIfc.MDM_SUCCESS_STATUS_CODE.equalsIgnoreCase(statusCodeNodeValue)) {
								throw new MobilyCommonException(statusCodeNodeValue);
							}
						}
					}
				}
			} else if (TagIfc.TAG_Body.equalsIgnoreCase(p_szTagName)) {
				NodeList bodyNodeList = l_Node.getChildNodes();			
				for(int i = 0; bodyNodeList != null && i< bodyNodeList.getLength(); i++){
					
						Element b_Node = (Element) bodyNodeList.item(i);
						
						NodeList customerAccountNodeList = b_Node.getChildNodes();		
						
					
						for(int m = 0; customerAccountNodeList != null && m < customerAccountNodeList.getLength(); m++) {
							Element p_Node = (Element) customerAccountNodeList.item(m);
							String p_szNodeTagName = p_Node.getTagName();
														
							if (TagIfc.TAG_Specification.equalsIgnoreCase(p_szNodeTagName)) {
								NodeList specificationNodeList = p_Node.getChildNodes();			
								for(int n = 0; specificationNodeList != null && n< specificationNodeList.getLength(); n++){
									Element specification_Node = (Element) specificationNodeList.item(n);
									String specification_szNodeTagName = specification_Node.getTagName();
									
									if (TagIfc.TAG_Type.equalsIgnoreCase(specification_szNodeTagName)) {
										String typeNodeValue = null;
										if (specification_Node.getFirstChild() != null) {
											typeNodeValue = specification_Node.getFirstChild().getNodeValue();
											log.info("CustomerProfileXMLHandler > parseXMLForMasterBillingAcc > typeNodeValue::"+typeNodeValue);
											customerProfileReplyVO.setAccountType(typeNodeValue);
										}
									}
								}
							} else if (TagIfc.TAG_AgreementItem.equalsIgnoreCase(p_szNodeTagName)) {
								customerProfileReplyVO = parseAgreementItemNode(p_Node,customerProfileReplyVO);
							} 
						}
					
				}
			}
		}
		
		
		
		
		return customerProfileReplyVO;
    } 
    
    public String generateXMLRequestToValidateCustomerId(String msisdn,String idNumber) {
    	log.debug("CustomerProfileXMLHandler > generateXMLRequestToValidateCustomerId > called for msisdn::"+msisdn+" and idNumber::"+idNumber);
    	String requestMessage = "";
   	 	try {
           Document doc = new DocumentImpl();
           
           Element root = doc.createElement(TagIfc.TAG_EJD + ":" + TagIfc.TAG_queryCustomerRq);
           root.setAttribute(TagIfc.TAG_xmlns + ":"+ TagIfc.TAG_ibm, TagIfc.TAG_IBM_SCHEMA_URL2);
           root.setAttribute(TagIfc.TAG_xmlns + ":"+ TagIfc.TAG_EJD, TagIfc.TAG_EJADA_URL);
           
           String srdate = FormatterUtility.FormateDate(new Date());
           
           Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_EJD + ":" + TagIfc.TAG_MsgRqHdr);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_RqUID,TagIfc.EI_Inquiry_RequestorChannelId+"_"+srdate);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_SCId,TagIfc.EI_Inquiry_RequestorChannelId);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_FUNC_ID, TagIfc.TAG_FuncId_40210010);
	       XMLUtility.generateElelemt(doc, header, TagIfc.TAG_EJD + ":" + TagIfc.TAG_RqMode, "0");
	       XMLUtility.closeParentTag(root, header);
           	
           //body
           	Element body = XMLUtility.openParentTag(doc, TagIfc.TAG_Body);
           		Element customer = XMLUtility.openParentTag(doc, TagIfc.TAG_IBM_CUSTOMER);
           			Element party = XMLUtility.openParentTag(doc, TagIfc.TAG_Party);
           				Element characteristicValue = XMLUtility.openParentTag(doc, TagIfc.TAG_CharacteristicValue);
           					Element characteristic = XMLUtility.openParentTag(doc, TagIfc.TAG_Characteristic);
           						XMLUtility.generateElelemt(doc, characteristic, TagIfc.TAG_Name,TagIfc.TAG_MSISDN);
           					XMLUtility.closeParentTag(characteristicValue,characteristic);
           					XMLUtility.generateElelemt(doc, characteristicValue, TagIfc.TAG_Value,msisdn);
           				XMLUtility.closeParentTag(party, characteristicValue);
           				
           				Element partyIdentification = XMLUtility.openParentTag(doc, TagIfc.TAG_PartyIdentification);
       						XMLUtility.generateElelemt(doc, partyIdentification, TagIfc.TAG_Id,idNumber);
       						XMLUtility.generateElelemt(doc, partyIdentification, TagIfc.TAG_Specification,"");
       					
       				XMLUtility.closeParentTag(party, partyIdentification);
           				
           			XMLUtility.closeParentTag(customer, party);
           		XMLUtility.closeParentTag(body, customer);
           	XMLUtility.closeParentTag(root, body);
           
           doc.appendChild(root);
           requestMessage = XMLUtility.serializeRequest(doc);

   	 	} catch(Exception e) {
   	 		log.debug("CustomerProfileXMLHandler > generateXMLRequestToValidateCustomerId > Exception::"+e.getMessage());
   	 		throw new SystemException(e);    
   	 	}
   	 	log.debug("CustomerProfileXMLHandler > generateXMLRequestToValidateCustomerId > ended for msisdn::"+msisdn+" and idNumber::"+idNumber);
   	 	return requestMessage;
       
    }
    
    public String parseXMLToValidateCustomerId (String xmlReply) throws MobilyCommonException {
		Document doc = XMLUtility.parseXMLString(xmlReply);
 		 		
 		Node rootNode = doc.getDocumentElement();

		NodeList nl = rootNode.getChildNodes();
		String returnCode = null;
		
		for (int j = 0; j < nl.getLength(); j++) {
			if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
				continue;
			Element l_Node = (Element) nl.item(j);
			String p_szTagName = l_Node.getTagName();
			String l_szNodeValue = null;
			if (l_Node.getFirstChild() != null){
				l_szNodeValue = l_Node.getFirstChild().getNodeValue();
			}	
			
			if (p_szTagName.indexOf(TagIfc.TAG_MsgRsHdr)  != -1) {
				
				NodeList headerNodeList = l_Node.getChildNodes();			
				for(int i = 0; headerNodeList != null && i< headerNodeList.getLength(); i++){
					Element s_Node = (Element) headerNodeList.item(i);
					String s_szNodeTagName = s_Node.getTagName();
					if (s_szNodeTagName.indexOf(TagIfc.TAG_StatusCode)  != -1 ) {
						
						if (s_Node.getFirstChild() != null) {
							returnCode = s_Node.getFirstChild().getNodeValue();
							log.debug("CustomerProfileXMLHandler > parseXMLToValidateCustomerId > inside returnCode::"+returnCode);
						}
					}
				}
			} 
		}
		
		log.info("CustomerProfileXMLHandler > parseXMLToValidateCustomerId > returnCode::"+returnCode);
		
		return returnCode;
    } 
    
	public static void main(String[] args) {
		CustomerProfileXMLHandler xmlHandler = new CustomerProfileXMLHandler();
		//String str= "<validateCustomerRs><MsgRsHdr><StatusCode>I000000</StatusCode><RqUID>eportal_20120718120715</RqUID></MsgRsHdr></validateCustomerRs>";
		String str = "";
		CustomerProfileReplyVO customerProfileVO = xmlHandler.parseCustomerProfile(str);
//		System.out.println(customerProfileVO);
		//String s = xmlHandler.generateXMLRequestToValidateCustomerId("966540510111","11111");
		//System.out.println("s="+s);
		
		/*CustomerProfileReplyVO customerProfileVO = null;
		try {
			String code = xmlHandler.parseXMLToValidateCustomerId(str);
			System.out.println("code="+code);
		} catch (Exception e) {
			System.out.println("error code="+e);
		}*/
		//System.out.println("account type="+customerProfileVO.getCustomerId());
		//System.out.println("is master ba="+customerProfileVO.getIsMasterBilingAccount());
	}
}
