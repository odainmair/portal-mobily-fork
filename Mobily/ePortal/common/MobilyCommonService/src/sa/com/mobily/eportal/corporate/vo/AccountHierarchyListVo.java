package sa.com.mobily.eportal.corporate.vo;

import java.util.List;

import sa.com.mobily.eportal.common.service.vo.BaseVO;
import sa.com.mobily.eportal.corporate.model.AccountHierarchy;

public class AccountHierarchyListVo extends BaseVO
{
	private static final long serialVersionUID = 2652204055707804325L;
	
	private List<AccountHierarchy> pageList;
	
	private Integer totalCount;

	public List<AccountHierarchy> getPageList()
	{
		return pageList;
	}

	public void setPageList(List<AccountHierarchy> pageList)
	{
		this.pageList = pageList;
	}

	public Integer getTotalCount()
	{
		return totalCount;
	}

	public void setTotalCount(Integer totalCount)
	{
		this.totalCount = totalCount;
	}
}