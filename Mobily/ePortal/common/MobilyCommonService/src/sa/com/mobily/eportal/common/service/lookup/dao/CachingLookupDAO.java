package sa.com.mobily.eportal.common.service.lookup.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.service.lookup.common.CachingLookupVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.service.DataSources;

public class CachingLookupDAO
{
	private Connection connection;

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private DataSources dataSourceName = DataSources.DEFAULT;// MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB);

	private Statement statement;

	private ResultSet resultSet;

	private String clazz = CachingLookupDAO.class.getName();

	private DataSource lookup(String jndiName) throws NamingException
	{
		InitialContext ic = new InitialContext();
		DataSource dataSource = (DataSource) ic.lookup(jndiName);
		return dataSource;
	}

	private Connection getConnection()
	{
		String methodName = "getConnection";
		try
		{
			DataSource ds = lookup(dataSourceName.toString());
			return ds.getConnection();
		}
		catch (NamingException e)
		{
			executionContext.log(Level.INFO, e.getMessage(), clazz, methodName, e);
			throw new IllegalStateException("Couldn't lookup: " + dataSourceName + ", due to " + e.getMessage());
		}
		catch (SQLException e)
		{
			executionContext.log(Level.INFO, e.getMessage(), clazz, methodName, e);
			throw new IllegalStateException("Couldn't lookup: " + dataSourceName + ", due to " + e.getMessage());
		}
	}

	public CachingLookupVO getCachingLookupVOFromDB(String lookup_type, String code)
	{
		String methodName = "getCachingLookupVOFromDB";
		connection = getConnection();
		String sqlQuery = "select * from mobily_catalog_lookup_tbl where mobily_catalog_lookup_tbl.code='" + code + "' and mobily_catalog_lookup_tbl.type = '" + lookup_type + "'";
		CachingLookupVO cachingLookupVO = new CachingLookupVO(lookup_type, code, "Short_Name", "English_Name", "Arabic_Name");
		try
		{
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlQuery);

			executionContext.log(Level.INFO, "call the DB", clazz, methodName, null);
			if (resultSet.next())
			{
				cachingLookupVO = new CachingLookupVO(resultSet.getString("TYPE"), resultSet.getString("Code"), resultSet.getString("Short_Name"),
						resultSet.getString("English_Name"), resultSet.getString("Arabic_Name"));
				executionContext.log(Level.INFO, "got the cache object", clazz, methodName, null);
				return cachingLookupVO;
			}

		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			executionContext.log(Level.SEVERE, "Exception : " + e.getMessage(), clazz, methodName, e);
			// return cachingLookupVO;
			throw new BackEndException(e.getMessage(), e.getCause());
		}
		return cachingLookupVO;
	}

	public ArrayList<CachingLookupVO> getCachingLookupVOListFromDB(String lookup_type)
	{
		String methodName = "getCachingLookupVOListFromDB";
		connection = getConnection();// DataBaseConnectionHandler.getDBConnection(dataSourceName);
		String sqlQuery = "select * from mobily_catalog_lookup_tbl where mobily_catalog_lookup_tbl.type = '" + lookup_type + "'";
		ArrayList<CachingLookupVO> cachingLookupVOlist = null;
		try
		{
			statement = connection.createStatement();
			resultSet = statement.executeQuery(sqlQuery);
			executionContext.log(Level.INFO, "call the DB", clazz, methodName, null);
			while (resultSet.next())
			{
				cachingLookupVOlist.add(new CachingLookupVO(resultSet.getString("TYPE"), resultSet.getString("Code"), resultSet.getString("Short_Name"), resultSet
						.getString("English_Name"), resultSet.getString("Arabic_Name")));
			}
			return cachingLookupVOlist;
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			executionContext.log(Level.SEVERE, "Exception : " + e.getMessage(), clazz, methodName, e);
			// return cachingLookupVOlist;
			throw new BackEndException(e.getMessage(), e.getCause());
		}

	}
	/*
	 * public Connection getLocalConnection() { Connection connection = null;
	 * try { Driver d = (Driver)Class.forName( "oracle.jdbc.driver.OracleDriver"
	 * ).newInstance(); connection =
	 * DataBaseConnectionHandler.getDBConnection(dataSourceName);
	 * //DriverManager.getConnection(
	 * "jdbc:oracle:thin:@10.14.170.167:EPLDEV","pcbeusr","pcbeusr") ; }catch
	 * (InstantiationException e) { // TODO Auto-generated catch block throw new
	 * SystemException(e); } catch (IllegalAccessException e) { // TODO
	 * Auto-generated catch block throw new SystemException(e); } catch
	 * (ClassNotFoundException e) { // TODO Auto-generated catch block throw new
	 * SystemException(e); } return connection; }
	 */
}
