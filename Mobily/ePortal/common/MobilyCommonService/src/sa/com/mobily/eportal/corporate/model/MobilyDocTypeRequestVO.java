package sa.com.mobily.eportal.corporate.model;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MobilyDocTypeRequestVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	private int LINEID;
	private String docType;
	
	public int getLINEID()
	{
		return LINEID;
	}
	public void setLINEID(int lINEID)
	{
		LINEID = lINEID;
	}
	/**
	 * @return the docType
	 */
	public String getDocType()
	{
		return docType;
	}
	/**
	 * @param docType the docType to set
	 */
	public void setDocType(String docType)
	{
		this.docType = docType;
	}
	
}
