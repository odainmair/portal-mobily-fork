/**
 * 
 */
package sa.com.mobily.eportal.action.audit.vo;

import java.util.Date;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author m.hassan.dar
 * 
 */
public class ActionAuditingRequestVO extends BaseVO
{

	private static final long serialVersionUID = 6397332555809858673L;

	public ActionAuditingRequestVO()
	{
		// TODO Auto-generated constructor stub
	}

	private Integer actionId = null;

	private String san;

	private String username;

	private Integer status = null;

	private String errorCode;

	private String errorMessage;

	private Date actionDate;

	public Integer getActionId()
	{
		return actionId;
	}

	public void setActionId(Integer actionId)
	{
		this.actionId = actionId;
	}

	public String getSan()
	{
		return san;
	}

	public void setSan(String san)
	{
		this.san = san;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public Integer getStatus()
	{
		return status;
	}

	public void setStatus(Integer status)
	{
		this.status = status;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public Date getActionDate()
	{
		return actionDate;
	}

	public void setActionDate(Date actionDate)
	{
		this.actionDate = actionDate;
	}

}
