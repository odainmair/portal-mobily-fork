/**
 * 
 */
package sa.com.mobily.eportal.common.service.action.auditing;

import sa.com.mobily.eportal.action.audit.vo.ActionAuditingRequestVO;
import sa.com.mobily.eportal.action.auditing.service.jms.ActionAuditingJMSRepository;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/**
 * @author m.hassan.dar
 * 
 */
public class ActionAuditingService
{

	private static ActionAuditingService instance = new ActionAuditingService();

	private static final String className = ActionAuditingService.class.getName();

	static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("action-auditing/log");

	protected ActionAuditingService()
	{

	}

	public static synchronized ActionAuditingService getInstance()
	{
		if (instance == null)
		{
			instance = new ActionAuditingService();
		}
		return instance;
	}

	public void auditAction(ActionAuditingRequestVO requestVO) throws Exception
	{
		ActionAuditingJMSRepository.getInstance().auditAction(requestVO);
	}
}
