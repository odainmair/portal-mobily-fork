/*
 * Created on Feb 7, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.mail;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;


import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.exception.SystemException;

import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.EmailVO;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ApacheMailSender {

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.EMAIL_SENDER_SERVICE_LOGGER_NAME);
	
	/**
     * Sends a Simple Emial
     * 
     * @param emailVO
     */
    public void sendSimpleEmail(EmailVO emailVO) {

    	executionContext.info("ApacheMailSender > sendSimpleEmail > Start...");

        SimpleEmail email = new SimpleEmail();

        try {
            email.setCharset("UTF-8");
            email.setHostName(MobilyUtility.getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME));
            executionContext.info("ApacheMailSender > sendSimpleEmail > SMTP HOST : "+ email.getHostName());
            email.setSubject(emailVO.getSubject());
            email.setFrom(emailVO.getSender());
            email.setMsg(emailVO.getMailBody());

            if (emailVO.getRecipients() != null) {
                int numOfRecipients = emailVO.getRecipients().size();
                for (int i = 0; i < numOfRecipients; i++) {
                    email.addTo((String) emailVO.getRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendSimpleEmail > recipent Added");
            }
            
            email.send();
            executionContext.info("ApacheMailSender > sendSimpleEmail > mail Sent successfuly");
        } catch (EmailException e) {
            executionContext.severe("ApacheMailSender > sendSimpleEmail > EmailException ,"+ e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            executionContext.severe("ApacheMailSender > sendSimpleEmail > Exception ,"+ e.getMessage());
            throw new SystemException(e);
        }

        executionContext.info("ApacheMailSender > sendSimpleEmail > End..");
    }

	
    /**
     * Sends an email with attachement
     * 
     * @param emailVO
     */
    public void sendEmailWithAttachement(EmailVO emailVO) {
        executionContext.info("ApacheMailSender > sendEmailWithAttachement > called");
        //define the mail requirement
        MultiPartEmail email = new MultiPartEmail();
        try {
            email.setCharset("UTF-8");

            if (emailVO.getAttachementFileInputStream() != null) {
                ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(
                emailVO.getAttachementFileInputStream(), emailVO.getContentType());
                email.attach(byteArrayDataSource, emailVO.getAttachmentFileName(), "Attachment");
                executionContext.info("ApacheMailSender > sendEmailWithAttachement > add Attchement ["+ emailVO.getAttachmentFileName() + "] to the email");
            }

            email.setHostName(MobilyUtility.getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME));
            executionContext.info("ApacheMailSender > sendEmailWithAttachement > SMTP HOST : "+ email.getHostName());
            email.setSubject(emailVO.getSubject());
            email.setFrom(emailVO.getSender());
            email.setMsg(emailVO.getMailBody());

            if (emailVO.getRecipients() != null) {
                for (int i = 0; i < emailVO.getRecipients().size(); i++) {
                    email.addTo((String) emailVO.getRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendEmailWithAttachement > recipent Added");
            }

            if (emailVO.getCcRecipients() != null) {
                for (int i = 0; i < emailVO.getCcRecipients().size(); i++) {
                    email.addTo((String) emailVO.getCcRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendEmailWithAttachement > cc recipient added");
            }

            email.send();
            executionContext.info("ApacheMailSender > sendEmailWithAttachement > mail Sent successfuly");
        } catch (IOException e) {
            executionContext.severe("ApacheMailSender > sendEmailWithAttachement > IOException ,"+ e.getMessage());
            throw new ServiceException(e);

        } catch (EmailException e) {
            executionContext.severe("ApacheMailSender > sendEmailWithAttachement > EmailException ,"+ e.getMessage());
            throw new ServiceException(e);
        } catch (Exception e) {
            executionContext.severe("ApacheMailSender > sendEmailWithAttachement > Exception ,"+ e.getMessage());
            throw new ServiceException(e);
        }
    }

    /**
     * Sends an email with attachement
     * 
     * @param emailVO
     */
    public void sendEmailWithAttachements(EmailVO emailVO) {
        executionContext.info("ApacheMailSender > sendEmailWithAttachements > called");
        //define the mail requirement
        MultiPartEmail email = new MultiPartEmail();
        try {
            email.setCharset("UTF-8");
            
            Map attachmentsMap = emailVO.getAttachements();
            if(attachmentsMap != null) {
            	Iterator attachmentsIterator =  attachmentsMap.keySet().iterator();
        	    while (attachmentsIterator.hasNext()) {
        	        String attachmentFileName = (String) attachmentsIterator.next();
        	        executionContext.info("ApacheMailSender > sendEmailWithAttachements > attachmentFileName="+attachmentFileName);
        	        
        	        InputStream fileAttachmentInputStream = (InputStream)attachmentsMap.get(attachmentFileName);
        	        executionContext.info("ApacheMailSender > sendEmailWithAttachements > fileAttachmentInputStream="+fileAttachmentInputStream);
        	        
        	        ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(
        	        		fileAttachmentInputStream, emailVO.getContentType());
    	                email.attach(byteArrayDataSource, attachmentFileName, "Attachment");
    	                executionContext.info("ApacheMailSender > sendEmailWithAttachements > add Attchement ["+ attachmentFileName + "] to the email");
        	    }
            }
            email.setHostName(MobilyUtility.getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME));
            executionContext.info("ApacheMailSender > sendEmailWithAttachements > SMTP HOST : "+ email.getHostName());
            email.setSubject(emailVO.getSubject());
            email.setFrom(emailVO.getSender());
            email.setMsg(emailVO.getMailBody());
           
            if (emailVO.getRecipients() != null) {
                for (int i = 0; i < emailVO.getRecipients().size(); i++) {
                    email.addTo((String) emailVO.getRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendEmailWithAttachements > recipent Added");
            }

            if (emailVO.getCcRecipients() != null) {
                for (int i = 0; i < emailVO.getCcRecipients().size(); i++) {
                    email.addTo((String) emailVO.getCcRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendEmailWithAttachements > cc recipient added");
            }

            email.send();
            executionContext.info("ApacheMailSender > sendEmailWithAttachements > mail Sent successfuly");
        } catch (IOException e) {
            executionContext.severe("ApacheMailSender > sendEmailWithAttachement > IOException ,"+ e.getMessage());
            throw new ServiceException(e);
        } catch (EmailException e) {
            executionContext.severe("ApacheMailSender > sendEmailWithAttachement > EmailException ,"+ e.getMessage());
            throw new ServiceException(e);
        } catch (Exception e) {
            executionContext.severe("ApacheMailSender > sendEmailWithAttachement > Exception ,"+ e.getMessage());
            throw new ServiceException(e);
        }
    }
    
    
    /**
     * Special case for Investor relation
     * 
     * @param emailVO
     */
    public void sendSeperateListEmailWithAttachement(EmailVO emailVO) {
        executionContext.info("ApacheMailSender > sendSeperateListEmailWithAttachement > called");
        //define the mail requirement
        MultiPartEmail email = null;
        ByteArrayDataSource byteArrayDataSource = null;
        try {
            

            if (emailVO.getAttachementFileInputStream() != null) {
                byteArrayDataSource = new ByteArrayDataSource(
                emailVO.getAttachementFileInputStream(), emailVO.getContentType());
               
                executionContext.info("ApacheMailSender > sendSeperateListEmailWithAttachement > add Attchement ["+ emailVO.getAttachmentFileName() + "] to the email");
            }


            for(int k =0 ; k < emailVO.getRecipients().size() ; k++){
                email = new MultiPartEmail();
                email.setCharset("UTF-8");
                email.attach(byteArrayDataSource, emailVO.getAttachmentFileName(), "Attachment");
                
                email.setHostName(MobilyUtility.getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME));
                executionContext.info("ApacheMailSender > sendSeperateListEmailWithAttachement > SMTP HOST : "+ email.getHostName());
                email.setSubject(emailVO.getSubject());
                email.setFrom(emailVO.getSender());
                email.setMsg(emailVO.getMailBody());

                
                
                executionContext.info("ApacheMailSender > sendSeperateListEmailWithAttachement > Send to "+(String) emailVO.getRecipients().get(k));
                email.addTo((String) emailVO.getRecipients().get(k));
                email.send();
                email = null;
            }
            executionContext.info("ApacheMailSender > sendSeperateListEmailWithAttachement > mail Sent successfuly");
        } catch (IOException e) {
            executionContext.severe("ApacheMailSender > sendSeperateListEmailWithAttachement > IOException ,"+ e.getMessage());
            throw new ServiceException(e);
        } catch (EmailException e) {
            executionContext.severe("ApacheMailSender > sendSeperateListEmailWithAttachement > EmailException ,"+ e.getMessage());
            throw new ServiceException(e);
        } catch (Exception e) {
            executionContext.severe("ApacheMailSender > sendSeperateListEmailWithAttachement > Exception ,"+ e.getMessage());
            throw new ServiceException(e);
        }
    }
    /**
     * Sends an email in HTML format to TO recipients and sends Attachment if any
     * 
     * @param emailVO
     */
    public void sendEmailAsHtml(EmailVO emailVO) {
        executionContext.info("ApacheMailSender > sendEmailAsHtml > called");
        //define the mail requirement
        HtmlEmail email = new HtmlEmail();
        try {
            email.setCharset("UTF-8");
            Map attachmentsMap = emailVO.getAttachements();
            if(attachmentsMap != null && !attachmentsMap.isEmpty()) {
            	Iterator attachmentsIterator =  attachmentsMap.keySet().iterator();
        	    while (attachmentsIterator.hasNext()) {
        	        String attachmentFileName = (String) attachmentsIterator.next();
        	        executionContext.info("ApacheMailSender > sendEmailAsHtml > attachmentFileName="+attachmentFileName);
        	        
        	        InputStream fileAttachmentInputStream = (InputStream)attachmentsMap.get(attachmentFileName);
        	        executionContext.info("ApacheMailSender > sendEmailAsHtml > fileAttachmentInputStream="+fileAttachmentInputStream);
        	        
        	        ByteArrayDataSource byteArrayDataSource = new ByteArrayDataSource(
        	        		fileAttachmentInputStream, emailVO.getContentType());
    	                email.attach(byteArrayDataSource, attachmentFileName, "Attachment");
    	                executionContext.info("ApacheMailSender > sendEmailAsHtml > add Attchement ["+ attachmentFileName + "] to the email");
        	    }
            }
            email.setHostName(MobilyUtility.getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME));
            executionContext.info("ApacheMailSender > sendEmailAsHtml > SMTP HOST : " + email.getHostName());
            email.setSubject(emailVO.getSubject());
            email.setFrom(emailVO.getSender());
            email.setMsg(emailVO.getMailBody());
            if (emailVO.getRecipients() != null) {
                for (int i = 0; i < emailVO.getRecipients().size(); i++) {
                    email.addTo((String) emailVO.getRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendEmailAsHtml > recipent Added");
            }

            if (emailVO.getCcRecipients() != null) {
                for (int i = 0; i < emailVO.getCcRecipients().size(); i++) {
                    email.addCc((String) emailVO.getCcRecipients().get(i));
                }
                executionContext.info("ApacheMailSender > sendEmailAsHtml > cc recipient added");
            }

            email.send();
            executionContext.info("ApacheMailSender > sendEmailAsHtml > mail Sent successfuly");
        } catch (EmailException e) {
            executionContext.severe("ApacheMailSender > sendEmailAsHtml > EmailException ," + e.getMessage());
            throw new ServiceException(e);
        } catch (Exception e) {
            executionContext.severe("ApacheMailSender > sendEmailAsHtml > Exception ," + e.getMessage());
            throw new ServiceException(e);
        }
    }

    
    
    /**
     * Sends an email in HTML format to Bcc recipients
     * 
     * @param emailVO
     */
    public void sendEmailAsHtmlToBcc(EmailVO emailVO) {
        executionContext.info("ApacheMailSender > sendEmailAsHtmlToBcc > called");
        //define the mail requirement
        HtmlEmail email = new HtmlEmail();
        ByteArrayDataSource byteArrayDataSource = null;
        try {
            email.setCharset("UTF-8");
            email.setHostName(MobilyUtility.getPropertyValue(ConstantIfc.EMAIL_SMTP_HOST_NAME));
            executionContext.info("ApacheMailSender > sendEmailAsHtmlToBcc > SMTP HOST : " + email.getHostName());
            email.setSubject(emailVO.getSubject());
            email.setFrom(emailVO.getSender());
            email.setMsg(emailVO.getMailBody());

            if (emailVO.getAttachementFileInputStream() != null  && !("".equals(emailVO.getAttachementFileInputStream()))) {
                byteArrayDataSource = new ByteArrayDataSource(emailVO.getAttachementFileInputStream(), emailVO.getContentType());
                executionContext.info("ApacheMailSender > sendEmailAsHtmlToBcc > add Attchement ["+ emailVO.getAttachmentFileName() + "] to the email");
                if(byteArrayDataSource!=null){
                	email.attach(byteArrayDataSource, emailVO.getAttachmentFileName(), "Attachment");
                }
            }
           
            
            if (emailVO.getRecipients() != null) {
                for (int i = 0; i < emailVO.getRecipients().size(); i++) {
                    email.addBcc((String) emailVO.getRecipients().get(i));
                    
                }
                executionContext.info("ApacheMailSender > sendEmailAsHtmlToBcc > recipent Added");
            }
            
            email.send();
            
            executionContext.info("ApacheMailSender > sendEmailAsHtmlToBcc > mail Sent successfuly");
        } catch (EmailException e) {
            executionContext.severe("ApacheMailSender > sendEmailAsHtmlToBcc > EmailException ," + e.getMessage());
            throw new ServiceException(e);
        } catch (Exception e) {
            executionContext.severe("ApacheMailSender > sendEmailAsHtmlToBcc > Exception ," + e.getMessage());
            throw new ServiceException(e);
        }
    }

    
	public static void main(String[] args) {
//		String ss ="Dear Sir/Madam, \n\nThank you for contacting Mobily, it�s our pleasure to be one of Mobily customers.\n\nRegarding your complaint, be assured that your email was transfer to the concerned party for proper investigation.\n\nRest assured that we are taking all steps required to guarantee the availability of our products and services to you our valued customer.\n\nIf you require any additional information or assistance, please do not hesitate to contact us on 1100 from Mobily or 056 010 1100 from other operators, we look forward to your call.\n\nBest Regards,\nMobily Customer Care\n\n\n\n\n??????/???????             ???????/????????\n\n???? ?? ?????? ?? ??????? ,? ???? ??? ????? ?????? ?? ???? ??? ????? ??????? ????????.\n\n????? ?????? ??????? ??? ??? ?????? ??? ????? ???????? ?????? ??????? ??????. ? ????? ??? ?????? ?????? ?? ???? ??????? ???? ????? ?????? ???? ????????? ??????? ????? ?? ???? ??????? ?????????? ???? ???? ??????.\n\n\n??? ???? ???? ?????? ?? ??????????? ?? ???? ?? ????? ??????? ???? ?? ????? ?? ??????? ??? ??? ???? ????? ????? ??????? ??? ??????? (1100) ?? ??????? ? (0560101100) ?? ?? ???? ???.\n\n??? ????? ?????? ???????.\n .............................................\n\n?? ?? ???????? ? ???????\n???? ??????? ??????? ????????";
//		System.out.println(ss.length());
//		System.out.println(ResourceUtility.getPropertyValue("test"));
//		org.apache.commons.mail.MultiPartEmail email = new org.apache.commons.mail.MultiPartEmail();
//		try {
//			
//			
//			sa.com.mobily.eportal.beal.valueobject.EmailVO emailVO = new sa.com.mobily.eportal.beal.valueobject.EmailVO();
//			emailVO.setContentType("text/html;UTF-8");
//		    
//HtmlEmail  html = new HtmlEmail();
//html.setCharset("UTF-8");
//html.setHostName("10.1.10.30");
//html.setSubject("Test");
////email.setContent("<html><body><br><br>Dear Sir/Madam, <br><br>Thank you for contacting Mobily, it�s our pleasure to be one of Mobily customers.\n\nRegarding your complaint, be assured that your email was transfer to the concerned party for proper investigation.\n\nRest assured that we are taking all steps required to guarantee the availability of our products and services to you our valued customer.\n\nIf you require any additional information or assistance, please do not hesitate to contact us on 1100 from Mobily or 056 010 1100 from other operators, we look forward to your call.\n\nBest Regards,\nMobily Customer Care\n\n\n\n\n??????/???????             ???????/????????\n\n???? ?? ?????? ?? ??????? ,? ???? ??? ????? ?????? ?? ???? ??? ????? ??????? ????????.\n\n????? ?????? ??????? ??? ??? ?????? ??? ????? ???????? ?????? ??????? ??????. ? ????? ??? ?????? ?????? ?? ???? ??????? ???? ????? ?????? ???? ????????? ??????? ????? ?? ???? ??????? ?????????? ???? ???? ??????.\n\n\n??? ???? ???? ?????? ?? ??????????? ?? ???? ?? ????? ??????? ???? ?? ????? ?? ??????? ??? ??? ???? ????? ????? ??????? ??? ??????? (1100) ?? ??????? ? (0560101100) ?? ?? ???? ???.\n\n??? ????? ?????? ???????.\n .............................................\n\n?? ?? ???????? ? ???????\n???? ??????? ??????? ????????</body></html>","text/html;UTF-8");
//html.setMsg("<html><body dir=rtl><br><br>Dear Sir/Madam, <br><br>Thank you for contacting Mobily, it�s our pleasure to be one of Mobily customers.\n\nRegarding your complaint, be assured that your email was transfer to the concerned party for proper investigation.\n\nRest assured that we are taking all steps required to guarantee the availability of our products and services to you our valued customer.\n\nIf you require any additional information or assistance, please do not hesitate to contact us on 1100 from Mobily or 056 010 1100 from other operators, we look forward to your call.\n\nBest Regards,\nMobily Customer Care\n\n\n\n\n??????/???????             ???????/????????\n\n???? ?? ?????? ?? ??????? ,? ???? ??? ????? ?????? ?? ???? ??? ????? ??????? ????????.\n\n????? ?????? ??????? ??? ??? ?????? ??? ????? ???????? ?????? ??????? ??????. ? ????? ??? ?????? ?????? ?? ???? ??????? ???? ????? ?????? ???? ????????? ??????? ????? ?? ???? ??????? ?????????? ???? ???? ??????.\n\n\n??? ???? ???? ?????? ?? ??????????? ?? ???? ?? ????? ??????? ???? ?? ????? ?? ??????? ??? ??? ???? ????? ????? ??????? ??? ??????? (1100) ?? ??????? ? (0560101100) ?? ?? ???? ???.\n\n??? ????? ?????? ???????.\n .............................................\n\n?? ?? ???????? ? ???????\n???? ??????? ??????? ????????</body></html>");
//html.setFrom("m.elbastawisi@mobily.com.sa");
//html.addTo("m.elbastawisi@mobily.com.sa");
//html.send();
//
////			email.setCharset("UTF-8");
////			email.setHostName("10.1.10.30");
////			email.setSubject("Test");
//////			email.setContent("<html><body><br><br>Dear Sir/Madam, <br><br>Thank you for contacting Mobily, it�s our pleasure to be one of Mobily customers.\n\nRegarding your complaint, be assured that your email was transfer to the concerned party for proper investigation.\n\nRest assured that we are taking all steps required to guarantee the availability of our products and services to you our valued customer.\n\nIf you require any additional information or assistance, please do not hesitate to contact us on 1100 from Mobily or 056 010 1100 from other operators, we look forward to your call.\n\nBest Regards,\nMobily Customer Care\n\n\n\n\n??????/???????             ???????/????????\n\n???? ?? ?????? ?? ??????? ,? ???? ??? ????? ?????? ?? ???? ??? ????? ??????? ????????.\n\n????? ?????? ??????? ??? ??? ?????? ??? ????? ???????? ?????? ??????? ??????. ? ????? ??? ?????? ?????? ?? ???? ??????? ???? ????? ?????? ???? ????????? ??????? ????? ?? ???? ??????? ?????????? ???? ???? ??????.\n\n\n??? ???? ???? ?????? ?? ??????????? ?? ???? ?? ????? ??????? ???? ?? ????? ?? ??????? ??? ??? ???? ????? ????? ??????? ??? ??????? (1100) ?? ??????? ? (0560101100) ?? ?? ???? ???.\n\n??? ????? ?????? ???????.\n .............................................\n\n?? ?? ???????? ? ???????\n???? ??????? ??????? ????????</body></html>","text/html;UTF-8");
////			email.setMsg("<html><body><br><br>Dear Sir/Madam, <br><br>Thank you for contacting Mobily, it�s our pleasure to be one of Mobily customers.\n\nRegarding your complaint, be assured that your email was transfer to the concerned party for proper investigation.\n\nRest assured that we are taking all steps required to guarantee the availability of our products and services to you our valued customer.\n\nIf you require any additional information or assistance, please do not hesitate to contact us on 1100 from Mobily or 056 010 1100 from other operators, we look forward to your call.\n\nBest Regards,\nMobily Customer Care\n\n\n\n\n??????/???????             ???????/????????\n\n???? ?? ?????? ?? ??????? ,? ???? ??? ????? ?????? ?? ???? ??? ????? ??????? ????????.\n\n????? ?????? ??????? ??? ??? ?????? ??? ????? ???????? ?????? ??????? ??????. ? ????? ??? ?????? ?????? ?? ???? ??????? ???? ????? ?????? ???? ????????? ??????? ????? ?? ???? ??????? ?????????? ???? ???? ??????.\n\n\n??? ???? ???? ?????? ?? ??????????? ?? ???? ?? ????? ??????? ???? ?? ????? ?? ??????? ??? ??? ???? ????? ????? ??????? ??? ??????? (1100) ?? ??????? ? (0560101100) ?? ?? ???? ???.\n\n??? ????? ?????? ???????.\n .............................................\n\n?? ?? ???????? ? ???????\n???? ??????? ??????? ????????</body></html>");
////			email.setFrom("m.elbastawisi@mobily.com.sa");
////			email.addTo("m.elbastawisi@mobily.com.sa");
////			email.send();
//			executionContext.info("ApacheMailSender > sendEmailWithAttachement > mail Sent successfuly");
//		}	catch (org.apache.commons.mail.EmailException e) {
//			e.printStackTrace();
//		
//		}
//		catch(Exception e){
//			e.printStackTrace();
//		
//		}
//		

	}
}
