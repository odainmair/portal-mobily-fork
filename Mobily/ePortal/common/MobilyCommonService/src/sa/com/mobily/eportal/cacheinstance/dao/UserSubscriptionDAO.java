package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.valueobjects.UserSubscriptionsVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.customerinfo.util.SubscriptionTypeUtil;

public class UserSubscriptionDAO extends AbstractDBDAO<UserSubscriptionsVO>{

	private static UserSubscriptionDAO INSTANCE = new UserSubscriptionDAO();
	
	private static final String COL_SUBSCRIPTION_ID = "SUBSCRIPTION_ID";
	private static final String COL_USER_ACCT_ID = "USER_ACCT_ID";
	private static final String COL_IS_DEFAULT = "IS_DEFAULT";
	private static final String COL_SUBSCRIPTION_TYPE = "SUBSCRIPTION_TYPE";
	private static final String COL_MSISDN = "MSISDN";
	private static final String COL_SERVICE_ACCT_NUMBER = "SERVICE_ACCT_NUMBER";
	private static final String COL_CUSTOMER_TYPE = "CUSTOMER_TYPE";
	private static final String COL_PACKAGE_ID = "PACKAGE_ID";
	private static final String COL_CREATED_DATE = "CREATED_DATE";
	private static final String COL_IS_ACTIVE = "IS_ACTIVE";
	private static final String COL_CC_STATUS = "CC_STATUS";
	private static final String COL_LAST_UPDATED_TIME = "LAST_UPDATED_TIME";
	private static final String COL_IQAMA = "IQAMA";
	private static final String COL_SUB_SUBSCRIPTION_TYPE = "SUB_SUBSCRIPTION_TYPE";
	private static final String US_FIND_ACTIVE_USER_SUBSCRIPTION_BY_ACCOUNT_ID = "SELECT SUBSCRIPTION_ID,USER_ACCT_ID,IS_DEFAULT,SUBSCRIPTION_TYPE,MSISDN,SERVICE_ACCT_NUMBER,CUSTOMER_TYPE,PACKAGE_ID,CREATED_DATE,IS_ACTIVE,CC_STATUS,LAST_UPDATED_TIME,SUBSCRIPTION_NAME,SUB_SUBSCRIPTION_TYPE,IQAMA FROM USER_SUBSCRIPTION WHERE IS_ACTIVE='Y' AND USER_ACCT_ID=(SELECT USER_ACCT_ID FROM USER_ACCOUNTS WHERE lower(USER_NAME) = ?)";
	
	protected UserSubscriptionDAO(){
		super(DataSources.DEFAULT);
	}
	
	public static UserSubscriptionDAO getInstance(){
		return INSTANCE;
	}
	
	public List<UserSubscriptionsVO> getUserSubscriptionsByAccountId(String userName){
		return query(US_FIND_ACTIVE_USER_SUBSCRIPTION_BY_ACCOUNT_ID,userName.toLowerCase());// updated to lower case by r.agarwal.mit
	}
	

	@Override
	protected UserSubscriptionsVO mapDTO(ResultSet rs) throws SQLException{
		UserSubscriptionsVO refactoredUserSubscriptionsVO = new UserSubscriptionsVO();
		refactoredUserSubscriptionsVO.setCcStatus(rs.getLong(COL_CC_STATUS));
		refactoredUserSubscriptionsVO.setCreatedDate(rs.getTimestamp(COL_CREATED_DATE));
		refactoredUserSubscriptionsVO.setCustomerType(rs.getString(COL_CUSTOMER_TYPE));
		refactoredUserSubscriptionsVO.setIsActive(rs.getString(COL_IS_ACTIVE));
		refactoredUserSubscriptionsVO.setIsDefault(rs.getString(COL_IS_DEFAULT));
		refactoredUserSubscriptionsVO.setLastUpdatedTime(rs.getTimestamp(COL_LAST_UPDATED_TIME));
		refactoredUserSubscriptionsVO.setLineType(rs.getInt(COL_SUB_SUBSCRIPTION_TYPE));
		refactoredUserSubscriptionsVO.setMsidsn(rs.getString(COL_MSISDN));
		refactoredUserSubscriptionsVO.setPackageId(rs.getString(COL_PACKAGE_ID));
		refactoredUserSubscriptionsVO.setServiceAccountNumber(rs.getString(COL_SERVICE_ACCT_NUMBER));
		refactoredUserSubscriptionsVO.setSubscriptionId(rs.getLong(COL_SUBSCRIPTION_ID));
		refactoredUserSubscriptionsVO.setSubscriptionType(rs.getInt(COL_SUBSCRIPTION_TYPE));
		refactoredUserSubscriptionsVO.setSubscriptionName(SubscriptionTypeUtil.getSubSubScriptionName(refactoredUserSubscriptionsVO.getSubscriptionType(), refactoredUserSubscriptionsVO.getLineType()));
		refactoredUserSubscriptionsVO.setIqama(rs.getString(COL_IQAMA));
		refactoredUserSubscriptionsVO.setUserAccountId(rs.getLong(COL_USER_ACCT_ID));
		return refactoredUserSubscriptionsVO;
	}

}
