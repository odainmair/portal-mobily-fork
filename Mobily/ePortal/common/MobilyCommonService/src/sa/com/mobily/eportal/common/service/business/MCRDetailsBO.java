package sa.com.mobily.eportal.common.service.business;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRDAO;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MCRDetailsBO {
    private static final Logger log = LoggerInterface.log;
    
    
	/**
	 * Responsible for getting the details from MCR database
	 *  
	 * @param mode
	 * @param billAcctNumber
	 * @param eligiblePackages
	 * @return ArrayList
	 */
    public ArrayList getMCRDetails(int mode, String billAcctNumber, String eligiblePackages)  {
    	log.info("MCRDetailsBO > getMCRDetails > called");
    	OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory(DAOFactory.ORACLE);
     	MCRDAO mcrDAO = (MCRDAO)daoFactory.getOracleMCRDAO();
     	
     	return mcrDAO.getMCRDetails(mode, billAcctNumber, eligiblePackages);
    }

}
