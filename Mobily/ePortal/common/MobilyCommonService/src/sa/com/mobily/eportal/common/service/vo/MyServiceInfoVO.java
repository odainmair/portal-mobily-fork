/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.ArrayList;

/**
 * @author n.gundluru.mit
 *
 */
public class MyServiceInfoVO {

	public String serviceName = "";
	public String serviceType = "";
	public int status = -1;
	public String subscriptionType = "";
	public String planType = "";
	public String bbServiceType = "";
	public String bbPlanType = "";
	public String subscriptionDate = "";                   
	public String subscriptionUnit = "";
	public String packageValue = "";
	private String wifiServiceType = null;
    private ArrayList listOfSubscribedPromotion = null;
	private ArrayList listOfNotSubscribedpromotion = null;
	private ArrayList msisdnList = null;
	public int serviceEnablestatus = -1;
	
	
	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}
	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the subscriptionType
	 */
	public String getSubscriptionType() {
		return subscriptionType;
	}
	/**
	 * @param subscriptionType the subscriptionType to set
	 */
	public void setSubscriptionType(String subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	/**
	 * @return the planType
	 */
	public String getPlanType() {
		return planType;
	}
	/**
	 * @param planType the planType to set
	 */
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	/**
	 * @return the bbServiceType
	 */
	public String getBbServiceType() {
		return bbServiceType;
	}
	/**
	 * @param bbServiceType the bbServiceType to set
	 */
	public void setBbServiceType(String bbServiceType) {
		this.bbServiceType = bbServiceType;
	}
	/**
	 * @return the bbPlanType
	 */
	public String getBbPlanType() {
		return bbPlanType;
	}
	/**
	 * @param bbPlanType the bbPlanType to set
	 */
	public void setBbPlanType(String bbPlanType) {
		this.bbPlanType = bbPlanType;
	}
	/**
	 * @return the subscriptionDate
	 */
	public String getSubscriptionDate() {
		return subscriptionDate;
	}
	/**
	 * @param subscriptionDate the subscriptionDate to set
	 */
	public void setSubscriptionDate(String subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}
	/**
	 * @return the subscriptionUnit
	 */
	public String getSubscriptionUnit() {
		return subscriptionUnit;
	}
	/**
	 * @param subscriptionUnit the subscriptionUnit to set
	 */
	public void setSubscriptionUnit(String subscriptionUnit) {
		this.subscriptionUnit = subscriptionUnit;
	}
	/**
	 * @return the packageValue
	 */
	public String getPackageValue() {
		return packageValue;
	}
	/**
	 * @param packageValue the packageValue to set
	 */
	public void setPackageValue(String packageValue) {
		this.packageValue = packageValue;
	}
	/**
	 * @return the listOfSubscribedPromotion
	 */
	public ArrayList getListOfSubscribedPromotion() {
		return listOfSubscribedPromotion;
	}
	/**
	 * @param listOfSubscribedPromotion the listOfSubscribedPromotion to set
	 */
	public void setListOfSubscribedPromotion(ArrayList listOfSubscribedPromotion) {
		this.listOfSubscribedPromotion = listOfSubscribedPromotion;
	}
	/**
	 * @return the listOfNotSubscribedpromotion
	 */
	public ArrayList getListOfNotSubscribedpromotion() {
		return listOfNotSubscribedpromotion;
	}
	/**
	 * @param listOfNotSubscribedpromotion the listOfNotSubscribedpromotion to set
	 */
	public void setListOfNotSubscribedpromotion(
			ArrayList listOfNotSubscribedpromotion) {
		this.listOfNotSubscribedpromotion = listOfNotSubscribedpromotion;
	}
	/**
	 * @return the wifiServiceType
	 */
	public String getWifiServiceType() {
		return wifiServiceType;
	}
	/**
	 * @param wifiServiceType the wifiServiceType to set
	 */
	public void setWifiServiceType(String wifiServiceType) {
		this.wifiServiceType = wifiServiceType;
	}
	/**
	 * @return the msisdnList
	 */
	public ArrayList getMsisdnList() {
		return msisdnList;
	}
	/**
	 * @param msisdnList the msisdnList to set
	 */
	public void setMsisdnList(ArrayList msisdnList) {
		this.msisdnList = msisdnList;
	}
	/**
	 * @return the serviceEnablestatus
	 */
	public int getServiceEnablestatus() {
		return serviceEnablestatus;
	}
	/**
	 * @param serviceEnablestatus the serviceEnablestatus to set
	 */
	public void setServiceEnablestatus(int serviceEnablestatus) {
		this.serviceEnablestatus = serviceEnablestatus;
	}
	
}