/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.exception;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MobilyCommonException extends Exception {
  private String errorCode  = "";
  private String errorMessage;
  private BaseVO requestVO;
  
  public MobilyCommonException(String errorCode ,String  errorMessage){
      this.errorCode = errorCode;
      this.errorMessage = errorMessage;
  }
  
  
  public MobilyCommonException(){
      
  }
   
  public MobilyCommonException(String errorCode ,String  errorMessage,BaseVO requestVO){
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.requestVO=requestVO;
}
  public MobilyCommonException(String errorCode ){
      this.errorCode = errorCode;
  }
  
  public MobilyCommonException(Exception e ){
      super(e);
  }
  

  
/**
 * @return Returns the errorCode.
 */
public String getErrorCode() {
    return errorCode;
}
/**
 * @param errorCode The errorCode to set.
 */
public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
}
/**
 * @return Returns the errorMessage.
 */
public String getErrorMessage() {
    return errorMessage;
}
/**
 * @param errorMessage The errorMessage to set.
 */
public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
}

/**
 * @return Returns the requestVO.
 */
public BaseVO getRequestVO() {
	return requestVO;
}
/**
 * @param requestVO The requestVO to set.
 */
public void setRequestVO(BaseVO requestVO) {
	this.requestVO = requestVO;
}
}
