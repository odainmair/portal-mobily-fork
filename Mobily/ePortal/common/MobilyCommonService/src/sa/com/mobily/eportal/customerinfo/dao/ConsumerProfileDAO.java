package sa.com.mobily.eportal.customerinfo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.customerinfo.vo.ConsumerProfileVO;
import sa.com.mobily.eportal.customerinfo.vo.UserProfileVO;

public class ConsumerProfileDAO extends AbstractDBDAO<ConsumerProfileVO> implements ConstantsIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.CUSTOMER_INFO_SERVICE_LOGGER_NAME);
	private static String className = ConsumerProfileDAO.class.getName();

	private static String GET_CONSUMER_PROFILE = "SELECT SAN,ACCOUNT_NUMBER,CREATED_DATE,UPDATED_DATE,ACCOUNT_STATUS,CUST_PROFILE_VO FROM EEDBUSR.CUSTOMER_INFO_TBL WHERE ACCOUNT_NUMBER =? AND ACCOUNT_STATUS = ?";
	
	private static String SAVE_CONSUMER_PROFILE_DATA = "INSERT INTO EEDBUSR.CUSTOMER_INFO_TBL(SAN,ACCOUNT_NUMBER,CREATED_DATE,UPDATED_DATE,CUST_PROFILE_VO) VALUES (?, ? , CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP , ?)";
	
	private static String DISABLE_CONSUMER_STATUS = "UPDATE EEDBUSR.CUSTOMER_INFO_TBL SET  UPDATED_DATE=CURRENT_TIMESTAMP,ACCOUNT_STATUS =? WHERE SAN = ?";
	
	private static String UPDATE_CONSUMER_PROFILE="UPDATE EEDBUSR.CUSTOMER_INFO_TBL SET  UPDATED_DATE=CURRENT_TIMESTAMP, ACCOUNT_STATUS=?,CUST_PROFILE_VO=? WHERE SAN = ?";
	
	private static String UPDATE_ONLY_CONSUMER_PROFILE_BY_SAN="UPDATE EEDBUSR.CUSTOMER_INFO_TBL SET CUST_PROFILE_VO=? WHERE SAN = ?";
	
	private ConsumerProfileDAO() {
		super(DataSources.DEFAULT);
	}
	
	private static class ConsumerProfileDAOHolder {
		private static final ConsumerProfileDAO consumerProfileDAOInstance = new ConsumerProfileDAO();
	}
	
	public static ConsumerProfileDAO getInstance() {
		return ConsumerProfileDAOHolder.consumerProfileDAOInstance;
	}
	
	public ConsumerProfileVO getConsumerProfile(String lineNumber) {
		
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > getConsumerProfile > lineNumber:: " + lineNumber, className, "getConsumerProfile");
		ConsumerProfileVO consumerProfileVO = null;
		List<ConsumerProfileVO> consumerProfileVOList =  query(GET_CONSUMER_PROFILE,lineNumber,ACTIVE_ACCOUNT_STATUS);
		//executionContext.audit(Level.INFO, "ConsumerProfileDAO > getUserProfile > userProfileVOList:: " + consumerProfileVOList, className, "getConsumerProfile");
		if(CollectionUtils.isNotEmpty(consumerProfileVOList)){
			executionContext.audit(Level.INFO, "ConsumerProfileDAO > getUserProfile > userProfileVOList not empty:: ", className, "getConsumerProfile");
			consumerProfileVO =  consumerProfileVOList.get(0);
		}
		return consumerProfileVO;
	}
	
	public boolean saveConsumerProfileData(ConsumerProfileVO consumerProfileVO) {
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > saveUserProfileData > Account Number:: " + consumerProfileVO.getAccountNumber(), className, "saveUserProfileData");
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > saveUserProfileData > Customer Info:: " + consumerProfileVO.getCustomeProfileData(), className, "saveUserProfileData");
		int count = update(SAVE_CONSUMER_PROFILE_DATA, consumerProfileVO.getSan(),consumerProfileVO.getAccountNumber(),consumerProfileVO.getCustomeProfileData());
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > saveUserProfileData > row inserted:: " + count, className, "saveUserProfileData");
		return count > 0 ? true : false;
	}

	public boolean updateConsumerProfile(ConsumerProfileVO consumerProfileVO) {
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > updateConsumerProfile > Account Number:: " + consumerProfileVO.getAccountNumber(), className, "updateConsumerProfile");
		int count = update(UPDATE_CONSUMER_PROFILE,consumerProfileVO.getAccountStatus(), consumerProfileVO.getCustomeProfileData(),consumerProfileVO.getSan());
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > updateConsumerProfile > row inserted:: " + count, className, "updateConsumerProfile");
		return count > 0 ? true : false;
	}
	public boolean disableConsumerStatus(String san){
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > disableConsumerStatus > Account Number:: " + san, className, "disableConsumerStatus");
		int count = update(DISABLE_CONSUMER_STATUS,INACTIVE_ACCOUNT_STATUS, san);
		return count > 0 ? true : false;
	}

	public boolean updateConsumerProfileBySAN(ConsumerProfileVO consumerProfileVO) {
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > updateUserProfileBySAN > Account Number:: " + consumerProfileVO.getAccountNumber(), className, "updateUserProfileBySAN");
		int count = update(UPDATE_ONLY_CONSUMER_PROFILE_BY_SAN, consumerProfileVO.getCustomeProfileData(),consumerProfileVO.getSan());
		executionContext.audit(Level.INFO, "ConsumerProfileDAO > updateUserProfileBySAN >  updated the customer profile for SAN successfully> Count:: " + count, className, "updateUserProfileBySAN");
		return count > 0 ? true : false;
	}
	
	@Override
	protected ConsumerProfileVO mapDTO(ResultSet rs) throws SQLException {
		
		ConsumerProfileVO consumerProfileVO = new ConsumerProfileVO();
		consumerProfileVO.setSan(rs.getString("SAN"));
		consumerProfileVO.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
		consumerProfileVO.setCreatedDate(rs.getString("CREATED_DATE"));
		consumerProfileVO.setCreatedTimeStamp(rs.getTimestamp("UPDATED_DATE"));
		consumerProfileVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
		consumerProfileVO.setAccountStatus(rs.getString("ACCOUNT_STATUS"));
		consumerProfileVO.setCustomeProfileData(rs.getBytes("CUST_PROFILE_VO"));
		return consumerProfileVO;
	}
}
