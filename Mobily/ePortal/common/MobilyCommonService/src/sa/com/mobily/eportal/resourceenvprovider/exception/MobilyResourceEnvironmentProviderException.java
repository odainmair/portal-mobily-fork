package sa.com.mobily.eportal.resourceenvprovider.exception;

/**
 * 
 * @author Muzammil Mohsin Shaikh
 */
public class MobilyResourceEnvironmentProviderException extends Exception {

	private static final long serialVersionUID = 1302329305117073353L;
	
	private String msg;
    private Throwable cause;
    
    public MobilyResourceEnvironmentProviderException(String msg, Throwable cause) {
		super();
		this.msg = msg;
		this.cause = cause;
	}
    
    public String getMsg() {
		return msg;
	}

	public Throwable getCause() {
		return cause;
	}

	@Override
	public String toString() {
		return "MobilyResourceEnvironmentProviderException [msg=" + msg + ", cause=" + cause + "]";
	}
}
