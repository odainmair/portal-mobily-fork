/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.BBRegDAO;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQCustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleCustomerProfileDAO;
import sa.com.mobily.eportal.common.service.dao.imp.SeibelBBRegDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.LineTypePackageLookup;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.valueobject.common.ManagedMSISDNDataVO;
import sa.com.mobily.eportal.common.service.vo.CorporateProfileVO;
import sa.com.mobily.eportal.common.service.vo.CustomerProfileReplyVO;
import sa.com.mobily.eportal.common.service.vo.CustomerTypeVO;
import sa.com.mobily.eportal.common.service.vo.IdVO;
import sa.com.mobily.eportal.common.service.vo.ServiceLineVO;
import sa.com.mobily.eportal.common.service.vo.UserAccountVO;
import sa.com.mobily.eportal.common.service.xml.CustomerProfileXMLHandler;

/**
 * @author msayed
 * 
 *         TODO To change the template for this generated type comment go to
 *         Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerProfileBO {
	//private static final Logger log = LoggerInterface.log;
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("MobilyCommonService/commonServices");
	  private static final String className = CustomerProfileBO.class.getName();
	  
	public CustomerProfileReplyVO getCustomerProfile(String msisdn,
			boolean fullInfo) {
		executionContext.log(Level.INFO,"for MSISDN = ["+ msisdn + "] , fullInfo = [" + fullInfo + "]", className, "getCustomerProfile", null);
		CustomerProfileReplyVO replyVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		OracleCustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		replyVO = customerProfileDAO.getSiebelCustomerInoByMsisdn(msisdn);

		if (fullInfo) {
			IdVO idVO = replyVO.getAccount().getId();
			ArrayList serviceLines = customerProfileDAO
					.getSiebelCustomerRelatedMsisdns(idVO.getIdDocType(),
							idVO.getIdNumber());
			replyVO.setServiceLines(serviceLines);
		}
		executionContext.log(Level.INFO,"for MSISDN = ["+ msisdn + "] > done ", className, "getCustomerProfile", null);
		return replyVO;
	}

	/**
	 * Gets the customer profile information for the given billing account
	 * number
	 * 
	 * @param billingAccNumber
	 * @return CustomerProfileReplyVO
	 */
	public CustomerProfileReplyVO getCustomerProfile(String billingAccNumber) {
		executionContext.log(Level.INFO,"for Billing Account Number = ["+ billingAccNumber + "] Start", className, "getCustomerProfile", null);
		CustomerProfileReplyVO replyVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		replyVO = customerProfileDAO
				.getSiebelCustomerInoByBillingNumber(billingAccNumber);

		executionContext.log(Level.INFO,"for Billing Account Number = ["+ billingAccNumber + "] > done ", className, "getCustomerProfile", null);
		return replyVO;
	}

	/**
	 * Responsible for get user account information for the given MSISDN
	 * 
	 * @param msisdn
	 * @return CustomerProfileReplyVO
	 */
	public CustomerProfileReplyVO getCustomerProfileByMSISDN(String msisdn) {
		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileByMSISDN > msisdn = ["
				+ msisdn + "] Start", className, "getCustomerProfileByMSISDN", null);
		CustomerProfileReplyVO replyVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		replyVO = customerProfileDAO.getSiebelCustomerInoByMSISDN(msisdn);

		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileByMSISDN > msisdn = ["
				+ msisdn + "] > done ", className, "getCustomerProfileByMSISDN", null);
		return replyVO;
	}

	/**
	 * Responsible for get user account information for the given MSISDN
	 * 
	 * @param msisdn
	 * @return UserAccountVO
	 */
	public UserAccountVO getUserAccountWebProfile(String msisdn) {
		executionContext.log(Level.INFO,"CustomerProfileBO > getUserAccountWebProfile > msisdn = ["
				+ msisdn + "] Start", className, "getUserAccountWebProfile", null);
		UserAccountVO replyVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		replyVO = customerProfileDAO.getUserAccountWebProfile(msisdn);

		executionContext.log(Level.INFO,"CustomerProfileBO > getUserAccountWebProfile > msisdn = ["
				+ msisdn + "] > done ", className, "getUserAccountWebProfile", null);
		return replyVO;
	}

	/**
	 * Responsible for get user account information for the given name
	 * 
	 * @param name
	 * @return UserAccountVO
	 */
	public UserAccountVO getUserAccountWebProfileByName(String userName) {
		executionContext.log(Level.INFO,"CustomerProfileBO > getUserAccountWebProfileByName > userName = ["
				+ userName + "] Start", className, "getUserAccountWebProfileByName", null);
		UserAccountVO replyVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		replyVO = customerProfileDAO.getUserAccountWebProfileByName(userName);

		executionContext.log(Level.INFO,"CustomerProfileBO > getUserAccountWebProfileByName > userName = ["
				+ userName + "] > done ", className, "getUserAccountWebProfileByName", null);
		return replyVO;
	}

	/**
	 * Gets the customer profile information for the given billing account
	 * number
	 * 
	 * @param billingAccNumber
	 * @return CustomerProfileReplyVO
	 * @updated by r.agarwal.mit on 06 Dec. 2011 for MDM Migration
	 */
	public CustomerProfileReplyVO getCustomerProfileFromSiebel(
			String billingAccNumber) {
		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileFromSiebel > for Billing Account Number = ["
				+ billingAccNumber + "] Start", className, "getCustomerProfileFromSiebel", null);
		CustomerProfileReplyVO replyVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		replyVO = customerProfileDAO
				.getCustomerProfileFromSiebel(billingAccNumber);

		/*
		 * if(replyVO != null && replyVO.getAccount() != null ) { executionContext.log(Level.INFO,
		 * "CustomerProfileBO > getCustomerProfileFromSiebel > customer category ::"
		 * +replyVO.getAccount().getCustomerCategory());
		 * if(!ConstantIfc.COMPANY_CUSTOMER_CATEGORY_BUSINESS
		 * .equalsIgnoreCase(replyVO.getAccount().getCustomerCategory())) {
		 * executionContext.log(Level.INFO,
		 * "CustomerProfileBO > getCustomerProfileFromSiebel > NOT a corporate customer"
		 * ); replyVO = null; } }
		 */

		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileFromSiebel > for Billing Account Number = ["
				+ billingAccNumber + "] > done ", className, "getCustomerProfileFromSiebel", null);
		return replyVO;
	}

	/**
	 * Date: Sept 17, 2009 Description: This method is used to get the Corporate
	 * Profile from Siebel
	 * 
	 * @author r.agarwal.mit
	 * @param aBillingNo
	 * @return CorporateProfileVO
	 * @updated by r.agarwal.mit on 06 Dec. 2011 for MDM Migration
	 */
	public CorporateProfileVO findCorporateProfile(String aBillingNo) {
		executionContext.log(Level.INFO,"CustomerProfileBO > findCorporateProfile > for Billing Account Number = ["
				+ aBillingNo + "] Start", className, "findCorporateProfile", null);
		CorporateProfileVO corporateProfileVO = null;

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();
		corporateProfileVO = customerProfileDAO
				.findCorporateProfile(aBillingNo);

		if (corporateProfileVO != null) {
			executionContext.log(Level.INFO,"CustomerProfileBO > findCorporateProfile > customer category ["
					+ corporateProfileVO.getCompanyCustomerCategory()+"]", className, "findCorporateProfile", null);
			if (!ConstantIfc.COMPANY_CUSTOMER_CATEGORY_BUSINESS
					.equalsIgnoreCase(corporateProfileVO
							.getCompanyCustomerCategory())) {
				executionContext.log(Level.INFO,"CustomerProfileBO > findCorporateProfile > NOT a corporate customer", className, "findCorporateProfile", null);
				throw new SystemException();
			}
		}
		return corporateProfileVO;

	}

	/**
	 * Date: July 20, 2010 This method should be used by Corporate Registration
	 * Revamp,BillingInfo Revamp and Credit Card Payment Revamp
	 * 
	 * @param aBillingAccount
	 * @return
	 */
	public List getBillingAccountMsisdnList(String aBillingAccount) {
		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
				.getCustomerProfileDAO();

		return customerProfileDAO.getBillingAccountMsisdnList(aBillingAccount);
	}

	/**
	 * This method responsible for getting the customer line type (GSM / Connect
	 * /FTTH / WIMAX) based on his package name we will check first the length
	 * of the customer account and based on that we will decide
	 * 
	 * @param customerNumber
	 * @return
	 */
	public int getLineTypeByCustomerNumber(String customerNumber) {
		CustomerTypeVO customerType = null;
		String packageName = "";
		int lineType = 0;
		executionContext.log(Level.INFO,"CustomerProfileBO > getLineTypeByCustomerNumber > Called", className, "getLineTypeByCustomerNumber", null);
		try {
			CustomerTypeBO customerTypeBO = new CustomerTypeBO();
			CustomerTypeVO customerTypeVO = new CustomerTypeVO();
			customerTypeVO.setLineNumber(customerNumber);
			customerType = customerTypeBO.getCustomerType(customerTypeVO);
			packageName = FormatterUtility.checkNull(customerType
					.getPackageDescription());
			executionContext.log(Level.INFO,"CustomerProfileBO > getLineTypeByCustomerNumber > from getCustomerType > packageName >["
					+ packageName + "]", className, "getLineTypeByCustomerNumber", null);
		} catch (MobilyCommonException e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > getLineTypeByCustomerNumber > MobilyCommonException > failed in getting customer Info and package Name", className, "getLineTypeByCustomerNumber", null);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > getLineTypeByCustomerNumber > Exception > failed in getting customer Info and package Name", className, "getLineTypeByCustomerNumber", null);
		}

		if ("".equals(packageName)) {
			OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
					.getDAOFactory(DAOFactory.ORACLE);
			CustomerProfileDAO customerProfileDAO = (OracleCustomerProfileDAO) daoFactory
					.getCustomerProfileDAO();
			CustomerProfileReplyVO customerProfileVO = null;
			if (customerNumber.length() > 13) {
				customerProfileVO = customerProfileDAO
						.getSiebelCustomerInoByBillingNumber(customerNumber);
			} else {
				customerProfileVO = customerProfileDAO
						.getSiebelCustomerInoByMSISDN(customerNumber);
			}
			if (customerProfileVO != null) {
				packageName = FormatterUtility
						.checkNull(((ServiceLineVO) customerProfileVO
								.getServiceLines().get(0)).getPackageName());
			}
			executionContext.log(Level.INFO,"CustomerProfileBO > getLineTypeByCustomerNumber > from customerProfile packageName=["
					+ packageName
					+ "] of customerNumber=["
					+ customerNumber
					+ "]", className, "getLineTypeByCustomerNumber", null);
			lineType = LineTypePackageLookup.getInstance()
					.getLineTypeByPackageName(packageName);
			if (lineType == ConstantIfc.LINE_TYPE_CONSUMER) {
				if (customerProfileVO.getAccount().getCustomerType()
						.equalsIgnoreCase(ConstantIfc.CUSTOMER_BUSINESS_TYPE)) {
					lineType = ConstantIfc.LINE_TYPE_CORPORATE_LINE;
				}
				// check if the customer is corporate line or normal consumber
				// thru package category

			}
		}
		return lineType;
	}

	/**
	 * This method responsible for getting the customer line type (GSM / Connect
	 * /FTTH / WIMAX) based on his package name we will check first the length
	 * of the customer account and based on that we will decide
	 * 
	 * @param customerNumber
	 *            not mandatory
	 * @param String
	 *            packageName (Mandatory)
	 * @return
	 */
	public int getLineTypeByCustomerPackageName(String customerNumber,
			String packageName) {
		int lineType = 0;
		try {
			lineType = LineTypePackageLookup.getInstance()
					.getLineTypeByPackageName(packageName);
			if (lineType == ConstantIfc.LINE_TYPE_CONSUMER) {
				lineType = getLineTypeByCustomerNumber(customerNumber);
			}
		} catch (SystemException e) {
			lineType = 0;
		} catch (Exception e) {
			lineType = 0;
		}
		return lineType;
	}

	/**
	 * This method responsible for getting the customer line type (GSM / Connect
	 * /FTTH / WIMAX) based on his package name also in this method we will
	 * ignoring the different between individule and corporate individule
	 * 
	 * @param customerNumber
	 *            not mandatory
	 * @param String
	 *            packageName (Mandatory)
	 * @return
	 */
	public int getLineTypeByCustomerPackageName(String packageName) {
		int lineType = 0;
		try {
			lineType = LineTypePackageLookup.getInstance()
					.getLineTypeByPackageName(packageName);

		} catch (SystemException e) {
			lineType = 0;
		} catch (Exception e) {
			lineType = 0;
		}
		return lineType;
	}

	/**
	 * This method responsible for getting the customer line type (GSM / Connect
	 * /FTTH / WIMAX) based on his package name and customer category in case
	 * the customer line type is 11 to ne sure it's consumer or corporate
	 * individule
	 * 
	 * @param customerNumber
	 *            not mandatory
	 * @param String
	 *            packageName (Mandatory)
	 * @return
	 */
	public int getLineTypeByCustomerPackageName_category(
			String customerCategory, String packageName) {
		int lineType = 0;
		try {
			lineType = LineTypePackageLookup.getInstance()
					.getLineTypeByPackageName(packageName);
			if (lineType == ConstantIfc.LINE_TYPE_CONSUMER) {
				if (customerCategory
						.equalsIgnoreCase(ConstantIfc.CUSTOMER_BUSINESS_TYPE)) {
					lineType = ConstantIfc.LINE_TYPE_CORPORATE_LINE;
				} else {
					lineType = ConstantIfc.LINE_TYPE_CONSUMER;
				}
			}
		} catch (SystemException e) {
			lineType = 0;
		} catch (Exception e) {
			lineType = 0;
		}
		return lineType;
	}

	/**
	 * this service is responsible for getting the customer related account for
	 * specific MSISDN /Service Account Number
	 * 
	 * @param userMSISDN
	 * @return
	 * @throws SystemException
	 */
	public ArrayList<ManagedMSISDNDataVO> getCustomerRelatedAccountByNumber(
			String accountNumber) throws SystemException {
		ArrayList<ManagedMSISDNDataVO> listOfRelatedNumber = null;
		executionContext.log(Level.INFO,"wimax BO > getCustomerRelatedAccountByMSISDN > called", className, "getCustomerRelatedAccountByNumber", null);
		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		BBRegDAO seibelBBRegDAO = (SeibelBBRegDAO) daoFactory.getBBRegDAO();
		listOfRelatedNumber = seibelBBRegDAO
				.getManagedWimaxNumbersList(accountNumber);

		// updating LineType for each related account
		if (listOfRelatedNumber != null) {
			ManagedMSISDNDataVO RelatedObj = null;
			CustomerProfileBO customerProfileBO = new CustomerProfileBO();
			for (int i = 0; i < listOfRelatedNumber.size(); i++) {
				RelatedObj = listOfRelatedNumber.get(i);
				if (RelatedObj.getPackageName() != null
						|| RelatedObj.getPackageName() != "") {
					RelatedObj.setLineType(customerProfileBO
							.getLineTypeByCustomerPackageName(RelatedObj
									.getPackageName()));
				}
				listOfRelatedNumber.set(i, RelatedObj);
			}
		}
		return listOfRelatedNumber;
	}

	public static void main(String args[]) {
		CustomerProfileBO bb = new CustomerProfileBO();
		bb.getLineTypeByCustomerNumber("100015038634952");
	}

	public CustomerProfileReplyVO getCustomerProfileFromMDM(
			String billingAccNumber) {
		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileFromMDM > for Billing Account Number = ["
				+ billingAccNumber + "] Start", className, "getCustomerProfileFromMDM", null);
		CustomerProfileReplyVO replyVO = null;

		boolean unitTesting = false;
		StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String xmlReply = "";

		String request = new CustomerProfileXMLHandler().generateMDMXMLRequest(
				"BillingAccountNumber", billingAccNumber);

		if (!FormatterUtility.isEmpty(request))
			xmlRequest.append(request);

		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileFromMDM > xmlRequest ::"
				+ xmlRequest, className, "getCustomerProfileFromMDM", null);

		if (unitTesting) {
			xmlReply = "<ejd:queryCustomerRs xmlns:ejd='http://www.ejada.com' xmlns:mtosi_xsd='http://www.ibm.com/telecom/common/schema/mtosi/v3_0' xmlns:ibm='http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/CustomerMessage' "
					+ "xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>	<ejd:MsgRsHdr><ejd:StatusCode>I000000</ejd:StatusCode><ejd:RqUID>IVR123456789123456</ejd:RqUID></ejd:MsgRsHdr><Body><tns:customer><Status>1</Status>"
					+ "<Party><Id>123456</Id><fromTimestamp>2001-12-17T09:30:47</fromTimestamp><CharacteristicValue><Characteristic><Name>Comments</Name></Characteristic><Value>bla blaa ....</Value></CharacteristicValue><Specification><Category>O</Category></Specification>"
					+ "<PartyIdentification><Id>98765432</Id><Specification><Type>13</Type></Specification><IssuePlace>Riyadh</IssuePlace><IssueDate>1967-08-13</IssueDate><ExpiryDate>1967-08-13</ExpiryDate><IssueDateHijri>1411-05-13</IssueDateHijri>"
					+ "<ExpiryDateHijri>1415-08-13</ExpiryDateHijri><LastVerifiedDate>1967-08-13</LastVerifiedDate></PartyIdentification><ContactMedium><PostalContact><BillingAddress><CharacteristicValue><Characteristic><Name>String</Name><Type>String</Type>"
					+ "</Characteristic><Value>String</Value></CharacteristicValue><Country>153</Country><StateOrProvince>Riyadh</StateOrProvince><Postcode>11234</Postcode><StreetName>Olaya</StreetName><UrbanPropertyAddressExtensions><POBox>55333</POBox>"
					+ "<AddressLineOne>Address Line One</AddressLineOne><AddressLineTwo>Address Line Two</AddressLineTwo><AddressLineThree>Address LineThree</AddressLineThree></UrbanPropertyAddressExtensions></BillingAddress></PostalContact>"
					+ "</ContactMedium><ContactPerson><Party><PartyIdentification><Id>123456</Id><fromTimestamp>2001-12-17T09:30:47</fromTimestamp><toTimestamp>2001-12-17T09:30:47</toTimestamp><Specification><Type>15</Type></Specification><IssuePlace>Riyadh</IssuePlace>"
					+ "<IssueDate>1977-08-13</IssueDate><ExpiryDate>1977-08-13</ExpiryDate><IssueDateHijri>1421-05-13</IssueDateHijri><ExpiryDateHijri>1425-08-13</ExpiryDateHijri><LastVerifiedDate>1977-08-13</LastVerifiedDate></PartyIdentification><ContactMedium>"
					+ "<EmailContact><EmailAddress>A123@abc.com</EmailAddress></EmailContact></ContactMedium><ContactMedium><CharacteristicValue><Characteristic><Name>PeferredContactFlag</Name></Characteristic><Value>True</Value></CharacteristicValue>"
					+ "<TelephoneContact><Number>0561234567</Number><Type>Fax</Type></TelephoneContact></ContactMedium><ContactMedium><TelephoneContact><Number>0561234568</Number><Type>Phone</Type></TelephoneContact></ContactMedium><PartyExtensions>"
					+ "<PartyChoice><Individual><Name><GivenName>Ahmed</GivenName><MiddleName>A.</MiddleName><ThirdName>Hany</ThirdName><FourthName>Sami</FourthName><FamilyName>Mohamed</FamilyName></Name><Gender>M</Gender><DateOfBirth>1977-11-11</DateOfBirth>"
					+ "<Nationality>153</Nationality></Individual></PartyChoice></PartyExtensions></Party></ContactPerson><PartyExtensions><PartyChoice><Organization><Name><TradingName>Mobily Infotech</TradingName></Name><Employees>200</Employees><Country>Canada</Country>"
					+ "<Capital>500000</Capital><IsPartner>false</IsPartner><OrganizationType>11</OrganizationType><IndustryType>22</IndustryType><AccountManager><KeyAccountManagerPF>89123</KeyAccountManagerPF><KeyAccountManagerEmail>ee123@xyz.com</KeyAccountManagerEmail>"
					+ "</AccountManager></Organization></PartyChoice><CrossReferenceIds><Id>A1234567</Id><IdType>1</IdType></CrossReferenceIds><PreferredLanguage>100</PreferredLanguage></PartyExtensions></Party></ibm:customer></Body></ejd:queryCustomerRs>";
		} else {
			MQDAOFactory daoFactory = (MQDAOFactory) DAOFactory
					.getDAOFactory(DAOFactory.MQ);
			CustomerProfileDAO customerProfileDAO = (MQCustomerProfileDAO) daoFactory
					.getMQCustomerProfileDAO();
			try {
				String requestQueueName = MobilyUtility
						.getPropertyValue(ConstantIfc.MDM_INQUIRY_REQUEST_QUEUENAME);
				String replyQueueName = MobilyUtility
						.getPropertyValue(ConstantIfc.MDM_INQUIRY_REPLY_QUEUENAME);
				xmlReply = customerProfileDAO
						.sendToMQWithReply(xmlRequest.toString(),
								requestQueueName, replyQueueName);
			} catch (MobilyCommonException e) {
				executionContext.log(Level.SEVERE, "CustomerProfileBO > getCustomerProfileFromMDM > MobilyCommonException while doing inquiry with MDM::"
						+ e, className, "getCustomerProfileFromMDM", null);
				throw new SystemException(e);
			} catch (Exception e) {
				executionContext.log(Level.SEVERE, "CustomerProfileBO > getCustomerProfileFromMDM > Exception while doing inquiry with MDM::"
						+ e, className, "getCustomerProfileFromMDM", null);
				throw new SystemException(e);
			}
		}

		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileFromMDM > xmlReply ::"
				+ xmlReply, className, "getCustomerProfileFromMDM", null);
		try {
			// Parse the reply xml
			if (!FormatterUtility.isEmpty(xmlReply))
				replyVO = new CustomerProfileXMLHandler()
						.parseCustomerProfile(xmlReply);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > getCustomerProfileFromMDM > Exception while parsing the xml reply::"
					+ e, className, "getCustomerProfileFromMDM", null);
			throw new SystemException(e);
		}

		if (replyVO != null)
			replyVO.setRootBillingAccountNumber(billingAccNumber);

		executionContext.log(Level.INFO,"CustomerProfileBO > getCustomerProfileFromMDM > for Billing Account Number = ["
				+ billingAccNumber + "] > done ", className, "getCustomerProfileFromMDM", null);
		return replyVO;
	}

	public CustomerProfileReplyVO isMasterBillingAccount(String billingAccNumber) {
		executionContext.log(Level.INFO,"CustomerProfileBO > isMasterBillingAccount > for Billing Account Number = ["
				+ billingAccNumber + "] Start", className, "isMasterBillingAccount", null);
		CustomerProfileReplyVO replyVO = null;

		// boolean unitTesting = false;
		StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String xmlReply = "";

		String request = new CustomerProfileXMLHandler()
				.generateXMLRequestForMasterBillingAcc(billingAccNumber);

		if (!FormatterUtility.isEmpty(request))
			xmlRequest.append(request);

		executionContext.log(Level.INFO,"CustomerProfileBO > isMasterBillingAccount > xmlRequest ::"
				+ xmlRequest, className, "isMasterBillingAccount", null);

		MQDAOFactory daoFactory = (MQDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.MQ);
		CustomerProfileDAO customerProfileDAO = (MQCustomerProfileDAO) daoFactory
				.getMQCustomerProfileDAO();
		try {
			String requestQueueName = MobilyUtility
					.getPropertyValue(ConstantIfc.IS_MASTER_BA_INQUIRY_REQUEST_QUEUENAME);
			String replyQueueName = MobilyUtility
					.getPropertyValue(ConstantIfc.IS_MASTER_BA_INQUIRY_REPLY_QUEUENAME);
			xmlReply = customerProfileDAO.sendToMQWithReply(
					xmlRequest.toString(), requestQueueName, replyQueueName);
		} catch (MobilyCommonException e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isMasterBillingAccount > MobilyCommonException while doing inquiry with MDM::"
					+ e, className, "isMasterBillingAccount", null);
			throw new SystemException(e);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isMasterBillingAccount > Exception while doing inquiry with MDM::"
					+ e, className, "isMasterBillingAccount", null);
			throw new SystemException(e);
		}

		executionContext.log(Level.INFO,"CustomerProfileBO > isMasterBillingAccount > xmlReply ::"
				+ xmlReply, className, "isMasterBillingAccount", null);
		try {
			// Parse the reply xml
			if (!FormatterUtility.isEmpty(xmlReply))
				replyVO = new CustomerProfileXMLHandler()
						.parseXMLForMasterBillingAcc(xmlReply);
		} catch (MobilyCommonException e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isMasterBillingAccount > MobilyCommonException while parsing the xml reply > error code::"
					+ e.getErrorCode(), className, "isMasterBillingAccount", null);
			throw new SystemException(e);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isMasterBillingAccount > Exception while parsing the xml reply::"
					+ e, className, "isMasterBillingAccount", null);
			throw new SystemException(e);
		}

		executionContext.log(Level.INFO,"CustomerProfileBO > isMasterBillingAccount > for Billing Account Number = ["
				+ billingAccNumber + "] > done ", className, "isMasterBillingAccount", null);
		return replyVO;
	}

	public String isValidCustomerId(String msisdn, String idNumber)
			throws MobilyCommonException {
		String errorCode = "";
		StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		String xmlReply = "";

		String request = new CustomerProfileXMLHandler()
				.generateXMLRequestToValidateCustomerId(msisdn, idNumber);

		if (!FormatterUtility.isEmpty(request))
			xmlRequest.append(request);

		executionContext.log(Level.INFO,"CustomerProfileBO > isValidCustomerId > xmlRequest ::"
				+ xmlRequest, className, "isValidCustomerId", null);

		MQDAOFactory daoFactory = (MQDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.MQ);
		CustomerProfileDAO customerProfileDAO = (MQCustomerProfileDAO) daoFactory
				.getMQCustomerProfileDAO();
		try {
			String requestQueueName = MobilyUtility
					.getPropertyValue(ConstantIfc.IS_VALID_ID_REQUEST_QUEUENAME);
			String replyQueueName = MobilyUtility
					.getPropertyValue(ConstantIfc.IS_VALID_ID_REPLY_QUEUENAME);
			xmlReply = customerProfileDAO.sendToMQWithReply(
					xmlRequest.toString(), requestQueueName, replyQueueName);
		} catch (MobilyCommonException e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isValidCustomerId > MobilyCommonException while doing inquiry with MDM::"
					+ e, className, "isValidCustomerId", null);
			throw new SystemException(e);
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isValidCustomerId > Exception while doing inquiry with MDM::"
					+ e, className, "isValidCustomerId", null);
			throw new SystemException(e);
		}

		executionContext.log(Level.INFO,"CustomerProfileBO > isValidCustomerId > xmlReply ::"
				+ xmlReply, className, "isValidCustomerId", null);
		try {
			// Parse the reply xml
			if (!FormatterUtility.isEmpty(xmlReply))
				errorCode = new CustomerProfileXMLHandler()
						.parseXMLToValidateCustomerId(xmlReply);

		} catch (MobilyCommonException e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isValidCustomerId > MobilyCommonException while parsing the xml reply > error code::"
					+ e.getErrorCode(), className, "isValidCustomerId", null);
			throw e;
		} catch (Exception e) {
			executionContext.log(Level.SEVERE, "CustomerProfileBO > isValidCustomerId > Exception while parsing the xml reply::"
					+ e, className, "isValidCustomerId", null);
			throw new SystemException(e);
		}

		return errorCode;
	}
}