/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;


/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface EBillDAO {
	public String sendToMQWithReply(String xmlRequestMessage) throws MobilyCommonException;
}
