package sa.com.mobily.eportal.common.interceptor;

import java.sql.SQLException;
import java.util.logging.Level;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.management.JMException;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.application.BusinessValidationException;
import sa.com.mobily.eportal.common.exception.application.DataNotFoundException;
import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.exception.system.ConfigurationException;
import sa.com.mobily.eportal.common.exception.system.MiddlewareException;
import sa.com.mobily.eportal.common.exception.system.MobilySystemException;
import sa.com.mobily.eportal.common.exception.system.PersistenceException;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.exception.system.XMLParserException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.jaxb.JAXBUtilities;
import sa.com.mobily.eportal.common.service.vo.FootPrintVO;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

import com.mobily.exception.mq.MQSendException;

/**
 * This class is responsible for intercepting all the calls to the services
 * operations to wrap any exception and to send the footprint.
 * 
 * @author Yousef Alkhalaileh
 */
public class ServiceInterceptor
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private static final String FOOTPRINT_CONNECTION_FACTORY = "mobily.footprint.connection.factory.jndi";

	private static final String FOOTPRINT_QUEUE = "mobily.footprint.queue.jndi";

	private static final String FOOTPRINT_STATUS_PROPERTY = "FOOT_PRINT_STATUS";

	private static final String FOOTPRINT_STATUS_ON = "ON";

	private static ConnectionFactory footPrintCF;

	private static Destination footPrintDest;

	private boolean exceptionOccured = false;

	private String clazz = null;

	private String method = null;

	/**
	 * This method intercept the service call, catch the exception if any, and
	 * save the foot print
	 * 
	 * @param <code>InvocationContext</code> invocation context
	 * @return <code>Object</code> the service return value
	 * @author Yousef Alkhalaileh
	 */
	@AroundInvoke
	public Object interceptService(InvocationContext ctx) throws MobilySystemException, MobilyApplicationException
	{
		initExecContext(ctx);
		try
		{
			return wrapException(ctx);
		}
		catch (MobilySystemException e)
		{
			throw e;
		}
		catch (MobilyApplicationException e)
		{
			throw e;
		}
		finally
		{
			footPrint(ctx);
			executionContext.endMethod(clazz, method, null);
		}
	}

	/**
	 * This method initialize the execution context and adds an entry for
	 * starting the service method.
	 * 
	 * @param <code>InvocationContext</code> invocation context
	 * @author Yousef Alkhalaileh
	 */
	private void initExecContext(InvocationContext ctx)
	{
		// start execution for service
		clazz = ctx.getMethod().getDeclaringClass().getName();
		method = ctx.getMethod().getName();
		executionContext.startMethod(clazz, method, null);
	}

	/**
	 * This method invokes the service and wrap the exception if any.
	 * 
	 * @param <code>InvocationContext</code> invocation context
	 * @return <code>Object</code> the service return value
	 * @author Yousef Alkhalaileh
	 */
	private Object wrapException(InvocationContext ctx)
	{
		try
		{
			Object object = ctx.proceed();
			return object;
		}
		catch (JMException jmse)
		{
			// context add error logging statement to context
			exceptionOccured = true;
			throw new MiddlewareException(jmse.getMessage(), jmse);
		}
		catch (SQLException sqle)
		{
			exceptionOccured = true;
			throw new PersistenceException(sqle.getMessage(), sqle);
		}
		catch (BackEndException bec)
		{
			exceptionOccured = true;
			executionContext.log(Level.SEVERE, "Back end exception " + bec.getErrorCode(), clazz, method, bec);
			throw bec;
		}
		catch (ConfigurationException cfge)
		{
			exceptionOccured = true;
			throw cfge;
		}
		catch (ServiceException se)
		{
			exceptionOccured = true;
			throw se;
		}
		catch (BusinessValidationException bve)
		{
			exceptionOccured = true;
			throw bve;
		}
		catch (DataNotFoundException dnfe)
		{
			exceptionOccured = true;
			throw dnfe;
		}
		catch (XMLParserException xmlpe)
		{
			exceptionOccured = true;
			throw xmlpe;
		}
		catch (RuntimeException re)
		{
			exceptionOccured = true;
			if (re.getCause() instanceof SQLException)
			{
				throw new PersistenceException(re.getMessage(), re.getCause());
			}
			if (re.getCause() instanceof MQSendException)
			{
				throw new MiddlewareException(re.getMessage(), re.getCause());
			}
			else
			{
				throw new MobilyApplicationException(re.getMessage(), re);
			}
		}
		catch (Throwable e)
		{
			exceptionOccured = true;
			throw new MobilySystemException(e.getMessage(), e);
		}
	}

	/**
	 * This method checks if the footprint is enabled it will cast the first
	 * parameter of the parameters list and send to the footprint queue
	 * 
	 * @param <code>InvocationContext</code> invocation context
	 * @author Yousef Alkhalaileh
	 */
	private void footPrint(InvocationContext ctx)
	{
		try
		{
			String status = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(FOOTPRINT_STATUS_PROPERTY);
			if (FOOTPRINT_STATUS_ON.equalsIgnoreCase(status))
			{
				if (ctx.getParameters() != null && ctx.getParameters().length > 0)
				{
					Object obj = ctx.getParameters()[0];
					if (obj instanceof FootPrintVO)
					{
						executionContext.log(Level.FINE, ">>> Sending the footprint", clazz, method);
						FootPrintVO requestObj = (FootPrintVO) obj;
						FootPrintVO footPrintVO = new FootPrintVO();
						footPrintVO.setComments(requestObj.getComments());
						footPrintVO.setErrorCode(requestObj.getErrorCode());
						footPrintVO.setEventName(requestObj.getEventName());
						footPrintVO.setKey(requestObj.getKey());
						footPrintVO.setRequesterChannelId(requestObj.getRequesterChannelId());
						footPrintVO.setServiceAccount(requestObj.getServiceAccount());
						footPrintVO.setSubKey(requestObj.getSubKey());
						footPrintVO.setSystemErrorMessage(requestObj.getSystemErrorMessage());
						footPrintVO.setRequestTimeStamp(requestObj.getRequestTimeStamp());
						footPrintVO.setUserErrorMessage(requestObj.getUserErrorMessage());
						footPrintVO.setUserId(requestObj.getUserId());
						String xml = JAXBUtilities.getInstance().marshal(footPrintVO);
						executionContext.log(Level.FINE, xml, clazz, method);
						MQConnectionHandler.getInstance().sendToMQ(xml, "EPORTAL.FOOTPRINTING");
					}
				}
			}
		}
		catch (Exception e2)
		{
			executionContext.log(Level.SEVERE, "Could not send foot print", clazz, method, e2);
		}
	}

	/**
	 * This method sends the footprint message to the footprint queue
	 * 
	 * @param <code>String</code> the footprint object as XML
	 * @author Yousef Alkhalaileh
	 */
	// public static void sendFootPrint(String txt) throws JMSException
	// {
	// if(footPrintCF == null)
	// footPrintCF = (ConnectionFactory)
	// ServiceLocator.getInstance().getQueueConnectionFactory(MobilyUtility.getMQJMSConfigPropertyValue(FOOTPRINT_CONNECTION_FACTORY));
	// if(footPrintDest == null)
	// footPrintDest = (Destination)
	// ServiceLocator.getInstance().getQueue(MobilyUtility.getMQJMSConfigPropertyValue(FOOTPRINT_QUEUE));
	// Connection conn = footPrintCF.createConnection();
	// Session jmsSession = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
	// MessageProducer producer = jmsSession.createProducer(footPrintDest);
	// TextMessage message = jmsSession.createTextMessage();
	// message.setText(txt);
	// producer.send(message);
	// producer.close();
	// jmsSession.close();
	// }
}