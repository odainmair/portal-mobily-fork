/*
 * Created on Feb 18, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;



/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AccountVO extends BaseVO {

    protected String title;
    protected String fullName;
    protected String firstName;
    protected String middleName;
    protected String lastName;
    protected String customerCategory;
    protected String customerType;
    protected Date   birthDate;
    protected String dateOfBirth;
    protected String birthDateFormat;
    protected String gender;
    protected String nationality;
    protected String occupation;
    protected String billingLanguage;
    protected String simNumber;
    protected String packageID;
    protected String billingCompanyName;
    protected String companyCountry;
    protected String companyCity;
    public String getCompanyCity() {
		return companyCity;
	}
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	protected String companyAddress;
	
	public String getCompanyCountry() {
		return companyCountry;
	}
	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}
	protected IdVO id = new IdVO();
    protected ContactVO contact = new ContactVO();
    
    //For Panda
    protected String accountManagerName;
    protected String accountManagerEmail;
    protected String pandaFlag = "";
    
    // for customer segment
    protected String customerSegment;
    
    
    /**
	 * @return the customerSegment
	 */
	public String getCustomerSegment() {
		return customerSegment;
	}
	/**
	 * @param customerSegment the customerSegment to set
	 */
	public void setCustomerSegment(String customerSegment) {
		this.customerSegment = customerSegment;
	}
	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPackageID() {
		return packageID;
	}
	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}
	/**
	 * @return Returns the pandaFlag.
	 */
	public String getPandaFlag() {
		return pandaFlag;
	}
	/**
	 * @param pandaFlag The pandaFlag to set.
	 */
	public void setPandaFlag(String pandaFlag) {
		this.pandaFlag = pandaFlag;
	}
    public String getAccountManagerName() {
		return accountManagerName;
	}
	public void setAccountManagerName(String accountManagerName) {
		this.accountManagerName = accountManagerName;
	}
	public String getAccountManagerEmail() {
		return accountManagerEmail;
	}
	public void setAccountManagerEmail(String accountManagerEmail) {
		this.accountManagerEmail = accountManagerEmail;
	}
	
	public String getSimNumber() {
		return simNumber;
	}
	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}
	/**
     * @return Returns the billingLanguage.
     */
    public String getBillingLanguage()
    {
        return billingLanguage;
    }
    /**
     * @param billingLanguage The billingLanguage to set.
     */
    public void setBillingLanguage(String billingLanguage)
    {
        this.billingLanguage = billingLanguage;
    }
    
    /**
     * @return Returns the birthDate.
     */
    public Date getBirthDate()
    {
        return birthDate;
    }
    /**
     * @param birthDate The birthDate to set.
     */
    public void setBirthDate(String birthDate)
    {
        SimpleDateFormat tempSimpleDateFormat = new SimpleDateFormat( ConstantIfc.DATE_FORMAT_BIRTH_DATE );
        
        try
        {
            
            this.birthDate = tempSimpleDateFormat.parse(birthDate);
        }
        catch(ParseException tempParseException)
        {
            this.birthDate = null;
        }
    }
    
    /**
     * @return Returns the birthDateFormat.
     */
    public String getBirthDateFormat()
    {
        return birthDateFormat;
    }
    /**
     * @param birthDateFormat The birthDateFormat to set.
     */
    public void setBirthDateFormat(String birthDateFormat)
    {
        this.birthDateFormat = birthDateFormat;
    }
    
    /**
     * @return Returns the customerCategory.
     */
    public String getCustomerCategory()
    {
        return customerCategory;
    }
    /**
     * @param customerCategory The customerCategory to set.
     */
    public void setCustomerCategory(String customerCategory)
    {
        this.customerCategory = customerCategory;
    }
    /**
     * @return Returns the customerType.
     */
    public String getCustomerType()
    {
        return customerType;
    }
    /**
     * @param customerType The customerType to set.
     */
    public void setCustomerType(String customerType)
    {
        this.customerType = customerType;
    }
    
    /**
     * @return Returns the firstName.
     */
    public String getFirstName()
    {
        return firstName;
    }
    /**
     * @param firstName The firstName to set.
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    /**
     * @return Returns the fullName.
     */
    public String getFullName()
    {
        return fullName;
    }
    /**
     * @param fullName The fullName to set.
     */
    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }
    
    /**
     * @return Returns the gender.
     */
    public String getGender()
    {
        return gender;
    }
    /**
     * @param gender The gender to set.
     */
    public void setGender(String gender)
    {
        this.gender = gender;
    }
    
    /**
     * @return Returns the lastName.
     */
    public String getLastName()
    {
        return lastName;
    }
    /**
     * @param lastName The lastName to set.
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    /**
     * @return Returns the middleName.
     */
    public String getMiddleName()
    {
        return middleName;
    }
    /**
     * @param middleName The middleName to set.
     */
    public void setMiddleName(String middleName)
    {
        this.middleName = middleName;
    }
    
    
    /**
     * @return Returns the nationality.
     */
    public String getNationality()
    {
        return nationality;
    }
    /**
     * @param nationality The nationality to set.
     */
    public void setNationality(String nationality)
    {
        this.nationality = nationality;
    }
    /**
     * @return Returns the occupation.
     */
    public String getOccupation()
    {
        return occupation;
    }
    /**
     * @param occupation The occupation to set.
     */
    public void setOccupation(String occupation)
    {
        this.occupation = occupation;
    }
    /**
     * @return Returns the title.
     */
    public String getTitle()
    {
        return title;
    }
    /**
     * @param title The title to set.
     */
    public void setTitle(String title)
    {
        this.title = title;
    }
    /**
     * @return Returns the contact.
     */
    public ContactVO getContact()
    {
        return contact;
    }
    /**
     * @param contact The contact to set.
     */
    public void setContact(ContactVO contact)
    {
        this.contact = contact;
    }
    /**
     * @return Returns the id.
     */
    public IdVO getId()
    {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(IdVO id)
    {
        this.id = id;
    }
    
    /**
     * @return Returns the dateOfBirth.
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    /**
     * @param dateOfBirth The dateOfBirth to set.
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
	public String getBillingCompanyName() {
		return billingCompanyName;
	}
	public void setBillingCompanyName(String billingCompanyName) {
		this.billingCompanyName = billingCompanyName;
	}
    
}
