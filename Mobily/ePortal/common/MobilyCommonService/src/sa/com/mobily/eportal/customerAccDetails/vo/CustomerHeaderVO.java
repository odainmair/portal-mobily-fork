package sa.com.mobily.eportal.customerAccDetails.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CustomerHeaderVO extends BaseVO
{
	private static final long serialVersionUID = 1L;
	private String funcId;
	
    private String msgVersion;
    
    private String requestorChannelId;
    
    private String requestorChannelFunction;
    
    private String channelTransactionId;
    
    private String srDate;
    
    private String errorCode;
    
    private String errorMsg;

	public String getFuncId()
	{
		return funcId;
	}

	public void setFuncId(String funcId)
	{
		this.funcId = funcId;
	}

	public String getMsgVersion()
	{
		return msgVersion;
	}

	public void setMsgVersion(String msgVersion)
	{
		this.msgVersion = msgVersion;
	}

	public String getRequestorChannelId()
	{
		return requestorChannelId;
	}

	public void setRequestorChannelId(String requestorChannelId)
	{
		this.requestorChannelId = requestorChannelId;
	}

	public String getRequestorChannelFunction()
	{
		return requestorChannelFunction;
	}

	public void setRequestorChannelFunction(String requestorChannelFunction)
	{
		this.requestorChannelFunction = requestorChannelFunction;
	}

	public String getChannelTransactionId()
	{
		return channelTransactionId;
	}

	public void setChannelTransactionId(String channelTransactionId)
	{
		this.channelTransactionId = channelTransactionId;
	}

	public String getSrDate()
	{
		return srDate;
	}

	public void setSrDate(String srDate)
	{
		this.srDate = srDate;
	}

	public String getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}

	public String getErrorMsg()
	{
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg)
	{
		this.errorMsg = errorMsg;
	}
    
}
