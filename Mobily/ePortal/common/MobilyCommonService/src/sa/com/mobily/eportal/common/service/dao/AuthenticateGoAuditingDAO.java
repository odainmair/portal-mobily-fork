package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.common.service.vo.AuthonticateGoAuditingVO;

/**
 * This DAO class is responsible for DB operations on CONTACTS table
 * <p> 
 * it contains methods for retrieve, add, delete and update.
 *  
 * @author Yousef Alkhalaileh
 */
public class AuthenticateGoAuditingDAO extends AbstractDBDAO<AuthonticateGoAuditingVO>{

	public static final String INSERT_AUTH_AND_GO_AUDIT = "INSERT INTO EEDBUSR.AUTHONTICATE_GO_AUDITING_TBL(SR_ID, MSISDN, ACTION_TIME, MODULE_ID, ACTION_ID, SERVICE_NAME, SERVICE_DESCRIPTION, ID) VALUES(?, ?, ?, ?, ?, ?, ?, AUTH_GO_AUDITING_SEQ.nextval)";
	
	private static AuthenticateGoAuditingDAO instance = new AuthenticateGoAuditingDAO();
	
	protected AuthenticateGoAuditingDAO() {
		super(DataSources.DEFAULT);
	}
	
	/**
	 * This method returns a singleton instance from the DAO class
	 * 
	 * @return <code>MyContactsDAO</code>
	 * @author Yousef Alkhalaileh
	 */
	public static AuthenticateGoAuditingDAO getInstance() {
		return instance;
	}
	
	public int insertAuthonticateGoAuditingVO(AuthonticateGoAuditingVO authonticateGoAuditingVO){
		return update(INSERT_AUTH_AND_GO_AUDIT, 
				authonticateGoAuditingVO.getSrId(),
				authonticateGoAuditingVO.getMsisdn(),
				authonticateGoAuditingVO.getActionTime(),
				authonticateGoAuditingVO.getModuleId(),
				authonticateGoAuditingVO.getActionId(),
				authonticateGoAuditingVO.getServiceName(),
				authonticateGoAuditingVO.getServiceDescribtion()
				);
	}

	@Override
	protected AuthonticateGoAuditingVO mapDTO(ResultSet rs) throws SQLException {
		return null;
	}
}
