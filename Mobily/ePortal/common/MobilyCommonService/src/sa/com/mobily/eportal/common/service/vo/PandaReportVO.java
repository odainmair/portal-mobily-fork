package sa.com.mobily.eportal.common.service.vo;



public class PandaReportVO {
	
   private String msisdn = "";
   private String totalAllocatedMinutes = "";
   private String totalMinutes = "";
   private String totalRemainingMinutes = "";
   private String totalUsedMinutes = "";
   private String yearMonth = "";
   
   private String distributionMonth = "";
   private String totalDistributedMinutes = "";
   
   private String customerType = "";
   private String fromDate = "";
   private String toDate = "";
   private String srDate = "";
   private String errorCode = "";
   private String errorMsg = "";
   private String status = "";
   private String description = "";
   private String profileName = "";
	private String requestInfo = "";
	
	/**
	 * @return Returns the requestInfo.
	 */
	public String getRequestInfo() {
		return requestInfo;
	}
	/**
	 * @param requestInfo The requestInfo to set.
	 */
	public void setRequestInfo(String requestInfo) {
		this.requestInfo = requestInfo;
	}	   
  
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return Returns the profileName.
	 */
	public String getProfileName() {
		return profileName;
	}
	/**
	 * @param profileName The profileName to set.
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the errorMsg.
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	/**
	 * @param errorMsg The errorMsg to set.
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	/**
	 * @return Returns the srDate.
	 */
	public String getSrDate() {
		return srDate;
	}
	/**
	 * @param srDate The srDate to set.
	 */
	public void setSrDate(String srDate) {
		this.srDate = srDate;
	}
	/**
	 * @return Returns the customerType.
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * @param customerType The customerType to set.
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * @return Returns the fromDate.
	 */
	public String getFromDate() {
		return fromDate;
	}
	/**
	 * @param fromDate The fromDate to set.
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	/**
	 * @return Returns the toDate.
	 */
	public String getToDate() {
		return toDate;
	}
	/**
	 * @param toDate The toDate to set.
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
  
	/**
	 * @return Returns the distributionMonth.
	 */
	public String getDistributionMonth() {
		return distributionMonth;
	}
	/**
	 * @param distributionMonth The distributionMonth to set.
	 */
	public void setDistributionMonth(String distributionMonth) {
		this.distributionMonth = distributionMonth;
	}
	/**
	 * @return Returns the totalDistributedMinutes.
	 */
	public String getTotalDistributedMinutes() {
		return totalDistributedMinutes;
	}
	/**
	 * @param totalDistributedMinutes The totalDistributedMinutes to set.
	 */
	public void setTotalDistributedMinutes(String totalDistributedMinutes) {
		this.totalDistributedMinutes = totalDistributedMinutes;
	}
	/**
	 * @return Returns the msisdn.
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn The msisdn to set.
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return Returns the totalAllocatedMinutes.
	 */
	public String getTotalAllocatedMinutes() {
		return totalAllocatedMinutes;
	}
	/**
	 * @param totalAllocatedMinutes The totalAllocatedMinutes to set.
	 */
	public void setTotalAllocatedMinutes(String totalAllocatedMinutes) {
		this.totalAllocatedMinutes = totalAllocatedMinutes;
	}
	/**
	 * @return Returns the totalMinutes.
	 */
	public String getTotalMinutes() {
		return totalMinutes;
	}
	/**
	 * @param totalMinutes The totalMinutes to set.
	 */
	public void setTotalMinutes(String totalMinutes) {
		this.totalMinutes = totalMinutes;
	}
	/**
	 * @return Returns the totalRemainingMinutes.
	 */
	public String getTotalRemainingMinutes() {
		return totalRemainingMinutes;
	}
	/**
	 * @param totalRemainingMinutes The totalRemainingMinutes to set.
	 */
	public void setTotalRemainingMinutes(String totalRemainingMinutes) {
		this.totalRemainingMinutes = totalRemainingMinutes;
	}
	/**
	 * @return Returns the totalUsedMinutes.
	 */
	public String getTotalUsedMinutes() {
		return totalUsedMinutes;
	}
	/**
	 * @param totalUsedMinutes The totalUsedMinutes to set.
	 */
	public void setTotalUsedMinutes(String totalUsedMinutes) {
		this.totalUsedMinutes = totalUsedMinutes;
	}
	/**
	 * @return Returns the yearMonth.
	 */
	public String getYearMonth() {
		return yearMonth;
	}
	/**
	 * @param yearMonth The yearMonth to set.
	 */
	public void setYearMonth(String yearMonth) {
		this.yearMonth = yearMonth;
	}
}
