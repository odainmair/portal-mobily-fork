
package sa.com.mobily.eportal.billing.service.jms;

import java.util.logging.Level;

import sa.com.mobily.eportal.billing.constants.BillPaymentConstants;
import sa.com.mobily.eportal.billing.constants.ConstantsIfc;
import sa.com.mobily.eportal.billing.util.AppConfig;
import sa.com.mobily.eportal.billing.xml.paymentnotification.PaymentNotificationXMLHandler;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyReplyVO;
import sa.com.mobily.eportal.common.service.vo.PaymentNotifyRequestVO;

/**
 * @author Suresh Vathsavai - MIT
 *
 */
public class PaymentNotificationJMSRepository {

	private static String className = PaymentNotificationJMSRepository.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	/**
	 * Description: Responsible to send the Payment Notification to Back-end
	 * Date: Nov 26, 2013
	 * @param requestVO
	 * @return PaymentNotifyReplyVO
	 */
	 public PaymentNotifyReplyVO sendPaymentNotification(PaymentNotifyRequestVO requestVO) {
		 String methodName = "sendPaymentNotification";
		 boolean isTesting = false;
		 String strXMLReply = null;
		 PaymentNotifyReplyVO notifyReplyVO = null;
		 MQAuditVO mqAuditVO = new MQAuditVO();
		 executionContext.startMethod(className, methodName, null);
		 String requestQueueName = AppConfig.getInstance().get(BillPaymentConstants.CUSTOMER_PAYMENT_NOTIFY_REQUEST_QUEUENAME);
		 String replyQueueName = AppConfig.getInstance().get(BillPaymentConstants.CUSTOMER_PAYMENT_NOTIFY_REPLY_QUEUENAME);
		 executionContext.audit(Level.INFO, "Payment Notification Request Queue Name=["+requestQueueName+"] and Reply queue name=["+replyQueueName+"]", className, methodName);
		
		 mqAuditVO.setRequestQueue(requestQueueName);
		 mqAuditVO.setReplyQueue(replyQueueName);
		 mqAuditVO.setUserName(requestVO.getRequestorUserId());
		 if(FormatterUtility.isNotEmpty(requestVO.getLineNumber())){
			 mqAuditVO.setMsisdn(requestVO.getLineNumber());
		 }else{
			 mqAuditVO.setMsisdn(requestVO.getAccountNumber());
		 }
		 mqAuditVO.setFunctionName(BillPaymentConstants.PAYMENT_NOTIFY_MESSAGE_FORMAT_VALUE);
		 
		 PaymentNotificationXMLHandler notificationXMLHandler = new PaymentNotificationXMLHandler();
		
		 String strXMLRequest = notificationXMLHandler.generatePymtNotificationXMLRequest(requestVO);
		 
		 executionContext.audit(Level.INFO, "Payment Notification Request=["+strXMLRequest+"]", className, methodName);
		 mqAuditVO.setMessage(strXMLRequest);
		 if(strXMLRequest != null) {
			 if(isTesting) {
				strXMLReply = "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>Payment Notification</MsgFormat><MsgVersion>01</MsgVersion><RequestorChannelId>MPAY</RequestorChannelId><RequestorChannelFunction>PMTNOT</RequestorChannelFunction><RequestorUserId>PMTNOT</RequestorUserId><RequestorLanguage>E</RequestorLanguage><RequestorSecurityInfo>secure</RequestorSecurityInfo><ReturnCode>0000</ReturnCode></EE_EAI_HEADER><ReferenceNumber>43981 35029 08871 18405</ReferenceNumber><PartnerReferenceNumber>MOBILY6014</PartnerReferenceNumber></EE_EAI_MESSAGE>";
			 }else {
				//strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(strXMLRequest, requestQueueName, replyQueueName);;
				 strXMLReply = MQConnectionHandler.getInstance().sendToMQWithReply(mqAuditVO);
				 
			 }
			 executionContext.audit(Level.INFO, "Payment Notification Reply=["+strXMLReply+"]", className, methodName);
			 mqAuditVO.setReply(strXMLReply);
			 
			 notifyReplyVO = notificationXMLHandler.parsePymtNotificationReply(strXMLReply);
			 mqAuditVO.setErrorMessage(notifyReplyVO.getErrorMessage());
	    	 mqAuditVO.setServiceError(notifyReplyVO.getErrorCode());
	    	 MQUtility.saveMQAudit(mqAuditVO);
			 if(notifyReplyVO != null && FormatterUtility.isNotEmpty(notifyReplyVO.getErrorCode()) && Integer.parseInt(notifyReplyVO.getErrorCode()) > 0)	{
	            executionContext.log(Level.SEVERE, "Error Code from Backend:["+notifyReplyVO.getErrorCode()+"]", className, methodName, new BackEndException());
	        
	            throw new BackEndException(ExceptionConstantIfc.PAYMENT_NOTIFICATION_SERVICE,notifyReplyVO.getErrorCode());
		     }
			 
			
		 }
		executionContext.endMethod(className, methodName, null);
		return notifyReplyVO;
	 }
	
}
