package sa.com.mobily.eportal.common.service.xml;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.MessageHeaderVO;
import sa.com.mobily.eportal.common.service.vo.SRMessageHeaderVO;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MessageHeaderHandler {

	private static final Logger log = LoggerInterface.log;
	
	/**
	 * 
	 * this Method is responsible for parsing Header element and save the
	 * parsing result into MessageHeaderVO object
	 * @param Eelemnt p_HeaderElement
	 * @return  MessageHeaderVO object
	 * @author Mohamed Sayed
	 */
	public MessageHeaderVO parsingXMLHeaderReplyMessage(Element p_HeaderElement) throws DOMException {
	  log.debug("start parsing xml Header Reply Message .....");
		
	  MessageHeaderVO header = new MessageHeaderVO();
		
		try {
			NodeList childList 		= p_HeaderElement.getChildNodes();
			
			for(int j = 0; j < childList.getLength(); j++){
			    if((childList.item(j)).getNodeType() != Node.ELEMENT_NODE)
			    	continue;
			    
			    Element l_Node         = (Element)childList.item(j);
			    String l_szNodeTagName = l_Node.getTagName();
			    String l_szNodeValue   = "";
			    if( l_Node.getFirstChild() != null )
			    	   l_szNodeValue   = l_Node.getFirstChild().getNodeValue();

			    if(l_szNodeTagName.equalsIgnoreCase(TagIfc.MSG_FORMAT))
			    	header.setMsgFormat(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.MSG_VERSION))
			    	header.setMsgVersion(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_CHANNEL_ID))
			    	header.setRequestorChannelID(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_CHANNEL_FUNCTION))
			    	header.setRequestorChannelFunction(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_USER_ID))
			    	header.setRequestorUserID(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_LANG))
			    	header.setLang(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_SECURITY_INFO))
			    	header.setSecurityInfo(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_DATE))
			    	header.setSrDate(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.RETURN_CODE))
			    	header.setReturnCode(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE))
			    	header.setErrorCode(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_MSG))
			    	header.setErrorMessage(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_MSG_OPTION))
			    	header.setMsgOption(l_szNodeValue);
			    
			    
			    
			}//end for

		} catch (DOMException e) {
			log.fatal("DOMException in Parsing xml header Reply Message...."+e);
			// TODO Auto-generated catch block
			throw e;
		}		
		log.debug("Parsing Xml Header Reply message is Done Successfuly ......");
		return header;
		
	}
	
	public SRMessageHeaderVO parsingSRXMLHeaderReplyMessage(Element p_HeaderElement) throws DOMException 
	{
		SRMessageHeaderVO header = new SRMessageHeaderVO();

		try 
		{
			NodeList childList = p_HeaderElement.getChildNodes();
			
			for(int j = 0; j < childList.getLength(); j++)
			{
			    if((childList.item(j)).getNodeType() != Node.ELEMENT_NODE)
			    	continue;
			    
			    Element l_Node         = (Element)childList.item(j);
			    String l_szNodeTagName = l_Node.getTagName();
			    String l_szNodeValue   = "";
			    if( l_Node.getFirstChild() != null )
			    	l_szNodeValue   = l_Node.getFirstChild().getNodeValue();

			    if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_FUNC_ID))
			    	header.setFuncID(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.MSG_VERSION))
			    	header.setMsgVersion(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REQUESTOR_CHANNEL_ID))
			    	header.setRequestorChannelID(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.REPLIER_CHANNEL_ID))
			    	header.setReplierChannelId(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_DATE))
			    	header.setSrDate(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_RCV_DATE))
			    	header.setSrRcvDate(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_STATUS))
			    	header.setSrStatus(l_szNodeValue);
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_ERR_CODE))
			    {
			    	if(l_szNodeValue == null || l_szNodeValue.equalsIgnoreCase("null"))
			    	{
			    		l_szNodeValue = ConstantIfc.RETURN_CODE_VALUE;	
			    	}
			    	header.setErrorCode(l_szNodeValue);
			    }
			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.RETURN_CODE))
			    {
			    	if(l_szNodeValue == null || l_szNodeValue.equalsIgnoreCase("null"))
			    	{
			    		l_szNodeValue = ConstantIfc.RETURN_CODE_VALUE;	
			    	}
			    	header.setReturnCode(l_szNodeValue);
			    }

			    else if(l_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_SR_STATUS))
			    	header.setSrStatus(l_szNodeValue);
			}//end for
		} 
		catch (DOMException e) 
		{
			// TODO Auto-generated catch block
			throw e;
		}		
		return header;
	}

}