package sa.com.mobily.eportal.common.service.vo;



/**
 * 
 * @author n.gundluru.mit
 *
 */

public class CoverageVO {
	private String errorCode;
	private String errorMessage;
	private String coverage;
	private String odbnumber;
	private String MsgFormat=null;
	private String MsgVersion=null;
	private String RequestorChannelId=null;
	private String RequestorChannelFunction=null;
	private String RequestorSecurityInfo=null;
	private String ChannelTransactionId=null;
	private String SrDate=null;
	private String rfs = null;
	
	// Added for report save into DB
	
	 private String emirateId;
	 private String governateId;
	 private String cityId;
	 private String districtId;
	
	  
	    private String longitude = "";
	    private String latitude = "";
	    private String type="";
	    private String coverageTime="";
	    private String locale="";
	
	    
	    
	/**
		 * @return the odbnumber
		 */
		public String getOdbnumber() {
			return odbnumber;
		}
		/**
		 * @param odbnumber the odbnumber to set
		 */
		public void setOdbnumber(String odbnumber) {
			this.odbnumber = odbnumber;
		}
	/**
		 * @return the emirateId
		 */
		public String getEmirateId() {
			return emirateId;
		}
		/**
		 * @param emirateId the emirateId to set
		 */
		public void setEmirateId(String emirateId) {
			this.emirateId = emirateId;
		}
		/**
		 * @return the governateId
		 */
		public String getGovernateId() {
			return governateId;
		}
		/**
		 * @param governateId the governateId to set
		 */
		public void setGovernateId(String governateId) {
			this.governateId = governateId;
		}
		/**
		 * @return the ciyId
		 */
		public String getCityId() {
			return cityId;
		}
		/**
		 * @param ciyId the ciyId to set
		 */
		public void setCityId(String cityId) {
			this.cityId = cityId;
		}
		/**
		 * @return the districtId
		 */
		public String getDistrictId() {
			return districtId;
		}
		/**
		 * @param districtId the districtId to set
		 */
		public void setDistrictId(String districtId) {
			this.districtId = districtId;
		}
	/**
		 * @return the locale
		 */
		public String getLocale() {
			return locale;
		}
		/**
		 * @param locale the locale to set
		 */
		public void setLocale(String locale) {
			this.locale = locale;
		}
	/**
		 * @return the coverageTime
		 */
		public String getCoverageTime() {
			return coverageTime;
		}
		/**
		 * @param coverageTime the coverageTime to set
		 */
		public void setCoverageTime(String coverageTime) {
			this.coverageTime = coverageTime;
		}

		/**
		 * @return the longitude
		 */
		public String getLongitude() {
			return longitude;
		}
		/**
		 * @param longitude the longitude to set
		 */
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		/**
		 * @return the latitude
		 */
		public String getLatitude() {
			return latitude;
		}
		/**
		 * @param latitude the latitude to set
		 */
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}
		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}
	/**
	 * @return the requestorChannelFunction
	 */
	public String getRequestorChannelFunction() {
		return RequestorChannelFunction;
	}
	/**
	 * @param requestorChannelFunction the requestorChannelFunction to set
	 */
	public void setRequestorChannelFunction(String requestorChannelFunction) {
		RequestorChannelFunction = requestorChannelFunction;
	}
	/**
	 * @return the msgFormat
	 */
	public String getMsgFormat() {
		return MsgFormat;
	}
	/**
	 * @param msgFormat the msgFormat to set
	 */
	public void setMsgFormat(String msgFormat) {
		MsgFormat = msgFormat;
	}
	/**
	 * @return the msgVersion
	 */
	public String getMsgVersion() {
		return MsgVersion;
	}
	/**
	 * @param msgVersion the msgVersion to set
	 */
	public void setMsgVersion(String msgVersion) {
		MsgVersion = msgVersion;
	}
	/**
	 * @return the requestorChannelId
	 */
	public String getRequestorChannelId() {
		return RequestorChannelId;
	}
	/**
	 * @param requestorChannelId the requestorChannelId to set
	 */
	public void setRequestorChannelId(String requestorChannelId) {
		RequestorChannelId = requestorChannelId;
	}
	/**
	 * @return the requestorSecurityInfo
	 */
	public String getRequestorSecurityInfo() {
		return RequestorSecurityInfo;
	}
	/**
	 * @param requestorSecurityInfo the requestorSecurityInfo to set
	 */
	public void setRequestorSecurityInfo(String requestorSecurityInfo) {
		RequestorSecurityInfo = requestorSecurityInfo;
	}
	/**
	 * @return the channelTransactionId
	 */
	public String getChannelTransactionId() {
		return ChannelTransactionId;
	}
	/**
	 * @param channelTransactionId the channelTransactionId to set
	 */
	public void setChannelTransactionId(String channelTransactionId) {
		ChannelTransactionId = channelTransactionId;
	}
	/**
	 * @return the srDate
	 */
	public String getSrDate() {
		return SrDate;
	}
	/**
	 * @param srDate the srDate to set
	 */
	public void setSrDate(String srDate) {
		SrDate = srDate;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return coverage;
	}
	/**
	 * @param coverage the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	/**
	 * @return the rfs
	 */
	public String getRfs() {
		return rfs;
	}
	/**
	 * @param rfs the rfs to set
	 */
	public void setRfs(String rfs) {
		this.rfs = rfs;
	}
	
}
