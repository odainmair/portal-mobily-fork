//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;


/**
 * This class represents the Value Object used for transfer of the
 * data of the File.
 */
public class File implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String fileName;
    private int id;
    private String contentType;
    private byte[] data;

    /**
     * Retrieves the File Name of the File.
     * @return the value of the fileName.
     */
    public final String getFileName() {
        return fileName;
    }

    /**
     * Sets the File Name of the File.
     * @param value the new value of the fileName field.
     */
    public final void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Retrieves the Id of the File.
     * @return the value of the id.
     */
    public final int getId() {
        return id;
    }

    /**
     * Sets the Id of the File.
     * @param value the new value of the id field.
     */
    public final void setId(int value) {
        this.id = value;
    }

    /**
     * Retrieves the Content Type of the File.
     * @return the value of the contentType.
     */
    public final String getContentType() {
        return contentType;
    }

    /**
     * Sets the Content Type of the File.
     * @param value the new value of the contentType field.
     */
    public final void setContentType(String value) {
        this.contentType = value;
    }

    /**
     * Retrieves the Data of the File.
     * @return the value of the data.
     */
    public final byte[] getData() {
        return data;
    }

    /**
     * Sets the Data of the File.
     * @param value the new value of the data field.
     */
    public final void setData(byte[] value) {
        this.data = value;
    }

    @Override
    public String toString() {
        return "File ["
                + "fileName = " + fileName
                + ", id = " + id
                + ", contentType = " + contentType
                + ", data = " + java.util.Arrays.toString(data)
                + "]";
    }

}
