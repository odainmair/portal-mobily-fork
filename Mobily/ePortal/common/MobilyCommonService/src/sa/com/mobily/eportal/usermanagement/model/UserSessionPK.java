package sa.com.mobily.eportal.usermanagement.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * The primary key class for the SR_USER_SESSIONVO_TBL database table.
 * 
 */
@Embeddable
public class UserSessionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	
	@NotNull
	@Size(min=1, max=100,message="sessionid can not be null and empty")
	private String sessionid;
	
	@NotNull
	@Size(min=1, max=100,message="username can not be null and empty")
	private String username;

    public UserSessionPK() {
    }
	public String getSessionid() {
		return this.sessionid;
	}
	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserSessionPK)) {
			return false;
		}
		UserSessionPK castOther = (UserSessionPK)other;
		return 
			this.sessionid.equals(castOther.sessionid)
			&& this.username.equals(castOther.username);

    }
    
	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.sessionid.hashCode();
		hash = hash * prime + this.username.hashCode();
		
		return hash;
    }
}