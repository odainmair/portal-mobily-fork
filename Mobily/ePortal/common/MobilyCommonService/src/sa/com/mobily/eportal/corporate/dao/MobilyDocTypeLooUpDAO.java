package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.MobilyDocTypeLooUp;
import sa.com.mobily.eportal.corporate.model.MobilyDocTypeRequestVO;

public class MobilyDocTypeLooUpDAO extends AbstractDBDAO<MobilyDocTypeLooUp> {
	
	private static MobilyDocTypeLooUpDAO instance = new MobilyDocTypeLooUpDAO();
	private static final String GET_ALL_DOC_TYPE = "SELECT * FROM MOBILY_DOC_TYPE_LOOKUP_TBL WHERE STATUS = '1' ORDER BY PRIORITY ASC";

	protected MobilyDocTypeLooUpDAO() {
		super(DataSources.DEFAULT);
	}
	
	public static MobilyDocTypeLooUpDAO getInstance() {
		return instance;
	}
	
	public List<MobilyDocTypeLooUp> getAllDocTypes() {
		List<MobilyDocTypeLooUp> lstLookUp = query(GET_ALL_DOC_TYPE);
		if (CollectionUtils.isNotEmpty(lstLookUp)) {
			return lstLookUp;
		}
		return null;
	}	

	public List<MobilyDocTypeLooUp> getDocTypes(MobilyDocTypeRequestVO mobilyDocTypeRequestVO) {
		StringBuffer buff = new StringBuffer();
		List<MobilyDocTypeLooUp> lstLookUp = null;
		//Prepare query
		buff.append("SELECT * FROM MOBILY_DOC_TYPE_LOOKUP_TBL WHERE STATUS = '1'");
		if (mobilyDocTypeRequestVO.getLINEID() > 0) {
			buff.append(" AND LINEID = "+mobilyDocTypeRequestVO.getLINEID());
		}
		if(FormatterUtility.isNotEmpty(mobilyDocTypeRequestVO.getDocType())) {
			buff.append(" AND DOCUMENT_TYPE = '"+mobilyDocTypeRequestVO.getDocType()+"'");
		}
		buff.append(" ORDER BY PRIORITY ASC");
	
		//Execute query
		//if (mobilyDocTypeRequestVO.getLINEID() > 0) {
			lstLookUp = query(buff.toString());
		//}
		
		if (CollectionUtils.isNotEmpty(lstLookUp)) {
			return lstLookUp;
		}
		return null;
	}	
	
	@Override
	protected MobilyDocTypeLooUp mapDTO(ResultSet rs) throws SQLException {
		MobilyDocTypeLooUp mobilyDocTypeLooUp = new MobilyDocTypeLooUp();
		mobilyDocTypeLooUp.setID(rs.getInt("ID"));
		mobilyDocTypeLooUp.setDOCUMENT_TYPE(rs.getString("DOCUMENT_TYPE"));
		mobilyDocTypeLooUp.setENGLISH_NAME(rs.getString("ENGLISH_NAME"));
		mobilyDocTypeLooUp.setARABIC_NAME(rs.getString("ARABIC_NAME"));
		mobilyDocTypeLooUp.setSTATUS(rs.getInt("STATUS"));
		mobilyDocTypeLooUp.setPRIORITY(rs.getInt("PRIORITY"));
		mobilyDocTypeLooUp.setLINEID(rs.getInt("LINEID"));
		return mobilyDocTypeLooUp;
	}

}
