/*
 * Created on Feb 7, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.io.InputStream;
import java.util.List;
import java.util.Map;



/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EmailVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1303397952069810586L;
	private InputStream attachementFileInputStream =null ;
	private String attachmentFileName;
	private String contentType;
	private String subject;
	private String mailBody;
	private List<String> recipients ;
	private List<String> ccRecipients;
	private String sender;
	private Map attachements = null;
	
	
	
	public Map getAttachements() {
		return attachements;
	}
	public void setAttachements(Map attachements) {
		this.attachements = attachements;
	}
	public static void main(String[] args) {
	}
	/**
	 * @return Returns the attachementFileInputStream.
	 */
	public InputStream getAttachementFileInputStream() {
		return attachementFileInputStream;
	}
	/**
	 * @param attachementFileInputStream The attachementFileInputStream to set.
	 */
	public void setAttachementFileInputStream(
			InputStream attachementFileInputStream) {
		this.attachementFileInputStream = attachementFileInputStream;
	}
	/**
	 * @return Returns the contentType.
	 */
	public String getContentType() {
		return contentType;
	}
	/**
	 * @param contentType The contentType to set.
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	/**
	 * @return Returns the mailBody.
	 */
	public String getMailBody() {
		return mailBody;
	}
	/**
	 * @param mailBody The mailBody to set.
	 */
	public void setMailBody(String mailBody) {
		this.mailBody = mailBody;
	}
	/**
	 * @return Returns the recipients.
	 */
	public List<String> getRecipients() {
		return recipients;
	}
	/**
	 * @param recipients The recipients to set.
	 */
	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}
	/**
	 * @return Returns the sender.
	 */
	public String getSender() {
		return sender;
	}
	/**
	 * @param sender The sender to set.
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}
	/**
	 * @return Returns the subject.
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject The subject to set.
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return Returns the attachmentFileName.
	 */
	public String getAttachmentFileName() {
		return attachmentFileName;
	}
	/**
	 * @param attachmentFileName The attachmentFileName to set.
	 */
	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}
	/**
	 * @return Returns the ccRecipients.
	 */
	public List<String> getCcRecipients() {
		return ccRecipients;
	}
	/**
	 * @param ccRecipients The ccRecipients to set.
	 */
	public void setCcRecipients(List<String> ccRecipients) {
		this.ccRecipients = ccRecipients;
	}
}
