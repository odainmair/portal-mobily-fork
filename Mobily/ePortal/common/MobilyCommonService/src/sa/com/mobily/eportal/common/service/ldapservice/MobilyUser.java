/*
 * Created on April 05, 2013
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.ldapservice;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author r.agarwal.mit
 * 
 *         This class is the mobily user object that must be returned from the
 *         LDAP as the user content from LDAP
 * 
 */
public class MobilyUser implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6963992710433195818L;

	private String cn = "";

	private String sn = "";

	private String uid = "";

	private String userAcctReference="";
	
	private String defaultMsisdn = "";

	private String email = "";

	private String preferredLanguage = "en";

	private int lineType;

	private String serviceAccountNumber = "";

	private int defaultSubscriptionType;
	
	private int defaultSubSubscriptionType;

	private String defaultSubId = "";

	private String corpMasterAcctNo = "";

	private String corpAPNumber = "";

	private String corpAPIDNumber = "";

	private String commercialRegisterationID = "";

	private boolean corpSurveyCompleted = false;

	private String userPassword = "";

	private String givenName = "";

	private String Status = "";

	private String firstName = "";

	private String lastName = "";

	private String custSegment = "";

	private int roleId;

	private String SecurityQuestion = "";

	private String SecurityAnswer = "";

	private Date birthDate;

	private String nationality = "";

	private String Gender = "";

	private Date registerationDate;

	private String iqama = "";
	
	private int registrationSourceId;//channel Id  of the user
	
	private String facebookId="";
	
	private String twitterId="";
	
	private String contactNumber = "";
	
	private Date passwordChangedTime;

	private List<String> groupList = null;

	public String getCn()
	{
		return cn;
	}

	public void setCn(String cn)
	{
		this.cn = cn;
	}

	public String getSn()
	{
		return sn;
	}

	public void setSn(String sn)
	{
		this.sn = sn;
	}

	public String getUid()
	{
		return uid;
	}

	public void setUid(String uid)
	{
		this.uid = uid;
	}

	public String getDefaultMsisdn()
	{
		return defaultMsisdn;
	}

	public void setDefaultMsisdn(String defaultMsisdn)
	{
		this.defaultMsisdn = defaultMsisdn;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getPreferredLanguage()
	{
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage)
	{
		this.preferredLanguage = preferredLanguage;
	}

	public int getLineType()
	{
		return lineType;
	}

	public void setLineType(int lineType)
	{
		this.lineType = lineType;
	}

	public String getServiceAccountNumber()
	{
		return serviceAccountNumber;
	}

	public void setServiceAccountNumber(String serviceAccountNumber)
	{
		this.serviceAccountNumber = serviceAccountNumber;
	}

	public int getDefaultSubscriptionType()
	{
		return defaultSubscriptionType;
	}

	public void setDefaultSubscriptionType(int defaultSubscriptionType)
	{
		this.defaultSubscriptionType = defaultSubscriptionType;
	}

	public String getDefaultSubId()
	{
		return defaultSubId;
	}

	public void setDefaultSubId(String defaultSubId)
	{
		this.defaultSubId = defaultSubId;
	}

	public String getCorpMasterAcctNo()
	{
		return corpMasterAcctNo;
	}

	public void setCorpMasterAcctNo(String corpMasterAcctNo)
	{
		this.corpMasterAcctNo = corpMasterAcctNo;
	}

	public String getCorpAPNumber()
	{
		return corpAPNumber;
	}

	public void setCorpAPNumber(String corpAPNumber)
	{
		this.corpAPNumber = corpAPNumber;
	}

	public String getCorpAPIDNumber()
	{
		return corpAPIDNumber;
	}

	public void setCorpAPIDNumber(String corpAPIDNumber)
	{
		this.corpAPIDNumber = corpAPIDNumber;
	}

	public String getCommercialRegisterationID()
	{
		return commercialRegisterationID;
	}

	public void setCommercialRegisterationID(String commercialRegisterationID)
	{
		this.commercialRegisterationID = commercialRegisterationID;
	}

	public boolean isCorpSurveyCompleted()
	{
		return corpSurveyCompleted;
	}

	public void setCorpSurveyCompleted(boolean corpSurveyCompleted)
	{
		this.corpSurveyCompleted = corpSurveyCompleted;
	}

	public String getUserPassword()
	{
		return userPassword;
	}

	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}

	public String getGivenName()
	{
		return givenName;
	}

	public void setGivenName(String givenName)
	{
		this.givenName = givenName;
	}

	public String getStatus()
	{
		return Status;
	}

	public void setStatus(String status)
	{
		Status = status;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getCustSegment()
	{
		return custSegment;
	}

	public void setCustSegment(String custSegment)
	{
		this.custSegment = custSegment;
	}

	public int getRoleId()
	{
		return roleId;
	}

	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	public String getSecurityQuestion()
	{
		return SecurityQuestion;
	}

	public void setSecurityQuestion(String securityQuestion)
	{
		SecurityQuestion = securityQuestion;
	}

	public String getSecurityAnswer()
	{
		return SecurityAnswer;
	}

	public void setSecurityAnswer(String securityAnswer)
	{
		SecurityAnswer = securityAnswer;
	}

	public Date getBirthDate()
	{
		return birthDate;
	}

	public void setBirthDate(Date birthDate)
	{
		this.birthDate = birthDate;
	}

	public String getNationality()
	{
		return nationality;
	}

	public void setNationality(String nationality)
	{
		this.nationality = nationality;
	}

	public String getGender()
	{
		return Gender;
	}

	public void setGender(String gender)
	{
		Gender = gender;
	}

	public Date getRegisterationDate()
	{
		return registerationDate;
	}

	public void setRegisterationDate(Date registerationDate)
	{
		this.registerationDate = registerationDate;
	}

	public String getIqama()
	{
		return iqama;
	}

	public void setIqama(String iqama)
	{
		this.iqama = iqama;
	}

	public List<String> getGroupList()
	{
		return groupList;
	}

	public void setGroupList(List<String> groupList)
	{
		this.groupList = groupList;
	}

	public String getFacebookId()
	{
		return facebookId;
	}

	public void setFacebookId(String facebookId)
	{
		this.facebookId = facebookId;
	}

	public int getDefaultSubSubscriptionType()
	{
		return defaultSubSubscriptionType;
	}

	public void setDefaultSubSubscriptionType(int defaultSubSubscriptionType)
	{
		this.defaultSubSubscriptionType = defaultSubSubscriptionType;
	}

	public String getUserAcctReference()
	{
		return userAcctReference;
	}

	public void setUserAcctReference(String userAcctReference)
	{
		this.userAcctReference = userAcctReference;
	}

	public int getRegistrationSourceId()
	{
		return registrationSourceId;
	}

	public void setRegistrationSourceId(int registrationSourceId)
	{
		this.registrationSourceId = registrationSourceId;
	}

	public String getContactNumber()
	{
		return contactNumber;
	}

	public void setContactNumber(String contactNumber)
	{
		this.contactNumber = contactNumber;
	}
	
	public Date getPasswordChangedTime()
	{
		return passwordChangedTime;
	}
	public void setPasswordChangedTime(Date passwordChangedTime)
	{
		this.passwordChangedTime = passwordChangedTime;
	}

	public String getTwitterId()
	{
		return twitterId;
	}

	public void setTwitterId(String twitterId)
	{
		this.twitterId = twitterId;
	}
	
	@Override
	public String toString()
	{
		return "cn=[" + cn + "],sn=[" + sn + "],uid=[" + uid + "],userAcctReference=[" + userAcctReference + "],defaultMsisdn=[" + defaultMsisdn + "],email=[" + email
				+ "],preferredLanguage=[" + preferredLanguage + "]," + "lineType=[" + lineType + "],serviceAccountNumber=[" + serviceAccountNumber + "],defaultSubscriptionType=["
				+ defaultSubscriptionType + "],defaultSubSubscriptionType=[" + defaultSubSubscriptionType + "]," + "defaultSubId=[" + defaultSubId + "],corpMasterAcctNo=["
				+ corpMasterAcctNo + "],corpAPNumber=[" + corpAPNumber + "], " + "corpAPIDNumber=[" + corpAPIDNumber + "],commercialRegisterationID=[" + commercialRegisterationID
				+ "],corpSurveyCompleted=[" + corpSurveyCompleted + "]," + "givenName=[" + givenName + "], Status=[" + Status + "], firstName=[" + firstName + "], lastName=["
				+ lastName + "], custSegment=[" + custSegment + "], roleId=[" + roleId + "], " + "securityQuestion=[" + SecurityQuestion + "], SecurityAnswer=[" + SecurityAnswer
				+ "], birthDate =[" + birthDate + "], nationality=[" + nationality + "], gender=[" + Gender + "], registrationDate=[" + registerationDate + "]," + "iqama =["
				+ iqama + "], registrationSourceId=[" + registrationSourceId + "], facebookId=[" + facebookId + "], twitterId=[" + twitterId + "], contactNumber=[" + contactNumber
				+ "]";
	}	

}