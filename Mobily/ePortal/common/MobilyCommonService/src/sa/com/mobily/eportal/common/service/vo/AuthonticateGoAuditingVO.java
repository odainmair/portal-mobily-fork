/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author @moustafa.hassan.suliman - mhassan.suliman@gmail.com
 * 
 */
public class AuthonticateGoAuditingVO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int Id;

	private String srId;

	private String msisdn;

	private Date actionTime;

	private int moduleId;

	private int actionId;

	private String serviceName;

	private String serviceDescribtion;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getSrId() {
		return srId;
	}

	public void setSrId(String srId) {
		this.srId = srId;
	}

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public Date getActionTime() {
		return actionTime;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getActionId() {
		return actionId;
	}

	public void setActionId(int actionId) {
		this.actionId = actionId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceDescribtion() {
		return serviceDescribtion;
	}

	public void setServiceDescribtion(String serviceDescribtion) {
		this.serviceDescribtion = serviceDescribtion;
	}

	@PrePersist
	@PreUpdate
	public void updateAuditInfo() {		
		setActionTime(Calendar.getInstance().getTime());
	}
	
}
