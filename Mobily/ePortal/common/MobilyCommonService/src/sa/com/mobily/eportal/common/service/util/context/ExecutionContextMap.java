package sa.com.mobily.eportal.common.service.util.context;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.service.util.LoggerHelper;

public class ExecutionContextMap implements ExecutionContext
{
	private static SimpleFormatter formatter;

	private boolean devMode;

	public ExecutionContextMap(boolean devMode)
	{
		this.devMode = devMode;
	}

	private Map<String, StringBuffer> errorCaseLog = new ConcurrentHashMap<String, StringBuffer>();

	private Map<String, StringBuffer> successCaseLog = new ConcurrentHashMap<String, StringBuffer>();

	@Override
	public void log(LogEntryVO executionContextVO)
	{
		log(executionContextVO.getLogLevel(), executionContextVO.getMessage(), executionContextVO.getClassName(), executionContextVO.getMethodName(), executionContextVO.getError());
	}

	@Override
	public void log(Level logLevel, String message, String className, String methodName, Throwable error)
	{
		LogRecord record = new LogRecord(logLevel, message);
		if (error != null)
		{
			String exceptionDesc = error.toString() + " \n ";
			StackTraceElement[] elements = error.getStackTrace();
			for (int i = 0; i < elements.length; i++)
			{
				exceptionDesc += elements[i].toString() + " \n ";
			}
			record.setMessage(record.getMessage() + " \n " + exceptionDesc);
		}
		record.setSourceClassName(className);
		record.setSourceMethodName(methodName);
		if (logLevel.intValue() <= Level.INFO.intValue() && error == null)
			getSuccessCaseLog().append(getFormatter().format(record) + "\n");
		else
			getErrorCaseLog().append(getFormatter().format(record) + "\n");
	}

	@Override
	public void audit(LogEntryVO executionContextVO)
	{
		audit(executionContextVO.getLogLevel(), executionContextVO.getMessage(), executionContextVO.getClassName(), executionContextVO.getMethodName());
	}

	@Override
	public void audit(Level logLevel, String message, String className, String methodName)
	{
		LogRecord record = new LogRecord(logLevel, message);
		record.setSourceClassName(className);
		record.setSourceMethodName(methodName);
		getErrorCaseLog().append(getFormatter().format(record) + "\n");
	}

	@Override
	public void clearExecutionContext()
	{
		if (errorCaseLog.containsKey(Thread.currentThread().getName()))
		{
			errorCaseLog.remove(Thread.currentThread().getName());
		}

		if (successCaseLog.containsKey(Thread.currentThread().getName()))
		{
			successCaseLog.remove(Thread.currentThread().getName());
		}
	}

	private StringBuffer getErrorCaseLog()
	{
		StringBuffer executionContextBuffer = errorCaseLog.get(Thread.currentThread().getName());
		if (executionContextBuffer == null)
			return new StringBuffer();
		return executionContextBuffer;
	}

	private StringBuffer getSuccessCaseLog()
	{
		StringBuffer executionContextBuffer = successCaseLog.get(Thread.currentThread().getName());
		if (executionContextBuffer == null)
			return new StringBuffer();
		return executionContextBuffer;
	}

	@Override
	public void startMethod(String className, String methodName, List<Object> parameters)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("Method ");
		buffer.append(methodName);
		buffer.append(" is started");
		if (CollectionUtils.isNotEmpty(parameters))
		{
			buffer.append(" with input parameters:\n");
			for (Object object : parameters)
			{
				buffer.append(object);
				buffer.append("\n");
			}
		}

		if (successCaseLog.get(Thread.currentThread().getName()) == null)
			successCaseLog.put(Thread.currentThread().getName(), new StringBuffer());

		if (errorCaseLog.get(Thread.currentThread().getName()) == null)
			errorCaseLog.put(Thread.currentThread().getName(), new StringBuffer());

		LogRecord record = new LogRecord(Level.INFO, buffer.toString());
		record.setSourceClassName(className);
		record.setSourceMethodName(methodName);
		getSuccessCaseLog().append(getFormatter().format(record) + "\n");
	}

	@Override
	public void endMethod(String className, String methodName, Object parameter)
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append("Method ");
		buffer.append(methodName);
		buffer.append(" is ended");
		if (parameter != null)
		{
			buffer.append(" with the result: ");
			buffer.append(parameter);
		}

		LogRecord record = new LogRecord(Level.INFO, buffer.toString());
		record.setSourceClassName(className);
		record.setSourceMethodName(methodName);
		getSuccessCaseLog().append(getFormatter().format(record) + "\n");
	}

	@Override
	public void print(String fileName, boolean exception)
	{
		Logger logger = LoggerHelper.getLogger(fileName);
		logger.setLevel(Level.FINEST);
		print(logger, getSuccessCaseLog());
		print(logger, getErrorCaseLog());
		// if (exception || devMode || getErrorCaseLog().indexOf("SEVERE") > 0)
		// {
		// print(logger, getErrorCaseLog());
		// }
	}

	private void print(Logger logger, StringBuffer messages)
	{
		if (messages != null && !messages.toString().trim().equals(""))
		{
			logger.logp(Level.INFO, logger.getName(), "", messages.toString());
		}
	}

	public Map<String, StringBuffer> getErrorMap()
	{
		return errorCaseLog;
	}

	public Map<String, StringBuffer> getSuccessMap()
	{
		return successCaseLog;
	}

	private static SimpleFormatter getFormatter()
	{
		if (formatter == null)
			formatter = new SimpleFormatter();
		return formatter;
	}

	@Override
	public void log(Level logLevel, String message, String className, String methodName)
	{
		log(logLevel, message, className, methodName, null);
	}

	@Override
	public void fine(String message)
	{
		getSuccessCaseLog().append(message + "\n");
	}

	@Override
	public void severe(String message)
	{
		getErrorCaseLog().append(message + "\n");
	}

	@Override
	public void info(String message)
	{
		getSuccessCaseLog().append(message + "\n");
	}
}