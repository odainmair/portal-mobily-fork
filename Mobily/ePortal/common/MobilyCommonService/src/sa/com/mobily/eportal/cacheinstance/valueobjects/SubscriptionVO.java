package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.io.Serializable;

public class SubscriptionVO implements Serializable, Cloneable
{
	private static final long serialVersionUID = 1646546464646L;

	private int subscriptionType;

	private int customerType;

	private String msisdn;

	private String serviceAccountNumber;
	
	private long subscriptionID;
	
	private String vipTier;

	public long getSubscriptionID()
	{
		return subscriptionID;
	}

	public void setSubscriptionID(long subscriptionID)
	{
		this.subscriptionID = subscriptionID;
	}

	public int getSubscriptionType()
	{
		return subscriptionType;
	}

	public void setSubscriptionType(int subscriptionType)
	{
		this.subscriptionType = subscriptionType;
	}

	public String getServiceAccountNumber()
	{
		return serviceAccountNumber;
	}

	public void setServiceAccountNumber(String serviceAccountNumber)
	{
		this.serviceAccountNumber = serviceAccountNumber;
	}

	public String getMsisdn()
	{
		return msisdn;
	}

	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}

	public int getCustomerType()
	{
		return customerType;
	}

	public void setCustomerType(int customerType)
	{
		this.customerType = customerType;
	}

	public String getVipTier()
	{
		return vipTier;
	}

	public void setVipTier(String vipTier)
	{
		this.vipTier = vipTier;
	}
	
}