/**
 * WiFiUserVO.java 
 * v.ravipati.mit
 * 07-Jul-2015 - 3:17:11 PM
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @version : 1.0
 * @author  : Venkat Rao Ravipati - MIT
 * @date    : 07-Jul-2015
 * @time    : 3:17:11 PM
 * @extends :
 * @implements :
 */
public class WiFiUserVO  extends BaseVO{
	private static final long serialVersionUID = 1L;
	private String userName = null;
	private String userType = null;
	private String MSISDN = null;
	private String dummyMSISDN = null;
	private String userStatus = null;
	private String status = null;
	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	/**
	 * @return the userType
	 */
	public String getUserType()
	{
		return userType;
	}
	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType)
	{
		this.userType = userType;
	}
	/**
	 * @return the mSISDN
	 */
	public String getMSISDN()
	{
		return MSISDN;
	}
	/**
	 * @param mSISDN the mSISDN to set
	 */
	public void setMSISDN(String mSISDN)
	{
		MSISDN = mSISDN;
	}
	/**
	 * @return the dummyMSISDN
	 */
	public String getDummyMSISDN()
	{
		return dummyMSISDN;
	}
	/**
	 * @param dummyMSISDN the dummyMSISDN to set
	 */
	public void setDummyMSISDN(String dummyMSISDN)
	{
		this.dummyMSISDN = dummyMSISDN;
	}
	/**
	 * @return the userStatus
	 */
	public String getUserStatus()
	{
		return userStatus;
	}
	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus)
	{
		this.userStatus = userStatus;
	}
	/**
	 * @return the status
	 */
	public String getStatus()
	{
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status)
	{
		this.status = status;
	}
	
	

}
