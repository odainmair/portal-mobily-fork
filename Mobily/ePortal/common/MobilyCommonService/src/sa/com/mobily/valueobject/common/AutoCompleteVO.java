package sa.com.mobily.valueobject.common;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class AutoCompleteVO extends BaseVO
{
	private static final long serialVersionUID = 7932863016322842328L;

	private String name;

	private String id;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}
}