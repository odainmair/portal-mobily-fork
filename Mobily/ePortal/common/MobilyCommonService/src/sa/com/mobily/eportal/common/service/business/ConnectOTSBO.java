package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.ConnectOTSDAO;
import sa.com.mobily.eportal.common.service.dao.imp.MQConnectOTSDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.vo.ConnectOTSVO;
import sa.com.mobily.eportal.common.service.xml.ConnectOTSXMLHandler;

/**
 * @author n.gundluru.mit
 *
 */
public class ConnectOTSBO {

	 private static final Logger log = LoggerInterface.log;
	 private static CustomerProfileBO customerProfileBO = new CustomerProfileBO();
	 
	/**
	 * @date: Jul 17, 2012
	 * @author: s.vathsavai.mit
	 * @description: 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public ConnectOTSVO doVoucherRecharge(ConnectOTSVO requestVO) throws MobilyCommonException {

		ConnectOTSVO replyVO = null;
		String msisdn = requestVO.getMsisdn();
		log.info("ConnectOTSBO > doVoucherRecharge > generate xml message for Customer ["+msisdn+"] ");
        
		//Check whether the recipient id is valid or not?  
		if(!FormatterUtility.isEmpty(msisdn)){
			    String xmlReply = "";
	            ConnectOTSXMLHandler  xmlHandler = new ConnectOTSXMLHandler();
	            String xmlMessage  = xmlHandler.generateVoucherPymtRequest(requestVO);
	            
	            log.debug("ConnectOTSBO > doVoucherRecharge > generate xml message for Customer ["+requestVO.getMsisdn()+"] > xmlRequest >"+xmlMessage);
	            
	            //get DAO object
	            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
	         	ConnectOTSDAO connectOTSDAO = (MQConnectOTSDAO)daoFactory.getConnectOTSDAO();
	         	xmlReply= connectOTSDAO.doVoucherRecharge(xmlMessage);
	            log.info("ConnectOTSBO > doVoucherRecharge > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > xmlReply >"+xmlReply);
	            
	            //parsing the xml reply
	            replyVO = xmlHandler.parseVoucherPymtReply(xmlReply);
		}else{
			throw new SystemException();
		}

        return replyVO;    
    }
	
	
	/**
	 * @date: Jul 17, 2012
	 * @author: s.vathsavai.mit
	 * @description: 
	 * @param requestVO
	 * @param isValidate : if it is true it will validate the customer id.
	 * @return
	 * @throws MobilyCommonException
	 */ 
	public ConnectOTSVO doVoucherRecharge(ConnectOTSVO requestVO, boolean isValidateCustomerID) throws MobilyCommonException {
		ConnectOTSVO replyVO = null;
		if(isValidateCustomerID) {
			if(!FormatterUtility.isEmpty(requestVO.getMsisdn()) && !FormatterUtility.isEmpty(requestVO.getCustomerId())) {
				String errorCode = FormatterUtility.checkNull(customerProfileBO.isValidCustomerId(requestVO.getMsisdn(), requestVO.getCustomerId()));
				log.info("ConnectOTSBO > doVoucherRecharge > generate xml message for Customer ["+requestVO.getMsisdn()+"], customer id ["+requestVO.getCustomerId()+"], errorCode ["+errorCode+"]");
				if(ConstantIfc.MDM_SUCCESS_STATUS_CODE.equalsIgnoreCase(errorCode)){
					replyVO = doVoucherRecharge(requestVO);
				}else{
					//E601017, E000212, E000214, E601024, E999999
					 replyVO = new ConnectOTSVO();
					 replyVO.setErrorCode(errorCode);
				}
			}else {
				log.fatal("ConnectOTSBO > doVoucherRecharge : mandatory data is missing: msisdn ["+requestVO.getCustomerId()+"], customer id ["+requestVO.getCustomerId()+"].");
				throw new SystemException();
			}
		}else {
			replyVO = doVoucherRecharge(requestVO);
		}
		return replyVO;
	}
	

	/**
	 * Used to generate xml request to get the msisdn for a sim number
	 * 
	 * @param requestVO
	 * @return
	 * @throws MobilyCommonException
	 */
	public ConnectOTSVO getMsisdnForSim(ConnectOTSVO requestVO) throws MobilyCommonException {
        
		ConnectOTSVO replyVO = null;
            String xmlReply = "";
            ConnectOTSXMLHandler  xmlHandler = new ConnectOTSXMLHandler();
            String xmlMessage  = xmlHandler.generateGetMsisdnRequest(requestVO);
            
            log.info("ConnectOTSBO > getMsisdnForSim > generate xml message for Customer ["+requestVO.getMsisdn()+"] > xmlRequest >"+xmlMessage);
            
            //get DAO object
            MQDAOFactory daoFactory = (MQDAOFactory)DAOFactory.getDAOFactory(DAOFactory.MQ);
         	ConnectOTSDAO connectOTSDAO = (MQConnectOTSDAO)daoFactory.getConnectOTSDAO();
         	xmlReply= connectOTSDAO.getMsidnForSim(xmlMessage);
            log.info("ConnectOTSBO > getMsisdnForSim > parsing the reply message for Customer ["+requestVO.getMsisdn()+"] > xmlReply >"+xmlReply);
            
            //parsing the xml reply
            replyVO = xmlHandler.parseGetMsisdnReply(xmlReply);

        return replyVO;    
    }
}