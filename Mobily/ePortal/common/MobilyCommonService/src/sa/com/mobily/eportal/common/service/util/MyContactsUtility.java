/**
 * 
 */
package sa.com.mobily.eportal.common.service.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import sa.com.mobily.eportal.cacheinstance.dao.MyContactsDAO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.ContactsVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

/**
 * @author n.gundluru.mit
 *
 */
public class MyContactsUtility
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("MobilyCommonService/commonServices");
	  private static final String className = MyContactsUtility.class.getName();
	  private static MyContactsUtility instance = new MyContactsUtility();
	  
	  public static MyContactsUtility getInstance()
	  {
	    return instance;
	  }
	  
	  public Map<String, String> getContactNames(long userAcctId)
	  {
	    String method = "getContactNames";
	    Map<String, String> contactsInfoMap = new HashMap();
	    
	    executionContext.log(Level.INFO, "For UserAcctId =[" + userAcctId + "]", className, method, null);
	    
	    List<ContactsVO> myContactsVoList = MyContactsDAO.getInstance().findContactsByAcountId(Long.valueOf(userAcctId));
	    if ((myContactsVoList != null) && (myContactsVoList.size() > 0))
	    {
	      executionContext.log(Level.INFO, "Contacts Present for userAcctId =[" + userAcctId + "]", className, method, null);
	      String name = "";
	      for (ContactsVO myContactsVoObj : myContactsVoList) {
	    	  
	    	  name = myContactsVoObj.getFirstName();
	    	  if(!FormatterUtility.isEmpty(myContactsVoObj.getLastName())){
	    		  name = name +" " + myContactsVoObj.getLastName();
	    	  }
	    	  
	        contactsInfoMap.put(myContactsVoObj.getPhone(),name); 
	        

	         // myContactsVoObj.getFirstName() + " " + myContactsVoObj.getLastName());
	      }
	    }
	    return contactsInfoMap;
	  }

}
