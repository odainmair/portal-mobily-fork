package sa.com.mobily.eportal.billing.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * 
 * @author n.gundluru.mit
 *
 */
public class MyFreeBalancesReplyVO extends BaseVO {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String freeMinutes = "0";
	private String freeCrossnetVoiceMinutes = "0";
	private String freeSocialMedia="0";
	private String freeSMS = "0";
	private String freeMMS = "0";
	private String freeGPRS = "";
	private String freeOnNetMinutes = "0";
	private String freeOnNetSMS = "0";
	private String freeOnNetMMS = "0";
	private String freeVideoMinutes = "0";
	private String freeVoiceSMS = "0";
	private String nationalMinutes = "0";
	private String rgMinutes = "0";
	private String memberMsisdn = "";
	private String memberName = "";
	private String freeWeekendMinutes = "0";
	private String freeWeekendGPRS = "0";
	private String freeCrossNetSMS = "0";

	//Added from Mobily Common Service App
	private String RGMinutes = "0";
	private String packageType = "";
	private String consumedGPRS = "0";
	private String freeGprsWithoutPromo = "0";
	
	private String freePizzaGPRS = "0";
	private String freePizzaGPRSExpiry = "";
	private String freePizzaSMS;
	private String freePizzaSMSExpiry = "";
	private String freePizzaMO;
	private String freePizzaMOExpiry = "";
	private String freePizzaMT;
	private String freePizzaMTExpiry = "";
	
	private String errorCode = "";
	private String returnCode = "";
	
	private String freeRoamingGPRS = "";
	private String freeRoamingMinutes = "0";
	private String freeRoamingMTMinutes = "0";
	
	private String fixedOnNet="0"; //SR22516 - ePortal Fixed Voice 
	private String fixedCrossNet="0";//SR22516 - ePortal Fixed Voice

	/**
	 * @return the freeMinutes
	 */
	public String getFreeMinutes()
	{
		return freeMinutes;
	}
	/**
	 * @param freeMinutes the freeMinutes to set
	 */
	public void setFreeMinutes(String freeMinutes)
	{
		this.freeMinutes = freeMinutes;
	}
	/**
	 * @return the freeCrossnetVoiceMinutes
	 */
	public String getFreeCrossnetVoiceMinutes()
	{
		return freeCrossnetVoiceMinutes;
	}
	/**
	 * @param freeCrossnetVoiceMinutes the freeCrossnetVoiceMinutes to set
	 */
	public void setFreeCrossnetVoiceMinutes(String freeCrossnetVoiceMinutes)
	{
		this.freeCrossnetVoiceMinutes = freeCrossnetVoiceMinutes;
	}
	/**
	 * @return the freeSMS
	 */
	public String getFreeSMS()
	{
		return freeSMS;
	}
	/**
	 * @param freeSMS the freeSMS to set
	 */
	public void setFreeSMS(String freeSMS)
	{
		this.freeSMS = freeSMS;
	}
	/**
	 * @return the freeMMS
	 */
	public String getFreeMMS()
	{
		return freeMMS;
	}
	/**
	 * @param freeMMS the freeMMS to set
	 */
	public void setFreeMMS(String freeMMS)
	{
		this.freeMMS = freeMMS;
	}
	/**
	 * @return the freeGPRS
	 */
	public String getFreeGPRS()
	{
		return freeGPRS;
	}
	/**
	 * @param freeGPRS the freeGPRS to set
	 */
	public void setFreeGPRS(String freeGPRS)
	{
		this.freeGPRS = freeGPRS;
	}
	/**
	 * @return the freeOnNetMinutes
	 */
	public String getFreeOnNetMinutes()
	{
		return freeOnNetMinutes;
	}
	/**
	 * @param freeOnNetMinutes the freeOnNetMinutes to set
	 */
	public void setFreeOnNetMinutes(String freeOnNetMinutes)
	{
		this.freeOnNetMinutes = freeOnNetMinutes;
	}
	/**
	 * @return the freeOnNetSMS
	 */
	public String getFreeOnNetSMS()
	{
		return freeOnNetSMS;
	}
	/**
	 * @param freeOnNetSMS the freeOnNetSMS to set
	 */
	public void setFreeOnNetSMS(String freeOnNetSMS)
	{
		this.freeOnNetSMS = freeOnNetSMS;
	}
	/**
	 * @return the freeOnNetMMS
	 */
	public String getFreeOnNetMMS()
	{
		return freeOnNetMMS;
	}
	/**
	 * @param freeOnNetMMS the freeOnNetMMS to set
	 */
	public void setFreeOnNetMMS(String freeOnNetMMS)
	{
		this.freeOnNetMMS = freeOnNetMMS;
	}
	/**
	 * @return the freeVoiceSMS
	 */
	public String getFreeVoiceSMS()
	{
		return freeVoiceSMS;
	}
	/**
	 * @param freeVoiceSMS the freeVoiceSMS to set
	 */
	public void setFreeVoiceSMS(String freeVoiceSMS)
	{
		this.freeVoiceSMS = freeVoiceSMS;
	}
	/**
	 * @return the nationalMinutes
	 */
	public String getNationalMinutes()
	{
		return nationalMinutes;
	}
	/**
	 * @param nationalMinutes the nationalMinutes to set
	 */
	public void setNationalMinutes(String nationalMinutes)
	{
		this.nationalMinutes = nationalMinutes;
	}
	/**
	 * @return the rgMinutes
	 */
	public String getRgMinutes()
	{
		return rgMinutes;
	}
	/**
	 * @param rgMinutes the rgMinutes to set
	 */
	public void setRgMinutes(String rgMinutes)
	{
		this.rgMinutes = rgMinutes;
	}
	/**
	 * @return the memberMsisdn
	 */
	public String getMemberMsisdn()
	{
		return memberMsisdn;
	}
	/**
	 * @param memberMsisdn the memberMsisdn to set
	 */
	public void setMemberMsisdn(String memberMsisdn)
	{
		this.memberMsisdn = memberMsisdn;
	}
	/**
	 * @return the memberName
	 */
	public String getMemberName()
	{
		return memberName;
	}
	/**
	 * @param memberName the memberName to set
	 */
	public void setMemberName(String memberName)
	{
		this.memberName = memberName;
	}
	/**
	 * @return the freeWeekendMinutes
	 */
	public String getFreeWeekendMinutes()
	{
		return freeWeekendMinutes;
	}
	/**
	 * @param freeWeekendMinutes the freeWeekendMinutes to set
	 */
	public void setFreeWeekendMinutes(String freeWeekendMinutes)
	{
		this.freeWeekendMinutes = freeWeekendMinutes;
	}
	/**
	 * @return the freeWeekendGPRS
	 */
	public String getFreeWeekendGPRS()
	{
		return freeWeekendGPRS;
	}
	/**
	 * @param freeWeekendGPRS the freeWeekendGPRS to set
	 */
	public void setFreeWeekendGPRS(String freeWeekendGPRS)
	{
		this.freeWeekendGPRS = freeWeekendGPRS;
	}
	/**
	 * @return the freeCrossNetSMS
	 */
	public String getFreeCrossNetSMS()
	{
		return freeCrossNetSMS;
	}
	/**
	 * @param freeCrossNetSMS the freeCrossNetSMS to set
	 */
	public void setFreeCrossNetSMS(String freeCrossNetSMS)
	{
		this.freeCrossNetSMS = freeCrossNetSMS;
	}
	/**
	 * @return the rGMinutes
	 */
	public String getRGMinutes()
	{
		return RGMinutes;
	}
	/**
	 * @param rGMinutes the rGMinutes to set
	 */
	public void setRGMinutes(String rGMinutes)
	{
		RGMinutes = rGMinutes;
	}
	/**
	 * @return the packageType
	 */
	public String getPackageType()
	{
		return packageType;
	}
	/**
	 * @param packageType the packageType to set
	 */
	public void setPackageType(String packageType)
	{
		this.packageType = packageType;
	}
	/**
	 * @return the consumedGPRS
	 */
	public String getConsumedGPRS()
	{
		return consumedGPRS;
	}
	/**
	 * @param consumedGPRS the consumedGPRS to set
	 */
	public void setConsumedGPRS(String consumedGPRS)
	{
		this.consumedGPRS = consumedGPRS;
	}
	/**
	 * @return the freeGprsWithoutPromo
	 */
	public String getFreeGprsWithoutPromo()
	{
		return freeGprsWithoutPromo;
	}
	/**
	 * @param freeGprsWithoutPromo the freeGprsWithoutPromo to set
	 */
	public void setFreeGprsWithoutPromo(String freeGprsWithoutPromo)
	{
		this.freeGprsWithoutPromo = freeGprsWithoutPromo;
	}
	/**
	 * @return the freePizzaGPRS
	 */
	public String getFreePizzaGPRS()
	{
		return freePizzaGPRS;
	}
	/**
	 * @param freePizzaGPRS the freePizzaGPRS to set
	 */
	public void setFreePizzaGPRS(String freePizzaGPRS)
	{
		this.freePizzaGPRS = freePizzaGPRS;
	}
	/**
	 * @return the freePizzaGPRSExpiry
	 */
	public String getFreePizzaGPRSExpiry()
	{
		return freePizzaGPRSExpiry;
	}
	/**
	 * @param freePizzaGPRSExpiry the freePizzaGPRSExpiry to set
	 */
	public void setFreePizzaGPRSExpiry(String freePizzaGPRSExpiry)
	{
		this.freePizzaGPRSExpiry = freePizzaGPRSExpiry;
	}
	/**
	 * @return the freePizzaSMS
	 */
	public String getFreePizzaSMS()
	{
		return freePizzaSMS;
	}
	/**
	 * @param freePizzaSMS the freePizzaSMS to set
	 */
	public void setFreePizzaSMS(String freePizzaSMS)
	{
		this.freePizzaSMS = freePizzaSMS;
	}
	/**
	 * @return the freePizzaSMSExpiry
	 */
	public String getFreePizzaSMSExpiry()
	{
		return freePizzaSMSExpiry;
	}
	/**
	 * @param freePizzaSMSExpiry the freePizzaSMSExpiry to set
	 */
	public void setFreePizzaSMSExpiry(String freePizzaSMSExpiry)
	{
		this.freePizzaSMSExpiry = freePizzaSMSExpiry;
	}
	/**
	 * @return the freePizzaMO
	 */
	public String getFreePizzaMO()
	{
		return freePizzaMO;
	}
	/**
	 * @param freePizzaMO the freePizzaMO to set
	 */
	public void setFreePizzaMO(String freePizzaMO)
	{
		this.freePizzaMO = freePizzaMO;
	}
	/**
	 * @return the freePizzaMOExpiry
	 */
	public String getFreePizzaMOExpiry()
	{
		return freePizzaMOExpiry;
	}
	/**
	 * @param freePizzaMOExpiry the freePizzaMOExpiry to set
	 */
	public void setFreePizzaMOExpiry(String freePizzaMOExpiry)
	{
		this.freePizzaMOExpiry = freePizzaMOExpiry;
	}
	/**
	 * @return the freePizzaMT
	 */
	public String getFreePizzaMT()
	{
		return freePizzaMT;
	}
	/**
	 * @param freePizzaMT the freePizzaMT to set
	 */
	public void setFreePizzaMT(String freePizzaMT)
	{
		this.freePizzaMT = freePizzaMT;
	}
	/**
	 * @return the freePizzaMTExpiry
	 */
	public String getFreePizzaMTExpiry()
	{
		return freePizzaMTExpiry;
	}
	/**
	 * @param freePizzaMTExpiry the freePizzaMTExpiry to set
	 */
	public void setFreePizzaMTExpiry(String freePizzaMTExpiry)
	{
		this.freePizzaMTExpiry = freePizzaMTExpiry;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode()
	{
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode)
	{
		this.errorCode = errorCode;
	}
	/**
	 * @return the returnCode
	 */
	public String getReturnCode()
	{
		return returnCode;
	}
	/**
	 * @param returnCode the returnCode to set
	 */
	public void setReturnCode(String returnCode)
	{
		this.returnCode = returnCode;
	}
	/**
	 * @return the freeSocialMedia
	 */
	public String getFreeSocialMedia()
	{
		return freeSocialMedia;
	}
	/**
	 * @param freeSocialMedia the freeSocialMedia to set
	 */
	public void setFreeSocialMedia(String freeSocialMedia)
	{
		this.freeSocialMedia = freeSocialMedia;
	}
	/**
	 * @return the freeVideoMinutes
	 */
	public String getFreeVideoMinutes()
	{
		return freeVideoMinutes;
	}
	/**
	 * @param freeVideoMinutes the freeVideoMinutes to set
	 */
	public void setFreeVideoMinutes(String freeVideoMinutes)
	{
		this.freeVideoMinutes = freeVideoMinutes;
	}
	/**
	 * @return the freeRoamingGPRS
	 */
	public String getFreeRoamingGPRS()
	{
		return freeRoamingGPRS;
	}
	/**
	 * @param freeRoamingGPRS the freeRoamingGPRS to set
	 */
	public void setFreeRoamingGPRS(String freeRoamingGPRS)
	{
		this.freeRoamingGPRS = freeRoamingGPRS;
	}
	/**
	 * @return the freeRoamingMinutes
	 */
	public String getFreeRoamingMinutes()
	{
		return freeRoamingMinutes;
	}
	/**
	 * @param freeRoamingMinutes the freeRoamingMinutes to set
	 */
	public void setFreeRoamingMinutes(String freeRoamingMinutes)
	{
		this.freeRoamingMinutes = freeRoamingMinutes;
	}
	/**
	 * @return the freeRoamingMTMinutes
	 */
	public String getFreeRoamingMTMinutes()
	{
		return freeRoamingMTMinutes;
	}
	/**
	 * @param freeRoamingMTMinutes the freeRoamingMTMinutes to set
	 */
	public void setFreeRoamingMTMinutes(String freeRoamingMTMinutes)
	{
		this.freeRoamingMTMinutes = freeRoamingMTMinutes;
	}
	/**
	 * @return the fixedOnNet
	 */
	public String getFixedOnNet()
	{
		return fixedOnNet;
	}
	/**
	 * @param fixedOnNet the fixedOnNet to set
	 */
	public void setFixedOnNet(String fixedOnNet)
	{
		this.fixedOnNet = fixedOnNet;
	}
	/**
	 * @return the fixedCrossNet
	 */
	public String getFixedCrossNet()
	{
		return fixedCrossNet;
	}
	/**
	 * @param fixedCrossNet the fixedCrossNet to set
	 */
	public void setFixedCrossNet(String fixedCrossNet)
	{
		this.fixedCrossNet = fixedCrossNet;
	}
	
	

	
}