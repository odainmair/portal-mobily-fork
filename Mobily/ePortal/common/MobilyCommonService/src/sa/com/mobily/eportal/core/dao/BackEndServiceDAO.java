package sa.com.mobily.eportal.core.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import sa.com.mobily.eportal.core.api.BackEndService;
import sa.com.mobily.eportal.core.service.DataSources;

public class BackEndServiceDAO extends AbstractDBDAO<BackEndService> {

	private static BackEndServiceDAO instance = new BackEndServiceDAO();

	protected BackEndServiceDAO() {
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static BackEndServiceDAO getInstance() {
		return instance;
	}

	@Override
	protected BackEndService mapDTO(ResultSet rs) throws SQLException {
		BackEndService backEndService = new BackEndService();
		backEndService.setId(rs.getLong("ID"));
		backEndService.setName(rs.getString("NAME"));
		return backEndService;
	}
}