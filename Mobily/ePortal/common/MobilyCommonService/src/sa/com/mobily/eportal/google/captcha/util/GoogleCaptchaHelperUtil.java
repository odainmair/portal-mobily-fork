package sa.com.mobily.eportal.google.captcha.util;

import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class GoogleCaptchaHelperUtil {
		
	private static GoogleCaptchaHelperUtil INSTANCE = null;
	
	private static ResourceEnvironmentProviderService resourceEnvironmentProviderService;
	private static ResourceEnvironmentProviderService forGoogleRecapREPS;
	private static boolean captchaKeysFromResourceEnv = false;
	
	/**
	 * @MethodName: getInstance, get Singleton Object reference with bundle 
	 * reference for Captcha property file.
	 * 
	 * @return RegistrationPropertiesUtil <code>INSTANCE</code>
	 * 
	 * @author Abu Sharaf
	 */
	public static GoogleCaptchaHelperUtil getInstance(){
		 
		captchaKeysFromResourceEnv = Boolean.valueOf(AppConfig.getInstance().get(GoogleCaptchaConstants.CAPTCHA_KEYS_FROM_SERVER).trim());
		if (captchaKeysFromResourceEnv)
			resourceEnvironmentProviderService = ResourceEnvironmentProviderService.getInstance();
		if(INSTANCE == null)
			INSTANCE = new GoogleCaptchaHelperUtil();
			
		if(forGoogleRecapREPS == null){
			forGoogleRecapREPS = ResourceEnvironmentProviderService.getInstance();
		}
		return INSTANCE;
	}
	
	/**
	 * @MethodName: getCaptchaPrivateKey, get Singleton Object reference with bundle 
	 * reference for Captcha config file.
	 * 
	 * @return RegistrationPropertiesUtil <code>INSTANCE</code>
	 * 
	 * @author Abu Sharaf
	 */
	public String getCaptchaPrivateKey(){
		String captchaPrivateKey = "";
		if (captchaKeysFromResourceEnv && resourceEnvironmentProviderService != null)
		{
			Object value = resourceEnvironmentProviderService.getPropertyValue(GoogleCaptchaConstants.RESOURCE_ENV_CAPTCHA_PRIVATE_KEY);
			if (value != null)
				captchaPrivateKey = (String) value;
		}
		else
			captchaPrivateKey = AppConfig.getInstance().get(GoogleCaptchaConstants.CAPTCHA_PRIVATE_KEY);
		
		return captchaPrivateKey;
	}
	
	public String getCaptchaPublicKey()
	{
		String publicKey = "";
		if (captchaKeysFromResourceEnv && resourceEnvironmentProviderService != null)
		{
			Object value = resourceEnvironmentProviderService.getPropertyValue(GoogleCaptchaConstants.RESOURCE_ENV_CAPTCHA_PUBLIC_KEY);
			if (value != null)
				publicKey = (String) value;
		}
		else{
			publicKey = AppConfig.getInstance().get(GoogleCaptchaConstants.CAPTCHA_PUBLIC_KEY);
		}
		return publicKey;
	}
	
	/**
	 * @MethodName: 
	 * @author v.ravipati.mit
	 */
	public String getReCaptchaV2SiteKey(){
		String reCaptchaV2SiteKey = "";
		if (forGoogleRecapREPS != null){
			Object value = forGoogleRecapREPS.getPropertyValue(GoogleCaptchaConstants.CAPTCHA_V2_SITE_KEY);
			if (value != null)
				reCaptchaV2SiteKey = (String) value;
		}
		else
			reCaptchaV2SiteKey = AppConfig.getInstance().get(GoogleCaptchaConstants.CAPTCHA_V2_SITE_KEY);
		
		return reCaptchaV2SiteKey;
	}
	
	public String getReCaptchaV2SecretKey(){
		String reCaptchaV2SercetKey = "";
		if (forGoogleRecapREPS != null)
		{
			Object value = forGoogleRecapREPS.getPropertyValue(GoogleCaptchaConstants.CAPTCHA_V2_SECRET_KEY);
			if (value != null)
				reCaptchaV2SercetKey = (String) value;
		}
		else{
			reCaptchaV2SercetKey = AppConfig.getInstance().get(GoogleCaptchaConstants.CAPTCHA_V2_SECRET_KEY);
		}
		return reCaptchaV2SercetKey;
	}
	
	public String getRemoteIP(){
		String remoteIP = "";
		if (forGoogleRecapREPS != null)
		{
			Object value = forGoogleRecapREPS.getPropertyValue(GoogleCaptchaConstants.CAPTCHA_V2_REMOTE_SERVER_IP);
			if (value != null)
				remoteIP = (String) value;
		}
		else{
			remoteIP = AppConfig.getInstance().get(GoogleCaptchaConstants.CAPTCHA_V2_REMOTE_SERVER_IP);
		}
		return remoteIP;
	}
}
