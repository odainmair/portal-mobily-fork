package sa.com.mobily.eportal.common.jms.nativejms;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.logging.Level;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.NamingException;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.MobilyApplicationException;
import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.MQUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

import com.ibm.jms.JMSTextMessage;
import com.ibm.mq.jms.JMSC;
import com.ibm.mq.jms.MQDestination;




public class MessageRequester
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.MQ_CONNECTION_HANDLER_SERVICE_LOGGER_NAME);

	private Connection connection;

	private Session session;

	private Destination replyQueue;

	private MessageProducer requestProducer;

	private MessageConsumer replyConsumer;

	// flag to determine enable or disable
	// requestProducer.setTimeToLive(paramLong);
	private boolean timeToLiveFlag = true;

	// time to live in milliseconds 15 seconds
	private long timeToLive =  Long.parseLong(ResourceEnvironmentProviderService.getInstance().getStringProperty("MQUEUE_TIME_TO_LIVE")); // 15000;

	// timeout in milliseconds 70 seconds
	private long timeout =  Long.parseLong(ResourceEnvironmentProviderService.getInstance().getStringProperty("MQUEUE_TIMEOUT")); //70000;
	
	private static String connectionFactoryJNDI = MobilyUtility.getMQJMSConfigPropertyValue(ConstantIfc.MQ_JMS_CONNECTION_FACTORY);
	
	private static QueueConnectionFactory queueConnectionFactory = null;

	public boolean isTimeToLiveFlag()
	{
		return timeToLiveFlag;
	}

	public void setTimeToLiveFlag(boolean timeToLiveFlag)
	{
		this.timeToLiveFlag = timeToLiveFlag;
	}

	protected MessageRequester()
	{
		super();
	}

	public static MessageRequester newRequestor(MQAuditVO mqAuditVO) throws JMSException, NamingException, MobilyApplicationException
	{
//		String connectionFactoryJNDI = MobilyUtility.getMQJMSConfigPropertyValue(ConstantIfc.MQ_JMS_CONNECTION_FACTORY);

		MessageRequester requestor = new MessageRequester();
		requestor.initialize(connectionFactoryJNDI, mqAuditVO.getRequestQueue(), mqAuditVO.getReplyQueue(), mqAuditVO.isSendingReply());
		return requestor;
	}

	protected void initialize(String qConnFactoryName, String requestQueueName, String replyQueueName, boolean isReply) throws NamingException, JMSException,
			MobilyApplicationException
	{
		// read m queue time to live variable
		try
		{
			timeToLive = Long.parseLong(ResourceEnvironmentProviderService.getInstance().getStringProperty("MQUEUE_TIME_TO_LIVE"));
		}
		catch (Exception e)
		{
			executionContext.severe("Unable to read time to live variable from resource enviroment ." + e);
		}

		try
		{
			timeout = Long.parseLong(ResourceEnvironmentProviderService.getInstance().getStringProperty("MQUEUE_TIMEOUT"));
			executionContext.fine("mq timeout resource enviroment variable=" + timeout);
		}
		catch (Exception e)
		{
			executionContext.severe("Unable to read timeout variable from resource enviroment." + e);
		}

		ServiceLocator serviceLocator = ServiceLocator.getInstance();
		if ( queueConnectionFactory == null )
		{
			queueConnectionFactory = (QueueConnectionFactory) serviceLocator.getQueueConnectionFactory(qConnFactoryName);
			executionContext.fine("New QueueConnectionFactory created");
			executionContext.severe("m.hassan.dar says connection created ");
		}
		connection = queueConnectionFactory.createConnection();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		if (isReply == false)
		{
			if (requestQueueName != null)
			{
				Destination requestQueue = serviceLocator.getQueue(MobilyUtility.getMQJMSConfigPropertyValue(requestQueueName));
				requestProducer = session.createProducer(requestQueue);
			}
			if (replyQueueName != null)
			{
				replyQueue = serviceLocator.getQueue(MobilyUtility.getMQJMSConfigPropertyValue(replyQueueName));
			}
		}
	}

	public void send(MQAuditVO mqAuditVO) throws JMSException
	{
		connection.start();
		TextMessage requestMessage = session.createTextMessage();
		requestMessage.setText(mqAuditVO.getMessage());
		requestMessage.setJMSReplyTo(replyQueue);

		// set time to live
		if (timeToLiveFlag == true)
			requestProducer.setTimeToLive(timeToLive);

		requestProducer.send(requestMessage);
		executionContext.severe("Fire And Forget request sent, mqAuditVO=" + mqAuditVO);
		mqAuditVO.setWaitingTime(0);
		
		saveMQAudit(mqAuditVO);
	}
	
	public void sendwithNoAudit(MQAuditVO mqAuditVO) throws JMSException
	{
		connection.start();
		TextMessage requestMessage = session.createTextMessage();
		requestMessage.setText(mqAuditVO.getMessage());
		requestMessage.setJMSReplyTo(replyQueue);

		// set time to live
		if (timeToLiveFlag == true)
			requestProducer.setTimeToLive(timeToLive);

		requestProducer.send(requestMessage);
		executionContext.severe("Fire And Forget request sent, mqAuditVO=" + mqAuditVO);
		mqAuditVO.setWaitingTime(0);
	}

	public String sendWithReply(MQAuditVO mqAuditVO) throws JMSException, UnsupportedEncodingException
	{
		String receivedMessage = "";
		TextMessage requestMessage = session.createTextMessage();
		requestMessage.setText(mqAuditVO.getMessage());
		requestMessage.setIntProperty("XMSC_WMQ_REPLYTO_STYLE", 1);
		requestMessage.setJMSReplyTo(replyQueue);
		try
		{
			connection.start();
			int waitingtime = -1;
			// set time to live
			requestProducer.setTimeToLive(timeToLive);
			long timeBeforeSending = new Date().getTime() / 1000;
			requestProducer.send(requestMessage);

			// Now, receive the reply
			replyConsumer = session.createConsumer(replyQueue, "JMSCorrelationID='" + requestMessage.getJMSMessageID() + "'");

			Message replyMessage = replyConsumer.receive(timeout);
			waitingtime = (int) (new Date().getTime() / 1000 - timeBeforeSending);
			if (replyMessage instanceof JMSTextMessage)
			{
				receivedMessage = ((JMSTextMessage) replyMessage).getText();
			}
			else if (replyMessage instanceof BytesMessage)
			{
				receivedMessage = convertBytesMessage((BytesMessage) replyMessage);
			}

			if (FormatterUtility.isEmpty(receivedMessage) && Math.abs(waitingtime - timeout / 1000) < 5)
			{
				mqAuditVO.setErrorMessage("Timed out");
			}
			else
			{
				mqAuditVO.setReply(receivedMessage);
			}
			executionContext.log(Level.INFO, "timeToLive =["+timeToLive+"], timeout =["+timeout+"] and waitingtime =["+new Integer(waitingtime)+"]", MessageRequester.class.getName(), "sendWithReply");
			
			mqAuditVO.setWaitingTime(new Integer(waitingtime));
			executionContext.severe("received reply after waitng time " + waitingtime + " mqAuditVO=" + mqAuditVO);
		}
		catch (Throwable t)
		{
			executionContext.log(Level.SEVERE, "Exception while sending receiving JMS message : " + t, MessageRequester.class.getName(), "sendWithReply", t);
			throw new RuntimeException(t);
		}
		try
		{
			saveMQAudit(mqAuditVO);
		}
		catch (Throwable e)
		{
			executionContext.severe("Error occcured during saving to MQAudit table ,exception=" + e);
		}
		return receivedMessage;
	}
	
	public String sendWithReplyWithNoAudit(MQAuditVO mqAuditVO) throws JMSException, UnsupportedEncodingException
	{
		String receivedMessage = "";
		TextMessage requestMessage = session.createTextMessage();
		requestMessage.setText(mqAuditVO.getMessage());
		requestMessage.setIntProperty("XMSC_WMQ_REPLYTO_STYLE", 1);
		requestMessage.setJMSReplyTo(replyQueue);
		try
		{
			connection.start();
			int waitingtime = -1;
			// set time to live
			requestProducer.setTimeToLive(timeToLive);
			long timeBeforeSending = new Date().getTime() / 1000;
			requestProducer.send(requestMessage);

			// Now, receive the reply
			replyConsumer = session.createConsumer(replyQueue, "JMSCorrelationID='" + requestMessage.getJMSMessageID() + "'");

			Message replyMessage = replyConsumer.receive(timeout);
			waitingtime = (int) (new Date().getTime() / 1000 - timeBeforeSending);
			if (replyMessage instanceof JMSTextMessage)
			{
				receivedMessage = ((JMSTextMessage) replyMessage).getText();
			}
			else if (replyMessage instanceof BytesMessage)
			{
				receivedMessage = convertBytesMessage((BytesMessage) replyMessage);
			}

			if (FormatterUtility.isEmpty(receivedMessage) && Math.abs(waitingtime - timeout / 1000) < 5)
			{
				mqAuditVO.setErrorMessage("Timed out");
			}
			else
			{
				mqAuditVO.setReply(receivedMessage);
			}
			executionContext.log(Level.INFO, "timeToLive =["+timeToLive+"], timeout =["+timeout+"] and waitingtime =["+new Integer(waitingtime)+"]", MessageRequester.class.getName(), "sendWithReply");
			
			mqAuditVO.setWaitingTime(new Integer(waitingtime));
			executionContext.severe("received reply after waitng time " + waitingtime + " mqAuditVO=" + mqAuditVO);
		}
		catch (Throwable t)
		{
			executionContext.log(Level.SEVERE, "Exception while sending receiving JMS message : " + t, MessageRequester.class.getName(), "sendWithReply", t);
			throw new RuntimeException(t);
		}
		
		return receivedMessage;
	}

	public void respondWithCorrelationID(String msg, String replyQueueName, byte[] correlationID) throws JMSException
	{
		connection.start();
		Destination destination = session.createQueue(replyQueueName);
		
		// If WMQ_MESSAGE_BODY is set to WMQ_MESSAGE_BODY_MQ, no additional header is added to the message body.
		//((MQDestination) destination).setMessageBodyStyle(com.ibm.msg.client.wmq.WMQConstants.WMQConstants.WMQ_MESSAGE_BODY_MQ);
		
		((MQDestination)destination).setTargetClient(JMSC.MQJMS_CLIENT_NONJMS_MQ);
		
		executionContext.info("After setting the Target Client");
		System.out.println("After setting the Target Client");
		
		// Create a MessageProducer from the Session to the Queue
		MessageProducer producer = session.createProducer(destination);
		producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		// Create a message
		TextMessage message = session.createTextMessage(msg);
		message.setJMSCorrelationIDAsBytes(correlationID);
		// Tell the producer to send the message
		producer.send(message);
		producer.close();

	}

	private void saveMQAudit(MQAuditVO mqAuditVO)
	{
		try
		{
        	String isAuditRequired = ResourceEnvironmentProviderService.getInstance().getStringProperty(ConstantsIfc.MQ_AUDIT_REQUIRED);
        	executionContext.severe("MessageRequester >  saveMQAudit > isAuditRequired =["+isAuditRequired+"]");

            if(FormatterUtility.isEmpty(isAuditRequired)
            		|| ConstantsIfc.TRUE.equalsIgnoreCase(FormatterUtility.checkNull(isAuditRequired))){
 			
				MQUtility.saveMQAudit(mqAuditVO);
				executionContext.severe("MessageRequester >  saveMQAudit > MQ request Saved Successfuly");
				
            }else{
            	executionContext.severe("MessageRequester >  saveMQAudit > MQ request Audit Not Required....");
            }
            	
		}
		catch (Exception e)
		{
			executionContext.severe("Exception while saving MQ request " + e);
			throw new RuntimeException(e);
		}
	}

	private String convertBytesMessage(BytesMessage bytesMessage) throws JMSException, UnsupportedEncodingException
	{
		String convertedMessage = "";
		int TEXT_LENGTH = new Long(bytesMessage.getBodyLength()).intValue();
		byte[] textBytes = new byte[TEXT_LENGTH];
		bytesMessage.readBytes(textBytes, TEXT_LENGTH);
		String codePage = bytesMessage.getStringProperty("JMS_IBM_Character_Set");
		convertedMessage = new String(textBytes, codePage);

		return convertedMessage;
	}

	public void cleanup()
	{
		if (requestProducer != null)
		{
			try
			{
				requestProducer.close();
			}
			catch (JMSException jmsex)
			{
				executionContext.severe("Producer could not be closed.");
				processJMSException(jmsex);
			}
		}
		if (replyConsumer != null)
		{
			try
			{
				replyConsumer.close();
			}
			catch (JMSException jmsex)
			{
				executionContext.severe("Consumer could not be closed.");
				processJMSException(jmsex);
			}
		}

		if (session != null)
		{
			try
			{
				session.close();
			}
			catch (JMSException jmsex)
			{
				executionContext.severe("Session could not be closed.");
				processJMSException(jmsex);
			}
		}

		if (connection != null)
		{
			try
			{
				connection.close();
				executionContext.severe("m.hassan.dar says connection closed");
			}
			catch (JMSException jmsex)
			{
				executionContext.severe("Connection could not be closed.");
				processJMSException(jmsex);
			}
		}
	}

	/**
	 * Process a JMSException and any associated inner exceptions.
	 * 
	 * @param jmsex
	 */
	private static void processJMSException(JMSException jmsex)
	{
		Throwable innerException = jmsex.getLinkedException();
		if (innerException != null)
		{
			executionContext.severe("Inner exception(s):" + innerException);
		}
		while (innerException != null)
		{
			// executionContext.severe(innerException.toString());
			innerException = innerException.getCause();
		}
		return;
	}
}
