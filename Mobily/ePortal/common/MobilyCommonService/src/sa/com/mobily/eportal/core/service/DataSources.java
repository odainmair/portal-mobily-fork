/**
 * 
 */
package sa.com.mobily.eportal.core.service;

/**
 * @author anas
 *
 */
public enum DataSources {
	DEFAULT ("jdbc/eeds"),
	SIEBLE ("jdbc/siebeldb"),
    CREDIT ("jdbc/creditdb"),
	BSLDB ("jdbc/bslprod"),
	BILLING ("jdbc/eedbBilling"),
	BILL_PROD ("jdbc/billprod"),
	FLDS_PROD ("jdbc/flds"),
	APPDB("jdbc/appdb"),
	EDIDB("jdbc/edi");
	
	DataSources(String text) {
		this.text = text;
	}
	@Override
    public String toString() {
        return text;
    }
    // members
    private final String text;
    
}
