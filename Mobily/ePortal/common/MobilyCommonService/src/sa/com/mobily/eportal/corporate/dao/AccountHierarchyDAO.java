package sa.com.mobily.eportal.corporate.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.vo.PagingVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.corporate.model.AccountHierarchy;
import sa.com.mobily.valueobject.common.AutoCompleteVO;

public class AccountHierarchyDAO extends AbstractDBDAO<AccountHierarchy>
{
	private static AccountHierarchyDAO instance = new AccountHierarchyDAO();

	public static final String COMMIRTIAL_PLAN_TYPE_PREPAID = "1";

	public static final String COMMIRTIAL_PLAN_TYPE_POSTPAID = "2";

	public static final String COMMIRTIAL_PLAN_TYPE_NEW_CONTROL = "3";

	public static final String COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS = "4";
	
	public static final String ACCOUNT_STATUS = "Active";
	
	public static final String IS_MBA_ACCOUNT = "Y";
	
	public static final String LINE_TYPE_POST_PAID = "LINE_TYPE_POST_PAID";
	
	public static final String LINE_TYPE_PRE_PAID = "LINE_TYPE_PRE_PAID";

	private static final String LinesByAccountNoPrepaid = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_PREPAID + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_NEW_CONTROL
			+ "' ) START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByServiceAndCriteriaPrepaid = "SELECT distinct ACC.* FROM ACCOUNT_HIERARCHY_CORP_TBL ACC INNER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND  ( ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_PREPAID
			+ "' OR ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' OR ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_NEW_CONTROL
			+ "' ) AND PKG.LINE_OF_BUSINESS = ? AND (ACC.MSISDN LIKE ? OR ACC.ACCOUNT_NO LIKE ? OR ACC.ALIAS_NAME LIKE ?) START WITH ACC.ACCOUNT_NO = ? AND ACC.IS_MBA = 'Y' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID";

//	private static final String LinesByServicePrepaid = "SELECT ACC.* FROM ACCOUNT_HIERARCHY_CORP_TBL ACC INNER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND  ( ACC.COMM_PLAN_TYPE = '"
//			+ COMMIRTIAL_PLAN_TYPE_PREPAID
//			+ "' OR ACC.COMM_PLAN_TYPE = '"
//			+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
//			+ "' OR ACC.COMM_PLAN_TYPE = '"
//			+ COMMIRTIAL_PLAN_TYPE_NEW_CONTROL
//			+ "' ) AND PKG.LINE_OF_BUSINESS = ? START WITH ACC.ACCOUNT_NO = ? AND ACC.IS_MBA = 'Y' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID";

	
	private static final String LinesByServicePrepaid = "SELECT distinct ACC.* FROM ACCOUNT_HIERARCHY_CORP_TBL ACC Left outer JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND ACC.ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND  PKG.COMMERCIAL_PLAN_TYPE in('"+COMMIRTIAL_PLAN_TYPE_PREPAID+"', '"+COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS+"', '"+COMMIRTIAL_PLAN_TYPE_NEW_CONTROL+"') AND PKG.LINE_OF_BUSINESS = ? START WITH ACC.ACCOUNT_NO = ? AND ACC.IS_MBA = '"+IS_MBA_ACCOUNT+"' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID";
	
	private static final String LinesByServiceAndCriteriaPrepaidNoJoin = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_PREPAID
			+ "' OR COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' OR COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_NEW_CONTROL
			+ "' ) AND LINE_OF_BUSINESS = ? AND (MSISDN LIKE ? OR ACCOUNT_NO LIKE ? OR ALIAS_NAME LIKE ?) START WITH ACCOUNT_NO = ? AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByServicePrepaidNoJoin = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_PREPAID + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_NEW_CONTROL
			+ "' ) AND LINE_OF_BUSINESS = ? START WITH ACCOUNT_NO = ? AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByCriteriaPrepaid = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_PREPAID + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_NEW_CONTROL
			+ "' ) AND (MSISDN LIKE ? OR ACCOUNT_NO LIKE ? OR ALIAS_NAME LIKE ?) START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByAccountNoPostpaid = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_POSTPAID + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' ) START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByServiceAndCriteriaPostpaidNoJoin = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_POSTPAID
			+ "' OR COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' ) AND LINE_OF_BUSINESS = ? AND (MSISDN LIKE ? OR ACCOUNT_NO LIKE ? OR ALIAS_NAME LIKE ?) START WITH ACCOUNT_NO = ? AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByServicePostpaidNoJoin = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_POSTPAID + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' ) AND LINE_OF_BUSINESS = ? START WITH ACCOUNT_NO = ? AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String LinesByServiceAndCriteriaPostpaid = "SELECT distinct ACC.* FROM ACCOUNT_HIERARCHY_CORP_TBL ACC INNER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND  ( ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_POSTPAID
			+ "' OR ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' ) AND PKG.LINE_OF_BUSINESS = ? AND (ACC.MSISDN LIKE ? OR ACC.ACCOUNT_NO LIKE ? OR ACC.ALIAS_NAME LIKE ?) START WITH ACC.ACCOUNT_NO = ? AND ACC.IS_MBA = 'Y' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID";

	private static final String LinesByServicePostpaid = "SELECT distinct ACC.* FROM ACCOUNT_HIERARCHY_CORP_TBL ACC INNER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND  ( ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_POSTPAID
			+ "' OR ACC.COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' ) AND PKG.LINE_OF_BUSINESS = ? START WITH ACC.ACCOUNT_NO = ? AND ACC.IS_MBA = 'Y' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID";

	private static final String LinesByCriteriaPostpaid = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  ( COMM_PLAN_TYPE = '"
			+ COMMIRTIAL_PLAN_TYPE_POSTPAID + "' OR COMM_PLAN_TYPE = '" + COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS
			+ "' ) AND (MSISDN LIKE ? OR ACCOUNT_NO LIKE ? OR ALIAS_NAME LIKE ?) START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String AllVoiceLinesForCorporate = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String MasterBillingAccount = "SELECT * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE IS_MBA='Y' AND ACCOUNT_TYPE='Billing' AND ACCOUNT_NO = ? ";

	private static final String INSERT_NEW_ACCOUNT = "Insert INTO ACCOUNT_HIERARCHY_CORP_TBL (ID,PARENT_ID,CUSTOMER_ID,ACCOUNT_NO,ACCOUNT_TYPE,ACCOUNT_STATUS,NAME,COMPANY_NAME,IS_MBA,ALIAS_NAME,EMAIL,MSISDN,SIM,VPNID,PACKAGE_NAME,PACKAGE_ID,LINE_OF_BUSINESS,PRODUCT_FAMILY,COMM_PLAN_TYPE,PRODUCT_TYPE, CUSTOMER_SEGMENT,ICE_CUBE_ELIGIBILITY,NO_OF_ACTIVE_SIMS,LAST_UPDATE_DATE) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

	private static final String DeleteAccountHierarchy = "DELETE FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE CUSTOMER_ID = ?";
	
	private static final String GET_ALL_LINES = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";
	
	private static final String GET_LINE_BY_MSISDN = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND (MSISDN = ? OR ACCOUNT_NO = ?) START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String GET_CONTROL_PLUS_LINES = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  " +
			"( COMM_PLAN_TYPE = '"	+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS	+ "' ) START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String GET_CONTROL_PLUS_LINES_BY_MSISDN = "SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND  " +
			"( COMM_PLAN_TYPE = '"	+ COMMIRTIAL_PLAN_TYPE_CONTROL_PLUS	+ "' ) AND MSISDN = ? START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID";

	private static final String GET_LINES_BY_SERVICE_AND_PRODUCT_FAMILY = "SELECT * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND " +
			"ACCOUNT_STATUS = '" + ACCOUNT_STATUS + "' AND " +
			"PACKAGE_NAME IS NOT NULL AND LINE_OF_BUSINESS = ? AND (PRODUCT_FAMILY = ? OR PRODUCT_FAMILY = '45') START WITH ACCOUNT_NO = ? CONNECT BY PRIOR ID = PARENT_ID";
	
	private static final String GET_LINE_DETAILS_BY_ACCOUNT_NO = "SELECT * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_NO = ?";
	
	private static final String GET_LINE_DETAILS_BY_MSISDN = "SELECT * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE MSISDN = ?";
	
	protected AccountHierarchyDAO()
	{
		super(DataSources.DEFAULT);
	}

	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static AccountHierarchyDAO getInstance()
	{
		return instance;
	}

	public List<AccountHierarchy> getLinesByAccountNo(String corporateAccountnumber, boolean isPrepaid)
	{
		String query = null;
		if (isPrepaid)
		{
			query = LinesByAccountNoPrepaid;
		}
		else
		{
			query = LinesByAccountNoPostpaid;
		}
		List<AccountHierarchy> lstAccountHierarchy = query(query, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}

	public List<AccountHierarchy> getLinesByCriteria(String corporateAccountnumber, String mobileNumber, boolean isPrepaid)
	{
		mobileNumber = "%" + mobileNumber + "%";
		String query = null;
		if (isPrepaid)
		{
			query = LinesByCriteriaPrepaid;
		}
		else
		{
			query = LinesByCriteriaPostpaid;
		}
		List<AccountHierarchy> lstAccountHierarchy = query(query, mobileNumber, mobileNumber, mobileNumber, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}

	public List<AccountHierarchy> getLinesByServiceAndCriteria(String corporateAccountnumber, String serviceID, String mobileNumber, boolean isPrepaid, boolean withJoin)
	{
		mobileNumber = "%" + mobileNumber + "%";
		String query = null;
		if (isPrepaid)
		{
			if (withJoin)
			{
				query = LinesByServiceAndCriteriaPrepaid;
			}
			else
			{
				query = LinesByServiceAndCriteriaPrepaidNoJoin;
			}
		}
		else
		{
			if (withJoin)
			{
				query = LinesByServiceAndCriteriaPostpaid;
			}
			else
			{
				query = LinesByServiceAndCriteriaPostpaidNoJoin;
			}
		}
		List<AccountHierarchy> lstAccountHierarchy = query(query, serviceID, mobileNumber, mobileNumber, mobileNumber, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}

	public List<AccountHierarchy> getLinesByService(String corporateAccountnumber, String serviceID, boolean isPrepaid, boolean withJoin)
	{
		String query = null;
		if (isPrepaid)
		{
			if (withJoin)
			{
				query = LinesByServicePrepaid;
			}
			else
			{
				query = LinesByServicePrepaidNoJoin;
			}
		}
		else
		{
			if (withJoin)
			{
				query = LinesByServicePostpaid;
			}
			else
			{
				query = LinesByServicePostpaidNoJoin;
			}
		}
		List<AccountHierarchy> lstAccountHierarchy = query(query, serviceID, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}

	public List<AccountHierarchy> getCorporateLines(String corporateAccountnumber)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(AllVoiceLinesForCorporate, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}

	public List<AccountHierarchy> getMaterBillingAccount(String corporateAccountBillingNumber)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(MasterBillingAccount, corporateAccountBillingNumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}

	public List<AutoCompleteVO> getCorporateLinesForAutoComplete(String corporateAccountnumber)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(AllVoiceLinesForCorporate, corporateAccountnumber);
		List<AutoCompleteVO> autoList = null;
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			autoList = new ArrayList<AutoCompleteVO>();
			for (AccountHierarchy accountHierarchy : lstAccountHierarchy)
			{
				AutoCompleteVO vo = new AutoCompleteVO();
				vo.setId(accountHierarchy.getMSISDN());
				vo.setName(accountHierarchy.getMSISDN());
				autoList.add(vo);
				vo = new AutoCompleteVO();
				vo.setId(accountHierarchy.getMSISDN());
				vo.setName(accountHierarchy.getACCOUNT_NO());
				autoList.add(vo);
				vo = new AutoCompleteVO();
				vo.setId(accountHierarchy.getMSISDN());
				vo.setName(accountHierarchy.getALIAS_NAME());
				autoList.add(vo);
			}
			return autoList;
		}
		return null;
	}
	
	public List<AutoCompleteVO> getCorporateLinesForAutoComplete(String corporateAccountnumber, String lineType)
	{
		List<AccountHierarchy> lstAccountHierarchy ;
		if(LINE_TYPE_POST_PAID.equals(lineType)){
			lstAccountHierarchy = query(LinesByAccountNoPostpaid, corporateAccountnumber);
		} else{
			lstAccountHierarchy = query(LinesByAccountNoPrepaid, corporateAccountnumber);
		}
		
		List<AutoCompleteVO> autoList = null;
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			autoList = new ArrayList<AutoCompleteVO>();
			for (AccountHierarchy accountHierarchy : lstAccountHierarchy)
			{
				AutoCompleteVO vo = new AutoCompleteVO();
				vo.setId(accountHierarchy.getMSISDN());
				vo.setName(accountHierarchy.getMSISDN());
				autoList.add(vo);
				vo = new AutoCompleteVO();
				vo.setId(accountHierarchy.getMSISDN());
				vo.setName(accountHierarchy.getACCOUNT_NO());
				autoList.add(vo);
				vo = new AutoCompleteVO();
				vo.setId(accountHierarchy.getMSISDN());
				vo.setName(accountHierarchy.getALIAS_NAME());
				autoList.add(vo);
			}
			return autoList;
		}
		return null;
	}

	public int saveCorporateUser(AccountHierarchy user)
	{
		return update(INSERT_NEW_ACCOUNT, user.getID(), user.getPARENT_ID(), user.getCUSTOMER_ID(), user.getACCOUNT_NO(), user.getACCOUNT_TYPE(), user.getACCOUNT_STATUS(),
				user.getNAME(), user.getCOMPANY_NAME(), user.getIS_MBA(), user.getALIAS_NAME(), user.getEMAIL(), user.getMSISDN(), user.getSIM(), user.getVPNID(),
				user.getPACKAGE_NAME(), user.getPACKAGE_ID(), user.getLINE_OF_BUSINESS(), user.getPRODUCT_FAMILY(), user.getCOMM_PLAN_TYPE(), user.getPRODUCT_TYPE(),
				user.getCUSTOMER_SEGMENT(), user.getICE_CUBE_ELIGIBILITY(), user.getNO_OF_ACTIVE_SIMS(), new java.sql.Date(new java.util.Date().getTime()));
	}

	public void deleteAccountHierarchy(String accountNumber)
	{
		update(DeleteAccountHierarchy, accountNumber);
	}

	public List<AccountHierarchy> getLinesByAccountNoWithPaging(String corporateAccountnumber, List<String> lineTypeList, PagingVO pagingVO, String serviceID, String mobileNumber)
	{
		int start = pagingVO.getStartIndex();
		int end = pagingVO.getEndIndex();
		boolean addServiceID = false;
		boolean addNumberOrName = false;
		
		if ( StringUtils.isNotEmpty(serviceID) )
		{
			addServiceID = true;
		}
		
		if ( StringUtils.isNotEmpty(mobileNumber) )
		{
			addNumberOrName = true;
		}
		
		StringBuffer buff = new StringBuffer();
		buff.append("select * from ( select a.*, ROWNUM rnum from ( ");
		//buff.append("SELECT distinct ACC.*, PKG.COMMERCIAL_PLAN_TYPE, PKG.PRODUCT_FAMILY as PF, PKG.LINE_OF_BUSINESS as LOB FROM ACCOUNT_HIERARCHY_CORP_TBL ACC LEFT OUTER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND ACC.ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND ACC.MSISDN IS NOT NULL AND ");
		buff.append("SELECT distinct ACC.*, PKG.COMMERCIAL_PLAN_TYPE, PKG.PRODUCT_FAMILY as PF, PKG.LINE_OF_BUSINESS as LOB FROM ACCOUNT_HIERARCHY_CORP_TBL ACC LEFT OUTER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND ACC.ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND ");
		String IN = null;
		if (CollectionUtils.isNotEmpty(lineTypeList))
		{
			IN = StringUtils.join(lineTypeList, ",");
			buff.append("PKG.COMMERCIAL_PLAN_TYPE IN (" + IN + ")");
			if ( addServiceID )
			{
				buff.append(" AND PKG.LINE_OF_BUSINESS = ?");
			}
			if ( addNumberOrName )
			{
				buff.append(" AND (ACC.MSISDN LIKE ? OR ACC.ACCOUNT_NO LIKE ? OR ACC.ALIAS_NAME LIKE ?)");
			}
			buff.append(" START WITH ACC.ACCOUNT_NO = ?  AND ACC.IS_MBA = 'Y' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID ORDER BY ACC.ACCOUNT_NO DESC");
			buff.append(" ) a where ROWNUM <= "+end+" ) where rnum  >= "+start);
			List<AccountHierarchy> lstAccountHierarchy = runAccountQuery(buff, corporateAccountnumber, serviceID, mobileNumber, addServiceID, addNumberOrName);
			
			if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
			{
				return lstAccountHierarchy;
			}
			else
			{
				buff = new StringBuffer();
				buff.append("select * from ( select a.*, ROWNUM rnum from ( ");
				//buff.append("SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND MSISDN IS NOT NULL AND ");
				buff.append("SELECT distinct * FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"'  AND ");
				buff.append("COMM_PLAN_TYPE IN (" + IN + ")");
				if ( addServiceID )
				{
					buff.append(" AND LINE_OF_BUSINESS = ?");
				}
				if ( addNumberOrName )
				{
					buff.append(" AND (MSISDN LIKE ? OR ACCOUNT_NO LIKE ? OR ALIAS_NAME LIKE ?)");
				}
				buff.append(" START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID ORDER BY ACCOUNT_NO DESC");
				buff.append(" ) a where ROWNUM <= "+end+" ) where rnum  >= "+start);
				
				lstAccountHierarchy = runAccountQuery(buff, corporateAccountnumber, serviceID, mobileNumber, addServiceID, addNumberOrName);
				
				if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
				{
					return lstAccountHierarchy;
				}
			}
		}
		return null;
	}
	
	private final List<AccountHierarchy> runAccountQuery(StringBuffer buff, String corporateAccountnumber, String serviceID, String mobileNumber , boolean addServiceID, boolean addNumberOrName )
	{
		List<AccountHierarchy> lstAccountHierarchy = null;
		mobileNumber = "%" + mobileNumber + "%";
		if (! addServiceID && ! addNumberOrName )
		{
			lstAccountHierarchy = query(buff.toString(), corporateAccountnumber);
		}
		else if(addServiceID && addNumberOrName )
		{
			lstAccountHierarchy = query(buff.toString(), serviceID, mobileNumber, mobileNumber, mobileNumber, corporateAccountnumber);
		}
		else if(addServiceID )
		{
			lstAccountHierarchy = query(buff.toString(), serviceID, corporateAccountnumber);
		}
		else if(addNumberOrName )
		{
			lstAccountHierarchy = query(buff.toString(), mobileNumber, mobileNumber, mobileNumber, corporateAccountnumber);
		}
		return lstAccountHierarchy;
	}

	public Integer getLinesByAccountNoCount(String corporateAccountnumber, List<String> lineTypeList, String serviceID, String mobileNumber)
	{
		int count = 0;
		boolean addServiceID = false;
		boolean addNumberOrName = false;
		if ( StringUtils.isNotEmpty(serviceID) )
		{
			addServiceID = true;
		}
		
		if ( StringUtils.isNotEmpty(mobileNumber) )
		{
			addNumberOrName = true;
		}
		StringBuffer buff = new StringBuffer();
		//buff.append("SELECT count(distinct msisdn) FROM ACCOUNT_HIERARCHY_CORP_TBL ACC LEFT OUTER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND ACC.ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND ACC.MSISDN IS NOT NULL AND ");
		buff.append("SELECT count(*) FROM ACCOUNT_HIERARCHY_CORP_TBL ACC LEFT OUTER JOIN MOBILY_PACKAGE_CATALOG_TBL PKG ON ACC.PACKAGE_ID = PKG.PACKAGE_ID WHERE ACC.ACCOUNT_TYPE = 'Service' AND ACC.ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND ");
		String IN = null;
		if (CollectionUtils.isNotEmpty(lineTypeList))
		{
			IN = StringUtils.join(lineTypeList, ",");
			buff.append("PKG.COMMERCIAL_PLAN_TYPE IN (" + IN + ")");
			if ( addServiceID )
			{
				buff.append(" AND PKG.LINE_OF_BUSINESS = ?");
			}
			if ( addNumberOrName )
			{
				buff.append(" AND (ACC.MSISDN LIKE ? OR ACC.ACCOUNT_NO LIKE ? OR ACC.ALIAS_NAME LIKE ?)");
			}
			buff.append(" START WITH ACC.ACCOUNT_NO = ?  AND ACC.IS_MBA = 'Y' CONNECT BY PRIOR ACC.ID = ACC.PARENT_ID ORDER BY ACC.ACCOUNT_NO DESC");
			count = runCountQuery(buff, corporateAccountnumber, serviceID, mobileNumber, addServiceID, addNumberOrName);
			if (count > 0)
			{
				return count;
			}
			else
			{
				buff = new StringBuffer();
				//buff.append("SELECT count(*) FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"'  AND MSISDN IS NOT NULL AND ");
				buff.append("SELECT count(*) FROM ACCOUNT_HIERARCHY_CORP_TBL WHERE ACCOUNT_TYPE = 'Service' AND ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"'  AND ");
				buff.append("COMM_PLAN_TYPE IN (" + IN + ")");
				if ( addServiceID )
				{
					buff.append(" AND LINE_OF_BUSINESS = ?");
				}
				if ( addNumberOrName )
				{
					buff.append(" AND (MSISDN LIKE ? OR ACCOUNT_NO LIKE ? OR ALIAS_NAME LIKE ?)");
				}
				buff.append(" START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID ORDER BY ACCOUNT_NO DESC");
				count = runCountQuery(buff, corporateAccountnumber, serviceID, mobileNumber, addServiceID, addNumberOrName);
				if (count > 0)
				{
					return count;
				}
			}
		}
		return count;
	}
	
	private final int runCountQuery(StringBuffer buff, String corporateAccountnumber, String serviceID, String mobileNumber , boolean addServiceID, boolean addNumberOrName )
	{
		int count = 0;
		mobileNumber = "%" + mobileNumber + "%";
		if (! addServiceID && ! addNumberOrName )
		{
			count = queryCount(buff.toString(), corporateAccountnumber);
		}
		else if(addServiceID && addNumberOrName )
		{
			count = queryCount(buff.toString(), serviceID, mobileNumber, mobileNumber, mobileNumber, corporateAccountnumber);
		}
		else if(addServiceID )
		{
			count = queryCount(buff.toString(), serviceID, corporateAccountnumber);
		}
		else if(addNumberOrName )
		{
			count = queryCount(buff.toString(), mobileNumber, mobileNumber, mobileNumber, corporateAccountnumber);
		}
		return count;
	}
	
	public List<AccountHierarchy> getAllLines(String corporateAccountnumber)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(GET_ALL_LINES, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}
	
	public List<AccountHierarchy> getAllLinesByMSISDN(String corporateAccountnumber, String msisdn)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(GET_LINE_BY_MSISDN, msisdn, msisdn, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}
	
	public List<AccountHierarchy> getControlPlusLines(String corporateAccountnumber, String mobileNumber)
	{
		List<AccountHierarchy> lstAccountHierarchy = null;
		if(FormatterUtility.isNotEmpty(mobileNumber)) {
			lstAccountHierarchy = query(GET_CONTROL_PLUS_LINES_BY_MSISDN, mobileNumber, corporateAccountnumber);
		} else {
			lstAccountHierarchy = query(GET_CONTROL_PLUS_LINES, corporateAccountnumber);
		}

		return lstAccountHierarchy;
	}
	
	public Integer getRemainingMonthsCountForEarlyExpiry(String corporateAccountnumber)
	{
		int count = 0;
		
		StringBuffer buff = new StringBuffer();
		buff.append("SELECT DISTINCT COUNT(*) FROM ACCOUNT_HIERARCHY_CORP_TBL ");
		buff.append("WHERE ACCOUNT_TYPE = 'Service'  AND line_of_business='2' and ACCOUNT_STATUS = '"+ACCOUNT_STATUS+"' AND comm_plan_type IN (2,4)");
		buff.append(" AND ABS(MCP_Remaining_Months) <= 2 START WITH ACCOUNT_NO = ?  AND IS_MBA = 'Y' CONNECT BY PRIOR ID = PARENT_ID ");
		
		count = queryCount(buff.toString(), corporateAccountnumber);

		return count;
	}

	public List<AccountHierarchy> getLinesByServiceAndProductFamily(String corporateAccountnumber, String serviceID, String productFamily)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(GET_LINES_BY_SERVICE_AND_PRODUCT_FAMILY, serviceID, productFamily, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}
	
	public List<AccountHierarchy> getLineDetailsByAccountNo(String corporateAccountnumber)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(GET_LINE_DETAILS_BY_ACCOUNT_NO, corporateAccountnumber);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}
	
	public List<AccountHierarchy> getLineDetailsByMsisdn(String msisdn)
	{
		List<AccountHierarchy> lstAccountHierarchy = query(GET_LINE_DETAILS_BY_MSISDN, msisdn);
		if (CollectionUtils.isNotEmpty(lstAccountHierarchy))
		{
			return lstAccountHierarchy;
		}
		return null;
	}
	
	@Override
	protected AccountHierarchy mapDTO(ResultSet rs) throws SQLException
	{
		AccountHierarchy accountHierarchy = new AccountHierarchy();
		accountHierarchy.setACCOUNT_NO(rs.getString("ACCOUNT_NO"));
		accountHierarchy.setACCOUNT_STATUS(rs.getString("ACCOUNT_STATUS"));
		accountHierarchy.setACCOUNT_TYPE(rs.getString("ACCOUNT_TYPE"));
		accountHierarchy.setALIAS_NAME(rs.getString("ALIAS_NAME"));
		String commPlanType = null;
		try
		{
			commPlanType = rs.getString("COMMERCIAL_PLAN_TYPE");
		}
		catch (Exception e)
		{
			// ignore in case this column does not exit in result set, t only
			// appears in join query
		}
		if (StringUtils.isEmpty(commPlanType))
		{
			accountHierarchy.setCOMM_PLAN_TYPE(rs.getString("COMM_PLAN_TYPE"));
		}
		else
		{
			accountHierarchy.setCOMM_PLAN_TYPE(commPlanType);
		}
		accountHierarchy.setCOMPANY_NAME(rs.getString("COMPANY_NAME"));
		accountHierarchy.setCUSTOMER_ID(rs.getString("CUSTOMER_ID"));
		accountHierarchy.setCUSTOMER_SEGMENT(rs.getString("CUSTOMER_SEGMENT"));
		accountHierarchy.setEMAIL(rs.getString("EMAIL"));
		accountHierarchy.setICE_CUBE_ELIGIBILITY(rs.getString("ICE_CUBE_ELIGIBILITY"));
		accountHierarchy.setID(rs.getString("ID"));
		accountHierarchy.setIS_MBA(rs.getString("IS_MBA"));
		accountHierarchy.setLAST_UPDATE_DATE(rs.getDate("LAST_UPDATE_DATE"));
		String lineOfBusiness = null;
		try
		{
			lineOfBusiness = rs.getString("LOB");
		}
		catch (Exception e)
		{
			// ignore in case this column does not exit in result set, t only
			// appears in join query
		}
		if (StringUtils.isEmpty(lineOfBusiness))
		{
			accountHierarchy.setLINE_OF_BUSINESS(rs.getString("LINE_OF_BUSINESS"));
		}
		else
		{
			accountHierarchy.setLINE_OF_BUSINESS(lineOfBusiness);
		}
		accountHierarchy.setMSISDN(rs.getString("MSISDN"));
		accountHierarchy.setNAME(rs.getString("NAME"));
		accountHierarchy.setNO_OF_ACTIVE_SIMS(rs.getInt("NO_OF_ACTIVE_SIMS"));
		accountHierarchy.setPACKAGE_ID(rs.getString("PACKAGE_ID"));
		accountHierarchy.setPACKAGE_NAME(rs.getString("PACKAGE_NAME"));
		accountHierarchy.setPARENT_ID(rs.getString("PARENT_ID"));
		String productFamily = null;
		try
		{
			productFamily = rs.getString("PF");
		}
		catch (Exception e)
		{
			// ignore in case this column does not exit in result set, t only
			// appears in join query
		}
		if (StringUtils.isEmpty(productFamily))
		{
			accountHierarchy.setPRODUCT_FAMILY(rs.getString("PRODUCT_FAMILY"));
		}
		else
		{
			accountHierarchy.setPRODUCT_FAMILY(productFamily);
		}
		accountHierarchy.setPRODUCT_TYPE(rs.getString("PRODUCT_TYPE"));
		accountHierarchy.setSIM(rs.getString("SIM"));
		accountHierarchy.setVPNID(rs.getString("VPNID"));
		
		accountHierarchy.setMODE_PACKAGE_NAME_EN(rs.getString("MODE_PACKAGE_NAME_EN"));
		accountHierarchy.setMODE_PACKAGE_NAME_AR(rs.getString("MODE_PACKAGE_NAME_AR"));
		
		String remainingMonths = null;
		try
		{
			remainingMonths = rs.getString("MCP_Remaining_Months");
		}
		catch (Exception e)
		{
			// ignore in case this column does not exit in result set
		}
		accountHierarchy.setREMAINING_MONTHS(remainingMonths);
		
		
		return accountHierarchy;
	}

	public static void main(String args[])
	{
		try
		{

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}