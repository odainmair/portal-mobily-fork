package sa.com.mobily.eportal.customerinfo.constant;

import java.util.ArrayList;
import java.util.List;

public abstract class CustomerTypeConstant {
	public static final String PrePaid = "Prepaid Plan";
	public static final String PostPaid = "Postpaid Plan";

	public static final int CUSTOMER_TYPE_PrePaid = 1;
	public static final int CUSTOMER_TYPE_PostPaid = 2;
	public static final int CUSTOMER_TYPE_CORPORATE = 3;

	public static final int getCustomerType(String customerTypeText) {
		int to_return = -1;

		if (customerTypeText.equals(PrePaid)) {
			to_return = CUSTOMER_TYPE_PrePaid;
		} else if (customerTypeText.equals(PostPaid)) {
			to_return = CUSTOMER_TYPE_PostPaid;
		}
		return to_return;
	}

	public static final List<String> getCustomerTypes() {
		List<String> subList = new ArrayList<String>();
		subList.add(PrePaid);
		subList.add(PostPaid);
		return subList;
	}

	public static final String getCustomerTypeText(int customerType) {
		String to_return = "";
		switch (customerType) {
		case CUSTOMER_TYPE_PrePaid:
			to_return = PrePaid;
			break;
		case CUSTOMER_TYPE_PostPaid:
			to_return = PostPaid;
			break;
		default:
			break;
		}
		return to_return;
	}
}
