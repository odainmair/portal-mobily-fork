//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;


/**
 * This class represents the Value Object used for transfer of the
 * data of the Pin Code.
 */
public class PinCode implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private PinCodeCaller pinCodeCaller;

    /**
     * Retrieves the Code of the Pin Code.
     * @return the value of the code.
     */
    public final String getCode() {
        return code;
    }

    /**
     * Sets the Code of the Pin Code.
     * @param value the new value of the code field.
     */
    public final void setCode(String value) {
        this.code = value;
    }

    /**
     * Retrieves the Pin Code Caller of the Pin Code.
     * @return the value of the pinCodeCaller.
     */
    public final PinCodeCaller getPinCodeCaller() {
        return pinCodeCaller;
    }

    /**
     * Sets the Pin Code Caller of the Pin Code.
     * @param value the new value of the pinCodeCaller field.
     */
    public final void setPinCodeCaller(PinCodeCaller value) {
        this.pinCodeCaller = value;
    }

    @Override
    public String toString() {
        return "PinCode ["
                + "code = " + code
                + ", pinCodeCaller = " + pinCodeCaller
                + "]";
    }

}
