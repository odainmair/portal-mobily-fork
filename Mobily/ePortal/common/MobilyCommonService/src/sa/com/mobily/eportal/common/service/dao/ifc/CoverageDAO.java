package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;


/**
 * 
 * @author n.gundluru.mit
 *
 */
public interface CoverageDAO {
    public String sendToMQWithReply(String xmlRequestMessage) throws MobilyCommonException;
}
