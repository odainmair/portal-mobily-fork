/*
 * Created on Feb 2, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;


/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SRHeaderReplyVO extends BaseVO implements ConstantIfc
{
    protected String funcId;
    protected String msgVersion;
    protected String requestorChannelId;
    protected Date   srDate;
    protected Date   srRcvDate;
    protected String srStatus;
    
    public String getFuncId()
    {
        return funcId;
    }
    public void setFuncId(String funcId)
    {
        this.funcId = funcId;
    }
    public String getMsgVersion()
    {
        return msgVersion;
    }
    public void setMsgVersion(String msgVersion)
    {
        this.msgVersion = msgVersion;
    }
    public String getRequestorChannelId()
    {
        return requestorChannelId;
    }
    public void setRequestorChannelId(String requestorChannelId)
    {
        this.requestorChannelId = requestorChannelId;
    }
    public Date getSrDate()
    {
        return srDate;
    }
    public void setSrDate(String srDate)
    {
        SimpleDateFormat tempSimpleDateFormat = new SimpleDateFormat( DATE_FORMAT );
        try
        {
            this.srDate = tempSimpleDateFormat.parse(srDate);
        }
        catch(ParseException tempParseException)
        {
            this.srDate = null;
        }
    }
    public Date getSrRcvDate()
    {
        return srRcvDate;
    }
    public void setSrRcvDate(String srRcvDate)
    {
        SimpleDateFormat tempSimpleDateFormat = new SimpleDateFormat( DATE_FORMAT );
        try
        {
            this.srRcvDate = tempSimpleDateFormat.parse(srRcvDate);
        }
        catch(ParseException tempParseException)
        {
            this.srRcvDate = null;
        }
    }
    public String getSrStatus()
    {
        return srStatus;
    }
    public void setSrStatus(String srStatus)
    {
        this.srStatus = srStatus;
    }
}
