//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the REST APIs to provide the error code
 * defined by VALIDATION_EXCEPTION.
 *
 * @see ErrorCodes
 */
public class ValidationException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public ValidationException(String message) {
        super(message, ErrorCodes.VALIDATION_EXCEPTION);
    }
    
    public ValidationException(String message, int errorCode) {
        super(message, errorCode);
    }
}
