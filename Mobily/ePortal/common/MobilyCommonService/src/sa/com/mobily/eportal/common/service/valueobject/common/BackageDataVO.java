/*
 * Created on Feb 20, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.valueobject.common;

import sa.com.mobily.eportal.common.service.vo.BaseVO;


/**
 * @author 
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class BackageDataVO extends BaseVO 
{
	private String packageId = null;
	private String packageNameEn = null;
	private String packageNameAr = null;
	private int nationalFVNCount;
	private int internationalFVNCount;
	private int familyAndFriendsCount;

	/**
	 * @return Returns the internationalFVNCount.
	 */
	public int getInternationalFVNCount() {
		return internationalFVNCount;
	}
	/**
	 * @param internationalFVNCount The internationalFVNCount to set.
	 */
	public void setInternationalFVNCount(int internationalFVNCount) {
		this.internationalFVNCount = internationalFVNCount;
	}
	/**
	 * @return Returns the nationalFVNCount.
	 */
	public int getNationalFVNCount() {
		return nationalFVNCount;
	}
	/**
	 * @param nationalFVNCount The nationalFVNCount to set.
	 */
	public void setNationalFVNCount(int nationalFVNCount) {
		this.nationalFVNCount = nationalFVNCount;
	}
	/**
	 * @return Returns the packageId.
	 */
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId The packageId to set.
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return Returns the packageNameAr.
	 */
	public String getPackageNameAr() {
		return packageNameAr;
	}
	/**
	 * @param packageNameAr The packageNameAr to set.
	 */
	public void setPackageNameAr(String packageNameAr) {
		this.packageNameAr = packageNameAr;
	}
	/**
	 * @return Returns the packageNameEn.
	 */
	public String getPackageNameEn() {
		return packageNameEn;
	}
	/**
	 * @param packageNameEn The packageNameEn to set.
	 */
	public void setPackageNameEn(String packageNameEn) {
		this.packageNameEn = packageNameEn;
	}
	
	/**
	 * @return Returns the familyAndFriendsCount.
	 */
	public int getFamilyAndFriendsCount() {
		return familyAndFriendsCount;
	}
	/**
	 * @param familyAndFriendsCount The familyAndFriendsCount to set.
	 */
	public void setFamilyAndFriendsCount(int familyAndFriendsCount) {
		this.familyAndFriendsCount = familyAndFriendsCount;
	}
}
