package sa.com.mobily.eportal.common.service.util;

import java.io.InputStream;
import java.sql.Blob;

import javax.sql.rowset.serial.SerialBlob;

import org.apache.log4j.Logger;
import org.apache.xerces.impl.dv.util.Base64;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneServiceDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.IPhoneRequestVO;


/**
 * 
 * @author n.gundluru.mit
 *
 */

public class IPhoneServiceUtility {

	private static final Logger log = LoggerInterface.log;
	
	/**
	 * Gets user details
	 * 
	 */
	public static void getUserDetails(IPhoneRequestVO requestVo) {
		if(requestVo != null){
			try {
				OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory.getDAOFactory(DAOFactory.ORACLE);
				IPhoneServiceDAO iphoneDAO = daoFactory.getOracleIPhoneServiceDAO();
				
				iphoneDAO.getUserDetails(requestVo);
				
			} catch (Exception e) {
			   	  log.fatal("IPhoneServiceUtility > getUserDetails > Exception > "+e.getMessage());
			   	  throw new SystemException(e);
			}	
		}
	}
	
	
	/**
	 * 
	 * @param imgObj
	 * @return
	 * @throws Exception
	 */
	public static String encodeFile(Blob imgObj) throws Exception{
		
		String encodedImage = "";
        int bytesRead = 0;
        int chunkSize = 10000000;
        byte[] chunk = new byte[chunkSize];
        InputStream is = imgObj.getBinaryStream();
        
        while ((bytesRead = is.read(chunk)) > 0) {
        	
            byte[] ba = new byte[bytesRead];
            for(int i = 0; i < ba.length;i++){
                ba[i] = chunk[i];
            }
            encodedImage = Base64.encode(ba);
        }
        return encodedImage;
	}

	/**
	 * Decodes the given encoded Image string to byte array
	 * 
	 * @param encodedString
	 * @return
	 * @throws Exception
	 */
	public static byte[] decodeFile(String encodedString) throws Exception{
		
		if(!FormatterUtility.isEmpty(encodedString)){
			return Base64.decode(encodedString);
		}else{
			return null;
		}
	}
	
	
	/**
	 * 
	 * @param imgObj
	 * @return
	 * @throws Exception
	 */
	public static Blob getBlobObject(byte[] byteAry) throws Exception{
		
		return new SerialBlob(byteAry);
	}
}