package sa.com.mobily.eportal.common.service.vo;

public class InactiveObject {

	private String msisdn ="";
	private String iqama ="";
	private int inactiveType =0;
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getIqama() {
		return iqama;
	}
	public void setIqama(String iqama) {
		this.iqama = iqama;
	}
	public int getInactiveType() {
		return inactiveType;
	}
	public void setInactiveType(int inactiveType) {
		this.inactiveType = inactiveType;
	}
}
