package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.vo.FootPrintVO;


/**
 * 
 * @author n.gundluru.mit
 *
 */

public interface FootPrintTrackingDAO {

	public void addRow(FootPrintVO  footPrintVO);
}
