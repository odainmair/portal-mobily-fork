package sa.com.mobily.eportal.common.service.dao.ifc;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;



/**
 * 
 * This insterface is used as the DAO interace for Database access
 * 
 * @author r.agarwal.mit
 * @since v1.0
 */
public interface PandaDAO {
    
	public String sendToMQWithReply(String xmlRequestMessage,String requestType) throws MobilyCommonException;

}
