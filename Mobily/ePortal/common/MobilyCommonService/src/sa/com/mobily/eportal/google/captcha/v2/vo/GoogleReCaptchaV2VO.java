package sa.com.mobily.eportal.google.captcha.v2.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class GoogleReCaptchaV2VO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String secretKey = null;
	private String siteResponse = null;
	private String remoteIp = null;
	
	public String getSecretKey()
	{
		return secretKey;
	}
	public void setSecretKey(String secretKey)
	{
		this.secretKey = secretKey;
	}
	public String getSiteResponse()
	{
		return siteResponse;
	}
	public void setSiteResponse(String siteResponse)
	{
		this.siteResponse = siteResponse;
	}
	public String getRemoteIp()
	{
		return remoteIp;
	}
	public void setRemoteIp(String remoteIp)
	{
		this.remoteIp = remoteIp;
	}
	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}

}
