package sa.com.mobily.eportal.common.service.vo;



public class DestinationTypeVO {
	  private int emirateId;
	  private int governateId;
	  private int cityId;
	  private int districtId;
	  
	  private String emirateNameEn;
	  private String emirateNameAr;
	  private String governateNameEn;
	  private String governateNameAr;
	  private String cityNameEn;
	  private String cityNameAr;
	  private String districtNameEn;
	  private String districtNameAr;
	  
	  private String longitude = "";
	  private String latitude = "";
	  private String type="";
	  private String layer; 
	    
	   
	
		/**
	 * @return the layer
	 */
	public String getLayer() {
		return layer;
	}
	/**
	 * @param layer the layer to set
	 */
	public void setLayer(String layer) {
		this.layer = layer;
	}
		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}
		/**
		 * @param type the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}
		/**
		 * @return the longitude
		 */
		public String getLongitude() {
			return longitude;
		}
		/**
		 * @param longitude the longitude to set
		 */
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		/**
		 * @return the latitude
		 */
		public String getLatitude() {
			return latitude;
		}
		/**
		 * @param latitude the latitude to set
		 */
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}	  
	  
	/**
	 * @return the emirateId
	 */
	public int getEmirateId() {
		return emirateId;
	}
	/**
	 * @param emirateId the emirateId to set
	 */
	public void setEmirateId(int emirateId) {
		this.emirateId = emirateId;
	}
	/**
	 * @return the governateId
	 */
	public int getGovernateId() {
		return governateId;
	}
	/**
	 * @param governateId the governateId to set
	 */
	public void setGovernateId(int governateId) {
		this.governateId = governateId;
	}
	/**
	 * @return the cityId
	 */
	public int getCityId() {
		return cityId;
	}
	/**
	 * @param cityId the cityId to set
	 */
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	/**
	 * @return the districtId
	 */
	public int getDistrictId() {
		return districtId;
	}
	/**
	 * @param districtId the districtId to set
	 */
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	/**
	 * @return the emirateNameEn
	 */
	public String getEmirateNameEn() {
		return emirateNameEn;
	}
	/**
	 * @param emirateNameEn the emirateNameEn to set
	 */
	public void setEmirateNameEn(String emirateNameEn) {
		this.emirateNameEn = emirateNameEn;
	}
	/**
	 * @return the emirateNameAr
	 */
	public String getEmirateNameAr() {
		return emirateNameAr;
	}
	/**
	 * @param emirateNameAr the emirateNameAr to set
	 */
	public void setEmirateNameAr(String emirateNameAr) {
		this.emirateNameAr = emirateNameAr;
	}
	/**
	 * @return the governateNameEn
	 */
	public String getGovernateNameEn() {
		return governateNameEn;
	}
	/**
	 * @param governateNameEn the governateNameEn to set
	 */
	public void setGovernateNameEn(String governateNameEn) {
		this.governateNameEn = governateNameEn;
	}
	/**
	 * @return the governateNameAr
	 */
	public String getGovernateNameAr() {
		return governateNameAr;
	}
	/**
	 * @param governateNameAr the governateNameAr to set
	 */
	public void setGovernateNameAr(String governateNameAr) {
		this.governateNameAr = governateNameAr;
	}
	/**
	 * @return the cityNameEn
	 */
	public String getCityNameEn() {
		return cityNameEn;
	}
	/**
	 * @param cityNameEn the cityNameEn to set
	 */
	public void setCityNameEn(String cityNameEn) {
		this.cityNameEn = cityNameEn;
	}
	/**
	 * @return the cityNameAr
	 */
	public String getCityNameAr() {
		return cityNameAr;
	}
	/**
	 * @param cityNameAr the cityNameAr to set
	 */
	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}
	/**
	 * @return the districtNameEn
	 */
	public String getDistrictNameEn() {
		return districtNameEn;
	}
	/**
	 * @param districtNameEn the districtNameEn to set
	 */
	public void setDistrictNameEn(String districtNameEn) {
		this.districtNameEn = districtNameEn;
	}
	/**
	 * @return the districtNameAr
	 */
	public String getDistrictNameAr() {
		return districtNameAr;
	}
	/**
	 * @param districtNameAr the districtNameAr to set
	 */
	public void setDistrictNameAr(String districtNameAr) {
		this.districtNameAr = districtNameAr;
	}
}
