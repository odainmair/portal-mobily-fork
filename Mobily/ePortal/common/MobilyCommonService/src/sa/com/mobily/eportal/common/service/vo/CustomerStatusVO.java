/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;


/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerStatusVO extends BaseVO{

	private String  errorCode ;
	private String  errorMsg ;
	private String  lineNumber;
	private int     status ;
	private int     reason;
    private String  statusOnly;
	private String  requesterUserId;

	  
    /**
     * @return Returns the errorCode.
     */
    public String getErrorCode() {
        return errorCode;
    }
    /**
     * @param errorCode The errorCode to set.
     */
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    /**
     * @return Returns the errorMsg.
     */
    public String getErrorMsg() {
        return errorMsg;
    }
    /**
     * @param errorMsg The errorMsg to set.
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    /**
     * @return Returns the lineNumber.
     */
    public String getLineNumber() {
        return lineNumber;
    }
    /**
     * @param lineNumber The lineNumber to set.
     */
    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }
    /**
     * @return Returns the reason.
     */
    public int getReason() {
        return reason;
    }
    /**
     * @param reason The reason to set.
     */
    public void setReason(int reason) {
        this.reason = reason;
    }
    /**
     * @return Returns the requesterUserId.
     */
    public String getRequesterUserId() {
        return requesterUserId;
    }
    /**
     * @param requesterUserId The requesterUserId to set.
     */
    public void setRequesterUserId(String requesterUserId) {
        this.requesterUserId = requesterUserId;
    }
    /**
     * @return Returns the status.
     */
    public int getStatus() {
        return status;
    }
    /**
     * @param status The status to set.
     */
    public void setStatus(int status) {
        this.status = status;
    }
    /**
     * @return Returns the statusOnly.
     */
    public String getStatusOnly() {
        return statusOnly;
    }
    /**
     * @param statusOnly The statusOnly to set.
     */
    public void setStatusOnly(String statusOnly) {
        this.statusOnly = statusOnly;
    }
}
