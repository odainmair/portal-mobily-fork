/**
 * 
 */
package sa.com.mobily.eportal.lp.util;

/**
 * @author n.gundluru.mit
 *
 */
public final class FailueDataAuditConstants {
	
	public static final String FAILURE_DATA_AUDIT_FILE_PATH = "/app/IBM/HTTPServer/htdocs/share/LPAuditFailures/";
	public static final String FILE_NOTIFICATION_ACTION = "LP_NOTIFICATION_ACTION_LP.txt";
	public static final String FILE_SERVICE_TRANSACTION = "LP_SERVICE_TRANSACTION_TBL.txt";
	public static final String FILE_USER_TRANSACTION = "LP_USER_TRANSACTION_TBL.txt";
	public static final String FILE_PAYMENT_TRANSACTION = "LP_PAYMENT_TRANS_TBL.txt";

}
