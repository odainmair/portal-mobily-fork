package sa.com.mobily.eportal.cacheinstance.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.cacheinstance.valueobjects.CorporateSubscriptionVO;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;


public class CorporateSubscriptionDAO extends AbstractDBDAO<CorporateSubscriptionVO>
{
	private static CorporateSubscriptionDAO INSTANCE = new CorporateSubscriptionDAO();

	private static final String COL_SUBSCRIPTION_ID = "SUBSCRIPTION_ID";

	private static final String COL_USER_ACCT_ID = "USER_ACCT_ID";

	private static final String COL_MASTER_BILLING_ACC = "MASTER_BILLING_ACC";

	private static final String COL_MOBILE = "MOBILE";

	private static final String COL_COMMERCIAL_ID = "COMMERCIAL_ID";

	private static final String COL_USER_ACTIVATED = "USER_ACTIVATED";
	
	private static final String COL_IS_SUB_ADMIN = "IS_SUB_ADMIN";
	
	private static final String COL_STATUS = "STATUS";
	
	private static final String COL_IS_PRIMARY = "IS_PRIMARY";

	private static final String US_FIND_ACTIVE_CORP_SUBSCRIPTION_BY_ACCOUNT_ID = "SELECT * FROM CORPORATE_SUBSCRIPTION WHERE USER_ACCT_ID=(SELECT USER_ACCT_ID FROM USER_ACCOUNTS WHERE lower(USER_NAME)= ?)";
	
	private static final String USER_SUBSCRIPTION_ADD = 
            "INSERT INTO CORPORATE_SUBSCRIPTION " +
            "(SUBSCRIPTION_ID,USER_ACCT_ID,MASTER_BILLING_ACC,MOBILE,COMMERCIAL_ID,USER_ACTIVATED) VALUES " +
            "(CORPORATE_ACCOUNT_SEQ.NEXTVAL,?, ?, ?, ?, ?)";
	
	private static final String USER_SUBSCRIPTION_UPDATE = 
            "UPDATE CORPORATE_SUBSCRIPTION SET USER_ACTIVATED=? WHERE SUBSCRIPTION_ID=? ";

// PBI13308, new method for sending Pin code for mobile number verification
	private static final String USER_MOBILE_NUMBER_UPDATE = 
            "UPDATE CORPORATE_SUBSCRIPTION SET MOBILE=? WHERE USER_ACCT_ID=(SELECT USER_ACCT_ID FROM USER_ACCOUNTS WHERE lower(USER_NAME)= ?) ";
    


	protected CorporateSubscriptionDAO()
	{
		super(DataSources.DEFAULT);
	}

	public static CorporateSubscriptionDAO getInstance()
	{
		return INSTANCE;
	}

	public List<CorporateSubscriptionVO> getCorporateSubscriptionsByAccountId(String userName)
	{
		return query(US_FIND_ACTIVE_CORP_SUBSCRIPTION_BY_ACCOUNT_ID, userName.toLowerCase());
	}
	
	/**
	 * Added for SOUQ Corporate Registration
	 * @author r.agarwal.mit
	 * @param dto
	 * @return
	 */
	public int addCorporateSubscription(CorporateSubscriptionVO dto)
	{
		return update(USER_SUBSCRIPTION_ADD, dto.getUserAccountId(), dto.getMasterBillingAccount(), dto.getMobile(), dto.getCommercialId(), dto.getUserActivated());
	}

	/**
	 * Added for SOUQ Corporate Registration
	 * @author r.agarwal.mit
	 * @param dto
	 * @return
	 */
	public int updateCorporateSubscriptionActivation(CorporateSubscriptionVO dto)
	{
		return update(USER_SUBSCRIPTION_UPDATE, dto.getUserActivated(), dto.getSubscriptionId());
	}

	@Override
	protected CorporateSubscriptionVO mapDTO(ResultSet rs) throws SQLException
	{
		CorporateSubscriptionVO corporateVO = new CorporateSubscriptionVO();
		corporateVO.setCommercialId(rs.getString(COL_COMMERCIAL_ID));
		corporateVO.setMasterBillingAccount(rs.getString(COL_MASTER_BILLING_ACC));
		corporateVO.setMobile(rs.getString(COL_MOBILE));
		corporateVO.setSubscriptionId(rs.getLong(COL_SUBSCRIPTION_ID));
		corporateVO.setUserAccountId(rs.getLong(COL_USER_ACCT_ID));
		corporateVO.setUserActivated(rs.getInt(COL_USER_ACTIVATED));
		corporateVO.setIsSubAdmin(rs.getString(COL_IS_SUB_ADMIN));
		corporateVO.setStatus(rs.getString(COL_STATUS));
		corporateVO.setIsPrimary(rs.getInt(COL_IS_PRIMARY));
		
		return corporateVO;
	}


	// PBI13308, new method for sending Pin code for mobile number verification
	public int updateCAPAdminMobile(String userName, String updatedMbNumber) {
		return update(USER_MOBILE_NUMBER_UPDATE, updatedMbNumber,userName);
		
    }
}