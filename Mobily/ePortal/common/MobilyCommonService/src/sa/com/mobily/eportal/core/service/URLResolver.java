//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class that provides mapping between URIs to objects.
 * It acts as a data structure for this mapping.
 * Its main functions is to add a URL through addURLMapping() and to
 * resolve a URL through the resolve() method.
 */
public class URLResolver<T> {

    /**
     * Mapping between the URL and the value to be resolved.
     *
     * The Key is either the (URL) in case of non-parameterized URLs
     * or (Base URL + "/*") in case of parameterized URLs
     *
     * The Value is either the value to be resolved in case of non-parameterized URLs
     * or in case of parameterized URLs, it will be an instance of the ParameterizedValue class
     */
    private Map<String, T> fURLMapping = new HashMap<String, T>();

    /**
     * A list of the base URLs of all parameterized URL, sorted by the string length
     */
    private List<ParameterizedURL<T>> fParamerizedBaseURLs = new ArrayList<ParameterizedURL<T>>();


    /**
     * Add a URL pattern to the data structure.
     * @param url the url pattern in the form of /base/abc/{param1}/{param2}
     * @param val the object value to be mapped to the given URL pattern
     */
    public void addURLMapping(String url, T val) {
        if (url.indexOf('{') == -1) {
            fURLMapping.put(url, val);
        } else {
            fParamerizedBaseURLs.add(new ParameterizedURL<T>(url, val));
            Collections.sort(fParamerizedBaseURLs);
        }
    }

    /**
     * Retrieves the object value associated with the given actual URL.
     * It also resolves the parameters of the URL pattern and stores the
     * actual values of the parameters in the sent params map
     * @param url the actual URL to be used for the mapping
     * @param params the resolved parameters
     * @return the object value of the mapping, or null if no mapping is found
     * @throws InvalidParametersException thrown when the URL mapping is
     * incorrect, such as when a parameter in the URL patter cannot be found
     */
    public T resolve(String url, Map<String, String> params) throws InvalidParametersException {
        T val;
        int index = url.indexOf('?');
        if (index == -1) {
            val = resolveURL(url, params);
        } else {
            val = resolveURL(url.substring(0, index), params);
            if (val != null) fillParams(url.substring(index + 1), params);
        }
        return val;
    }

    private void fillParams(String paramsStr, Map<String, String> params) {
        if (paramsStr.length() == 0) return;
        int index = 0;
        while (index >= 0) {
            int index1 = paramsStr.indexOf('=', index);
            int index2 = index1 == -1 ? -1 : paramsStr.indexOf('&', index1 + 1);
            if (index1 == -1) {
                index = -1;
            } else if (index2 == -1) {
                params.put(paramsStr.substring(index, index1), paramsStr.substring(index1 + 1));
                index = -1;
            } else {
                params.put(paramsStr.substring(index, index1), paramsStr.substring(index1 + 1, index2));
                index = index2 + 1;
                if (index >= paramsStr.length()) index = -1;
            }
        }
    }

    private T resolveURL(String url, Map<String, String> params) throws InvalidParametersException {
        T val = fURLMapping.get(url);
        if (val != null) return val;

        boolean matched = false;
        InvalidParametersException exception = null;
        for (int i = 0; i < fParamerizedBaseURLs.size(); i++) {
            ParameterizedURL<T> parameterizedURL = fParamerizedBaseURLs.get(i);
            if (parameterizedURL.match(url)) {
                matched = true;
                try {
                    return parameterizedURL.fillParametersMap(url, params);
                } catch (InvalidParametersException e) {
                    exception = e;
                }
            } else if (matched) {
                break;
            }
        }

        if (exception != null) throw exception;

        return null;
    }

    public static String resolveParam(String paramValue, Map<String, String> params) {
        int index = paramValue.indexOf('{');
        if (index >= 0) {
            StringBuffer value = new StringBuffer();
            value.append(paramValue.substring(0, index));
            while (index >= 0) {
                int index2 = paramValue.indexOf('}', index);
                if (index2 < 0) {
                    throw new IllegalArgumentException("Invalid Param: " + paramValue);
                }
                String paramName = paramValue.substring(index + 1, index2);
                String paramVal = params.get(paramName);
                if (paramVal != null) {
                    value.append(paramVal);
                }
                index = paramValue.indexOf('{', index2 + 1);
                if (index < 0) {
                    value.append(paramValue.substring(index2 + 1));
                } else {
                    value.append(paramValue.substring(index2 + 1, index));
                }
            }

            // Truncate any trailing slashes (if any)
            while (value.length() > 0 && value.charAt(value.length() - 1) == '/') {
                value.setLength(value.length() - 1);
            }

            return value.toString();
        } else {
            return paramValue;
        }
    }

    private static class ParameterizedURL<T> implements Comparable<ParameterizedURL<T>> {
        private String baseURL;
        private String url;
        private ParameterName[] parameterNames;
        private T value;

        public ParameterizedURL(String url, T value) {
            this.url = url;
            this.baseURL = getBaseURL(url);
            this.parameterNames = getParameterNames(url);
            this.value = value;
        }

        public boolean match(String url) {
            if (!url.startsWith(baseURL)) return false;
            String parameters = url.substring(baseURL.length());
            if (parameters.length() > 0 && !parameters.startsWith("/")) {
                return false;
            }
            return true;
        }

        public T fillParametersMap(String url, Map<String, String> params) throws InvalidParametersException {
            String parameters = url.substring(baseURL.length());
            String[] paramValues = parameters.length() == 0 ? new String[0] : parameters.substring(1).split("/");
            if (paramValues.length > parameterNames.length) {
                throw new InvalidParametersException("Too many parameters for URL: " + this.url + ", " + url);
            }

            // 1. Assign the given values to the parameters
            for (int i = 0; i < paramValues.length; i++) {
                if (parameterNames[i].isParametrized) {
                    params.put(parameterNames[i].name, paramValues[i]);
                } else {
                    if (!parameterNames[i].name.equals(paramValues[i])) {
                        throw new InvalidParametersException("Invalid parameters for URL: " + this.url);
                    }
                }
            }

            // 2. Validate that the rest of the paramters are optional
            for (int i = paramValues.length; i < parameterNames.length; i++) {
                if (!parameterNames[i].isOptional) {
                    throw new InvalidParametersException("Missing Parameter: " + parameterNames[i] + " for URL: " + this.url
                            + ", actual URL: " + url + ", params: " + params);
                }
            }

            return value;
        }

        public int compareTo(ParameterizedURL<T> o) {
            // to provide descending sort
            int ret = o.baseURL.length() - baseURL.length();
            if (ret == 0) ret = baseURL.compareTo(o.baseURL);
            if (ret == 0) ret = url.compareTo(o.url);
            return ret;
        }

        private static String getBaseURL(String url) {
            int index = url.indexOf('{');
            if (index >= 0) {
                if (url.charAt(index - 1) == '/') {
                    index--;
                }
                return url.substring(0, index);
            } else {
                return url;
            }
        }

        private static ParameterName[] getParameterNames(String url) {
            int index = url.indexOf('{');
            if (index >= 0) {
                String[] params = url.substring(index).split("/");
                ParameterName[] paramNames = new ParameterName[params.length];
                for (int i = 0; i < params.length; i++) {
                    paramNames[i] = new ParameterName(params[i]);
                }
                return paramNames;
            } else {
                return null;
            }
        }

        private static class ParameterName {
            public String name;
            public boolean isParametrized;
            public boolean isOptional;

            public ParameterName(String param) {
                if (param.startsWith("{")) {
                    if (!param.endsWith("}")) {
                        throw new IllegalArgumentException("Invalid Parameter name: " + param);
                    }
                    isParametrized = true;
                    if (param.endsWith("?}")) {
                        isOptional = true;
                        name = param.substring(1, param.length() - 2);
                    } else {
                        name = param.substring(1, param.length() - 1);
                    }
                } else {
                    name = param;
                }
            }

            public String toString() {
                return "[name=" + name + ", isParametrized=" + isParametrized  + ", isOptional=" + isOptional + "]";
            }
        }

    }

}
