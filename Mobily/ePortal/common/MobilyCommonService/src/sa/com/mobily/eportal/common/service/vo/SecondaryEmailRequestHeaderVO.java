/*
 * Created on Sep 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author abadr
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SecondaryEmailRequestHeaderVO {

	private String funcId = null;
	private String channelId = null;
	private String requestDate = null;

	/**
	 * @return Returns the channelId.
	 */
	public String getChannelId() {
		return channelId;
	}
	/**
	 * @param channelId The channelId to set.
	 */
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	/**
	 * @return Returns the funcId.
	 */
	public String getFuncId() {
		return funcId;
	}
	/**
	 * @param funcId The funcId to set.
	 */
	public void setFuncId(String funcId) {
		this.funcId = funcId;
	}
	/**
	 * @return Returns the requestDate.
	 */
	public String getRequestDate() {
		return requestDate;
	}
	/**
	 * @param requestDate The requestDate to set.
	 */
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
}
