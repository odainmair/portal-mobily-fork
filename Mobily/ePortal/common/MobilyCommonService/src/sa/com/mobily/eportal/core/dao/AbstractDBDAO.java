//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.core.service.Logger;

public abstract class AbstractDBDAO<T>
{

	protected static Logger logger = Logger.getLogger(AbstractDBDAO.class);
	
	protected static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	protected static final String className = AbstractDBDAO.class.getName();

	protected DataSource ds = null;

	protected AbstractDBDAO(DataSources datasourceConfig)
	{
		String methodName = "AbstractDBDAO";
		try
		{
			ds = lookup(datasourceConfig.toString());
		}
		catch (NamingException e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
//			logger.throwing("Constructor", new Exception(e));
			throw new IllegalStateException("Couldn't lookup: " + datasourceConfig + ", due to " + e.getMessage());
		}
	}

	private DataSource lookup(String jndiName) throws NamingException
	{
		InitialContext ic = new InitialContext();
		DataSource dataSource = (DataSource) ic.lookup(jndiName);
		return dataSource;
	}

	/**
	 * @return database connection
	 * @throws SQLException
	 */
	protected Connection getConnection() throws SQLException
	{
		return ds.getConnection();
	}

	/**
	 * This method should be overriden if the inherited DAO does not own
	 * connection.
	 * 
	 * @param conn
	 */
	protected void closeConnection(Connection conn)
	{
		String methodName = "closeConnection"; 
		try
		{
			if (conn != null)
				conn.close();
		}
		catch (Exception e)
		{
//			logger.throwing("closeConnection", e);
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
		}
	}

	/**
	 * This method gets a prepared query and a the needed parameters, to return
	 * a list of the dtos representing news items
	 * 
	 * @param connection
	 *            : the connection object.
	 * @param sql
	 *            : the sql statement that searches the database
	 * @param paramValues
	 *            : a list of params to build the
	 * @return a list of retrieved dtos
	 */
	protected final List<T> query(Connection conn, String sql, Object... paramValues)
	{
		String methodName = "query";
		if (sql == null)
		{
			throw new RuntimeException("SQL Statement not set, " + getClass().getName());
		}
		ResultSet rs = null;

		try
		{
//			logger.debug("** SQL = " + sql);
			executionContext.log(Level.INFO, "** SQL = " + sql, className, methodName);
			rs = executeQuery(conn, sql, paramValues, mapParamTypes(paramValues));

			if (rs == null)
			{
//				logger.debug("@@@ result set is NULL");
				executionContext.log(Level.INFO, "@@@ result set is NULL", className, methodName);
			}
			else
			{
//				logger.debug("@@@ result set is not null");
				executionContext.log(Level.INFO, "@@@ result set is not null", className, methodName);
			}
			List<T> dtos = getDTOs(rs);

			if (dtos != null)
			{
//				logger.debug("dtos list size is: " + dtos == null ? "NULL" : dtos.size() + "");
				executionContext.log(Level.INFO, "dtos list size is: " + dtos == null ? "NULL" : dtos.size() + "", className, methodName);
			}

			return dtos;

		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e);

		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
			}
			catch (Exception e)
			{
//				logger.throwing("query", e);
				executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			}
		}
	}

	protected final Integer queryCount(Connection conn, String sql, Object... paramValues)
	{
		String methodName = "queryCount";
		if (sql == null)
		{
			throw new RuntimeException("SQL Statement not set, " + getClass().getName());
		}
		ResultSet rs = null;
		int count = 0;
		try
		{
//			logger.debug("** SQL = " + sql);
			executionContext.log(Level.INFO, "** SQL = " + sql, className, methodName);
			rs = executeQuery(conn, sql, paramValues, mapParamTypes(paramValues));

			if (rs == null)
			{
//				logger.debug("@@@ result set is NULL");
				executionContext.log(Level.INFO, "@@@ result set is NULL", className, methodName);
			}
			else
			{
//				logger.debug("@@@ result set is not null");
				executionContext.log(Level.INFO, "@@@ result set is not null", className, methodName);
			}
			if (rs.next())
			{
				count = rs.getInt(1);
			}
			return count;

		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e);

		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
			}
			catch (Exception e)
			{
//				logger.throwing("query", e);
				executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			}
		}
	}

	/**
	 * This method gets a prepared query and a the needed parameters, to return
	 * a list of the dtos representing news items with the generated keys
	 * 
	 * @param connection
	 *            : the connection object.
	 * @param sql
	 *            : the sql statement that searches the database
	 * @param paramValues
	 *            : a list of params to build the
	 * @return a list of retrieved dtos
	 */
	protected final List<T> updateWithKeys(Connection conn, String sql)
	{
		String methodName = "updateWithKeys";
		if (sql == null)
		{
			throw new RuntimeException("SQL Statement not set, " + getClass().getName());
		}
		ResultSet rs = null;
		try
		{
			rs = executeUpdateWithKeys(conn, sql);
			List<T> dtos = getDTOs(rs);
			return dtos;

		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e);

		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
			}
			catch (Exception e)
			{
//				logger.throwing("updateWithKeys", e);
				executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			}
		}
	}

	/**
	 * This method gets a prepared query and a the needed parameters, to return
	 * a list of the dtos representing news items
	 * 
	 * @param sql
	 *            : the sql statement that searches the database
	 * @param paramValues
	 *            : a list of params to build the
	 * @return a list of retrieved dtos
	 */
	protected final List<T> query(String sql, Object... paramValues)
	{
		String methodName = "query";
		List<T> results = null;
		Connection conn = null;

		try
		{
			conn = getConnection();
			results = query(conn, sql, paramValues);
		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e.getMessage());
		}
		finally
		{
			closeConnection(conn);
		}
		return results;
	}

	protected final Integer queryCount(String sql, Object... paramValues)
	{
		String methodName = "queryCount";
		int results = 0;
		Connection conn = null;

		try
		{
			conn = getConnection();
			results = queryCount(conn, sql, paramValues);
		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e.getMessage());
		}
		finally
		{
			closeConnection(conn);
		}
		return results;
	}

	/***
	 * Executes a stored procedure call and returns the results as DTOs<br/>
	 * The method assumes the last parameter of the procedure is an out
	 * parameter of type Cursor. This parameter will be used to extract the
	 * return of the method.
	 * 
	 * @param procedureCall
	 *            procedure call statement with parameters placeholders,
	 *            including a placeholder for the last output parameter <br/>
	 *            Example: <code>"call myproc(?, ?, ?)"</code>
	 * @param inputParamValues
	 *            a list of input parameter values
	 * @return
	 */
	protected final List<T> queryProcedure(String procedureCall, Object... inputParamValues)
	{
		List<T> results = null;
		Connection conn = null;

		try
		{
			conn = getConnection();
			results = queryProcedure(conn, procedureCall, inputParamValues);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		finally
		{
			closeConnection(conn);
		}
		return results;
	}

	protected final List<T> queryProcedure(Connection conn, String sql, Object... paramValues)
	{
		String methodName = "queryProcedure";
		if (sql == null)
		{
			throw new RuntimeException("Procedure call statement not set, " + getClass().getName());
		}
		ResultSet rs = null;

		try
		{
//			logger.debug("** SQL = " + sql);
			executionContext.log(Level.INFO, "** SQL = " + sql, className, methodName);
			rs = executeQueryProcedure(conn, sql, paramValues, mapParamTypes(paramValues));

			if (rs == null)
			{
//				logger.debug("@@@ result set is NULL");
				executionContext.log(Level.INFO, "@@@ result set is NULL", className, methodName);
			}
			else
			{
//				logger.debug("@@@ result set is not null");
				executionContext.log(Level.INFO, "@@@ result set is not null", className, methodName);
			}
			List<T> dtos = getDTOs(rs);

			if (dtos != null)
			{
//				logger.debug("dtos list size is: " + dtos == null ? "NULL" : dtos.size() + "");
				executionContext.log(Level.INFO, "dtos list size is: " + dtos == null ? "NULL" : dtos.size() + "", className, methodName);
			}

			return dtos;

		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e);

		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
			}
			catch (Exception e)
			{
//				logger.throwing("query", e);
				executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			}
		}
	}

	/**
	 * This method gets a prepared query and a the needed parameters, to return
	 * a list of the dtos representing news items
	 * 
	 * @param sql
	 *            : the sql statement that searches the database
	 * @param paramValues
	 *            : a list of params to build the
	 * @return a list of retrieved dtos
	 */
	protected final List<T> updateWithKeys(String sql)
	{
		List<T> results = null;
		Connection conn = null;
		try
		{
			conn = getConnection();
			results = updateWithKeys(conn, sql);
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e.getMessage());
		}
		finally
		{
			closeConnection(conn);
		}
		return results;
	}

	/**
	 * Method to insert row using prepared statement and returns the generated
	 * ID of the inserted row
	 * 
	 * @param conn
	 *            Connection
	 * @param sql
	 *            SQL statement
	 * @param paramValues
	 * @param paramTypes
	 * @return
	 * @throws SQLException
	 */
	private ResultSet executeUpdateWithKeys(Connection conn, String sql, Object[] paramValues, int[] paramTypes) throws SQLException
	{

		PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		if (paramValues != null)
		{
			for (int i = 0; i < paramValues.length; i++)
			{
				stmt.setObject(i + 1, paramValues[i], paramTypes[i]);
			}
		}

		stmt.executeUpdate();
		return stmt.getGeneratedKeys();
	}

	/**
	 * This method gets a prepared query and a the needed parameters, to update,
	 * insert or delete database records
	 * 
	 * @param sql
	 * @param paramValues
	 * @return the number of processed dtabase records
	 */
	protected final int update(Connection conn, String sql, Object... paramValues)
	{
		String methodName = "update";
		if (sql == null)
		{
			throw new RuntimeException("SQL Statement not set, " + getClass().getName());
		}
		ResultSet rs = null;

//		logger.debug("%%% inside AbstractSQL STATEMENT IS: " + sql);
		//executionContext.log(Level.INFO, "%%% inside AbstractSQL STATEMENT IS: " + sql, className, methodName);
		try
		{
			return executeUpdate(conn, sql, paramValues, mapParamTypes(paramValues));

		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e);

		}
		finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
				}
			}
			catch (Exception e)
			{
//				logger.throwing(e.getMessage(), e);
				executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			}
		}
	}

	/**
	 * This method gets a prepared query and a the needed parameters, to update,
	 * insert or delete database records
	 * 
	 * @param sql
	 * @param paramValues
	 * @return the number of processed dtabase records
	 */
	protected final int update(String sql, Object... paramValues)
	{
		String methodName = "update";
		Connection conn = null;

		try
		{
			conn = getConnection();
			return update(conn, sql, paramValues);

		}
		catch (SQLException e)
		{
//			e.printStackTrace();
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new RuntimeException(e);

		}
		finally
		{
			closeConnection(conn);
		}
	}

	/**
	 * This method maps one record of the resultset retrieved form the database
	 * into DTO. Every subclass implements this method based on the specific
	 * business need
	 * 
	 * @param rs
	 * @return one DTO
	 * @throws SQLException
	 */
	protected abstract T mapDTO(ResultSet rs) throws SQLException;

	/**
	 * This method looks for the datasource reference in the map using the JNDI
	 * name. If it's not found, the method looks it up in the initial context
	 * 
	 * @param jndiName
	 * @return datasource reference
	 */
	protected static DataSource getDataSource(String jndiName)
	{
		String methodName = "getDataSource";
		try
		{
			InitialContext ic = new InitialContext();
			DataSource dataSource = (DataSource) ic.lookup(jndiName);
			return dataSource;

		}
		catch (Throwable e)
		{
//			logger.throwing("getDataSource", new Exception(e));
			executionContext.log(Level.SEVERE, e.getMessage(), className, methodName,e);
			throw new IllegalStateException("Couldn't lookup: " + jndiName + ", due to " + e.getMessage());
		}
	}

	/**
	 * 
	 * @param conn
	 * @param sql
	 * @param paramValues
	 * @param paramTypes
	 * @return the number of processed database records
	 * @throws SQLException
	 */
	private int executeUpdate(Connection conn, String sql, Object[] paramValues, int[] paramTypes) throws SQLException
	{
		PreparedStatement stmt = conn.prepareStatement(sql);
		if (paramValues != null)
		{
			for (int i = 0; i < paramValues.length; i++)
			{
				stmt.setObject(i + 1, paramValues[i], paramTypes[i]);
			}
		}
		return stmt.executeUpdate();
	}

	/**
	 * Maps the records in the resultset into a list of DTOs
	 * 
	 * @param rs
	 * @return array of DTOs
	 * @throws SQLException
	 */
	private List<T> getDTOs(ResultSet rs) throws SQLException
	{
		String methodName = "getDTOs";
//		logger.entering("getDTOs");
		executionContext.log(Level.INFO, "entering getDTOs", className, methodName);
		List<T> dtos = new ArrayList<T>();
		while (rs.next())
		{
			dtos.add(mapDTO(rs));
		}
//		logger.exiting("getDTOs");
		executionContext.log(Level.INFO, "exiting getDTOs", className, methodName);
		return dtos;
	}

	/**
	 * @param conn
	 * @param sql
	 * @param paramValues
	 * @param paramTypes
	 * @return
	 * @throws SQLException
	 */
	private ResultSet executeQuery(Connection conn, String sql, Object[] paramValues, int[] paramTypes) throws SQLException
	{

		PreparedStatement stmt = conn.prepareStatement(sql);
		if (paramValues != null)
		{
			for (int i = 0; i < paramValues.length; i++)
			{
				stmt.setObject(i + 1, paramValues[i], paramTypes[i]);
			}
		}

		return stmt.executeQuery();
	}

	/***
	 * Executes a call to stored procedure and extracts a ResultSet from the
	 * procedure's last output parameter<br/>
	 * <br/>
	 * The procedure must have an output parameter of type cursor as a last
	 * parameter, which will be used to extract the returned resultSet
	 * 
	 * @param conn
	 *            the connection to be used
	 * @param sql
	 *            the procedure call statement with parameter place holders
	 * @param paramValues
	 *            the list of input parameter values
	 * @param paramTypes
	 *            the list of input parameter types
	 * @return a ResultSet extracted from the last output parameter of the
	 *         procedure
	 * @throws SQLException
	 */
	protected ResultSet executeQueryProcedure(Connection conn, String sql, Object[] paramValues, int[] paramTypes) throws SQLException
	{

		CallableStatement stmt = conn.prepareCall(sql);
		int paramIndx = 0;
		if (paramValues != null)
		{
			// add the input parameters
			for (paramIndx = 0; paramIndx < paramValues.length; paramIndx++)
			{
				stmt.setObject(paramIndx + 1, paramValues[paramIndx], paramTypes[paramIndx]);
			}
		}

		// last parameter should be the output cursor parameter
		stmt.registerOutParameter(paramIndx + 1, OracleTypes.CURSOR);
		stmt.execute();
		// get the last output parameter as a ResultSet
		ResultSet rs = (ResultSet) stmt.getObject(paramIndx + 1);
		return rs;
	}

	/**
	 * @param conn
	 * @param sql
	 * @param paramValues
	 * @param paramTypes
	 * @return
	 * @throws SQLException
	 */
	private ResultSet executeUpdateWithKeys(Connection conn, String sql) throws SQLException
	{

		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = stmt.getGeneratedKeys();

		return rs;
	}

	/**
	 * Maps the java types of the passed query parameters into database types
	 * 
	 * @param paramValues
	 * @return list of database types
	 */
	private int[] mapParamTypes(Object[] paramValues)
	{
		if (paramValues == null)
			return null;
		int[] types = new int[paramValues.length];

		for (int i = 0; i < paramValues.length; i++)
		{
			paramValues[i] = convertUnsupportedTypes(paramValues[i]);
			types[i] = mapParamType(paramValues[i]);
		}
		return types;
	}

	private Object convertUnsupportedTypes(Object paramValue)
	{

		if (paramValue == null)
		{
			return null;
		}
		
		// if a date is passed as java.sql.Timestamp
		if (paramValue instanceof java.sql.Timestamp)
		{
			// return same
			return paramValue;
		} else if (paramValue instanceof java.util.Date) // if a date is passed as java.util.Date
		{
			// convert it into java.sql.Date
			return new java.sql.Date(((java.util.Date) paramValue).getTime());
		}

		return paramValue;
	}

	/**
	 * @param paramValue
	 * @return supported database type
	 */
	private int mapParamType(Object paramValue)
	{
		if (paramValue == null)
		{
			return Types.NULL;
		}

		if (paramValue instanceof Short)
		{
			return Types.SMALLINT;
		}
		else if (paramValue instanceof Integer)
		{
			return Types.INTEGER;
		}
		else if (paramValue instanceof Long)
		{
			return Types.BIGINT;
		}
		else if (paramValue instanceof Float)
		{
			return Types.FLOAT;
		}
		else if (paramValue instanceof Double)
		{
			return Types.DOUBLE;
		}
		else if (paramValue instanceof Character)
		{
			return Types.CHAR;
		}
		else if (paramValue instanceof String)
		{
			return Types.VARCHAR;
		}
		else if (paramValue instanceof java.sql.Time)
		{
			return Types.TIME;
		}
		else if (paramValue instanceof Timestamp)
		{
			return Types.TIMESTAMP;
		}
		else if (paramValue instanceof java.util.Date)
		{
			return Types.DATE;
		}
		else if (paramValue instanceof byte[])
		{
			// commented by MHASSAN
			// return Types.BLOB;
			// added by MHASSAN
			return Types.BINARY;

		}
		else if (paramValue instanceof Boolean)
		{
			return Types.BOOLEAN;
		}
		else
		{
			throw new IllegalArgumentException("Unsupported type: " + paramValue.getClass().getName() + ", " + paramValue);
		}
	}

}
