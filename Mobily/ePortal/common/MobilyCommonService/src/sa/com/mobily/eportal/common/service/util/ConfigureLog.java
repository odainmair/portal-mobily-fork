/*
 * Created on Nov 26, 2005
 */
package sa.com.mobily.eportal.common.service.util;

import java.util.Enumeration;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.helpers.Loader;

/**
 * @author sea
 *
 * This class contains the basic functionality to locate and load 
 * main configuration of LOG4J module
 */
public class ConfigureLog {

	// the following static final member will represent the logger for the eportal common jar and all other methods in this class will not be used any more
	// this change made by Ayman Hassn in 3/4/2007 and all the log4j setting will be loaded from the XML file only 
	public static final Logger log = Logger.getLogger("eportalCommon_logger");
	public static final Logger SMS_logger = Logger.getLogger("sms_logger");
	private static final Logger logger = sa.com.mobily.eportal.common.service.logger.LoggerInterface.log;
	/**
	 * This flag is used to indicate if configuration is already set
	 */
	private static boolean configurationSet = false;
	/**
	 * read and load LOG4J configuration 
	 * should be called by any logger in the appliation
	 */
	public static void configure(){
		if(!configurationSet){
			logger.debug(" :O:O:O:O Reading the Log4j Configuration ...");
			// Do the configuration
			try
			{
				java.net.URL tmp_logConfURL = Loader.getResource("log4j.properties");
				if(tmp_logConfURL!=null){
					logger.debug(" URL for configuration file = "+tmp_logConfURL);
					PropertyConfigurator.configure(tmp_logConfURL);
					logger.debug("configuration Done :D");
					// Set the flag
					configurationSet = true;
				}else{
					logger.debug(":( Failed to read property .. Log4J configuration will be searched in classpath");
				}
			}catch(Exception ex){
				logger.debug(":(:( Error Loading LOG4J Configuration :> " + ex.getClass().getName() + " : "+ex.getMessage());
			}
		}
	}
	/**
	 * Use this function only to log with a specific logger that is not exposed as static function
	 * @param aLoggerName name of the defined logging class
	 * @return Logger
	 */
	public static Logger getLogger(String aLoggerName){
		Logger tmp_toRet = null;
//		configure();
		try{
			logger.debug(">>>>>>>>>>>>>>>>Trying to oad logger :>"+aLoggerName);
			tmp_toRet = Logger.getLogger(aLoggerName);
			logger.debug("Logger Initiated  - Checking Validity");
			if(tmp_toRet==null)
				logger.debug("Null Logger Constructed");
			else{
				tmp_toRet.fatal("Logging Started for Logger :>"+aLoggerName);
				Enumeration l_appenders = tmp_toRet.getAllAppenders();
				logger.debug("Enumerating Appenders");
				while(l_appenders.hasMoreElements()){
					Appender l_appender = (Appender) l_appenders.nextElement();
					logger.debug(l_appender.getName());
					if(RollingFileAppender.class.isInstance(l_appender)){
						logger.debug("Rolling Over file appender");
						logger.debug("FileName :>"+((RollingFileAppender)l_appender).getFile());
					}else if(ConsoleAppender.class.isInstance(l_appender)){
						logger.debug("Console appender");
						logger.debug("Target :>"+((ConsoleAppender)l_appender).getTarget());
					}
				}
			}
		}catch(Exception ex){
			logger.debug("Failed to Get Logger :> "+aLoggerName+ " : "+ ex.getMessage());
		}
		return tmp_toRet;
	}

	static Logger backendLogger =null;
	/**
	 * Retrieves an instance to backend Logger, instanciate it if not exist
	 * @return
	 */
	public static Logger BACKENDLOGGER(){
		if(backendLogger==null){
			backendLogger = getLogger("eportal-syslog");
		}
		return backendLogger; 
	}
	static Logger backendFatalLogger =null;
	/**
	 * Retrieves an instance to Backend fatal Logger, instanciate it if not exist
	 * @return
	 */
	public static Logger BACKENDFATALLOGGER(){
		if(backendFatalLogger==null){
			backendFatalLogger = getLogger("eportal-fatallog"); 
		}
			return	backendFatalLogger;
	}
	
	
	static Logger creditlogger = null;
	public static Logger CREDITLOGGER(){
		if(creditlogger == null){
			creditlogger = getLogger("eportal-credit-logger");
		}
		return creditlogger;
	}

	
//	static Logger businessLogger =null;
	/**
	 * Retrieves an instance to Business Logger, instanciate it if not exist
	 * @return
	 */
//	public static Logger BUSINESSLOGGER(){
//		if(businessLogger == null){
//			businessLogger = getLogger("eportal-businesslog");
//		}
//		return businessLogger;
//	}
//	static Logger businessFatalLogger =null;
	/**
	 * Retrieves an instance to Business Fatal Logger, instanciate it if not exist
	 * @return
	 */
//	public static Logger BUSINESSFATALLOGGER(){
//		if(businessFatalLogger==null){
//			businessFatalLogger = 	getLogger("eportal-fatallog");
//		}
//		return businessFatalLogger;
//	}
	static Logger uiLogger =null;
	/**
	 * Retrieves an instance to UI  Logger, instanciate it if not exist
	 * @return
	 */
	public static Logger UILOGGER(){
		if(uiLogger==null){
			uiLogger = getLogger("eportal-ui-logger");
		}
		return uiLogger;
	}
	static Logger uiFatalLogger =null;
	/**
	 * Retrieves an instance to UI Fatal Logger, instanciate it if not exist
	 * @return
	 */
	public static Logger UIFATALLOGGER(){
		if(uiFatalLogger==null){
			uiFatalLogger =	getLogger("eportal-fatallog");
		}
		return uiFatalLogger;
	}

	
	static Logger uinewLogger =null;
	/**
	 * Retrieves an instance to UI  Logger, instanciate it if not exist
	 * @return
	 */
	public static Logger UINEWLOGGER(){
		if(uiLogger==null){
			uinewLogger = getLogger("eportal-uilog");
		}
		return uinewLogger;
	}
	/**
	 * static Part added by Shady Esmat to enforce log references
	 * @author sea
	 *
	 * TODO To change the template for this generated type comment go to
	 * Window - Preferences - Java - Code Style - Code Templates
	 */
	static{
/*		configure();
		System.out.println("-----------------------------------------------------------------------------");
		System.out.println("                    Mobily E-Portal Basic Log configuration");
		System.out.println("                    Log Initialization Started: ");
		System.out.println("-----------------------------------------------------------------------------");
		System.out.println("********FATAL LOGGERS");
		BACKENDFATALLOGGER();
		BUSINESSFATALLOGGER();
		UIFATALLOGGER();
		System.out.println("********USER INTERFACE LOGGER");
		UILOGGER();
		System.out.println("********BUSINESS LOGGER");
		BUSINESSLOGGER();
		System.out.println("********BACKEND LOGGER");
		BACKENDLOGGER();
		System.out.println("-----------------------------------------------------------------------------");
*/	}
}
