/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.Date;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CustomerTypeVO {
    private String lineNumber ;
    private String accountNumber;
    
	private String requestorUserId;
    private int    customerType ;
    private String packageID;
    private String packageDescription;
    private Date   creationDate;
    private String returnCode ;
    private String packageCategory;
    private String customerCategory;
    private String creationTime;
    private String isMix;
    private String corporatePackage;
    private String isNewControl;
    private String isCorporateKA;
    private String customerBSLId;
    private String mNPNumber;
    private String iDType;
    private String iDNumber;
    private String customerId;
    private String billingId;
    private String serviceId;
    private String dealId;
    private String iSMSIM;
    private String cPEMACAddress;
    private boolean isIUC = false;
    
    public boolean isIUC() {
		return isIUC;
	}
	public void setIUC(boolean isIUC) {
		this.isIUC = isIUC;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
    
    public String getPackageCategory() {
		return packageCategory;
	}
	public void setPackageCategory(String packageCategory) {
		this.packageCategory = packageCategory;
	}
	public String getCustomerCategory() {
		return customerCategory;
	}
	public void setCustomerCategory(String customerCategory) {
		this.customerCategory = customerCategory;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getIsMix() {
		return isMix;
	}
	public void setIsMix(String isMix) {
		this.isMix = isMix;
	}
	public String getCorporatePackage() {
		return corporatePackage;
	}
	public void setCorporatePackage(String corporatePackage) {
		this.corporatePackage = corporatePackage;
	}
	public String getIsNewControl() {
		return isNewControl;
	}
	public void setIsNewControl(String isNewControl) {
		this.isNewControl = isNewControl;
	}
	public String getIsCorporateKA() {
		return isCorporateKA;
	}
	public void setIsCorporateKA(String isCorporateKA) {
		this.isCorporateKA = isCorporateKA;
	}
	public String getCustomerBSLId() {
		return customerBSLId;
	}
	public void setCustomerBSLId(String customerBSLId) {
		this.customerBSLId = customerBSLId;
	}
	public String getMNPNumber() {
		return mNPNumber;
	}
	public void setMNPNumber(String number) {
		mNPNumber = number;
	}
	public String getIDType() {
		return iDType;
	}
	public void setIDType(String type) {
		iDType = type;
	}
	public String getIDNumber() {
		return iDNumber;
	}
	public void setIDNumber(String number) {
		iDNumber = number;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	public String getServiceId() {
		return serviceId;
	}
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	public String getDealId() {
		return dealId;
	}
	public void setDealId(String dealId) {
		this.dealId = dealId;
	}
	public String getISMSIM() {
		return iSMSIM;
	}
	public void setISMSIM(String ismsim) {
		iSMSIM = ismsim;
	}
	public String getCPEMACAddress() {
		return cPEMACAddress;
	}
	public void setCPEMACAddress(String address) {
		cPEMACAddress = address;
	}
	
    

    /**
     * @return Returns the creationDate.
     */
    public Date getCreationDate() {
        return creationDate;
    }
    /**
     * @param creationDate The creationDate to set.
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    /**
     * @return Returns the customerType.
     */
    public int getCustomerType() {
        return customerType;
    }
    /**
     * @param customerType The customerType to set.
     */
    public void setCustomerType(int customerType) {
        this.customerType = customerType;
    }
    /**
     * @return Returns the lineNumber.
     */
    public String getLineNumber() {
        return lineNumber;
    }
    /**
     * @param lineNumber The lineNumber to set.
     */
    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }
    /**
     * @return Returns the packageDescription.
     */
    public String getPackageDescription() {
        return packageDescription;
    }
    /**
     * @param packageDescription The packageDescription to set.
     */
    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }
    /**
     * @return Returns the packageID.
     */
    public String getPackageID() {
        return packageID;
    }
    /**
     * @param packageID The packageID to set.
     */
    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }
    /**
     * @return Returns the requestorUserId.
     */
    public String getRequestorUserId() {
        return requestorUserId;
    }
    /**
     * @param requestorUserId The requestorUserId to set.
     */
    public void setRequestorUserId(String requestorUserId) {
        this.requestorUserId = requestorUserId;
    }
    /**
     * @return Returns the returnCode.
     */
    public String getReturnCode() {
        return returnCode;
    }
    /**
     * @param returnCode The returnCode to set.
     */
    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }
    
}
