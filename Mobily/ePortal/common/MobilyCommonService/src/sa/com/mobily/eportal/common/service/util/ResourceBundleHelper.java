package sa.com.mobily.eportal.common.service.util;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResourceBundleHelper {
	
	private String _bundleFile = "loggerconfig";;
	private static PropertyResourceBundle props = null;
	private static Logger logger = Logger.getLogger("javalogging");

	public ResourceBundleHelper() {
		props = (PropertyResourceBundle) ResourceBundle.getBundle(_bundleFile);
	}

	public ResourceBundleHelper(String _bundleFile) {
		this._bundleFile = _bundleFile;
		props = (PropertyResourceBundle) ResourceBundle.getBundle(_bundleFile);
	}

	public String getKeyValue(String _key) {

		if (_bundleFile == null) {
			logger.log(Level.CONFIG, "[" + this._bundleFile + "] NULL Configuration File !!!!!!");
			return null;
		}
		
		if(_key==null){
			logger.log(Level.CONFIG, "[" + _key + "] NULL Key !!!!!!");
			return null;			
		}
	
		String _value = props.getString(_key);
//		logger.log(Level.CONFIG, "[" + _key + "] = ["+_value+"]");

		return _value;
	}


}
