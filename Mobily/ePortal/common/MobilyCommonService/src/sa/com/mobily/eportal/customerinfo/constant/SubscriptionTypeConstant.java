package sa.com.mobily.eportal.customerinfo.constant;

import java.util.ArrayList;
import java.util.List;

public abstract class SubscriptionTypeConstant
{
	// BackEnd constant
	public static final String GSM = "GSM";

	public static final String Connect = "Connect"; // 3G

	public static final String WiMAX = "WiMAX";

	public static final String FTTH = "FTTH";

	public static final String LTE = "LTE";

	public static final String IPTV = "IPTV";
	
	//SR22516 - ePortal Fixed Voice
	public static final String FV = "Fixed voice";

	// Application constants
	public static final int SUBS_TYPE_GSM = 1;

	public static final int SUBS_TYPE_CONNECT = 2;

	public static final int SUBS_TYPE_WiMAX = 3;

	// TODO rename the constant to Elife
	public static final int SUBS_TYPE_FTTH = 4;
	
	//SR22516 - ePortal Fixed Voice
	public static final int SUBS_TYPE_FV = 5;
	
	public static final int getSubscriptionType(String subscriptionTypeText)
	{
		int to_return = -1;

		if (subscriptionTypeText.equalsIgnoreCase(GSM))
		{
			to_return = SUBS_TYPE_GSM;
		}
		else if (subscriptionTypeText.equalsIgnoreCase(Connect) || subscriptionTypeText.equalsIgnoreCase(LTE))
		{
			to_return = SUBS_TYPE_CONNECT;
		}
		else if (subscriptionTypeText.equalsIgnoreCase(WiMAX))
		{
			to_return = SUBS_TYPE_WiMAX;
		}
		else if (subscriptionTypeText.equalsIgnoreCase(FTTH) || subscriptionTypeText.equalsIgnoreCase(IPTV))
		{
			to_return = SUBS_TYPE_FTTH;
		}
		else if (subscriptionTypeText.equalsIgnoreCase(FV) )//SR22516 - ePortal Fixed Voice
		{
			to_return = SUBS_TYPE_FV;
		}
		return to_return;
	}

	public static final List<String> getSubscriptionTypes()
	{
		List<String> subList = new ArrayList<String>();
		subList.add(GSM);
		subList.add(Connect);
		subList.add(WiMAX);
		subList.add(FTTH);
		//SR22516 - ePortal Fixed Voice
		subList.add(FV);
		return subList;
	}

	public static final String getSubscriptionName(int subscriptionType)
	{
		String to_return = "";
		switch (subscriptionType)
		{
		case SUBS_TYPE_GSM:
			to_return = GSM;
			break;
		case SUBS_TYPE_CONNECT:
			to_return = Connect;
			break;
		case SUBS_TYPE_WiMAX:
			to_return = WiMAX;
			break;
		case SUBS_TYPE_FTTH:
			to_return = FTTH;
			break;
		case SUBS_TYPE_FV://SR22516 - ePortal Fixed Voice
			to_return = FV;
			break;	
		default:
			break;
		}
		return to_return;
	}
}