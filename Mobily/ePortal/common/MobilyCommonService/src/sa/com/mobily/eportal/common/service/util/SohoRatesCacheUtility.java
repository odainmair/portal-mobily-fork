package sa.com.mobily.eportal.common.service.util;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.OracleDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerPackageDAO;
import sa.com.mobily.eportal.common.service.dao.imp.OracleSohoAddOnTableDAO;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.AddOnVO;

/**
 * @author m.abdelmaged
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SohoRatesCacheUtility {

	private static final Logger log = LoggerInterface.log;

	private static ArrayList<AddOnVO> addOnRates = null;
	private static SohoRatesCacheUtility sohoRatesCacheUtility = null;

	/**
	 * private constructor 
	 */
	private SohoRatesCacheUtility() {

	}

	/**
	 * Creates the object if it is not created before.
	 * @return sohoRatesCacheUtility
	 */
	public static SohoRatesCacheUtility getInstance() {
		if (sohoRatesCacheUtility == null) {
			sohoRatesCacheUtility = new SohoRatesCacheUtility();
		}
		return sohoRatesCacheUtility;
	}

	/**
	 * Loads the Soho Rates from the database
	 * @return packageDataList
	 */
	public ArrayList<AddOnVO> getSohoRatesItems() {

		if (addOnRates == null) {
			addOnRates = loadSohoRatesCashedItem();
			log.debug("SohoRatesCacheUtility > Loading Soho Rates for the first time.");
		}
		return addOnRates;
	}

	/**
	 * Load Soho Add-on Rates from DB Into Memory
	 * @return
	 * @throws MobilyServicesException
	 */
	private static ArrayList<AddOnVO> loadSohoRatesCashedItem() {

		OracleDAOFactory daoFactory = (OracleDAOFactory) DAOFactory
				.getDAOFactory(DAOFactory.ORACLE);
		OracleSohoAddOnTableDAO SohoAddOnDao = daoFactory.getSohoAddOnTableDAO();

		return SohoAddOnDao.getSohoAddOnRates();
	}

	public void clearCache() {
		log.info("PackageCacheutility > clearCache > Called");
		addOnRates = null;
		
	}

}