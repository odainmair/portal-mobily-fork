package sa.com.mobily.eportal.common.service.dao;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class GlobyusGroupIdSeqDAO extends AbstractDBDAO<Long>{
	
	public static final String GET_NEXT_VAL = "select GLOYBAS_SEQ.nextval groupId from dual";
	
	private static GlobyusGroupIdSeqDAO instance = new GlobyusGroupIdSeqDAO();
	
	protected GlobyusGroupIdSeqDAO() {
		super(DataSources.DEFAULT);
	}
	
	/***
	 * Method for obtaining the singleton instance
	 * 
	 * @return
	 */
	public static GlobyusGroupIdSeqDAO getInstance() {
		return instance;
	}
	
	public Long getNextValue(){
		List<Long> values = query(GET_NEXT_VAL);
		if(values != null && values.size() > 0)
			return values.get(0);
		return null;
	}
	
	@Override
	protected Long mapDTO(ResultSet rs) throws SQLException {
		return rs.getLong("groupId");
	}

}
