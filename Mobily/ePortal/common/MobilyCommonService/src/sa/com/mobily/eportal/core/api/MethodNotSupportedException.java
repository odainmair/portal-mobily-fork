//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This exception is thrown by the REST APIs to provide the error code
 * defined by METHOD_NOT_SUPPORTED.
 *
 * @see ErrorCodes
 */
public class MethodNotSupportedException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public MethodNotSupportedException(String message) {
        super(message, ErrorCodes.METHOD_NOT_SUPPORTED);
    }
    
    public MethodNotSupportedException(String message, int errorCode) {
        super(message, errorCode);
    }
}
