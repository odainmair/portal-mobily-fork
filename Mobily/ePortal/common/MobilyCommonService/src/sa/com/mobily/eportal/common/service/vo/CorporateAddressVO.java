/*
 * Created on Nov 18, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CorporateAddressVO extends BaseVO {

	private String companyAddress;

	private String companyPoBox;

	private String companyCity;

	private String companyPostalCode;

	private String companyAddressType;

	private String companyCountry;

	/**
	 *  
	 */
	public CorporateAddressVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return Returns the companyAddress.
	 */
	public String getCompanyAddress() {
		return companyAddress;
	}

	/**
	 * @param companyAddress
	 *            The companyAddress to set.
	 */
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	/**
	 * @return Returns the companyAddressType.
	 */
	public String getCompanyAddressType() {
		return companyAddressType;
	}

	/**
	 * @param companyAddressType
	 *            The companyAddressType to set.
	 */
	public void setCompanyAddressType(String companyAddressType) {
		this.companyAddressType = companyAddressType;
	}

	/**
	 * @return Returns the companyCity.
	 */
	public String getCompanyCity() {
		return companyCity;
	}

	/**
	 * @param companyCity
	 *            The companyCity to set.
	 */
	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	/**
	 * @return Returns the companyCountry.
	 */
	public String getCompanyCountry() {
		return companyCountry;
	}

	/**
	 * @param companyCountry
	 *            The companyCountry to set.
	 */
	public void setCompanyCountry(String companyCountry) {
		this.companyCountry = companyCountry;
	}

	/**
	 * @return Returns the companyPoBox.
	 */
	public String getCompanyPoBox() {
		return companyPoBox;
	}

	/**
	 * @param companyPoBox
	 *            The companyPoBox to set.
	 */
	public void setCompanyPoBox(String companyPoBox) {
		this.companyPoBox = companyPoBox;
	}

	/**
	 * @return Returns the companyPostalCode.
	 */
	public String getCompanyPostalCode() {
		return companyPostalCode;
	}

	/**
	 * @param companyPostalCode
	 *            The companyPostalCode to set.
	 */
	public void setCompanyPostalCode(String companyPostalCode) {
		this.companyPostalCode = companyPostalCode;
	}
}
