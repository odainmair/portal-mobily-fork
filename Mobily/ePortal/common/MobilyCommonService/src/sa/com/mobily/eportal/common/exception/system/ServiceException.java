package sa.com.mobily.eportal.common.exception.system;


public class ServiceException extends MobilySystemException {

	private static final long serialVersionUID = 3508961876555803251L;

	public ServiceException() {
	}

	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}