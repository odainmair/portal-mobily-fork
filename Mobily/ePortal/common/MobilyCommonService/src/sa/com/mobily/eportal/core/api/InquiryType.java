//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.api;

/**
 * This is an enumeration for the list of possible values of
 * the Inquiry Type that can be used by the Value Objects of the module.
 */
public enum InquiryType {
    PlansAndTariffs, ProductsAndServices, OtherInquiry, MobilyBusiness, CoverageNetwork, MobileSettings, Suggestion, Complaint, 
}
