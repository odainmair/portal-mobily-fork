/*
 * Created on 19/11/2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.billing.constants;



/**
 * @author r.agarwal.mit
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public interface BillingConstantsIfc {
	public static final String BILL_PDF_URL = "pdf_server_url";
	public static final String UNIQUE_NAME_KEY = "portlet_unique_name";
	public static final String UNBILLED_CALL_TYPE = "unbilled";
	public static final String BILLED_CALL_TYPE = "billed";
	
	public static boolean DEBUG_MODE = false;
	
	public static final String SESSION_DOCUMENTS_IDS = "documentIds";
	
	public String PORTLET_DATA_CONTENT_ATTR = "content";
	public static String USER_ANONYMOUS ="Anonymous";
	
	public String VIEW_BILL_PARAM = "bill";
	public String VIEW_BALANCE_PARAM = "balance";
	public String VIEW_ADVANCED_BILL_PARAM = "advancedBill";
	public String VIEW_BILL_BRIEF_PARAM = "briefBill";
	public String VIEW_BALANCE_BRIEF_PARAM = "briefBalance";
	public String VIEW_PAYMENT_HISTORY_PARAM = "paymentHistory";
	public String VIEW_CORPORATE_MY_BILL_PARAM = "CorporateMyBill";
	public String VIEW_KAUST_MY_BILL_PARAM = "KaustMyBill";
	public String VIEW_CORPORATE_BRIEF_BAL_PARAM = "CorporateBriefBalance";
	public String VIEW_CORPORATE_BAL_PARAM = "CorporateBalance";
	public String EXTRACT_INVOICES_PARAM = "ExtractInvoice";
	public String VIEW_UNBILLED_PARAM = "Unbilled";
	public String VIEW_BILL_HISTORY_PARAM = "BillHistory";
	public String VIEW_BILING_PROFILE_PARAM = "BillingProfile";
	public String VIEW_PHONE_USAGE_PARAM = "PhoneUsage";
	
	public static final String ENABLE_PDF = "enable_pdf";
	public static final String REQUEST_ENABLE_PDF = "enablePDF";
	
	public static final String PAGE_ACTION_CUSTOMIZE_NUMBER_VIEW = "viewCustomizeNumber";
	public static final String PAGE_ACTION_CUSTOMIZE_NUMBER_SAVE = "saveCustomizeNumber";
	public static final String PAGE_ACTION_CALLS_ITEMISED = "viewCallsItemised";
	public static final String PAGE_ACTION_UNBILLED_CALLS = "viewUnbilledCalls";
	
	public static final String WEB_SERVICE_BILL_PDF_USER_NAME = "WEB_SERVICE_BILL_PDF_USER_NAME";
	public static final String WEB_SERVICE_BILL_PDF_PASSWORD = "WEB_SERVICE_BILL_PDF_PASSWORD";
	public static final String WEB_SERVICE_BILL_PDF_DATA_SOURCE = "WEB_SERVICE_BILL_PDF_DATA_SOURCE";
	public static final String WEB_SERVICE_BILL_PDF_LIB_NAME = "WEB_SERVICE_BILL_PDF_LIB_NAME";
	public static final String WEB_SERVICE_URL_THROUGH_MBI = "WEB_SERVICE_URL_THROUGH_MBI";
	
	public static final int CUSTOMIZE_NUMBER_CONFIRM = 1;
	public static final int CUSTOMIZE_NUMBER_ERROR = 2;
	
	public static final String SESSION_BILLS_HASHTABLE = "sessionBillsHashtable";
	
	public static String EI_ErrorCode_151 = "151";
	public static String EI_ErrorCode_152= "152";
	public static String EI_ErrorCode_153 = "153";
	public static String EI_ErrorCode_155 = "155";
	public static String EI_ErrorCode_156 = "156";
	public static String EI_ErrorCode_157 = "157";
	public static String EI_ErrorCode_158 = "158";
	public static String EI_ErrorCode_159 = "159";
	public static String EI_ErrorCode_1500 = "1500";
	public static String EI_ErrorCode_1502 = "1502";
	public static String EI_ErrorCode_1503 = "1503";
	public static String EI_ErrorCode_1504 = "1504";
	public static String EI_ErrorCode_1505 = "1505";
	public static String EI_ErrorCode_1506 = "1506";
	public static String EI_ErrorCode_1507 = "1507";
	public static String EI_ErrorCode_1508 = "1508";
	public static String EI_ErrorCode_1513 = "1513";
	public static String EI_ErrorCode_1514 = "1514";
	public static String EI_ErrorCode_1515 = "1515";
	public static String EI_ErrorCode_1516 = "1516";
	public static String EI_ErrorCode_1518 = "1518";

	public static String EI_ErrorCode_151_MsgCode = "ErrorCode_151_Message";
	public static String EI_ErrorCode_152_MsgCode = "ErrorCode_152_Message";
	public static String EI_ErrorCode_153_MsgCode = "ErrorCode_153_Message";
	public static String EI_ErrorCode_155_MsgCode = "ErrorCode_155_Message";
	public static String EI_ErrorCode_156_MsgCode = "ErrorCode_156_Message";
	public static String EI_ErrorCode_157_MsgCode = "ErrorCode_157_Message";
	public static String EI_ErrorCode_158_MsgCode = "ErrorCode_158_Message";
	public static String EI_ErrorCode_159_MsgCode = "ErrorCode_159_Message";
	public static String EI_ErrorCode_1500_MsgCode = "ErrorCode_1500_Message";
	public static String EI_ErrorCode_1502_MsgCode = "ErrorCode_1502_Message";
	public static String EI_ErrorCode_1503_MsgCode = "ErrorCode_1503_Message";
	public static String EI_ErrorCode_1504_MsgCode = "ErrorCode_1504_Message";
	public static String EI_ErrorCode_1505_MsgCode = "ErrorCode_1505_Message";
	public static String EI_ErrorCode_1506_MsgCode = "ErrorCode_1506_Message";
	public static String EI_ErrorCode_1507_MsgCode = "ErrorCode_1507_Message";
	public static String EI_ErrorCode_1508_MsgCode = "ErrorCode_1508_Message";
	public static String EI_ErrorCode_1513_MsgCode = "ErrorCode_1513_Message";
	public static String EI_ErrorCode_1514_MsgCode = "ErrorCode_1514_Message";
	public static String EI_ErrorCode_1515_MsgCode = "ErrorCode_1515_Message";
	public static String EI_ErrorCode_1516_MsgCode = "ErrorCode_1516_Message";
	public static String EI_ErrorCode_1518_MsgCode = "ErrorCode_1518_Message";

	//	Bulk balance constants
	public static final int PAGING_NO_OF_ITEMS = 15;
    
    public static final String SORT_TYPE_ASCENDING  = "true";
    public static final String SORT_TYPE_DESCENDING = "false";
    
    public static final String SORT_BY_LINE             = "line";
    public static final String SORT_BY_BALANCE          = "balance";
    public static final String SORT_BY_UNBILLED_AMOUNT  = "unbilledAmount";
    public static final String SORT_BY_BILLED_AMOUNT    = "billedAmount";
    public static final String SORT_BY_TOTAL_AMOUNT_DUE = "totalAmountDue";
//  Balance Inquiry
	public static final String BALANCE_INQ_REQUEST_QUEUENAME = "mq.balance.request.queue";
	public static final String BALANCE_INQ_WIMAX_REQUEST_QUEUENAME = "mq.balance.wimax.request.queue";
	public static final String BALANCE_INQ_REPLY_QUEUENAME = "mq.balance.reply.queue";
	
	//Free balance Inquiry
	public static final String FREE_BALANCE_INQ_REQUEST_QUEUENAME = "mq.free.balance.request.queue";
	public static final String FREE_BALANCE_INQ_REPLY_QUEUENAME = "mq.free.balance.reply.queue";
	
//	Queue manager
	public static final String QUEUE_MANAGER_NAME_IN_CLUSTER = "mq.queue.manager.name";
	

	// Email Invoice Payment Queue Names
	public static final String EMAIL_INVOICE_INQ_REQUEST_QUEUENAME = "mq.emailInvoice.inq.request.queue";
	public static final String EMAIL_INVOICE_INQ_REPLY_QUEUENAME = "mq.emailInvoice.inq.reply.queue";
	
	public static final String EMAIL_INVOICE_SERVICES_REQUEST_QUEUENAME = "mq.emailInvoice.services.request.queue";
	public static final String EMAIL_INVOICE_SERVICES_REPLY_QUEUENAME = "mq.emailInvoice.services.reply.queue";

	//Queue manager staging
	public static final String QUEUE_MANAGER_NAME_STAGING = "mq.queue.manager.name.staging";
	public static final String ALARM_MQ_BALANCE_INQ ="1003";
	public static final String ALARM_MQ_FREE_BALANCE_INQ ="1007";
	
	public static final String ALARM_DB_BILLING ="1010";
	public static final String ALARM_DB_BSL ="1009";
	public static final String ALARM_DB_PAYMENT ="1008";
	public static final String ALARM_DB_SIBEL ="1011";
	public static final String ALARM_MQ_Email_Invoice = "1015";
	
//	Corporate and normal lines types
 	public static final int LINE_TYPE_CONSUMER = 11;
	public static final int LINE_TYPE_CORPORATE_LINE = 12;
	public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON = 13;
	public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON_CHANGE_IDENTITY = 14;
	
	public static final String MQ_GSM_MESSAGE_FORMAT = "BALANCE_INQ";
	public static final String MQ_WIMAX_MESSAGE_FORMAT = "BILL_INQ";
	public static final String TAG_BILL_ACCOUNTNUMBER = "AccountNumber";
	public static final String WIMAX_UNALOCATED_AMOUNT = "UnAllocated";
	
	public static final int FOOTPRINT_EVENT_BALANCE                 = 41;
	public static final String RETURN_CODE_VALUE = "0000";
	public static final String RETURN_CODE_VALUE_ZERO = "0";
	public static final byte  CUSTOMER_TYPE_WIMAX = 2;
	public static final int  EMPTY_BALANCE_WIMAX = -1;
	
	public static final int VIEW_BILL_DETAILS = 1;				// 0000000000000001
	public static final int MANAGE_SERVICES = 2;				// 0000000000000010
	public static final int ADD_OTHER_NUM = 4;				// 0000000000000100
	
    public static final int CORPORATE_PHONE_USAGE_CATEGORY_LOCAL         = 0;
    public static final int CORPORATE_PHONE_USAGE_CATEGORY_INTERNATIONAL = 1;
    public static final int CORPORATE_PHONE_USAGE_CATEGORY_ROAMING       = 2;
    
    // Corporate Balance Inquiry
	public static final String CORPORATE_BALANCE_INQ_REQUEST_QUEUENAME = "mq.corp.balance.request.queue";
	public static final String CORPORATE_BALANCE_INQ_REPLY_QUEUENAME = "mq.corp.balance.reply.queue";
	
	public static final String SERVICE_TYPE_ALL = "ALL";
	public static final String SERVICE_TYPE_GSM = "GSM";
	public static final String SERVICE_TYPE_DIA = "DIA";
	public static final String SERVICE_TYPE_Ethernet = "Ethernet";
	public static final String SERVICE_TYPE_IPVPN = "IPVPN";
	public static final String SERVICE_TYPE_Wimax = "Wimax";
	public static final String SERVICE_TYPE_IPTRANSIT = "IPTRANSIT";
	public static final String SERVICE_TYPE_COLOCATION = "COLOCATION";
	public static final String SERVICE_TYPE_BULKMSG = "BULKMSG";
	public static final String SERVICE_KAUST = "KAUST";
	public static final String SERVICE_TYPE_CLOUD = "Cloud";
	public static final String SERVICE_EXTRACT_INVOICES = "EXTRACT_INVOICES";
	
	public static final String SERVICE_TYPE = "corp.service.type.";
	
	
	//IPhone App
		public static final String GET_BALANCE_NEWS = "GET_BALANCE_NEWS";
		public static final String GET_BALANCE = "GET_BALANCE";
		public static final String GET_BILL_PERSONAL_INFO = "GET_BILL_PERSONAL_INFO";
		public static final String GET_BILL_INFO = "GET_BILL_INFO";
		public static final String GET_BILL_SUMMARY = "GET_BILL_SUMMARY";
		public static final String GET_EMAIL_INVOICE_INQUIRY = "Email_Invoice_Inquiry";
		public static final String GET_EMAIL_INVOICE = "Email_Invoice";
		public static final String GET_BILL_INFO_SUMMARY = "GET_BILL_INFO_SUMMARY";
		public static final String PAYMENT_NOTIFICATION = "PAYMENT_NOTIFICATION";
		public static final String GET_BILL_HISTORY = "BILL_HISTORY_INFORMATION";
		
	    public static final String LANG_EN = "E";
	    public static final String LANG_AR = "A";

		public static final int CUSTOMER_PREPAID = 1;
		public static final int CUSTOMER_POSTPAID = 2;

		public static final String EXPIRY_DATEFORMAT = "dd MMM yy";
		
		public static final String EMAIL_INVOICE_POBOX = "POBOX";
		public static final String EMAIL_INVOICE_EMAIL = "Email";
		public static final String EMAIL_INVOICE_NONE = "None";
		
		public static final String BILL_NO_PREPAID_ERROR_CODE = "5001";
		public static final String BILL_NOT_FOUND_ERROR_CODE = "5002";
		public static final String BILL_NO_RECORDS_FOUND_ERROR_CODE = "5003";
		
		public static final int FOOTPRINT_EVENT_BILLING  = 1;
		
		public static final String FOOTPRINT_NO_PREPAID = "Prepaid customers do not have bills.";
		public static final String FOOTPRINT_NO_BILLS = "Currently, there is no bill under your account in the system.  Please check back after the next billing cycle to view your bills.";
		
	    public static final String CONTENT_NEWS = "1";
	    public static final String CONTENT_PROMOTION = "2";
	    
	    public static final String	VIEW_BALANCE_GPRS_UNLIMITED = "Unlimited";
	    
	    public static final String PAGE_ACTION_VIEW_BILLS = "view";
	    public static final String PAGE_ACTION_VIEW_LINES = "viewLines";
	    
	    // Extract invoice bill inquiry
		public static final String EXTRACT_INVOICE_BILL_INQ_REQUEST_QUEUENAME = "mq.extract.invoice.request.queue";
		public static final String EXTRACT_INVOICE_BILL_INQ_REPLY_QUEUENAME = "mq.extract.invoice.reply.queue";
	    public static final String SERVICE_TYPE_VOICE = "VOICE";
	    public static final String SERVICE_TYPE_CONNECT = "CONNECT";
	    public static final String SERVICE_TYPE_WIMAX = "WIMAX";

	    public static final String SUPPLEMENTARY_SERVICE_GPRS ="GPRS";
	    public static final String KEY_GPRS ="mega.gprs.";
	    public static final String GPRS_NOT_SUBSCRIBED ="N/A";
	    
	    // Payment History
	    public static final String PAYMENT_HISTORY_REQUEST_QUEUENAME = "mq.payment.history.request.queue";
		public static final String PAYMENT_HISTORY_REPLY_QUEUENAME = "mq.payment.history.reply.queue";
		public static final String VALUE_ZERO = "0";
		
		//Xenos New bill download
		public static final String XENOS_ALIAS = "pdfdsrproject/processFlows/retrieve/ISRARetrieve.xProcessFlow";
		public static final String KEY_REQUEST_TYPE = "RequestType";
		public static final String KEY_MSISDN = "MSISDN";
		public static final String KEY_BILL_START_DATE = "BillStartDate";
		public static final String KEY_BILL_END_DATE = "BillEndDate";
		public static final String KEY_BILL_NUMBER = "BillNumber";
		public static final String REQUEST_TYPE_LIST = "LIST";
		public static final String REQUEST_TYPE_RETRIEVE = "RETRIEVE";
		public static final String XENOS_ALIAS_FQN = "IsAdapter:MobilyISRAAlias";
		public static final String XENOS_ADMIN_USER = "SysAdmin";
		public static final String XENOS_ADMIN_PWD = "dmsAdmin";
		public static final String XENOS_USER = "JobRunner";
		public static final String XENOS_PWD = "JobRunner";
		
		public static final String XENOS_DOC_HISTORY = "DocHistory";
		public static final String XENOS_BILL_DATE = "BillDate";
		public static final String XENOS_BILL_NUMBER = "BillNumber";
		public static final String XENOS_MSISDN = "MSISDN";
		public static final String XENOS_F_DOCNUMBER = "F_DOCNUMBER";
		public static final String XENOS_RETRIEVED_DOCUMENT = "RetrievedDocument";
		
		public static final String TAB_SELECTED_NATIONAL = "national";
		public static final String TAB_SELECTED_ROAMING = "roaming";
		public static final String SERVICE_TYPE_CALLS = "calls";
		public static final String SERVICE_TYPE_TELEPHONY = "telephony";
		public static final String SERVICE_TYPE_SMS = "sms";
		public static final String SERVICE_TYPE_GPRS = "gprs";
		public static final String SERVICE_TYPE_DATA = "data";
		public static final String SERVICE_TYPE_MMS= "mms";
		public static final String SERVICE_TYPE_ROAMING = "roaming";
		
		public static final String SERVICE_ID_DEFAULT = "0";	
		public static final String SERVICE_ID_IBILL_FOR_RAQI ="1";
		public static final String SERVICE_ID_IBILL_FOR_IPAD ="2";
		
		public static final String SERVICE_NAME_UNKNOW = "Unknown";
		public static final String SERVICE_NAME_DEFAULT = "Default";	
		public static final String SERVICE_NAME_RAQI_FOR_IPAD ="Raqi App";
		public static final String SERVICE_NAME_IBILL_FOR_IPAD ="iBill App";
		
		public static final String FUNC_ID_FTTH_RENEW_INQ = "FTTHAR_Renewal_Payment_Amount_Inq";
		public static final String DUMMY_VAL_111111="111111";
		
		public static final String CHANNEL_ID_VALUE = "ePortal";
		
		public static final String FTTH_RENEW_PAYMENT_INQ_REQUEST_QUEUENAME = "mq.ftth.renewal.payment.request.queue"; //MOBILY.FUNC.FTTH.RNW.AMOUNT.INQ.REQUEST";
		public static final String FTTH_RENEW_PAYMENT_INQ_REPLY_QUEUENAME = "mq.ftth.renewal.payment.reply.queue"; //EPORTAL.FUNC.FTTH.RNW.AMOUNT.INQ.REPLY"
		
		// Get Last Bills
		public static final String REQUEST_QUEUE_LAST_BILLS = "last.bills.request.queue";
		public static final String REPLY_QUEUE_LAST_BILLS = "last.bills.reply.queue";
		public static final String FUN_GET_LAST_BILLS_INQ = "LastBillsInq";
		public static final String INVALID_REQUEST ="20131";
		public static final String INVALID_REPLY ="20132";
		
		public static final String PAYMENT_REASON_18 = "18";
		public static final String PAYMENT_REASON_20 = "20";
		public static final String PAYMENT_REASON_28 = "28";
		public static final String PAYMENT_REASON_562 = "562";
		public static final String PAYMENT_REASON_332 = "332";
		public static final String PAYMENT_REASON_169 = "169";
		
		public static final String PAYMENT_TYPE_01 = "01";
		public static final String PAYMENT_TYPE_02 = "02";
		public static final String PAYMENT_TYPE_03 = "03";
		public static final String PAYMENT_TYPE_63 = "63";
		public static final String PAYMENT_TYPE_64 = "64";
		public static final String PAYMENT_TYPE_65 = "65";
		public static final String PAYMENT_TYPE_66 = "66";
		
		public static final String PAYMENT_MODE_14 = "14";
		public static final String PAYMENT_MODE_03 = "03";
}
