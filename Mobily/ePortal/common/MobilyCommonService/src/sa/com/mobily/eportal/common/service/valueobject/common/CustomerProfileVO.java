/*
 * Created on Mar 19, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.valueobject.common;
import sa.com.mobily.eportal.common.service.vo.BaseVO;
import sa.com.mobily.eportal.common.service.vo.ContactVO;

/**
 * 
 * @author n.gundluru.mit
 *
 */
public class CustomerProfileVO extends BaseVO {
	private String    mSISDN;
	private String    userName;
	private String    language;
	private ContactVO contactVO;

	public CustomerProfileVO(){
		contactVO=new ContactVO();
		
	}
	/**
	 * @return Returns the contactVO.
	 */
	public ContactVO getContactVO() {
		return contactVO;
	}
	/**
	 * @param contactVO The contactVO to set.
	 */
	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}
	/**
	 * @return Returns the language.
	 */
	public String getLanguage() {
		return language;
	}
	/**
	 * @param language The language to set.
	 */
	public void setLanguage(String language) {
		this.language = language;
	}
	/**
	 * @return Returns the mSISDN.
	 */
	public String getMSISDN() {
		return mSISDN;
	}
	/**
	 * @param msisdn The mSISDN to set.
	 */
	public void setMSISDN(String msisdn) {
		mSISDN = msisdn;
	}
	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
