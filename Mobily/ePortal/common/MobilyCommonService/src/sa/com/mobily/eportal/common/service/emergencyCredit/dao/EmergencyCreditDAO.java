package sa.com.mobily.eportal.common.service.emergencyCredit.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.service.emergencyCredit.vo.EmergencyCreditVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;

public class EmergencyCreditDAO extends AbstractDBDAO<EmergencyCreditVO>
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext("MobilyCommonService/commonServices");
	private static String clazz = EmergencyCreditDAO.class.getName();
	
	private static EmergencyCreditDAO instance = new EmergencyCreditDAO();
	
	public static final String GET_EMERGENCY_CREDIT_DETAILS = "SELECT ECID, DISPLAY_TEXT_EN , DISPLAY_TEXT_AR, EC_LEVEL, PARENT_ID, SUBSCRIPTION_CONFIG, STATUS, PRIORITY FROM EMERGENCY_CREDIT_SERVICES";
	
	protected EmergencyCreditDAO() {
		super(DataSources.DEFAULT);
	}
	
	public static EmergencyCreditDAO getInstance(){
		return instance;
	}

	public ArrayList<EmergencyCreditVO> getEmergencyCreditDetails() {
		executionContext.log(Level.INFO, "getEmergencyCreditDetails called: ", clazz, "getEmergencyCreditDetails");
		return (ArrayList<EmergencyCreditVO>) query(GET_EMERGENCY_CREDIT_DETAILS);
	}
	
	@Override
	protected EmergencyCreditVO mapDTO(ResultSet rs) throws SQLException
	{
		EmergencyCreditVO emCreditVO = new EmergencyCreditVO();
		emCreditVO.setEcID(rs.getString("ECID"));
		emCreditVO.setDisplayTextEn(rs.getString("DISPLAY_TEXT_EN"));
		emCreditVO.setDisplayTextAr(rs.getString("DISPLAY_TEXT_AR"));
		emCreditVO.setEcLevel(rs.getString("EC_LEVEL"));
		emCreditVO.setParentID(rs.getString("PARENT_ID"));
		emCreditVO.setSubscriptionConfig(rs.getString("SUBSCRIPTION_CONFIG"));
		emCreditVO.setStatus(rs.getString("STATUS"));
		emCreditVO.setPriority(rs.getString("PRIORITY"));
		return emCreditVO;
	}

}
