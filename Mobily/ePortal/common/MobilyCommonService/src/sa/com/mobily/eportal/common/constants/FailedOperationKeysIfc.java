package sa.com.mobily.eportal.common.constants;

public interface FailedOperationKeysIfc
{
	public static final String REGISTER_ASYNC_OPERATION_SEND_EMAIL_ID = "1";
	public static final String REGISTER_ASYNC_OPERATION_REG_USER_GLOBYS_ID = "2";
	public static final String REGISTER_ASYNC_OPERATION_UPDATE_POID_LIST_ID = "3";
	public static final String REGISTER_OPERATION_DELETE_USER_LDAP = "4";
	
	public static final String FORGOT_ACCT_SEND_EMAIL = "10";
}
