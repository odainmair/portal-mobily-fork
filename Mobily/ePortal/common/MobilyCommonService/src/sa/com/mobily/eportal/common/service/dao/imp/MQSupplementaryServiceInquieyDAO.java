/*
 * Created on Feb 19, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import com.mobily.exception.mq.MQSendException;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.SupplementaryServiceInquieyDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
 
/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQSupplementaryServiceInquieyDAO implements SupplementaryServiceInquieyDAO{
 
    private static final Logger log = LoggerInterface.log;
    
	public String getSupplementaryInquiry(String xmlRequestMessage) throws MobilyCommonException {
		String replyMessage = "";
		log.debug("MQSupplementaryServiceInquieyDAO > getSupplementaryInquiry > called");
		try {
			/*
			 * Get QueuName
			 */
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.SUPPLEMENTARY_SERVICE_INQUIRY_REQUEST_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getSupplementaryInquiry >Request Queue Name  ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.SUPPLEMENTARY_SERVICE_INQUIRY_REPLY_QUEUENAME );
			log.debug("MQSupplementaryServiceInquieyDAO > getSupplementaryInquiry >Reply Queue Name ["+ strReplyQueueName + "]");
			
				replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, 
				        														   strQueueName,
																				   strReplyQueueName);
			
			} catch (RuntimeException e) {
//			       AlarmService alarmService = new AlarmService();
//			       alarmService.RaiseAlarm(ConstantIfc.ALARM_MQ_CUSTOMER_TYPE);

				log.fatal("MQSupplementaryServiceInquieyDAO > getSupplementaryInquiry > MQSend Exception "+ e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			} catch (Exception e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getSupplementaryInquiry >Exception " + e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			}
		return replyMessage;
	}

	/**
	 * 
	 */
	public String getWifiServiceInquiry(String xmlRequestMessage) throws MobilyCommonException {
		String replyMessage = "";
		log.debug("MQSupplementaryServiceInquieyDAO > getWifiServiceInquiry > called");
		try {
			/*
			 * Get QueuName
			 */
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.WIFI_ROAMING_SERVICE_INQUIRY_REQUEST_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getWifiServiceInquiry >Request Queue Name  ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.WIFI_ROAMING_SERVICE_INQUIRY_REPLY_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getWifiServiceInquiry >Reply Queue Name ["+ strReplyQueueName + "]");
			
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, 
				        														   strQueueName,
																				   strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getWifiServiceInquiry > MQSend Exception "+ e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			} catch (Exception e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getWifiServiceInquiry > Exception " + e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			}
		return replyMessage;
	}

	/**
	 * 
	 */
	public String getRaomingCallServiceInquiry(String xmlRequestMessage) throws MobilyCommonException {
		String replyMessage = "";
		log.debug("MQSupplementaryServiceInquieyDAO > getRaomingCallServiceInquiry > called");
		try {
			/*
			 * Get QueuName
			 */
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.ROAMING_CALL_INQ_REQUEST_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getRaomingCallServiceInquiry >Request Queue Name  ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.ROAMING_CALL_INQ_REPLY_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getRaomingCallServiceInquiry >Reply Queue Name ["+ strReplyQueueName + "]");
			
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, 
				        														   strQueueName,
																				   strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getRaomingCallServiceInquiry > MQSend Exception "+ e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			} catch (Exception e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getRaomingCallServiceInquiry > Exception " + e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			}
		return replyMessage;
	}

	/**
	 * 
	 */
	public String getSafeArrStatAndRecipInquiry(String xmlRequestMessage) throws MobilyCommonException {
		String replyMessage = "";
		log.debug("MQSupplementaryServiceInquieyDAO > getSafeArrStatAndRecipInquiry > called");
		try {
			/*
			 * Get QueuName
			 */
			log.debug("Start get MQ parameter value from ResourceBundle....");
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.SAFE_ARRIVAL_INQ_REQUEST_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getSafeArrStatAndRecipInquiry >Request Queue Name  ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.SAFE_ARRIVAL_INQ_REPLY_QUEUENAME);
			log.debug("MQSupplementaryServiceInquieyDAO > getSafeArrStatAndRecipInquiry >Reply Queue Name ["+ strReplyQueueName + "]");
			
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage, 
				        														   strQueueName,
																				   strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getSafeArrStatAndRecipInquiry > MQSend Exception "+ e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			} catch (Exception e) {
				log.fatal("MQSupplementaryServiceInquieyDAO > getSafeArrStatAndRecipInquiry > Exception " + e);
				throw new MobilyCommonException(ErrorIfc.EXCEPTION_MQ_EXCEPTION);
			}
		return replyMessage;
	}
}
