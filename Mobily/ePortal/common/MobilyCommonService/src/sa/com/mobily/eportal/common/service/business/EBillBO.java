/*
 * Created on Jan 6, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.MQDAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.EBillDAO;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.vo.EBillReplyVO;
import sa.com.mobily.eportal.common.service.vo.EBillRequestVO;
import sa.com.mobily.eportal.common.service.xml.EBillXMLHandler;


/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EBillBO {
	  private static final Logger log = LoggerInterface.log;

	public EBillReplyVO subscribeEBill(EBillRequestVO eBillRequestVO)
			throws MobilyCommonException, SystemException {

		log.debug("EBillBO > subscribeEBill > start");

		EBillReplyVO objEBillReplyVO = new EBillReplyVO();

		//generate XML request
		try {
			String xmlReply = "";
			EBillXMLHandler xmlHandler = new EBillXMLHandler();
			String xmlRequest = xmlHandler
					.generateRequestXMLMessage(eBillRequestVO);
			log.debug("EBillBO > subscribeEBill > XML request > " + xmlRequest);

			//get DAO object
			MQDAOFactory daoFactory = (MQDAOFactory) DAOFactory
					.getDAOFactory(DAOFactory.MQ);
			EBillDAO eBillDAO = (EBillDAO) daoFactory.getMQEBillDAO();
			xmlReply = eBillDAO.sendToMQWithReply(xmlRequest);
			log.debug("EBillBO > subscribeEBill > XML Reply > " + xmlReply);

			//parsing the xml reply
			objEBillReplyVO = xmlHandler.parsingReplyXMLMessage(xmlReply);

			//check if the error code returned fom backend greater than 0 then it will throw MobilyCommonexception object
			/*if(Integer.parseInt(objEBillReplyVO.getErrorCode()) > 0){
			 MobilyCommonException ex = new MobilyCommonException(objEBillReplyVO.getErrorCode());
			 throw ex;
			 }*/

		} catch (MobilyCommonException e) {
			log.fatal("EBillBO > subscribeEBill > MobilyCommonException > "
					+ e.getMessage());
			throw e;
		} catch (Exception e) {
			log
					.fatal("EBillBO > subscribeEBill >Exception > "
							+ e.getMessage());
			throw new SystemException(e);
		}
		log.debug("EBillBO > subscribeEBill > end");
		return objEBillReplyVO;

	}
}