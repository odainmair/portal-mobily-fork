/*
 * Created on Sep 10, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author abadr
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SecondaryEmailReplyVO {

	private SecondaryEmailReplyHeaderVO header = null;
	private String errorCode = null;

	/**
	 * @return Returns the errorCode.
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode The errorCode to set.
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return Returns the header.
	 */
	public SecondaryEmailReplyHeaderVO getHeader() {
		return header;
	}
	/**
	 * @param header The header to set.
	 */
	public void setHeader(SecondaryEmailReplyHeaderVO header) {
		this.header = header;
	}
}
