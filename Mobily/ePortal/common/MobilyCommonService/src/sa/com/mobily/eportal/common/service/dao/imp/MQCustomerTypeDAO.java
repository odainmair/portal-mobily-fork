/*
 * Created on Feb 17, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CustomerTypeDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQCustomerTypeDAO implements CustomerTypeDAO {
    private static final Logger log = LoggerInterface.log;
    
    public String getCustomerType(String xmlMessage) {
		String replyMessage = "";
		log.info("MQCustomerTypeDAO > getCustomerType > Called.................");
		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CUSTOMER_INQ_REQUEST_QUEUENAME);
			log.debug("MQCustomerTypeDAO > getCustomerType > The request Queue Name for Customer Type ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CUSTOMER_INQ_REPLY_QUEUENAME);
			log.debug("MQCustomerTypeDAO > getCustomerType > The reply Queue Name for Customer Type ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
//			       AlarmService alarmService = new AlarmService();
//			       alarmService.RaiseAlarm(ServicesIfc.ALARM_MQ_CUSTOMER_TYPE);

				log.fatal("MQCustomerTypeDAO > getCustomerType > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQCustomerTypeDAO > getCustomerType > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQCustomerTypeDAO > getCustomerType > Done.................");
		return replyMessage;

    
    }

}
