/**
 * 
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.StrategicTrackVO;

import com.mobily.exception.mq.MQSendException;


/**
 * @author s.vathsavai.mit
 *
 */
public class MQCorporateStrategicTrackDAO implements CorporateStrategicTrackDAO {

	private static final Logger log = LoggerInterface.log;
	
	
	public String doCorporateInquiry(String xmlRequest) {
		log.info("MQCorporateStrategicTrackDAO > doCorporateInquiry :: start");
		
		String replyMessage = "";
		String requestQueName = MobilyUtility.getPropertyValue(ConstantIfc.REQUEST_QUEUENAME_CORPORATE_INQUIRY);
		String replyQueName = MobilyUtility.getPropertyValue( ConstantIfc.RESPONSE_QUEUENAME_CORPORATE_INQUIRY );
		log.debug("MQCorporateStrategicTrackDAO > doCorporateInquiry :: xmlRequest = "+xmlRequest);
		log.debug("MQCorporateStrategicTrackDAO > doCorporateInquiry :: requestQueName = "+requestQueName+" reply queue name = "+replyQueName);
		try {
            replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequest, requestQueName, replyQueName);
		}catch (RuntimeException e) {
			log.fatal("MQCorporateStrategicTrackDAO -> doCorporateInquiry> MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
		    log.fatal("MQCorporateStrategicTrackDAO-> doCorporateInquiry > MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		}
		
		log.info("MQCorporateStrategicTrackDAO > doCorporateInquiry :: end");
        return replyMessage;
	}


	public String updateCircuitAliasName(String xmlRequest) {
		log.info("MQCorporateStrategicTrackDAO > updateCircuitAliasName :: start");
		
		String replyMessage = "";
		String requestQueName = MobilyUtility.getPropertyValue(ConstantIfc.REQUEST_QUEUENAME_UPDATE_CIRCUIT_ALIAS_NAME);
		String replyQueName = MobilyUtility.getPropertyValue( ConstantIfc.RESPONSE_QUEUENAME_UPDATE_CIRCUIT_ALIAS_NAME );
		log.debug("MQCorporateStrategicTrackDAO > updateCircuitAliasName :: xmlRequest = "+xmlRequest);
		log.debug("MQCorporateStrategicTrackDAO > updateCircuitAliasName :: requestQueName = "+requestQueName+" reply queue name = "+replyQueName);
		try {
            replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequest, requestQueName, replyQueName);
		}catch (RuntimeException e) {
			log.fatal("MQCorporateStrategicTrackDAO -> updateCircuitAliasName> MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
		    log.fatal("MQCorporateStrategicTrackDAO-> updateCircuitAliasName > MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		}
		
		log.info("MQCorporateStrategicTrackDAO > updateCircuitAliasName :: end");
        return replyMessage;
	}


	public void doCorporateInquiryAsync(String xmlRequest) {
		log.info("MQCorporateStrategicTrackDAO > doCorporateInquiryAsync :: start");
		
		String requestQueName = MobilyUtility.getPropertyValue(ConstantIfc.REQUEST_QUEUENAME_CORPORATE_INQUIRY);
		log.debug("MQCorporateStrategicTrackDAO > doCorporateInquiryAsync :: requestQueName = "+requestQueName);
		
		String replyToQueueName = MobilyUtility.getPropertyValue(ConstantIfc.RESPONSE_QUEUENAME_CORPORATE_INQUIRY_ASYNC);
		log.debug("MQCorporateStrategicTrackDAO > doCorporateInquiryAsync :: replyToQueueName = "+replyToQueueName);
		
		String queueManagerName = MobilyUtility.getMQConfigPropertyValue(ConstantIfc.MQ_QUEUE_MANAGER_NAME);
		log.debug("MQCorporateStrategicTrackDAO > doCorporateInquiryAsync :: queueManagerName = "+queueManagerName);
		
		try {
            MQConnectionHandler.getInstance().sendMessageOnly(xmlRequest, requestQueName,replyToQueueName,queueManagerName);
		} catch (RuntimeException e) {
			log.fatal("MQCorporateStrategicTrackDAO -> doCorporateInquiryAsync> MQSend Exception > "+ e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
		    log.fatal("MQCorporateStrategicTrackDAO-> doCorporateInquiryAsync > Exception > "+ e.getMessage());
			throw new SystemException(e);
		}
		
		log.info("MQCorporateStrategicTrackDAO > doCorporateInquiryAsync :: end");
       
	}


	public StrategicTrackVO doInitialInquiryFromDB(String billingAccountNumber) {
		// TODO Auto-generated method stub
		return null;
	}


	public boolean insertCorpRecordInDB(String billingAccountNumber) {
		// TODO Auto-generated method stub
		return false;
	}


	public boolean updateCorpRecordInDB(String billingAccountNumber) {
		// TODO Auto-generated method stub
		return false;
	}


	public String getMessageFromDB(String billingAccountNumber) {
		// TODO Auto-generated method stub
		return null;
	}


	public void updateStrategicTrackRecordInDB(String billingAccountNumber,
			String xmlMessage) {
		// TODO Auto-generated method stub
		
	}


	public boolean updateExpiredStatusInDB(String billingAccountNumber,
			String expiredStatus) {
		// TODO Auto-generated method stub
		return false;
	}

}
