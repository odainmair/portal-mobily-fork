/*
 * Created on Apr 15, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.ContactUSCacheDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.ContactUSObjectVO;


/**
 * @author r.bandi.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OracleContactUSCacheDAO implements ContactUSCacheDAO{
	private static final Logger log = LoggerInterface.log;

	public ArrayList getContactUsInquiryTypeItems(){
		log.info("ContactUSCacheDAO > getContactUsInquiryTypeItems > Called ");
		ArrayList inquiryList = new ArrayList();
	 
		Connection connection   = null;
	    ResultSet resultset     = null;
	    PreparedStatement pstm  = null;
	   	  
	   try{
	 	  String sqlStatament = "SELECT * From SR_CONTACTUS_TBL WHERE type=1 order by ID"; //type 1=consumer
		   
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament);
	      resultset  = pstm.executeQuery();
	      ContactUSObjectVO contactVO = null;
	      while(resultset.next()){
	      	    contactVO = new ContactUSObjectVO();
	      		contactVO.setID(resultset.getString("ID"));
	      		contactVO.setInquiryTypeDesc_en(resultset.getString("DESC_EN"));
	      		contactVO.setInquiryTypeDesc_ar(resultset.getString("DESC_AR"));
	      		contactVO.setMailTO(resultset.getString("MAIL_TO"));
	      		contactVO.setCount(resultset.getInt("count"));
	      		contactVO.setReply(resultset.getString("reply"));
	      		contactVO.setShowToUser(resultset.getInt("SHOW_TO_USER"));
	      		inquiryList.add(contactVO);
	      }
	      log.debug("ContactUSCacheDAO > getContactUsInquiryTypeItems > > Done ["+inquiryList.size()+"]");
	   }catch(SQLException e){
	   	  log.fatal("ContactUSCacheDAO > getContactUsInquiryTypeItems  > SQLException > "+e.getMessage());
	   	  throw new SystemException(e);
	   	
	   }catch(Exception e){
	   	  log.fatal("ContactUSCacheDAO > getContactUsInquiryTypeItems >  Exception > "+e.getMessage());
	   	  throw new SystemException(e);  
	   }
	   finally{
	   	JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
	   }
	   return inquiryList;
		
	}

	public ArrayList getContactUsBusinessUserInquiryTypeItems() {
		log.info("ContactUSCacheDAO > getContactUsBusinessUserInquiryTypeItems > Called ");
		ArrayList inquiryList = new ArrayList();
	 
		Connection connection   = null;
	    ResultSet resultset     = null;
	    PreparedStatement pstm  = null;
	   	  
	   try{
	 	  String sqlStatament = "SELECT * From SR_CONTACTUS_TBL WHERE type=2 order by ID"; //type 2=business
		   
	      connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
	      pstm       = connection.prepareStatement(sqlStatament);
	      resultset  = pstm.executeQuery();
	      ContactUSObjectVO contactVO = null;
	      while(resultset.next()){
	      	    contactVO = new ContactUSObjectVO();
	      		contactVO.setID(resultset.getString("ID"));
	      		contactVO.setInquiryTypeDesc_en(resultset.getString("DESC_EN"));
	      		contactVO.setInquiryTypeDesc_ar(resultset.getString("DESC_AR"));
	      		contactVO.setMailTO(resultset.getString("MAIL_TO"));
	      		contactVO.setCount(resultset.getInt("count"));
	      		contactVO.setReply(resultset.getString("reply"));
	      		contactVO.setShowToUser(resultset.getInt("SHOW_TO_USER"));
	      		inquiryList.add(contactVO);
	      }
	      log.debug("ContactUSCacheDAO > getContactUsBusinessUserInquiryTypeItems > > Done ["+inquiryList.size()+"]");
	   }catch(SQLException e){
	   	  log.fatal("ContactUSCacheDAO > getContactUsBusinessUserInquiryTypeItems  > SQLException > "+e.getMessage());
	   	  throw new SystemException(e);
	   	
	   }catch(Exception e){
	   	  log.fatal("ContactUSCacheDAO > getContactUsBusinessUserInquiryTypeItems >  Exception > "+e.getMessage());
	   	  throw new SystemException(e);  
	   }
	   finally{
	   	JDBCUtility.closeJDBCResoucrs(connection , pstm , resultset);
	   }
	   return inquiryList;
	}
	
	public static void main(String[] args) {
		}
}

