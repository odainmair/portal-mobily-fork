/*
 * Created on Nov 18, 2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.customtag;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.LookupTableUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.LookupTableVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;

/**
 * @author msayed
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ComboBoxByItemTag extends TagSupport {

	private String id = "";
	private String locale = "en";
	private String name = "";
	private String style = "";
	private String selectedItem = null  ;
	private String script = "";
	private int selectIndex;
	private String objectId = "";
	private String readOnly = "";
	private String addOptiontitle="";
	private String data_dojo_props="";
	
	public String getData_dojo_props()
	{
		return data_dojo_props;
	}



	public void setData_dojo_props(String data_dojo_props)
	{
		this.data_dojo_props = data_dojo_props;
	}



	public String getAddOptiontitle() {
		return addOptiontitle;
	}



	public void setAddOptiontitle(String addOptiontitle) {
		this.addOptiontitle = addOptiontitle;
	}
	private static final Logger log = LoggerInterface.log;
	
	public int doStartTag() throws JspException {
		try {
			// TODO Auto-generated method stub
			JspWriter out =pageContext.getOut();
			ArrayList listOfItem = (ArrayList)LookupTableUtility.getInstance().getLookupDataById(id);
			if(listOfItem != null)
				log.debug("ComboBoxByItemTag > Item Loaded from DB for Id ="+id +" = "+listOfItem.size()+" items");
			
			
			StringBuffer strBuffer = new StringBuffer();
			if(listOfItem != null) {
				if(FormatterUtility.isEmpty(objectId) ) {
					objectId ="";
				}else {
					objectId =" id ="+objectId;
				}
				
				if(FormatterUtility.isEmpty(readOnly)) {
					readOnly ="";
				}else {
					readOnly =" readonly ="+readOnly;
				}
				
				if(FormatterUtility.isEmpty(data_dojo_props)) {
					data_dojo_props ="";
				}else {
					data_dojo_props =" data_dojo_props =\""+data_dojo_props+"\" ";
				}
			  strBuffer.append("<SELECT dojoType='dijit.form.Select' name="+name+" "+objectId+" "+readOnly+" "+data_dojo_props+" "+"class="+style+ " "+script +" >");
			  LookupTableVO objectVO = null;
			  String desc = "";
			  String selected = "";

			  if(addOptiontitle!=""){
				  strBuffer.append("<OPTION value = -1" +selected+">"+addOptiontitle +"</OPTION>");
			  }
			  if(listOfItem!= null){
			  for(int i =0 ; i< listOfItem.size() ; i++) {
			    	objectVO = (LookupTableVO)listOfItem.get(i);
			    	
			    	locale = MobilyUtility.checkNull(locale);
			    	if("en".equalsIgnoreCase(locale))
			    		desc = objectVO.getItemDesc_en();
			    	else
			    		desc = objectVO.getItemDesc_ar();
			    	
			    	selectedItem = MobilyUtility.checkNull(selectedItem);
			    	if("".equals(selectedItem)) {
			    		//log.debug("ComboBoxByItemTag > Item Not Choosed");
			    	}else {
			    		//selectIndex = Integer.parseInt(selectedItem);
			    		//selectIndex = Integer.parseInt(selectedItem.split("_")[1]);
//			    		System.out.println("selectedItem : " + selectedItem + " , "+ id+"_"+objectVO.getSub_id());
			    		if((id+"_"+objectVO.getSub_id()).equalsIgnoreCase(selectedItem)) //if(selectedItem.equalsIgnoreCase(objectVO.getItemDesc_en()) || selectedItem.equalsIgnoreCase(objectVO.getItemDesc_ar()))
				    		selected =" selected ";
			    	}
//					if(selectedItem != null && selectedItem.equalsIgnoreCase("null") &&  selectedItem.length()> 0);
//					     selectIndex = Integer.parseInt(selectedItem);
			    	
			    	strBuffer.append("<OPTION value = '"+id+"_"+objectVO.getSub_id()+"'" +selected+">"+desc +"</OPTION>");
			    	selected = "";
			  }
			  }
			  strBuffer.append("</SELECT>");
			  out.println(strBuffer.toString());
			  return SKIP_BODY;
			  
			}
		}  catch (SystemException e) {
			// TODO Auto-generated catch block
			log.fatal("ComboBoxByItemTag >  SystemException > "+e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
		    log.fatal("ComboBoxByItemTag >  SystemException > "+e.getMessage());
		}
		
		return super.doStartTag();

	}

	
	
	public static void main(String[] args) {
		ComboBoxByItemTag test = new ComboBoxByItemTag();
		try {
			test.doStartTag();
		} catch (JspException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new SystemException(e);
		}
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSelectedItem() {
		return selectedItem;
	}
	public void setSelectedItem(String selectedItem) {
		this.selectedItem = selectedItem;
	}
	public String getStyle() {
		return style;
	}
	public void setStyle(String style) {
		this.style = style;
	}
	public String getScript() {
		return script;
	}
	public void setScript(String script) {
		this.script = script;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getReadOnly() {
		return readOnly;
	}
	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}
	public int getSelectIndex() {
		return selectIndex;
	}
	public void setSelectIndex(int selectIndex) {
		this.selectIndex = selectIndex;
	}
}
