package sa.com.mobily.eportal.core.service.jms.stub;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import sa.com.mobily.eportal.core.service.JMSConnectionManager;
import sa.com.mobily.eportal.core.service.Logger;
import sa.com.mobily.eportal.core.service.Queues;

public class JMSStubConnectionManager extends JMSConnectionManager {

    private static final Logger log = Logger.getLogger(JMSStubConnectionManager.class);
    private static final String STUBS_DIR = "/jms-stubs/";
    private static final String FILE_NAME_ENABLED_STUBS =  STUBS_DIR + "enabled-stubs.txt";
//    private static final String JMS_STUBS_EXTENTION = "xml";
    private static JMSStubConnectionManager instance = null;
    private static final Object singletonLock = new Object();
    
    private Map<String, List<QueueStub>> queueStubs = new HashMap<String, List<QueueStub>>();
    String[] stubFiles;
    
    protected JMSStubConnectionManager(String connectionFactory) throws NamingException {
	    super(connectionFactory);
	    initStubs();
    }
    
    public static JMSStubConnectionManager getInstance(String queueConnectionFactory) throws NamingException {
	    if (instance != null) {
	        return instance;
	    }
	    
	    synchronized(singletonLock) {
	        if(instance == null) {
	        	instance = new JMSStubConnectionManager(queueConnectionFactory);
	        }
	        
	        return instance;
	    }
    }
    
    @Override
    protected boolean initConnectionFactory(String queueConnectionFactory) throws NamingException {
    	try {
    		// try to initialize parent JMS connection manager
    		log.info(" trying to initialize parent JMS Connection Manager with [" + queueConnectionFactory + "]");
        
    		return super.initConnectionFactory(queueConnectionFactory);
        } catch (Throwable e) {
            log.error("Failed to initialize parent JMS Connection Factory with [" + queueConnectionFactory + "]", e);
            log.info(" JMS Connection Manager will continue with the stubs only.");
        }

    	return true;
    }

    private void initStubs() {
    	try {
	        log.info(" initializing jms stubs ...");
	        
	        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
	        log.debug(" currentClassLoader [" + contextClassLoader + "]");
	        
	        InputStream streamEnabledStubs = Thread.currentThread().getContextClassLoader().getResourceAsStream(FILE_NAME_ENABLED_STUBS);
	        
	        if(streamEnabledStubs == null) {
		        log.info(" File [" + FILE_NAME_ENABLED_STUBS + "] is not found. No JMS stubs will be provided.");
		        return;
	        }
	        
	        BufferedReader brEnabledStubs = new BufferedReader(new InputStreamReader(streamEnabledStubs));
	        
	        String stubFileName;
	        
	        while((stubFileName = brEnabledStubs.readLine()) != null) {
	            InputStream streamStubFile = contextClassLoader.getResourceAsStream(STUBS_DIR + stubFileName);
	            log.info(" Stub file [" + stubFileName + "] " + (streamStubFile != null? "Found" : "Not Found"));
	            
	            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
                log.info(" Parsing jms stub file [" + stubFileName + "].");
                
                StubContentHandler stubContentHandler = new StubContentHandler(stubFileName, queueStubs);
                xmlReader.setContentHandler(stubContentHandler);
                xmlReader.parse(new InputSource(new BufferedReader(new InputStreamReader(streamStubFile))));
                log.info(" Parsing jms stub file [" + stubFileName + "] done.");
	        }
	        
	        log.info(" jms stubs initialization completed, [" + queueStubs.size() + "] stubbed queues");        
        } catch (SAXException e) {
            log.error(" Error parsing JMS stubs", e);
        } catch (IOException e) {
            log.error(" Error initializing JMS stubs", e);
        }
    }
    
    @Override
    public String sendMessageWithReply(String message, Queues jndiToQueue, Queues jndiReplyqueue, long timeOut) throws Exception {
        // try to get stubbedReply
        String stubbedReply = getStubbedReply(jndiToQueue, message);
        
        if (stubbedReply != null) {
            return stubbedReply;
        }
        
        // if there's no stubbed reply, try invoking the parent method
        log.info("No Stubbed reply found for " + jndiToQueue + " whose reply is " + jndiReplyqueue + ", will use parent JMSConnectionManager.");
        return super.sendMessageWithReply(message, jndiToQueue, jndiReplyqueue, timeOut);
    }

    @Override
    public String sendMessageWithReply(String message, Queues jndiToQueue, Queues jndiReplyqueue) throws Exception {
        // try to get stubbedReply
        String stubbedReply = getStubbedReply(jndiToQueue, message);
        if (stubbedReply != null) {
            return stubbedReply;
        }
    
        // if there's no stubbed reply, try invoking the parent method
        log.info(" No Stubbed reply found, will use parent JMSConnectionManager.");
        return super.sendMessageWithReply(message, jndiToQueue, jndiReplyqueue);
    }
    
    @Override
    public boolean sendMessage(String message, Queues jniToQueue, Queues jndiReplyQueue) throws Exception {
        // try to find a stub
        String stubbedReply = getStubbedReply(jniToQueue, message);
        if (stubbedReply != null) {
            return true;
        }
        
        // if there's no stub, try invoking the parent method
        log.info(" No Stub found, will use parent JMSConnectionManager.");
        return super.sendMessage(message, jniToQueue, jndiReplyQueue);
    }
    
    public String getStubbedReply(Queues queueName, String messageText) throws JMSException, NamingException {
	    QueueStub matchingStub = findMatchingStub(queueName, messageText);
	    
	    if (matchingStub != null) {
	        return matchingStub.getResponse();
	    }
	
	    // if no stubbed reply could be found
	    return null; 
    }
    
    private QueueStub findMatchingStub(Queues queueName, String messageText) {
    	DocumentBuilder docBuilder;
    	
        try {
            
            // get the stubs defined for this queue
            List<QueueStub> stubs = queueStubs.get(queueName.toString());
            
            // If there are no stubs for this queue
            if(stubs == null) {
	            log.info(" no stubs for queue [" + queueName + "]");
	            return null;
            }
            
            Document xmlRequest = null;
            try {
                // Parse the requestXml into an XML Document
                docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                xmlRequest = docBuilder.parse(new ByteArrayInputStream(messageText.getBytes()));
            } catch (Exception e) {
                log.error("Failed to parse request message as xml", e);
                log.info(" Conditoinal Stubs will be skipped from matching.");
            }
        
            XPath xPath = XPathFactory.newInstance().newXPath();
        
	        // for each declared stub
	        for (QueueStub stub : stubs) {
		        
	        	// Stub with no conditions
		        if (stub.getRequestFilters() == null) {
		            log.info(" unconditional stub found for queue [" + queueName + "]");
		            return stub;
		        }
		
		        // if the request could be parsed as XML
		        if (xmlRequest != null) {
		           
		        	// check the xPath conditions in order
		            for (String requestFilter : stub.getRequestFilters()) {
		                if ("".equals(requestFilter)
		                        || xPath.evaluate("boolean(" + requestFilter + ")", xmlRequest) == "true") {
		                    log.info(" Queue [" + queueName + "] matched condition [" + requestFilter + "]");
		                    return stub;
		                }
		            }
		        }
	        }
        
        } catch (XPathExpressionException e) {
            log.error("Error finding matching stub", e);
        }
    
        // no matching stubs
        return null;
    }
}
