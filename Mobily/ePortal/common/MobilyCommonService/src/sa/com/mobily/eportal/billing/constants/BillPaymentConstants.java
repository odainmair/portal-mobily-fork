package sa.com.mobily.eportal.billing.constants;

public interface BillPaymentConstants {

	public static final String INVALID_REQUEST ="20131";
	public static final String INVALID_REPLY ="20132";
	
	// Email Invoice Payment Queue Names
		public static final String EMAIL_INVOICE_INQ_REQUEST_QUEUENAME = "mq.emailInvoice.inq.request.queue";
		public static final String EMAIL_INVOICE_INQ_REPLY_QUEUENAME = "mq.emailInvoice.inq.reply.queue";
		
		public static final String EMAIL_INVOICE_SERVICES_REQUEST_QUEUENAME = "mq.emailInvoice.services.request.queue";
		public static final String EMAIL_INVOICE_SERVICES_REPLY_QUEUENAME = "mq.emailInvoice.services.reply.queue";
	
		
		public static final String ERROR_CODE_EMPTY_REPLY_XML = "9999";
		
		
		//For ConnectOTS
		public static final String FUNC_ID = "WiMAXOffShelf";
		public static final String SECURITY_KEY = "0000";
		public static final String REQUESTOR_CHANNEL_ID = "ePortal";
		public static final String REQUESTOR_CHANNEL_IVR = "IVR";
		public static final String MSG_VERSION = "0000";
		public static final String ERROR_INVALID_CPE_NUMBER = "15701";
		public static final String ERROR_CPE_NUMBER_ALREADY_ACTIVATED = "15707";
		public static final String ERROR_UNDER_PROGRESS = "15708";
		
		public static final String CPE_INQUIRY_FUNC_ID = "WimaxCPESerialInquiry";
		public static final String ACC_BAL_ADPTR_FUNC_ID = "ACC_BAL_ADPTR";
		public static final String GENERAL_INQUIRY_FUNC_ID = "GENERAL_INQUIRY";
		public static final String FORCE_RENEWAL_FUNC_ID = "FORCE_RENEWAL";
		
		 public static final String CONNECT_VOUCHER_RECHARGE = "CONNECT_VOUCHER_RECHARGE";
		 
		 //payment notification
	    public static final String CUSTOMER_PAYMENT_NOTIFY_REQUEST_QUEUENAME = "mq.customer.payment.notify.request.queue";
	    public static final String CUSTOMER_PAYMENT_NOTIFY_REPLY_QUEUENAME = "mq.customer.payment.notify.reply.queue";
	    public static final String PAYMENT_NOTIFY_MESSAGE_FORMAT_VALUE = "Payment Notification";
		public static final String MESSAGE_VER_VALUE = "01";
		public static final String CHANNEL_ID_VALUE_MPAY = "MPAY";
		public static final String CHANNEL_ID_VALUE_EPOR = "EPOR";
		public static final String CHANNEL_FUNC_VALUE_PMTNOT = "PMTNOT";
		public static final String LANG_VALUE = "E";
		public static final String LANG_VALUE_A = "A";
		public static final String SECURITY_INFO_VALUE = "secure";
		public static final String RETURN_CODE_VALUE = "0000";
		
}
