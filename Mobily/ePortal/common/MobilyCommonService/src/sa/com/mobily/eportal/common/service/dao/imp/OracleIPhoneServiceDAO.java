package sa.com.mobily.eportal.common.service.dao.imp;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.IPhoneServiceDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.IPhoneRequestVO;


/**
 * 
 * @author n.gundluru.mit
 *
 */
public class OracleIPhoneServiceDAO implements IPhoneServiceDAO{

	private static final Logger logger = LoggerInterface.log;
    
	/**
	 * Gets the user details
	 *  
	 * @param requestVo
	 */
	public void getUserDetails(IPhoneRequestVO requestVo){
		
		logger.info("OracleIPhoneServiceDAO > getUserDetails > start");
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rsltSet = null;
		
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));

		    StringBuffer sqlQuery = new StringBuffer();
		    
				sqlQuery.append("SELECT ORIGINAL_MSISDN,PARENT_MSISDN,PARENT_HASHCODE,CUSTOMER_TYPE,LINE_TYPE,");
					sqlQuery.append(" PACKAGE_ID, USER_ID, USER_BLOCKED, DEVICE_ID, DEVICE_NAME");
					sqlQuery.append(" FROM MOBILY_IPHONE_SERVICE_TBL");
					sqlQuery.append(" WHERE  LOWER(ORIGINAL_HASHCODE) = LOWER(?)");
			
			pstmt  = connection.prepareStatement(sqlQuery.toString());
			
				pstmt.setString(1, requestVo.getOriginalHashCode());
			
			rsltSet = pstmt.executeQuery();

			
			if( rsltSet!= null && rsltSet.next()){
				requestVo.setOriginalMsisdn(rsltSet.getString("ORIGINAL_MSISDN"));
				requestVo.setParentMsisdn(rsltSet.getString("PARENT_MSISDN"));
				requestVo.setParentHashCode(rsltSet.getString("PARENT_HASHCODE"));
				requestVo.setCustomerType(rsltSet.getString("CUSTOMER_TYPE"));
				requestVo.setLineType(rsltSet.getInt("LINE_TYPE"));
				requestVo.setPackageID(rsltSet.getString("PACKAGE_ID"));
				requestVo.setUserId(rsltSet.getString("USER_ID"));
				requestVo.setIsBlockedCustomer(rsltSet.getString("USER_BLOCKED"));
				requestVo.setDevice(rsltSet.getString("DEVICE_ID"));
				requestVo.setDeviceName(rsltSet.getString("DEVICE_NAME"));
			}
		}catch(Exception e){
			logger.fatal("OracleIPhoneServiceDAO > getUserDetails > Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
		}finally{
			JDBCUtility.closeJDBCResoucrs(connection, pstmt, rsltSet);
		}
	}
}