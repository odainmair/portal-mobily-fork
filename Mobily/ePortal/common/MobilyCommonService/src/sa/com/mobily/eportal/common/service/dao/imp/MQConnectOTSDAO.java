package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.ConnectOTSDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQConnectOTSDAO implements ConnectOTSDAO {

    private static final Logger log = LoggerInterface.log;
    
    /**
     * 
     */
    public String doVoucherRecharge(String xmlMessage) {
		String replyMessage = "";
		log.info("MQConnectOTSDAO > doVoucherRecharge > Called");

		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CONNECT_VOUCHER_RECHARGE_REQUEST_QUEUENAME);
			log.debug("MQConnectOTSDAO > doVoucherRecharge > The request Queue ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CONNECT_VOUCHER_RECHARGE_REPLY_QUEUENAME);
			log.debug("MQConnectOTSDAO > doVoucherRecharge > The reply Queue Name ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQConnectOTSDAO > doVoucherRecharge > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQConnectOTSDAO > doVoucherRecharge > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQConnectOTSDAO > doVoucherRecharge > Done");
		return replyMessage;
    }


    /**
     * 
     */
    public String getMsidnForSim(String xmlMessage) {
		String replyMessage = "";
		log.info("MQConnectOTSDAO > getMsidnForSim > Called");
		
		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CONNECT_GET_MSISDN_REQUEST_QUEUENAME);
			log.debug("MQConnectOTSDAO > getMsidnForSim > The request Queue Name ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.CONNECT_GET_MSISDN_REPLY_QUEUENAME);
			log.debug("MQConnectOTSDAO > getMsidnForSim > The reply Queue Name ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQConnectOTSDAO > getMsidnForSim > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQConnectOTSDAO > getMsidnForSim > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQConnectOTSDAO > getMsidnForSim > Done");
		return replyMessage;
    }
}
