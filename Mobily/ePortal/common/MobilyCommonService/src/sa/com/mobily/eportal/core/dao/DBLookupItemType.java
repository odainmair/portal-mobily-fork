//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.dao;

/***
 * Identifies the available database Lookup item types
 * @author Hossam Omar
 *
 */
public enum DBLookupItemType {
    REGION(1)
    , CITY(2)
    , COUNTRY(9)
    , IPTV_ADD_ON(53)
    , IPTV_A_LA_CARTE(54);

    private final int id;

    private DBLookupItemType(final int id) {
	this.id = id;
    }

    public int getID() {
	return id;
    }
}
