package sa.com.mobily.eportal.notification.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.notification.vo.MDBNotificationVO;

/**
 * @author r.agarwal.mit
 * @Description: 
 * @Date March 31, 2017
 */
public class MDBNotificationDAO extends AbstractDBDAO<MDBNotificationVO>{
	
	
	private static final String SAVE_DATA = "INSERT INTO LP_MDB_NOTIFICATION_MSG_TBL(QUEUE, MESSAGE, TRANSACTION_ID, SERVICE_ID, CREATED_DATE) VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP)";
	
	
	private MDBNotificationDAO() {
		super(DataSources.DEFAULT);
	}
	public static MDBNotificationDAO getInstance() {
		return MDBNotificationDAOHolder.mdbNotificationDAOInstance;
	}
	
	private static class MDBNotificationDAOHolder {
		private static final MDBNotificationDAO mdbNotificationDAOInstance = new MDBNotificationDAO();
	}


	public int createMDBNotification(MDBNotificationVO mdbNotificationVO)
	{
		return update(SAVE_DATA, mdbNotificationVO.getQueue(), mdbNotificationVO.getMessage(), mdbNotificationVO.getTransactionId(), mdbNotificationVO.getServiceId());
	}
	
	@Override
	protected MDBNotificationVO mapDTO(ResultSet rs) throws SQLException {
		MDBNotificationVO serviceTransactionVO = new MDBNotificationVO();
		serviceTransactionVO.setQueue(rs.getString("QUEUE"));
		serviceTransactionVO.setServiceId(rs.getInt("SERVICE_ID"));
		serviceTransactionVO.setMessage(rs.getString("MESSAGE"));
		serviceTransactionVO.setTransactionId(rs.getString("TRANSACTION_ID"));
		
		
		return serviceTransactionVO;
	}
	
	
}
