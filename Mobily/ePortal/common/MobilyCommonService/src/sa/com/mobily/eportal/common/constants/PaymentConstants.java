package sa.com.mobily.eportal.common.constants;

public interface PaymentConstants
{

	
	public static final String BANK_ERROR_DECLINED ="Declined";
	
	public static final String BANK_ERROR_INVALID_CARD_NUMBER ="Invalid Card Number";
	
	public static final String BANK_ERROR_INVALID_CARD_CVV ="Invalid credit card: incorrect secure code number length : Invalid Card Security Code length";
	
	public static final String BANK_ERROR_INVALID_CARD_EXPIRY_DATE="Invalid Card Expiry Date";
	
	public static final String BANK_ERROR_UNSPECIFIED_FAILURE="UNSPECIFIED_FAILURE";
	
	public static final String BANK_ERROR_INSUFFICIENT_FUNDS_1 ="INSUFFICIENT_FUNDS";
	
	public static final String BANK_ERROR_INSUFFICIENT_FUNDS_2 ="Insufficient Funds";
	
	public static final String BANK_ERROR_TRANSACTION_BLOCKED ="Transaction was blocked by the Payment Server because it did not pass all risk checks.";
	
	public static final String BANK_ERROR_TIMED_OUT ="TIMED_OUT";
	
	public static final String BANK_ERROR_RELATION_NOT_FOUND_1="Relationship not found for merchantID 601000189, trans type PRCH, card type MC";
	
	public static final String BANK_ERROR_DO_NOT_PROCEED ="gatewayRecommendation:DO_NOT_PROCEED";
	
	public static final String BANK_ERROR_RELATION_NOT_FOUND_2="Relationship not found for merchantID 601000189, trans type PRCH, card type MS";
	
	public static final String BANK_ERROR_CARD_PAYMENT_NOT_DETERMINE_1="Unable to determine card payment";
	
	public static final String BANK_ERROR_CARD_PAYMENT_NOT_DETERMINE_2="Cannot determine card brand.";
	
	public static final String BANK_ERROR_RELATION_NOT_FOUND_3="Relationship not found for merchantID 601000189, trans type PRCH, card type DS";
	
	public static final String ERROR_CODE_MBE_404 ="MBE_404";
	
	public static final String ERROR_CODE_MBE_405 ="MBE_405";
	
	public static final String ERROR_CODE_MBE_406 ="MBE_406";
	
	public static final String ERROR_CODE_MBE_407 ="MBE_407";
	
	public static final String ERROR_CODE_MBE_408 ="MBE_408";
	
	public static final String ERROR_CODE_MBE_409 ="MBE_409";
	
	public static final String ERROR_CODE_MBE_410 ="MBE_410";
	
	public static final String ERROR_CODE_MBE_411 ="MBE_411";
	
	public static final String ERROR_CODE_MBE_412 ="MBE_412";

}
