//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.03.04 at 12:59:53 PM IST 
//


package sa.com.mobily.eportal.customerAccDetails.jaxb.accDetailsResponse;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{}FuncId"/>
 *         &lt;element ref="{}MsgVersion"/>
 *         &lt;element ref="{}RequestorChannelId"/>
 *         &lt;element ref="{}RequestorChannelFunction"/>
 *         &lt;element ref="{}ChannelTransactionId"/>
 *         &lt;element ref="{}SrDate"/>
 *         &lt;element ref="{}ErrorCode"/>
 *         &lt;element ref="{}ErrorMsg"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "funcId",
    "msgVersion",
    "requestorChannelId",
    "requestorChannelFunction",
    "channelTransactionId",
    "srDate",
    "errorCode",
    "errorMsg"
})
@XmlRootElement(name = "SR_HEADER_REPLY")
public class SRHEADERREPLY {

    @XmlElement(name = "FuncId", required = true)
    protected String funcId;
    @XmlElement(name = "MsgVersion", required = true)
    protected String msgVersion;
    @XmlElement(name = "RequestorChannelId", required = true)
    protected String requestorChannelId;
    @XmlElement(name = "RequestorChannelFunction", required = true)
    protected String requestorChannelFunction;
    @XmlElement(name = "ChannelTransactionId", required = true)
    protected String channelTransactionId;
    @XmlElement(name = "SrDate", required = true)
    protected String srDate;
    @XmlElement(name = "ErrorCode", required = true)
    protected String errorCode;
    @XmlElement(name = "ErrorMsg", required = true)
    protected String errorMsg;

    /**
     * Gets the value of the funcId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncId() {
        return funcId;
    }

    /**
     * Sets the value of the funcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncId(String value) {
        this.funcId = value;
    }

    /**
     * Gets the value of the msgVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMsgVersion() {
        return msgVersion;
    }

    /**
     * Sets the value of the msgVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMsgVersion(String value) {
        this.msgVersion = value;
    }

    /**
     * Gets the value of the requestorChannelId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorChannelId() {
        return requestorChannelId;
    }

    /**
     * Sets the value of the requestorChannelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorChannelId(String value) {
        this.requestorChannelId = value;
    }

    /**
     * Gets the value of the requestorChannelFunction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestorChannelFunction() {
        return requestorChannelFunction;
    }

    /**
     * Sets the value of the requestorChannelFunction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestorChannelFunction(String value) {
        this.requestorChannelFunction = value;
    }

    /**
     * Gets the value of the channelTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelTransactionId() {
        return channelTransactionId;
    }

    /**
     * Sets the value of the channelTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelTransactionId(String value) {
        this.channelTransactionId = value;
    }

    /**
     * Gets the value of the srDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrDate() {
        return srDate;
    }

    /**
     * Sets the value of the srDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrDate(String value) {
        this.srDate = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the value of the errorMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMsg(String value) {
        this.errorMsg = value;
    }

}
