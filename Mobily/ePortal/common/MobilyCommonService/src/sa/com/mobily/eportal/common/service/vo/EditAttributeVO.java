/**
 * 
 */
package sa.com.mobily.eportal.common.service.vo;

/**
 * @author s.vathsavai.mit
 *
 */
public class EditAttributeVO extends BaseVO {
	public static final String PACKAGE_CLASSIFICATION_NON_GSM = "Non GSM";
	public static final String PACKAGE_CLASSIFICATION_GSM = "GSM";
	private String attributeName = null;
	private String attributeValue = null;
	private String kamEmail = null;
	private String attributeOldValue = null;
	private String accountNumber = null;
	private String clientName = null;
	private String errorCode = null;
	private String errorMessage = null;
	private String email = "";
	private String packageClassification;
	
		
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getKamEmail() {
		return kamEmail;
	}
	public void setKamEmail(String kamEmail) {
		this.kamEmail = kamEmail;
	}
	public String getAttributeOldValue() {
		return attributeOldValue;
	}
	public void setAttributeOldValue(String attributeOldValue) {
		this.attributeOldValue = attributeOldValue;
	}
	public String getPackageClassification()
	{
		return packageClassification;
	}
	public void setPackageClassification(String packageClassification)
	{
		this.packageClassification = packageClassification;
	}
		
}
