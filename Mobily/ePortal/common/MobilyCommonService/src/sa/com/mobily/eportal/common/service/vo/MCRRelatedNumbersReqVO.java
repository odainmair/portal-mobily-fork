package sa.com.mobily.eportal.common.service.vo;


/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MCRRelatedNumbersReqVO {
    
    private String requestorUserId;
    private String msisdn ;
    
    public String getRequestorUserId() {
		return requestorUserId;
	}
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
}