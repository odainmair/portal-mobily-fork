/*
 * Created on Sept 16, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.DAOFactory;
import sa.com.mobily.eportal.common.service.dao.ifc.ManageCreditCardDAO;
import sa.com.mobily.eportal.common.service.dao.imp.ePortalDBDao;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.facade.MobilyCommonFacade;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.sms.SMSSender;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.RandomGenerator;
import sa.com.mobily.eportal.common.service.valueobject.common.ManagedMSISDNDataVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardMessageHeaderVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentReplyVO;
import sa.com.mobily.eportal.common.service.vo.CreditCardPaymentRequestVO;
import sa.com.mobily.eportal.common.service.vo.ManagedWimaxDataVO;
import sa.com.mobily.eportal.common.service.xml.ManageCreditCardXMLHandler;



/**
 * @author r.agrawal.mit
 */

public class ManageCreditCardBO 
{
	private static final Logger log =LoggerInterface.log;
	public static final String CREDIT_CARD_PIN_SMS_ID_EN ="9093";
	public static final String CREDIT_CARD_PIN_SMS_ID_AR ="9094";

	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to call MQ for credit card management(Enquiry,Payment,Change PIN,Change Prime MSISDN,Cancel CC)
	  * @author r.agarwal.mit
	  * @param objCreditCardPaymentRequestVO
	  * @param requesterChannelFunction
	  * @return CreditCardPaymentReplyVO
	 * @throws MobilyCommonException 
	  */
	public CreditCardPaymentReplyVO manageCreditCards(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO,String requesterChannelFunction) throws MobilyCommonException {
		log.debug("ManageCreditCardBO > manageCreditCards > called");
		CreditCardPaymentReplyVO objCreditCardPaymentReplyVO = null;
    	String replyXMLmessage = null;
        String requestXMLmessage = null;
        
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.MQ);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
		
    	CreditCardMessageHeaderVO objCreditCardMessageHeaderVO = new CreditCardMessageHeaderVO();
		objCreditCardMessageHeaderVO.setMsgFormat(ConstantIfc.Messsage_Format);
    	objCreditCardMessageHeaderVO.setRequestorChannelId(ConstantIfc.Requestor_ChannelId);
    	objCreditCardMessageHeaderVO.setRequestorChannelFunction(requesterChannelFunction);
    	if(objCreditCardPaymentRequestVO.getUserIP() != null)
    		objCreditCardMessageHeaderVO.setRequestorIpAddress(objCreditCardPaymentRequestVO.getUserIP());
    	if(objCreditCardPaymentRequestVO.getUserUID() != null)
    		objCreditCardMessageHeaderVO.setRequestorUserId(objCreditCardPaymentRequestVO.getUserUID());
    	objCreditCardMessageHeaderVO.setReturnCode(ConstantIfc.RETURN_CODE_VALUE);
    	objCreditCardPaymentRequestVO.setHeader(objCreditCardMessageHeaderVO);
        //objCreditCardPaymentRequestVO.setTransactionID(getThirdGenerationTransId());
    	objCreditCardPaymentRequestVO.setTransactionDate((new SimpleDateFormat(ConstantIfc.DATE_FORMAT)).format(Calendar.getInstance().getTime()));
			
    	requestXMLmessage  = ManageCreditCardXMLHandler.generateXMLRequestMessage(objCreditCardPaymentRequestVO);
        log.debug("ManageCreditCardBO > getCreditCardsInquiry > XML request > "+requestXMLmessage);
        
        log.debug("ManageCreditCardBO > getCreditCardsInquiry > requesterChannelFunction > "+requesterChannelFunction);
        replyXMLmessage = manageCreditCardDAO.manageCreditCards(requestXMLmessage,requesterChannelFunction);
        log.debug("ManageCreditCardBO > getCreditCardsInquiry > XML Reply > "+replyXMLmessage);  
        
        // Parse the reply xml
        objCreditCardPaymentReplyVO = ManageCreditCardXMLHandler.parseXMLReplyMessage(replyXMLmessage);
        //log.debug("ManageCreditCardBO > manageCreditCardsInquiry > objCreditCardPaymentReplyVO after parsing ="+objCreditCardPaymentReplyVO);
        
        // For auditing the credit card payment request
        if(ConstantIfc.Requestor_Channel_Function_Payment.equalsIgnoreCase(requesterChannelFunction)) {
        	auditCreditCardPayment(objCreditCardPaymentRequestVO);
        }

		return objCreditCardPaymentReplyVO;
	}
	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the next transaction id from the Sequence created in EEDB
	  * @author r.agarwal.mit
	  * @return String
	  */
	public String getThirdGenerationTransId() {
		log.debug("ManageCreditCardBO > getThirdGenerationTransId > called");
        //get DAO object
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
		return manageCreditCardDAO.getThirdGenerationTransId();
	}
	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to audit the credit card payment transaction in EEDB
	  * @author r.agarwal.mit
	  * @param objCreditCardPaymentRequestVO
	  */
	public void auditCreditCardPayment(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO) {
		log.debug("ManageCreditCardBO > auditCreditCardPayment > called");
		
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
		
	    manageCreditCardDAO.auditCreditCardTransactionInDB(objCreditCardPaymentRequestVO);
	}
	
	 /**
	  * Date: Sept 17, 2009
	  * Description: This method is used to get the user managed number from EEDB and then check the validity of the numbers with Siebel
	  * @author r.agarwal.mit
	  * @param aMsisdn
	  * @return List
	  */
	public List getUserManagedNumbersList(String aMsisdn) {
		log.debug("ManageCreditCardBO > getUserManagedNumbersList > called");
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
		
		// Get Related Numbers from Siebel Stored Procedure -- START
		ArrayList<ManagedMSISDNDataVO> relatedList = new ArrayList<ManagedMSISDNDataVO>();
		
		try {
			MobilyCommonFacade mobilyCommonFacade= new MobilyCommonFacade();
			relatedList = mobilyCommonFacade.getCustomerRelatedAccountByNumber(aMsisdn);
		} catch (SystemException e) {
			log.error("ManageCreditCardBO > getUserManagedNumbersList  >> Exception while getting related numbers from Siebel::"+e); 
			
		} 
		// Get Related Numbers from Siebel Stored Procedure -- END
		
		// Get Managed Numbers from ePortal DB -- START
		ePortalDBDao l_DAO = new ePortalDBDao();
		ArrayList<ManagedWimaxDataVO> managedList = new ArrayList<ManagedWimaxDataVO>(); 
		
		try {
			managedList = l_DAO.getRelatedMSISDNList(aMsisdn);
		} catch (Exception e) {
			log.error("ManageCreditCardBO > getUserManagedNumbersList  >> Exception while getting managed numbers from DB::"+e); 
		}
		// Get Managed Numbers from ePortal DB -- END
		
		// filter the managed numbers
		managedList = filterManagedList(managedList, relatedList,aMsisdn);
		
		log.debug("ManageCreditCardBO > getUserManagedNumbersList > final managedList::"+managedList);
		
		return managedList;
		
	}
	
	/**
	 * This method is used to filter managed list
	 * @param managedList
	 * @param relatedList
	 * @return ArrayList<ManagedWimaxDataVO>
	 */
	public ArrayList<ManagedWimaxDataVO> filterManagedList(ArrayList<ManagedWimaxDataVO> managedList,	ArrayList<ManagedMSISDNDataVO> relatedList , String userPrimaryMSISDN) {
		log.debug("ManageCreditCardBO > filterManagedList > called ");
		ArrayList<ManagedWimaxDataVO>  filteredManagedList =  new ArrayList<ManagedWimaxDataVO> ();
		String 	relatedMSISDN = "";
		String 	managedMSISDN = "";
		boolean validNumber = false;

		for (int i = 0; managedList != null && i < managedList.size(); i++) {
			managedMSISDN = ((ManagedWimaxDataVO) managedList.get(i)).getMSISDN();
			validNumber = false;
			for (int j = 0; relatedList != null && j < relatedList.size(); j++) {
				relatedMSISDN = ((ManagedMSISDNDataVO) relatedList.get(j)).getMSISDN();
				if (managedMSISDN.equals(relatedMSISDN)) {
					validNumber = true;
					if (!FormatterUtility.isEmpty( ( (ManagedMSISDNDataVO) relatedList.get(j) ).getCustomertype() ) ) {
						log.debug("ManageCreditCardBO > filterManagedList > userPrimaryMSISDN  : "+userPrimaryMSISDN+"  and  Cust Type"+((ManagedMSISDNDataVO) relatedList.get(j)).getCustomertype());
						if (((ManagedMSISDNDataVO) relatedList.get(j)).getCustomertype().equals(ConstantIfc.CUSTOMER_PLAN_TYPE_PRE)){
							((ManagedWimaxDataVO) managedList.get(i)).setType(ConstantIfc.CUSTOMER_PREPAID_TYPE+"");
						} else if (((ManagedMSISDNDataVO) relatedList.get(j)).getCustomertype().equals(ConstantIfc.CUSTOMER_PLAN_TYPE_POST)){
							((ManagedWimaxDataVO) managedList.get(i)).setType(ConstantIfc.CUSTOMER_POSTPAID_TYPE+"");
						}
					}
					
					break;
				}
			}
			
			if (validNumber) {
				filteredManagedList.add(managedList.get(i));
       		}
		}
		
		log.debug("ManageCreditCardBO > filterManagedList > Done ");
		return filteredManagedList;
	}
	
	public boolean pinRequest(CreditCardPaymentRequestVO requestVO) {
		log.debug("ManageCreditCardBO > pinRequest > called");
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
        
        boolean returnFlag = true;
        
        // Check for valid record in DB
        boolean isValidRecord = manageCreditCardDAO.isValidRecord(requestVO);
        
        if(isValidRecord) {
        	log.debug("ManageCreditCardBO > pinRequest > valid record is already present in DB for billing account number::"+ requestVO.getBillAccountNumber()+" Don't send SMS again");
        } else {
        	log.debug("ManageCreditCardBO > pinRequest > NO valid record in DB for billing account number::"+ requestVO.getBillAccountNumber());
        	
        	try {
            	// Clear the invalid Record from DB
        		manageCreditCardDAO.clearInvalidRecord(requestVO);
        		
        		String pin =  RandomGenerator.generateNumericRandom();
        		requestVO.setTransactionPIN(pin);
            	
            	// insert the record in DB
        		manageCreditCardDAO.insertTransactionPIN(requestVO);
            	
            	// Send SMS to the Customer
            	String messageId = CREDIT_CARD_PIN_SMS_ID_EN;
            	if("A".equalsIgnoreCase(requestVO.getLanguage())){
            		messageId = CREDIT_CARD_PIN_SMS_ID_AR;
            	}
            	String[] msgParams = new String[1];
            	msgParams[0] = pin;
            	log.debug("ManageCreditCardBO > pinRequest > Sending PIN for billing account number::"+ requestVO.getBillAccountNumber()+ " and the CAP contact Number::"+requestVO.getSmsTo()+" STARTED");
            	SMSSender.sendSMS(requestVO.getSmsTo(), messageId, msgParams);
            	log.debug("ManageCreditCardBO > pinRequest > Sending PIN for billing account number::"+ requestVO.getBillAccountNumber()+ " and the CAP contact Number::"+requestVO.getSmsTo()+" ENDED");
        		
        	} catch (Exception e) {
        		log.fatal("ManageCreditCardBO > pinRequest > Exception::"+e.getMessage());
        		returnFlag = false;
			}
        }
		return returnFlag;
	}
	
	public boolean validateActivationCode(CreditCardPaymentRequestVO requestVO) {
		log.debug("ManageCreditCardBO > validateActivationCode > called");
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
        
        // Check for valid record in DB
        boolean isValidActivationCode = manageCreditCardDAO.isValidActivationCode(requestVO);
        
        log.debug("ManageCreditCardBO > validateActivationCode > isValidActivationCode for billing account number::"+ requestVO.getBillAccountNumber()+" is::"+isValidActivationCode);

        return isValidActivationCode;
	}
	
	public CreditCardPaymentReplyVO capCreditCardCancellation(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO) throws MobilyCommonException {
		log.debug("ManageCreditCardBO > capCreditCardCancellation > called");
		
		CreditCardPaymentReplyVO objCreditCardPaymentReplyVO = null;
		
		objCreditCardPaymentReplyVO = new ManageCreditCardBO().manageCreditCards(objCreditCardPaymentRequestVO,ConstantIfc.Requestor_Channel_Function_Cancellation);
		
        return objCreditCardPaymentReplyVO;
	}	
	
	public CreditCardPaymentReplyVO capCreditCardPayment(CreditCardPaymentRequestVO objCreditCardPaymentRequestVO) throws MobilyCommonException {
		log.debug("ManageCreditCardBO > capCreditCardPayment > called");
		
		CreditCardPaymentReplyVO objCreditCardPaymentReplyVO = null;
		
		boolean isValidPINCode = validateActivationCode(objCreditCardPaymentRequestVO);
		
		if(isValidPINCode) {
			log.debug("ManageCreditCardBO > PIN is valid for Billing Account Number::"+objCreditCardPaymentRequestVO.getBillAccountNumber()+ " Payment Process STARTED");
			objCreditCardPaymentReplyVO = new ManageCreditCardBO().manageCreditCards(objCreditCardPaymentRequestVO,ConstantIfc.Requestor_Channel_Function_Payment);
		} else {
			log.debug("ManageCreditCardBO > PIN is invalid for Billing Account Number::"+objCreditCardPaymentRequestVO.getBillAccountNumber()+ " Send Error Message");
			throw new MobilyCommonException("1000");
		}

    	return objCreditCardPaymentReplyVO;
	}
	
	public void clearInvalidRecord(CreditCardPaymentRequestVO requestVO) {
		log.debug("ManageCreditCardBO > clearInvalidRecord > called");
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
		try {
			manageCreditCardDAO.clearInvalidRecord(requestVO);
		} catch (Exception e) {
			log.debug("ManageCreditCardBO > clearInvalidRecord:: exception while clearing the PIN code from DB");
		}
		
	}
	
	public boolean resendPIN(CreditCardPaymentRequestVO requestVO) {
		log.debug("ManageCreditCardBO > resendPIN > called");
		DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.ORACLE);
		ManageCreditCardDAO manageCreditCardDAO = daoFactory.getManageCreditCardDAO();
        
        boolean returnFlag = true;
        
        // Check for valid record in DB
        String pinCode = manageCreditCardDAO.getValidPINCode(requestVO);
        
        if(!FormatterUtility.isEmpty(pinCode)) {
        	log.debug("ManageCreditCardBO > resendPIN > valid record is present in DB for billing account number::"+ requestVO.getBillAccountNumber()+" Resend SMS again");
        	try {
            	
            	// Send SMS to the Customer
            	String messageId = CREDIT_CARD_PIN_SMS_ID_EN;
            	if("A".equalsIgnoreCase(requestVO.getLanguage())) {
            		messageId = CREDIT_CARD_PIN_SMS_ID_AR;
            	}
            	String[] msgParams = new String[1];
            	msgParams[0] = pinCode;
            	log.debug("ManageCreditCardBO > resendPIN > Sending PIN for billing account number::"+ requestVO.getBillAccountNumber()+ " and the CAP contact Number::"+requestVO.getSmsTo()+" STARTED");
            	SMSSender.sendSMS(requestVO.getSmsTo(), messageId, msgParams);
            	log.debug("ManageCreditCardBO > resendPIN > Sending PIN for billing account number::"+ requestVO.getBillAccountNumber()+ " and the CAP contact Number::"+requestVO.getSmsTo()+" ENDED");
        		
        	} catch (Exception e) {
        		log.fatal("ManageCreditCardBO > resendPIN > Exception::"+e.getMessage());
        		returnFlag = false;
			}
        } else {
        	log.debug("ManageCreditCardBO > resendPIN > NO valid record in DB for billing account number::"+ requestVO.getBillAccountNumber());
        	returnFlag = false;

        }
		return returnFlag;
	}
}
