/*
 * Created on 03/12/2007
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;

import java.util.Date;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;


/**
 * @author ysawy
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SkyUserAccountVO extends BaseVO
{
    private String msisdn;
    private int    skyType;
    private int    skyEmailStatus = ConstantIfc.SKY_EMAIL_STATUS_UNDEFINED;
    private Date   skyActionTime;
    private int    skyActionReason;
    private Date   skyTOSTime;
    private Date   skyCreateScheduleTime;
    //This is to contain the expected time for suspending grace emails
    private Date   skyScheduledDate;

    public String getMsisdn()
    {
        return msisdn;
    }
    public void setMsisdn(String msisdn)
    {
        this.msisdn = msisdn;
    }
    public int getSkyActionReason()
    {
        return skyActionReason;
    }
    public void setSkyActionReason(int skyActionReason)
    {
        this.skyActionReason = skyActionReason;
    }
    public Date getSkyActionTime()
    {
        return skyActionTime;
    }
    public void setSkyActionTime(Date skyActionTime)
    {
        this.skyActionTime = skyActionTime;
    }
    public Date getSkyCreateScheduleTime()
    {
        return skyCreateScheduleTime;
    }
    public void setSkyCreateScheduleTime(Date skyCreateScheduleTime)
    {
        this.skyCreateScheduleTime = skyCreateScheduleTime;
    }
    public int getSkyEmailStatus()
    {
        return skyEmailStatus;
    }
    public void setSkyEmailStatus(int skyEmailStatus)
    {
        this.skyEmailStatus = skyEmailStatus;
    }
    public Date getSkyTOSTime()
    {
        return skyTOSTime;
    }
    public void setSkyTOSTime(Date skyTOSTime)
    {
        this.skyTOSTime = skyTOSTime;
    }
    public int getSkyType()
    {
        return skyType;
    }
    public void setSkyType(int skyType)
    {
        this.skyType = skyType;
    }
    public Date getSkyScheduledDate()
    {
        return skyScheduledDate;
    }
    public void setSkyScheduledDate(Date skyScheduledDate)
    {
        this.skyScheduledDate = skyScheduledDate;
    }
}
