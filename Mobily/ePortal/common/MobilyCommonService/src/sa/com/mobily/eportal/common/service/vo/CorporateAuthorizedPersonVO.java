/*
 * Created on Nov 18, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;



/**
 * @author aghareeb
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CorporateAuthorizedPersonVO extends BaseVO {

	private String name;

	private String bithDate;

	private String idDocType;

	private String gender;

	private String nationality;

	private String idDocNo;

	private CorporateAuthorizedPersonAddressVO corporateAuthorizedPersonAddressVO;

	/**
	 * @return Returns the bithDate.
	 */
	public String getBithDate() {
		return bithDate;
	}

	/**
	 * @param bithDate
	 *            The bithDate to set.
	 */
	public void setBithDate(String bithDate) {
		this.bithDate = bithDate;
	}

	/**
	 * @return Returns the corporateAuthorizedPersonAddressVO.
	 */
	public CorporateAuthorizedPersonAddressVO getCorporateAuthorizedPersonAddressVO() {
		return corporateAuthorizedPersonAddressVO;
	}

	/**
	 * @param corporateAuthorizedPersonAddressVO
	 *            The corporateAuthorizedPersonAddressVO to set.
	 */
	public void setCorporateAuthorizedPersonAddressVO(
			CorporateAuthorizedPersonAddressVO corporateAuthorizedPersonAddressVO) {
		this.corporateAuthorizedPersonAddressVO = corporateAuthorizedPersonAddressVO;
	}

	/**
	 * @return Returns the gender.
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            The gender to set.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return Returns the idDocNo.
	 */
	public String getIdDocNo() {
		return idDocNo;
	}

	/**
	 * @param idDocNo
	 *            The idDocNo to set.
	 */
	public void setIdDocNo(String idDocNo) {
		this.idDocNo = idDocNo;
	}

	/**
	 * @return Returns the idDocType.
	 */
	public String getIdDocType() {
		return idDocType;
	}

	/**
	 * @param idDocType
	 *            The idDocType to set.
	 */
	public void setIdDocType(String idDocType) {
		this.idDocType = idDocType;
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return Returns the nationality.
	 */
	public String getNationality() {
		return nationality;
	}

	/**
	 * @param nationality
	 *            The nationality to set.
	 */
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
}
