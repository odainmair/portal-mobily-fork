/**
 * 
 */
package sa.com.mobily.eportal.common.service.xml;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.BillVO;
import sa.com.mobily.eportal.common.service.vo.BillingAccountVO;
import sa.com.mobily.eportal.common.service.vo.CutServiceSubAccountVO;
import sa.com.mobily.eportal.common.service.vo.EditAttributeVO;

/**
 * @author s.vathsavai.mit
 *
 */
public class CorporateRevampXMLHandler {
	
	 private static final Logger log = LoggerInterface.log;
	 
	 /**
		 * responsible for generate xml header tag , this method is generate header tag thats common for all royalty request
		 * @param doc
		 * @param requestVO
		 * @return org.w3c.dom.Element
		 */
		private  static Element generateHeaderRequest(Document doc,String funcID ) {
			log.debug("CorporateRevampXMLHandler > generateHeaderRequest :: start");
			Element header = null;
			try{
				String srdate = FormatterUtility.FormateDate(new Date());
				header = XMLUtility.openParentTag(doc,TagIfc.TAG_SR_BK_HEADER);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,funcID);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SECURITY_KEY,"");
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SERVICE_REQUEST_ID,ConstantIfc.SERVICE_REQUESTOR_ID+srdate);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_MSG_VERSION,ConstantIfc.MSG_VER_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_REQUESTOR_CHANNEL_ID,ConstantIfc.CHANNEL_ID_VALUE);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_SR_DATE,srdate);
				XMLUtility.generateElelemt(doc, header, TagIfc.TAG_BACKEND_CHANNEL,ConstantIfc.BACKEND_CHANNEL_VALUE);
				
			}catch(Exception e){
				log.fatal("CorporateRevampXMLHandler > generateHeaderRequest :: Exception "+e);
				throw new SystemException(e);
			}
			log.debug("CorporateRevampXMLHandler > generateHeaderRequest :: end");
			return header;
		}
	 
		/**
		 * <MOBILY_BSL_SR>
			      <SR_BK_HEADER>
			            <FuncId>AliasNameUpdate</FuncId>
			            <SecurityKey></SecurityKey>
			            <ServiceRequestId> TEST_SR_7075836</ServiceRequestId>
			            <MsgVersion>0000</MsgVersion>
			            <RequestorChannelId> BSL</RequestorChannelId>
			            <SrDate>20090630195109</SrDate>
			            <BackendChannel>NETWORK</BackendChannel>
			      </SR_BK_HEADER>
			      <AccountNumber></AccountNumber>
			      <AliasName> </AliasName>
			      <Email></Email>
			</MOBILY_BSL_SR>
		 */
		public String generateUpdateAliasNameXMLRequest(EditAttributeVO attributeVO) {
			String xmlRequest = "";
			log.debug("CorporateRevampXMLHandler > generateUpdateAliasNameXMLRequest :: start");
			try {
				Document doc = new DocumentImpl();
		
				Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
				Element header = generateHeaderRequest(doc,ConstantIfc.FUNC_ID_ALIAS_NAME_UPDATE);
				XMLUtility.closeParentTag(root, header);
				
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ACCOUNT_NUMBER,attributeVO.getAccountNumber());
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ALIAS_NAME,attributeVO.getAttributeValue());
				XMLUtility.generateElelemt(doc, root, TagIfc.TAG_EMAIL,attributeVO.getEmail());
				if(FormatterUtility.isNotEmpty(attributeVO.getPackageClassification()))
					XMLUtility.generateElelemt(doc, root, TagIfc.TAG_PACKAGE_CLASSIFICATION,attributeVO.getPackageClassification());
				
				doc.appendChild(root);
				xmlRequest = XMLUtility.serializeRequest(doc);
				log.debug("CorporateRevampXMLHandler > generateUpdateAliasNameXMLRequest > XML generated....");
			} catch (Exception e) {
				log.fatal("LCorporateRevampXMLHandler > generateUpdateAliasNameXMLRequest :: Exception ,"+ e);
				throw new SystemException(e);
			}
			
			log.debug("CorporateRevampXMLHandler > generateUpdateAliasNameXMLRequest :: end");
			return xmlRequest;
		}

		/**
		 * <MOBILY_BSL_SR_REPLY>
		      <SR_BK_HEADER_Reply>
		            <FuncID>AliasNameUpdate</FuncID>
		            <SecurityKey />
		            <ServiceRequestId>SR_1417310438</ServiceRequestId>
		            <MsgVersion>0000</MsgVersion>
		            <RequestorChannelId>ePortal</RequestorChannelId>
		            <SrDate>20091030214708</SrDate>
		            <BackendChannel>NETWORK</BackendChannel>
		            <SrRcvDate>20091030214808</SrRcvDate>
		            <SrStatus>1</SrStatus>
		      </SR_BK_HEADER_Reply>
		      <ErrorCode>0</ErrorCode>
		      <ErrorMsg></ErrorMsg>
			</MOBILY_BSL_SR_REPLY>
		 * @param xmlReply
		 */
		public EditAttributeVO parseUpdateAliasNameXMLReply(String xmlReply,EditAttributeVO attributeVO) {
			log.info("CorporateRevampXMLHandler > parseUpdateAliasNameXMLReply :: start ");
			
			
			try {
				if (xmlReply == null || xmlReply == "")
					throw new SystemException();

				Document doc = XMLUtility.parseXMLString(xmlReply);
				
				if(doc != null) {
					NodeList nodeList = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY);
					if(nodeList != null && nodeList.getLength() > 0) {
						Node rootNode = nodeList.item(0);
						NodeList nl = rootNode.getChildNodes();
						for (int j = 0; j < nl.getLength(); j++) {
							if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
								continue;
							
							Element l_Node = (Element) nl.item(j);
							String p_szTagName = l_Node.getTagName();
							String l_szNodeValue = null;
							if (l_Node.getFirstChild() != null)
								l_szNodeValue = l_Node.getFirstChild().getNodeValue();
							
							if(TagIfc.TAG_ERROR_CODE.equalsIgnoreCase(p_szTagName)) {
								log.debug("CorporateRevampXMLHandler > parseUpdateAliasNameXMLReply :: error code ="+l_szNodeValue);
								attributeVO.setErrorCode(l_szNodeValue);
							}
							if(TagIfc.TAG_ERROR_MSG.equalsIgnoreCase(p_szTagName)) {
								log.debug("CorporateRevampXMLHandler > parseUpdateAliasNameXMLReply :: error msg="+l_szNodeValue);
								attributeVO.setErrorMessage(l_szNodeValue);
							}
						}
					}
				}
			}catch (Exception e) {
				log.fatal("CorporateRevampXMLHandler > parseUpdateAliasNameXMLReply :: Exception :: "	+ e);
				throw new SystemException(e);
			}
			log.info("CorporateRevampXMLHandler > parseUpdateAliasNameXMLReply :: end ");
			return attributeVO;
		}
	
	/**
	 * This method is used to parse the corporate inquiry xml reply.
	 * @param xmlReply
	 * @return BillingAccountVO
	 */
	public BillingAccountVO parseCorporateInquiryReply(String xmlReply) {
		log.info("CorporateRevampXMLHandler > parseCorporateInquiryReply :: start ");
		
		BillingAccountVO billingAccountVO = new BillingAccountVO();
		
		try {
			if (xmlReply == null || xmlReply == "")
				throw new SystemException();

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			if(doc != null) {
				NodeList nodeList = doc.getElementsByTagName(TagIfc.TAG_MOBILY_BSL_SR_REPLY);
				if(nodeList != null && nodeList.getLength() > 0) {
					Node rootNode = nodeList.item(0);
					NodeList nl = rootNode.getChildNodes();
					
					int nlSize = nl.getLength();
					Element node = null;
					String tagName = null;
					for (int j = 0; j < nlSize; j++) {
						if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
							continue;
						
						node = (Element) nl.item(j);
						tagName = node.getTagName();
						//log.debug(tagName);
						if(TagIfc.TAG_ERROR_CODE.equalsIgnoreCase(tagName)) {
							int errorCode = 0;
							if(node.getFirstChild() != null) 
								errorCode = Integer.parseInt(node.getFirstChild().getNodeValue());
							log.debug("CorporateRevampXMLHandler > parseCorporateInquiryReply ::errorCode = "+errorCode);
							if(errorCode > 0)
								throw new MobilyCommonException(node.getFirstChild().getNodeValue());
						}else if(TagIfc.TAG_LIST_OF_BILLING_ACCOUNT.equalsIgnoreCase(tagName)) {
							NodeList nl1 = node.getChildNodes();
							//log.debug("childs of list of blac "+nl1.getLength());
							int size = nl1.getLength();
							Element node1 = null;
							String tagName1 = null;
							for (int i = 0; i < size; i++) {
								if ((nl1.item(i)).getNodeType() != Node.ELEMENT_NODE)
									continue;
								node1 = (Element) nl1.item(i);
								tagName1 = node1.getTagName();
								//log.debug(tagName1);
								
								if(TagIfc.TAG_BILLING_ACCOUNT.equalsIgnoreCase(tagName1)) {
									billingAccountVO = parseBillingAccount(billingAccountVO,node1);
								}
							}
						}
						
					}
				}
			}
			
		}catch (Exception e) {
			log.fatal("CorporateRevampXMLHandler > parseCorporateInquiryReply :: Exception :: "	+ e);
			throw new SystemException(e);
		}
		log.info("CorporateRevampXMLHandler > parseCorporateInquiryReply :: end ");
		return billingAccountVO;
	}
	
	
	public BillingAccountVO parseBillingAccount(BillingAccountVO billingAccountVO,Element billingAccNode) {
		
		log.info("CorporateRevampXMLHandler > parseBillingAccount :: start");
		List billingAccountVOList = new ArrayList();
		
		if(billingAccNode != null) {
			NodeList  contentNode = billingAccNode.getChildNodes();
			int size = contentNode.getLength();
			Element node = null;
			String nodeTagName = null;
			for(int i=0; i< size; i++) {
				if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				node = (Element) contentNode.item(i);
				nodeTagName = node.getTagName();
				//log.debug(nodeTagName);
				if(TagIfc.TAG_ACCOUNT_NUMBER.equalsIgnoreCase(nodeTagName)) {
					if(node != null && node.getFirstChild() != null)
						billingAccountVO.setServiceAccNumber(node.getFirstChild().getNodeValue());
					log.debug("billingAccountVO.setServiceAccNumber"+billingAccountVO.getServiceAccNumber());
				}else if(TagIfc.TAG_NAME.equalsIgnoreCase(nodeTagName)) {
					if(node != null && node.getFirstChild() != null)
						billingAccountVO.setName(node.getFirstChild().getNodeValue());
					log.debug("billingAccountVO.setName = "+billingAccountVO.getName());
				}else if(TagIfc.TAG_BILLS_INFO.equalsIgnoreCase(nodeTagName)) {
					billingAccountVO.setBillVOList(parseBillsInfo(node));
					log.debug("billVOList.size() == "+billingAccountVO.getBillVOList().size());
				}else if(TagIfc.TAG_PRODUCT_TYPE.equalsIgnoreCase(nodeTagName)) {
					if(node != null && node.getFirstChild() != null)
						billingAccountVO.setProductType(node.getFirstChild().getNodeValue());
					log.debug("product type = "+billingAccountVO.getProductType());
				}else if(TagIfc.TAG_NETWORK_CONFIGURATION.equalsIgnoreCase(nodeTagName)) {
					if(node != null && node.getFirstChild() != null)
						billingAccountVO.setNetworkConfiguration(node.getFirstChild().getNodeValue());
					log.debug("newwork configuration = "+billingAccountVO.getNetworkConfiguration());
				}else if(TagIfc.TAG_KAM.equalsIgnoreCase(nodeTagName)) {
					if(node != null && node.getFirstChild() != null)
						billingAccountVO.setKamEmail(node.getFirstChild().getNodeValue());
					log.debug("KamEmail = "+billingAccountVO.getKamEmail());
				}else if(TagIfc.TAG_BILLING_ACCOUNT.equalsIgnoreCase(nodeTagName)) {
					billingAccountVOList.add(parseBillingAccount(new BillingAccountVO(),node));
				}else if(TagIfc.TAG_LIST_OF_SERVICE_SUB_ACCOUNTS.equalsIgnoreCase(nodeTagName)) {
					billingAccountVO.setCutServiceSubAccVOList(parseListCutServiceSubSccounts(node));
				}
			}
			log.debug("billingAccountVOList size....."+billingAccountVOList.size());
			
			billingAccountVO.setBillingAccountVOList(billingAccountVOList);
		}
		log.info("CorporateRevampXMLHandler > parseBillingAccount :: end");
		return billingAccountVO;
	}
	
	
	//node is bills info tag node
	public List parseBillsInfo(Element billsInfoNode) {
		
		log.info("CorporateRevampXMLHandler > parseBillsInfo :: start");
		List billVOList = new ArrayList();
		if(billsInfoNode != null) {
			NodeList  contentNode = billsInfoNode.getChildNodes();
			
			int size = contentNode.getLength();
			Element node = null;
			String nodeTagName = null;
			//BillVO billVO = null;
			Element billNode = null;
			for(int i=0; i< size; i++) {
				if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				node = (Element) contentNode.item(i);
				nodeTagName = node.getTagName();
				if(TagIfc.TAG_BILLS.equalsIgnoreCase(nodeTagName)) {
					
					NodeList  billNodes = node.getChildNodes();
					for(int j=0; j< billNodes.getLength(); j++) {
						if ((billNodes.item(j)).getNodeType() != Node.ELEMENT_NODE)
							continue;
						
						billNode = (Element) billNodes.item(j);
						//billVO = parseBillNode(billNode);
						billVOList.add(parseBillNode(billNode));
					}
				}
			}
		}
		log.info("CorporateRevampXMLHandler > parseBillsInfo :: end");
		return billVOList;
	}
	
	public BillVO parseBillNode(Element billNode) {
		
		BillVO billVO = new BillVO();
		log.info("CorporateRevampXMLHandler > parseBillNode :: start");
		if(billNode != null) {
			NodeList  contentNode = billNode.getChildNodes();
			if(contentNode != null) {
				int size = contentNode.getLength();
				Element node = null;
				String nodeTagName = null;
				
				for(int i=0; i< size; i++) {
					if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
						continue;
					
					node = (Element) contentNode.item(i);
					nodeTagName = node.getTagName();
					
					if(TagIfc.TAG_BILL_NUMBER.equalsIgnoreCase(nodeTagName))
						billVO.setBillNumber(node.getFirstChild().getNodeValue());
					else if(TagIfc.TAG_BILL_END_DATE.equalsIgnoreCase(nodeTagName))
						billVO.setBillEndDate(node.getFirstChild().getNodeValue());
				}
			}
		}
		log.info("CorporateRevampXMLHandler > parseBillNode :: end");
		return billVO;
	}
	
	public List parseListCutServiceSubSccounts(Element listCutServiceSubAccNode) {
		
		List cutServiceSubAccVOList = new ArrayList();
		log.info("CorporateRevampXMLHandler > parseListCutServiceSubSccounts :: start");
		if(listCutServiceSubAccNode != null) {
			NodeList  contentNode = listCutServiceSubAccNode.getChildNodes();
			Element node = null;
			String nodeTagName = null;
			int size = contentNode.getLength();
			//CutServiceSubAccountVO serviceSubAccountVO = null;
			for(int i=0; i< size; i++) {
				if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				node = (Element) contentNode.item(i);
				nodeTagName = node.getTagName();
				
				if(TagIfc.TAG_CUT_SERVICE_SUB_ACCOUNTS.equalsIgnoreCase(nodeTagName)) {
					//serviceSubAccountVO = parseCutServiceSubAccount(node);
					cutServiceSubAccVOList.add(parseCutServiceSubAccount(node));
				}
			}
			
			log.debug("cutServiceSubAccVOList size = "+cutServiceSubAccVOList.size());
		}
		log.info("CorporateRevampXMLHandler > parseListCutServiceSubSccounts :: end");
		return cutServiceSubAccVOList;
	}
	
	
	public CutServiceSubAccountVO parseCutServiceSubAccount(Element cutServiceSubAccNode) {
		
		
		log.info("CorporateRevampXMLHandler > parseCutServiceSubAccount :: start");
		
		CutServiceSubAccountVO serviceSubAccountVO = new CutServiceSubAccountVO();
		if(cutServiceSubAccNode != null) {
			NodeList  contentNode = cutServiceSubAccNode.getChildNodes();
			if(contentNode != null) {
				Element node = null;
				String nodeTagName = null;
				int size = contentNode.getLength();
				for(int i=0; i< size; i++) {
					if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
						continue;
					
					node = (Element) contentNode.item(i);
					nodeTagName = node.getTagName();
					if(TagIfc.TAG_ACCOUNT_NUMBER.equalsIgnoreCase(nodeTagName)) {
						if(node.getFirstChild() != null)
							serviceSubAccountVO.setServiceAccNumber(node.getFirstChild().getNodeValue());
						log.debug("serviceSubAccountVO.setServiceAccNumber = "+serviceSubAccountVO.getServiceAccNumber());
						
					}else if(TagIfc.TAG_BILLS_INFO.equalsIgnoreCase(nodeTagName)) {
						//ArrayList billVOList = parseBillsInfo(node);
						serviceSubAccountVO.setBillVOList(parseBillsInfo(node));
						//log.debug("billVOList.size() in cut service asub account == "+billVOList.size());
					}else if(TagIfc.TAG_VPN_ID.equalsIgnoreCase(nodeTagName)) {
						if(node.getFirstChild() != null)
							serviceSubAccountVO.setVpnID(node.getFirstChild().getNodeValue());
						log.debug("serviceSubAccountVO.getVpnID = "+serviceSubAccountVO.getVpnID());
					}else if(TagIfc.TAG_PACKAGE_NAME.equalsIgnoreCase(nodeTagName)) {
						if(node.getFirstChild() != null)
							serviceSubAccountVO.setPackageName(node.getFirstChild().getNodeValue());
						log.debug("serviceSubAccountVO.getPackageName = "+serviceSubAccountVO.getPackageName());
						
					}else if(TagIfc.TAG_NAME.equalsIgnoreCase(nodeTagName)) {
						if(node.getFirstChild() != null)
							serviceSubAccountVO.setName(node.getFirstChild().getNodeValue());
						log.debug("serviceSubAccountVO.setName = "+serviceSubAccountVO.getName());
					}else if(TagIfc.TAG_ALIAS.equalsIgnoreCase(nodeTagName)) {
						if(node.getFirstChild() != null)
							serviceSubAccountVO.setAliasName(node.getFirstChild().getNodeValue());
						log.debug("serviceSubAccountVO.setAliasName = "+serviceSubAccountVO.getAliasName());
					}else if(TagIfc.TAG_ITEMS.equalsIgnoreCase(nodeTagName)) {
						log.debug("parsing items..starting");
						Map itemsMap = parseItems(node,serviceSubAccountVO);
						serviceSubAccountVO.setItemsMap(itemsMap);
						log.debug("items map = "+serviceSubAccountVO.getItemsMap());
						log.debug("service sub account status = "+serviceSubAccountVO.getStatus());
					}
				}
			}
		}
		
		log.info("CorporateRevampXMLHandler > parseCutServiceSubAccount :: end");
		return serviceSubAccountVO;
	}
	
	
	public Map parseItems(Element itemsNode,CutServiceSubAccountVO serviceSubAccountVO) {
		log.info("CorporateRevampXMLHandler > parseItems :: start");
		Map itemMap = new HashMap();
		Map attributeMap = null;
		if(itemsNode != null) {
			NodeList  itemNodes = itemsNode.getChildNodes();
			Element itemNode = null;
			String itemNodeTagName = null;
			int itemNodesSize = itemNodes.getLength();
			for(int index=0; index< itemNodesSize; index++) {
				if ((itemNodes.item(index)).getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				itemNode = (Element) itemNodes.item(index);
				itemNodeTagName = itemNode.getTagName();
				
				if(TagIfc.TAG_ITEM.equalsIgnoreCase(itemNodeTagName)) {
					NodeList  contentNode = itemNode.getChildNodes();
					Element node = null;
					String nodeTagName = null;
					int size = contentNode.getLength();
					String itemName = "";
					String status = "";
					for(int i=0; i< size; i++) {
						if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
							continue;
						
						node = (Element) contentNode.item(i);
						nodeTagName = node.getTagName();
						
						if(TagIfc.TAG_ITEM_NAME.equalsIgnoreCase(nodeTagName)) {
							if(node.getFirstChild() != null)
								itemName = node.getFirstChild().getNodeValue();
							log.debug("CorporateRevampXMLHandler > parseItems :: itemName = "+itemName);
						}else if(TagIfc.TAG_STATUS.equalsIgnoreCase(nodeTagName)) {
							if(ConstantIfc.ITEM_NAME_DIA.equalsIgnoreCase(itemName) || ConstantIfc.ITEM_NAME_ETHERNET.equalsIgnoreCase(itemName) || ConstantIfc.ITEM_NAME_IPVPN.equalsIgnoreCase(itemName) || ConstantIfc.ITEM_NAME_DIA_BACKUP.equalsIgnoreCase(itemName) || ConstantIfc.ITEM_NAME_ETHERNET_BACKUP.equalsIgnoreCase(itemName) || ConstantIfc.ITEM_NAME_IPVPN_BACKUP.equalsIgnoreCase(itemName) ) { 
								if(node.getFirstChild() != null)
									status = node.getFirstChild().getNodeValue();
								serviceSubAccountVO.setStatus(status);
								log.debug("CorporateRevampXMLHandler > parseItems :: status = "+status);
							}
						}else if(TagIfc.TAG_ATTRIBUTES.equalsIgnoreCase(nodeTagName)) {
							attributeMap = new HashMap();
//							if(ConstantsIfc.ITEM_NAME_DIA.equalsIgnoreCase(itemName) || ConstantsIfc.ITEM_NAME_ETHERNET.equalsIgnoreCase(itemName) || ConstantsIfc.ITEM_NAME_IPVPN.equalsIgnoreCase(itemName) ) {
								NodeList  attributeNodes = node.getChildNodes();
								int attrSize = attributeNodes.getLength();
								Element attributeNode = null;
								String attributeNodeTagName = null;
								for(int j=0; j< attrSize; j++) {
									if ((attributeNodes.item(j)).getNodeType() != Node.ELEMENT_NODE)
										continue;
									
									attributeNode = (Element) attributeNodes.item(j);
									attributeNodeTagName = attributeNode.getTagName();
									
									if(TagIfc.TAG_ATTRIBUTE.equalsIgnoreCase(attributeNodeTagName)) {
										NodeList  attrChildNodes = attributeNode.getChildNodes();
										int attrChildNodesSize = attrChildNodes.getLength();
										String attrName = "";
										String attrValue = "";
										for(int k=0; k< attrChildNodesSize; k++) {
											if ((attrChildNodes.item(k)).getNodeType() != Node.ELEMENT_NODE)
												continue;
											
											node = (Element) attrChildNodes.item(k);
											nodeTagName = node.getTagName();
											
											
											if(TagIfc.TAG_NAME.equalsIgnoreCase(nodeTagName)) {
												if(node.getFirstChild() != null)
													attrName = node.getFirstChild().getNodeValue();
											}else if(TagIfc.TAG_TEXT_VALUE.equalsIgnoreCase(nodeTagName)) {
												if(node.getFirstChild() != null)
													attrValue = node.getFirstChild().getNodeValue();
												attributeMap.put(attrName,attrValue);
											}
										}
									}
								}
								itemMap.put(itemName, attributeMap);
								log.debug("CorporateRevampXMLHandler > parseItems :: attributes map = "+attributeMap);
								
//							}
							
//							else if(ConstantsIfc.ITEM_NAME_MANAGE_CONNECTIVITY.equalsIgnoreCase(itemName)) {
//								manageConItemExist = true;
//								itemMap.put("Managed Router","Active");
//								routersList = new ArrayList();
//								routersList.add("Cisco 871 Security Bundle with Advanced IP Services");
//								routersList.add("Cisco 881 Ethernet Sec Router w/ Adv IP Services");
//								routersList.add("Cisco 881 Ethernet Sec Router w/ Adv IP Services");
//							}else if(routersList != null && routersList.contains(itemName)) {
//								itemMap.put(itemName,itemName);
//							}else if(ConstantsIfc.ITEM_NAME_SLA.equalsIgnoreCase(itemName)) {
//								NodeList  attributeNodes = node.getChildNodes();
//								int attrSize = attributeNodes.getLength();
//								Element attributeNode = null;
//								String attributeNodeTagName = null;
//								for(int j=0; j< attrSize; j++) {
//									if ((attributeNodes.item(j)).getNodeType() != Node.ELEMENT_NODE)
//										continue;
//									
//									attributeNode = (Element) attributeNodes.item(j);
//									attributeNodeTagName = node.getTagName();
//									if(TagsIfc.TAG_ATTRIBUTE.equalsIgnoreCase(attributeNodeTagName)) {
//										NodeList  attrChildNodes = attributeNode.getChildNodes();
//										int attrChildNodesSize = attrChildNodes.getLength();
//										String attrName = "";
//										String attrValue = "";
//										for(int k=0; k< attrChildNodesSize; k++) {
//											if ((attrChildNodes.item(k)).getNodeType() != Node.ELEMENT_NODE)
//												continue;
//											
//											node = (Element) attrChildNodes.item(k);
//											nodeTagName = node.getTagName();
//											
//											if(TagsIfc.TAG_NAME.equalsIgnoreCase(nodeTagName)) {
//												if(node.getFirstChild() != null)
//													attrName = node.getFirstChild().getNodeValue();
//											}else if(TagsIfc.TAG_TEXT_VALUE.equalsIgnoreCase(nodeTagName)) {
//												if(node.getFirstChild() != null)
//													attrValue = node.getFirstChild().getNodeValue();
//												
//												if("SLA".equalsIgnoreCase(attrName)) {
//													itemMap.put("SLA tier",attrValue);
//												}
//											}
//										}
//									}
//								}
//								
//							}
						}
						
					}
				}
				
			}
		}
		
		log.info("CorporateRevampXMLHandler > parseItems :: end");
		return itemMap;
	}
	
	
	public void parseItem(Element itemNode,Map itemMap) {
		if(itemNode != null) {
			
		}
	}
	
	
	public void parserAttribute(Element attributeNode,String attrName) {
		
		if(attributeNode != null) {
			NodeList  contentNode = attributeNode.getChildNodes();
			Element node = null;
			String nodeTagName = null;
			int size = contentNode.getLength();
			for(int i=0; i< size; i++) {
				if ((contentNode.item(i)).getNodeType() != Node.ELEMENT_NODE)
					continue;
				
				node = (Element) contentNode.item(i);
				nodeTagName = node.getTagName();
				
			}
		}
	}
	
	/**
		 * <MOBILY_BSL_SR>
		      <SR_BK_HEADER>
		            <FuncId>CORP_INQ</FuncId>
		            <SecurityKey></SecurityKey>
		            <ServiceRequestId> TEST_SR_7075836</ServiceRequestId>
		            <MsgVersion>0000</MsgVersion>
		            <RequestorChannelId> BSL</RequestorChannelId>
		            <SrDate>20090630195109</SrDate>
		            <BackendChannel>NETWORK</BackendChannel>
		      </SR_BK_HEADER>
	      <AccountNumber></AccountNumber>
	</MOBILY_BSL_SR>

	 * @return
	 */
	public String generateCorporateInquiryXMLRequest(String billingAccountNumber) {
		
		String xmlRequest = "";
		log.debug("CorporateRevamp > CorporateRevampXMLHandler > generateCorporateInquiryXMLRequest :: start");
		try {
			Document doc = new DocumentImpl();
	
			Element root = doc.createElement(TagIfc.TAG_MOBILY_BSL_SR);
			Element header = generateHeaderRequest(doc,ConstantIfc.FUNC_ID_CORPORATE_INQUIRY);
			XMLUtility.closeParentTag(root, header);
			
			XMLUtility.generateElelemt(doc, root, TagIfc.TAG_ACCOUNT_NUMBER,billingAccountNumber);
			doc.appendChild(root);
			xmlRequest = XMLUtility.serializeRequest(doc);
			log.debug("CorporateRevamp > CorporateRevampXMLHandler > generateCorporateInquiryXMLRequest > XML generated....");
		} catch (Exception e) {
			log.fatal("Loyalty > LoyaltyCorporateRevampXMLHandler > generateCorporateInquiryXMLRequest :: Exception ,"+ e);
			throw new SystemException(e);
		}
		
		log.debug("CorporateRevamp > CorporateRevampXMLHandler > generateCorporateInquiryXMLRequest :: end");
		return xmlRequest;
	}
	
	
	

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		
		
//		String sss = "<MOBILY_BSL_SR_REPLY><SR_BK_HEADER_Reply><FuncID>AliasNameUpdate</FuncID><SecurityKey /><ServiceRequestId>SR_1417310438</ServiceRequestId><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20091030214708</SrDate><BackendChannel>NETWORK</BackendChannel><SrRcvDate>20091030214808</SrRcvDate><SrStatus>1</SrStatus></SR_BK_HEADER_Reply>	      <ErrorCode>0</ErrorCode>	      <ErrorMsg>no error</ErrorMsg>		</MOBILY_BSL_SR_REPLY>";
//		
//		CorporateRevampXMLHandler ss = new CorporateRevampXMLHandler();
//		ss.parseUpdateAliasNameXMLReply(sss);
		
		//CorporateRevampXMLHandler ss = new CorporateRevampXMLHandler();
		//String xml = ss.getXMLFile();
		//String ssxml = "<MOBILY_BSL_SR_REPLY>    <SR_BK_HEADER>        <FuncId>CORP_INQ</FuncId>        <SecurityKey>123</SecurityKey>        <MsgVersion>123</MsgVersion>        <RequestorChannelId>SIEBEL</RequestorChannelId>    </SR_BK_HEADER>    <ListOfBillingAccount>        <BillingAccount>            <AccountNumber>100013543348897</AccountNumber>            <BillInfo>                <BillCycle>Monthly</BillCycle>                <Bills>                    <Bill>                        <BillNumber>1107396</BillNumber>                        <EndDate>20100131102026</EndDate>                    </Bill>                    <Bill>                        <BillNumber>1107397</BillNumber>                        <EndDate>20100131102026</EndDate>                    </Bill>                    <Bill>                        <BillNumber>1107398</BillNumber>                        <EndDate>20100131102026</EndDate>                    </Bill>                    <Bill>                        <BillNumber>1107399</BillNumber>                        <EndDate>20100131102026</EndDate>                    </Bill>                    <Bill>                        <BillNumber>1113092</BillNumber>                        <EndDate>20100131102026</EndDate>                    </Bill>                </Bills>            </BillInfo>            <AccountTypeCode>Billing</AccountTypeCode>            <ProductType/>            <ContractPeriod/>            <NetworkConfiguration/>            <ParentAccountNumber>100013318112665</ParentAccountNumber>            <MasterAccountNumber>100013318074451</MasterAccountNumber>            <Name>QATEST 88OCT1</Name>            <BillingAccount>                <AccountNumber>100013543348897</AccountNumber>                <BillInfo>                </BillInfo>                <AccountTypeCode>Billing</AccountTypeCode>                <ProductType/>                <ContractPeriod/>                <NetworkConfiguration/>                <ParentAccountNumber>100013318112665</ParentAccountNumber>                <MasterAccountNumber>100013318074451</MasterAccountNumber>                <Name>QATEST 88OCT1</Name>                <BillingAccount>                    <AccountNumber>100013543348897</AccountNumber>                    <BillInfo>                </BillInfo>                    <AccountTypeCode>Billing</AccountTypeCode>                    <ProductType>DIA</ProductType>                    <ContractPeriod>1 Year</ContractPeriod>                    <NetworkConfiguration/>                    <ParentAccountNumber>100013318112665</ParentAccountNumber>                    <MasterAccountNumber>100013318074451</MasterAccountNumber>                    <Name>QATEST 88OCT1</Name>                    <ListOfServiceSubAccounts>                        <CutServiceSubAccounts>                            <AccountNumber>100013543377987</AccountNumber>                            <BillInfo>                                <BillCycle>Monthly</BillCycle>                                <Bills>                                    <Bill>                                        <BillNumber>1107396</BillNumber>                                        <EndDate>20100131102026</EndDate>                                    </Bill>                                    <Bill>                                        <BillNumber>1107397</BillNumber>                                        <EndDate>20100131102026</EndDate>                                    </Bill>                                    <Bill>                                        <BillNumber>1107398</BillNumber>                                        <EndDate>20100131102026</EndDate>                                    </Bill>                                    <Bill>                                        <BillNumber>1107399</BillNumber>                                        <EndDate>20100131102026</EndDate>                                    </Bill>                                    <Bill>                                        <BillNumber>1113092</BillNumber>                                        <EndDate>20100131102026</EndDate>                                    </Bill>                                </Bills>                            </BillInfo>                            <VPNID/>                            <PackageName>DIA - Circuit</PackageName>                            <Name>QATEST 88OCT1 [100013543377987]</Name>                            <Alias>QATEST 88OCT1 [100013543377987]</Alias>                            <Items>                                <Item>                                    <ItemName>DIA - Circuit</ItemName>                                    <Status>Suspended</Status>                                    <Attributes>                                        <Attribute>                                            <Name>Bandwidth</Name>                                            <TextValue>18 MB</TextValue>                                        </Attribute>                                        <Attribute>                                            <Name>Connection Mode</Name>                                            <TextValue>Dedicated</TextValue>                                        </Attribute>                                        <Attribute>                                            <Name>Access Network</Name>                                            <TextValue>VSAT</TextValue>                                        </Attribute>                                        <Attribute>                                            <Name>NTU</Name>                                            <TextValue>VSAT Tranciever</TextValue>                                        </Attribute>                                        <Attribute>                                            <Name>Default CIDR Profile</Name>                                            <TextValue>/29</TextValue>                                        </Attribute>                                    </Attributes>									</Item>                                    <Item>                                        <ItemName>DIA Installation New</ItemName>                                        <Status>Active</Status>                                        <Attributes/>                                    </Item>                                    <Item>                                        <ItemName>Managed Connectivity</ItemName>                                        <Status>Active</Status>                                        <Attributes/>                                    </Item>                                    <Item>                                        <ItemName>Cisco 871 Security Bundle with Advanced IP Services</ItemName>                                        <Status>Active</Status>                                        <Attributes/>                                    </Item>                                    <Item>                                        <ItemName>SLA</ItemName>                                        <Status>Active</Status>                                        <Attributes>                                            <Attribute>                                                <Name>SLA</Name>                                                <TextValue>Standard</TextValue>                                            </Attribute>                                        </Attributes>                                    </Item>                            </Items>                        </CutServiceSubAccounts>                    </ListOfServiceSubAccounts>                </BillingAccount>            </BillingAccount>        </BillingAccount>    </ListOfBillingAccount></MOBILY_BSL_SR_REPLY>";
		//BillingAccountVO accountVO = ss.parseCorporateInquiryReply(ssxml);
		
		
		
		String ssa = "<AttributeInfo/>";
		
		Document doc = XMLUtility.parseXMLString(ssa);
		
		if(doc != null) {
			NodeList nodeList = doc.getElementsByTagName("AttributeInfo");
			if(nodeList != null && nodeList.getLength() > 0) {
				Node rootNode = nodeList.item(0);
				NodeList nl = rootNode.getChildNodes();
				log.debug(nl.getLength());
				
			}
		}
		log.debug("*******************************");
		
		
	}
	
	// this method is for testing purpose only
	public String getXMLFile() {
		StringBuffer sbf = new StringBuffer();
		try{
		    FileInputStream fstream = new FileInputStream("C:\\example.xml");
		    DataInputStream in = new DataInputStream(fstream);
		        BufferedReader br = new BufferedReader(new InputStreamReader(in));
		    String strLine;
		    
		    
		    while ((strLine = br.readLine()) != null)   {
		    	sbf.append(strLine);
		    }
		    in.close();
		    log.debug(sbf.toString());
		    }catch (Exception e){//Catch exception if any
		      System.err.println("Error: " + e.getMessage());
		    }
		   return sbf.toString();
	}

}
