package sa.com.mobily.eportal.billing.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class MyBalanceRequestVO extends BaseVO
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1423755253861590685L;

	private String userID;
	private String msisdn;
	private String serviceAcctNumber;
	private int lineType = 0;
	private int subscriptionType;
	private int customerType = 0;
	private String locale;
	private boolean ftthRenewInfoRequired = false;
	private boolean favoriteNumberRequired=false;
	private boolean freeBenfitsRequired=false;
	private boolean balanceInquiryRequired=true;
	public String getUserID()
	{
		return userID;
	}
	public void setUserID(String userID)
	{
		this.userID = userID;
	}
	public String getMsisdn()
	{
		return msisdn;
	}
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	public String getServiceAcctNumber()
	{
		return serviceAcctNumber;
	}
	public void setServiceAcctNumber(String serviceAcctNumber)
	{
		this.serviceAcctNumber = serviceAcctNumber;
	}
	public int getLineType()
	{
		return lineType;
	}
	public void setLineType(int lineType)
	{
		this.lineType = lineType;
	}
	public int getSubscriptionType()
	{
		return subscriptionType;
	}
	public void setSubscriptionType(int subscriptionType)
	{
		this.subscriptionType = subscriptionType;
	}
	public int getCustomerType()
	{
		return customerType;
	}
	public void setCustomerType(int customerType)
	{
		this.customerType = customerType;
	}
	public String getLocale()
	{
		return locale;
	}
	public void setLocale(String locale)
	{
		this.locale = locale;
	}
	public boolean isFtthRenewInfoRequired()
	{
		return ftthRenewInfoRequired;
	}
	public void setFtthRenewInfoRequired(boolean ftthRenewInfoRequired)
	{
		this.ftthRenewInfoRequired = ftthRenewInfoRequired;
	}
	public boolean isFavoriteNumberRequired()
	{
		return favoriteNumberRequired;
	}
	public void setFavoriteNumberRequired(boolean favoriteNumberRequired)
	{
		this.favoriteNumberRequired = favoriteNumberRequired;
	}
	public boolean isFreeBenfitsRequired()
	{
		return freeBenfitsRequired;
	}
	public void setFreeBenfitsRequired(boolean freeBenfitsRequired)
	{
		this.freeBenfitsRequired = freeBenfitsRequired;
	}
	public boolean isBalanceInquiryRequired()
	{
		return balanceInquiryRequired;
	}
	public void setBalanceInquiryRequired(boolean balanceInquiryRequired)
	{
		this.balanceInquiryRequired = balanceInquiryRequired;
	}
	
}
