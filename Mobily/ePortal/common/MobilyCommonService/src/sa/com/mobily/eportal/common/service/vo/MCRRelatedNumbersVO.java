package sa.com.mobily.eportal.common.service.vo;


/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MCRRelatedNumbersVO {
    
	private String packageName ;
    private String requestorUserId;
    private String packageDataFlag;
    private String idDocNumber;
    private String billingNumber;
    private String idDocType;
    private String msisdn ;
    private String cpeSerialNum ;
    private String planType;
    private String accountType;
    private String serviceAccNumber;
    
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getRequestorUserId() {
		return requestorUserId;
	}
	public void setRequestorUserId(String requestorUserId) {
		this.requestorUserId = requestorUserId;
	}
	public String getPackageDataFlag() {
		return packageDataFlag;
	}
	public void setPackageDataFlag(String packageDataFlag) {
		this.packageDataFlag = packageDataFlag;
	}
	public String getIdDocNumber() {
		return idDocNumber;
	}
	public void setIdDocNumber(String idDocNumber) {
		this.idDocNumber = idDocNumber;
	}
	public String getBillingNumber() {
		return billingNumber;
	}
	public void setBillingNumber(String billingNumber) {
		this.billingNumber = billingNumber;
	}
	public String getIdDocType() {
		return idDocType;
	}
	public void setIdDocType(String idDocType) {
		this.idDocType = idDocType;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getCpeSerialNum() {
		return cpeSerialNum;
	}
	public void setCpeSerialNum(String cpeSerialNum) {
		this.cpeSerialNum = cpeSerialNum;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getServiceAccNumber() {
		return serviceAccNumber;
	}
	public void setServiceAccNumber(String serviceAccNumber) {
		this.serviceAccNumber = serviceAccNumber;
	}
}