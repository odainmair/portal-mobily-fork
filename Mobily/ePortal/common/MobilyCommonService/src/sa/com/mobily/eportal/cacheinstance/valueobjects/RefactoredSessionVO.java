package sa.com.mobily.eportal.cacheinstance.valueobjects;

import java.sql.Timestamp;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class RefactoredSessionVO extends BaseVO {
	
	private static final long serialVersionUID = 1L;
	private String sessionId;
	private String userName;
	private long subscriptionId;
	private Timestamp createdDate;
	
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public long getSubscriptionId(){
		return subscriptionId;
	}
	public void setSubscriptionId(long subscriptionId){
		this.subscriptionId = subscriptionId;
	}
	public Timestamp getCreatedDate(){
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate){
		this.createdDate = createdDate;
	}
}
