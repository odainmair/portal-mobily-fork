package sa.com.mobily.eportal.billing.jaxb.object;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EE_EAI_HEADER", propOrder = { "msgFormat", "msgVersion",
		"channelId", "channelFunction", "userId",
		"language", "securityInfo", "returnCode", "functionId", "securityKey", "srDate", "srStatus",
		"channelTransId", "srRcvDate", "overwriteOpenOrder", "chargeable", "chargingMode", "chargeAmount",
		"dealerId", "shopId", "agentId", "serviceRequestId" })
public class MessageRequestHeader {
	@XmlElement(name = "MsgFormat")
	protected String msgFormat;
	@XmlElement(name = "MsgVersion")
	protected String msgVersion;

	@XmlElement(name = "RequestorChannelId")
	protected String channelId;
	@XmlElement(name = "RequestorChannelFunction")
	protected String channelFunction;
	@XmlElement(name = "RequestorUserId")
	protected String userId;
	@XmlElement(name = "RequestorLanguage")
	protected String language;
	@XmlElement(name = "RequestorSecurityInfo")
	protected String securityInfo;
	@XmlElement(name = "ReturnCode")
	protected String returnCode;
	@XmlElement(name = "FuncId")
	protected String functionId ;
	
	@XmlElement(name = "SecurityKey")
	protected String securityKey ;
	
	@XmlElement(name = "SrDate")
	protected String srDate ;
	
	@XmlElement(name = "SrStatus")
	protected String srStatus ;
	
	@XmlElement(name = "ChannelTransId")
	protected String channelTransId ;
	
	public String getSrStatus() {
		return srStatus;
	}

	public void setSrStatus(String srStatus) {
		this.srStatus = srStatus;
	}

	@XmlElement(name = "SrRcvDate")
	protected String srRcvDate ;
	
	public String getSrRcvDate() {
		return srRcvDate;
	}

	public void setSrRcvDate(String srRcvDate) {
		this.srRcvDate = srRcvDate;
	}

	@XmlElement(name = "OverwriteOpenOrder")
	protected String overwriteOpenOrder ;
	
	@XmlElement(name = "Chargeable")
	protected String chargeable ;
	
	@XmlElement(name = "ChargingMode")
	protected String chargingMode ;
	
	
	
	
	@XmlElement(name = "ChargeAmount")
	protected double chargeAmount ;
	
	
	@XmlElement(name = "dealerId")
	protected String dealerId ;
	
	
	@XmlElement(name = "ShopId")
	protected String shopId ;
	
	
	@XmlElement(name = "AgentId")
	protected String agentId ;
	
	
	
	@XmlElement(name = "ServiceRequestId")
	protected String serviceRequestId ;

	public String getServiceRequestId() {
		return serviceRequestId;
	}

	public void setServiceRequestId(String serviceRequestId) {
		this.serviceRequestId = serviceRequestId;
	}

	public String getChargeable() {
		return chargeable;
	}

	public void setChargeable(String chargeable) {
		this.chargeable = chargeable;
	}

	public String getSecurityKey() {
		return securityKey;
	}

	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}

	public String getMsgFormat() {
		return msgFormat;
	}

	public void setMsgFormat(String msgFormat) {
		this.msgFormat = msgFormat;
	}

	public String getMsgVersion() {
		return msgVersion;
	}

	public void setMsgVersion(String msgVersion) {
		this.msgVersion = msgVersion;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getChannelFunction() {
		return channelFunction;
	}

	public void setChannelFunction(String channelFunction) {
		this.channelFunction = channelFunction;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSecurityInfo() {
		return securityInfo;
	}

	public void setSecurityInfo(String securityInfo) {
		this.securityInfo = securityInfo;
	}

	public String getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}

	public String getFunctionId() {
		return functionId;
	}

	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}

	public String getSrDate() {
		return srDate;
	}

	public void setSrDate(String srDate) {
		this.srDate = srDate;
	}

	public String getOverwriteOpenOrder() {
		return overwriteOpenOrder;
	}

	public void setOverwriteOpenOrder(String overwriteOpenOrder) {
		this.overwriteOpenOrder = overwriteOpenOrder;
	}

	public String getChargingMode() {
		return chargingMode;
	}

	public void setChargingMode(String chargingMode) {
		this.chargingMode = chargingMode;
	}

	public double getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(double chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	public String getDealerId() {
		return dealerId;
	}

	public void setDealerId(String dealerId) {
		this.dealerId = dealerId;
	}

	public String getShopId() {
		return shopId;
	}

	public void setShopId(String shopId) {
		this.shopId = shopId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getChannelTransId() {
		return channelTransId;
	}

	public void setChannelTransId(String channelTransId) {
		this.channelTransId = channelTransId;
	}

	
}