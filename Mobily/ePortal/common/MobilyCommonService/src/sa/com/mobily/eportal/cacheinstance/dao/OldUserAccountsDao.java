package sa.com.mobily.eportal.cacheinstance.dao;

import java.util.List;

import javax.persistence.EntityManager;

import sa.com.mobily.eportal.usermanagement.model.UserAccount;

public interface OldUserAccountsDao
{
	List<UserAccount> findByUserAccountId(String userId, EntityManager em);

	UserAccount findByUsername(String userName, EntityManager em);
}