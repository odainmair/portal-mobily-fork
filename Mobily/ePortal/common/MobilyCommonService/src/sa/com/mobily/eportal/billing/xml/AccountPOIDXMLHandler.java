package sa.com.mobily.eportal.billing.xml;

import java.io.StringReader;

import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import sa.com.mobily.eportal.billing.constants.ConstantsIfc;
import sa.com.mobily.eportal.billing.constants.TagIfc;
import sa.com.mobily.eportal.billing.vo.AccountPOIDReplyVO;
import sa.com.mobily.eportal.billing.vo.AccountPOIDRequestVO;
import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.exception.application.BackEndException;
import sa.com.mobily.eportal.common.exception.constant.ExceptionConstantIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.CustomerServiceUtil;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;
import sa.com.mobily.eportal.common.service.valueobject.common.ConsumerRequestHeaderVO;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;

/**
 * 
 * @author r.agarwal.mit
 * 
 */

public class AccountPOIDXMLHandler implements TagIfc
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);

	private final String className = AccountPOIDXMLHandler.class.getName();

	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to generate the request XML for Consumer
	 *               Profile
	 * @param requestVO
	 * @return xmlRequest
	 */

	public String generateAccountPOIDXML(AccountPOIDRequestVO requestVO)
	{
		String method = "generateAccountPOIDXML";
		String xmlRequest = "";

		if (requestVO == null)
		{
			// log.fatal("CustomerProfileXMLHandler > generateCustomerProfileXML : ActiveLinesInquiryVO is null.");
			throw new BackEndException(ExceptionConstantIfc.CUSTOMER_PROFILE_INFO_SERVICE, ConstantsIfc.INVALID_REQUEST);
		}

		executionContext.audit(Level.INFO, "AccountPOIDXMLHandler > generateAccountPOIDXML : started for account: " + requestVO.getServiceAccountNumber(),
				className, method);
		
		final ByteOutputStream outputStream = new ByteOutputStream();
		XMLStreamWriter xmlStreamWriter = null;
		try
		{
			final XMLOutputFactory outputFactory = XMLOutputFactory.newInstance();
			xmlStreamWriter = outputFactory.createXMLStreamWriter(outputStream, "UTF-8");

			JAXBContext jaxbContext = JAXBContext.newInstance(AccountPOIDRequestVO.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(requestVO, xmlStreamWriter);
			outputStream.flush();

			xmlRequest = new String(outputStream.getBytes(), "UTF-8").trim();
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while generating account poid xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		finally
		{
			if (xmlStreamWriter != null)
			{
				try
				{
					xmlStreamWriter.close();
				}
				catch (XMLStreamException e)
				{
					executionContext.log(Level.SEVERE, e.getMessage(), className, method, e);
				}
			}
			if (outputStream != null)
			{
				outputStream.close();
			}
		}
		return xmlRequest;
	}

	/**
	 * @date: July 22, 2013
	 * @author: r.agarwal.mit
	 * @description: Responsible to parse the Consumer profile XML
	 * @param replyMessage
	 * @return CustomerInfoVO
	 */
	public AccountPOIDReplyVO parseAccountPOIDXML(String replyMessage)
	{
		String method = "parseAccountPOIDXML";
		AccountPOIDReplyVO replyVO = null;
		try
		{
			JAXBContext jc = JAXBContext.newInstance(AccountPOIDReplyVO.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			XMLInputFactory xmlif = XMLInputFactory.newInstance();

			// for testing -- START
			// FileReader fr = new
			// FileReader("G:\\Portal 8\\AccountPOIDReply.xml");
			// XMLStreamReader xmler = xmlif.createXMLStreamReader(fr);

			// for Testing -- END
			
			XMLStreamReader xmler = xmlif.createXMLStreamReader(new StringReader(replyMessage));
			replyVO = (AccountPOIDReplyVO) unmarshaller.unmarshal(xmler);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "Exception while parsing account poid xml" + e.getMessage(), className, method, e);
			throw new ServiceException(e.getMessage(), e);
		}
		return replyVO;
	}

	public static void main(String agrs[])
	{
		AccountPOIDRequestVO requestVO = new AccountPOIDRequestVO();
		requestVO.setMsisdn("966540510111");
		//requestVO.setServiceAccountNumber("966540510111");

		String userId = "rupesh";

		// Populate Header
		ConsumerRequestHeaderVO headerVO = new CustomerServiceUtil().populateRequestHeaderVO(ConstantsIfc.FUN_GET_ACCOUNT_POID_INQ, userId);

		requestVO.setHeaderVO(headerVO);

		AccountPOIDXMLHandler handler = new AccountPOIDXMLHandler();
		try
		{
			String requestXml = handler.generateAccountPOIDXML(requestVO);
//			System.out.println("requestXml::" + requestXml);

			AccountPOIDReplyVO replyVO = handler.parseAccountPOIDXML(requestXml);
			if (replyVO != null)
			{
//				System.out.println("msg format=" + replyVO.getHeaderVO().getMsgFormat());
//				System.out.println("status=" + replyVO.getHeaderVO().getStatus());
//				System.out.println("POID ID=" + replyVO.getAccountPOID());
//				System.out.println("parent Poid Id=" + replyVO.getParentPOID());
			}
		}
		catch (Exception e)
		{
//			System.out.println("here=" + e.getMessage());
//			e.printStackTrace();
		}
	}
}