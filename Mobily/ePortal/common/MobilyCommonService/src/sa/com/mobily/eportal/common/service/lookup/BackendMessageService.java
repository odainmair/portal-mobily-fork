package sa.com.mobily.eportal.common.service.lookup;

import java.util.HashMap;
import java.util.List;

import javax.validation.constraints.NotNull;

import sa.com.mobily.eportal.cacheinstance.dao.BackendMessagesDAO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.BackendMessage;
import sa.com.mobily.eportal.common.service.util.caching.CachingService;

/**
 * 
 * @author Mohamed Shalaby
 *
 */

public class BackendMessageService
{
	BackendMessagesDAO backendMessagesDAO;
	HashMap<String, BackendMessage> messagesMap;
	private final static String LOCALE_AR = "AR";
	
	
	private final static String CACHE_KEY_BACKEND_MESSAGE = "BACKEND_MESSAGE";

	/**
	 * This method returns the localized backend message from Backend_Messages DB table using the message key
	 * @param key the message key
	 * @param locale the locale of the message. Possible values are "ar" for Arabic, "en" for English
	 * @return String, the localized message
	 */
	@SuppressWarnings("unchecked")
	public String getLocalizedBackendMessage(@NotNull String key, @NotNull String locale)
	{
		String localizedMessage = "";
		if(messagesMap == null){
			List<BackendMessage> messages = BackendMessagesDAO.getInstance().getMessages();
			messagesMap = populateMessagesMap(messages);
			CachingService.getInstance().putCachedResponse(CACHE_KEY_BACKEND_MESSAGE, messagesMap);
		} else{
			messagesMap = (HashMap<String, BackendMessage>) CachingService.getInstance().getCachedResponse(CACHE_KEY_BACKEND_MESSAGE);
		}
		BackendMessage message =  messagesMap.get(key);
		if(message != null){
			if(LOCALE_AR.equalsIgnoreCase(locale)){
				localizedMessage = message.getMessage_ar();
			} else{
				localizedMessage = message.getMessage_en();
			}
		}
		return localizedMessage;
	}
	
	public HashMap<String, BackendMessage> populateMessagesMap(List<BackendMessage> messages){
		messagesMap = new HashMap<String, BackendMessage>();
		for(BackendMessage message: messages){
			messagesMap.put(message.getKey(), message);
		}
		return messagesMap;
	}
}
