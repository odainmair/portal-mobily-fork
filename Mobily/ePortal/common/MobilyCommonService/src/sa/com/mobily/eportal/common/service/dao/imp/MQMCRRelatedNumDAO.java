package sa.com.mobily.eportal.common.service.dao.imp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.MCRRelatedNumDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * @author n.gundluru.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MQMCRRelatedNumDAO implements MCRRelatedNumDAO {

    private static final Logger log = LoggerInterface.log;
    
    /**
     * 
     */
    public String getRelatedMsisdnFromMCR(String xmlMessage) {
		String replyMessage = "";
		log.info("MQMCRRelatedNumDAO > getRelatedMsisdnFromMCR > Called");

		try {
			/*
			 * Get QueuName
			 */
			String strQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MCR_RELATED_MSISDN_REQUEST_QUEUENAME);
			log.debug("MQMCRRelatedNumDAO > getRelatedMsisdnFromMCR > The request Queue ["+ strQueueName + "]");
			/*
			 * Get ReplyQueuName
			 */
			String strReplyQueueName = MobilyUtility.getPropertyValue(ConstantIfc.MCR_RELATED_MSISDN_REPLY_QUEUENAME);
			log.debug("MQMCRRelatedNumDAO > getRelatedMsisdnFromMCR > The reply Queue Name ["+ strReplyQueueName + "]");
			/*
			 * Get reply Queu manager Name
			 */
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlMessage, strQueueName,strReplyQueueName);
			
			} catch (RuntimeException e) {
				log.fatal("MQMCRRelatedNumDAO > getRelatedMsisdnFromMCR > MQSend Exception > "+ e.getMessage());
				throw new SystemException(e);
			} catch (Exception e) {
			    log.fatal("MQMCRRelatedNumDAO > getRelatedMsisdnFromMCR > Exception > "+ e.getMessage());
				throw new SystemException(e);
			}
		log.info("MQMCRRelatedNumDAO > getRelatedMsisdnFromMCR > Done");
		return replyMessage;
    }
}
