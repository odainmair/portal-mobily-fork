/**
 * 
 */
package sa.com.mobily.eportal.common.service.util;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.service.mq.dao.AuditingDAO;
import sa.com.mobily.eportal.common.service.mq.dao.MQAuditDAO;
import sa.com.mobily.eportal.common.service.mq.vo.MQAuditVO;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.ConstantsIfc;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

/**
 * @author Nabeel Abid
 * 
 */
public class MQUtility
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.MQ_CONNECTION_HANDLER_SERVICE_LOGGER_NAME);

	private static final String MSG_TYPE_1 = "<MOBILY_BSL_SR_REPLY>";

	private static final String MSG_TYPE_2 = "<EE_EAI_MESSAGE>";

	//private static final String timeout_msg = "Time-out";

	private static final String ec_sTag = "<ErrorCode>";

	private static final String ec_eTag = "</ErrorCode>";

	private static final String em_sTag = "<ErrorMsg>";

	private static final String em_eTag = "</ErrorMsg>";

	private static final String rc_sTag = "<ReturnCode>";

	private static final String rc_eTag = "</ReturnCode>";

	public static void saveMQAudit(String requestQueue, String replyQueue, String message, String reply)
	{
		MQAuditVO mqAuditVO = new MQAuditVO();
		try
		{
			String audityType = ResourceEnvironmentProviderService.getInstance().getStringProperty(ConstantsIfc.MQ_AUDIT_TYPE);
        	executionContext.severe("MQUtility >  saveMQAudit > audityType =["+audityType+"]");
        	
            if(ConstantsIfc.MQ_AUDIT_TYPE_DB.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				
            	mqAuditVO.setRequestQueue(requestQueue);
				mqAuditVO.setReplyQueue(replyQueue);
				mqAuditVO.setMessage(message);
				mqAuditVO.setReply(reply);
	
				parseResponse(mqAuditVO);
            	
            	MQAuditDAO.getInstance().saveMQRequest(mqAuditVO);

			} else if(ConstantsIfc.MQ_AUDIT_TYPE_MDB.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				
				mqAuditVO.setRequestQueue(requestQueue);
				mqAuditVO.setReplyQueue(replyQueue);
				mqAuditVO.setMessage(message);
				mqAuditVO.setReply(reply);
	
				parseResponse(mqAuditVO);
				
				new AuditingDAO().auditMQRequest(mqAuditVO);
			} else if(ConstantsIfc.MQ_AUDIT_TYPE_NO_AUDIT.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				executionContext.severe("MQUtility >  saveMQAudit > MQ request Audit Not Required....");
			} else {
				executionContext.severe("MQUtility >  saveMQAudit > MQ request Audit Not Required....");
			}
		}
		catch (Exception e)
		{
			executionContext.severe("Exception while saving MQ request " + e);
		}
	}

	public static void saveMQAudit(MQAuditVO mqAuditVO)
	{
		try
		{
			String audityType = ResourceEnvironmentProviderService.getInstance().getStringProperty(ConstantsIfc.MQ_AUDIT_TYPE);
        	executionContext.severe("MQUtility >  saveMQAudit > audityType =["+audityType+"]");
        	
            if(ConstantsIfc.MQ_AUDIT_TYPE_DB.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				
            	parseResponse(mqAuditVO);
            	
            	MQAuditDAO.getInstance().saveMQRequest(mqAuditVO);

			} else  if(ConstantsIfc.MQ_AUDIT_TYPE_MDB.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				
				parseResponse(mqAuditVO);
				
				new AuditingDAO().auditMQRequest(mqAuditVO);
			} else if(ConstantsIfc.MQ_AUDIT_TYPE_NO_AUDIT.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				executionContext.severe("MQUtility >  saveMQAudit > MQ request Audit Not Required....");
			} else {
				executionContext.severe("MQUtility >  saveMQAudit > MQ request Audit Not Required....");
			}
		}
		catch (Throwable e)
		{
			executionContext.severe("Exception while saving MQ request " + e);
			throw new RuntimeException(e);
		}
	}
	
	public static void auditMQRequest(MQAuditVO mqAuditVO)
	{
		try
		{
			String audityType = ResourceEnvironmentProviderService.getInstance().getStringProperty(ConstantsIfc.MQ_AUDIT_TYPE);
        	executionContext.severe("MQUtility >  auditMQRequest > audityType =["+audityType+"]");
        	
            if(ConstantsIfc.MQ_AUDIT_TYPE_DB.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				
            	MQAuditDAO.getInstance().saveMQRequest(mqAuditVO);

			} else  if(ConstantsIfc.MQ_AUDIT_TYPE_MDB.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				
				new AuditingDAO().auditMQRequest(mqAuditVO);
				
			} else if(ConstantsIfc.MQ_AUDIT_TYPE_NO_AUDIT.equalsIgnoreCase(FormatterUtility.checkNull(audityType))) {
				executionContext.severe("MQUtility >  auditMQRequest > MQ request Audit Not Required....");
			} else {
				executionContext.severe("MQUtility >  auditMQRequest > MQ request Audit Not Required....");
			}			
		}
		catch (Throwable e)
		{
			executionContext.severe("Exception while saving MQ request " + e);
			throw new RuntimeException(e);
		}
	}

	private static MQAuditVO parseResponse(MQAuditVO mqAuditVO) throws Exception
	{
		try
		{
			if (mqAuditVO != null)
			{
				//mqAuditVO.setErrorMessage(null);
				if (mqAuditVO.getReplyQueue() != null && mqAuditVO.getReplyQueue().length() > 0)
				{
					if (mqAuditVO.getReply() != null && mqAuditVO.getReply().contains(MSG_TYPE_1))
					{
						mqAuditVO.setErrorMessage(extractErrorCodeAndMessage(mqAuditVO.getReply()));
					}
					else if (mqAuditVO.getReply() != null && mqAuditVO.getReply().contains(MSG_TYPE_2))
					{
						mqAuditVO.setErrorMessage(extractReturnCode(mqAuditVO.getReply()));
					}
					// else { //Commented by abdelhamid 7-March 2016
					// mqAuditVO.setErrorMessage(timeout_msg);
					// }
				}
			}
		}
		catch (Throwable e)
		{
			throw new RuntimeException(e);
		}
		return mqAuditVO;
	}

	/**
	 * This method is used to extract the error code and error message from the
	 * XML reply from queue
	 * 
	 * @author Nabeel Abid
	 * @param replyXML
	 * @return String
	 */
	private static String extractErrorCodeAndMessage(String replyXML) throws Exception
	{
		String error = "";
		try
		{
			String ec = replyXML.substring(replyXML.indexOf(ec_sTag), replyXML.lastIndexOf(ec_eTag)).substring(ec_sTag.length());
			String em = replyXML.substring(replyXML.indexOf(em_sTag), replyXML.lastIndexOf(em_eTag)).substring(em_sTag.length());
			error = ec;
			if (em.length() > 0)
			{
				error = error + " - " + em;
			}
		}
		catch (Exception e)
		{
			// throw e;
			executionContext.severe("One of the expected xml tags " + ec_sTag + ", " + ec_eTag + ", " + em_eTag + " and " + em_sTag + " is missing");
		}
		return error;
	}

	/**
	 * This method is used to extract the error code and error message from the
	 * XML reply from queue
	 * 
	 * @author Nabeel Abid
	 * @param replyXML
	 * @return String
	 */
	private static String extractReturnCode(String replyXML) throws Exception
	{
		String error = "";
		try
		{
			error = replyXML.substring(replyXML.indexOf(rc_sTag), replyXML.lastIndexOf(rc_eTag)).substring(rc_sTag.length());
		}
		catch (Exception e)
		{
			// throw e;
			executionContext.severe("One of the expected xml tags " + ec_sTag + ", " + ec_eTag + ", " + em_eTag + " and " + em_sTag + " is missing");
		}
		return error;
	}
	/*
	 * public static void main(String[] args) throws Exception { MQAuditVO
	 * mqAuditVO = new MQAuditVO(); mqAuditVO.setRequestQueue("requestQueue");
	 * mqAuditVO.setReplyQueue("replyQueue"); mqAuditVO.setMessage("message");
	 * 
	 * String xml = ""; xml =
	 * "<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>VIDEO_STRM_UNSUB</FuncId><SecurityKey></SecurityKey><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20131216163731</SrDate><SrRcvDate>20131216163627</SrRcvDate><SrStatus>2</SrStatus></SR_HEADER_REPLY><ChannelTransId>ePortal_20131216163627</ChannelTransId><ServiceRequestId>SR_JD1123914570</ServiceRequestId><ErrorCode>12345</ErrorCode><ErrorMsg>Error message is here...</ErrorMsg></MOBILY_BSL_SR_REPLY>"
	 * ; mqAuditVO.setReply(xml); System.out.println("1. error: " +
	 * parseResponse(mqAuditVO).getErrorMessage()); xml =
	 * "<MOBILY_BSL_SR_REPLY><SR_HEADER_REPLY><FuncId>VIDEO_STRM_UNSUB</FuncId><SecurityKey></SecurityKey><MsgVersion>0000</MsgVersion><RequestorChannelId>ePortal</RequestorChannelId><SrDate>20131216163731</SrDate><SrRcvDate>20131216163627</SrRcvDate><SrStatus>2</SrStatus></SR_HEADER_REPLY><ChannelTransId>ePortal_20131216163627</ChannelTransId><ServiceRequestId>SR_JD1123914570</ServiceRequestId><ErrorCode>0</ErrorCode><ErrorMsg></ErrorMsg></MOBILY_BSL_SR_REPLY>"
	 * ; mqAuditVO.setReply(xml); System.out.println("2. error: " +
	 * parseResponse(mqAuditVO).getErrorMessage()); xml =
	 * "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>CreditCardPaymentGateway</MsgFormat><RequestorChannelId>EPOR</RequestorChannelId><RequestorChannelFunction>CreditCardEquiry</RequestorChannelFunction><ReturnCode>0014</ReturnCode></EE_EAI_HEADER><CreditCardDetails/></EE_EAI_MESSAGE>"
	 * ; mqAuditVO.setReply(xml); System.out.println("3. error: " +
	 * parseResponse(mqAuditVO).getErrorMessage()); xml =
	 * "<EE_EAI_MESSAGE><EE_EAI_HEADER><MsgFormat>CreditCardPaymentGateway</MsgFormat><RequestorChannelId>EPOR</RequestorChannelId><RequestorChannelFunction>CreditCardEquiry</RequestorChannelFunction><ReturnCode></ReturnCode></EE_EAI_HEADER><CreditCardDetails/></EE_EAI_MESSAGE>"
	 * ; mqAuditVO.setReply(xml); System.out.println("4. error: " +
	 * parseResponse(mqAuditVO).getErrorMessage()); xml = "";
	 * mqAuditVO.setReply(xml); System.out.println("5. error: " +
	 * parseResponse(mqAuditVO).getErrorMessage()); xml = null;
	 * mqAuditVO.setReply(xml); System.out.println("6. error: " +
	 * parseResponse(mqAuditVO).getErrorMessage()); }
	 */
}
