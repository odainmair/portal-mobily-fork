/**
 * 
 */
package sa.com.mobily.eportal.common.service.dao.imp;


import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CoverageDAO;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.mq.MQConnectionHandler;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;

import com.mobily.exception.mq.MQSendException;

/**
 * @author r.bandi.mit
 *
 */
public class MQCoverageDAO implements CoverageDAO{

	private static final Logger log = LoggerInterface.log;

	public String sendToMQWithReply(String xmlRequestMessage) throws SystemException {
		log.info("MQCoverageDAO > sendToMQWithReply > start");
		String replyMessage = "";
		try {
			/*
			 * Get QueuName
			 */
			String requestQueue = MobilyUtility.getPropertyValue(ConstantIfc.COVERAGE_REQUEST_QUEUENAME);
			String replyQueue = MobilyUtility.getPropertyValue(ConstantIfc.COVERAGE_REPLY_QUEUENAME);

			log.debug("MQCoverageDAO > sendToMQWithReply > Request Queue Name ["+ requestQueue+ "]");
			log.debug("MQCoverageDAO > sendToMQWithReply > Reply Queue Name ["+ replyQueue + "]");
			
			replyMessage = MQConnectionHandler.getInstance().sendToMQWithReply(xmlRequestMessage,requestQueue,replyQueue);

			log.debug("MQCoverageDAO > sendToMQWithReply >xmlReplyMessage" +replyMessage);
			
		} catch (RuntimeException e) {
			log.fatal("MQCoverageDAO > sendToMQWithReply > MQSend Exception :"+ e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("MQCoverageDAO > sendToMQWithReply > Exception " + e.getMessage());
			throw new SystemException(e);
		}
		log.info("MQCoverageDAO > sendToMQWithReply > end");
		return replyMessage;
	}
}