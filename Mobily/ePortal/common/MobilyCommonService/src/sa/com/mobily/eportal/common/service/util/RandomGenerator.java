/*
 * Created on Apr 24, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.util;

import java.util.Date;

import com.Ostermiller.util.RandPass;

/**
 * @author g.krishnamurthy.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RandomGenerator {
	
	public static final char[] CUSTOM_LETTERS_ALPHABET = {
		'A','B','C','D','E','F','G','H',
		'J','K','M','N','P',
		'Q','R','S','T','U','V','W','X',
		'Y','Z','a','b','c','d','e','f',
		'g','h','j','k','m','n',
		'p','q','r','s','t','u','v',
		'w','x','y','z',
	};
	
	public static final char[] NUMERIC = {'1','2','3','4','5','6','7','8', '9'};
	
	public static final char[] CUSTOM_LETTERS_ALPHABET_NUMBERIC = {
		'1','2','3','4','5','6','7','8','9',
		'A','B','C','D','E','F','G','H',
		'J','K','M','N','P',
		'Q','R','S','T','U','V','W','X',
		'Y','Z',
		
	};
	
	/**
	 * 
	 */
	public RandomGenerator() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
     *  This function is used to generate a 8 digit password
     *
     *  @return long of the generated password
     */
    public static String generateNumericRandom( )
    {
        long value = 0;

        // loop till you have a number > 6 digits
        while ( value < 100000 )
            value = Math.round( Math.random() * 100000000 );
        
        String return_value = Long.toString( value);
        return return_value;
    }

    
    public static String generateAlphabetNumericRandom(int aPasswordLength) {
       RandPass l_randPass = new RandPass();
        l_randPass.setMaxRepetition(0);
        l_randPass.setAlphabet(CUSTOM_LETTERS_ALPHABET); // Added by r.agarwal.mit for Forget Password
        
        String l_szNewPass = l_randPass.getPass( aPasswordLength);
        l_szNewPass = l_szNewPass.trim();
        
        return l_szNewPass;
    }

    public static String generateNumericRandom(int aPasswordLength) {
       RandPass l_randPass = new RandPass();
        l_randPass.setMaxRepetition(0);
        l_randPass.setAlphabet(NUMERIC); // Added by r.agarwal.mit for Forget Password
        
        String l_szNewPass = l_randPass.getPass( aPasswordLength);
        l_szNewPass = l_szNewPass.trim();
        
        return l_szNewPass;
    }
    
    
    public static String generateAlphaNumericRandom(int aPasswordLength) {
       RandPass l_randPass = new RandPass();
        l_randPass.setMaxRepetition(0);
        l_randPass.setAlphabet(CUSTOM_LETTERS_ALPHABET_NUMBERIC);
        
        String l_szNewPass = l_randPass.getPass( aPasswordLength);
        l_szNewPass = l_szNewPass.trim();
        
        return l_szNewPass;
    }
    

    public static String generateAlphabetRandom(int aPasswordLength) {
        RandPass l_randPass = new RandPass();
         l_randPass.setMaxRepetition(0);
         l_randPass.setAlphabet(CUSTOM_LETTERS_ALPHABET); // Added by r.agarwal.mit for Forget Password
         
         String l_szNewPass = l_randPass.getPass( aPasswordLength);
         l_szNewPass = l_szNewPass.trim();
         
         return l_szNewPass;
     }
    
    /**
     *  This function is used to generate 6 digit pin number
     *
     *  @return long of the generated password
     */
    public static String generateSixDigitNumeric() {
        long value = 0;

        while ( value < 100000 )
            value = Math.round( Math.random() * 1000000 );
        
        String return_value = Long.toString( value);
        return return_value;
    }
    
    
    public static void main(String args[]){
//    	System.out.println(RandomGenerator.generateAlphabetNumericRandom(5).toLowerCase() + RandomGenerator.generateAlphabetNumericRandom(3).toUpperCase() +
//    			RandomGenerator.generateNumericRandom().substring(0, 3));
    	System.out.println(generateAlphabetNumericRandom(10));
    	System.out.println(generateNumericRandom(4));
    	System.out.println(generateAlphaNumericRandom(4));
    }
    
    public static String generateTransactionID(){
    	return Long.toString(new Date().getTime()) + Long.toString(Math.round( Math.random() * 1000 ));
    }
}
