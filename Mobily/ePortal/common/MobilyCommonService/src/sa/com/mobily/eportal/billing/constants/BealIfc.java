/*
 * Created on Jan 25, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.billing.constants;

/**
 * @author msayed, Yasser Alsawy
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public interface BealIfc {
	
    //	Common
    public static final String MESSAGE_VERSION_VALUE = "0000";
    public static final String DATE_FORMAT = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_4S = "yyyyMMddHHmmSSss";

    public static final String BILL_PDF_DATE_FORMAT = "dd/MM/yyyy";
    
    public static final String TIMESTAMP_FORMAT = "yyyy-MM-dd hh:mm:ss";
    public static final String TIMESTAMP_FORMAT_MONTH = "yyyyMM";
    public static final String TIMESTAMP_FORMAT_PAYMENT = "YYMMDDHHMMSS";
    public static final String PAYMENT_CREATIONDATE_FORMAT ="dd/MM/yyyy HH:mm:ss";
    public static final String DATE_FORMAT_BIRTH_DATE = "yyyyMMdd";
    public static final String CHANNEL_ID_VALUE = "ePortal";
    public static final String CHANNEL_ID_MBI_VALUE = "MBI";
    public static final String CHANNEL_ID_BSL_VALUE = "BSL";
	public static final String LANG_VALUE = "E";
	public static final String SECURITY_INFO_VALUE = "secure";
	public static final String RETURN_CODE_VALUE = "0000";

	//	Balance Inquiry specific
	public static final String BALANCE_MESSAGE_FORMAT_VALUE = "BALANCE_INQ";
	public static final String BALANCE_CHANNEL_FUNC_VALUE = "INQ";
	public static final String BALANCE_CHARGABLE = "0";
	public static final String BALANCE_CHARGA_AMOUNT_VALUE = "0";
	public static final String BALANCE_GET_IN_VALUE = "0";
	
	//Customer Type Specification
	public static final String CUSTOMER_TYPE_MESSAGE_FORMAT_VALUE = "HLR.INQUIRY.REQUEST";
	public static final String CUSTOMER_TYPE_MESSAGE_VER_VALUE = "01";
	public static final String CUSTOMER_TYPE_CHANNEL_FUNC_VALUE = "INQ";
	
	// Bill Summary
	public static final String BILLSUMMARY_MESSAGE_FUNC_VALUE = "BILL_SUMMARY";
	
	//	Customer Profile specific
	public static final String CUSTOMER_PROFILE_FUNC_ID = "CUST_INQ";
	public static final String CUSTOMER_PROFILE_SECURITY_KEY = "";
	public static final String CUSTOMER_PROFILE_OVERWRITE_OPEN_ORDER = "Y";
	public static final String CUSTOMER_PROFILE_CHARGABLE = "N";
	public static final String CUSTOMER_PROFILE_CHARGE_AMOUNT = "0.0";
	public static final String CUSTOMER_PROFILE_CHARGING_MODE = "";
	
	public static final int CUSTOMER_PREPAID_TYPE =1;
	public static final int CUSTOMER_POSTPAID_TYPE =2;
	public static final String CUSTOMER_TYPE_BUSINESS ="Business";
	public static final String CUSTOMER_TYPE_CONSUMER ="Consumer";
	
	
	//customer status from CREDIT CONTROL
	public static final int CUSTOMER_STATUS_ACTIVE =1; 
	public static final int CUSTOMER_STATUS_WARNED =2;
	//	Bill List specific
	public static final String BILL_LIST_FUNC_ID = "BILL_LIST";
	public static final String BILL_LIST_SECURITY_KEY = "";
	
	//	Top Ten specific
	public static final String TOP_TEN_FUNC_ID = "BILL_LIST";
	public static final String TOP_TEN_SECURITY_KEY = "";
	public static final String  TOP_TEN_TYPE_LOCAL = "1";
	public static final String  TOP_TEN_TYPE_INTERNATIONAL = "2";
	public static final int  TOP_TEN_COUNT = 10;
	
	//	Service Orders List Specific
	public static final String SERVICE_ORDERS_LIST_FUNC_ID = "SERVICE_ORDERS_LIST";
	public static final String SERVICE_ORDERS_LIST_SECURITY_KEY = "";
	
	//	Call Details List
	public static final String  CALL_DETAILS_FUNC_ID = "CALL_DETAILS";
	public static final String  CALL_DETAILS_SECURITY_KEY = "";
	
	//	Promotion List
	public static final String  PROMOTION_INQUIRY_FUNC_ID   = "PREPAID_PROMO_INQUIRY";
	public static final String  PROMOTION_SUBSCRIBE_FUNC_ID = "PREPAID_PROMO_SUBSCRIBE";
	public static final String  PROMOTION_SECURITY_KEY      = "";
	
	//	MNP Corporate List
	public static final String  MNP_CORPORATE_FUNC_ID = "MNP_CORPORATE";
	public static final String  MNP_CORPORATE_SECURITY_KEY = "";
	
	//Payment List
	public static final String PAYMENT_FUNC_ID_VALUE ="PAYMENT_LIST";
	
	//Queue manager
	public static final String QUEUE_MANAGER_NAME_IN_CLUSTER = "mq.queue.manager.name";
	
	//Balance Inquiry
	public static final String BALANCE_INQ_REQUEST_QUEUENAME = "mq.balance.request.queue";
	public static final String BALANCE_INQ_REPLY_QUEUENAME = "mq.balance.reply.queue";
	
	//Complaint Services
	public static final String COMPLAINT_MSG_FORMAT_VALUE ="COMP_REQ";
	public static final String COMPLAINT_DETAIL_MSG_FORMAT_VALUE ="COMP_REQ_DETAIL";
	public static final String COMPLAINT_CHANNEL_FUNC_VALUE = "INQ";
	
	//Favorite Numbers
	public static final String FAVNUMBER_FUNC_ID_VALUE ="FAV_NUMBER_SETTING";
	public static final String FAVNUMBER_SEC_KEY_VALUE ="asdcsdasdasd";
	public static final String FAVNUMBER_OVERWRITE_OPEN_ORDER = "N";
	public static final String FAVNUMBER_CHARGABLE_VALUE = "Y";
	public static final String FAVNUMBER_CHARGE_AMOUNT = "0.0";
	public static final String FAVNUMBER_CHARGE_AMOUNT_NATIONAL = "0.0";
	public static final String FAVNUMBER_CHARGE_AMOUNT_INTERNATIONAL = "0.0";
	
	public static final int FAVNUMBER_TYPE_NATIONAL =1;
	public static final int FAVNUMBER_TYPE_INTERNATIONAL =2;
	
	public static final String FN_Req_FAVORITE_NUMBER_INQUIRY = "FAVORITE_NUMBER_INQUIRY";
	public static final String FN_Req_MsgOption = "";
	public static final String FN_Req_MsgVersion = "0000";
	public static final String FN_Req_RequestorChannelId = "E-Portal";
	public static final String FN_Req_RequestorChannelFunction = "INQ";
	public static final String FN_Req_RequestorUserId = "";
	public static final String FN_Req_RequestorLanguage = "E";
	public static final String FN_Req_RequestorSecurityInfo = "secure";
	public static final String FN_Req_ReturnCode = "0000";

	public static final String FNS_Req_FuncId = "FAV_NUMBER_SETTING";
	public static final String FNS_Req_MsgVersion = "0000";
	public static final String FNS_Req_RequestorChannelId = "E-Portal";
	public static final String FNS_Req_RequestorUserId = "E-Portal";
	public static final String FNS_Req_RequestorLanguage = "E";
	public static final String FNS_Req_OverwriteOpenOrder = "N";
	public static final String FNS_Req_Chargeable = "N";
	public static final String FNS_Req_ChargeAmount = "0.0";
	
	//	Supplementary Service specific
	public static final String SUPPLEMENTARY_SERVICE_FUNC_ID = "SUPPLEMENTARY_SERVICE";
	public static final String SUPPLEMENTARY_SERVICE_TWININGS = "TWININGS";
	public static final String SUPPLEMENTARY_SERVICE_LBS = "LBS";
	public static final String SUPPLEMENTARY_SERVICE_VSMS = "VSMS";
	
	public static final String SUPPLEMENTARY_SERVICE_INQ_MSG_FORMAT = "SUPPLEMENTARY_SERVICES_INQUIRY";
	public static final String SUPPLEMENTARY_SERVICE_CHANNEL_FUNC_VALUE = "INQ";
	public static final String SUPPLEMENTARY_SERVICE_RETURN_CODE_VALUE = "0";
	public static final String SUPPLEMENTARY_SERVICE_SECURITY_KEY = "12qweq342r2fwefw3";
	public static final String SUPPLEMENTARY_SERVICE_MSG_VER = "0000";
	public static final String SUPPLEMENTARY_SERVICE_VSMS_MSG_VER = "0001";
	public static final String SUPPLEMENTARY_SERVICE_NEW_MSG_VER = "0001";
	public static final String SUPPLEMENTARY_SERVICE_OVERWRITE_OPEN_ORDER = "N";
	public static final String SUPPLEMENTARY_SERVICE_CHARGABLE = "Y";
	public static final String SUPPLEMENTARY_SERVICE_NO_CHARGABLE = "N";
	public static final String SUPPLEMENTARY_SERVICE_CHARGE_AMOUNT = "0.0";
	public static final String SUPPLEMENTARY_SERVICE_REPLY_TO_REQUESTER = "Y";

	public static final String SUPPLEMENTARY_SERVICE_RBT ="RBT";
	public static final String SUPPLEMENTARY_SERVICE_GPRS ="GPRS";
	public static final String SUPPLEMENTARY_SERVICE_GPRS5 ="GPRS5";
	public static final String SUPPLEMENTARY_SERVICE_INTERNATIONAL_BARRING ="NSA";
	
	//MyServices
	public static final String MySERVICES_MSG_FORMAT_VALUE ="SERV_STATUS_INQ";
	
	//	Edit Customer Profile
	public static final String EDIT_CUSTOMER_PROFILE_MESSAGE_BODY = "EDIT_CUSTOMER_PROFILE_MESSAGE_BODY";
	public static final String EDIT_CUSTOMER_PROFILE_HOST_NAME = "EDIT_CUSTOMER_PROFILE_HOST_NAME";
	public static final String EMAIL_SMTP_HOST_NAME ="EMAIL_SMTP_HOST_NAME";
	public static final String EDIT_CUSTOMER_PROFILE_HOST_IP = "EDIT_CUSTOMER_PROFILE_HOST_IP";
	public static final String EDIT_CUSTOMER_PROFILE_MAIL_FROM = "EDIT_CUSTOMER_PROFILE_MAIL_FROM";
	public static final String EDIT_CUSTOMER_PROFILE_MAIL_TO_ = "EDIT_CUSTOMER_PROFILE_MAIL_TO_";
	public static final String EDIT_CUSTOMER_PROFILE_NO_OF_EMAILS = "EDIT_CUSTOMER_PROFILE_NO_OF_EMAILS";
	
	
	//OTA Service Type
	public static final String OTA_WAP_MMS_KEY_MESSAGE ="WAP_MMS_SETTINGS";
	public static final String OTA_INTERNET_KEY_MESSAGE ="INTERNET_SETTINGS";
    public static final int OTA_WAP_MMS_TYPE = 1;
    public static final int OTA_INTERNET_TYPE = 2;
    public static final int OTA_CHANNEL_ID = 61;
	
    // Customer Status
    public static final String CUSTSTATUS_FUNC_ID_VALUE ="CUSTOMER_STATUS";
    public static final String CUSTSTATUS_SEC_KEY_VALUE ="kjasdhfjk48938jkdhf";
    public static final String CUSTSTATUS_CHARGABLE_VALUE ="N";
    public static final String CUSTSTATUS_CHARGING_MODE ="Payment";
    public static final String CUSTSTATUS_CHARGE_AMOUNT ="0.0";
    
    public static final String PRIVILEGE_DEFAULT_VALUE ="PRIVILEGE_DEFAULT_VALUE";
    
    //	3G Service
    public static final String THIRD_GENERATION_SECURITY_KEY = "0000";
    public static final String THIRD_GENERATION_OVERWRITE_OPEN_ORDER = "N";
	public static final String THIRD_GENERATION_CHARGABLE = "N";
	public static final String THIRD_GENERATION_CHARGE_AMOUNT = "0.0";
	public static final String THIRD_GENERATION_REPLY_TO_REQUESTER = "Y";
	    
    public static final int VIDEO_TEL_SUB = 702;
    public static final int VIDEO_TEL_UNSUB = 703;
    public static final int VIDEO_STRM_SUB = 704;
    public static final int VIDEO_STRM_UNSUB = 705;
    public static final int GAMING_SUB = 706;
    public static final int GAMING_UNSUB = 707;
    
    public static final String VIDEO_TEL_SUB_FUNC_ID = "VIDEO_TEL_SUB";
    public static final String VIDEO_TEL_UNSUB_FUNC_ID = "VIDEO_TEL_UNSUB";
    public static final String VIDEO_STRM_SUB_FUNC_ID = "VIDEO_STRM_SUB";
    public static final String VIDEO_STRM_UNSUB_FUNC_ID = "VIDEO_STRM_UNSUB";
    public static final String GAMING_SUB_FUNC_ID = "GAMING_SUB";
    public static final String GAMING_UNSUB_FUNC_ID = "GAMING_UNSUB";
    
    public static final int THIRD_GENERATION_REPLY_STATUS_ACCEPTED = 2;
    public static final int THIRD_GENERATION_REPLY_STATUS_FAILED = 3;
    public static final int THIRD_GENERATION_REPLY_STATUS_PENDING = 4;
    public static final int THIRD_GENERATION_REPLY_STATUS_REJECTED = 5;
    public static final int THIRD_GENERATION_REPLY_STATUS_SUCCESSFUL = 6;
    
    public static final String THIRD_GENERATION_REPLY_STATUS_ACCEPTED_MESSAGE = "0";
    public static final String THIRD_GENERATION_REPLY_STATUS_FAILED_MESSAGE = "11";
    public static final String THIRD_GENERATION_REPLY_STATUS_PENDING_MESSAGE = "11";
    public static final String THIRD_GENERATION_REPLY_STATUS_REJECTED_MESSAGE = "11";
    public static final String THIRD_GENERATION_REPLY_STATUS_SUCCESSFUL_MESSAGE = "0";
    
    
    //	TariffSelector
    public static final int RATE_ID_ON_NET = 1;
    public static final int RATE_ID_FIXED_LINE = 2;
    public static final int RATE_ID_OMO = 3;
    public static final int RATE_ID_FNF = 4;

	// Credit Card Payment
    public static final String Messsage_Format = "CreditCardPaymentGateway";
	public static final String Requestor_ChannelId = "EPOR";
	public static final String Requestor_Channel_Function_Inquiry = "CreditCardEquiry";
	public static final String Requestor_Channel_Function_Update = "Update";
	public static final String Requestor_Channel_Function_Reset = "Reset";
	public static final String Requestor_Channel_Function_Cancellation = "Cancellation";
	public static final String Requestor_Channel_Function_Payment = "Payment";
	public static final String Requestor_Channel_Function_REGISTRATION = "Register";
	
	public static final String Return_Code = "0000";
	public static final String Return_Code_Payment_PIN_Code_Error = "0001";
	public static final String Return_Code_Data_Not_Found = "0001";
	public static final String Return_Code_PIN_Code_Error = "0002";
	public static final String Return_Code_No_Inquiry_Data = "0003";
	public static final String Return_Code_Generic_Exception = "9999";
	
	public static final String Keys = "CREDIT_CARD_PAYMENT_KEYS";
	public static final String Key = "CREDIT_CARD_PAYMENT_KEY";
	
	
	//International call barring Module
	public static final String INTERNATIONAL_CALL_BARRING = "INTERNATIONAL_BARRING";
	public static final String INTERNATIONAL_CALL_UNBARRING = "INTERNATIONAL_UNBARRING";
	public static final String INTERNATIONAL_BARRING_NOTIFYBE = "1";
	public static final String INTERNATIONAL_BARRING_REASONID = "3";
	public static final String BARRING_OPERTION_KET = "BARRING";
	public static final String UNBARRING_OPERTION_KET = "UNBARRING";
	public static final String INTERNATIONAL_SUPPLEMENTARY_SERVICE_CHARGABLE = "N";
	public static final String INTERNATIONAL_SUPPLEMENTARY_SERVICE_CHARGA_MODE = "PAYMENT";
	
	// Credit Transfer
	public static final String CREDIT_TRANSFER_FUNCTION_ID = "CREDIT_TRANSFER";
	public static final String CREDIT_TRANSFER_SECURITY_KEY = "0000";
	public static final String CREDIT_TRANSFER_MESSAGE_VERSION = "0000";
	public static final String CREDIT_TRANSFER_REQUESTOR_CHANNEL_ID = "ePortal";
	//public static final String CREDIT_TRANSFER_SR_DATE = SrDate = null;
	public static final String CREDIT_TRANSFER_REQUESTOR_USER_ID = "db2admin";
	public static final String CREDIT_TRANSFER_REQUESTOR_LANGUAGE = "E";
	public static final String CREDIT_TRANSFER_OVER_WRITE_OPEN_ORDER = "";
	public static final String CREDIT_TRANSFER_CHARGEABLE = "";
	public static final String CREDIT_TRANSFER_CHARGING_MODE = "";
	public static final String CREDIT_TRANSFER_CHARGE_AMOUNT = "";
	
	//Family And Friends
	public static final int FAMILY_FRIENDS_ACTIVE_STATUS = 1;
	public static final int FAMILY_FRIENDS_INACTIVE_STATUS = 2;
	public static final String FAMILY_FRIENDS_ADD_COMMAND ="Add";
	public static final String FAMILY_FRIENDS_REPLACE_COMMAND ="Replace";
	public static final String FAMILY_FRIENDS_DELETE_COMMAND ="Delete";
	public static final String FAMILY_FRIENDS_LIST_COMMAND ="List";
	public static final String FAMILY_FRIENDS_FUNC_ID="FAMILY_FRIENDS";
	public static final String FAMILY_FRIENDS_MSG_VER="0000";
	public static final String FAMILY_FRIENDS_SEC_KEY="123";
	public static final String FAMILY_OVER_WRITE_OPEN_ORDER="N";
	public static final String FAMILY_CHARGEABLE="N";
	public static final String FAMILY_CHARGE_AMOUNT="0.0";
	public static final String FAMILY_REASON_ID="1";
	
	//	PTT
	public static final String PTT_TID = "10000";
	public static final String PTT_ACTION_QUERY = "Query";
	public static final String PTT_ACTION_ADD = "Add";
	public static final String PTT_ACTION_DELETE = "Delete";
	public static final String PTT_ACTION_UPDATE = "Update";
	public static final String PTT_ACTION_LOGIN = "LOGIN";
	public static final String PTT_ACTION_LOGOUT = "LOGOUT";
	public static final String PTT_ACTION_RESET = "RESET";
	public static final String PTT_ACTION_PURGE = "PURGE";
	
	public static int PTT_GET_ALL_CONTACTS = 1;
    public static int PTT_ADD_CONTACT = 2;
    public static int PTT_UPDATE_CONTACT = 3;
    public static int PTT_REMOVE_CONTACT = 4;
    public static int PTT_GET_ALL_GROUPS = 5;
    public static int PTT_ADD_GROUP = 6;
    public static int PTT_UPDATE_GROUP = 7;
    public static int PTT_REMOVE_GROUP = 8;
    public static int PTT_GET_GROUP_CONTACTS = 9;
    public static int PTT_ADD_CONTACT_TO_GROUP = 10;
    public static int PTT_REMOVE_CONTACT_FROM_GROUP = 11;
    public static int PTT_GET_PREFERENCES = 12;
    public static int PTT_UPDATE_PREFERENCES = 13;
    public static int PTT_GET_PREFERENCES2 = 14;
    public static int PTT_UPDATE_PREFERENCES2 = 15;
    public static int PTT_GET_BLOCKING_EXCLUSION_LIST = 16;
    public static int PTT_UPDATE_BLOCKING_EXCLUSION_LIST = 17;
    public static int PTT_GET_BLOCKING_EXCLUSION_CONTACT_LIST = 18;
    public static int PTT_ADD_CONTACT_TO_BLOCKING_EXCLUSION_CONTACT_LIST = 19;
    public static int PTT_UPDATE_CONTACT_IN_BLOCKING_EXCLUSION_CONTACT_LIST = 20;
    public static int PTT_REMOVE_CONTACT_FROM_BLOCKING_EXCLUSION_CONTACT_LIST = 21;
    public static int PTT_GET_NOTIFICATION_MESSAGE_LIST = 22;
    
    public static int PTT_CONTACT_ACTION = 101;
    public static int PTT_GROUP_ACTION = 102;
    public static int PTT_PREFERENCES_ACTION = 103;
    public static int PTT_PREFERENCES2_ACTION = 104;
    public static int PTT_BLOCKING_EXCLUSION_LIST_ACTION = 105;
    public static int PTT_BLOCKING_EXCLUSION_CONTACT_LIST_ACTION = 106;
    public static int PTT_NOTIFICATION_MESSAGE_LIST_ACTION = 107;
    
	public static final int PTT_SUCCESS = 1;
	public static final int PTT_INVALID_XML_FORMAT = 2;
	public static final int PTT_ACTIVE_DIRECTORY_UNAVAILABLE = 3;
	public static final int PTT_ATTEMPTING_TO_ADD_SELF = 4;
	public static final int PTT_INVALID_NAME_FORMAT = 5;
	public static final int PTT_MAX_SIZE_REACHED = 6;
	public static final int PTT_ALREADY_EXISTS = 7;
	public static final int PTT_MIN_MDN_MISMATCH = 8;
	public static final int PTT_LOGIN_FAILURE = 9;
	public static final int PTT_PASSWORD_MODIFICATION_FAILURE = 10;
	public static final int PTT_ERROR_MESSAGE = 11;
	public static final int PTT_ENTRY_NOT_FOUND = 12;
	public static final int PTT_UNKNOWN_ERROR = 13;
	public static final int PTT_BUDDY_ADDITION_DENIED = 14;
	public static final int PTT_INVALID_BUDDY_NAME_SIZE = 15;
	public static final int PTT_INVALID_CHARS_IN_BUDDY_NAME = 16;
	public static final int PTT_INVALID_GROUP_NAME_SIZE = 17;
	public static final int PTT_INVALID_CHARS_IN_GROUP_NAME = 18;
	
	public static final String PTT_ON = "on";
	public static final String PTT_OFF = "off";
	
	public static final String PTT_MODE_ALLOW = "ALLOW";
	public static final String PTT_MODE_BLOCK = "BLOCK";
	
	public static final String PTT_ADDITION_ALLOW_EDIT = "true";
	public static final String PTT_ADDITION_ALLOW_QUERY = "1";
	public static final String PTT_ADDITION_BLOCK_EDIT = "false";
	public static final String PTT_ADDITION_BLOCK_QUERY = "0";
	
	//	PTT Subscribe
	public static final String PTT_SUBSCRIBE_FUNC_VALUE = "SUPPLEMENTARY_SERVICE";
	public static final String PTT_SUBSCRIBE_MESSAGE_VERSION_VALUE = "0000";
	public static final String PTT_SUBSCRIBE_REQUESTOR_CHANNEL_ID_VALUE = "ePortal";
	public static final String PTT_SUBSCRIBE_SECURITY_KEY_VALUE = "123";
	public static final String PTT_SUBSCRIBE_OVERWRITE_OPEN_ORDER_VALUE = "N";
	public static final String PTT_SUBSCRIBE_CHARGEABLE_VALUE = "Y";
	public static final String PTT_SUBSCRIBE_CHARGE_AMOUNT_VALUE = "0.0";
	public static final String PTT_SUBSCRIBE_REPLY_TO_REQUESTER_VALUE = "Y";
	public static final String PTT_SUBSCRIBE_SUPPLEMENTARY_SERVICE = "PTT";
	
	public static final String PTT_SUBSCRIBE = "subscribe";
	public static final String PTT_UNSUBSCRIBE = "unsubscribe";

	//Web2SMS
	public static final String W2SMS_Req_FuncId = "SEND_SMS";
	public static final String W2SMS_Req_SecurityKey = "asdcsdasdasd";
	public static final String W2SMS_Req_MsgVersion = "0000";
	public static final String W2SMS_Req_RequestorChannelId = "ePortal";
	//public static final String W2SMS_Req_SrDate = "";
	public static final String W2SMS_Req_RequestorUserId = "ePortal";
	public static final String W2SMS_Req_RequestorLanguage = "E";
	public static final String W2SMS_Req_Chargeable = "N";
	public static final String W2SMS_Req_ChargeAmount = "0.0";
	
	//ManageComplaint
	public static final String COMPLAINT_ADD_FUNC_ID_VALUE="ADD_COMPLAINT";
	public static final String COMPLAINT_VIEW_FUNC_ID_VALUE="VIEW_COMPLAINT";
	public static final String COMPLAINT_LIST_FUNC_ID_VALUE="LIST_COMPLAINT";
	public static final String COMPLAINT_LIST_MANDATORY_FIELD_FUNC_ID_VALUE="LIST_MANDATORY";

	//	Corporate and normal lines types
 	public static final int LINE_TYPE_CONSUMER = 11;
	public static final int LINE_TYPE_CORPORATE_LINE = 12;
	public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON = 13;
	public static final int LINE_TYPE_CORPORATE_AUTHORIZED_PERSON_CHANGE_IDENTITY = 14;
	
	//	Corporate calls sorting
	public static final int CALL_TYPE_PERSONAL = 1;
	public static final int CALL_TYPE_BUSINESS = 2;
	
	//Content provider
	public static final String CONTENT_PROVIDER_FUNC_VALUE = "SUPPLEMENTARY_SERVICE";
	public static final String CONTENT_PROVIDER_SECURITY_KEY = "1234421211";
	public static final String CONTENT_PROVIDER_CHARGE_AMOUNT = "0.0";
	public static final String CONTENT_PROVIDER_CHARGEABLE = "Y";
	public static final String CONTENT_PROVIDER_OVERWRITE = "N";
	public static final String CONTENT_PROVIDER_LANG = "E";
	public static final String CONTENT_PROVIDER_REQUESTOR_CHANNEL = "ePortal";
	public static final String CONTENT_PROVIDER_SUPP_SERVICE = "ContentProvider";
	public static final String CONTENT_PROVIDER_REPLY_TO_REQUSTOR = "Y";
	public static final String CONTENT_PROVIDER_OPERATION_SUBSCRIBE = "subscribe";
	public static final String CONTENT_PROVIDER_OPERATION_UNSUBSCRIBE = "unsubscribe";
	public static final String CONTENT_PROVIDER_OPERATION_LIST_SUBSCRIBE = "List_Sub";
	public static final String CONTENT_PROVIDER_OPERATION_LIST_UNSUBSCRIBE = "List_Unsub";
	public static final String CONTENT_PROVIDER_DATE_FORMATE = "yyyyMMdd";
	public static final String CONTENT_PROVIDER_DATE_FORMATE_24H = "yyyyMMddkkmmss";
	public static final String CONTENT_PROVIDER_PERIOD_COUNT = "30";
	public static final String CONTENT_PROVIDER_COMMAND_ARABIC="A";
	public static final String CONTENT_PROVIDER_COMMAND_ENGLISH="E";
	public static final int CONTENT_PROVIDER_COMMAND_LANG_ARABIC_VALUE=1;
	public static final int CONTENT_PROVIDER_COMMAND_LANG_ENGLISH_VALUE=2;
	
	
	
	//Loyalty
	public static final String LOYALTY_CHARGE_AMOUNT = "0.0";
	public static final String LOYALTY_CHARGEABLE = "N";
	public static final String LOYALTY_OVERWRITE = "N";
	
	public static final String FUNCTION_ID_LOYALTY_SIMPLE_INQUIRY = "LOYALTY_INQUIRY_SIMPLE";
	public static final String FUNCTION_ID_LOYALTY_FULL_INQUIRY = "LOYALTY_INQUIRY";
	public static final String FUNCTION_ID_LOYALTY_POINTS_EXPIRY = "LOYALTY_INQUIRY_EXPIRY";
	public static final String FUNCTION_ID_LOYALTY_INQUIRY_ITEMS = "LOYALTY_INQUIRY_ITEMS";
	public static final String FUNCTION_ID_LOYALTY_REDEMPTION = "LOYALTY_REDEMPTION";
	
	public static final String ITEM_CATEGORY_REDEMPTION_TYPE_INTERNAL = "Internal";
	public static final String ITEM_CATEGORY_REDEMPTION_TYPE_EVOUCHER = "Evoucher";
	public static final String ITEM_CATEGORY_REDEMPTION_TYPE_INTERNAL_EVOUCHER = "Internal,Evoucher";
	
	public static final String FULL_INQUIRY_TRANSACTION_TYPE_EVOUCHER = "Evouchers";
	public static final String FULL_INQUIRY_TRANSACTION_TYPE_EARNINGS = "Earnings";
	public static final String FULL_INQUIRY_TRANSACTION_TYPE_REDEMPTIONS = "Redemptions";
	public static final String FULL_INQUIRY_TRANSACTION_TYPE_LOST_POINTS = "LostPoints";
	
	public static final String MESSAGE_VERSION_LOYALTY = "0000";
	public static final String SECURITY_KEY_LOYALTY = "sedasfsdfwertwsfgwer";
	public static final String CHANNEL_ID_LOYALTY = "CRM";
	public static final String SERVICE_REQUESTOR_ID_LOYALTY = "SR_";
	
	public static final String SKYWARDS_ITEM_SUB_CATEGORY = "SKYWARDS";//added by Suresh V for Skywards in loyalty
	
	//	To be added to XML request to specify which MQ at ISL will be used
	public static final int LOYALTY_GET_ITEM_CATALOG = 1;
	public static final int LOYALTY_FULL_INQUIRY = 2;
	public static final int LOYALTY_SIMPLE_INQUIRY = 3;
	public static final int LOYALTY_POINT_EXPIRY = 4;
	public static final int LOYALTY_REDEMPTION_EVOUCHER = 5;
	public static final String LOYALTY_ACTION="action";
	public static final String LOYALTY_GROUP_BY_NONE="None";
	public static final String LOYALTY_GROUP_BY_DAILY="Daily";
	public static final String LOYALTY_GROUP_BY_MONTHLY="Monthly";
	
	public static final int PACKAGE_ID_PREPAID_ANEES         = 1;
	public static final int PACKAGE_ID_PREPAID_WAFEER        = 2;
	public static final int PACKAGE_ID_PREPAID_FALLA         = 3;
	public static final int PACKAGE_ID_PREPAID_RIHAL         = 0;
	public static final int PACKAGE_ID_PREPAID_RED1          = 4;
	public static final int PACKAGE_ID_PREPAID_RED2          = 5;
	public static final int PACKAGE_ID_PREPAID_SKY_1G        = 7;
	public static final int PACKAGE_ID_PREPAID_SKY_5G        = 8;
	public static final int PACKAGE_ID_PREPAID_SKY_UNLIMITED = 9;
	
	
	//	PTT promotion
	public static final String MESSAGE_VERSION_PTT_PROMO       = "0000";
	public static final String SECURITY_KEY_PTT_PROMO          = "securitykey";
	public static final String CHANNEL_ID_PTT_PROMO            = "ePortal";
	public static final String SERVICE_REQUESTOR_ID_PTT_PROMO  = "SR_";
	public static final String SUPPLEMENTARY_SERVICE_PTT_PROMO = "PTTPROMO";
	public static final String OPERATION_PTT_PROMO_SUBSCRIBE   = "subscribe";
	public static final String OPERATION_PTT_PROMO_INQUIRY     = "inquiry";
	
	
	// Email Invoice
	public static final String EI_Inquiry_FuncId = "Email_Invoice_Inquiry";
	public static final String EI_Inquiry_SecurityKey = "kjasdhfjk48938jkdhf";
	public static final String EI_Inquiry_MsgVersion = "0000";
	public static final String EI_Inquiry_RequestorChannelId = "Siebel";
	public static final String EI_Inquiry_RequestorUserId = "s.ali";
	public static final String EI_Inquiry_RequestorLanguage = "E";

	public static final String EI_SubUnsub_FuncId = "Email_Invoice";
	public static final String EI_SubUnsub_SecurityKey = "";
	public static final String EI_SubUnsub_MsgVersion = "0000";
	public static final String EI_SubUnsub_RequestorChannelId = "ePortal";
	public static final String EI_SubUnsub_RequestorUserId = "";
	public static final String EI_SubUnsub_RequestorLanguage = "E";
	public static final String EI_SubUnsub_OverwriteOpenOrder = "Y";
	public static final String EI_SubUnsub_Chargeable = "N";
	public static final String EI_SubUnsub_ChargingMode = "";
	public static final String EI_SubUnsub_ChargeAmount = "0";
	public static final String EI_SubUnsub_DealerId = "";
	public static final String EI_SubUnsub_ShopId = "";
	public static final String EI_SubUnsub_AgentId = "";
	
	public static final String EI_SubUnsub_Operation_ServiceChange = "servicechange";
	public static final String EI_SubUnsub_Operation_Subscribe = "subscribe";
	public static final String EI_SubUnsub_Operation_Unsubscribe = "unsubscribe";
	
	public static final String EI_SubUnsub_InvoiceMechanism_Email = "Email";
	public static final String EI_SubUnsub_InvoiceMechanism_Vassal = "Vassal";
	public static final String EI_SubUnsub_InvoiceMechanism_POBOX = "POBOX";
	public static final String EI_SubUnsub_InvoiceMechanism_POSTAL = "Postal";
	
	public static final String EI_InvoiceMechanism_EditEmailMode = "1";
	
	public static final String EI_InvoiceMechanism_Email = "1";
	public static final String EI_InvoiceMechanism_POBOX = "2";
	public static final String EI_InvoiceMechanism_None = "0";
	
   //Lookup Table
	public static final String LOOKUP_TABLE_REGION = "1";
	public static final String LOOKUP_TABLE_CITY = "2";
	public static final String LOOKUP_TABLE_PAYMENT_CHANNEL = "3";
	public static final String LOOKUP_TABLE_PAYMENT_STATUS = "4";
	public static final String LOOKUP_TABLE_DEVICE_MODEL = "5";
	public static final String LOOKUP_TABLE_PACKAGE_NAME = "6";
	public static final String LOOKUP_AGE_ID = "7";
	public static final String LOOKUP_GENDER_ID = "8";
	public static final String LOOKUP_COUNTRIES_ID = "9";
	public static final String LOOKUP_ACTIVATION_ID = "10";
	public static final int LOOKUP_CUSTOMER_PROFILE = 11;

	 
	// User Information Reqyeat
	public static final int IR_EMAILME_FINANCIAL_RESULTS = 1;
	public static final int IR_EMAILME_AGM_EVENT_NOTIFICATION = 2;
	public static final int IR_EMAILME_IR_EVENT_NOTIFICATION = 4;
	
	// Ring Back Tone
	public static final String RBT_FUNCID_QUERY_TONE = "QUERY_TONE";
	public static final String RBT_FUNCID_VIEW_TONE_DETAILS = "VIEW_TONE_DETAILS";
	public static final String RBT_FUNCID_ORDER_TONE = "ORDER_TONE";
	public static final String RBT_FUNCID_DELETE_INBOX_TONE = "DELETE_INBOX_TONE";
	public static final String RBT_FUNCID_QUERY_INBOX_TONE = "QUERY_INBOX_TONE";
	public static final String RBT_FUNCID_CREATE_TONE = "CREATE_RULE";
	public static final String RBT_FUNCID_DELETE_TONE = "DELETE_RULE";
	public static final String RBT_FUNCID_LIST_TONE = "LIST_RULE";
	
	//Home Zone
	public static final String HZ_Func_Id = "HOME_ZONE";
	public static final String HZ_SecurityKey = "123";
	public static final String HZ_MsgVersion = "0000";
	public static final String HZ_RequestorChannelId = "EPOR";
//	public static final String HZ_SrDate = "20070718164132";
	public static final String HZ_RequestorUserId = "HOMEZONE";
	public static final String HZ_RequestorLanguage = "E";
	public static final String HZ_OverwriteOpenOrder = "Y";
	public static final String HZ_Chargeable = "N";
	public static final String HZ_ChargeAmount = "0.0";

	public static final String HZ_SupplementaryService = "HOME_ZONE";
	public static final String HZ_Operation_PROVISION = "PROVISION";
	public static final String HZ_Operation_SUBSCRIBE = "SUBSCRIBE";
	public static final String HZ_Operation_UNSUBSCRIBE = "UNSUBSCRIBE";
	public static final String HZ_ReplyToRequester_Y = "Y";
	public static final String HZ_ReplyToRequester_N = "N";
	public static final String SUBSCRIPTION_TYPE_MONTHLY="MONTHLY";
	
	public static final int PROMOTION_INQUIRY   = 1;
	public static final int PROMOTION_SUBSCRIBE = 2;
	
	public static final int SecondaryEmailStatus_Active = 1;
	public static final int SecondaryEmailStatus_Inactive = 2;
	
	public static final String SecondaryEmailStatus_PENDING = "PENDING";
	public static final String SecondaryEmailStatus_UNPENDING = "UNPENDING";
	
	public static final String SecondaryEmailStatus_DELETE_LDAP = "LDAP";
	
	public static final int SECONDARY_EMAIL_DELETE_LDAP = 1;
	
	public static final int SKY_EMAIL_SERVICE_FAIL                   = 1;
	public static final int SKY_EMAIL_SERVICE_ENTERED_BY_BATCH       = 2;
	public static final int SKY_EMAIL_SERVICE_ENTERED_BY_THIRD_PARTY = 3;
	public static final int SKY_EMAIL_SERVICE_SCHEDULED              = 4;
	public static final int SKY_EMAIL_SERVICE_SCHEDULED_CANCELED     = 5;
	
	public static final int SKY_EMAIL_ERROR_SUCCESS                  = 0;
	public static final int SKY_EMAIL_ERROR_CAPTCHA_REQUIRED         = 1600;
	public static final int SKY_EMAIL_ERROR_AUTHENTICATION_EXCEPTION = 1601;
	public static final int SKY_EMAIL_ERROR_SSL_HANDSAKE_EXCEPTION   = 1602;
	public static final int SKY_EMAIL_ERROR_Exception = 9999;
	
	public static final int SKY_TYPE_UNDEFINED                 = -1;
	public static final int SKY_TYPE_NO_INTERNET_BUNDLE        = 0;
	public static final int SKY_TYPE_INTERNET_BUNDLE_5M        = 1;
	public static final int SKY_TYPE_INTERNET_BUNDLE_20M       = 2;
	public static final int SKY_TYPE_INTERNET_BUNDLE_UNLIMITED = 3;
	public static final int SKY_TYPE_INTERNET_BUNDLE_1G        = 4;
	public static final int SKY_TYPE_INTERNET_BUNDLE_5G        = 5;
	public static final int SKY_TYPE_INTERNET_BUNDLE_30M       = 6;
	public static final int SKY_TYPE_INTERNET_BUNDLE_100M      = 7;
	public static final int SKY_TYPE_MOBILY_CONNECT            = 8;
	public static final String GPRS_OPERATION_5M = "5MB";
	public static final String GPRS_OPERATION_20M = "20MB";
	public static final String GPRS_OPERATION_30M = "30MB";
	public static final String GPRS_OPERATION_100M = "100MB";
	public static final String GPRS_OPERATION_UNLIMITED = "Unlimited";
	public static final String GPRS_OPERATION_1G = "1GB";
	public static final String GPRS_OPERATION_5G = "5GB";
	
	public static final String INTERNET_BUNDLE_5MB ="5MB";
	public static final String INTERNET_BUNDLE_20MB ="20MB";
	public static final String INTERNET_BUNDLE_30MB = "30MB";
	public static final String INTERNET_BUNDLE_100MB = "100MB";
	public static final String INTERNET_BUNDLE_1GB = "1GB";
	public static final String INTERNET_BUNDLE_5GB = "5GB";
	public static final String INTERNET_BUNDLE_UNLIMITED = "Unlimited";
	public static final String MOBILY_CONNECT = "MOBILY CONNECT";

	public static final String INTERNET_BUNDLE_5M ="5M";
	public static final String INTERNET_BUNDLE_20M ="20M";
	public static final String INTERNET_BUNDLE_30M = "30M";
	public static final String INTERNET_BUNDLE_100M = "100M";
	public static final String INTERNET_BUNDLE_1G = "1G";
	public static final String INTERNET_BUNDLE_5G = "5G";

	public static final String IntrnetBundleStatus_unsubscribe = "0";
	public static final String IntrnetBundleStatus_subscribe = "1";
	public static final String IntrnetBundleStatus_replace = "2";
	
	public static final int USER_TYPE_UNDEFINED      = -1;
	public static final int USER_TYPE_PORTAL         = 0;
	public static final int USER_TYPE_SKY            = 1;
	//	Should be removed. Suspension status is showed by SKY_EMAIL_STATUS
	//public static final int USER_TYPE_SKY_SUSPENDED  = 2;
	
	public static final String USER_ACCOUNT_BLOCK = "1";
	public static final String USER_ACCOUNT_UNBLOCK = "0";
	public static final String USER_ACTIVATION_CODE = "1";
	
	public static final String Mobily_International_Code = "9665";
	
	public static final int SKY_EMAIL_STATUS_UNDEFINED            = -1;
	public static final int SKY_EMAIL_STATUS_NEVER_CREATED_BEFORE = 0;
	public static final int SKY_EMAIL_STATUS_ACTIVE               = 1;
	public static final int SKY_EMAIL_STATUS_GRACE                = 2;
	public static final int SKY_EMAIL_STATUS_SUSPENDED            = 3;
	public static final int SKY_EMAIL_STATUS_DELETED              = 4;
	public static final int SKY_EMAIL_STATUS_SCHEDULED_TO_CREATE  = 5;
	
//	public static final int SKY_EMAIL_STATUS_CANCEL_SCHEDULED_TO_CREATE = 6;
	
    // new types not defined yet
	public static final String MOBILY_SKY_FUNC_ID_ACTIVE_MESSAGE_ID_AR = "";
    public static final String MOBILY_SKY_FUNC_ID_ACTIVE_MESSAGE_ID_EN = "";

    public static final String MOBILY_SKY_FUNC_ID_GRACE_MESSAGE_ID_AR = "";
    public static final String MOBILY_SKY_FUNC_ID_GRACE_MESSAGE_ID_EN = "";

    public static final String MOBILY_SKY_FUNC_ID_SCHEDULED_TO_CREATE_MESSAGE_ID_AR = "";
    public static final String MOBILY_SKY_FUNC_ID_SCHEDULED_TO_CREATE_MESSAGE_ID_EN = "";
    
    public static final String MOBILY_SKY_FUNC_ID_CANCEL_SCHEDULED_TO_CREATE_MESSAGE_ID_AR = "";
    public static final String MOBILY_SKY_FUNC_ID_CANCEL_SCHEDULED_TO_CREATE_MESSAGE_ID_EN = "";
    
    public static final String MOBILY_SKY_FUNC_ID_CREATE_MESSAGE_ID_AR = "714";
    public static final String MOBILY_SKY_FUNC_ID_CREATE_MESSAGE_ID_EN = "713";
  
    public static final String MOBILY_SKY_FUNC_ID_SUSPEND_MESSAGE_ID_AR = "716";
    public static final String MOBILY_SKY_FUNC_ID_SUSPEND_MESSAGE_ID_EN = "715";

    public static final String MOBILY_SKY_FUNC_ID_RESTORE_MESSAGE_ID_AR = "718";
    public static final String MOBILY_SKY_FUNC_ID_RESTORE_MESSAGE_ID_EN = "717";
	
    public static final String McnProcessStatus_NotEportalUser = "notEportalUser";
    public static final String McnProcessStatus_NotSkyUser = "notEportalUser";
    public static final String McnProcessStatus_CreateEmail = "create";
    public static final String McnProcessStatus_SuspendEmail = "suspend";
    public static final String McnProcessStatus_RestoreEmail = "restore";
    public static final String McnProcessStatus_NothingChange = "nothingChange";

	public static final int SKY_ACTION_REASON_OG_BARRED                   = 1;
	public static final int SKY_ACTION_REASON_FULL_BARRED                 = 2;
	public static final int SKY_ACTION_REASON_UNSUBSCRIBED                = 3;
	public static final int SKY_ACTION_REASON_OG_BARRED_AND_UNSUBSCRIBED  = 4;
	public static final int SKY_ACTION_REASON_FULL_BARRED_AND_UNSUBSCRIBE = 5;
	
	public static final int SKY_SECONDARY_EMAILS_ACTION_REASON_NO_REASON = 0;
	public static final int SKY_SECONDARY_EMAILS_ACTION_REASON_SYSTEM    = 1;
	public static final int SKY_SECONDARY_EMAILS_ACTION_REASON_PRIMARY   = 2;
		
	//Phone Book
	public static final int PHONE_BOOK_FUNCTION_UNDEFINE = 0;
	public static final int PHONE_BOOK_FUNCTION_VSMS = 1;
	public static final int PHONE_BOOK_ERROR_CONTACT_ALREADY_EXIST = 10001;
	public static final int PHONE_BOOK_ERROR_GROUP_ALREADY_EXIST = 10002;
		
	public static final String BULK_CREDIT_CARD_PAYMENT_TRANSACTION_STATUS_INPROCESS = "inprocess";
	
	// Mobily Promotion
	public static final int MOBILY_PROMOTION_SUBSCRIBE = 1;
	public static final int MOBILY_PROMOTION_INQUIRY = 2;
	public static final int MOBILY_PROMOTION_ACKNOWLADGE = 1;

	public static final String ACTIVATION_CODE_SMS_ID_AR = "652";
	public static final String ACTIVATION_CODE_SMS_ID_EN = "651";
	public static final String FORGOT_USERNAME_SMS_ID_AR = "389";
	public static final String FORGOT_USERNAME_SMS_ID_EN = "388";
	public static final String FORGOT_USERNAME_WITH_MESSEGE_ID_SMS_ID_AR = "390";
	public static final String FORGOT_USERNAME_WITH_MESSEGE_ID_SMS_ID_EN = "391";
	public static final String FORGOT_PASSWORD_SMS_ID_AR = "393";
	public static final String FORGOT_PASSWORD_SMS_ID_EN = "392";

	// BMS 
	public static final String BMS_FUNC_ID = "REGISTER_TOKEN";
	
	//	Footprint
	public static final int FOOTPRINT_MODULE_CALL_CENTER           = 2;
	public static final int FOOTPRINT_MODULE_SUPPLEMENTARY_SERVICE = 3;
	public static final int FOOTPRINT_MODULE_BILLING               = 1;
	public static final int FOOTPRINT_MODULE_LOGIN                 = 4;
	
	public static final int FOOTPRINT_EVENT_LOGIN                  = 35;
	
	public static final int FOOTPRINT_EVENT_BILLING_MYBILL          = 36;
	public static final int FOOTPRINT_EVENT_BILLING_PHONE_USAGE     = 37;
	public static final int FOOTPRINT_EVENT_BILLING_BILL_HISTORY    = 38;
	public static final int FOOTPRINT_EVENT_BILLING_PAYMENT_HISTORY = 39;
	public static final int FOOTPRINT_EVENT_BILLING_UNBILLED_CALLS  = 40;
	public static final int FOOTPRINT_EVENT_BALANCE                 = 41;
	public static final int FOOTPRINT_EXPORT_PDF                 	= 67;
	public static final String BILL_PDF_FILE_NOT_FOUND = "BILL PDF NOT FOUND";
	
	// Corporate
	public static final String FUNC_ID_CORPORATE_BALANCE_INQUIRY= "CORPORATE_BALANCE_INQUIRY";
	public static final String BACKEND_CHANNEL_PORTAL = "Portal";
	public static final String MODE_ALL = "ALL";
	
	// Extract Invoices
	public static final String FUNC_ID_EXTRACT_INVOICE_BILL_INQUIRY = "EXTRACT_INVOICE_BILL_INQUIRY";
	public static final String SR_CORP= "SR_CORP_";
	
	// Payment History
	public static final String MSG_FORMAT_PAYMENT = "Payment_INQ";
	
	//Xenos New bill download
	public static final String TIMESTAMP_FORMAT_MONTH_YEAR = "MMyyyy";
	
	public static final String VALUE_Y = "Y";
	public static final String VALUE_N = "N";
}	
