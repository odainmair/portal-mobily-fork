package sa.com.mobily.eportal.common.service.vo;


/**
 * 
 * @author n.gundluru.mit
 *
 */
public class PaymentNotifyReplyVO {

	private String lineNumber;	
	private String partnerRefNumber;
	private String referenceNumber;
	private String errorCode;
	private String errorMessage;
	/**
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}
	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	/**
	 * @return the partnerRefNumber
	 */
	public String getPartnerRefNumber() {
		return partnerRefNumber;
	}
	/**
	 * @param partnerRefNumber the partnerRefNumber to set
	 */
	public void setPartnerRefNumber(String partnerRefNumber) {
		this.partnerRefNumber = partnerRefNumber;
	}
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	
}