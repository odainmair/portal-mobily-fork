package sa.com.mobily.eportal.cacheinstance.valueobjects;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CorporateSubscriptionVO extends BaseVO
{
	private static final long serialVersionUID = -768177583689232692L;

	private long subscriptionId;

	private long userAccountId;

	private String masterBillingAccount;

	private String mobile;

	private String commercialId;

	private int userActivated;

	private String isSubAdmin;
	
	private String status;
	
	private int isPrimary;
	
	public String getIsSubAdmin()
	{
		return isSubAdmin;
	}

	public void setIsSubAdmin(String isSubAdmin)
	{
		this.isSubAdmin = isSubAdmin;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public int getIsPrimary()
	{
		return isPrimary;
	}

	public void setIsPrimary(int isPrimary)
	{
		this.isPrimary = isPrimary;
	}

	public long getSubscriptionId()
	{
		return subscriptionId;
	}

	public void setSubscriptionId(long subscriptionId)
	{
		this.subscriptionId = subscriptionId;
	}

	public long getUserAccountId()
	{
		return userAccountId;
	}

	public void setUserAccountId(long userAccountId)
	{
		this.userAccountId = userAccountId;
	}

	public String getMasterBillingAccount()
	{
		return masterBillingAccount;
	}

	public void setMasterBillingAccount(String masterBillingAccount)
	{
		this.masterBillingAccount = masterBillingAccount;
	}

	public String getMobile()
	{
		return mobile;
	}

	public void setMobile(String mobile)
	{
		this.mobile = mobile;
	}

	public String getCommercialId()
	{
		return commercialId;
	}

	public void setCommercialId(String commercialId)
	{
		this.commercialId = commercialId;
	}

	public int getUserActivated()
	{
		return userActivated;
	}

	public void setUserActivated(int userActivated)
	{
		this.userActivated = userActivated;
	}
}