/*
 * Created on Jan 20, 2010
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sa.com.mobily.eportal.common.service.vo;


/**
 * @author r.agarwal.mit
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ConnectOTSVO {

	private String msisdn = "";
    private String idType = "";
    private String firstName = "";
    private String lastName = "";
    private String idNumber = "";
    private String emailAddress = "";
    private String loginId = "";
    private String cpeNumber = "";
    private String ip = "";
    private String accountNumber = "";
    private String errorCode = "";
    private String errorMessage = "";
    
    private String fullName = "";
    private String userName = "";
    private String userId = "";
    private boolean isValidPinCode = false;
    private String voucherNumber = "";
    private String simNumber = "";
    
    private String packageId = "";
    private String pricingPlan = "";
    private String locale = null;
    private String idExpiryDate = null;
    private String functionId = null;
    private int projectId = 0;
    private String operation = null;
    
    //Business OTS
    private String billingAccountNumber = null;
    private String companyDocumentId = null;
    
    //CITC rule chage: hold customer's iqama/national id.
    public String customerId = null;
    
    
    
    
    
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return the billingAccountNumber
	 */
	public String getBillingAccountNumber() {
		return billingAccountNumber;
	}
	/**
	 * @param billingAccountNumber the billingAccountNumber to set
	 */
	public void setBillingAccountNumber(String billingAccountNumber) {
		this.billingAccountNumber = billingAccountNumber;
	}
	/**
	 * @return the companyDocumentId
	 */
	public String getCompanyDocumentId() {
		return companyDocumentId;
	}
	/**
	 * @param companyDocumentId the companyDocumentId to set
	 */
	public void setCompanyDocumentId(String companyDocumentId) {
		this.companyDocumentId = companyDocumentId;
	}
	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the functionId
	 */
	public String getFunctionId() {
		return functionId;
	}
	/**
	 * @param functionId the functionId to set
	 */
	public void setFunctionId(String functionId) {
		this.functionId = functionId;
	}
	/**
	 * @return the idExpiryDate
	 */
	public String getIdExpiryDate() {
		return idExpiryDate;
	}
	/**
	 * @param idExpiryDate the idExpiryDate to set
	 */
	public void setIdExpiryDate(String idExpiryDate) {
		this.idExpiryDate = idExpiryDate;
	}
	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}
	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getPackageId() {
		return packageId;
	}
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	public String getPricingPlan() {
		return pricingPlan;
	}
	public void setPricingPlan(String pricingPlan) {
		this.pricingPlan = pricingPlan;
	}
	public String getSimNumber() {
		return simNumber;
	}
	public void setSimNumber(String simNumber) {
		this.simNumber = simNumber;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public boolean isValidPinCode() {
		return isValidPinCode;
	}
	public void setValidPinCode(boolean isValidPinCode) {
		this.isValidPinCode = isValidPinCode;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getIdType() {
		return idType;
	}
	public void setIdType(String idType) {
		this.idType = idType;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getCpeNumber() {
		return cpeNumber;
	}
	public void setCpeNumber(String cpeNumber) {
		this.cpeNumber = cpeNumber;
	}
	/**
	 * @return the operation
	 */
	public String getOperation() {
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	
}