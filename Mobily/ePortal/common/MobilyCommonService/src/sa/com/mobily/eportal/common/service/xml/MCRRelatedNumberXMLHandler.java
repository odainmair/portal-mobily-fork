package sa.com.mobily.eportal.common.service.xml;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.apache.xerces.dom.DocumentImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.constant.ErrorIfc;
import sa.com.mobily.eportal.common.service.constant.TagIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.XMLUtility;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersReplyVO;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersReqVO;
import sa.com.mobily.eportal.common.service.vo.MCRRelatedNumbersVO;

/**
 * @author n.gundluru.mit
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class MCRRelatedNumberXMLHandler {
    private static final Logger log = LoggerInterface.log;

    
    /**
		<CR_REQUEST_MESSAGE>
			<CR_HEADER>
				<FuncId>GENERAL_INQUIRY</FuncId>
				<MsgVersion>0000</MsgVersion>
				<RequestorChannelId>BSL</RequestorChannelId>
				<RequestorSrId>SR_1577393287</RequestorSrId>
				<RequestorUserId>PF100458</RequestorUserId>
				<RequestorLanguage>E</RequestorLanguage>
			</CR_HEADER>
			<InquiryType>RELATED_LINES_SR3587_MSISDN | RELATED_LINES_SR3587_ACCTNO</InquiryType>
			<Params>
				<Param>
					<Key>MSISDN | ACCTNO</Key>
					<Value>966500100050 | 992008020600000133</Value>
				</Param>
			</Params>
		</CR_REQUEST_MESSAGE> 
     */
    
    /**
     * Generate the XML request for related numbers
     * 
     * @param requestVO
     * @return
     * @throws MobilyCommonException
     */
    public String generateRelatedNumbersXMLRequest(MCRRelatedNumbersReqVO requestVo) throws MobilyCommonException {
        log.info("MCRRelatedNumberXMLHandler > generateRelatedNumbersXMLRequest > for Customer  ["+requestVo.getMsisdn()+"] Called");
        String requestMessage = "";
        try {
        	String msisdn = FormatterUtility.checkNull(requestVo.getMsisdn());
            if (msisdn == null
                    || "".equals(msisdn)) {
                
            	log.fatal("MCRRelatedNumberXMLHandler > generateRelatedNumbersXMLRequest > MSISDN is NULL:");
                throw new MobilyCommonException(ErrorIfc.NUMBER_NOT_FOUND_ECEPTION);
            }
            Document doc = new DocumentImpl();
            Element root = doc.createElement(TagIfc.TAG_CR_REQUEST_MESSAGE);
	            Element header = XMLUtility.openParentTag(doc,TagIfc.TAG_CR_HEADER);
	            	XMLUtility.generateElelemt(doc, header, TagIfc.TAG_FUNC_ID,ConstantIfc.FUNC_ID_MCR_RELATED);
	            	XMLUtility.generateElelemt(doc, header, TagIfc.MSG_VERSION,ConstantIfc.PANDA_MESSAGE_VERSION);
	            	XMLUtility.generateElelemt(doc, header,TagIfc.REQUESTOR_CHANNEL_ID, ConstantIfc.CHANNEL_ID_VALUE);
	            	XMLUtility.generateElelemt(doc, header,TagIfc.TAG_REQUESTOR_SR_ID, ConstantIfc.MCR_SR_ID_VALUE);
	            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_USER_ID,requestVo.getRequestorUserId());
	            	XMLUtility.generateElelemt(doc, header, TagIfc.REQUESTOR_LANG,ConstantIfc.LANG_VALUE);
	            XMLUtility.closeParentTag(root, header);
	            
	            //body
		            if(msisdn.length() > 12)
		            	XMLUtility.generateElelemt(doc, root, TagIfc.TAG_INQUIRY_TYPE, ConstantIfc.CUSTOMER_INFO_INQ_BY_ACCOUNT);
		            else
		            	XMLUtility.generateElelemt(doc, root, TagIfc.TAG_INQUIRY_TYPE, ConstantIfc.CUSTOMER_INFO_INQ_BY_MSISDN);
		            
					//gernerate Params element
					Element params = XMLUtility.openParentTag(doc, TagIfc.TAG_PARAMS);
			    		Element paramElement = XMLUtility.openParentTag(doc, TagIfc.TAG_PARAM);
			            if(msisdn.length() > 12)
				    		XMLUtility.generateElelemt(doc, paramElement, TagIfc.TAG_KEY, ConstantIfc.CUSTOMER_INFO_ACCOUNT);
			            else
				    		XMLUtility.generateElelemt(doc, paramElement, TagIfc.TAG_KEY, ConstantIfc.CUSTOMER_INFO_MSISDN);

			    		XMLUtility.generateElelemt(doc, paramElement, TagIfc.TAG_VALUE, msisdn);
		    		XMLUtility.closeParentTag(params, paramElement);
		    		
	    		XMLUtility.closeParentTag(root, params);
	            
            doc.appendChild(root);
            requestMessage = XMLUtility.serializeRequest(doc);
        } catch (java.io.IOException e) {
            log.fatal("MCRRelatedNumberXMLHandler > generateRelatedNumbersXMLRequest >IOException > " + e.getMessage());
            throw new SystemException(e);
        } catch (Exception e) {
            log.fatal("MCRRelatedNumberXMLHandler > generateRelatedNumbersXMLRequest > Exception > "+ e.getMessage());
            throw new SystemException(e);
        }
        return requestMessage;
    }

    /**
     * 
     * @param xmlReply
     * @return
     */
	public static MCRRelatedNumbersReplyVO parseReleatedMsisdnReply(String xmlReply) {
		
		log.debug("MCRRelatedNumberXMLHandler > parseReleatedMsisdnReply :: start");
		
		MCRRelatedNumbersReplyVO relatedNumbersReplyVO = new MCRRelatedNumbersReplyVO();
		
	   	try {
			if (xmlReply == null || xmlReply == "")
					throw new SystemException();

			Document doc = XMLUtility.parseXMLString(xmlReply);
			
			Node rootNode = doc.getElementsByTagName(TagIfc.TAG_CR_REPLY_MESSAGE).item(0);

			NodeList nl = rootNode.getChildNodes();

			MCRRelatedNumbersVO mcrRelatedNumbersVO = null;
			ArrayList relatedNumList = new ArrayList();
			
			for (int j = 0; j < nl.getLength(); j++) {
				if ((nl.item(j)).getNodeType() != Node.ELEMENT_NODE)
					continue;

				
				String tagName = "";
				Element l_Node = (Element) nl.item(j);
				String p_szTagName = l_Node.getTagName();
				String l_szNodeValue = null;
				if (l_Node.getFirstChild() != null)
					l_szNodeValue = l_Node.getFirstChild().getNodeValue();

				if (p_szTagName.equalsIgnoreCase(TagIfc.TAG_CR_HEADER)) {
					// Do Nothing
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_ERROR_CODE)){
					relatedNumbersReplyVO.setErrorCode(l_szNodeValue);
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_EI_ErrorMessage)){
					relatedNumbersReplyVO.setErrorMessage(l_szNodeValue);
				} else if(p_szTagName.equalsIgnoreCase(TagIfc.TAG_RESULTS)){
					NodeList paramsNodeList = l_Node.getChildNodes();
					mcrRelatedNumbersVO = new MCRRelatedNumbersVO();
					for(int i=0; paramsNodeList != null && i< paramsNodeList.getLength(); i++){
						Element s_Node = (Element) paramsNodeList.item(i);
						String s_szNodeTagName = s_Node.getTagName();
						if (s_szNodeTagName.equalsIgnoreCase(TagIfc.TAG_RESULT)) {
							NodeList resultNodeList = s_Node.getChildNodes();
							
							for (int k = 0; k < resultNodeList.getLength(); k++) {					
								if ((resultNodeList.item(k)).getNodeType() != Node.ELEMENT_NODE){
									continue;
								}
								Element c_Node = (Element) resultNodeList.item(k);
								String p_szTagName1 = c_Node.getTagName();
								String l_szNodeValue1 = null;
								if (c_Node.getFirstChild() != null){
									l_szNodeValue1 = c_Node.getFirstChild().getNodeValue();
								}
								
								if (p_szTagName1.equalsIgnoreCase(TagIfc.TAG_KEY)) {						
									tagName = l_szNodeValue1;					
								} else if (p_szTagName1.equalsIgnoreCase(TagIfc.TAG_VALUE)) {
									if(tagName.equalsIgnoreCase(ConstantIfc.SERVICE_ACCOUNT)){
										mcrRelatedNumbersVO.setServiceAccNumber(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.ACCOUNT_TYPE)){
										mcrRelatedNumbersVO.setAccountType(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.PACKAGE)){
										mcrRelatedNumbersVO.setPackageName(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.PACKAGE_DATA_FLAG)){
										mcrRelatedNumbersVO.setPackageDataFlag(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.ID_DOC_NO)){
										mcrRelatedNumbersVO.setIdDocNumber(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.BILLING_NO)){
										mcrRelatedNumbersVO.setBillingNumber(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.ID_DOC_TYPE)){
										mcrRelatedNumbersVO.setIdDocType(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.MSISDN)){
										mcrRelatedNumbersVO.setMsisdn(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.CPE_SERIAL_NO)){
										mcrRelatedNumbersVO.setCpeSerialNum(l_szNodeValue1);
									}else if(tagName.equalsIgnoreCase(ConstantIfc.PLAN_TYPE)){
										mcrRelatedNumbersVO.setPlanType(l_szNodeValue1);
									}
								} 
							}
							
						}
					}
					relatedNumList.add(mcrRelatedNumbersVO);
				} 
			}
			relatedNumbersReplyVO.setMcrRelatedNumbersVoList(relatedNumList);
		} catch (Exception e) {
			log.fatal("MCRRelatedNumberXMLHandler > parseReleatedMsisdnReply :: Exception :: "	+ e.getMessage());
			throw new SystemException(e);
		}
		
		log.debug("MCRRelatedNumberXMLHandler > parseReleatedMsisdnReply :: end");
		return relatedNumbersReplyVO;
	}

    public static void main(String[] args) {
    	
    	String xmlrequest = "<CR_REPLY_MESSAGE><CR_HEADER><FuncId>GENERAL_INQUIRY</FuncId><MsgVersion>0000</MsgVersion><RequestorChannelId>BSL</RequestorChannelId><RequestorSrId>SR1577393287</RequestorSrId><RequestorUserId>PF100458</RequestorUserId><RequestorLanguage>E</RequestorLanguage></CR_HEADER><ErrorCode>0</ErrorCode><ErrorMessage>SUCCESS</ErrorMessage><Results><Result><Key>SERVICE_ACCOUNT</Key><Value>992008020600000132</Value></Result><Result><Key>PACKAGE</Key><Value>mobily 500</Value></Result><Result><Key>PACKAGE_DATA_FLAG</Key><Value>N</Value></Result><Result><Key>ID_DOC_NO</Key><Value>2022573121</Value></Result><Result><Key>BILLING_NO</Key><Value>991008020600000132</Value></Result><Result><Key>ID_DOC_TYPE</Key><Value>IQAMA</Value></Result><Result><Key>MSISDN</Key><Value>966500073950</Value></Result><Result><Key>CPE_SERIAL_NO</Key><Value>10.6.37.59</Value></Result><Result><Key>PLAN_TYPE</Key><Value>Postpaid Plan</Value></Result><Result><Key>ACCOUNT_TYPE</Key><Value>GSM</Value></Result></Results><Results><Result><Key>SERVICE_ACCOUNT</Key><Value>992008020600000130</Value></Result><Result><Key>PACKAGE</Key><Value>mobily 1000</Value></Result><Result><Key>PACKAGE_DATA_FLAG</Key><Value>N</Value></Result><Result><Key>ID_DOC_NO</Key><Value>2022573121</Value></Result><Result><Key>BILLING_NO</Key><Value>991008020600000130</Value></Result><Result><Key>ID_DOC_TYPE</Key><Value>IQAMA</Value></Result><Result><Key>MSISDN</Key><Value>966500100050</Value></Result><Result><Key>CPE_SERIAL_NO</Key><Value></Value></Result><Result><Key>PLAN_TYPE</Key><Value>Postpaid Plan</Value></Result><Result><Key>ACCOUNT_TYPE</Key><Value>GSM</Value></Result></Results></CR_REPLY_MESSAGE>";
    	
    	MCRRelatedNumbersReplyVO relatedNumbersReplyVO = MCRRelatedNumberXMLHandler.parseReleatedMsisdnReply(xmlrequest);
    	MCRRelatedNumbersVO relatedNumbersVO = null; 
//    	System.out.println(relatedNumbersReplyVO.getErrorCode());
//    	System.out.println(relatedNumbersReplyVO.getErrorMessage());
    	for(int i = 0; i < relatedNumbersReplyVO.getMcrRelatedNumbersVoList().size(); i++){
    		relatedNumbersVO = (MCRRelatedNumbersVO)relatedNumbersReplyVO.getMcrRelatedNumbersVoList().get(i);
    		
//    		System.out.println(relatedNumbersVO.getBillingNumber());
//    		System.out.println(relatedNumbersVO.getMsisdn());
//    		System.out.println(relatedNumbersVO.getPackageName());
//    		System.out.println(relatedNumbersVO.getPackageDataFlag());
//    		System.out.println("------------------------");
    		
    	}
    }
}