package sa.com.mobily.eportal.common.util.payment;

import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonServiceslLoggerConstantIfcs;
import sa.com.mobily.eportal.common.constants.PaymentConstants;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;

public class PaymentUtils
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonServiceslLoggerConstantIfcs.COMMON_SERVICES_LOGGER_NAME);
	
	private static final String className = PaymentUtils.class.getName();
	
	public static String getErrorCodeForMsg(String errorMsg){
		
		String methodName="getErrorCodeForMsg";
		
		String errorCode=null;
		executionContext.audit(Level.INFO,"Payment3DUtil> "+methodName+" > errorMessage -> "+errorMsg,methodName,className);
		
		if(errorMsg.contains(PaymentConstants.BANK_ERROR_DECLINED)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_404;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_INVALID_CARD_NUMBER)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_405;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_INVALID_CARD_CVV)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_406;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_INVALID_CARD_EXPIRY_DATE)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_407;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_UNSPECIFIED_FAILURE) 
				|| errorMsg.contains(PaymentConstants.BANK_ERROR_TRANSACTION_BLOCKED)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_408;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_INSUFFICIENT_FUNDS_1) 
				|| errorMsg.contains(PaymentConstants.BANK_ERROR_INSUFFICIENT_FUNDS_2)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_409;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_TIMED_OUT) ){
			errorCode = PaymentConstants.ERROR_CODE_MBE_410;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_RELATION_NOT_FOUND_1) 
					|| errorMsg.contains(PaymentConstants.BANK_ERROR_CARD_PAYMENT_NOT_DETERMINE_1)
						|| errorMsg.contains(PaymentConstants.BANK_ERROR_CARD_PAYMENT_NOT_DETERMINE_2)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_411;
		}
		else if (errorMsg.contains(PaymentConstants.BANK_ERROR_DO_NOT_PROCEED) 
				|| errorMsg.contains(PaymentConstants.BANK_ERROR_RELATION_NOT_FOUND_2)
					|| errorMsg.contains(PaymentConstants.BANK_ERROR_RELATION_NOT_FOUND_3)){
			errorCode = PaymentConstants.ERROR_CODE_MBE_412;
		}
		
		executionContext.audit(Level.INFO,"Payment3DUtil> "+methodName+" > errorCode -> "+errorCode,methodName,className);
		return errorCode;
	}

}
