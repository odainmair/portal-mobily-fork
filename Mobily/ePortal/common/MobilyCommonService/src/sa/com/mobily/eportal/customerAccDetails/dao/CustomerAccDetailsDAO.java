package sa.com.mobily.eportal.customerAccDetails.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.collections.CollectionUtils;

import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.customerAccDetails.util.CustomerAccountDetailsUtil;
import sa.com.mobily.eportal.customerAccDetails.vo.CustomerDetailsVO;

public class CustomerAccDetailsDAO extends AbstractDBDAO<CustomerDetailsVO>
{
	public static final String CUSTOMER_ACC_DETAILS_LOGGER_NAME ="MobilyCommonService/customerAccDetails";
	private static final String  GET_CUSTOMER_DETAILS = "SELECT ID_TYPE, ID_NUMBER, ACCOUNT_NUMBER, CREATED_DATE, UPDATED_DATE, CUSTOMER_DETAILS FROM EEDBUSR.CUSTOMER_ACC_DETAILS_TBL WHERE ID_TYPE =? AND ID_NUMBER =?";
	private static final String  SAVE_CUSTOMER_DETAILS = "INSERT INTO EEDBUSR.CUSTOMER_ACC_DETAILS_TBL (ID_TYPE, ID_NUMBER, ACCOUNT_NUMBER,CREATED_DATE,UPDATED_DATE, CUSTOMER_DETAILS )VALUES (?, ?, ?, CURRENT_TIMESTAMP ,CURRENT_TIMESTAMP , ?)";
	private static final String UPDATE_CUSTOMER_DETAILS = "UPDATE EEDBUSR.CUSTOMER_ACC_DETAILS_TBL SET CUSTOMER_DETAILS =?, UPDATED_DATE=CURRENT_TIMESTAMP WHERE ID_TYPE =? AND ID_NUMBER =?";
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CUSTOMER_ACC_DETAILS_LOGGER_NAME);
	private static String className = CustomerAccountDetailsUtil.class.getName();
	
	private CustomerAccDetailsDAO() {
		super(DataSources.DEFAULT);
	}
	
	private static class CustomerAccDetailsDAOHolder {
		private static final CustomerAccDetailsDAO customerAccDetailsDAOInstance = new CustomerAccDetailsDAO();
	}
	
	public static CustomerAccDetailsDAO getInstance() {
		return CustomerAccDetailsDAOHolder.customerAccDetailsDAOInstance;
	}
	
	public CustomerDetailsVO getCustomerDetails(String idType,String idNumber)
	{
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > getCustomerDetails > id:: " + idNumber, className, "getCustomerDetails");
		CustomerDetailsVO customerDetailsVO = null;
		List<CustomerDetailsVO> customerDetailsVOs =  query(GET_CUSTOMER_DETAILS,idType, idNumber);
		//executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > getCustomerDetails > userProfileVOList:: " + userProfileVOList, className, "getCustomerDetails");
		if(CollectionUtils.isNotEmpty(customerDetailsVOs)){
			executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > getCustomerDetails > userProfileVOList not empty:: ", className, "getCustomerDetails");
			customerDetailsVO =  customerDetailsVOs.get(0);
		}
		return customerDetailsVO;
	}
	public boolean saveCustomerDetailsData(CustomerDetailsVO customerDetails)
	{
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > saveCustomerDetailsData > ID Type:: " + customerDetails.getIdType(), className, "saveCustomerDetailsData");
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > saveCustomerDetailsData >id Number:: " + customerDetails.getIdNumber(), className, "saveCustomerDetailsData");
		int count = update(SAVE_CUSTOMER_DETAILS, customerDetails.getIdType(),customerDetails.getIdNumber(),customerDetails.getAccountNumber(),customerDetails.getCustomerDetailsData());
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > saveCustomerDetailsData > row inserted:: " + count, className, "saveCustomerDetailsData");
		return count > 0 ? true : false;
	}

	public boolean updateCustomerDetails(CustomerDetailsVO customerDetails)
	{
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > updateCustomerDetails > ID Type:: " + customerDetails.getIdType(), className, "updateCustomerDetails");
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > updateCustomerDetails >id Number:: " + customerDetails.getIdNumber(), className, "updateCustomerDetails");
		int count = update(UPDATE_CUSTOMER_DETAILS, customerDetails.getCustomerDetailsData(),customerDetails.getIdType(),customerDetails.getIdNumber());
		executionContext.audit(Level.INFO, "CustomerAccDetailsDAO > updateCustomerDetails > row inserted:: " + count, className, "updateCustomerDetails");
		return count > 0 ? true : false;
	}
	
	@Override
	protected CustomerDetailsVO mapDTO(ResultSet rs) throws SQLException
	{
		CustomerDetailsVO customerDetailsVO  = new CustomerDetailsVO();
		customerDetailsVO.setIdType(rs.getString("ID_TYPE"));
		customerDetailsVO.setIdNumber(rs.getString("ID_NUMBER"));
		customerDetailsVO.setAccountNumber(rs.getString("ACCOUNT_NUMBER"));
		customerDetailsVO.setCreatedDate(rs.getString("CREATED_DATE"));
		customerDetailsVO.setCreatedTimestamp(rs.getTimestamp("UPDATED_DATE"));
		customerDetailsVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
		customerDetailsVO.setCustomerDetailsData(rs.getBytes("CUSTOMER_DETAILS"));
		
		return customerDetailsVO;
	}

	

	

	
	
}
