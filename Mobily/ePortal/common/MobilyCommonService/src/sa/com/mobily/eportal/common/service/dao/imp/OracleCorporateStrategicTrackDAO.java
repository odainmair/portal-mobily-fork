/**
 * 
 */
package sa.com.mobily.eportal.common.service.dao.imp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

import sa.com.mobily.eportal.common.service.constant.ConstantIfc;
import sa.com.mobily.eportal.common.service.dao.ifc.CorporateStrategicTrackDAO;
import sa.com.mobily.eportal.common.service.db.DataBaseConnectionHandler;
import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.logger.LoggerInterface;
import sa.com.mobily.eportal.common.service.util.JDBCUtility;
import sa.com.mobily.eportal.common.service.util.MobilyUtility;
import sa.com.mobily.eportal.common.service.vo.StrategicTrackVO;


/**
 * @author s.vathsavai.mit
 *
 */
public class OracleCorporateStrategicTrackDAO implements CorporateStrategicTrackDAO {

	private static final Logger log = LoggerInterface.log;
	
	public StrategicTrackVO doInitialInquiryFromDB(String billingAccountNumber) {
	    log.info("OracleCorporateStrategicTrackDAO > doInitialInquiryFromDB > Callled.........");

	    Connection  connection 				= null;
		PreparedStatement preparedStatement = null;
		ResultSet  resultSet 				= null;

		StrategicTrackVO strategicTrackVO = null;
		
		try {
			
		    StringBuffer strSQL = new StringBuffer();
		    strSQL.append("SELECT STATUS,EXPIRED,TO_CHAR(REQUEST_TIME,'DD:MM:YYYY; HH24:MI; AM') requestedTime,");
		    strSQL.append(" TO_CHAR(RECEIVING_TIME,'DD:MM:YYYY; HH24:MI; AM') receivedTime FROM SR_STRATEGIC_TRACK_TBL WHERE");
		    strSQL.append(" BILLING_ACCOUNT_NO = ? ");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		    //connection = getLocalEEDBConnection();
						
			preparedStatement = connection.prepareStatement(strSQL.toString());
			preparedStatement.setString(1, billingAccountNumber);
			
			resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				strategicTrackVO = new StrategicTrackVO();
				strategicTrackVO.setStatus(resultSet.getString("STATUS"));
				strategicTrackVO.setExpired(resultSet.getString("EXPIRED"));
				strategicTrackVO.setRequestedTime(resultSet.getString("requestedTime"));
				strategicTrackVO.setReceivedTime(resultSet.getString("receivedTime"));
			}
			
		} catch (SQLException e) {
			log.fatal("OracleCorporateStrategicTrackDAO > doInitialInquiryFromDB > SQLException >"+e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("OracleCorporateStrategicTrackDAO > doInitialInquiryFromDB > Exception >"+e.getMessage());
			throw new SystemException(e);
		} finally {
		    JDBCUtility.closeJDBCResoucrs(connection,preparedStatement,resultSet);
		}
		
		log.info("OracleCorporateStrategicTrackDAO > doInitialInquiryFromDB > End.....");
		
		return strategicTrackVO;
	}
	
	public String getMessageFromDB(String billingAccountNumber) {
	    log.info("OracleCorporateStrategicTrackDAO > getMessageFromDB > Callled.........");

	    Connection  connection 				= null;
		PreparedStatement preparedStatement = null;
		ResultSet  resultSet 				= null;
		StringBuffer xmlMessage = new StringBuffer();
		String aux;
		
		try {
			
		    StringBuffer strSQL = new StringBuffer();
		    strSQL.append("SELECT MESSAGE FROM SR_STRATEGIC_TRACK_TBL WHERE");
		    strSQL.append(" BILLING_ACCOUNT_NO = ? ");
			
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
		    //connection = getLocalEEDBConnection();
						
			preparedStatement = connection.prepareStatement(strSQL.toString());
			preparedStatement.setString(1, billingAccountNumber);
			
			resultSet = preparedStatement.executeQuery();
			
			if(resultSet.next()) {
				BufferedReader br = new BufferedReader(resultSet.getCharacterStream("MESSAGE"));
				while ((aux=br.readLine()) != null)
					xmlMessage.append(aux);
				
				br.close();
			}
			
		} catch (SQLException e) {
			log.fatal("OracleCorporateStrategicTrackDAO > getMessageFromDB > SQLException >"+e.getMessage());
			throw new SystemException(e);
		} catch (Exception e) {
			log.fatal("OracleCorporateStrategicTrackDAO > getMessageFromDB > Exception >"+e.getMessage());
			throw new SystemException(e);
		} finally {
		    JDBCUtility.closeJDBCResoucrs(connection,preparedStatement,resultSet);
		}
		
		log.info("OracleCorporateStrategicTrackDAO > getMessageFromDB > xmlMessage.....>"+xmlMessage);
		log.info("OracleCorporateStrategicTrackDAO > getMessageFromDB > End.....");
		
		return xmlMessage.toString();
	}
	
	public boolean insertCorpRecordInDB(String billingAccountNumber) {
		 
		log.info("OracleCorporateStrategicTrackDAO > insertCorpRecordInDB > start");
		Connection connection = null;
		PreparedStatement pstmt = null;
		boolean statusFlag = false;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			//connection = getLocalEEDBConnection();
			java.util.Date today = new java.util.Date();
			java.sql.Timestamp currentTimeStampDate = new Timestamp(today.getTime());
			log.info("OracleCorporateStrategicTrackDAO > insertCorpRecordInDB > currentTimeStampDate > "+currentTimeStampDate);
			
		    StringBuffer sqlQuery = new StringBuffer();
		    
			sqlQuery.append("INSERT INTO SR_STRATEGIC_TRACK_TBL(BILLING_ACCOUNT_NO, STATUS,REQUEST_TIME,EXPIRED)");
			sqlQuery.append(" VALUES(?,?,?,?)");
			//sqlQuery.append(" VALUES(?,?,CURRENT_TIMESTAMP,?)");
			
			pstmt = connection.prepareStatement(sqlQuery.toString());
					
			pstmt.setString(1,billingAccountNumber);
			pstmt.setString(2,ConstantIfc.STATUS_PENDING);
			pstmt.setTimestamp(3, currentTimeStampDate);
			pstmt.setString(4,ConstantIfc.EXPIRED_STATUS_TRUE);
			
			int status = pstmt.executeUpdate();
			if(status > 0)
				statusFlag = true;
				
					
		} catch (SQLException e) {
			log.fatal("OracleCorporateStrategicTrackDAO > insertCorpRecordInDB > SQLException >"+e.getMessage());
			throw new SystemException(e);
		} catch(Exception e) {
			log.fatal("OracleCorporateStrategicTrackDAO > insertCorpRecordInDB > Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pstmt, null);
		}
		
		return statusFlag;
	}
	
	public boolean updateExpiredStatusInDB(String billingAccountNumber,String expiredStatus) {
		 
		log.info("OracleCorporateStrategicTrackDAO > updateExpiredStatusInDB > start for expired Status >"+expiredStatus);
		Connection connection = null;
		PreparedStatement pstmt = null;
		boolean statusFlag = false;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			
		    StringBuffer sqlQuery = new StringBuffer();
		    
		    sqlQuery.append("UPDATE SR_STRATEGIC_TRACK_TBL SET EXPIRED = ?");
			sqlQuery.append(" WHERE BILLING_ACCOUNT_NO = ? ");
			
			pstmt = connection.prepareStatement(sqlQuery.toString());
				
			pstmt.setString(1,expiredStatus);
			pstmt.setString(2,billingAccountNumber);
			
			int status = pstmt.executeUpdate();
			if(status > 0)
				statusFlag = true;
				
					
		} catch (SQLException e) {
			log.fatal("OracleCorporateStrategicTrackDAO > updateExpiredStatusInDB > SQLException >"+e.getMessage());
			throw new SystemException(e);
		} catch(Exception e) {
			log.fatal("OracleCorporateStrategicTrackDAO > updateExpiredStatusInDB > Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pstmt, null);
		}
		
		return statusFlag;
	}
	
	public boolean updateCorpRecordInDB(String billingAccountNumber) {
		 
		log.info("OracleCorporateStrategicTrackDAO > updateCorpRecordInDB > start");
		Connection connection = null;
		PreparedStatement pstmt = null;
		boolean statusFlag = false;
		try {
			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			//connection = getLocalEEDBConnection();
			java.util.Date today = new java.util.Date();
			java.sql.Timestamp currentTimeStampDate = new Timestamp(today.getTime());
			log.info("OracleCorporateStrategicTrackDAO > updateCorpRecordInDB > currentTimeStampDate :: new Requested Time set in DB > "+currentTimeStampDate);
			
		    StringBuffer sqlQuery = new StringBuffer();
		    
			sqlQuery.append("UPDATE SR_STRATEGIC_TRACK_TBL SET STATUS = ?, REQUEST_TIME = ?");
			sqlQuery.append(" WHERE BILLING_ACCOUNT_NO = ? ");
					
			pstmt = connection.prepareStatement(sqlQuery.toString());
			
			pstmt.setString(1,ConstantIfc.STATUS_PENDING);
			pstmt.setTimestamp(2, currentTimeStampDate);
			pstmt.setString(3, billingAccountNumber);
			int status = pstmt.executeUpdate();
			if(status > 0)
				statusFlag = true;
				
					
		} catch (SQLException e) {
			log.fatal("OracleCorporateStrategicTrackDAO > updateCorpRecordInDB > SQLException >"+e.getMessage());
			throw new SystemException(e);
		} catch(Exception e) {
			log.fatal("OracleCorporateStrategicTrackDAO > updateCorpRecordInDB > Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
		} finally {
			JDBCUtility.closeJDBCResoucrs(connection, pstmt, null);
		}
		
		return statusFlag;
	}
	
	public void updateStrategicTrackRecordInDB(String billingAccountNumber,String xmlMessage) {
		log.info("OracleCorporateStrategicTrackDAO > updateStrategicTrackRecordInDB > Start");
	    
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			
		    StringBuffer sqlQuery = new StringBuffer();
		    
			sqlQuery.append(" UPDATE SR_STRATEGIC_TRACK_TBL " );
			sqlQuery.append(" SET MESSAGE = EMPTY_CLOB(),RECEIVING_TIME = ?,STATUS = ? WHERE BILLING_ACCOUNT_NO = ?"); 

			connection = DataBaseConnectionHandler.getDBConnection(MobilyUtility.getPropertyValue(ConstantIfc.DATA_SOURCE_EEDB));
			//connection = getLocalEEDBConnection();
			
			java.util.Date today = new java.util.Date();
			java.sql.Timestamp currentTimeStampDate = new Timestamp(today.getTime());
			log.info("OracleCorporateStrategicTrackDAO > updateStrategicTrackRecordInDB > currentTimeStampDate :: new Received Time set in DB > "+currentTimeStampDate);

			
			pstmt  = connection.prepareStatement(sqlQuery.toString());
			pstmt.setTimestamp(1, currentTimeStampDate);
			pstmt.setString(2,ConstantIfc.STATUS_COMPLETED);		
			pstmt.setString(3,billingAccountNumber);			
			
			int sqlStatus = pstmt.executeUpdate();
			log.info("OracleCorporateStrategicTrackDAO > updateStrategicTrackRecordInDB > sqlStatus="+sqlStatus);
			
			if(sqlStatus > 0) {
				connection.setAutoCommit(false);
		    	Statement st = connection.createStatement();
		    	rs = st.executeQuery("SELECT MESSAGE FROM SR_STRATEGIC_TRACK_TBL WHERE BILLING_ACCOUNT_NO = "+billingAccountNumber+" FOR UPDATE");
    	        if(rs != null) {
	    	        if (rs.next()) {
		    	        oracle.sql.CLOB clob = (oracle.sql.CLOB)rs.getClob("MESSAGE");
		    	        BufferedWriter out = new BufferedWriter (clob.getCharacterOutputStream ()); 
		    	        StringReader sr = new StringReader (xmlMessage); 
		    	        BufferedReader in = new BufferedReader (sr); 
		    	        int c; 
		    	        while((c = in.read ())!=- 1) { 
		    	        	out.write (c); 
		    	        } 
		    	        in.close (); 
		    	        out.close ();  
	    	        }
    	        }
    	        connection.commit();       
			}
		} catch (SQLException e) {
			log.fatal("OracleCorporateStrategicTrackDAO > updateStrategicTrackRecordInDB > SQLException > "+e.getMessage());
		   	 throw new SystemException(e.getMessage(), e);	
		} catch(Exception e) {
			log.fatal("OracleCorporateStrategicTrackDAO > updateStrategicTrackRecordInDB >  Exception > "+e.getMessage());
	   	  	throw new SystemException(e.getMessage(), e);  
	    } finally {
	        JDBCUtility.closeJDBCResoucrs(connection, pstmt, rs);
		}
	    log.info("OracleStrategicTrackDAO > updateStrategicTrackRecordInDB > End");
	}	
	
	public String doCorporateInquiryFromDB(String billingAccountNumber) {
		// TODO Auto-generated method stub
		return null;
	}


	public String doCorporateInquiry(String xmlRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	public String updateCircuitAliasName(String xmlRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	public void doCorporateInquiryAsync(String xmlRequest) {
	}
	
	public static Connection getLocalEEDBConnection() {

		try {
				//System.out.println("step 1");
				String driverClass = "oracle.jdbc.driver.OracleDriver";
	
			   // Load the JDBC driver
			   Class.forName(driverClass);
			   //System.out.println("step 2");
			   // Create a connection to the database
			   String url = "jdbc:oracle:thin:@10.14.11.203:1521:EPDEV";
			   Connection returnConn = DriverManager.getConnection(url, "eedbusr", "eedbusr");
			   //System.out.println("after connection");
			   return returnConn;
		}catch (SQLException e) {
	        // TODO Auto-generated catch block
			System.out.println("ResourceUtility > getEEDBConnection > SQLException >" +e.getMessage());
	    } catch (ClassNotFoundException e) {
	    	System.out.println("ResourceUtility > getEEDBConnection > ClassNotFoundException >" +e.getMessage());
		} catch(Exception e){
			System.out.println("ResourceUtility > getEEDBConnection > Exception >" +e.getMessage());
		}
		return null;
	}

	public static void main(String args[]){
		
		//String xml = new OracleCorporateStrategicTrackDAO().getMessageFromDB("123456789");
		/*StrategicTrackVO strategicTrackVO = new StrategicTrackVO();
		String billingAccountNumber = "123456789";
		String xmlMessage = "<MOBILY_BSL_SR_REPLY><SR_BK_HEADER><FuncId>CORP_INQ</FuncId><SecurityKey/><ServiceRequestId>TEST_SR_20100313100942</ServiceRequestId><MsgVersion>0000</MsgVersion><RequestorChannelId>EPORTAL</RequestorChannelId><SrDate>20100313070850</SrDate><BackendChannel>NETWORK</BackendChannel><SrRcvDate>20100313070842</SrRcvDate><SrStatus>6</SrStatus></SR_BK_HEADER><ErrorCode>0</ErrorCode><ErrorMsg/><ListOfBillingAccount><BillingAccount><AccountNumber>123456789</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238351</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013828988647</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType/><ContractPeriod/><NetworkConfiguration/><ParentAccountNumber>100013827238368</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts/><BillingAccount><AccountNumber>100013829159589</AccountNumber><AccountTypeCode>Billing</AccountTypeCode><ProductType>DIA</ProductType><ContractPeriod>1 Year</ContractPeriod><NetworkConfiguration/><ParentAccountNumber>100013828988647</ParentAccountNumber><MasterAccountNumber>100013827238351</MasterAccountNumber><KAM>h.alotaibi@mobily.com.sa</KAM><Name>QATEST EPORTAL DIA TEST</Name><ListOfServiceSubAccounts><CutServiceSubAccounts><AccountNumber>100013829180705</AccountNumber><AccountTypeCode>Service</AccountTypeCode><VPNID/><PackageName>DIA Backup</PackageName><Name>QATEST EPORTAL DIA TEST [100013829180705]</Name><Alias/><Items><Item><ItemName>DIA Backup</ItemName><Status>Active</Status><Attributes><Attribute><Name>Bandwidth</Name><TextValue>400 MB</TextValue></Attribute><Attribute><Name>Connection Mode</Name><TextValue>Silver</TextValue></Attribute><Attribute><Name>Access Network</Name><TextValue>GPON</TextValue></Attribute><Attribute><Name>NTU</Name><TextValue>ONT</TextValue></Attribute><Attribute><Name>Default CIDR Profile</Name><TextValue>/29</TextValue></Attribute></Attributes></Item><Item><ItemName>DIA Installation New</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA - Managed Connectivity</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>Dual Ethernet Security Router with V.92 Modem Backup</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>2821 Bundle w/AIM-VPN/SSL-2,Adv. IP Serv,10 SSL lic,64F/256D</ItemName><Status>Active</Status><Attributes/></Item><Item><ItemName>DIA SLA</ItemName><Status>Active</Status><Attributes><Attribute><Name>SLA</Name><TextValue>Silver</TextValue></Attribute></Attributes></Item></Items></CutServiceSubAccounts></ListOfServiceSubAccounts></BillingAccount></BillingAccount></BillingAccount></ListOfBillingAccount></MOBILY_BSL_SR_REPLY>";
		
		new OracleCorporateStrategicTrackDAO().updateStrategicTrackRecordInDB(billingAccountNumber,xmlMessage);*/
		
		//new OracleCorporateStrategicTrackDAO().getMessageFromDB("100013835312648");
		
		boolean status = new OracleCorporateStrategicTrackDAO().insertCorpRecordInDB("1234567890");
		StrategicTrackVO strategicTrackVO = new OracleCorporateStrategicTrackDAO().doInitialInquiryFromDB("1234567890");
		//Timestamp receivedDate = MobilyUtility.getTimestampDate(strategicTrackVO.getReceivedTime());
		log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: getRequestedTime = "+strategicTrackVO.getRequestedTime());
		//log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: receivedDate2 = "+receivedDate);
		Timestamp requestDate = MobilyUtility.getTimestampDate(strategicTrackVO.getRequestedTime());
		log.info("CorporateStrategicTrackBO > handleDIARequestAsync :: requestDate = "+requestDate);
		
		
		
		/*Connection connection = null;
		
		try {
			connection = getLocalEEDBConnection();
			String sql = "SELECT BILLING_ACCOUNT_NO,STATUS,TO_CHAR(REQUEST_TIME,'DD:MM:YYYY; HH:MI; AM') requestedTime,TO_CHAR(RECEIVING_TIME,'DD:MM:YYYY; HH:MI; AM') receivedTime FROM SR_STRATEGIC_TRACK_TBL where BILLING_ACCOUNT_NO = ?";
			PreparedStatement stmt = connection.prepareStatement(sql);
			stmt.setString(1,"100013835312648");	
			ResultSet resultSet = stmt.executeQuery();
			while (resultSet.next()) {
				/*File data = new File("G:\\Rupesh\\test.txt");
				
				Reader reader = resultSet.getCharacterStream(1);
				FileWriter writer = new FileWriter(data);
				char[] buffer = new char[1];
				while (reader.read(buffer) > 0) {
					writer.write(buffer);
				}
				writer.close();*/
				
	/*			System.out.println("billingAccountNumber="+resultSet.getString(1));
				System.out.println("status="+resultSet.getString(2));
				System.out.println("requested time="+resultSet.getString(3));
				System.out.println("receiving time="+resultSet.getString(4));
			}
			
			System.out.println("done");
			
		}catch (SQLException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if (connection != null && !connection.isClosed()) {
					connection.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} */
	}

}
