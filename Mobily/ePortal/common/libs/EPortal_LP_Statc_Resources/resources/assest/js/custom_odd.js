function retrieveActivePaymentMethod(parent_component,selector){return document.getElementById(parent_component).querySelectorAll(selector)[0].id;}
function togglePaymentMethod(element){$(element).parent().parent().closest('ul').find('.active').removeClass('active');$(element).addClass('active');return false;}
function useCreditCheckboxChange(parent_component, element) {
	var creditAmountPayment = $("#" + parent_component).find('.credit-amount');
	var creditPayment = $("#" + parent_component).find('.credit-payment');
	var totalPayment = $("#" + parent_component).find('.total-payment');
	if($(element).is(":checked")) {
		if((creditAmountPayment.text().trim() - creditPayment.text().trim().replace(",", "")) < 0) {
			totalPayment.text(0);
			//hide payment options, toggle none
			$("#" + parent_component).find(".payment-method").hide();
			$("#" + parent_component).find("#debitCard").removeClass("active");
			$("#" + parent_component).find("#sadad").removeClass("active");
		} else {
			totalPayment.text(creditAmountPayment.text().trim() - creditPayment.text().trim().replace(",", ""));
		}
	} else {
		totalPayment.text(creditAmountPayment.text().trim());
		//show payment options, toggle cc
		$("#" + parent_component).find(".payment-method").show();
		$("#" + parent_component).find("#debitCard").addClass("active");
		$("#" + parent_component).find("#sadad").removeClass("active");
	}
}