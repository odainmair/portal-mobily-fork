function sendExpandEvent(element, creditQuantity) {
	if(!$(element).hasClass("active")) {
		sendAnalyticsData('event', 'Addons', 'expand', creditQuantity);
	}
}

function sendSelectEvent(element, selectedIndex, creditQuantity) {
	var selectedMethod = document.getElementById('payment_div_' + selectedIndex).querySelectorAll('a.active');

	if(selectedMethod.length > 0) {
		selectedMethod = selectedMethod[0].id;
	} else {
		selectedMethod = "MA";
	}
	
	var paymentMethod = "";
	
	switch(selectedMethod) {
		case "debitCard":
			paymentMethod = "CREDIT_CARD";
			break;
			
		case "sadad":
			paymentMethod = "SADAD";
			break;
			
		default:
			paymentMethod = "MA";
			break;
	}

	sendAnalyticsData('event', 'Addons', 'select_' + paymentMethod, creditQuantity);
}

function sendCheckoutCreditCardEvent(creditQuantity) {
	sendAnalyticsData('event', 'Addons', 'checkout_CREDIT_CARD', creditQuantity);
}

function sendAbandonCreditCardEvent(creditQuantity) {
	sendAnalyticsData('event', 'Addons', 'abandon_CREDIT_CARD', creditQuantity);
}

function sendDataVoucherRechargeEvent() {
	sendAnalyticsData('event', 'Data voucher', 'recharge', '');
}

function sendDataVoucherAbandonRechargeEvent() {
	sendAnalyticsData('event', 'Data voucher', 'abandon_recharge', '');
}

function sendMonetaryVoucherRechargeEvent() {
	sendAnalyticsData('event', 'Monetary voucher', 'recharge', '');
}

function sendMonetaryVoucherAbandonRechargeEvent() {
	sendAnalyticsData('event', 'Monetary voucher', 'abandon_recharge', '');
}

function sendPaygEvent() {
	sendAnalyticsData('event', 'PAYG', 'activate', '');
}

function sendAnalyticsData(type, category, action, label) {
	try {
		ga('send', type, category, action, label);
	}
	catch(err) {
		console.log("Could not send analytics event. Error: " + err.message);
	}
}
