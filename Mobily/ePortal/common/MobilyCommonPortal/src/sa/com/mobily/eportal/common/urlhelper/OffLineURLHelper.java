package sa.com.mobily.eportal.common.urlhelper;

/* @copyright module */
/*                                                                            */
/*  DISCLAIMER OF WARRANTIES:                                                 */
/*  -------------------------                                                 */
/*  The following [enclosed] code is sample code created by IBM Corporation.  */
/*  This sample code is provided to you solely for the purpose of assisting   */
/*  you in the development of your applications.                              */
/*  The code is provided "AS IS", without warranty of any kind. IBM shall     */
/*  not be liable for any damages arising out of your use of the sample code, */
/*  even if they have been advised of the possibility of such damages.        */
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletMode;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import com.ibm.portal.ObjectID;
import com.ibm.portal.state.Constants;
import com.ibm.portal.state.EngineURL;
import com.ibm.portal.state.StateManager;
import com.ibm.portal.state.accessors.locale.LocaleAccessorController;
import com.ibm.portal.state.accessors.locale.LocaleAccessorFactory;
import com.ibm.portal.state.accessors.portlet.PortletAccessorController;
import com.ibm.portal.state.accessors.portlet.PortletAccessorFactory;
import com.ibm.portal.state.accessors.portlet.PortletTargetAccessorController;
import com.ibm.portal.state.accessors.portlet.PortletTargetAccessorFactory;
import com.ibm.portal.state.accessors.selection.SelectionAccessorController;
import com.ibm.portal.state.accessors.selection.SelectionAccessorFactory;
import com.ibm.portal.state.accessors.url.URLAccessorFactory;
import com.ibm.portal.state.exceptions.StateException;
import com.ibm.portal.state.service.StateManagerHome;


/**
 * @author James Barnes
 * 
 *         This class is used for generating urls when you do not want to
 *         preserve the state or base them off the current request. Additionally
 *         when creating urls you cannot use it to target portlet actions for
 *         authenticated pages, only actions on portlets on unauthenticated
 *         pages.
 */

public class OffLineURLHelper {
	private static StateManagerHome stateMgrHome = null;
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);
	private static String className = OffLineURLHelper.class.getName();

	/**
	 * This will return a generic state manager for portal, it does not base its
	 * creation on the request and response
	 * 
	 * @return generic StateManager
	 * @throws StateException
	 * @throws NamingException
	 */
	@Deprecated
	private static StateManager getStateManager() throws StateException,
			NamingException {
		if (stateMgrHome == null) {
			Context ctx = new InitialContext();
			stateMgrHome = (StateManagerHome) ctx
					.lookup("portalservice/com.ibm.portal.state.StateManager");
			return stateMgrHome.getStateManager();
		} else {
			return stateMgrHome.getStateManager();
		}
	}

	/**
	 * This will generate a url to a portal page using the ServerContext.
	 * 
	 * @param pageName
	 *            uniqueName of a pagename, these are not shareable across
	 *            virtual portals
	 * @param portletName
	 *            uniqueName of the portlet on page, to set this please follow
	 *            the instructions here <a href=
	 *            "http://www-1.ibm.com/support/docview.wss?rs=688&ca=portall2&uid=swg21220164"
	 *            >Technote</a>
	 * @param params
	 *            You can pass in params the Strings must be a string array
	 * 
	 *            <pre>
	 * HashMap map = new HashMap();
	 * String[] value1 = { &quot;testthis&quot; };
	 * map.put(&quot;p2psample4_search_text&quot;, value1);
	 * </pre>
	 * @param serverContext
	 *            Use the MyServerContext class or MyVpServerContext
	 * @param nonPublicPage
	 *            this tells the generator to create a private or public page
	 * @return String of the generated url
	 * @throws StateException
	 * @throws NamingException
	 * @throws IOException
	 */
	@Deprecated
	public static String generateUrl(String pageName, String portletName,
			HashMap params, MyServerContext serverContext, boolean nonPublicPage)
			throws StateException, NamingException, IOException {

		final StateManager mgr = getStateManager();
		
		// Get the URL factory
		final URLAccessorFactory urlFactory = (URLAccessorFactory) mgr
				.getAccessorFactory(URLAccessorFactory.class);

		// Request a URL
		// The 3rd argument specifies whether the URL points to the protected
		// (authenticated) area;
		// pass in "false" if your page is accessible for unauthenticated users

		final EngineURL url = urlFactory.newURL(serverContext, false,
				nonPublicPage, mgr.newState(), Constants.EMPTY_COPY);

		// Set the page this URL should point to
		final SelectionAccessorFactory selectionFactory = (SelectionAccessorFactory) mgr
				.getAccessorFactory(SelectionAccessorFactory.class);
		// Request the selection controller to set the page; pass in the state
		// associated with the created URL
		final SelectionAccessorController selectionCtrl = selectionFactory
				.getSelectionController(url.getState());
		// Set the page; you need the unique name (String)
		selectionCtrl.setSelection(pageName);
		// Dispose the accessor (avoids memory leak)
		selectionCtrl.dispose();

		if (portletName != null) {
			// Set portlet render parameters
			final PortletAccessorFactory portletAccessorFactory = (PortletAccessorFactory) mgr
					.getAccessorFactory(PortletAccessorFactory.class);
			// Get the portlet controller to set render parameters; pass in the
			// state associated with rge created URL
			final PortletAccessorController portletCtrl = portletAccessorFactory
					.getPortletController(portletName, url.getState());
			// Get the modifiable render parameter map
			final Map parameters = portletCtrl.getParameters();
			// Set the render parameter
			if (params != null) {
				String key = null;
				Iterator paramsIterator = params.keySet().iterator();
				while (paramsIterator.hasNext()) {
					key = (String) paramsIterator.next();
					parameters.put(key, params.get(key));
				}
			}
			// Dispose the accessor (avoids memory leak)
			portletCtrl.dispose();
		}
		return url.writeDispose(new StringWriter()).toString();
	}


	/**
	 * This will generate a url to a portal page using the ServerContext.
	 * 
	 * @param pageName
	 *            uniqueName of a pagename, these are not shareable across
	 *            virtual portals
	 * @param portletName
	 *            uniqueName of the portlet on page, to set this please follow
	 *            the instructions here <a href=
	 *            "http://www-1.ibm.com/support/docview.wss?rs=688&ca=portall2&uid=swg21220164"
	 *            >Technote</a>
	 * @param params
	 *            You can pass in params the Strings must be a string array
	 * 
	 *            <pre>
	 * HashMap map = new HashMap();
	 * String[] value1 = { &quot;testthis&quot; };
	 * map.put(&quot;p2psample4_search_text&quot;, value1);
	 * </pre>
	 * @param serverContext
	 *            Use the MyServerContext class or MyVpServerContext
	 * @param nonPublicPage
	 *            this tells the generator to create a private or public page
	 * @return String of the generated url
	 * @throws StateException
	 * @throws NamingException
	 * @throws IOException
	 */
	@Deprecated
	public static String generateUrl(String pageName, String portletName,
			HashMap params, MyServerContext serverContext, boolean nonPublicPage,Locale locale)
			throws StateException, NamingException, IOException {

		final StateManager mgr = getStateManager();
		
		// Get the URL factory
		final URLAccessorFactory urlFactory = (URLAccessorFactory) mgr
				.getAccessorFactory(URLAccessorFactory.class);

		// Request a URL
		// The 3rd argument specifies whether the URL points to the protected
		// (authenticated) area;
		// pass in "false" if your page is accessible for unauthenticated users

		final EngineURL url = urlFactory.newURL(serverContext, false,
				nonPublicPage, mgr.newState(), Constants.EMPTY_COPY);

		// Set the Language this URL should point to
	    final LocaleAccessorFactory localeFactory = (LocaleAccessorFactory) mgr.getAccessorFactory(LocaleAccessorFactory.class);
	    // Request the selection controller to set the page; pass in the state associated with the created URL
	    final LocaleAccessorController localeCtrl = localeFactory.getLocaleAccessorController(url.getState());
	    // Set the locale...
	    localeCtrl.setLocale(locale);
	    // Dispose (avoids memory leaks)
	    localeCtrl.dispose();
	
		
		
		// Set the page this URL should point to
		final SelectionAccessorFactory selectionFactory = (SelectionAccessorFactory) mgr
				.getAccessorFactory(SelectionAccessorFactory.class);
		// Request the selection controller to set the page; pass in the state
		// associated with the created URL
		final SelectionAccessorController selectionCtrl = selectionFactory
				.getSelectionController(url.getState());
		// Set the page; you need the unique name (String)
		selectionCtrl.setSelection(pageName);
		// Dispose the accessor (avoids memory leak)
		selectionCtrl.dispose();

		if (portletName != null) {
			// Set portlet render parameters
			final PortletAccessorFactory portletAccessorFactory = (PortletAccessorFactory) mgr
					.getAccessorFactory(PortletAccessorFactory.class);
			// Get the portlet controller to set render parameters; pass in the
			// state associated with rge created URL
			final PortletAccessorController portletCtrl = portletAccessorFactory
					.getPortletController(portletName, url.getState());
			// Get the modifiable render parameter map
			final Map parameters = portletCtrl.getParameters();
			// Set the render parameter
			if (params != null) {
				String key = null;
				Iterator paramsIterator = params.keySet().iterator();
				while (paramsIterator.hasNext()) {
					key = (String) paramsIterator.next();
					parameters.put(key, params.get(key));
				}
			}
			// Dispose the accessor (avoids memory leak)
			portletCtrl.dispose();
		}
		return url.writeDispose(new StringWriter()).toString();
	}

	
	
	
	
	
	
	/**
	 * This works the same as above, but takes objectids instead of the
	 * uniquename
	 * 
	 * @param pageName
	 * @param portletName
	 * @param params
	 * @param serverContext
	 * @param nonPublicPage
	 * @return
	 * @throws StateException
	 * @throws NamingException
	 * @throws IOException
	 */
	@Deprecated
	public static String generateUrl(ObjectID pageName, ObjectID portletName,
			HashMap params, MyServerContext serverContext, boolean nonPublicPage)
			throws StateException, NamingException, IOException {

		final StateManager mgr = getStateManager();

		// Get the URL factory
		final URLAccessorFactory urlFactory = (URLAccessorFactory) mgr
				.getAccessorFactory(URLAccessorFactory.class);

		// Request a URL
		// The 3rd argument specifies whether the URL points to the protected
		// (authenticated) area;
		// pass in "false" if your page is accessible for unauthenticated users

		final EngineURL url = urlFactory.newURL(serverContext, false,
				nonPublicPage, mgr.newState(), Constants.EMPTY_COPY);

		// Set the page this URL should point to
		final SelectionAccessorFactory selectionFactory = (SelectionAccessorFactory) mgr
				.getAccessorFactory(SelectionAccessorFactory.class);
		// Request the selection controller to set the page; pass in the state
		// associated with the created URL
		final SelectionAccessorController selectionCtrl = selectionFactory
				.getSelectionController(url.getState());
		// Set the page; you need the unique name (String)
		selectionCtrl.setSelection(pageName);
		// Dispose the accessor (avoids memory leak)
		selectionCtrl.dispose();

		if (portletName != null) {
			// Set portlet render parameters
			final PortletAccessorFactory portletAccessorFactory = (PortletAccessorFactory) mgr
					.getAccessorFactory(PortletAccessorFactory.class);
			// Get the portlet controller to set render parameters; pass in the
			// state associated with rge created URL
			final PortletAccessorController portletCtrl = portletAccessorFactory
					.getPortletController(portletName, url.getState());
			// Get the modifiable render parameter map
			final Map parameters = portletCtrl.getParameters();
			// Set the render parameter
			if (params != null) {
				String key = null;
				Iterator paramsIterator = params.keySet().iterator();
				while (paramsIterator.hasNext()) {
					key = (String) paramsIterator.next();
					parameters.put(key, params.get(key));
				}
			}
			// Dispose the accessor (avoids memory leak)
			portletCtrl.dispose();
		}
		return url.writeDispose(new StringWriter()).toString();
	}
	
	/**
	 * This will target an action and change the portlet mode. can only be used
	 * to target an action on a public page
	 * 
	 * @param pageName
	 * @param portletName
	 * @param portletModeName
	 *            string representation of the portletMode
	 * @param params
	 * @param serverContext
	 * @param nonPublicPage
	 * @return
	 * @throws StateException
	 * @throws NamingException
	 * @throws IOException
	 */
	public static String generateUrl(String pageName, String portletName,
			String portletModeName, HashMap params,
			MyServerContext serverContext, boolean nonPublicPage,
			PortletType portletType) throws StateException, NamingException,
			IOException {
		String finalUrl = "";

		final StateManager service = getStateManager();

		final URLAccessorFactory urlFactory = (URLAccessorFactory) service
				.getAccessorFactory(URLAccessorFactory.class);

		// Request a URL
		// The 3rd argument specifies whether the URL points to the protected
		// (authenticated) area;
		// pass in "false" if your page is accessible for unauthenticated users

		final EngineURL url = urlFactory.newURL(serverContext, false,
				nonPublicPage, service.newState(), Constants.EMPTY_COPY);

		final PortletTargetAccessorFactory tAccFct = (PortletTargetAccessorFactory) service
				.getAccessorFactory(PortletTargetAccessorFactory.class);
		final PortletAccessorFactory pAccFct = (PortletAccessorFactory) service
				.getAccessorFactory(PortletAccessorFactory.class);
		// get a new URL from the URL factory
		// null indicates that the current state should be copied

		final SelectionAccessorFactory selectionFactory = (SelectionAccessorFactory) service
				.getAccessorFactory(SelectionAccessorFactory.class);
		// Request the selection controller to set the page; pass in the state
		// associated with the created URL
		final SelectionAccessorController selectionCtrl = selectionFactory
				.getSelectionAccessorController(url.getState());
		// Set the page; you need the unique name (String) or the ObjectID of
		// that page
		selectionCtrl.setSelection(pageName);
		// Dispose the accessor (avoids memory leak)
		selectionCtrl.dispose();
		// get a target controller that operates on the URL-specific state
		final PortletTargetAccessorController tCtrl = getPortletTargetAccessor(
				portletType, tAccFct, url);
		// get a portlet controller that operates on the URL-specific state
		final PortletAccessorController pCtrl = getPortletAccessor(portletType,
				pAccFct, portletName, url);
		if (portletModeName != null) {
			PortletMode portletMode = new PortletMode(portletModeName);
			pCtrl.setPortletMode(portletMode);
		}
		try {
			// set the action target
			tCtrl.setActionTarget(portletName);
			// set the action parameters
			pCtrl.getParameters().putAll(params);
		} finally {
			tCtrl.dispose();
			pCtrl.dispose();
		}
		finalUrl = url.writeDispose(new StringWriter()).toString();
		return finalUrl;
	}

	/**
	 * Generates a url to an action, changing the mode, or changing the window
	 * state, can only be used to target an action on a public page
	 * 
	 * @param pageName
	 * @param portletName
	 * @param portletModeName
	 * @param windowState
	 * @param params
	 * @param serverContext
	 * @param nonPublicPage
	 * @return
	 * @throws StateException
	 * @throws NamingException
	 * @throws IOException
	 */
	@Deprecated
	public static String generateUrl(String pageName, String portletName,
			String portletModeName, WindowState windowState, HashMap params,
			MyServerContext serverContext, boolean nonPublicPage,
			PortletType portletType) throws StateException, NamingException,
			IOException {
		String finalUrl = "";

		final StateManager service = getStateManager();

		final URLAccessorFactory urlFactory = (URLAccessorFactory) service
				.getAccessorFactory(URLAccessorFactory.class);

		final PortletTargetAccessorFactory tAccFct = (PortletTargetAccessorFactory) service
				.getAccessorFactory(PortletTargetAccessorFactory.class);
		final PortletAccessorFactory pAccFct = (PortletAccessorFactory) service
				.getAccessorFactory(PortletAccessorFactory.class);

		// get a new URL from the URL factory
		// null indicates that the current state should be copied
		final EngineURL url = urlFactory.newURL(serverContext, false,
				nonPublicPage, service.newState(), Constants.EMPTY_COPY);
		final SelectionAccessorFactory selectionFactory = (SelectionAccessorFactory) service
				.getAccessorFactory(SelectionAccessorFactory.class);
		// Request the selection controller to set the page; pass in the state
		// associated with the created URL
		final SelectionAccessorController selectionCtrl = selectionFactory
				.getSelectionAccessorController(url.getState());
		// Set the page; you need the unique name (String) or the ObjectID of
		// that page
		selectionCtrl.setSelection(pageName);
		// Dispose the accessor (avoids memory leak)
		selectionCtrl.dispose();
		// get a target controller that operates on the URL-specific state
		final PortletTargetAccessorController tCtrl = getPortletTargetAccessor(
				portletType, tAccFct, url);
		// get a portlet controller that operates on the URL-specific state
		final PortletAccessorController pCtrl = getPortletAccessor(portletType,
				pAccFct, portletName, url);
		if (windowState != null) {
			pCtrl.setWindowState(windowState);
		}
		if (portletModeName != null) {
			PortletMode portletMode = new PortletMode(portletModeName);
			pCtrl.setPortletMode(portletMode);
		}
		try {
			// set the action target
			tCtrl.setActionTarget(portletName);
			// set the action parameters
			pCtrl.getParameters().putAll(params);
		} finally {
			tCtrl.dispose();
			pCtrl.dispose();
		}
		finalUrl = url.writeDispose(new StringWriter()).toString();
		return finalUrl;
	}

	/**
	 * Will generate a url to allow you to log the user out using the portal
	 * logout command
	 * 
	 * @param request
	 * @param response
	 * @return string for the url
	 * @throws StateException
	 * @throws NamingException
	 * @throws IOException
	 */
	@Deprecated
	public static String generateLogOutURL(HttpServletRequest request,
			HttpServletResponse response, MyServerContext serverContext,
			boolean nonPublicPage) throws StateException, NamingException,
			IOException {
		String finalUrl = "";
		final StateManager service = getStateManager();

		// Get the URL factory
		// URLFactory urlFactory = mgr.getURLFactory();
		final URLAccessorFactory urlFactory = (URLAccessorFactory) service
				.getAccessorFactory(URLAccessorFactory.class);

		final EngineURL url;
		com.ibm.portal.state.accessors.action.engine.logout.LogoutActionAccessorController logoutCtrl = null;
		try {
			url = urlFactory.newURL(serverContext, false, nonPublicPage,
					service.newState(), Constants.EMPTY_COPY);

			com.ibm.portal.state.accessors.action.engine.logout.LogoutActionAccessorFactory logoutFct = (com.ibm.portal.state.accessors.action.engine.logout.LogoutActionAccessorFactory) service
					.getAccessorFactory(com.ibm.portal.state.accessors.action.engine.logout.LogoutActionAccessorFactory.class);
			logoutCtrl = logoutFct.newLogoutActionController(request, url
					.getState());

			finalUrl = url.writeDispose(new StringWriter()).toString();
		} finally {
			if (logoutCtrl != null) {
				logoutCtrl.dispose();
			}
		}
		return finalUrl;
	}

	/**
	 * This is a private method only to be used internally to return the correct
	 * PortletAccessor
	 * 
	 * @param pType
	 *            PortletType
	 * @param pAccFct
	 *            PortletAccessorFactory
	 * @param portletName
	 *            uniquename of the portlet on the page
	 * @param url
	 *            EngineURL created in the calling method
	 * @return PortletAccessorController or LegacyPortletAccessorController
	 */
	@Deprecated
	private static PortletAccessorController getPortletAccessor(
			PortletType pType, final PortletAccessorFactory pAccFct,
			String portletName, EngineURL url) {
		String methodName = "getPortletAccessor";
		try {
			if (pType.getPortletType().equals(pType.LEGACY_PORTLET)) {

				return pAccFct.getLegacyPortletAccessorController(portletName,
						url.getState());
			} else {
				return pAccFct.getPortletAccessorController(portletName, url
						.getState());
			}
		} catch (Exception ex) {
			executionContext.log(Level.SEVERE,"There was a problem in the getPortletAccessor: "+ex.getMessage(),className,methodName,ex);
			return null;
		}
	}

	/**
	 * @param pType
	 *            PortletType
	 * @param tAccFct
	 *            PortletTargetAccessorFactory
	 * @param url
	 *            EngineURL created in calling method
	 * @return PortletTargetAccessorController this can be of type
	 *         LegacyPortletTargetAccessorController or just
	 *         PortletTargetAccessorController
	 * 
	 */
	@Deprecated
	private static PortletTargetAccessorController getPortletTargetAccessor(
			PortletType pType, final PortletTargetAccessorFactory tAccFct,
			EngineURL url) {
		String methodName = "getPortletTargetAccessor";
		try {
			if (pType.getPortletType().equals(pType.LEGACY_PORTLET)) {
				return tAccFct.getLegacyPortletTargetAccessorController(url
						.getState());
			} else {
				return tAccFct.getPortletTargetAccessorController(url
						.getState());
			}
		} catch (Exception ex) {
			executionContext.log(Level.SEVERE,"There was a problem in the getPortletTargetAccessor: "+ex.getMessage(),className,methodName,ex);
			return null;
		}
	}

}
