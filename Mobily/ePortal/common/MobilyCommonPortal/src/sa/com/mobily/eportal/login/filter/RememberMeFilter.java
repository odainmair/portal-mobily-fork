package sa.com.mobily.eportal.login.filter;

import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.LoggerHelper;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

public class RememberMeFilter implements javax.servlet.Filter
{
	private static final Logger logger = LoggerHelper.getLogger(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);

	private static String eaiLoginUrl = null;

	public static Locale getLocalefromString(String locale)
	{
		String parts[] = locale.split("_", -1);
		if (parts.length == 1)
			return new Locale(parts[0]);
		else if (parts.length == 2 || (parts.length == 3 && parts[2].startsWith("#")))
			return new Locale(parts[0], parts[1]);
		else
			return new Locale(parts[0], parts[1], parts[2]);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
	{
		logger.log(Level.FINE, "RememberMeFilter > doFilter > Remeber me Filter has been started.");
		HttpServletRequest req = (HttpServletRequest) request;
		req.setCharacterEncoding("UTF-8");
		HttpServletResponse resp = (HttpServletResponse) response;
		String localeStr = req.getParameter("locale");
		String langStr = req.getParameter("lang");
		if (FormatterUtility.isNotEmpty(langStr))
		{
			logger.log(Level.FINE, "RememberMeFilter > doFilter > Received lang parameter in query string=" + langStr);
			try
			{
				resp.sendRedirect(req.getRequestURL().toString().replace("?lang=", "?locale="));
				logger.log(Level.FINE, "RememberMeFilter > doFilter > After redirecting user for removing lang parameter from url");
			}
			catch (Exception e)
			{
			}
		}
		if (FormatterUtility.isNotEmpty(localeStr))
		{
			// wrap the request with our locale request wrapper
			try
			{
				if (localeStr.equalsIgnoreCase("ar"))
					req = new LocaleRequestWrapper(req);
				else if (localeStr.equalsIgnoreCase("en"))
					req = new LocaleRequestWrapper(req);
				logger.log(Level.FINE, "RememberMeFilter > doFilter > Local request wrapper created successfully");
			}
			catch (Exception e)
			{
				logger.log(Level.SEVERE, "RememberMeFilter > doFilter > Error occured in creating locale request wrapper > Exception::" + e.getMessage());
			}
		}

		String currentUrl = req.getRequestURL().toString();
		String username = getUsername(req);// read remember me cookie
		if (FormatterUtility.isNotEmpty(username) && !hasSession(req))
		{
			logger.log(Level.INFO, "RememberMeFilter > doFilter > Cookie has been found for user[" + username + "] Current URL[" + currentUrl + "]");
			resp.sendRedirect(eaiLoginUrl);
		}
		else
		{
			logger.log(Level.INFO, "RememberMeFilter > doFilter > Cookie has not been found .");
			if (chain != null)
				chain.doFilter(req, response);
		}
		logger.log(Level.INFO, "RememberMeFilter > doFilter > Remeber me Filter has been  completed.");
	}

	@Override
	public void destroy()
	{

	}

	@Override
	public void init(FilterConfig config) throws ServletException
	{
		eaiLoginUrl = ResourceEnvironmentProviderService.getInstance().getStringProperty("EAI_LOGIN_URL");
	}

	private static String getUsername(HttpServletRequest request)
	{
		String username = null;
		Cookie[] cookies = request.getCookies();
		if (cookies != null)
		{
			for (int i = 0; i < cookies.length; i++)
			{
				Cookie c = cookies[i];
				if (c.getName().equals("username"))
					username = c.getValue();
			}
		}
		return username;
	}

	private static boolean hasSession(HttpServletRequest req)
	{
		boolean hasSession = false;
		Cookie[] cookies = req.getCookies();
		if (cookies != null)
			for (int i = 0; i < cookies.length; i++)
			{
				if ("LtpaToken".equals(cookies[i].getName()))
					hasSession = true;
			}
		return hasSession;
	}
}
