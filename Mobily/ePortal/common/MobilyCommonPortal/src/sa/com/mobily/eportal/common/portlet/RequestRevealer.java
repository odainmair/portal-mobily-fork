package sa.com.mobily.eportal.common.portlet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//from wp.engine.impl.jar
import com.ibm.wps.engine.ExtendedLocaleRequest;
//from wp.engine.impl.jar
import com.ibm.wps.engine.PortalRequestWrapper;
// from WAS_HOME\plugins\com.ibm.ws.portletcontainer_6.1.0.jar
import com.ibm.ws.portletcontainer.portlet.PortletUtils;

public class RequestRevealer
{
	public static HttpServletResponse getResponse(PortletResponse portletResponse)
	{
		HttpServletResponse httpServletResponse;
		if (portletResponse instanceof ActionResponse)
		{
			httpServletResponse = PortletUtils.getHttpServletResponse((ActionResponse) portletResponse);
		}
		else if (portletResponse instanceof RenderResponse)
		{
			httpServletResponse = PortletUtils.getHttpServletResponse((RenderResponse) portletResponse);
		}
		else
		{
			throw new RuntimeException("Wrong type...:" + portletResponse);
		}
		return httpServletResponse;
	}

	public static HttpServletRequest getRequest(PortletRequest portletRequest)
	{
		HttpServletRequest httpServletRequest;
		if (portletRequest instanceof ActionRequest)
		{
			httpServletRequest = PortletUtils.getHttpServletRequest((ActionRequest) portletRequest);
		}
		else if (portletRequest instanceof RenderRequest)
		{
			httpServletRequest = PortletUtils.getHttpServletRequest((RenderRequest) portletRequest);
		}
		else
		{
			throw new RuntimeException("Wrong type...:" + portletRequest);
		}
		PortalRequestWrapper portalRequestWrapper = (PortalRequestWrapper) httpServletRequest;
		ExtendedLocaleRequest extendedLocaleRequest = (ExtendedLocaleRequest) portalRequestWrapper.getRequest();
		HttpServletRequest response = (HttpServletRequest) extendedLocaleRequest.getRequest();
		return response;
	}
}