package sa.com.mobily.eportal.common.lookup.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import sa.com.mobily.eportal.common.service.exception.SystemException;
import sa.com.mobily.eportal.common.service.lookup.common.LookupCommonService;
import sa.com.mobily.eportal.common.service.lookup.common.LookupCommonServiceImpl;
import sa.com.mobily.eportal.common.service.lookup.vo.Lookup;


public class LookUpCommonCustomTag extends TagSupport{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static ResourceBundle bundle = null;
	private String id = "";
	private String name = "";
	private String locale = "";
	private String objectId = "";
	private String selectType = "";
	private String styleScript = "";
	private String required = "";
	private String componentId = "";
	private String selected = "";
	private String validate = "";
	private String idForScript = "";
	private String errorMessageRecordsNotFound = "<div class = " + "alert_msg alert_yield_msg" + " id= " + "idError " + ">"
									+"<p> No Record Found for given Id </p> " + " </div>";
	private String errorMessage = "<div class = " + "alert_msg alert_yield_msg" + " id= " + "idError " + ">"
									+"<p> Please Enter Valid Id </p> " + " </div>";
	
	private LookupCommonService lookUpCommonService = new LookupCommonServiceImpl();
	private List<Lookup> lookupList = new ArrayList<Lookup>();

	@Override
	public int doStartTag() throws JspException {
		JspWriter out =pageContext.getOut();
		String lookUpId = this.getId();
		StringBuffer strBuffer = new StringBuffer();
		String resourcePropertyPrefix="lookup.select.label.";
		if(!(this.getId().equalsIgnoreCase("null") || this.getId().equalsIgnoreCase("0"))){
			
		this.lookupList = lookUpCommonService.findById(lookUpId, this.getLocale().equalsIgnoreCase("en"));
		try{
			bundle = ResourceBundle.getBundle("sa.com.mobily.eportal.common.lookup.helper.nl.CustomLookupResource", new Locale(getLocale()));
		}
		catch (Exception e){
			throw new SystemException(e);
		}
		String selectedItem = "";
		String itemDesc = "";
		if(this.lookupList != null && this.lookupList.size() > 0) {
			
				if(this.getComponentId() != ""){
					objectId =" id = "+ "'" +getComponentId()+ "'" ;
				}
				else{
					objectId = " ";
				}
					selectType = " dojoType= " + "'dijit.form.Select'";
					styleScript = " maxHeight=-1 ";
				if(!(this.getRequired().equalsIgnoreCase("null")) || (this.getRequired().equalsIgnoreCase(""))){	
					validate = " required = "+ "'" +getRequired()+ "'";
				}	
					if(this.getSelected().equalsIgnoreCase("")){
						selectedItem = "  ";
					}
					else{
						selectedItem = " selected ";
					}
			  strBuffer.append("<SELECT class='cta_select' name="+"'"+name+"'"+" "+objectId+" "+selectType+"  "+styleScript+" "+validate+">");
			  String defaultSelectLabel=null;
			  try{
				  defaultSelectLabel = bundle.getString(resourcePropertyPrefix+lookUpId);
			  }catch(Exception e){
				  defaultSelectLabel = bundle.getString("lookup.select.label");
			  }
			  if(defaultSelectLabel != null && defaultSelectLabel.length() != 0)
				  strBuffer.append("<OPTION>"+ defaultSelectLabel +"</OPTION>");
			   for(int i =0 ; i< lookupList.size() ; i++) {
				   if(this.getLocale().equalsIgnoreCase("en")){
					   itemDesc = lookupList.get(i).getItemDescriptionInEnglish();
				   }
				   else if(this.getLocale().equalsIgnoreCase("ar")){
					   itemDesc = lookupList.get(i).getItemDescriptionInArabic();   
				   }   
					
				   if(this.selected.equalsIgnoreCase(String.valueOf(lookupList.get(i).getSubId()))){
					   strBuffer.append("<OPTION value = '"+this.selected+"'" +selectedItem+">"+ itemDesc +"</OPTION>");
				   }
				   
				   if(lookupList.get(i).getSubId() != 0 && (!(this.selected.equalsIgnoreCase(String.valueOf(lookupList.get(i).getSubId()))))){
					   if(lookupList.get(i).getSubId() == -1){
						   strBuffer.append("<OPTION value = ''" + ">"+ itemDesc +"</OPTION>");
					   }
					   else{
						   if(lookUpId.equals("9") || lookUpId.equals("2"))
						   {
							   // if id == 9 for countries. Values should be country name.  No relationship required
							// if id == 2 for cities. Values should be city name.  No relationship required
							   strBuffer.append("<OPTION value = '"+lookupList.get(i).getItem()+"'" + ">"+ itemDesc +"</OPTION>");
						   }
						   else
						   {
							   strBuffer.append("<OPTION value = '"+lookupList.get(i).getSubId()+"'" + ">"+ itemDesc +"</OPTION>");
						   }
					   }
				   }
					   
			  }
		  
		  strBuffer.append("</SELECT>");
		}
		else{
			strBuffer.append(errorMessageRecordsNotFound);
		}
		}
		else{
			strBuffer.append(errorMessage);
		}
		  try {
			out.println(strBuffer.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return super.doStartTag();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	/**
	 * @return the required
	 */
	public String getRequired() {
		return required;
	}

	/**
	 * @param required the required to set
	 */
	public void setRequired(String required) {
		this.required = required;
	}

	/**
	 * @return the componentId
	 */
	public String getComponentId() {
		return componentId;
	}

	/**
	 * @param componentId the componentId to set
	 */
	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}
	
	
	
}
