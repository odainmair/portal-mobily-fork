/* @copyright module */
/*                                                                            */
/*  DISCLAIMER OF WARRANTIES:                                                 */
/*  -------------------------                                                 */
/*  The following [enclosed] code is sample code created by IBM Corporation.  */
/*  This sample code is provided to you solely for the purpose of assisting   */
/*  you in the development of your applications.                              */
/*  The code is provided "AS IS", without warranty of any kind. IBM shall     */
/*  not be liable for any damages arising out of your use of the sample code, */
/*  even if they have been advised of the possibility of such damages.        */

package sa.com.mobily.eportal.common.urlhelper;

import java.nio.charset.Charset;
import com.ibm.portal.state.accessors.url.ServerContext;

/**
 * @author James Barnes
 *
 */
public class MyServerContext implements ServerContext {
	
	private String hostname = "";
	private String port = "";


	/**
	 * This constructor will set it up so that you can create a link to your portal
	 * based on the hostname and port passed in
	 * @param hostname
	 * @param port
	 */
	public MyServerContext(String hostname, String port) {
		super();
		this.hostname = hostname;
		this.port = port;
	}

 
	/**
	 * Change this if you have changed your portal context root
	 * @see com.ibm.portal.state.accessors.url.ServerContext#getContextPath()
	 */
	public String getContextPath() {
		return "/wps";
 	}

	/**
	 * change this if you have changed it as well for your protected url
	 * @see com.ibm.portal.state.accessors.url.ServerContext#getHomeProtected()
	 */
	public String getHomeProtected() {
		return "/myportal";
	}

	/**
	 * Change this if it is different for the public url
	 * @see com.ibm.portal.state.accessors.url.ServerContext#getHomePublic()
	 */
	public String getHomePublic() {		
		return "/portal";
	}

	/**
	 * @see com.ibm.portal.state.accessors.url.ServerContext#getHostName()
	 */
	public String getHostName() {
		return hostname;
	}

	/**
	 * 	@see com.ibm.portal.state.accessors.url.ServerContext#getHostPortHTTP()
	 */
	public String getHostPortHTTP() {
		return port;
	}

	/**
	 * if you use ssl and it is different that 443 change it to the correct one
	 * @see com.ibm.portal.state.accessors.url.ServerContext#getHostPortHTTPS()
	 */
	public String getHostPortHTTPS() {
		return "443";
	}

	/**
	 * @see com.ibm.portal.state.accessors.url.ServerContext#getURLCharset()
	 */
	public Charset getURLCharset() {
		return Charset.forName("UTF-8");
	}
}


