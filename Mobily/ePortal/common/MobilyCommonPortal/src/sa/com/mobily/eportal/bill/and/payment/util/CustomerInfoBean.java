package sa.com.mobily.eportal.bill.and.payment.util;

import java.util.Map;

import sa.com.mobily.eportal.cacheinstance.valueobjects.SubscriptionVO;
import sa.com.mobily.eportal.common.service.vo.BaseVO;

public class CustomerInfoBean  extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 394273986623953699L;
	private String msidsn;
	private String serviceAcctNumber;
	private String customerType;
	private int subscriptionType;
	private int lineType;
	private String userId;
	private Map<String,SubscriptionVO> otherSupscription;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMsidsn() {
		return msidsn;
	}
	public void setMsidsn(String msidsn) {
		this.msidsn = msidsn;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public int getLineType() {
		return lineType;
	}
	public void setLineType(int lineType) {
		this.lineType = lineType;
	}
	public int getSubscriptionType() {
		return subscriptionType;
	}
	public void setSubscriptionType(int subscriptionType) {
		this.subscriptionType = subscriptionType;
	}
	public Map<String,SubscriptionVO> getOtherSupscription() {
		return otherSupscription;
	}
	public void setOtherSupscription(Map<String,SubscriptionVO> otherSupscription) {
		this.otherSupscription = otherSupscription;
	}
	public String getServiceAcctNumber()
	{
		return serviceAcctNumber;
	}
	public void setServiceAcctNumber(String serviceAcctNumber)
	{
		this.serviceAcctNumber = serviceAcctNumber;
	}
	
}
