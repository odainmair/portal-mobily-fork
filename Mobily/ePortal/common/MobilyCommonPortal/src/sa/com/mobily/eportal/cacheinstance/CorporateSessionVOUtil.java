package sa.com.mobily.eportal.cacheinstance;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils; 

import sa.com.mobily.eportal.cacheinstance.dao.CorporateSubscriptionDAO;
import sa.com.mobily.eportal.cacheinstance.dao.SessionVODAO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.CorporateSubscriptionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.RefactoredSessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SubscriptionVO;
import sa.com.mobily.eportal.cacheinstance.valueobjects.UserAccountVO;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.corporate.dao.PersonalizationDao;

public class CorporateSessionVOUtil
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.CACHING_SERVICE_LOGGER_NAME);

	private static final String className = CorporateSessionVOUtil.class.getName();

	public static SessionVO getExistingSessionVO(UserAccountVO userAccount, List<RefactoredSessionVO> refactoredSessionVOs, String portalUserName)
	{
		String method = "getExistingSessionVO";
		CorporateSubscriptionVO defaultCorpSubcriptoin = null;
		SessionVO sessionVONew = null;
		List<CorporateSubscriptionVO> corpVOList = CorporateSubscriptionDAO.getInstance().getCorporateSubscriptionsByAccountId(portalUserName);
		if (CollectionUtils.isNotEmpty(corpVOList))
		{
			for (CorporateSubscriptionVO corpVO : corpVOList)
			{
				if (corpVO.getSubscriptionId() == refactoredSessionVOs.get(0).getSubscriptionId())
				{
					defaultCorpSubcriptoin = corpVO;
					corpVOList.remove(corpVO);
					break;
				}
			}
		}
		else
		{
			throw new ServiceException("No subscription found for corporate user " + portalUserName);
		}
		executionContext.log(Level.FINER, "Session retrieved from DB for corporate user : " + portalUserName, className, method, null);
		sessionVONew = buildCorporateSessionVO(userAccount, defaultCorpSubcriptoin, corpVOList);
		return sessionVONew;
	}

	public static SessionVO getNewSessionVO(UserAccountVO userAccount, String portalUserName, String sessionID)
	{
		String method = "getNewSessionVO";
		CorporateSubscriptionVO defaultCorpSubcriptoin = null;
		SessionVO sessionVONew = null;
		RefactoredSessionVO refactoredSessionVO = new RefactoredSessionVO();
		refactoredSessionVO.setSessionId(sessionID);
		refactoredSessionVO.setUserName(portalUserName);

		List<CorporateSubscriptionVO> corpVOList = CorporateSubscriptionDAO.getInstance().getCorporateSubscriptionsByAccountId(portalUserName);
		if (CollectionUtils.isNotEmpty(corpVOList))
		{
			defaultCorpSubcriptoin = corpVOList.get(0);
			corpVOList.remove(defaultCorpSubcriptoin);
			refactoredSessionVO.setSubscriptionId(defaultCorpSubcriptoin.getSubscriptionId());
			refactoredSessionVO.setCreatedDate(new Timestamp(new Date().getTime()));
			int result = SessionVODAO.getInstance().insertUserSessionVO(refactoredSessionVO);
			if (result <= 0)
				throw new ServiceException("Can not insert record in session table for user " + portalUserName + " and sessionId " + refactoredSessionVO.getSessionId());
			sessionVONew = buildCorporateSessionVO(userAccount, defaultCorpSubcriptoin, corpVOList);
			executionContext.log(Level.FINEST, "Created session entry in DB for corporate user : " + portalUserName, className, method, null);
		}
		return sessionVONew;
	}

	public static SessionVO resetSessionVO(UserAccountVO userAccount, String portalUserName, String newNumber, String sessionID)
	{
		CorporateSubscriptionVO defaultCorpSubcriptoin = null;
		SessionVO sessionVONew = null;
		List<CorporateSubscriptionVO> corpVOList = CorporateSubscriptionDAO.getInstance().getCorporateSubscriptionsByAccountId(portalUserName);
		if (CollectionUtils.isNotEmpty(corpVOList))
		{
			for (CorporateSubscriptionVO corpVo : corpVOList)
			{
				if ((StringUtils.isNotEmpty(corpVo.getMasterBillingAccount()) && corpVo.getMasterBillingAccount().equals(newNumber)))
				{
					defaultCorpSubcriptoin = corpVo;
					corpVOList.remove(corpVo);
					break;
				}
			}
			sessionVONew = buildCorporateSessionVO(userAccount, defaultCorpSubcriptoin, corpVOList);
			RefactoredSessionVO refactoredSessionVO = new RefactoredSessionVO();
			refactoredSessionVO.setSessionId(sessionID);
			refactoredSessionVO.setSubscriptionId(defaultCorpSubcriptoin.getSubscriptionId());
			SessionVODAO.getInstance().updateUserSessionVO(refactoredSessionVO);
		}
		return sessionVONew;
	}

	private static SessionVO buildCorporateSessionVO(UserAccountVO userAccount, CorporateSubscriptionVO defaultCorpSubscription,
			List<CorporateSubscriptionVO> otherCorpSubscriptionList)
	{
		String method = "buildCorporateSessionVO";
		executionContext.log(Level.FINER,
				"userAccount.getUserName() : " + userAccount.getUserName() + " defaultCorpSubscription.getSubscriptionId() : " + defaultCorpSubscription.getSubscriptionId(),
				className, method, null);
		SessionVO sessionVONew = new SessionVO();
		sessionVONew.setUserAcctId(userAccount.getUserAccountId());
		sessionVONew.setMsisdn(defaultCorpSubscription.getMobile());
		sessionVONew.setSubscriptionID(defaultCorpSubscription.getSubscriptionId());
		sessionVONew.setRootBillingAccountNumber(defaultCorpSubscription.getMasterBillingAccount());
		sessionVONew.setUserName(userAccount.getUserName());
		sessionVONew.setLastLoginTime(userAccount.getLastLoginTime());
		sessionVONew.setIqama(defaultCorpSubscription.getCommercialId());
		try
		{
			// Getting Personalization Information based corporate sub-accounts'
			// subscriptions
//			PersonalizationVo personalizationVo = PersonalizationDao.getInstance().getPersonalizedVo(defaultCorpSubscription.getMasterBillingAccount());
//
//			// Adding Personalization Information
//			sessionVONew.setHasDiaSubscribers(personalizationVo.getDia() > 0 ? true : false);
//			sessionVONew.setHasEthernetSubscribers(personalizationVo.getEthernet() > 0 ? true : false);
//			sessionVONew.setHasIpVpnSubscribers(personalizationVo.getIpVpn() > 0 ? true : false);
			boolean isIceCubeEligible = PersonalizationDao.getInstance().isIceCubeEligibilty(defaultCorpSubscription.getMasterBillingAccount());
			sessionVONew.setHasIceCubeEligibility(isIceCubeEligible);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE," EXCEPTION IN PERSONALIZATION QUERY"+e.getMessage(),className, method,e);
		}

		Map<String, SubscriptionVO> otherSubscriptions = null;
		if (CollectionUtils.isNotEmpty(otherCorpSubscriptionList))
		{
			otherSubscriptions = new HashMap<String, SubscriptionVO>();
			for (CorporateSubscriptionVO otherCorpSubribptionVO : otherCorpSubscriptionList)
			{
				SubscriptionVO subscriptionVO = new SubscriptionVO();
				subscriptionVO.setSubscriptionID(otherCorpSubribptionVO.getSubscriptionId());
				subscriptionVO.setMsisdn(otherCorpSubribptionVO.getMobile());
				otherSubscriptions.put(otherCorpSubribptionVO.getMobile(), subscriptionVO);
			}
		}
		else
		{
			executionContext.log(Level.INFO, "No subscription found for corporate user : " + userAccount.getUserName(), className, method, null);
		}
		sessionVONew.setOtherSubscriptions(otherSubscriptions);
		return sessionVONew;
	}
}