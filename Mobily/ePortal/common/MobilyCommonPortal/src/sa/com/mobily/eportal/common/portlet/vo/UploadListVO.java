package sa.com.mobily.eportal.common.portlet.vo;

import java.util.ArrayList;

public class UploadListVO
{
private String [] header;
private ArrayList<String[]> rows; 
public String[] getHeader()
{
	return header;
}
public ArrayList<String[]> getRows()
{
	return rows;
}
public void setHeader(String[] header)
{
	this.header = header;
}
public void setRows(ArrayList<String[]> rows)
{
	this.rows = rows;
}
}
