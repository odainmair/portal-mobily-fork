package sa.com.mobily.eportal.core.client;

import sa.com.mobily.eportal.core.api.User;

/**
 * A implementation of the User interface to provide read-only methods
 * of the user information.
 * The Context interface returns an instance of this class.
 * @see Context
 * @see User
 */
class UserImpl implements User {

    private String userId;
    private String serviceAccountNumber;
    private String defaultMSISDN;
    private String iqama;
    private String customerType;
    private String packageId;
    private int subscriptionType;
    private int lineType;

    @Override
    public String getUserId() {
	return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String getServiceAccountNumber() {
	return serviceAccountNumber;
    }

    public void setServiceAccountNumber(String serviceAccountNumber) {
        this.serviceAccountNumber = serviceAccountNumber;
    }

    @Override
    public String getDefaultMSISDN() {
	return defaultMSISDN;
    }
    
    public void setDefaultMSISDN(String defaultMSISDN) {
        this.defaultMSISDN = defaultMSISDN;
    }

    @Override
    public String getIqama() {
        return iqama;
    }

    public void setIqama(String iqama) {
        this.iqama = iqama;
    }

    @Override
    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @Override
    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    @Override
    public int getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(int subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    @Override
    public int getLineType() {
        return lineType;
    }

    public void setLineType(int lineType) {
        this.lineType = lineType;
    }
}
