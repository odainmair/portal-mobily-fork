//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.common.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * <p>
 * <code>CoreTestServlet</code> acts as the base test servlet which 
 * will be extended by all child test servlets in order to run JUnit test classes.
 * </p>
 * @author hazems
 *
 */
public abstract class CoreTestServlet extends HttpServlet {

    private static final long serialVersionUID = 12323283833832L;

    public CoreTestServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String enableTestingMode = getServletContext().getInitParameter("ENABLE_TESTING_MODE");

        if (enableTestingMode == null || !enableTestingMode.equals("true")) {
            return; // Disable testing ...
        }

        JUnitCore junit = new JUnitCore();
        Result result = junit.run(getTestSuite());
        int index = 1;

        String output = "<h1>Test Results</h1>"
                      + "<br/>Number of running tests: " + result.getRunCount() + "<br/>"
                      + "<br/>Number of failing tests: " + result.getFailureCount() + "<br/>";

        List<Failure> failures = result.getFailures();

        if (failures.size() > 0) {
            output += "<h2>Failing Tests</h2>";
            output += "<ol>";
        }

        for (Failure failure : failures) {
            output += "<li style='color:red'>" + failure.getDescription() + ", Message[" + failure.getMessage() + "]</li>";
            ++index;
        }

        if (failures.size() > 0) {
            output += "</ol>";
        }

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Test Report</title></head><body>" + output + "</body></body>");
        out.close();
    }
    
    protected abstract Class<?> getTestSuite();
}
