package sa.com.mobily.eportal.core.client;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import sa.com.mobily.eportal.common.exception.system.MobilySystemException;
import sa.com.mobily.eportal.core.service.Logger;

/**
 * Servlet Filter implementation class RESTFilter
 */
@WebFilter("/api/*" ) 
public class RESTFilter implements Filter {

    /**
     * Default constructor. 
     */
    public RESTFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String clazz = RESTFilter.class.getName();
		String method = "doFilter";
		boolean exceptionOccured = false;
		Logger logger = Logger.getLogger(clazz);
		try{
			logger.entering(method);
			// pass the request along the filter chain
			chain.doFilter(request, response);
		} catch(Throwable t) {
			exceptionOccured = true;
			throw new MobilySystemException(t.getMessage(), t);
		} finally {
			logger.exiting(method);
			logger.flush(clazz, exceptionOccured);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
