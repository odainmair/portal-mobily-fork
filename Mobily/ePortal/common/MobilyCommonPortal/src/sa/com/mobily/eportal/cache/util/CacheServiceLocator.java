package sa.com.mobily.eportal.cache.util;

import java.util.ResourceBundle;
import java.util.logging.Level;

import javax.naming.NamingException;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

import com.ibm.portal.portlet.service.PortletServiceHome;

/**
 * 
 * @author Muzammil Mohsin Shaikh
 * 
 */
public class CacheServiceLocator
{
	private static final String className = CacheServiceLocator.class.getName();

	private static final String propertiesFile = "cacheinstance_configuration";

	private static final String PORTLET_SERVICE_JNDI = "PORTLET_SERVICE_JNDI";

	private PortletServiceHome portletServiceHome = null;

	private static CacheServiceLocator instance = null;
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.CACHING_SERVICE_LOGGER_NAME);
	private CacheServiceLocator()
	{

	}

	public static PortletServiceHome getDistributedMapService()
	{
		if (instance == null)
		{
			instance = new CacheServiceLocator();
		}
		if (instance.getPortletServiceHome() != null)
		{
			return instance.getPortletServiceHome();
		}
		try
		{
			ResourceBundle properties = ResourceBundle.getBundle(propertiesFile);
			javax.naming.Context ctx = new javax.naming.InitialContext();
			Object objectHome = ctx.lookup(properties.getString(PORTLET_SERVICE_JNDI));

			if (objectHome != null)
			{
				instance.setPortletServiceHome((PortletServiceHome) objectHome);
			}
		}
		catch (NamingException namingexception)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "NamingException caught at PortletServiceLookup! " + namingexception.getMessage(), className,
					"PortletServiceHome", namingexception);
			executionContext.log(logEntryVO);
		}
		return instance.getPortletServiceHome();
	}

	private PortletServiceHome getPortletServiceHome()
	{
		return portletServiceHome;
	}

	private void setPortletServiceHome(PortletServiceHome portletServiceHome)
	{
		this.portletServiceHome = portletServiceHome;
	}
}