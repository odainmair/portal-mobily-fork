package sa.com.mobily.eportal.login.exception;

/**
 * Exception class for Login Filter Project.
 * 
 * @author Muzammil Mohsin Shaikh
 */
public class MobilyLoginFilterException extends Exception {
	
	private static final long serialVersionUID = 4930842766480478613L;
	private String msg;
    private Throwable cause;
    
    public MobilyLoginFilterException(String msg) {
		super();
		this.msg = msg;
	}
    
    public MobilyLoginFilterException(String msg, Throwable cause) {
		super();
		this.msg = msg;
		this.cause = cause;
	}
    
    public String getMsg() {
		return msg;
	}

	public Throwable getCause() {
		return cause;
	}

	@Override
	public String toString() {
		return "MobilyAuthenticationFilterException [msg=" + msg + ", cause=" + cause + "]";
	}
}
