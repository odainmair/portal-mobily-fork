package sa.com.mobily.eportal.common.util.puma;

public class PumaException extends Exception {

	private static final long serialVersionUID = 1L;
	private String msg;
    private Throwable cause;
    
    public PumaException(String msg, Throwable cause) {
		super();
		this.msg = msg;
		this.cause = cause;
	}
    
    public String getMsg() {
		return msg;
	}

	public Throwable getCause() {
		return cause;
	}

	@Override
	public String toString() {
		return "PumaException [msg=" + msg + ", cause=" + cause + "]";
	}
}