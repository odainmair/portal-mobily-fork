package sa.com.mobily.eportal.bill.and.payment.util;

/**
 * Bill and payment constants class
 * @author Mohammad Sharaf - SBM
 * @author Abdalla Khalaf - SBM
 * */
public class BillAndPaymentConstants {

	public static final String EMAIL_SENDER_KEY = "BILL_ADDRESS_SENDER_EMAIL";
	public static final String ACTIVATION_CODE_PARAM = "activationCodeParam";
	public static final String EMAIL_PARAM = "emailParam";
	public static final String MSIDSN_PARAM = "msidsnParam";
	public static final String INVOICE_EMAIL_ACTIVATION_TYPE = "E";//Invoice email activation
	
	
	public static final String ERROR_OCCURRED = "errorOccurred";
	public static final String GEN_ERROR_OCCURRED = "genericErrorOccurred";
	
	public static final String BILL_AND_PAYMENT_ARABIC_RESOURCE_BUNDLE = "sa.com.mobily.eportal.common.nl.BillAndPayment_ar";
	public static final String BILL_AND_PAYMENT_ENGLISH_RESOURCE_BUNDLE = "sa.com.mobily.eportal.common.nl.BillAndPayment_en";
	
	public static final String BILL_MODE_DESCRIPTION_CONFIG_FILE = "sa.com.mobily.eportal.common.config.BillModeConfig";
	
	public static final String CALLS_TO_MOBILY = "phone.usage.home.label.calls.to.mobily";
	public static final String CALLS_TO_FIXED_NETWORK = "phone.usage.home.label.calls.to.fixed.network";
	public static final String FORWARD_CALLS_TO_FIXED_LINE = "phone.usage.home.label.forward.calls.to.fixed.line";
	public static final String CALLS_TO_OTHER_LOCAL_NETWORK = "phone.usage.home.label.calls.to.other.local.networks";
	public static final String FORWARD_CALLS_TO_OTHER_MOBILE = "phone.usage.home.label.forward.calls.to.other.mobile";
	public static final String VIDEO_CALLS_TO_MOBILY_NETWORK = "phone.usage.home.label.video.calls.to.mobily.network";
	public static final String VIDEO_CALLS_TO_OTHER_LOCAL_NETWORK = "phone.usage.home.label.video.calls.to.other.local.network";
	public static final String CALLS_TO_INTERNATIONAL = "phone.usage.home.label.calls.to.international";
	public static final String CALLS_WHILE_ROAMING = "phone.usage.home.label.calls.while.roaming";
	public static final String VIDEO_CALLS_WHILE_ROAMING = "phone.usage.home.label.video.calls.while.roaming";
	public static final String CALLS_TO_MOBILY_SERVICES = "phone.usage.home.label.calls.to.mobily.services";
	public static final String CALLS_TO_UNIVERSAL_ACCESS_SERVICE = "phone.usage.home.label.calls.to.universal.access.service";
	
	public static final String CALLS_TO_MOBILY_ID = "0";
	public static final String CALLS_TO_FIXED_NETWORK_ID = "1";
	public static final String FORWARD_CALLS_TO_FIXED_LINE_ID = "2";
	public static final String CALLS_TO_OTHER_LOCAL_NETWORK_ID = "3";
	public static final String FORWARD_CALLS_TO_OTHER_MOBILE_ID = "4";
	public static final String VIDEO_CALLS_TO_MOBILY_NETWORK_ID = "5";
	public static final String VIDEO_CALLS_TO_OTHER_LOCAL_NETWORK_ID = "";
	public static final String CALLS_TO_INTERNATIONAL_ID = "6";
	public static final String CALLS_WHILE_ROAMING_ID = "7";
	public static final String VIDEO_CALLS_WHILE_ROAMING_ID= "43";
	public static final String CALLS_TO_MOBILY_SERVICES_ID = "8";
	public static final String CALLS_TO_UNIVERSAL_ACCESS_SERVICE_ID = "38";
	public static final String BILL_MODE_PREFIX = "unique.id.";

	public static final String NUMBER_FORMAT_PATTERN = "^[0-9]*$";

	public static final String ARABIC_LOCALE_SYBMOL = "ar";
	public static final String ENGLISH_LOCALE_SYMBOL = "en";

	public static final String FREE_MINUTES_TO_MOBILY_KEY = "bill.and.payment.home.label.free.minutes.to.mobily";
	public static final String FREE_MINUTES_TO_OUTSIDE_KEY = "bill.and.payment.home.label.free.minutes.to.outside";
	public static final String FREE_MINUTES_TO_OTHER_KEY = "bill.and.payment.home.label.free.minutes.to.other";
	public static final String FREE_MINUTES_TO_MOBILY_WEEKENDS_KEY = "bill.and.payment.home.label.free.minutes.to.mobilyweekends";
	
	public static final String FREE_SMS_TO_MOBILY_KEY = "bill.and.payment.home.label.free.sms.to.mobily";
	public static final String FREE_SMS_TO_OTHER_KEY = "bill.and.payment.home.label.free.sms.to.other";
	
	public static final String FREE_MMS_TO_MOBILY_KEY = "bill.and.payment.home.label.free.mms.to.mobily";
	public static final String FREE_MMS_TO_OTHER_KEY = "bill.and.payment.home.label.free.mms.to.other";
	
	public static final String FREE_MINUTES_LABEL = "bill.and.payment.home.label.free.minutes";
	public static final String FREE_SMS_LABEL = "bill.and.payment.home.label.free.sms";
	public static final String FREE_MMS_LABEL = "bill.and.payment.home.label.free.mms";
	public static final String SAUDI_NUMBERS_PREFIX = "9665";
	public static final String BILL_SUMMARY_DATE_PATTERN = "dd-MMMMM-yyyy";
	public static final String BILL_DETAILS_DATE_PATTERN = "dd MMM yy";
	public static final String BILL_DETAILS_TIME_PATTERN = "hh:mm";
	public static final String PHONE_USAGE_TIME_PATTERN = "hh:mm:ss";
	public static final String HISTORY_PAYMENT_DATE_PATTERN = "yyyyMMddHHmmSS";
	public static final String LAST_BILLS_DATE_PATTERN = "yyyyMM";
	
	
	public static final String LOGGING_ENTERING_METHOD = "Entering Method >> ";
	public static final String LOGGING_EXITING_METHOD  = "Exiting Method >> ";
	public static final String LOGGING_EXCEPTION_OCCURRED = "Exception Occurred >> ";
	public static final String LOGGING_RETURNED_DATA = "Returned Data >> ";
	public static final String LOGGING_CREATING_INSTANCE = "Create Instance >> ";
	public static final String MY_BALANCE_LOGGING_CONFIG_FILE_PATH = "sa.com.mobily.eportal.bill.payment.logging.config.myBalanceLoggerConfig";
	
	public static final String PORTLET_VIEW_PHASE = "doView";
	public static final String PORTLET_ACTION_PHASE = "processAction";
	public static final String PORTLET_RESOURCE_PHASE = "serveResource";
	public static final String PORTLET_HEADERS_PHASE = "doHeaders";
	public static final String EC_PORTLET_CLASS = "portlet-class";
	public static final String EC_PORTLET_METHOD = "portlet-method";
	public static final String EC_EXCEPTION_OCCURRED = "exceptionOccurred";
	public static final String EC_EXCEPTION_OCCURRED_YES_FRAGMENT = "YES";
	public static final String EC_EXCEPTION_OCCURRED_NO_FRAGMENT = "NO";
	public static final String EX_EXCEPTION = "EXCEPTION";
	
	public static final String GET_CUSTOMER_INFO_METHOD_NAME = "getCustomerInfo";
	public static final String GET_FAV_INT_NUMBER_METHOD_NAME = "getFavoriteInternationalNumber";
	public static final String REQUEST_NEW_DOCUMENT_METHOD_NAME = "requestNewDocument";
	
	public static final String BACKEND_REPLY_NULL = "BACK_END_REPLY_NULL";
	
	public static final String EXCEPTION = "EXCEPTION ";
	public static final String LOG_INIT_METHOD_NAME = "init";
	public static final String LOG_DO_VIEW_METHOD_NAME = "doView";
	public static final String LOG_SERVE_RESOURCE_METHOD_NAME = "serveResource";
	
	public static final String XENOS_USER = "JobRunner";
	public static final String XENOS_PWD = "JobRunner";
	
	public static final String KEY_REQUEST_TYPE = "RequestType";
	public static final String MSISDN = "MSISDN";
	public static final String BILL_START_DATE = "BillStartDate";
	public static final String BILL_END_DATE = "BillEndDate";
	public static final String KEY_BILL_NUMBER = "BillNumber";
	public static final String REQUEST_TYPE_RETRIEVE = "RETRIEVE";
	public static final String XENOS_ALIAS = "pdfdsrproject/processFlows/retrieve/ISRARetrieve.xProcessFlow";
	public static final String XENOS_RETRIEVED_DOCUMENT = "RetrievedDocument";
	public static final String XENOS_WEB_SERVICE_URL = "XENOS_WEB_SERVICE_URL";
}
