package sa.com.mobily.eportal.login.filter;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.LoggerHelper;

import com.ibm.portal.auth.SessionTimeoutFilterChain;
import com.ibm.portal.auth.exceptions.UserSessionTimeoutException;
import com.ibm.portal.security.SecurityFilterConfig;
import com.ibm.portal.security.exceptions.SecurityFilterInitException;

public class MobilySessionTimeoutFilter implements com.ibm.portal.auth.SessionTimeoutFilter
{
	private static final Logger logger = LoggerHelper.getLogger(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);

	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void init(SecurityFilterConfig arg0) throws SecurityFilterInitException
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onUserSessionTimeout(HttpSession session, Map sessionTimeoutContext, SessionTimeoutFilterChain chain) throws UserSessionTimeoutException
	{

		// first call the next filter in the chain
		chain.onUserSessionTimeout(session, sessionTimeoutContext);
		try
		{
			String uid = null;
			if (session.getAttribute("uid") != null)
				uid = (String) session.getAttribute("uid");
			logger.log(Level.INFO, "MobilySessionTimeoutFilter > Sessoin timeout has been invoked for sessoin ID=" + session.getId() + " of User name=" + uid);
			if (FormatterUtility.isNotEmpty(uid))
			{
				CacheManagerServiceUtil.removeCachedData(session.getId(), uid);
				logger.log(Level.FINE, "MobilySessionTimeoutFilter > " + uid + " User removed from cache successfully.");
			}
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, "MobilySessionTimeoutFilter > error occured during clearing user session=" + e.getMessage());
		}

	}

}
