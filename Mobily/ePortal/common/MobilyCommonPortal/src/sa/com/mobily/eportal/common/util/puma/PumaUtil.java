package sa.com.mobily.eportal.common.util.puma;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.constants.PumaProfileConstantsIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;

import com.ibm.portal.um.PumaController;
import com.ibm.portal.um.PumaLocator;
import com.ibm.portal.um.PumaProfile;
import com.ibm.portal.um.User;

public class PumaUtil
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.PUMA_SERVICE_LOGGER_NAME);
	private static final String className = PumaUtil.class.getName();

	private Map<String, Object> userAttributeMap;

	public String getSn()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.sn) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.sn);
		}
		return null;
	}

	public String getUid()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.uid) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.uid);
		}
		return null;
	}

	public String getDefaultMsisdn()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.defaultMsisdn) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.defaultMsisdn);
		}
		return null;
	}

	public String getEmail()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.email) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.email);
		}
		return null;
	}

	public String getPreferredLanguage()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.preferredLanguage) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.preferredLanguage);
		}
		return null;
	}

	public String getServiceAccountNumber()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.serviceAccountNumber) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.serviceAccountNumber);
		}
		return null;
	}

	public Integer getDefaultSubscriptionType()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.defaultSubscriptionType) != null)
		{
			return (Integer) userAttributeMap.get(PumaProfileConstantsIfc.defaultSubscriptionType);
		}
		return null;
	}

	public Integer getDefaultSubSubscriptionType()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.defaultSubSubscriptionType) != null)
		{
			return (Integer) userAttributeMap.get(PumaProfileConstantsIfc.defaultSubSubscriptionType);
		}
		return null;
	}
	
	
	public String getUserAcctReference()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.userAcctReference) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.userAcctReference);
		}
		return null;
	}
	public String getDefaultSubId()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.defaultSubId) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.defaultSubId);
		}
		return null;
	}

	public String getCorpMasterAcctNo()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.corpMasterAcctNo) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.corpMasterAcctNo);
		}
		return null;
	}

	public String getCorpAPNumber()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.corpAPNumber) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.corpAPNumber);
		}
		return null;
	}

	public boolean isCorpSurveyCompleted()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.corpSurveyCompleted) != null)
		{
			return (Boolean) userAttributeMap.get(PumaProfileConstantsIfc.corpSurveyCompleted);
		}
		return false;
	}

	public String getPassword()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.password) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.password);
		}
		return null;
	}

	public List getGivenName()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.givenName) != null)
		{
			return (List) userAttributeMap.get(PumaProfileConstantsIfc.givenName);
		}
		return null;
	}

	public String getStatus()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.Status) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.Status);
		}
		return null;
	}

	public String getFirstName()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.firstName) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.firstName);
		}
		return null;
	}

	public String getLastName()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.lastName) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.lastName);
		}
		return null;
	}

	public String getCustSegment()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.custSegment) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.custSegment);
		}
		return null;
	}

	public Integer getRoleId()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.roleId) != null)
		{
			return (Integer) userAttributeMap.get(PumaProfileConstantsIfc.roleId);
		}
		return null;
	}

	public String getSecurityQuestion()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.SecurityQuestion) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.SecurityQuestion);
		}
		return null;
	}

	public String getSecurityAnswer()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.SecurityAnswer) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.SecurityAnswer);
		}
		return null;
	}

	public Date getBirthDate()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.birthDate) != null)
		{
			return (Date) userAttributeMap.get(PumaProfileConstantsIfc.birthDate);
		}
		return null;
	}

	public String getNationality()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.nationality) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.nationality);
		}
		return null;
	}

	public String getGender()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.Gender) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.Gender);
		}
		return null;
	}

	public Date getRegisterationDate()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.registerationDate) != null)
		{
			return (Date) userAttributeMap.get(PumaProfileConstantsIfc.registerationDate);
		}
		return null;
	}

	public String getCommercialRegisterationID()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.commercialRegisterationID) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.commercialRegisterationID);
		}
		return null;
	}

	public String getCorpAPIDNumber()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.corpAPIDNumber) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.corpAPIDNumber);
		}
		return null;
	}
	
	public String getContactNumber()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.contactNumber) != null)
		{
			return (String) userAttributeMap.get(PumaProfileConstantsIfc.contactNumber);
		}
		return null;
	}
	
	public Date getPasswordChangedTime()
	{
		if (userAttributeMap!=null &&userAttributeMap.get(PumaProfileConstantsIfc.passwordChangedTime) != null)
		{
			return (Date) userAttributeMap.get(PumaProfileConstantsIfc.passwordChangedTime);
		}
		return null;
	}
	public long getPasswordChangedPeriod(String Period)
	{
		if (userAttributeMap!=null && userAttributeMap.get(PumaProfileConstantsIfc.passwordChangedTime) != null)
		{
			Date passwordChangedDate = (Date) userAttributeMap.get(PumaProfileConstantsIfc.passwordChangedTime);
			Date currentDate = new Date();
			long diff = currentDate.getTime() - passwordChangedDate.getTime(); 
			if("day".equalsIgnoreCase(Period))
				return (diff / (24 * 60 * 60 * 1000));
			if("month".equalsIgnoreCase(Period))
				return (diff / (30 * 24 * 60 * 60 * 1000));
			if("year".equalsIgnoreCase(Period))
				return (diff / (12 * 30 * 24 * 60 * 60 * 1000));
		}
		return -1;
	}
	public List<String> getAttrbuiteList()
	{
		List<String> attributeList = new ArrayList<String>();
		attributeList.add(PumaProfileConstantsIfc.uid);
		attributeList.add(PumaProfileConstantsIfc.sn);
		attributeList.add(PumaProfileConstantsIfc.defaultMsisdn);
		attributeList.add(PumaProfileConstantsIfc.email);
		attributeList.add(PumaProfileConstantsIfc.preferredLanguage);
		attributeList.add(PumaProfileConstantsIfc.serviceAccountNumber);
		attributeList.add(PumaProfileConstantsIfc.defaultSubscriptionType);
		attributeList.add(PumaProfileConstantsIfc.defaultSubId);
		attributeList.add(PumaProfileConstantsIfc.corpMasterAcctNo);
		attributeList.add(PumaProfileConstantsIfc.corpAPNumber);
		attributeList.add(PumaProfileConstantsIfc.corpSurveyCompleted);
		attributeList.add(PumaProfileConstantsIfc.password);
		attributeList.add(PumaProfileConstantsIfc.givenName);
		attributeList.add(PumaProfileConstantsIfc.Status);
		attributeList.add(PumaProfileConstantsIfc.firstName);
		attributeList.add(PumaProfileConstantsIfc.lastName);
		attributeList.add(PumaProfileConstantsIfc.custSegment);
		attributeList.add(PumaProfileConstantsIfc.roleId);
		attributeList.add(PumaProfileConstantsIfc.SecurityQuestion);
		attributeList.add(PumaProfileConstantsIfc.SecurityAnswer);
		attributeList.add(PumaProfileConstantsIfc.birthDate);
		attributeList.add(PumaProfileConstantsIfc.nationality);
		attributeList.add(PumaProfileConstantsIfc.Gender);
		attributeList.add(PumaProfileConstantsIfc.registerationDate);
		attributeList.add(PumaProfileConstantsIfc.commercialRegisterationID);
		attributeList.add(PumaProfileConstantsIfc.corpAPIDNumber);
		attributeList.add(PumaProfileConstantsIfc.defaultSubSubscriptionType);
		attributeList.add(PumaProfileConstantsIfc.userAcctReference);
		attributeList.add(PumaProfileConstantsIfc.contactNumber);
		attributeList.add(PumaProfileConstantsIfc.passwordChangedTime);
		return attributeList;
	}

	public PumaUtil(String attributeName, String attributeValue)
	{
		try
		{
			PumaProfile pumaProfile = PumaCachedHome.getInstance().getPumaHome().getProfile();
			PumaLocator pumaLocator = PumaCachedHome.getInstance().getPumaHome().getLocator();
			List<User> list = pumaLocator.findUsersByAttribute(attributeName, attributeValue);
			if (list != null && list.size() > 0)
			{
				userAttributeMap = pumaProfile.getAttributes(list.get(0), getAttrbuiteList());
			}
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, e.getMessage(), className, "PumaUtil", e);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	public PumaUtil()
	{
		String method = "PumaUtil";
		try
		{
			PumaProfile pumaProfile = PumaCachedHome.getInstance().getPumaHome().getProfile();

			User user = pumaProfile.getCurrentUser();
			userAttributeMap = pumaProfile.getAttributes(user, getAttrbuiteList());
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	public void updateAttribute(String attributeName, Object attributeValue)
	{
		String method = "updateAttribute";
		try
		{
			PumaProfile pumaProfile = PumaCachedHome.getInstance().getPumaHome().getProfile();
			PumaController pController = PumaCachedHome.getInstance().getPumaHome().getController();
			Map<String, Object> attributeMap = new HashMap<String, Object>();
			attributeMap.put(attributeName, attributeValue);
			User user = pumaProfile.getCurrentUser();
			pController.setAttributes(user, attributeMap);
			LogEntryVO logEntryVO = new LogEntryVO(Level.FINER, "Attribute updated", className, method, null);
			executionContext.log(logEntryVO);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	/**
	 * The attribute map should have one or more attribute with names and values
	 * 
	 * @param attributeMap
	 * @throws ServiceException
	 */
	public void updateAttributes(Map<String, Object> attributeMap)
	{
		String method = "updateAttributes";
		try
		{
			PumaProfile pumaProfile = PumaCachedHome.getInstance().getPumaHome().getProfile();
			PumaController pController = PumaCachedHome.getInstance().getPumaHome().getController();
			User user = pumaProfile.getCurrentUser();
			pController.setAttributes(user, attributeMap);
			LogEntryVO logEntryVO = new LogEntryVO(Level.FINER, "Attributes updated", className, method, null);
			executionContext.log(logEntryVO);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	}

	/**
	 * @param attrName
	 * @param attrValue
	 * @return Will return List of com.ibm.wps.puma.User
	 * @throws ServiceException
	 */

	public List<User> findUsersByAttribute(String attrName, String attrValue)
	{
		String method = "findUsersByAttribute";
		PumaLocator pumaLocator = null;
		List<User> userList = null;
		try
		{
			pumaLocator = PumaCachedHome.getInstance().getPumaHome().getLocator();
			userList = pumaLocator.findUsersByAttribute(attrName, attrValue);
			LogEntryVO logEntryVO = new LogEntryVO(Level.FINER, "Attribute found attrName " + attrName + " attrValue" + attrValue, className, method, null);
			executionContext.log(logEntryVO);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
		return userList;
	}

	/**
	 * @param attributeName
	 * @param attributeValue
	 * @return
	 * @throws ServiceException
	 */
	public User findUserByAttribute(String attributeName, String attributeValue)
	{
		String method = "findUserByAttribute";
		List<User> userList = this.findUsersByAttribute(attributeName, attributeValue);
		User user = null;
		if (null != userList && !userList.isEmpty())
		{
			user = userList.get(0);
			LogEntryVO logEntryVO = new LogEntryVO(Level.FINER, "Attribute found attributeName " + attributeName + " attributeValue" + attributeValue, className, method, null);
			executionContext.log(logEntryVO);
		}
		return user;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public boolean isLoggedInUser(){
		String method = "isLoggedInUser";
		try{
		    String userName = getUid();
		    if(userName != null && !userName.equalsIgnoreCase("anonymous portal user")){
		    	return true;
		    }
		}catch (Exception e) {
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			ServiceException se = new ServiceException(e.getMessage(), e.getCause());
			throw se;
		}
	    return false;
	}
	
	
}