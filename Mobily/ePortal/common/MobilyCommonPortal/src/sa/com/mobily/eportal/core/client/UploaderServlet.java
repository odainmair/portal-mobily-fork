package sa.com.mobily.eportal.core.client;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import sa.com.mobily.eportal.core.dao.TempBlobDAO;
import sa.com.mobily.eportal.core.service.Logger;

/**
 * A servlet that is used by the UI JSPs to upload file attachments
 * and store them in a DB table before being sent to the backend services.
 */
public class UploaderServlet extends HttpServlet {
	private static final long serialVersionUID = 1809724554045451657L;
    private static final Logger LOG = Logger.getLogger(UploaderServlet.class);	
    private static final String MSIE_7 = "MSIE 7";
	private static final String MSIE_8 = "MSIE 8";
	private static final String MSIE_9 = "MSIE 9";
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        String result = "";
        
        try {
            List<FileItem> items = upload.parseRequest(request);
            Iterator<FileItem> iter = items.iterator();
            int index = 0;

            result += "[";
            
            while (iter.hasNext()) {
                FileItem item = iter.next();

                if (!item.isFormField()) {
                    if (index != 0) {
                        result += ", ";
                    }
                    
                    String feedback = processUploadedFile(item);
                    
                    //Handle IE7 ugly uploading bug
                    if (feedback == null) {
                        continue;
                    } else {
                        result += feedback;
                    }
                    
                    ++index;
                }
            }
            
            result += "]";
            
            respondToClient(request, response, result);
            
            LOG.debug(result);
        } catch (FileUploadException e) {
        	generateErrorResponse(request, response, e);
        } catch (Exception e) {
        	generateErrorResponse(request, response, e);
        }
    }

    private static String processUploadedFile(FileItem item) throws SQLException, IOException {
        byte[] data = item.get();
        String fileName = item.getName();
        String contentType = item.getContentType();
        
        // Handle IE7 file uploading ugly bug (It submits falsy file with no name and 0 bytes) ...
        if (fileName.equals("")) {
            return null; //ignore
        }
        
        LOG.info(fileName + ", " + contentType + ", " + data.length);   

        int fileID = TempBlobDAO.getInstance().add(fileName, contentType, data);
			
        return "{'fileName':'" + fileName + "', " + 
                "'contentType':'" + contentType + "', " + 
                "'size':" + data.length + ", " + 
                "'id':" + fileID /*(System.currentTimeMillis() + new Random().nextInt(100))*/ + "}";
    }
    
    private static void respondToClient(HttpServletRequest request, HttpServletResponse response, String result) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();        
        
        writer.write(formatOutput(request, result));

        writer.flush();        
    }
    
    private static String formatOutput(HttpServletRequest request, String result) {
        String browser = request.getHeader("User-Agent");
        
        if (browser.indexOf(MSIE_9) > 0 || browser.indexOf(MSIE_8) > 0 || browser.indexOf(MSIE_7) > 0 ) {
            
            // For IE 9, 8, and 7 browser, render JSON object inside text area ...
            //String sampleOutput = "<textarea>{'success':'true', 'id':'123456789'}</textarea>";
            return ("<textarea>" + result + "</textarea>");
        } else {  
        
        	//For non-IE browsers and IE 10 (or later), render normal JSON objects.
        	//String sampleOutput = "{\"success\":\"true\", \"id\":\"123456789\"}";
        	return (result.replace("'", "\""));
        }    	
    }
    
    private static void generateErrorResponse(HttpServletRequest request, HttpServletResponse response, Exception exception) throws IOException {
        String browser = request.getHeader("User-Agent");
        
        //IE breaks when the HTTP response status is set to Internal Server Error! It looks like it does not expect errors while uploading!!!
        if (browser.indexOf(MSIE_9) > 0 || browser.indexOf(MSIE_8) > 0 || browser.indexOf(MSIE_7) > 0 ) {
        	//DO NOTHING for IE
        } else {
        	response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
        
        String result = "[{'error':'" + "Error" + "'}]";
        
        LOG.error(exception.getMessage(), exception);
        
        respondToClient(request, response, result);    	
    }
}
