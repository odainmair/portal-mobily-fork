package sa.com.mobily.eportal.cacheinstance;

import java.io.Serializable;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;

import com.ibm.ws.security.auth.DistributedMapFactory;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.exception.system.ServiceException;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.util.context.LogEntryVO;



/**
 * This is a general purpose DistributedMap for dynamic cache it can be called
 * either from presentation or business tier.
 * <P>
 * This cache interface based on
 * 
 * @todo check invalidation policy on cachespec.xml make sure the inactive time
 *       is set
 * @author Omar Kalaldeh
 * @version 1.0
 * 
 */
public class GeneralDistributedMap
{
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.CACHING_SERVICE_LOGGER_NAME);

	/*
	 * the DistributedMap with cached data
	 * 
	 * @see com.ibm.websphere.cache.DistributedMap
	 */
	private Map<String, Serializable> map = null;

	private final String CACHE_INSTANCE_JNDI = "CACHE_INSTANCE_JNDI";

	private final String FILE_PROPERTIES = "cacheinstance_configuration";

	private final String className = GeneralDistributedMap.class.getName();

	private static GeneralDistributedMap generalDistributedMap;

	/**
	 * Private constructor
	 */
	@SuppressWarnings("unchecked")
	private GeneralDistributedMap()
	{
		String method = "GeneralDistributedMap";
		try
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.INFO, "GeneralDistributedMap , starting...", className, method, null);
			executionContext.log(logEntryVO);
			map = (Map<String, Serializable>) DistributedMapFactory.getMap(getPropertyValue(CACHE_INSTANCE_JNDI));
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, "DistributedMap.init(), CacheInstance was not found.", className, method, e);
			executionContext.log(logEntryVO);
		}
	}

	/**
	 * Gets the singleton instance for this class
	 * 
	 * @return the singleton instance for this class
	 */
	public static GeneralDistributedMap getInstance()
	{
		if (generalDistributedMap == null)
		{
			generalDistributedMap = new GeneralDistributedMap();
		}
		return generalDistributedMap;

	}

	/**
	 * Removes data from the cache
	 * 
	 * @param cacheEntryId
	 *            the key for cache entry in the map
	 * @throws SessionException
	 */
	public void removeSessionData(String cacheEntryId) throws ServiceException
	{
		String method = "removeSessionData";
		try
		{
			if (map == null)
			{
				throw new ServiceException("The dyna cache distributed map is null");
			}
			map.remove(cacheEntryId);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			throw new ServiceException("Error removing data from dyna cache map for key " + cacheEntryId, e);
		}
	}

	/**
	 * Gets cache entry from the map
	 * 
	 * @param cacheEntryId
	 *            the key for cache entry in the map
	 * @return cache entry from the map
	 * @throws SessionException
	 */
	public Serializable getCachedData(String cacheEntryId)
	{
		String method = "getCachedData";
		try
		{
			if (map == null)
			{
				throw new ServiceException("The dyna cache distributed map is null");
			}
			return map.get(cacheEntryId);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			throw new ServiceException("Error removing data from dyna cache map for key " + cacheEntryId, e);
		}
	}

	/**
	 * Adds new cache entry to the map
	 * 
	 * @param cacheEntryId
	 *            the key for cache entry in the map
	 * @param cachedEntry
	 *            the value will be cached in the map
	 * @throws SessionException
	 */
	public void setCachedData(String cacheEntryId, Serializable cachedEntry)
	{
		String method = "setCachedData";
		try
		{
			if (map == null)
			{
				throw new ServiceException("The dyna cache distributed map is null");
			}
			map.put(cacheEntryId, cachedEntry);
		}
		catch (Exception e)
		{
			LogEntryVO logEntryVO = new LogEntryVO(Level.SEVERE, e.getMessage(), className, method, e);
			executionContext.log(logEntryVO);
			throw new ServiceException("Error removing data from dyna cache map for key " + cacheEntryId, e);
		}
	}

	/**
	 * Reads param from bundle config
	 * 
	 * @param aKey
	 * @return
	 * @throws SessionException
	 */
	private String getPropertyValue(String aKey)
	{
		ResourceBundle tempResourceBundle = null;
		String tempValue = null;
		try
		{
			tempResourceBundle = ResourceBundle.getBundle(FILE_PROPERTIES);
			tempValue = tempResourceBundle.getString(aKey);
		}
		catch (Exception e)
		{
			throw new ServiceException("Error when getPropertyValue for key " + aKey, e);
		}
		return tempValue;
	}
}