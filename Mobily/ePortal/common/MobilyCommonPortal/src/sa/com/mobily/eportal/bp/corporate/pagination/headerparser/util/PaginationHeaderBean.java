package sa.com.mobily.eportal.bp.corporate.pagination.headerparser.util;

public class PaginationHeaderBean {

	private String fromIndex;
	private String toIndex;
	private String totalRecords;
	public String getFromIndex() {
		return fromIndex;
	}
	public void setFromIndex(String fromIndex) {
		this.fromIndex = fromIndex;
	}
	public String getToIndex() {
		return toIndex;
	}
	public void setToIndex(String toIndex) {
		this.toIndex = toIndex;
	}
	public String getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}
}
