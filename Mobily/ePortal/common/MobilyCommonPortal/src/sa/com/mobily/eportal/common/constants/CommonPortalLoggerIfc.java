package sa.com.mobily.eportal.common.constants;

public interface CommonPortalLoggerIfc
{
	public static String COMMON_Portal_LOGGER_NAME = "MobilyCommonPortal/commonPortal";
	public static String CACHING_SERVICE_LOGGER_NAME = "MobilyCommonPortal/cacheService";
	public static String PUMA_SERVICE_LOGGER_NAME = "MobilyCommonPortal/pumaService";
	public static String BILL_SERVICE_LOGGER_NAME = "MobilyCommonPortal/billService";
	public static String PERSONALIZATOIN_LOGGER_NAME = "MobilyCommonPortal/perspnalization";
	public static String SEARCH_LOGGER_NAME = "MobilyCommonPortal/search";
}
