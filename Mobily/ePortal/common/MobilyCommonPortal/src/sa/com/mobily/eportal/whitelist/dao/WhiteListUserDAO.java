package sa.com.mobily.eportal.whitelist.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.core.dao.AbstractDBDAO;
import sa.com.mobily.eportal.core.service.DataSources;
import sa.com.mobily.eportal.whitelist.vo.WhiteListUserVO;

/**
 * @author Suresh Vathsavai-MIT
 * @Description: 
 * @Date Apr 9, 2014
 */
public class WhiteListUserDAO extends AbstractDBDAO<WhiteListUserVO>
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.PERSONALIZATOIN_LOGGER_NAME);
	private static final String CLASS_NAME = WhiteListUserDAO.class.getName();
	

	private static final String GET_WHITE_LIST_USER_BY_USER_NAME = "SELECT MSISDN from SR_WHITELIST_USER_TBL where UPPER(USERNAME) = UPPER(?)";
	
	private WhiteListUserDAO()
	{
		super(DataSources.DEFAULT);
		// TODO Auto-generated constructor stub
	}

	public static WhiteListUserDAO getInstance() {
		return WhiteListUserDAOHolder.whiteListUserDAOInstance;
	}
	
	private static class WhiteListUserDAOHolder {
		private static final WhiteListUserDAO whiteListUserDAOInstance = new WhiteListUserDAO();
	}
	
	/**
	 * @Description: Responsible to check the provided user is white listed or not. 
	 * @Date Apr 9, 2014
	 * @param userVO (userName*)
	 * @return boolean
	 */
	public boolean isWhiteListUser(WhiteListUserVO userVO) {
		executionContext.log(Level.SEVERE, "WhiteListUserDAO > isWhiteListUser : start for userName["+userVO.getUserName()+"]", CLASS_NAME, "isWhiteListUser",null);
		boolean isWhiteList = false;
		List<WhiteListUserVO> list = query(GET_WHITE_LIST_USER_BY_USER_NAME, userVO.getUserName());
		if(list != null && list.size() > 0) {
			isWhiteList = true;
		}
		executionContext.log(Level.SEVERE, "WhiteListUserDAO > isWhiteListUser : end for userName["+userVO.getUserName()+"] and isWhiteList:["+isWhiteList+"]", CLASS_NAME, "isWhiteListUser",null);
		return isWhiteList;
	}
	
	@Override
	protected WhiteListUserVO mapDTO(ResultSet rs) throws SQLException
	{
		WhiteListUserVO userVO = new WhiteListUserVO();
		userVO.setMsisdn(rs.getString("MSISDN"));
		return userVO;
	}
}
