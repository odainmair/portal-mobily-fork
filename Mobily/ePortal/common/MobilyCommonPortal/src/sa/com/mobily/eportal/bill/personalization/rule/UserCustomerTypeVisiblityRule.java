package sa.com.mobily.eportal.bill.personalization.rule;

import java.io.Serializable;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.customerinfo.constant.CustomerTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.LineTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.RoleIdConstant;
import sa.com.mobily.eportal.customerinfo.constant.SubscriptionTypeConstant;
import sa.com.mobily.eportal.customerinfo.constant.VIPTierConstant;

import com.ibm.websphere.personalization.RequestContext;
import com.ibm.websphere.personalization.applicationObjects.SelfInitializingApplicationObject;

public class UserCustomerTypeVisiblityRule implements
SelfInitializingApplicationObject, Serializable {
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);
	/**
	 * Auther @Mohammad Sharaf Hijjeh
	 */
	private static final long serialVersionUID = 1L;
	private String customerType;
	private int subscriptionType;
	private int lineType;
	private String packageId;
	private String vipTier;
	private int roleId;
	
	private final int subGsm = SubscriptionTypeConstant.SUBS_TYPE_GSM;
	private final int subConnect = SubscriptionTypeConstant.SUBS_TYPE_CONNECT;
	private final int subWiMax = SubscriptionTypeConstant.SUBS_TYPE_WiMAX;
	private final int subFTTH = SubscriptionTypeConstant.SUBS_TYPE_FTTH;
	private final int subFV = SubscriptionTypeConstant.SUBS_TYPE_FV; //22516 - ePortal Fixed Voice 
	
	private final int customerPrePaid = CustomerTypeConstant.CUSTOMER_TYPE_PrePaid;
	private final int customerPostpaid = CustomerTypeConstant.CUSTOMER_TYPE_PostPaid;
	
	private final int lineGSM = LineTypeConstant.SUBS_TYPE_GSM;
	private final int line3G = LineTypeConstant.SUBS_TYPE_3G;
	private final int lineLTE = LineTypeConstant.SUBS_TYPE_LTE;
	private final int lineWiMax = LineTypeConstant.SUBS_TYPE_WiMAX;
	private final int lineFTTH = LineTypeConstant.SUBS_TYPE_FTTH;
	private final int lineIPTV = LineTypeConstant.SUBS_TYPE_IPTV;
	private final int lineFV = LineTypeConstant.SUBS_TYPE_FV;//22516 - ePortal Fixed Voice 
	
	private final String vipTierGold = VIPTierConstant.GOLD;
	private final String vipTierPlatinum = VIPTierConstant.PLATINUM;
	private final String vipTierTitanium = VIPTierConstant.TITANIUM;
	
	private final int roleIdConsumer = RoleIdConstant.ROLE_ID_CONSUMER_CUSTOMER;
	private final int roleIdCorporate = RoleIdConstant.ROLE_ID_CORPORATE_CUSTOMER;
	private final int roleIdNonMobily = RoleIdConstant.ROLE_ID_REGISTERED_USER;
	private final int roleIdNonPrimary = RoleIdConstant.ROLE_ID_REGISTERED_MOBILY_USER;
	private final int roleIdNeqatyPartner = RoleIdConstant.ROLE_ID_NEQATY_PARTNER;
	
	public int getSubGsm() {
		return subGsm;
	}

	public int getSubConnect() {
		return subConnect;
	}

	public int getSubWiMax() {
		return subWiMax;
	}

	public int getSubFTTH() {
		return subFTTH;
	}

	public int getSubFV() {//22516 - ePortal Fixed Voice 
		return subFV;
	}
	
	public int getCustomerPrePaid() {
		return customerPrePaid;
	}

	public int getCustomerPostpaid() {
		return customerPostpaid;
	}

	public int getLineGSM() {
		return lineGSM;
	}

	public int getLine3G() {
		return line3G;
	}

	public int getLineLTE() {
		return lineLTE;
	}

	public int getLineWiMax() {
		return lineWiMax;
	}

	public int getLineFTTH() {
		return lineFTTH;
	}

	public int getLineIPTV() {
		return lineIPTV;
	}

	public int getLineFV() {//22516 - ePortal Fixed Voice
		return lineFV;
	}
	public int getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(int subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public int getLineType() {
		return lineType;
	}

	public void setLineType(int lineType) {
		this.lineType = lineType;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getPackageId()
	{
		return packageId;
	}

	public void setPackageId(String packageId)
	{
		this.packageId = packageId;
	}
	
	
	public String getVipTier()
	{
		return vipTier;
	}

	public void setVipTier(String vipTier)
	{
		this.vipTier = vipTier;
	}
	

	public String getVipTierGold()
	{
		return vipTierGold;
	}

	public String getVipTierPlatinum()
	{
		return vipTierPlatinum;
	}

	public String getVipTierTitanium()
	{
		return vipTierTitanium;
	}
	
	public int getRoleId()
	{
		return roleId;
	}

	public void setRoleId(int roleId)
	{
		this.roleId = roleId;
	}

	public int getRoleIdConsumer()
	{
		return roleIdConsumer;
	}

	public int getRoleIdCorporate()
	{
		return roleIdCorporate;
	}

	public int getRoleIdNonMobily()
	{
		return roleIdNonMobily;
	}

	public int getRoleIdNonPrimary()
	{
		return roleIdNonPrimary;
	}
	
	public int getRoleIdNeqatyPartner()
	{
		return roleIdNeqatyPartner;
	}

	@Override
	public void init(RequestContext requestContext) {
//		PznTransformationModel.invalidate((PortletRequest) requestContext);
		fillUserAttributesInSession(requestContext);
//		requestContext.setSessionAttribute("userTelecomeProfile", this);
		requestContext.setRequestAttribute("userTelecomeProfile", this);
//		requestContext.setRequestParameter(arg0, arg1)
	}

	private void fillUserAttributesInSession(RequestContext requestContext)
	{	 
		try {
			
			String username = requestContext.getPznRequestObjectInterface().getUserPrincipal();
			executionContext.severe("Start fillUserAttributesInSession with user::"+username);
			String sessionId = requestContext.getSessionInfo();
			sessionId = sessionId.substring(sessionId.indexOf("_sessionId=")+11,sessionId.indexOf("_sessionId=")+34);
			SessionVO sessionVO = CacheManagerServiceUtil.getCachedData(sessionId,username);
			executionContext.severe("fillUserAttributesInSession sessionVO="+sessionVO);
			this.setCustomerType(sessionVO.getCustomerType());
			this.setPackageId(sessionVO.getPackageId());
			//incase of GSM anf WIMAX there are no values in subscription Type ,setting the values as in SubscriptionType
			if(sessionVO.getLineType()==0 || sessionVO.getLineType()==-1)
			    this.setLineType(sessionVO.getSubscriptionType());
			else
			this.setLineType(sessionVO.getLineType());
			this.setSubscriptionType(sessionVO.getSubscriptionType());
			this.setVipTier(sessionVO.getVipTier());
			
			this.setRoleId(sessionVO.getRoleId());
			
			executionContext.severe("Completed filling user sessionVO for personalization");
		}
		catch(Exception e) {
			executionContext.severe("Exception thrown during fillUserAttributesInSession error="+e);
		}
	}

}
