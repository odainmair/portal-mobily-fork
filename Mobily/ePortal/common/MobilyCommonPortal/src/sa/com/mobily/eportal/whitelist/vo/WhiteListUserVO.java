package sa.com.mobily.eportal.whitelist.vo;

import sa.com.mobily.eportal.common.service.vo.BaseVO;

/**
 * @author Suresh Vathsavai-MIT
 * @Description: 
 * @Date Apr 9, 2014
 */
public class WhiteListUserVO extends BaseVO {

	private static final long serialVersionUID = 1L;
	
	private String userName = null;
	private String msisdn = null;
	
	/**
	 * @return the userName
	 */
	public String getUserName()
	{
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	/**
	 * @return the msisdn
	 */
	public String getMsisdn()
	{
		return msisdn;
	}
	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn)
	{
		this.msisdn = msisdn;
	}
	
	
	
	

	
}
