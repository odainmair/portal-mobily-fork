package sa.com.mobily.eportal.whitelist.personalization;

import com.ibm.websphere.personalization.RequestContext;
import com.ibm.websphere.personalization.applicationObjects.SelfInitializingApplicationObject;
import java.io.Serializable;
import java.util.logging.Level;

import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.whitelist.dao.WhiteListUserDAO;
import sa.com.mobily.eportal.whitelist.vo.WhiteListUserVO;

/**
 * @author Suresh Vathsavai-MIT
 * @Description: Personalization rule to validate whether logged-in user is whitelisted or not. 
 * @Date Apr 9, 2014
 */
public class WhiteListUserVisibilityRule implements SelfInitializingApplicationObject, Serializable
{

	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.PERSONALIZATOIN_LOGGER_NAME);
	private static final String CLASS_NAME = WhiteListUserVisibilityRule.class.getName();
	
	
	private static final long serialVersionUID = 3121037361778662449L;

	private int whiteListUser = 0;
	
	private final int whitelist_yes = 1;
	private final int whitelist_no = 0;
	
	@Override
	public void init(RequestContext requestContext)
	{
		checkWhiteListEligibility(requestContext);
		requestContext.setRequestAttribute("whiteListUserVisibility", this);
	}

	/**
	 * @Description: Responsible to check whether logged-in user is whitelisted or not. 
	 * @Date Apr 9, 2014
	 * @param requestContext
	 */
	private void checkWhiteListEligibility(RequestContext requestContext) {
		String userName = requestContext.getPznRequestObjectInterface().getUserPrincipal();
		executionContext.log(Level.SEVERE, "WhiteListUserVisibilityRule > checkWhiteListEligibility : start for userName["+userName+"]", CLASS_NAME, "checkWhiteListEligibility",null);
		if(userName != null && !"".equals(userName)) {
			WhiteListUserVO userVO = new WhiteListUserVO();
			userVO.setUserName(userName);
			boolean status = WhiteListUserDAO.getInstance().isWhiteListUser(userVO);
			if(status)
				this.setWhiteListUser(whitelist_yes);
			else 
				this.setWhiteListUser(whitelist_no);
		}
	}

	/**
	 * @return the whiteListUser
	 */
	public int getWhiteListUser()
	{
		return whiteListUser;
	}

	/**
	 * @param whiteListUser the whiteListUser to set
	 */
	public void setWhiteListUser(int whiteListUser)
	{
		this.whiteListUser = whiteListUser;
	}

	/**
	 * @return the whitelist_yes
	 */
	public int getWhitelist_yes()
	{
		return whitelist_yes;
	}

	/**
	 * @return the whitelist_no
	 */
	public int getWhitelist_no()
	{
		return whitelist_no;
	}

	

	
	

}
