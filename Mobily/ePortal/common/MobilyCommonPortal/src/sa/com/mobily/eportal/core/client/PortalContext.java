//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.client;

import java.util.Locale;

import javax.naming.NamingException;
import javax.portlet.PortletRequest;

import sa.com.mobily.eportal.cache.util.CacheServiceLocator;
import sa.com.mobily.eportal.cacheinstance.DistributedMapJSRIfc;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.ContextException;
import sa.com.mobily.eportal.core.api.User;
import sa.com.mobily.eportal.core.service.Logger;

import com.ibm.portal.portlet.service.PortletServiceHome;
import com.ibm.portal.portlet.service.PortletServiceUnavailableException;
import com.ibm.portal.um.PumaProfile;
import com.ibm.portal.um.portletservice.PumaHome;

/**
 * An implementation of the Context interface for portlets.
 * @see sa.com.mobily.eportal.core.api.Context
 */
public class PortalContext implements Context {

    private static final Logger log = Logger.getLogger(PortalContext.class);
    private static PumaHome fCachedPumaHomeService = null;

    private PortletRequest fRequest = null;
    private UserImpl currentUser = null;

    public PortalContext(PortletRequest request) {
        fRequest = request;
    }
    
    @Override
    public User getCurrentUser() {
        // if currentUser has not been initialized yet
        if(currentUser == null) {
            initCurrentUser();
//            initTestUser();
        }

        return currentUser;
    }

    private void initTestUser() {
        log.info(">>>>>> PortalContext >> Using Test User.");
        currentUser = new UserImpl();
        currentUser.setUserId("testUser");
        currentUser.setDefaultMSISDN("966565885281");
        currentUser.setServiceAccountNumber("100018908695700");
    }

    private void initCurrentUser() {
        try {
            PumaHome pumaHomeService = getPumaHome();
            PumaProfile profile = pumaHomeService.getProfile(fRequest);            
            com.ibm.portal.um.User puma_user = profile.getCurrentUser();
            
            // Anonymous user
            if (puma_user == null) {
                currentUser = null;
                return;
            }
            
            currentUser = new UserImpl();
            String distinguishedName = profile.getIdentifier(puma_user);
            currentUser.setUserId(extractUserId(distinguishedName));
            log.debug("extracted userId from PUMA [" + currentUser.getUserId() + "]");
            
            PortletServiceHome portletServiceHome = CacheServiceLocator.getDistributedMapService();
            DistributedMapJSRIfc distMap = (DistributedMapJSRIfc)portletServiceHome.getPortletService(DistributedMapJSRIfc.class);
            SessionVO sessionVO = distMap.getSessionData(fRequest.getPortletSession().getId(), currentUser.getUserId());
            
            if(sessionVO != null) {
                currentUser.setDefaultMSISDN(sessionVO.getMsisdn());
                currentUser.setServiceAccountNumber(sessionVO.getServiceAccountNumber());
                currentUser.setCustomerType(sessionVO.getCustomerType());
                currentUser.setLineType(sessionVO.getLineType());
                currentUser.setPackageId(sessionVO.getPackageId());
                currentUser.setSubscriptionType(sessionVO.getSubscriptionType());
                currentUser.setIqama(sessionVO.getIqama());
            }
            else {
                log.error("Unable to retrieve sessionVO");
            }

        } catch (Exception e) {
            log.error("Error while initializing current user.", e);
        }
    }
    
    @Override
    public Locale getLocale() {
        return fRequest.getLocale();
    }
    
    @Override
    public ChannelType getChannel() {
        return ChannelType.web;
    }
    
    private String extractUserId(String distinguishedName) {
        int startIndex = distinguishedName.indexOf("=");
        int endIndex = distinguishedName.indexOf(',');
        String userId = distinguishedName.substring(startIndex + 1, endIndex);
        return userId;
    }

    private static synchronized PumaHome getPumaHome() throws ContextException {
        if (fCachedPumaHomeService != null) {
            return fCachedPumaHomeService;
        }

        try {
            javax.naming.Context ctx = new javax.naming.InitialContext();
            PortletServiceHome portletServiceHome = (PortletServiceHome) ctx.lookup("portletservice/com.ibm.portal.um.portletservice.PumaHome");
            fCachedPumaHomeService = (PumaHome) portletServiceHome.getPortletService(PumaHome.class);
        } catch(NamingException e) {
            fCachedPumaHomeService = null;
            e.printStackTrace();
            throw new ContextException(e);
        } catch(PortletServiceUnavailableException e) {
            fCachedPumaHomeService = null;
            e.printStackTrace();
            throw new ContextException(e);
        } catch (Exception e) {
            fCachedPumaHomeService = null;
            e.printStackTrace();
            throw new ContextException(e);
        }
        return fCachedPumaHomeService;
    }
}
