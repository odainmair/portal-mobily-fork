package sa.com.mobily.eportal.bill.and.payment.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.portlet.PortletRequest;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.exception.MobilyCommonException;
import sa.com.mobily.eportal.common.service.facade.MobilyCommonFacade;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.service.vo.FavoriteNumberInquiryVO;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

import com.xenos.framework.service.repository.IRepositoryServicesServiceLocator;
import com.xenos.framework.service.repository.RepositoryServicesSoapBindingStub;
import com.xenos.framework.ws.jobrunner.JobRunnerWebServiceProxy;
import com.xenos.framework.ws.jobrunner.JobRunnerWebServiceServiceLocator;
import com.xenos.framework.ws.jobrunner.JobRunnerWebServiceSoapBindingStub;
import com.xenos.j2ee.serialization.JobTicketWrapper;
import com.xenos.j2ee.serialization.MapData;

public class BillAndPaymentUtil {

	public BillAndPaymentUtil() {}
	private static BillAndPaymentUtil INSTANCE;
	private static Map<String,String> PHONE_USAGE_CONFIG_MAP = new HashMap<String,String>();
	private static ResourceBundle ARABIC_RESOURCE_BUNDLE;
	private static ResourceBundle ENGLISH_RESOURCE_BUNDLE;
	private static final String CLASS_NAME = BillAndPaymentUtil.class.getName();
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.BILL_SERVICE_LOGGER_NAME);

	public static BillAndPaymentUtil getInsatnce(){
		if(INSTANCE == null)
			INSTANCE = new BillAndPaymentUtil();
		return INSTANCE;
	}

	/**
	 * @MethodName: getPhoneUsageResourceBundleArabic
	 * <p>
	 * Retrieves Resource Bundle object that points to Arabic properties file for phone usage service
	 * 
	 * 
	 * @return ResourceBundle resourceBundle
	 * 
	 * @author Abu Sharaf
	 */
	public static ResourceBundle getPhoneUsageResourceBundleArabic(){
		if(BillAndPaymentUtil.isNull(ARABIC_RESOURCE_BUNDLE))
			ARABIC_RESOURCE_BUNDLE = ResourceBundle.getBundle(BillAndPaymentConstants.BILL_AND_PAYMENT_ARABIC_RESOURCE_BUNDLE, new Locale(BillAndPaymentConstants.ARABIC_LOCALE_SYBMOL));
		return ARABIC_RESOURCE_BUNDLE;		
	}

	/**
	 * @MethodName: getPhoneUsageResourceBundleEnglish
	 * <p>
	 * Retrieves Resource Bundle object that points to English properties file for phone usage service
	 * 
	 * 
	 * @return ResourceBundle resourceBundle
	 * 
	 * @author Abu Sharaf
	 */
	public static ResourceBundle getPhoneUsageResourceBundleEnglish(){
		if(BillAndPaymentUtil.isNull(ENGLISH_RESOURCE_BUNDLE))
			ENGLISH_RESOURCE_BUNDLE = ResourceBundle.getBundle(BillAndPaymentConstants.BILL_AND_PAYMENT_ENGLISH_RESOURCE_BUNDLE, new Locale(BillAndPaymentConstants.ENGLISH_LOCALE_SYMBOL));
		return ENGLISH_RESOURCE_BUNDLE;		
	}


	/**
	 * @MethodName: getResourceBundle
	 * <p>
	 * Retrieves Resource Bundle object By language passed to it
	 * 
	 * @param String locale
	 * 
	 * @return ResourceBundle resourceBundle
	 * 
	 * @author Abu Sharaf
	 */
	public ResourceBundle getResourceBundle(String locale){
		if(BillAndPaymentConstants.ARABIC_LOCALE_SYBMOL.equalsIgnoreCase(locale))
			return getPhoneUsageResourceBundleArabic();
		else
			return getPhoneUsageResourceBundleEnglish();
	}

	/**
	 * @MethodName: getBillModeConfigMap
	 * <p>
	 * Extracts the phone usage map of labels and values from config file to map it for the service result
	 * 
	 * 
	 * @return Map<String,String> PhoneUsageConfigMap
	 * 
	 * @author Abu Sharaf
	 */
	public static Map<String,String> getBillModeConfigMap(){
		if(PHONE_USAGE_CONFIG_MAP.isEmpty())
			BillAndPaymentUtil.getInsatnce().fillConfigMap(PHONE_USAGE_CONFIG_MAP, BillAndPaymentConstants.BILL_MODE_DESCRIPTION_CONFIG_FILE);
		return PHONE_USAGE_CONFIG_MAP;
	}
	
	/**
	 * @MethodName: isNull
	 * <p>
	 * define a way to check any passed object if it's initialized or not.
	 * 
	 * @param Object value
	 * 
	 * @return Boolean true if value is null, false if not.
	 * 
	 * @author Abu Sharaf
	 */
	public static boolean isNull(Object value){
		boolean isNull = false;
		if(value == null || value.equals(""))
			isNull = true;
		return isNull;
	}
	
	/**
	 * @MethodName: isValidEmail
	 * <p>
	 * Check if the passed String is a valid email address
	 * 
	 * @param String email
	 * 
	 * @return Boolean true = valid email, false = not valid
	 * 
	 * @author Abu Sharaf
	 */
	public static boolean isValidEmail(String email) {
		Pattern p = Pattern.compile("[[A-Z][a-z][0-9]_.-]+@[[A-Z][a-z][0-9]]+\\.[[A-Z][a-z][0-9].]+");
		Matcher m = p.matcher(email);
		return m.matches();
	}
	
	/**
	 * @MethodName: isValidNumber
	 * <p>
	 * Check if the passed String is a valid number
	 * 
	 * @param String number
	 * 
	 * @return Boolean true = valid number, false = not valid
	 * 
	 * @author Abu Sharaf
	 */
	public static boolean isValidNumber(String number){
		Pattern p = Pattern.compile(BillAndPaymentConstants.NUMBER_FORMAT_PATTERN);
		Matcher m = p.matcher(number);
		return m.matches();
	}

	/**
	 * @MethodName: constructDateAndTime
	 * <p>
	 * Takes full date and time as a long along with the pattern to produce a formatted date by that pattern
	 * 
	 * @param long time
	 * @param String pattern
	 * 
	 * @return String well-formed date as a String 
	 * 
	 * @author Abu Sharaf
	 */
	public static String constructDateAndTime(long time,String pattern){
		Date date = new Date((time * 1000) - Calendar.getInstance().getTimeZone().getRawOffset());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(date);
	}

	/**
	 * @MethodName: constructDateAndTime
	 * <p>
	 * Format passed date object as per the passed pattern
	 * 
	 * @param Date date
	 * @param String pattern
	 * 
	 * @return String constructed date and time
	 * 
	 * @author Abu Sharaf
	 */
	public static String constructDateAndTime(Date date,String pattern){
		if(date==null) return "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		return simpleDateFormat.format(date);
	}

	/**
	 * @MethodName: isValidLength
	 * <p>
	 * Check if the passed object length is between the passed min and max length
	 * 
	 * @param String object
	 * @param int maxLength
	 * @param int minLength
	 * 
	 * @return Boolean true = valid length, false = not valid
	 * 
	 * @author Abu Sharaf
	 */
	public static boolean isValidLength(String object,int maxLength,int minLength){
		boolean isValidLength = true;
		if((object.length() < minLength) || (object.length() > maxLength)){
			isValidLength = false ;
		}
		return isValidLength;
	}

	/**
	 * @MethodName: isValidLength
	 * <p>
	 * Check if the passed object length is equal to the passed length
	 * 
	 * @param String object
	 * @param int length
	 * 
	 * @return Boolean true = valid length, false = not valid
	 * 
	 * @author Abu Sharaf
	 */
	public static boolean isValidLength(String object, int length){
		boolean isValidLength = true;
		if(object.length() != length){
			isValidLength = false;
		}
		return isValidLength;
	}

	/**
	 * @MethodName: fillConfigMap
	 * <p>
	 * Fill map of labels and values as per the passed resource bundle to convert all the keys
	 * and values inside it to map of labels and values
	 * 
	 * @param Map<String,String> configMap
	 * @param String bundleName
	 * 
	 * 
	 * @author Abu Sharaf
	 */
	public void fillConfigMap(Map<String,String> configMap,String bundleName){
		ResourceBundle configBundle = ResourceBundle.getBundle(bundleName);
		Enumeration<String> keys = configBundle.getKeys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			configMap.put(key, configBundle.getString(key));
			key = null;
		}
	}
	
	/**
	 * @MethodName: getCustomerInfo
	 * <p>
	 * Fill a custom Bean VO with the values from dynamic cache manager
	 * 
	 * @param PortletRequest request
	 * 
	 * @return CustomerInfoBean customerInfoBean
	 * 
	 * @author Abu Sharaf
	 */
	public CustomerInfoBean getCustomerInfo(PortletRequest request){
		request.setAttribute(BillAndPaymentConstants.EC_PORTLET_CLASS,CLASS_NAME );
		request.setAttribute(BillAndPaymentConstants.EC_PORTLET_METHOD, BillAndPaymentConstants.GET_CUSTOMER_INFO_METHOD_NAME );
		CustomerInfoBean customerInfoBean = new CustomerInfoBean();
		try {
			SessionVO sessionVO = CacheManagerServiceUtil.getCachedData(request.getPortletSession().getId(), request.getRemoteUser());
			customerInfoBean.setOtherSupscription(sessionVO.getOtherSubscriptions());
			customerInfoBean.setCustomerType(sessionVO.getCustomerType());
			customerInfoBean.setLineType(sessionVO.getLineType());
			customerInfoBean.setMsidsn(sessionVO.getMsisdn());
			customerInfoBean.setServiceAcctNumber(sessionVO.getServiceAccountNumber());
			customerInfoBean.setSubscriptionType(sessionVO.getSubscriptionType());
			customerInfoBean.setUserId(request.getRemoteUser());
		} catch (Exception e) {			
			request.setAttribute(BillAndPaymentConstants.EC_EXCEPTION_OCCURRED, BillAndPaymentConstants.EC_EXCEPTION_OCCURRED_YES_FRAGMENT);
			executionContext.log(Level.SEVERE,BillAndPaymentConstants.EX_EXCEPTION+e.getMessage(),CLASS_NAME,BillAndPaymentConstants.GET_CUSTOMER_INFO_METHOD_NAME,e);
		}
		return customerInfoBean;
	}

	/**
	 * @MethodName: getFavoriteInternationalNumber
	 * <p>
	 * Retrieve international favorite number by the passed msisdn and user id
	 * 
	 * @param String msisdn
	 * @param String userId
	 * @param PortletRequest request
	 * 
	 * @return String intFavNumber
	 * 
	 * @author Abu Sharaf
	 */
	public String getFavoriteInternationalNumber(String msidsn,String userId,PortletRequest request) {
		request.setAttribute(BillAndPaymentConstants.EC_PORTLET_CLASS,CLASS_NAME );
		request.setAttribute(BillAndPaymentConstants.EC_PORTLET_METHOD, BillAndPaymentConstants.GET_FAV_INT_NUMBER_METHOD_NAME );
		MobilyCommonFacade commonFacade = new MobilyCommonFacade();
		FavoriteNumberInquiryVO  requestVO = new FavoriteNumberInquiryVO();
		requestVO.setMsisdn(msidsn);
		requestVO.setRequestorUserId(userId);
		String favoriteNumber = "";
		FavoriteNumberInquiryVO favoriteNumberInquiryVO;
		try {
			favoriteNumberInquiryVO = commonFacade.getFavoriteNumberInquiry(requestVO);
			if((isNull(favoriteNumberInquiryVO)) || (isNull(favoriteNumberInquiryVO.getInternationalList())) || (favoriteNumberInquiryVO.getInternationalList().size() == 0))
				return "";
		} catch (MobilyCommonException e){
			request.setAttribute(BillAndPaymentConstants.EC_EXCEPTION_OCCURRED, BillAndPaymentConstants.EC_EXCEPTION_OCCURRED_YES_FRAGMENT);
			executionContext.log(Level.SEVERE,BillAndPaymentConstants.EX_EXCEPTION+e.getMessage(),CLASS_NAME,BillAndPaymentConstants.GET_FAV_INT_NUMBER_METHOD_NAME,e);
			return "";
		}
		favoriteNumber = (String)favoriteNumberInquiryVO.getInternationalList().get(0);
		return favoriteNumber;
	}
	
	/**
	 * @MethodName: requestNewDocument
	 * <p>
	 * Returns array of bytes (Stream) for the bill by it's number
	 * 
	 * @param String billNumber
	 * @param PortletRequest request
	 * 
	 * @return byte[] dataStream
	 * 
	 * @author Abu Sharaf
	 */
	public byte[] requestNewDocument(String billNumber,PortletRequest request) {

		byte[] tempByteArrayOutputStream = null;
		String errorMessage = "";
		String statusCode = "";
		JobTicketWrapper jobTicketWrapper = null;
		request.setAttribute(BillAndPaymentConstants.EC_PORTLET_CLASS,CLASS_NAME );
		request.setAttribute(BillAndPaymentConstants.EC_PORTLET_METHOD, BillAndPaymentConstants.REQUEST_NEW_DOCUMENT_METHOD_NAME);
		
		executionContext.log(Level.INFO,"requestNewDocument Started...",CLASS_NAME,"requestNewDocument",null);
		executionContext.log(Level.INFO,"requestNewDocument > billNumber: " + billNumber,CLASS_NAME,"requestNewDocument",null);
		
		try{
			// for creating session
			IRepositoryServicesServiceLocator loginLocator = new IRepositoryServicesServiceLocator();
			RepositoryServicesSoapBindingStub loginStub = (RepositoryServicesSoapBindingStub) loginLocator.getRepositoryServices();

			// for actual method call
			JobRunnerWebServiceServiceLocator locator = new JobRunnerWebServiceServiceLocator();
			JobRunnerWebServiceSoapBindingStub stub = (JobRunnerWebServiceSoapBindingStub) locator.getJobRunnerWebService();
			stub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY,BillAndPaymentConstants.XENOS_USER);
			stub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY,BillAndPaymentConstants.XENOS_PWD);
			//JobRunnerWebServiceProxy proxy = new JobRunnerWebServiceProxy();
			String serviceEndpoint = ResourceEnvironmentProviderService.getInstance().getStringProperty(BillAndPaymentConstants.XENOS_WEB_SERVICE_URL);
			stub._setProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY, serviceEndpoint);

			MapData mapData = new MapData();
			String keyAry[] = { BillAndPaymentConstants.KEY_REQUEST_TYPE,BillAndPaymentConstants.KEY_BILL_NUMBER };
			mapData.setKeys(keyAry);
			String valAry[] = { BillAndPaymentConstants.REQUEST_TYPE_RETRIEVE, billNumber };
			mapData.setValues(valAry);

			jobTicketWrapper = stub.runJob(BillAndPaymentConstants.XENOS_ALIAS, mapData,null, null);

			if (jobTicketWrapper != null) {
				statusCode = jobTicketWrapper.getStatus();

				if (jobTicketWrapper.getFatalErrorNumber() > 0 || jobTicketWrapper.getErrorNumber() > 0) {
					errorMessage = jobTicketWrapper.getFailureMessage();

				} else {

					MapData tempMapData = jobTicketWrapper.getResultMap();
					if (tempMapData != null) {
						String tempKeys[] = tempMapData.getKeys();
						int retrivedDocVal = -1;
						for (int i = 0; tempKeys != null && i < tempKeys.length; i++) {
							if (BillAndPaymentConstants.XENOS_RETRIEVED_DOCUMENT.equalsIgnoreCase(tempKeys[i])) {
								retrivedDocVal = i;
							}
						}
						Object tempValues[] = tempMapData.getValues();
						if (tempValues != null && retrivedDocVal >= 0 && tempValues.length > retrivedDocVal) {
							tempByteArrayOutputStream = (byte[]) tempValues[retrivedDocVal];

						}
					}
				}
			}
		}catch(Exception exception){
			executionContext.log(Level.SEVERE,BillAndPaymentConstants.EX_EXCEPTION+exception.getMessage(),CLASS_NAME,BillAndPaymentConstants.REQUEST_NEW_DOCUMENT_METHOD_NAME,exception);
		}
		return tempByteArrayOutputStream;
	}
	
	/**
	 * @MethodName: requestNewDocumentCorporate
	 * <p>
	 * Returns array of bytes (Stream) for the bill by Root billing accout number and monthYear (mmyyyy)
	 * 
	 * @param String billNumber
	 * @param PortletRequest request
	 * 
	 * @return byte[] dataStream
	 * 
	 * @author Abu Sharaf
	 */
	public byte[] requestNewDocumentCorporate(String rootBillingAccountNumber, String monthYear) {

		byte[] tempByteArrayOutputStream = null;
		String errorMessage = "";
		String statusCode = "";
		JobTicketWrapper jobTicketWrapper = null;
		try{
			
			executionContext.log(Level.INFO,"requestNewDocumentCorporate Started...",CLASS_NAME,"requestNewDocumentCorporate",null);
			executionContext.log(Level.INFO,"requestNewDocumentCorporate > rootBillingAccountNumber: " + rootBillingAccountNumber + ", monthYear: " + monthYear,CLASS_NAME,"requestNewDocumentCorporate",null);
			
			// for creating session
			IRepositoryServicesServiceLocator loginLocator = new IRepositoryServicesServiceLocator();
			RepositoryServicesSoapBindingStub loginStub = (RepositoryServicesSoapBindingStub) loginLocator.getRepositoryServices();

			// for actual method call
			JobRunnerWebServiceServiceLocator locator = new JobRunnerWebServiceServiceLocator();
			JobRunnerWebServiceSoapBindingStub stub = (JobRunnerWebServiceSoapBindingStub) locator.getJobRunnerWebService();
			stub._setProperty(javax.xml.rpc.Stub.USERNAME_PROPERTY,BillAndPaymentConstants.XENOS_USER);
			stub._setProperty(javax.xml.rpc.Stub.PASSWORD_PROPERTY,BillAndPaymentConstants.XENOS_PWD);
			//JobRunnerWebServiceProxy proxy = new JobRunnerWebServiceProxy();
			String serviceEndpoint = ResourceEnvironmentProviderService.getInstance().getStringProperty(BillAndPaymentConstants.XENOS_WEB_SERVICE_URL);
			stub._setProperty(javax.xml.rpc.Stub.ENDPOINT_ADDRESS_PROPERTY, serviceEndpoint);

			MapData mapData = new MapData();
			String keyAry[] = { BillAndPaymentConstants.KEY_REQUEST_TYPE,BillAndPaymentConstants.MSISDN, 
					BillAndPaymentConstants.BILL_START_DATE, BillAndPaymentConstants.BILL_END_DATE };
			
			mapData.setKeys(keyAry);
			String valAry[] = { BillAndPaymentConstants.REQUEST_TYPE_RETRIEVE, rootBillingAccountNumber, "01"+monthYear, "01"+monthYear };
			mapData.setValues(valAry);

			jobTicketWrapper = stub.runJob(BillAndPaymentConstants.XENOS_ALIAS, mapData,null, null);

			if (jobTicketWrapper != null) {
				statusCode = jobTicketWrapper.getStatus();

				if (jobTicketWrapper.getFatalErrorNumber() > 0 || jobTicketWrapper.getErrorNumber() > 0) {
					errorMessage = jobTicketWrapper.getFailureMessage();

				} else {

					MapData tempMapData = jobTicketWrapper.getResultMap();
					if (tempMapData != null) {
						String tempKeys[] = tempMapData.getKeys();
						int retrivedDocVal = -1;
						for (int i = 0; tempKeys != null && i < tempKeys.length; i++) {
							if (BillAndPaymentConstants.XENOS_RETRIEVED_DOCUMENT.equalsIgnoreCase(tempKeys[i])) {
								retrivedDocVal = i;
							}
						}
						Object tempValues[] = tempMapData.getValues();
						if (tempValues != null && retrivedDocVal >= 0 && tempValues.length > retrivedDocVal) {
							tempByteArrayOutputStream = (byte[]) tempValues[retrivedDocVal];

						}
					}
				}
			}
		}catch(Exception exception){
			executionContext.log(Level.SEVERE,BillAndPaymentConstants.EX_EXCEPTION+exception.getMessage(),CLASS_NAME,"requestNewDocumentCorporate",exception);
		}
		return tempByteArrayOutputStream;
	}
	
	/**
	 * @MethodName: getCorporateAccountNumber
	 * <p>
	 * Get corporate master account number for the current logged in user from LDAP
	 * 
	 * 
	 * @return String corpAccountNumber
	 * 
	 * @author Abu Sharaf
	 */
	public String getCorporateAccountNumber(){
		PumaUtil pumaUtil = new PumaUtil();
		return pumaUtil.getCorpMasterAcctNo();
	}
}
