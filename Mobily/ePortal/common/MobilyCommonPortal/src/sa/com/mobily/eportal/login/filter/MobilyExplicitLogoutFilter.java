package sa.com.mobily.eportal.login.filter;

import java.util.logging.Level;

import javax.security.auth.login.LoginException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.LDAPService;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;
import sa.com.mobily.eportal.resourceenvprovider.service.ResourceEnvironmentProviderService;

import com.ibm.portal.auth.ExplicitLogoutFilter;
import com.ibm.portal.auth.FilterChainContext;
import com.ibm.portal.auth.LogoutFilterChain;
import com.ibm.portal.auth.exceptions.LogoutException;
import com.ibm.portal.security.SecurityFilterConfig;
import com.ibm.portal.security.exceptions.SecurityFilterInitException;

public class MobilyExplicitLogoutFilter implements ExplicitLogoutFilter
{
	private static final String LOGOUT_REDIRECT_URL_RESOURCE_KEY = "LOGOUT_REDIRECT_URL";

	String redirectURL = null;
	
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);

	private static final String className = LDAPService.class.getName();

	@Override
	public void destroy()
	{
	}

	@Override
	public void init(SecurityFilterConfig arg0) throws SecurityFilterInitException
	{
		try
		{
			redirectURL = (String) ResourceEnvironmentProviderService.getInstance().getPropertyValue(LOGOUT_REDIRECT_URL_RESOURCE_KEY);
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "MobilyExplicitLogoutFilter > init > Failed to read logout redirect URL from  resource enviroment > Exception::" + e.getMessage(), className, "init");
		}
	}

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, FilterChainContext filterChainContext, LogoutFilterChain chain) throws LogoutException,
			LoginException
	{
		String method = "logout";
		executionContext.log(Level.FINE, "MobilyExplicitLogoutFilter > Logging out before chain.logout()" , className, method);
		chain.logout(request, response, filterChainContext);
		try
		{
			eraseCookies(request, response);
			executionContext.log(Level.FINE, "MobilyExplicitLogoutFilter > Cookies have been removed successfully...." , className, method);
		}
		catch (Exception e)
		{
			executionContext.log(Level.FINE, "MobilyExplicitLogoutFilter > Failed to remove cookies error=" + e.getMessage() , className, method);
		}
		PumaUtil pumaUtil = new PumaUtil();
		if (pumaUtil.getRoleId() == null)
		{
			// throw new
			// MobilyLoginFilterException("No Role found for user. User must have Role assigned");
		}
		try
		{
			CacheManagerServiceUtil.removeCachedData(request.getSession().getId(), pumaUtil.getUid());
		}
		catch (Exception e)
		{
			executionContext.log(Level.FINE, "MobilyExplicitLogoutFilter > Not able to remove cache manager entry from caching service... error=" + e.getMessage() , className, method);
		}

		filterChainContext.setRedirectURL(redirectURL);
		executionContext.log(Level.FINE, "MobilyExplicitLogoutFilter > Logging out after chain.logout()" , className, method);
	}

	public static Cookie eraseCookie(String strCookieName, String strPath)
	{
		Cookie cookie = new Cookie(strCookieName, "");
		cookie.setMaxAge(0);
		cookie.setPath(strPath);

		return cookie;
	}

	private void eraseCookies(HttpServletRequest req, HttpServletResponse resp)
	{
		String method = "eraseCookies";
		Cookie[] cookies = req.getCookies();
		if (cookies != null)
			for (int i = 0; i < cookies.length; i++)
			{
				executionContext.log(Level.FINE, "MobilyExplicitLogoutFilter > eraseCookies > Removing cookie::" + cookies[i].getValue() , className, method);
				cookies[i].setValue("");
				cookies[i].setPath("/");
				cookies[i].setMaxAge(0);
				resp.addCookie(cookies[i]);
			}
	}
}