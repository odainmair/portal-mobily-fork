package sa.com.mobily.eportal.site.portlets.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class BeanValidator<T> {
	private static ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
   
	public static <T> Set<ConstraintViolation<T>> validateBean(T t){
		 Validator validator = factory.getValidator();
		 return validator.validate(t);
	}
	
	// added by MHASSAN to support groups
	public static <T> Set<ConstraintViolation<T>> validateBean(T t, Class<?> paramArrayOfClass){
		 Validator validator = factory.getValidator();
		 return validator.validate(t,paramArrayOfClass);
	}
		
	public static <T> Map<String,String> validate(T t){
		 Map<String,String> voilationsMap = new HashMap<String, String>();
		 Set<ConstraintViolation<T>> constraintViolations = validateBean(t);
		 if(null != constraintViolations && !constraintViolations.isEmpty()){
			 Iterator<ConstraintViolation<T>> iterator = constraintViolations.iterator();
			 while(iterator.hasNext()){
				 ConstraintViolation<T> voilation = iterator.next();
				 voilationsMap.put(voilation.getPropertyPath().toString(), voilation.getMessage());
			 }
		 }
		 return voilationsMap;
	}
	
	public static <T> Set<ConstraintViolation<T>> validateBeanProperty(T t,String strProperty){
		 Validator validator = factory.getValidator();
		 return validator.validateProperty(t, strProperty);
	}
	
	public static <T> Map<String,String> validateProperty(T t,String strProperty){
		 Map<String,String> voilationsMap = new HashMap<String, String>();
		 Set<ConstraintViolation<T>> constraintViolations = validateBeanProperty(t,strProperty);
		 if(null != constraintViolations && !constraintViolations.isEmpty()){
			 Iterator<ConstraintViolation<T>> iterator = constraintViolations.iterator();
			 while(iterator.hasNext()){
				 ConstraintViolation<T> voilation = iterator.next();
				 voilationsMap.put(voilation.getPropertyPath().toString(), voilation.getMessage());
			 }
		 }
		 return voilationsMap;
	}
}
