package sa.com.mobily.eportal.cache.util;

import sa.com.mobily.eportal.cacheinstance.DistributedMapJSRIfc;
import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;
import sa.com.mobily.eportal.common.exception.system.ServiceException;

import com.ibm.portal.portlet.service.PortletServiceHome;
import com.ibm.portal.portlet.service.PortletServiceUnavailableException;

public class CacheManagerServiceUtil{

	public static SessionVO getCachedData(String aSessionId, String userName){
		SessionVO sessionVO = null;
		DistributedMapJSRIfc service = null;
		userName=getUID(userName);
		PortletServiceHome portletServiceHome = CacheServiceLocator.getDistributedMapService();
		if (portletServiceHome != null)
			try{
				service = (DistributedMapJSRIfc) portletServiceHome.getPortletService(DistributedMapJSRIfc.class);
			}catch (PortletServiceUnavailableException e){
				throw new ServiceException(e.getMessage(), e);
			}
		if (service != null)
			sessionVO = (SessionVO) service.getSessionData(aSessionId, userName);
		return sessionVO;
	}

	public static SessionVO setCachedData(String aSessionId, String userName, String newNumber) throws Exception{
		SessionVO sessionVO = null;
		DistributedMapJSRIfc service = null;
		userName=getUID(userName);
		PortletServiceHome portletSericeHome = CacheServiceLocator.getDistributedMapService();
		if (portletSericeHome != null)
			service = (DistributedMapJSRIfc) portletSericeHome.getPortletService(DistributedMapJSRIfc.class);
		if (service != null)
			sessionVO = service.setSessionData(aSessionId, userName, newNumber);
		return sessionVO;
	}

	public static void removeCachedData(String aSessionId, String userName) throws Exception{
		DistributedMapJSRIfc service = null;
		userName=getUID(userName);
		PortletServiceHome portletSericeHome = CacheServiceLocator.getDistributedMapService();
		if (portletSericeHome != null)
			service = (DistributedMapJSRIfc) portletSericeHome.getPortletService(DistributedMapJSRIfc.class);
		if (service != null)
			service.removeSessionData(aSessionId, userName);
	}
	private static String getUID(String name){
		 if(name.startsWith("uid=")&&name.indexOf(',')!=-1)
			  return  name.substring(name.indexOf('=')+1, name.indexOf(','));
		  return name;
					  
	}
	public static void main (String x[]){
		System.out.println(getUID("abdelhamidfgs"));
		System.out.println(getUID("uid=corporate2,cn=users,o=mobily,c=sa"));
	}
}