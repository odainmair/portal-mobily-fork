//************************************************************************
// Licensed Material - Property of Mobily.
//
// (c) Copyright Mobily 2013. All rights reserved.
//************************************************************************
package sa.com.mobily.eportal.core.client;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import sa.com.mobily.eportal.core.api.Context;
import sa.com.mobily.eportal.core.api.ErrorCodes;
import sa.com.mobily.eportal.core.api.InternalServerErrorException;
import sa.com.mobily.eportal.core.api.MethodNotSupportedException;
import sa.com.mobily.eportal.core.api.ResourceAlreadyExistsException;
import sa.com.mobily.eportal.core.api.ResourceManager;
import sa.com.mobily.eportal.core.api.ResourceNotFoundException;
import sa.com.mobily.eportal.core.api.ServiceException;
import sa.com.mobily.eportal.core.api.UserAuthorizationException;
import sa.com.mobily.eportal.core.api.ValidationException;
import sa.com.mobily.eportal.core.service.EPortalUtils;
import sa.com.mobily.eportal.core.service.Logger;
import sa.com.mobily.eportal.core.service.ResourceMapper;
import sa.com.mobily.eportal.core.service.ResourceMapper.MappingException;

/**
 * A base portlet class to provided to support REST-based functionality.
 * The serveResource() method provides the needed GET, POST, PUT and DELETE
 * RST functionality.
 * <p>
 * By default, it calls the ResourceManager EJB as its backend, but for
 * portrlets with no backend (such as the portlets that display iframe) the
 * mechanism for inherited portlets will be to override the method
 * getResourceManagerJNDIName() and return null.
 */
public abstract class BasePortlet extends GenericPortlet {

    private static final Logger LOG = Logger.getLogger(BasePortlet.class);
    private static final String RESOURCE_MANAGER_NAME = "sa.com.mobily.eportal.core.api.resource-manager";
    private static final String RESOURCE_MANAGER_JNDI_NAME = "java:comp/env/ejb/ResourceManager";
    private static final String ERROR_MSG_BUNDLE = "nl.error-messages";
    private static final String ERROR_MSG_KEY_PREFIX = "error.message.";

    private ResourceMapper fMapper = new ResourceMapper();
    

    /**
     * @see javax.portlet.Portlet#init()
     */
    public void init() throws PortletException {
        super.init();

        String resourceManagerJNDIName = getResourceManagerJNDIName();
        if (resourceManagerJNDIName != null) {
            ResourceManager rm = (ResourceManager) getPortletContext().getAttribute(RESOURCE_MANAGER_NAME);
            try {
                if (rm == null) {
                    javax.naming.Context context = new InitialContext();
                    rm = (ResourceManager) context.lookup(resourceManagerJNDIName);
                    getPortletContext().setAttribute(RESOURCE_MANAGER_NAME, rm);
                }
            } catch (NamingException e) {
                e.printStackTrace();
                throw new PortletException(e);
            }
        }
    }

    protected String getResourceManagerJNDIName() {
        return RESOURCE_MANAGER_JNDI_NAME;
    }

    public void doView(RenderRequest request, RenderResponse response) throws PortletException, IOException {
    }

    public void serveResource(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {
        boolean exceptionOccured = false;
        try {
            LOG.entering("serveResource");
            if (getResourceManager() == null) throw new MethodNotSupportedException("serveResource");
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            String method = request.getMethod();
            String uri = getResourceURI(request.getParameterMap());

            if ("GET".equalsIgnoreCase(method)) {
                doGet(request, response, uri);
            } else if ("POST".equalsIgnoreCase(method)) {
                // Tunneling the POST, PUT and DELETE
                String data = request.getParameter("data");
                String actionMethod = request.getProperty("X-HTTP-Method-Override");
                if ("POST".equalsIgnoreCase(actionMethod)) {
                    doPost(request, response, uri, data);
                } else if ("PUT".equalsIgnoreCase(actionMethod)) {
                    doPut(request, response, uri, data);
                } else if ("DELETE".equalsIgnoreCase(actionMethod)) {
                    doDelete(request, response, uri);
                } else {
                    throw new IllegalArgumentException("Invalid HTTP POST method: " + actionMethod);
                }
            } else {
                throw new IllegalArgumentException("Invalid HTTP method: " + method);
            }

        } catch (ResourceNotFoundException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_NOT_FOUND);
        } catch (UserAuthorizationException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_UNAUTHORIZED);
        } catch (MethodNotSupportedException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        } catch (MappingException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ValidationException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
        } catch (ResourceAlreadyExistsException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_CONFLICT);
        } catch (InternalServerErrorException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (RuntimeException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (ServiceException e) {
            exceptionOccured = true;
            handleException(request, response, e, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } finally {
            LOG.exiting("serveResource");
            LOG.flush(getClass().getName(), exceptionOccured);
        }
    }

    private String getResourceURI(Map<String, String[]> parameters) {
        Set<String> paramNames = parameters.keySet();
        String resouceParameters = "";

        for (String paramName : paramNames) {
            if (! paramName.equals("resource-uri")) {
                if (resouceParameters.length() > 0) {
                    resouceParameters += "&";
                }
                resouceParameters += paramName + "=" + parameters.get(paramName)[0];
            }
        }

        String resourceURI = parameters.get("resource-uri")[0];

        if (! "".equals(resouceParameters)) {
            resourceURI += "?" + resouceParameters;
        }

        return resourceURI;
    }

    private void doGet(ResourceRequest request, ResourceResponse response, String uri) throws ServiceException, IOException {
        // process the request
        Object result = getResourceManager().get(getCtx(request), uri);

        // writing the result to the response
        response.getWriter().write(fMapper.write(result));
        setStatusCode(response, HttpServletResponse.SC_OK);
    }

    private void doPost(ResourceRequest request, ResourceResponse response, String uri, String data) throws ServiceException, IOException {
        LOG.info("POST: " + uri + " data: " + data);

        // process the request
        Object result = getResourceManager().post(getCtx(request), uri, data);

        // writing the result to the response
        if (result != null) {
            response.getWriter().write(fMapper.write(result));
            setStatusCode(response, HttpServletResponse.SC_OK);
        } else {
            setStatusCode(response, HttpServletResponse.SC_NO_CONTENT);
        }
    }

    private void doPut(ResourceRequest request, ResourceResponse response, String uri, String data) throws ServiceException, IOException {
        LOG.info("PUT: " + uri);

        // process the request
        getResourceManager().put(getCtx(request), uri, data);

        // writing the result to the response
        setStatusCode(response, HttpServletResponse.SC_OK);
    }

    private void doDelete(ResourceRequest request, ResourceResponse response, String uri) throws ServiceException, IOException {
        LOG.info("DELETE: " + uri);

        // process the request
        getResourceManager().delete(getCtx(request), uri);

        // writing the result to the response
        setStatusCode(response, HttpServletResponse.SC_OK);
    }

    private void handleException(ResourceRequest request, ResourceResponse response, Exception e, int httpStatusCode) {
        LOG.error(e.getMessage(), "handleException", null);
        //response.setProperty("message", e.getMessage());

        ErrorDetails error = new ErrorDetails();
        error.setErrorCode(ErrorCodes.GENERAL_ERROR);   // default error code, if not assigned
        error.setErrorDetails(e.getMessage());

        Object[] messageParams = null;
        // if e is ServiceException, get the errorCode and messageParams(if any) from it
        if(e instanceof ServiceException) {
            ServiceException serviceEx = (ServiceException) e;
            error.setErrorCode(serviceEx.getErrorCode());
            messageParams = serviceEx.getMessageParams();
        }

        // get the localized error message
        try {
            String errorMessage = getLocalizedErrorMessage(error.getErrorCode(), request.getLocale());

            // if there are message substitution parameters
            if(messageParams != null) {
                errorMessage = MessageFormat.format(errorMessage, messageParams);       // substitute the parameters in the message
            }

            error.setErrorMessage(errorMessage);
            response.getWriter().write(new ResourceMapper().write(error));      // write error details to the response
        } catch(Exception ex) {
            LOG.error("Failed to resolve localized exception message.", ex);
        }

        setStatusCode(response, httpStatusCode);
    }

    private void setStatusCode(ResourceResponse response, int httpStatusCode) {
        response.setProperty(ResourceResponse.HTTP_STATUS_CODE, String.valueOf(httpStatusCode));
    }

    private Context getCtx(PortletRequest request) {
        return new PortalContext(request);
    }

    /**
     * A helper method to retrieve the view manager from the application context
     */
    private ResourceManager getResourceManager() {
        return (ResourceManager) getPortletContext().getAttribute(RESOURCE_MANAGER_NAME);
    }

    protected final Object getResource(RenderRequest request, String uri) throws ServiceException {
        if (getResourceManager() == null) throw new MethodNotSupportedException("getResource");
        try {
            return getResourceManager().get(getCtx(request), uri);
        } catch (MethodNotSupportedException e) {
            return getResourceManager().post(getCtx(request), uri, "{}");
        }
    }

    private String getLocalizedErrorMessage(int errorCode, Locale locale) {
        String msg = EPortalUtils.getProperty(ERROR_MSG_BUNDLE, locale, ERROR_MSG_KEY_PREFIX + errorCode, this.getClass().getClassLoader());

        if(msg == null) {
            LOG.error("Unable to get localized exception message for error code [" + errorCode + "] and locale [" + locale + "]", "getLocalizedErrorMessage");
            return EPortalUtils.getProperty(ERROR_MSG_BUNDLE, locale, ERROR_MSG_KEY_PREFIX + ErrorCodes.GENERAL_ERROR, this.getClass().getClassLoader());
        }

        return msg;
    }

}
