/**
 * @author s.vathsavai.mit
 * This file is used to read the data from the file, for example Excel, text, .csv etc.
 */
package sa.com.mobily.eportal.site.portlets.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.portlet.vo.UploadListVO;
import sa.com.mobily.eportal.common.service.util.FormatterUtility;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;


public class FileUtility {
	
	public static final String FILE_TYPE_XLS = ".xls";
	public static final String FILE_TYPE_XLSX = ".xlsx";
	public static final String DONT_READ_THIS_COLUMN = "No need";
	private static ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);
	private static String className = FileUtility.class.getName();

	
	/**
	 * 1. First row should be header
	 * 2. inputFile object can be Struts FormFile or can be IO File object
	 * 3. if maxRows is zero it will read all the rows
	 * 4. Always number of columns in columnsMap should be less than or equal to total columns in file, else throws error
	 * 
	 * Input parameters:
	 * inputFile : Source file object. It can be java.io.File or org.apache.struts.upload.FormFile object
	 * dataVOClass : Destination VO Class which contains instance members and setters and getters.
	 * columnsMap : Configuration Map. Contains key and value pairs, Keys are column names in Excel file and values are corresponding variables in the destination VO.
	 * maxRows : Maximum number of row to read form the Source file. If this value is Zero, then it will be considered as total number of rows in source file
	 * 
	 * returns: list of destination VO objects. one vo object contains information of one row in source file
	 */
	
	public static UploadListVO readExcel(InputStream inputStream) {
		UploadListVO uploadListVO = new UploadListVO();
		ArrayList<String[]> dataVOList = new ArrayList<String[]>();
		String methodName = "readExcel";
		try {
		    POIFSFileSystem fs = new POIFSFileSystem(inputStream);
		    HSSFWorkbook wb = new HSSFWorkbook(fs);
		    HSSFSheet sheet = wb.getSheetAt(0);
		    HSSFRow row;
		    int rows; // No of rows
		    rows = sheet.getLastRowNum();		    
		    for(int r = 0; r <= rows; r++) {
		        row = sheet.getRow(r);
		          	ArrayList<String> rowArray = new ArrayList<String>();
		          	if(row!=null && row.getCell(0)!=null){
		        	for (int i = 0; i < row.getLastCellNum(); i++)
					{
						if(row.getCell(i)!=null && row.getCell(i).getCellType() != HSSFCell.CELL_TYPE_BLANK){
							String value = "";
							switch (row.getCell(i).getCellType())
							{
							case HSSFCell.CELL_TYPE_NUMERIC:
								value = FormatterUtility.numberFormatter(row.getCell(i).getNumericCellValue());
								break;
							case HSSFCell.CELL_TYPE_BOOLEAN:
								value = row.getCell(i).getBooleanCellValue()+"";
								break;
							case HSSFCell.CELL_TYPE_STRING:
								value = row.getCell(i).getStringCellValue();
								break;
							/*default:
								value = row.getCell(i).toString();
								break;*/
							}
							executionContext.log(Level.FINE, "value : "+value, className, methodName, null);
							rowArray.add(value);
						}
					}
		          	if(r==0)
		          	{
		          		uploadListVO.setHeader((String[]) rowArray.toArray(new String[rowArray.size()]));
		          	}
		          	else
		          		dataVOList.add((String[]) rowArray.toArray(new String[rowArray.size()]));
		          	}
		        }
		    uploadListVO.setRows(dataVOList);
		    //}
		} catch(Exception ioe) {
		    ioe.printStackTrace();
		}
		return uploadListVO;
		
		
	}
	
	
	/**
	 * Responsible to set the date in the VO object by invoking the corresponding setter method
	 * @param dataVO
	 * @param vo_MemberName
	 * @param value
	 * @return
	 */
	/*public static Object setData(Object dataVO,String vo_MemberName,String value) throws MobilyCommonException {
		Class cls = dataVO.getClass();
		Method[] methods = cls.getDeclaredMethods();
		String valueAr[] = {value};
		for(int i=0; i<methods.length; i++) {
			Method method = methods[i];
			if(method.getReturnType() == java.lang.Void.TYPE) {
				if(method.getName().toUpperCase().indexOf(vo_MemberName.toUpperCase()) != -1) {
					try {
						method.invoke(dataVO, valueAr);
						break;
					} catch (IllegalArgumentException e) {
						// throw error UNABLE_TO_INVOKE_METHOD
						throw new MobilyCommonException(FileErrorCodesIfc.UNABLE_TO_INVOKE_METHOD);
					} catch (IllegalAccessException e) {
						throw new MobilyCommonException(FileErrorCodesIfc.UNABLE_TO_INVOKE_METHOD);
					} catch (InvocationTargetException e) {
						throw new MobilyCommonException(FileErrorCodesIfc.UNABLE_TO_INVOKE_METHOD);
					}
				}
			}
		}
		return dataVO;
	}*/
	
	/**
	 * Responsible to create the instance for the provided class
	 * @param cls
	 * @return instance of the input param
	 */
	/*public static Object getDataVOInstance(Class cls) throws MobilyCommonException {
		try {
			return cls.newInstance();
		} catch (InstantiationException e) {
			throw new MobilyCommonException(FileErrorCodesIfc.UNABLE_TO_INVOKE_METHOD);
		} catch (IllegalAccessException e) {
			throw new MobilyCommonException(FileErrorCodesIfc.UNABLE_TO_INVOKE_METHOD);
		}
		
	}*/
	
	/**
     * This method is used to read the data from the csv file. it will read the data separated by (,). And in the uploaded document first row should be header
     * @param inputStream
     * @return UploadListVO
     */
    public static UploadListVO readCSVFile(InputStream inputStream){
    	UploadListVO uploadListVO = new UploadListVO();
    	ArrayList<String[]> dataVOList = new ArrayList<String[]>();
    	String br = null;
    	String cvsSplitBy = ",";
    	String methodName = "readCSVFile";
     
    	try {
    		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
    		br = in.readLine();
    		uploadListVO.setHeader(br.split(cvsSplitBy));
    		while ((br = in.readLine()) != null) {
    			String[] member = br.split(cvsSplitBy);
    			dataVOList.add(member);   			
    		}
    		in.close();
    	
    	} catch (IOException e) {
    		e.printStackTrace();
    	} 
    	uploadListVO.setRows(dataVOList);
    	executionContext.log(Level.FINE, "Done", className, methodName, null);
      return uploadListVO;
    }
    
    /**
     * This method is used to read the data from the text file. it will read the data with Columns Separated by (:) . And in the uploaded document first row should be header
     * @param inputStream
     * @return UploadListVO
     */
    public static UploadListVO readTextFile(InputStream inputStream){
    	UploadListVO uploadListVO = new UploadListVO();
    	ArrayList<String[]> dataVOList = new ArrayList<String[]>();
    	String br = null;
    	String cvsSplitBy = ":";
    	String methodName = "readTextFile";
     
    	try {
    		BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
    		br = in.readLine();
    		uploadListVO.setHeader(br.split(cvsSplitBy));
    		while ((br = in.readLine()) != null) {
    			String[] member = br.split(cvsSplitBy);
    			dataVOList.add(member);   			
    		}
    		in.close();
    	
    	} catch (IOException e) {
    		e.printStackTrace();
    	} 
    	uploadListVO.setRows(dataVOList);
    	executionContext.log(Level.FINE, "Done", className, methodName, null);
      return uploadListVO;
    }
    
    public static void main(String ar[]) {
    	
    	
    }
    
}
