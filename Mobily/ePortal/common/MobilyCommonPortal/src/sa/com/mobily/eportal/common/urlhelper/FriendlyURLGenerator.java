package sa.com.mobily.eportal.common.urlhelper;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibm.portal.resolver.friendly.FriendlyURL;
import com.ibm.portal.state.exceptions.StateException;

public class FriendlyURLGenerator
{

	private static FriendlyURLGenerator INSTANCE = null;
	private static com.ibm.wps.model.ModelUtil util = null;
	private static com.ibm.portal.navigation.NavigationNode node = null;
	private static com.ibm.portal.resolver.friendly.service.PortalFriendlySelectionServiceHome psh = null; 
	private static javax.naming.Context ctx = null;
	private static com.ibm.portal.resolver.friendly.service.PortalFriendlySelectionService service = null;

	@SuppressWarnings("deprecation")
	private FriendlyURLGenerator(){
//		try
//		{
//			util =  com.ibm.wps.model.ModelUtil.from((javax.servlet.ServletRequest)request);
//			node = (com.ibm.portal.navigation.NavigationNode) util.getNavigationSelectionModel().getSelectedNode();
//		}
//		catch (ModelException e)
//		{
//			e.printStackTrace();
//		}
		try
		{
			ctx = new javax.naming.InitialContext();
			psh = (com.ibm.portal.resolver.friendly.service.PortalFriendlySelectionServiceHome)ctx.lookup("portal:service/resolver/FriendlySelectionService");
		}
		catch (NamingException e)
		{
			e.printStackTrace();
		}
		
	}

	public static FriendlyURLGenerator getInstance(){
		if(INSTANCE==null)
			INSTANCE = new FriendlyURLGenerator();
		return INSTANCE;
	}

	private String getPortalPageUniqueName(){
		String pageUniqueName="";
		if(node != null && node.getContentNode() != null && node.getContentNode().getObjectID().getUniqueName() != null)
			pageUniqueName= node.getContentNode().getObjectID().getUniqueName().trim();
		
		return pageUniqueName;
	}
	
	private static String getSelectURL(String selectURL,HttpServletRequest request,boolean isPrefix, boolean isLoggedIn)
	{
		if(isEmpty(selectURL))
			return selectURL;
		if(isPrefix)
		{
			selectURL = selectURL.replaceFirst("/wps/portal", "");
			selectURL = selectURL.replaceFirst("/wps/myportal", "");
			
			String urlPrefix="/wps/portal";
			if(isLoggedIn){
				urlPrefix="/wps/myportal";
			}
			selectURL =urlPrefix+ selectURL ;
		}
		return selectURL;
	}
	
	private synchronized static com.ibm.portal.resolver.friendly.service.PortalFriendlySelectionService getFriendlyService(HttpServletRequest request,
			HttpServletResponse response)
	{
		try {
			service = psh.getPortalFriendlySelectionService(request, response);
			return service;
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
	
	public synchronized String generateFriendlyURL(String uniqueName,HttpServletRequest request,
			HttpServletResponse response,boolean isPrefix, boolean isLoggedIn) throws StateException, NamingException, IOException
	{
		String selectURL=null;
		try {
			com.ibm.portal.resolver.friendly.service.PortalFriendlySelectionService service = getFriendlyService(request,response);
			FriendlyURL url=service.getURLFactory().newURL();
			url.setSelection(uniqueName);	
			selectURL =url.toString();
			service.dispose();
			selectURL=getSelectURL(selectURL,request,isPrefix, isLoggedIn);
		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
		return selectURL;
	}
	
	private static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0
				|| str.trim().equalsIgnoreCase("null");
	}

}
