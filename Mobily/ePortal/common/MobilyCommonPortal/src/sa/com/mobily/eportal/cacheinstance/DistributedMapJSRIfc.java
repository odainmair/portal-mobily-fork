package sa.com.mobily.eportal.cacheinstance;

import sa.com.mobily.eportal.cacheinstance.valueobjects.SessionVO;

import com.ibm.portal.portlet.service.PortletService;

public interface DistributedMapJSRIfc extends PortletService
{
	public SessionVO setSessionData(String sessionId, String userName, String newNumber);

	public SessionVO getSessionData(String sessionId, String userName);

	public void removeSessionData(String sessionId, String userName);
}