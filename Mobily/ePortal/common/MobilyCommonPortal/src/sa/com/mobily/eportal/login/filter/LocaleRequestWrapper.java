package sa.com.mobily.eportal.login.filter;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import sa.com.mobily.eportal.common.service.util.FormatterUtility;

import com.ibm.wsspi.webcontainer.servlet.IServletRequest;

public class LocaleRequestWrapper extends HttpServletRequestWrapper implements com.ibm.wsspi.webcontainer.servlet.IServletRequest
{
	private String localeStr = null;

	private Locale localeObj;

	private Vector<String> locales = new Vector<String>(1);

	public LocaleRequestWrapper(HttpServletRequest request)
	{
		super(request);

		localeStr = request.getParameter("locale");
		if (FormatterUtility.isNotEmpty(localeStr))
		{
			if (localeStr.equalsIgnoreCase("ar"))
				localeObj = new Locale("ar");
			else if (localeStr.equalsIgnoreCase("en"))
				localeObj = new Locale("en");
		}

		if (localeObj != null)
			locales.add(localeStr);
	}

	public Object clone() throws CloneNotSupportedException
	{
		LocaleRequestWrapper crequest = (LocaleRequestWrapper) super.clone();
		javax.servlet.ServletRequest inner = crequest.getRequest();
		if (inner instanceof IServletRequest)
		{
			crequest.setRequest((HttpServletRequest) ((IServletRequest) inner).clone());
		}
		else
		{
			throw new CloneNotSupportedException();
		}
		return crequest;
	}

	private boolean isAcceptLanguageHeader(String s)
	{
		return s.intern() == "Accept-Language" || s.equalsIgnoreCase("Accept-Language");
	}

	public String getHeader(String header)
	{
//		System.out.println("LocaleRequestWrapper getHeader  header=" + header);
		if (isAcceptLanguageHeader(header))
		{//
			return localeStr;
		}
		else
		{
//			System.out.println("LocaleRequestWrapper getHeader  isAcceptLanguageHeader=false header  =" + header + "super.getHeader(header)=" + super.getHeader(header));
			return super.getHeader(header);
		}
	}

	public Enumeration getHeaders(String s)
	{
		// System.out.println("LocaleRequestWrapper getHeaders  header="+s);
		if (isAcceptLanguageHeader(s))
		{//
			// System.out.println("LocaleRequestWrapper getHeaders  isAcceptLanguageHeader=true return  ="+locales.elements());
			return locales.elements();
		}
		else
		{
			// System.out.println("LocaleRequestWrapper getHeaders  isAcceptLanguageHeader=false header  ="+s
			// +"super.getHeader(header)"+super.getHeaders(s));
			return super.getHeaders(s);
		}
	}

	public Locale getLocale()
	{
		// System.out.println("getLocale started.");

		if (localeObj != null)
		{
			// System.out.println("getLocale return get cahced locale="+localeObj);
			return localeObj;
		}
		else
		{
			// get the original request
			HttpServletRequest request = (HttpServletRequest) getRequest();
			// System.out.println("getLocale return original requestlocale="+request.getLocale());
			return request.getLocale();
		}
	}

	public Enumeration getLocales()
	{
		if (localeObj != null)
		{
			return Collections.enumeration(locales);
		}
		else
		{
			// get the original request
			HttpServletRequest request = (HttpServletRequest) getRequest();
			Enumeration<Locale> enu = request.getLocales();
			return request.getLocales();
		}
	}
}