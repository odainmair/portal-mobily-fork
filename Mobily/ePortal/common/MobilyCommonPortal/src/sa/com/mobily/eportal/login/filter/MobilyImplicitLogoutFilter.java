package sa.com.mobily.eportal.login.filter;

import java.util.logging.Level;

import javax.security.auth.login.LoginException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sa.com.mobily.eportal.cache.util.CacheManagerServiceUtil;
import sa.com.mobily.eportal.common.constants.CommonPortalLoggerIfc;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContext;
import sa.com.mobily.eportal.common.service.util.context.ExecutionContextFactory;
import sa.com.mobily.eportal.common.util.puma.PumaUtil;

import com.ibm.portal.auth.FilterChainContext;
import com.ibm.portal.auth.ImplicitLogoutFilter;
import com.ibm.portal.auth.LogoutFilterChain;
import com.ibm.portal.auth.exceptions.LogoutException;
import com.ibm.portal.security.SecurityFilterConfig;
import com.ibm.portal.security.exceptions.SecurityFilterInitException;

public class MobilyImplicitLogoutFilter implements ImplicitLogoutFilter
{

	private ExecutionContext executionContext = ExecutionContextFactory.getExecutionContext(CommonPortalLoggerIfc.COMMON_Portal_LOGGER_NAME);

	private String className = MobilyImplicitLogoutFilter.class.getName();

	@Override
	public void destroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void init(SecurityFilterConfig arg0) throws SecurityFilterInitException
	{
		String methodName = "init";
		executionContext.log(Level.FINE, "MobilyImplicitLogoutFilter > init > called", className, methodName);
	}

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, FilterChainContext filterChainContext, LogoutFilterChain chain) throws LogoutException,
			LoginException
	{
		String methodName = "logout";
		executionContext.log(Level.FINE, "MobilyImplicitLogoutFilter > Logging out before chain.logout()", className, methodName);
		chain.logout(request, response, filterChainContext);
		PumaUtil pumaUtil = new PumaUtil();
		try
		{
			CacheManagerServiceUtil.removeCachedData(request.getSession().getId(), pumaUtil.getUid());
		}
		catch (Exception e)
		{
			executionContext.log(Level.SEVERE, "MobilyImplicitLogoutFilter > Not able to remove cache manager entry from caching service > Exception=" + e.getMessage(), className,
					methodName);
		}
		executionContext.log(Level.FINE, "MobilyImplicitLogoutFilter > Logging out after chain.logout()", className, methodName);
	}

}
